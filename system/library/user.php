<?php
class User {
	private $user_id;
	private $username;
	private $email;
	private $permission = array();
	private $division;
	private $region;
	private $site;
	private $device;
	private $company_id;

	public function __construct($registry) {
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');

		if (isset($this->session->data['user_id'])) {
			$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$this->session->data['user_id'] . "' AND status = '1'");

			if ($user_query->num_rows) {
				$this->user_id = $user_query->row['user_id'];
				$this->email = $user_query->row['email'];
				$this->username = $user_query->row['username'];
				$this->division = $user_query->row['division'];
				$this->region = $user_query->row['region'];
				$this->site = $user_query->row['site'];
				$this->device = $user_query->row['device'];
				$this->company_id = $user_query->row['company_id'];

				$this->db->query("UPDATE " . DB_PREFIX . "user SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE user_id = '" . (int)$this->session->data['user_id'] . "'");

				$uids = $this->db->query("SELECT `uid` FROM `oc_user_group` WHERE `user_group_id` = '".$user_query->row['user_group_id']."' ");
				$uid = '9999';
				if($uids->num_rows > 0){
					$uid = $uids->row['uid'];
				}
				if($user_query->row['company_id'] == ''){
					$user_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_group WHERE uid = '" . (int)$uid . "' AND `company_id` = '1' ");
					$permissions = unserialize($user_group_query->row['permission']);

					if (is_array($permissions)) {
						foreach ($permissions as $key => $value) {
							$this->permission[$key] = $value;
						}
					}
				} else {
					$company_array = explode(',', html_entity_decode($user_query->row['company_id']));
					sort($company_array);
					foreach($company_array as $ckey => $cvalue){
						$user_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_group WHERE uid = '" . (int)$uid . "' AND `company_id` = '".(int)$cvalue."' ");
						if($user_group_query->num_rows > 0){
							$permissions = unserialize($user_group_query->row['permission']);
							if (is_array($permissions)) {
								foreach ($permissions as $key => $value) {
									$this->permission[$key] = $value;
								}
								break;
							}
						}
					}
					/*
					//$company_string = "'" . str_replace(",", "','", html_entity_decode($user_query->row['company_id'])) . "'";
					//$user_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_group WHERE uid = '" . (int)$uid . "' AND LOWER(`company_id`) IN (" . strtolower($company_string) . ") ");
					$company_string = str_replace("multiselect-all,", "", html_entity_decode($user_query->row['company_id']));
					$company_array = explode(',', html_entity_decode($company_string));
					sort($company_array);
					$access_array = array();
					$add_array = array();
					$modify_array = array();
					$delete_array = array();
					for($i = 1; $i <= 20; $i++){
						$access_array[$i] = array();
						$add_array[$i] = array();
						$modify_array[$i] = array();
						$delete_array[$i] = array();
					}
					foreach($company_array as $ckey => $cvalue){
						if(!isset($access_array[$cvalue])){
							$access_array[$cvalue] = array();
						}
						if(!isset($add_array[$cvalue])){
							$add_array[$cvalue] = array();
						}
						if(!isset($modify_array[$cvalue])){
							$modify_array[$cvalue] = array();
						}
						if(!isset($delete_array[$cvalue])){
							$delete_array[$cvalue] = array();
						}
						$user_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_group WHERE uid = '" . (int)$uid . "' AND `company_id` = '".(int)$cvalue."' ");
						if($user_group_query->num_rows > 0){
							$permissions = unserialize($user_group_query->row['permission']);
							if (is_array($permissions)) {
								foreach ($permissions as $key => $value) {
									if($key == 'access'){
										$access_array[$cvalue] = $value;
									}
									if($key == 'add'){
										$add_array[$cvalue] = $value;
									}
									if($key == 'modify'){
										$modify_array[$cvalue] = $value;
									}
									if($key == 'delete'){
										$delete_array[$cvalue] = $value;
									}
									//$this->permission[$key] = $value;
								}
							}
						}
					}
					$access_array_result = array_merge($access_array[1], $access_array[2], $access_array[3], $access_array[4], $access_array[5], $access_array[6], $access_array[7], $access_array[8], $access_array[9], $access_array[10]);
					$access_array_result = array_unique($access_array_result);

					$add_array_result = array_merge($add_array[1], $add_array[2], $add_array[3], $add_array[4], $add_array[5], $add_array[6], $add_array[7], $add_array[8], $add_array[9], $add_array[10]);
					$add_array_result = array_unique($add_array_result);

					$modify_array_result = array_merge($modify_array[1], $modify_array[2], $modify_array[3], $modify_array[4], $modify_array[5], $modify_array[6], $modify_array[7], $modify_array[8], $modify_array[9], $modify_array[10]);
					$modify_array_result = array_unique($modify_array_result);

					$delete_array_result = array_merge($delete_array[1], $delete_array[2], $delete_array[3], $delete_array[4], $delete_array[5], $delete_array[6], $delete_array[7], $delete_array[8], $delete_array[9], $delete_array[10]);
					$delete_array_result = array_unique($delete_array_result);

					$this->permission['access'] = $access_array_result;
					$this->permission['add'] = $add_array_result;
					$this->permission['modify'] = $modify_array_result;
					$this->permission['delete'] = $delete_array_result;
					*/
				}
				
				
				//$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");
				//$permissions = unserialize($user_group_query->row['permission']);
				// if (is_array($permissions)) {
				// 	foreach ($permissions as $key => $value) {
				// 		$this->permission[$key] = $value;
				// 	}
				// }
			} else {
				/*
				$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "employee WHERE emp_code = '" . (int)$this->session->data['user_id'] . "' AND status = '1'");
				if ($user_query->num_rows) {
					$this->user_id = $user_query->row['emp_code'];
					$this->username = $user_query->row['username'];
					$this->division = $user_query->row['division'];
					$this->region = $user_query->row['region'];
					$this->site = $user_query->row['site'];
					$this->device = $user_query->row['device'];
					$this->company_id = $user_query->row['company_id'];
					
					$this->db->query("UPDATE " . DB_PREFIX . "employee SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE emp_code = '" . (int)$this->session->data['user_id'] . "'");

					$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

					$permissions = unserialize($user_group_query->row['permission']);

					if (is_array($permissions)) {
						foreach ($permissions as $key => $value) {
							$this->permission[$key] = $value;
						}
					}
				} else {
				*/
					$this->logout();
				//}

			}
		}
	}

	public function login($username, $password) {
		unset($this->session->data['user_id']);
		unset($this->session->data['emp_code']);
		unset($this->session->data['is_dept']);
		unset($this->session->data['dept_name']);
		unset($this->session->data['d_emp_id']);
		unset($this->session->data['is_user']);
		unset($this->session->data['is_super']);
		unset($this->session->data['is_super1']);
		$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE username = '" . $this->db->escape($username) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");

		if ($user_query->num_rows) {
			$this->session->data['user_id'] = $user_query->row['user_id'];
			if($user_query->row['is_super'] == '1'){
				$this->session->data['is_super1'] = $user_query->row['is_super'];
				//$this->session->data['d_emp_id'] = 0;//$user_query->row['emp_code'];
			}
			$this->user_id = $user_query->row['user_id'];
			$this->username = $user_query->row['username'];			
			$this->division = $user_query->row['division'];
			$this->region = $user_query->row['region'];
			$this->site = $user_query->row['site'];
			$this->device = $user_query->row['device'];
			$this->company_id = $user_query->row['company_id'];

			$uids = $this->db->query("SELECT `uid` FROM `oc_user_group` WHERE `user_group_id` = '".$user_query->row['user_group_id']."' ");
			$uid = '9999';
			if($uids->num_rows > 0){
				$uid = $uids->row['uid'];
			}

			if($user_query->row['company_id'] == ''){
				$user_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_group WHERE uid = '" . (int)$uid . "' AND `company_id` = '1' ");
				$permissions = unserialize($user_group_query->row['permission']);

				if (is_array($permissions)) {
					foreach ($permissions as $key => $value) {
						$this->permission[$key] = $value;
					}
				}
			} else {
				//$company_string = "'" . str_replace(",", "','", html_entity_decode($user_query->row['company_id'])) . "'";
				//$user_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_group WHERE uid = '" . (int)$uid . "' AND LOWER(`company_id`) IN (" . strtolower($company_string) . ") ");
				$company_array = explode(',', html_entity_decode($user_query->row['company_id']));
				sort($company_array);
				foreach($company_array as $ckey => $cvalue){
					$user_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_group WHERE uid = '" . (int)$uid . "' AND `company_id` = '".(int)$cvalue."' ");
					if($user_group_query->num_rows > 0){
						$permissions = unserialize($user_group_query->row['permission']);
						if (is_array($permissions)) {
							foreach ($permissions as $key => $value) {
								$this->permission[$key] = $value;
							}
							break;
						}
					}
				}
			}
			return true;
		} else {
			/*
			$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "employee WHERE username = '" . $this->db->escape($username) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
			if ($user_query->num_rows) {
				$this->session->data['user_id'] = $user_query->row['emp_code'];
				if($user_query->row['is_super'] == '1'){
					$this->session->data['is_super'] = $user_query->row['is_super'];
					$this->session->data['d_emp_id'] = $user_query->row['emp_code'];
				} elseif($user_query->row['is_dept'] == '1'){
					$this->session->data['is_dept'] = $user_query->row['is_dept'];
					$this->session->data['dept_name'] = $user_query->row['department'];
					$this->session->data['d_emp_id'] = $user_query->row['emp_code'];
				} else {
					$this->session->data['is_user'] = '1';
					$this->session->data['emp_code'] = $user_query->row['emp_code'];
				}

				$this->user_id = $user_query->row['emp_code'];
				$this->username = $user_query->row['username'];
				$this->division = $user_query->row['division'];
				$this->region = $user_query->row['region'];
				$this->site = $user_query->row['site'];
				$this->device = $user_query->row['device'];
				$this->company_id = $user_query->row['company_id'];

				$this->db->query("UPDATE " . DB_PREFIX . "employee SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE emp_code = '" . (int)$user_query->row['emp_code'] . "'");

				$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

				$permissions = unserialize($user_group_query->row['permission']);

				if (is_array($permissions)) {
					foreach ($permissions as $key => $value) {
						$this->permission[$key] = $value;
					}
				}
				return true;
			} else {
				return false;
			}
			*/
		}
	}

	public function logout() {
		unset($this->session->data['user_id']);
		unset($this->session->data['emp_code']);
		unset($this->session->data['is_dept']);
		unset($this->session->data['dept_name']);
		unset($this->session->data['d_emp_id']);
		unset($this->session->data['is_user']);
		unset($this->session->data['is_super']);
		unset($this->session->data['is_super1']);

		$this->user_id = '';
		$this->username = '';
		$this->division = '';
		$this->region = '';
		$this->site = '';
		$this->device = '';
		$this->company_id = '';

		session_destroy();
	}

	public function hasPermission($key, $value) {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){    
  			return  true;
		} else {
			if (isset($this->permission[$key])) {
				return in_array($value, $this->permission[$key]);
			} else {
				return false;
			}
		}
	}

	public function isLogged() {
		return $this->user_id;
	}

	public function getId() {
		return $this->user_id;
	}

	public function getdivision() {
		return $this->division;
	}

	public function getregion() {
		return $this->region;
	}

	public function getsite() {
		return $this->site;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getdevice() {
		return $this->device;
	}

	public function getCompanyId() {
		return $this->company_id;
	}

	public function getUserNameId($user_id) {
		$user_datas = $this->db->query("SELECT * FROM oc_user WHERE user_id = '".$user_id."' ");
		$user_name = '';
		if ($user_datas->num_rows > 0) {
			$user_name = $user_datas->row['username'];
		}
		return $user_name;
	}

	public function getUserName() {
		if(isset($this->session->data['is_dept']) && $this->session->data['is_dept'] == '1'){
			$user_name = $this->db->query("SELECT `name` FROM " . DB_PREFIX . "employee WHERE `emp_code` = '" . $this->db->escape($this->session->data['d_emp_id']) . "' ")->row['name'];
		} elseif(isset($this->session->data['is_user'])){
			$user_name = $this->db->query("SELECT `name` FROM " . DB_PREFIX . "employee WHERE `emp_code` = '" . $this->db->escape($this->session->data['user_id']) . "' ")->row['name'];
		} elseif(isset($this->session->data['is_super'])){
			$user_name = $this->db->query("SELECT `name` FROM " . DB_PREFIX . "employee WHERE `emp_code` = '" . $this->db->escape($this->session->data['d_emp_id']) . "' ")->row['name'];
		}else {
			$user_name = $this->username;
		}
		return $user_name;
	}
}
?>