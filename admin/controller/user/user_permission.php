<?php
class ControllerUserUserPermission extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		$this->getList();
	}

	public function insert() {
		$this->language->load('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_user_user_group->addUserGroup($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_user_user_group->editUserGroup($this->request->get['user_group_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() { 
		$this->language->load('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $user_group_id) {
				$this->model_user_user_group->deleteUserGroup($user_group_id);	
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}	

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('user/user_permission/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('user/user_permission/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['user_groups'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$user_group_total = $this->model_user_user_group->getTotalUserGroups();

		$results = $this->model_user_user_group->getUserGroups($data);

		foreach ($results as $result) {
			if($result['user_group_id'] != '1'){
				$action = array();

				$action[] = array(
					'text' => $this->language->get('text_edit'),
					'href' => $this->url->link('user/user_permission/update', 'token=' . $this->session->data['token'] . '&user_group_id=' . $result['user_group_id'] . $url, 'SSL')
				);		

				$this->data['user_groups'][] = array(
					'user_group_id' => $result['user_group_id'],
					'name'          => $result['name'],
					'selected'      => isset($this->request->post['selected']) && in_array($result['user_group_id'], $this->request->post['selected']),
					'action'        => $action
				);
			}
		}	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_name'] = $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $user_group_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();				

		$this->data['sort'] = $sort; 
		$this->data['order'] = $order;

		$this->template = 'user/user_group_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_access'] = $this->language->get('entry_access');
		$this->data['entry_modify'] = $this->language->get('entry_modify');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['user_group_id'])) {
			$this->data['action'] = $this->url->link('user/user_permission/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('user/user_permission/update', 'token=' . $this->session->data['token'] . '&user_group_id=' . $this->request->get['user_group_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['user_group_id']) && $this->request->server['REQUEST_METHOD'] != 'POST') {
			$user_group_info = $this->model_user_user_group->getUserGroup($this->request->get['user_group_id']);
		}

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($user_group_info)) {
			$this->data['name'] = $user_group_info['name'];
		} else {
			$this->data['name'] = '';
		}

		if (isset($this->request->post['uid'])) {
			$this->data['uid'] = $this->request->post['uid'];
		} elseif (!empty($user_group_info)) {
			$this->data['uid'] = $user_group_info['uid'];
		} else {
			$last_uids = $this->db->query("SELECT `uid` FROM `oc_user_group` ORDER BY `uid` DESC LIMIT 1");
			if($last_uids->num_rows > 0){
				$last_uid = $last_uids->row['uid'];
				$uid = $last_uid + 1;
			} else {
				$uid = 1;
			}
			$this->data['uid'] = $uid;
		}

		$ignore = array(
			'common/home',
			'common/startup',
			'common/login',
			'common/logout',
			'common/forgotten',
			'common/reset',			
			'error/not_found',
			'error/permission',
			'common/footer',
			'common/header'
		);

		$this->data['permissions'] = array();

		// $files = glob(DIR_APPLICATION . 'controller/*/*.php');

		// foreach ($files as $file) {
		// 	$data = explode('/', dirname($file));

		// 	$permission = end($data) . '/' . basename($file, '.php');

		// 	if (!in_array($permission, $ignore)) {
		// 		$this->data['permissions'][] = $permission;
		// 	}
		// }
		$this->data['permissions']['catalog/company'] = 'Company';
		$this->data['permissions']['catalog/contractor'] = 'Contractor';
		$this->data['permissions']['catalog/category'] = 'Category';
		$this->data['permissions']['catalog/region'] = 'Region';
		$this->data['permissions']['catalog/division'] = 'Division';
		$this->data['permissions']['catalog/state'] = 'State';
		$this->data['permissions']['catalog/unit'] = 'Site';
		$this->data['permissions']['catalog/department'] = 'Department';
		$this->data['permissions']['catalog/designation'] = 'Designation';
		$this->data['permissions']['catalog/grade'] = 'Grade';
		$this->data['permissions']['catalog/employement'] = 'Employement';
		$this->data['permissions']['catalog/employee'] = 'Employee';
		$this->data['permissions']['catalog/employee_change'] = 'Employee Transfer';
		$this->data['permissions']['catalog/employeelocal'] = 'Employee Exit';
		$this->data['permissions']['catalog/shift'] = 'Shift';
		$this->data['permissions']['catalog/device'] = 'Device';
		$this->data['permissions']['catalog/in_process'] = 'Spooling';
		$this->data['permissions']['transaction/manualpunch'] = 'Manual Punch';
		//$this->data['permissions']['catalog/employeelocal'] = 'Local Employee';
		//$this->data['permissions']['catalog/shift_schedule'] = 'Employee Shift Schedule';
		//$this->data['permissions']['catalog/week'] = 'Week Off';
		//$this->data['permissions']['catalog/holiday'] = 'Holiday';
		//$this->data['permissions']['catalog/shift_schedule'] = 'Shift Schedule';
		$this->data['permissions']['report/today'] = 'Today Attendance';
		$this->data['permissions']['report/daily'] = 'Daily In/Out Report';
		$this->data['permissions']['report/daily1'] = 'Daily In/Out 1 Report';
		$this->data['permissions']['report/dailylabour'] = 'Daily Labour Report';
		$this->data['permissions']['report/muster'] = 'Muster Report';
		//$this->data['permissions']['report/dailysummary'] = 'Daily Summary Report';
		//$this->data['permissions']['report/late'] = 'Late Report';
		//$this->data['permissions']['report/early'] = 'Early Report';
		//$this->data['permissions']['report/lessworking'] = 'Less Working Hours Report';
		//$this->data['permissions']['report/lateearly'] = 'Late / Early / Less Report';
		//$this->data['permissions']['report/lateearlysumm'] = 'Late / Early / Less Summary Report';
		//$this->data['permissions']['report/working'] = 'Working Hours Report';
		//$this->data['permissions']['report/averageworking'] = 'Average Working Hours Report';
		//$this->data['permissions']['report/performance'] = 'Monthly Detail Report';
		//$this->data['permissions']['report/attendance'] = 'Daily Attendance';
		$this->data['permissions']['report/employeehistory'] = 'Employee Transfer History Report';
		//$this->data['permissions']['report/manualpunch'] = 'Manual Punch Report';
		//$this->data['permissions']['catalog/leavemaster'] = 'Leave Master';
		//$this->data['permissions']['catalog/leave'] = 'Employee Leave Master';
		//$this->data['permissions']['transaction/leave'] = 'Leave Entry';
		//$this->data['permissions']['transaction/leaveprocess'] = 'Leave Process';
		//$this->data['permissions']['report/leave_register'] = 'Leave Register';
		//$this->data['permissions']['report/leave_report'] = 'Leave Report';
		//$this->data['permissions']['report/leave_report_type'] = 'Out Door Entry Report';
		//$this->data['permissions']['report/leave_credit'] = 'Leave Credit Report';
		$this->data['permissions']['tool/empimport'] = 'Employee Import';
		$this->data['permissions']['tool/externalapi'] = 'Update New Attendance';
		$this->data['permissions']['tool/recalculate'] = 'Recalculate Attendance';
		$this->data['permissions']['tool/backup'] = 'Data Import';
		$this->data['permissions']['tool/backup1'] = 'Backup';
		$this->data['permissions']['report/new_employee'] = 'New Employee';
		
		$this->data['company_datas'] = $this->db->query("SELECT * FROM `oc_company` WHERE 1=1 ")->rows;

		if (isset($this->request->post['permission']['access'])) {
			$this->data['access'] = $this->request->post['permission']['access'];
		} elseif (isset($user_group_info['permission']['access'])) {
			$company_datas = $this->db->query("SELECT * FROM `oc_company` WHERE 1=1 ");
			foreach($company_datas->rows as $ckeys => $cvalues){
				$user_permission_c = $this->db->query("SELECT * FROM `oc_user_group` WHERE `company_id` = '".$cvalues['company_id']."' AND `uid` = '".$this->data['uid']."' ");
				if($user_permission_c->num_rows > 0){
					$permissions = unserialize($user_permission_c->row['permission']);
					$this->data['access'][$cvalues['company_id']] = $permissions['access'];	
				} else {
					$this->data['access'][$cvalues['company_id']] = array();
				}
			}
			//$this->data['access'] = $user_group_info['permission']['access'];
		} else { 
			$company_datas = $this->db->query("SELECT * FROM `oc_company` WHERE 1=1 ");
			foreach($company_datas->rows as $ckeys => $cvalues){
				$this->data['access'][$cvalues['company_id']] = array();
			}
		}

		// echo '<pre>';
		// print_r($this->data['access']);
		// exit;

		if (isset($this->request->post['permission']['modify'])) {
			$this->data['modify'] = $this->request->post['permission']['modify'];
		} elseif (isset($user_group_info['permission']['modify'])) {
			$company_datas = $this->db->query("SELECT * FROM `oc_company` WHERE 1=1 ");
			foreach($company_datas->rows as $ckeys => $cvalues){
				$user_permission_c = $this->db->query("SELECT * FROM `oc_user_group` WHERE `company_id` = '".$cvalues['company_id']."' AND `uid` = '".$this->data['uid']."' ");
				if($user_permission_c->num_rows > 0){
					$permissions = unserialize($user_permission_c->row['permission']);
					$this->data['modify'][$cvalues['company_id']] = $permissions['modify'];	
				} else {
					$this->data['modify'][$cvalues['company_id']] = array();
				}
			}
			//$this->data['modify'] = $user_group_info['permission']['modify'];
		} else { 
			$company_datas = $this->db->query("SELECT * FROM `oc_company` WHERE 1=1 ");
			foreach($company_datas->rows as $ckeys => $cvalues){
				$this->data['modify'][$cvalues['company_id']] = array();
			}
		}

		if (isset($this->request->post['permission']['add'])) {
			$this->data['add'] = $this->request->post['permission']['add'];
		} elseif (isset($user_group_info['permission']['add'])) {
			$company_datas = $this->db->query("SELECT * FROM `oc_company` WHERE 1=1 ");
			foreach($company_datas->rows as $ckeys => $cvalues){
				$user_permission_c = $this->db->query("SELECT * FROM `oc_user_group` WHERE `company_id` = '".$cvalues['company_id']."' AND `uid` = '".$this->data['uid']."' ");
				if($user_permission_c->num_rows > 0){
					$permissions = unserialize($user_permission_c->row['permission']);
					$this->data['add'][$cvalues['company_id']] = $permissions['add'];	
				} else {
					$this->data['add'][$cvalues['company_id']] = array();
				}
			}
			//$this->data['add'] = $user_group_info['permission']['add'];
		} else { 
			$company_datas = $this->db->query("SELECT * FROM `oc_company` WHERE 1=1 ");
			foreach($company_datas->rows as $ckeys => $cvalues){
				$this->data['add'][$cvalues['company_id']] = array();
			}
		}

		if (isset($this->request->post['permission']['delete'])) {
			$this->data['delete'] = $this->request->post['permission']['delete'];
		} elseif (isset($user_group_info['permission']['delete'])) {
			$company_datas = $this->db->query("SELECT * FROM `oc_company` WHERE 1=1 ");
			foreach($company_datas->rows as $ckeys => $cvalues){
				$user_permission_c = $this->db->query("SELECT * FROM `oc_user_group` WHERE `company_id` = '".$cvalues['company_id']."' AND `uid` = '".$this->data['uid']."' ");
				if($user_permission_c->num_rows > 0){
					$permissions = unserialize($user_permission_c->row['permission']);
					$this->data['delete'][$cvalues['company_id']] = $permissions['delete'];	
				} else {
					$this->data['delete'][$cvalues['company_id']] = array();
				}
			}
			//$this->data['delete'] = $user_group_info['permission']['delete'];
		} else { 
			$company_datas = $this->db->query("SELECT * FROM `oc_company` WHERE 1=1 ");
			foreach($company_datas->rows as $ckeys => $cvalues){
				$this->data['delete'][$cvalues['company_id']] = array();
			}
		}

		$this->template = 'user/user_group_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'user/user_permission')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'user/user_permission')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('user/user');

		foreach ($this->request->post['selected'] as $user_group_id) {
			$user_total = $this->model_user_user->getTotalUsersByGroupId($user_group_id);

			if ($user_total) {
				$this->error['warning'] = sprintf($this->language->get('error_user'), $user_total);
			}
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>