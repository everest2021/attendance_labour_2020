<?php  
class ControllerUserUser extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('user/user');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user');

		$this->getList();
	}

	public function insert() {
		$this->language->load('user/user');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_user_user->addUser($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('user/user', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('user/user');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_user_user->editUser($this->request->get['user_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('user/user', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('user/user');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $user_id) {
				$this->model_user_user->deleteUser($user_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('user/user', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		// echo "<pre>";
		// print_r($this->request->get);
		// exit;
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'username';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['user'])) {
			$user = $this->request->get['user'];
			$this->data['user'] = $this->request->get['user'];
		} else {
			$user = '';
			$this->data['user'] = '';
		}

		if (isset($this->request->get['user_id'])) {
			$user_id = $this->request->get['user_id'];
			$this->data['user_id'] = $this->request->get['user_id'];
		} else {
			$user_id = '';
			$this->data['user_id'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('user/user', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['token'] = $this->session->data['token'];

		$this->data['insert'] = $this->url->link('user/user/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('user/user/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['users'] = array();

		$data = array(
			'user_id'  => $user_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$user_total = $this->model_user_user->getTotalUsers($data);

		$results = $this->model_user_user->getUsers($data);

		foreach ($results as $result) {
			$user_id = $this->user->getId();
			$in = 0;
			if($user_id != '1'){
				if($user_id == $result['user_id']){
					$in = 1;
				}
			} else {
				$in = 1;
			}
			if($result['user_id'] != '1' && $in == 1){
				$action = array();

				$action[] = array(
					'text' => $this->language->get('text_edit'),
					'href' => $this->url->link('user/user/update', 'token=' . $this->session->data['token'] . '&user_id=' . $result['user_id'] . $url, 'SSL')
				);

				$this->data['users'][] = array(
					'user_id'    => $result['user_id'],
					'username'   => $result['username'],
					'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'selected'   => isset($this->request->post['selected']) && in_array($result['user_id'], $this->request->post['selected']),
					'action'     => $action
				);
			}
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_username'] = $this->language->get('column_username');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_date_added'] = $this->language->get('column_date_added');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_username'] = $this->url->link('user/user', 'token=' . $this->session->data['token'] . '&sort=username' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('user/user', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		$this->data['sort_date_added'] = $this->url->link('user/user', 'token=' . $this->session->data['token'] . '&sort=date_added' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$user_id = $this->user->getId();
		$in = 0;
		if($user_id == '1'){
			$in = 1;
		}
		$this->data['show'] = $in;

		$pagination = new Pagination();
		$pagination->total = $user_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('user/user', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'user/user_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_username'] = $this->language->get('entry_username');
		$this->data['entry_password'] = $this->language->get('entry_password');
		$this->data['entry_confirm'] = $this->language->get('entry_confirm');
		$this->data['entry_firstname'] = $this->language->get('entry_firstname');
		$this->data['entry_lastname'] = $this->language->get('entry_lastname');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_user_group'] = $this->language->get('entry_user_group');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_captcha'] = $this->language->get('entry_captcha');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['username'])) {
			$this->data['error_username'] = $this->error['username'];
		} else {
			$this->data['error_username'] = '';
		}

		if (isset($this->error['password'])) {
			$this->data['error_password'] = $this->error['password'];
		} else {
			$this->data['error_password'] = '';
		}

		if (isset($this->error['confirm'])) {
			$this->data['error_confirm'] = $this->error['confirm'];
		} else {
			$this->data['error_confirm'] = '';
		}

		if (isset($this->error['firstname'])) {
			$this->data['error_firstname'] = $this->error['firstname'];
		} else {
			$this->data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			$this->data['error_lastname'] = $this->error['lastname'];
		} else {
			$this->data['error_lastname'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('user/user', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['user_id'])) {
			$this->data['action'] = $this->url->link('user/user/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('user/user/update', 'token=' . $this->session->data['token'] . '&user_id=' . $this->request->get['user_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('user/user', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['user_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$user_info = $this->model_user_user->getUser($this->request->get['user_id']);
		}

		if (isset($this->request->post['username'])) {
			$this->data['username'] = $this->request->post['username'];
		} elseif (!empty($user_info)) {
			$this->data['username'] = $user_info['username'];
		} else {
			$this->data['username'] = '';
		}

		if (isset($this->request->post['password'])) {
			$this->data['password'] = $this->request->post['password'];
		} else {
			$this->data['password'] = '';
		}

		if (isset($this->request->post['confirm'])) {
			$this->data['confirm'] = $this->request->post['confirm'];
		} else {
			$this->data['confirm'] = '';
		}

		if (isset($this->request->post['firstname'])) {
			$this->data['firstname'] = $this->request->post['firstname'];
		} elseif (!empty($user_info)) {
			$this->data['firstname'] = $user_info['firstname'];
		} else {
			$this->data['firstname'] = '';
		}

		if (isset($this->request->post['lastname'])) {
			$this->data['lastname'] = $this->request->post['lastname'];
		} elseif (!empty($user_info)) {
			$this->data['lastname'] = $user_info['lastname'];
		} else {
			$this->data['lastname'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (!empty($user_info)) {
			$this->data['email'] = $user_info['email'];
		} else {
			$this->data['email'] = '';
		}

		// if (isset($this->request->post['company_id'])) {
		// 	$this->data['company_id'] = $this->request->post['company_id'];
		// } elseif (!empty($user_info)) {
		// 	$this->data['company_id'] = $user_info['company_id'];
		// } else {
		// 	$this->data['company_id'] = '';
		// }

		if (isset($this->request->post['company_id'])) {
			$this->data['company_id'] = $this->request->post['company_id'];
			$this->data['company_id_string'] = implode(',', $this->request->post['company_id']);
		} elseif (!empty($user_info)) {
			if($user_info['company_id'] != ''){
				$company_id_data = explode(',', $user_info['company_id']);
			} else {
				$company_id_data = array();
			}
			$this->data['company_id'] = $company_id_data;
			$this->data['company_id_string'] = $user_info['company_id'];
		} else {
			$this->data['company_id'] = array();
			$this->data['company_id_string'] = '';
		}

		if (isset($this->request->post['user_group_data'])) {
			$this->data['user_group_data'] = $this->request->post['user_group_data'];
		} elseif (!empty($user_info)) {
			$user_group_data = array();
			if($user_info['add'] == '1'){
				$user_group_data['add'] = 'add';
			}
			if($user_info['edit'] == '1'){
				$user_group_data['edit'] = 'edit';
			}
			if($user_info['view'] == '1'){
				$user_group_data['view'] = 'view';
			}
			$this->data['user_group_data'] = $user_group_data;
		} else {
			$this->data['user_group_data'] = array();
		}

		// echo '<pre>';
		// print_r($this->data['user_group_data']);
		// exit;

		$this->data['user_group_datas'] = array(
			'view' => 'View',
			'add' => 'Add',
			'edit' => 'Edit',
		);

		if (isset($this->request->post['user_group_id'])) {
			$this->data['user_group_id'] = $this->request->post['user_group_id'];
		} elseif (!empty($user_info)) {
			$this->data['user_group_id'] = $user_info['user_group_id'];
		} else {
			$this->data['user_group_id'] = '1';
		}

		$this->load->model('user/user_group');

		$this->data['user_groups'] = $this->model_user_user_group->getUserGroups();
		foreach($this->data['user_groups'] as $ukey => $uvalue){
			if($uvalue['user_group_id'] == '1'){
				unset($this->data['user_groups'][$ukey]);
			}
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($user_info)) {
			$this->data['status'] = $user_info['status'];
		} else {
			$this->data['status'] = 0;
		}

		if (isset($this->request->post['division'])) {
			$this->data['division'] = $this->request->post['division'];
			$this->data['division_string'] = implode(',', $this->request->post['division']);
		} elseif (!empty($user_info)) {
			if($user_info['division'] != ''){
				$division_data = explode(',', $user_info['division']);
			} else {
				$division_data = array();
			}
			$this->data['division'] = $division_data;
			$this->data['division_string'] = $user_info['division'];
		} else {
			$this->data['division'] = array();
			$this->data['division_string'] = '';
		}

		if (isset($this->request->post['region'])) {
			$this->data['region'] = $this->request->post['region'];
			$this->data['region_string'] = implode(',', $this->request->post['region']);
		} elseif (!empty($user_info)) {
			if($user_info['region'] != ''){
				$region_data = explode(',', $user_info['region']);
			} else {
				$region_data = array();
			}
			$this->data['region'] = $region_data;
			$this->data['region_string'] = $user_info['region'];
		} else {
			$this->data['region'] = array();
			$this->data['region_string'] = '';
		}

		if (isset($this->request->post['site'])) {
			$this->data['site'] = $this->request->post['site'];
			$this->data['site_string'] = implode(',', $this->request->post['site']);
		} elseif (!empty($user_info)) {
			if($user_info['site'] != ''){
				$site_data = explode(',', $user_info['site']);
			} else {
				$site_data = array();
			}
			$this->data['site'] = $site_data;
			$this->data['site_string'] = $user_info['site'];
		} else {
			$this->data['site'] = array();
			$this->data['site_string'] = '';
		}

		if (isset($this->request->post['device'])) {
			$this->data['device'] = $this->request->post['device'];
		} elseif (!empty($user_info)) {
			if($user_info['device'] != ''){
				$device_data = explode(',', $user_info['device']);
			} else {
				$device_data = array();
			}
			$this->data['device'] = $device_data;
		} else {
			$this->data['device'] = array();
		}

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_ids' => $this->data['division_string'],
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		//$site_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['sites'] = $site_data;

		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		//$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['regions'] = $region_data;


		$this->load->model('catalog/division');
		$data = array(
			'filter_region_ids' => $this->data['region_string'],
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		//$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['divisions'] = $division_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'Please Select';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_datas'] = $company_data;

		//$this->data['company_datas'] = $this->db->query("SELECT * FROM `oc_company` WHERE 1=1 ")->rows;	

		// echo '<pre>';
		// print_r($this->data['regions']);
		// echo '<pre>';
		// print_r($this->data['divisions']);
		// echo '<pre>';
		// print_r($this->data['sites']);
		// exit;

		// $this->load->model('catalog/division');
		// $this->data['divisions'] = $this->model_catalog_division->getDivisions($data = array());

		// $this->load->model('catalog/region');
		// $this->data['regions'] = $this->model_catalog_region->getRegions($data = array());

		// $this->load->model('catalog/unit');
		// $this->data['sites'] = $this->model_catalog_unit->getUnits($data = array());

		$this->load->model('catalog/device');
		$filter_unit_exp = explode(',', $this->data['site_string']);
		$site_name_string = '';
		foreach($filter_unit_exp as $key => $value){
			$site_names = $this->db->query("SELECT `unit` FROM `oc_unit` WHERE `unit_id` = '".$value."' ");
			if($site_names->num_rows > 0){
				$site_name_string .= $site_names->row['unit'].',';
			}
		}
		$site_name_string = rtrim($site_name_string, ',');
		$data = array(
			'filter_units' => $site_name_string,
		);
		$this->data['devices'] = $this->model_catalog_device->getDevices($data);
		// $this->data['devices'][0]['device_name'] = 'Test Device';
		// $this->data['devices'][0]['device_id'] = '1';

		// $this->data['devices'][1]['device_name'] = 'Test Device 2';
		// $this->data['devices'][1]['device_id'] = '2';

		$this->data['token'] = $this->session->data['token'];

		$user_id = $this->user->getId();
		$in = 0;
		if($user_id == '1'){
			$in = 1;
		}
		$this->data['show'] = $in;
		// echo '<pre>';
		// print_r($this->data['devices']);
		// echo '<pre>';
		// print_r($this->data['sites']);
		// exit;

		$this->template = 'user/user_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'user/user')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['username']) < 3) || (utf8_strlen($this->request->post['username']) > 20)) {
			$this->error['username'] = $this->language->get('error_username');
		}

		$user_info = $this->model_user_user->getUserByUsername($this->request->post['username']);

		if (!isset($this->request->get['user_id'])) {
			if ($user_info) {
				$this->error['warning'] = $this->language->get('error_exists');
			}
		} else {
			if ($user_info && ($this->request->get['user_id'] != $user_info['user_id'])) {
				$this->error['warning'] = $this->language->get('error_exists');
			}
		}

		if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen($this->request->post['firstname']) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen($this->request->post['lastname']) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ($this->request->post['password'] || (!isset($this->request->get['user_id']))) {
			if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
				$this->error['password'] = $this->language->get('error_password');
			}

			if ($this->request->post['password'] != $this->request->post['confirm']) {
				$this->error['confirm'] = $this->language->get('error_confirm');
			}
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'user/user')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['selected'] as $user_id) {
			if ($this->user->getId() == $user_id) {
				$this->error['warning'] = $this->language->get('error_account');
			}
		}

		if (!$this->error) {
			return true;
		} else { 
			return false;
		}
	}

	public function autocomplete() {
		$json = $this->db->query("SELECT * FROM oc_user WHERE username LIKE '%".$this->request->get["user_name"]."%' ")->rows;
		// echo "<pre>";
		// print_r($user_datas);
		// exit;

		$this->response->setOutput(json_encode($json));
	}
}
?>