<?php    
class ControllerTransactionRequestformunit extends Controller { 
	private $error = array();

	public function index() {

		$this->language->load('transaction/requestformdept');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('report/attendance');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');
		$this->load->model('transaction/dayprocess');

		$this->getList();
	}

	public function approve() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('report/attendance');
		$this->load->model('catalog/shift');
		$this->load->model('transaction/dayprocess');
		$this->load->model('catalog/employee');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$can_leave_sql = "UPDATE `oc_requestform` SET `approval_1` = '4' WHERE `id` = '".$id."' ";
				$this->db->query($can_leave_sql);
			}

			$this->session->data['success'] = 'Request Done Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/requestformunit', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['id']) && $this->validateDelete()){
			$id = $this->request->get['id'];
			$can_leave_sql = "UPDATE `oc_requestform` SET `approval_1` = '4' WHERE `id` = '".$id."' ";
			$this->db->query($can_leave_sql);
			$this->session->data['success'] = 'Request Done Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/requestformunit', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$filter_approval_1 = $this->request->get['filter_approval_1'];
		} else {
			$filter_approval_1 = '0';
		}

		if (isset($this->request->get['filter_dept'])) {
			$filter_dept = $this->request->get['filter_dept'];
		} else {
			$filter_dept = null;
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			if($this->user->getId() == 7) {
				$filter_unit = 'Moving';
			} else if($this->user->getId() == 3) {
				$filter_unit = 'Mumbai';
			} else if($this->user->getId() == 4) {
				$filter_unit = 'Pune';
			} else {
				$filter_unit = 'Mumbai';
			}
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			//$filter_date = $this->model_transaction_dayprocess->getNextDate($filter_unit);
			$filter_date = null;//date('Y-m-d');
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_dept'])) {
			$url .= '&filter_dept=' . $this->request->get['filter_dept'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/requestformdept', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$url_insert = '&filter_name_id='.$filter_name_id.'&filter_name='.$filter_name;

		$this->data['approve'] = $this->url->link('transaction/requestformunit/approve', 'token=' . $this->session->data['token'] . $url . $url_insert, 'SSL');
		
		$this->data['requests'] = array();
		$employee_total = 0;

		$data = array(
			'filter_name' => $filter_name,
			'filter_name_id' => $filter_name_id,
			'filter_approval_1' => $filter_approval_1,
			'filter_date' => $filter_date,
			'filter_dept' => $filter_dept,
			'filter_unit' => $filter_unit,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$request_types = array(
			'1' => 'Revert Leave',
			'2' => 'Missing Punch',
			'3' => 'Shift Change',
			'4' => 'Weekly Off Change',
			'5' => 'Holiday Change',
			//'6' => 'On Duty',
		);

		
		$employee_total = $this->model_transaction_transaction->getTotalrequest_unit($data);
		$results = $this->model_transaction_transaction->getrequests_unit($data);
		foreach ($results as $result) {
			$emp_data = $this->model_transaction_transaction->getempdata($result['emp_id']);
			
			$action = array();
			if ($result['approval_1'] == '1') {
				$action[] = array(
					'text' => 'Done',
					'href' => $this->url->link('transaction/requestformunit/approve', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
				);

				if($result['request_type'] == '1'){
					$action[] = array(
						'text' => 'Revert Leave',
						'href' => $this->url->link('transaction/leave', 'token=' . $this->session->data['token'] . '&filter_batch=' . $result['batch_id'] . '&filter_name=' . $emp_data['name'] . '&filter_name_id=' . $result['emp_id'] . '&requestform=1'.$url, 'SSL')
					);
				} elseif($result['request_type'] == '2'){
					$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `date` = '".$result['dot']."' AND `emp_id` = '".$result['emp_id']."' ")->row['transaction_id'];
					$action[] = array(
						'text' => 'Manual Punch',
						'href' => $this->url->link('transaction/manualpunch', 'token=' . $this->session->data['token'] . '&transaction_id=' . $transaction_id . '&requestform=1'.$url, 'SSL')
					);
				}
			}	
			

			if($result['approval_1'] == '1'){
				$approval_1 = 'Approved';
			} elseif($result['approval_1'] == '4') {
				$approval_1 = 'Done';
			}

			if($result['batch_id'] != '0'){
				$leave_from = $this->model_transaction_transaction->getleave_from_ess($result['batch_id']);
				$leave_to = $this->model_transaction_transaction->getleave_to_ess($result['batch_id']);
				$leave_data = $this->model_transaction_transaction->getleave_data($result['batch_id']);
				$leave_data = $leave_data['leave_type'].' '.date('d-m-Y', strtotime($leave_from)).' - '.date('d-m-Y', strtotime($leave_to));
				$result['dot'] = $leave_data;
			} else {
				$leave_data = '';
			}

			$this->data['requests'][] = array(
				'id' => $result['id'],
				'dot' => $result['dot'],
				'doi' => $result['doi'],
				'name' => $emp_data['name'],
				'description' => $result['description'],
				'dept_name' => $result['dept_name'],
				'unit' => $result['unit'],
				'request_type' => $request_types[$result['request_type']],
				'leave_data'        => $leave_data,
				'approval_1' => $approval_1,
				'selected'        => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				'action'          => $action
			);
		}

		// echo '<pre>';
		// print_r($this->data['leaves']);
		// exit;
		$approves = array(
			'0' => 'All',
			'1' => 'Approved',
			'4' => 'Done',
		);

		$this->data['approves'] = $approves;

		if($this->user->getId() == 1) {
			$unit_data = array(
				'Mumbai' => 'Mumbai',
				'Pune' => 'Pune',
				'Moving' => 'Moving' 
			);
		} else if($this->user->getId() == 7) {
			$unit_data = array(
				'Moving' => 'Moving' 
			);
		} else if($this->user->getId() == 3) {
			$unit_data = array(
				'Mumbai' => 'Mumbai' 
			);
		} else if($this->user->getId() == 4) {
			$unit_data = array(
				'Pune' => 'Pune' 
			);
		} else {
			$unit_data = array(
				'Mumbai' => 'Mumbai',
				'Pune' => 'Pune',
				'Moving' => 'Moving' 
			);
		}

		$this->data['unit_data'] = $unit_data;

		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department']] = $dvalue['department'];
		}
		$this->data['dept_data'] = $department_data;

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['button_insert'] = $this->language->get('button_insert');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		// if (isset($this->request->get['page'])) {
		// 	$url .= '&page=' . $this->request->get['page'];
		// }

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_dept'])) {
			$url .= '&filter_dept=' . $this->request->get['filter_dept'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}
		

		$pagination = new Pagination();
		$pagination->total = $employee_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/requestformunit', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_date'] = $filter_date;
		$this->data['filter_approval_1'] = $filter_approval_1;
		$this->data['filter_dept'] = $filter_dept;
		$this->data['filter_unit'] = $filter_unit;
		
		$this->template = 'transaction/requestformunit_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		$this->load->model('transaction/transaction');

		// if (!$this->user->hasPermission('modify', 'transaction/horse_wise')) {
		// 	$this->error['warning'] = $this->language->get('error_permission');
		// }
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete(){
		// $this->load->model('transaction/transaction');
		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $batch_id) {
		// 		$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' AND `p_status` = '1' ";
		// 		$date_res = $this->db->query($leave_sql);
		// 		if ($date_res->num_rows > 0) {
		// 			$this->error['warning'] = 'Leave With batch ' . $batch_id . ' already processed';
		// 		}
		// 	}	
		// } elseif(isset($this->request->get['batch_id'])){
		// 	$batch_id = $this->request->get['batch_id'];
		// 	$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' AND `p_status` = '1' ";
		// 	$date_res = $this->db->query($leave_sql);
		// 	if ($date_res->num_rows > 0) {
		// 		$this->error['warning'] = 'Leave With batch ' . $batch_id . ' already processed';
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
	
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>
