<?php       
class ControllerCommonLogout extends Controller {   
	public function index() { 
		$site = $this->user->getsite();
		$unit_ids = explode(',', $site);
		if(isset($unit_ids[1])){
			$unit_id = $unit_ids[1];
		}else{
			$unit_id = $unit_ids[0];
		}
		if($unit_id != ''){
			$this->db->query("UPDATE `oc_status` SET process_status = 0 WHERE unit_id = '".$unit_id."' ");
		}
		$this->user->logout();

		unset($this->session->data['token']);

		$this->redirect($this->url->link('common/login', '', 'SSL'));
	}
}  
?>
