<?php 
class ControllerToolRecalculate extends Controller { 
	private $error = array();
	public function index() {	
		if(isset($this->request->get['filter_date_start'])){
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = date('Y-m-d');
		}

		if(isset($this->request->get['filter_date_end'])){
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		$this->language->load('tool/recalculate');
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$this->data['entry_restore'] = $this->language->get('entry_restore');
		$this->data['entry_recalculate'] = $this->language->get('entry_recalculate');

		$this->data['button_recalculate'] = $this->language->get('button_recalculate');
		$this->data['button_restore'] = $this->language->get('button_restore');
		$this->data['button_revert'] = $this->language->get('button_revert');

		if (isset($this->session->data['error'])) {
			$this->data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('tool/recalculate', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $division,
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		$site_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['unit_data'] = $site_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;


		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_data'] = $company_data;

		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;

		$this->data['token'] = $this->session->data['token'];
		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['division'] = $division;
		$this->data['region'] = $region;
		$this->data['company'] = explode(',', $company);

		$this->template = 'tool/recalculate.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}
	

	public function recalculate_data() {
		$this->language->load('tool/recalculate');
		
		if(isset($this->request->get['filter_date_start'])){
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '2017-08-26';//date('Y-m-d');
		}

		if(isset($this->request->get['filter_date_end'])){
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '2017-09-25';//date('Y-m-d');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}
		
		/*
		$serverName = "BIOFACE1";
		//$connectionInfo = array("Database"=>"staffdata", "UID"=>"sa", "PWD"=>"1234");
		$connectionInfo = array("Database"=>"staffdata");
		$conn1 = sqlsrv_connect($serverName, $connectionInfo);
		if($conn1) {
			 //echo "Connection established.<br />";
		}else{
			 //echo "Connection could not be established.<br />";
			 $dat['connection'] = 'Connection could not be established';
			 //echo '<pre>';
			 //print_r(sqlsrv_errors());
			 //exit;
			 //die( print_r( sqlsrv_errors(), true));
		}
		*/
		/*
		$tran_datas_week_sql = "SELECT `transaction_id`, `emp_id`, `date`, `shift_intime`, `firsthalf_status`, `secondhalf_status`, `working_time`, `leave_status` FROM `oc_transaction` WHERE `date` >= '".$filter_date_start."' AND `date` <= '".$filter_date_end."' AND `firsthalf_status` = 'WO' AND `secondhalf_status` = 'WO' ";
		if (!empty($filter_name_id)) {
			$tran_datas_week_sql .= " AND `emp_id` = '" . $this->db->escape(strtolower($filter_name_id)) . "'";
		}

		if (!empty($unit)) {
			$tran_datas_week_sql .= " AND `unit_id` = '" . $this->db->escape(strtolower($unit)) . "'";
		}

		if (!empty($department)) {
			$tran_datas_week_sql .= " AND `department_id` = '" . $this->db->escape(strtolower($department)) . "'";
		}

		if (!empty($division)) {
			$tran_datas_week_sql .= " AND `division_id` = '" . $this->db->escape(strtolower($division)) . "'";
		}

		if (!empty($region)) {
			$tran_datas_week_sql .= " AND `region_id` = '" . $this->db->escape(strtolower($region)) . "'";
		}

		if (!empty($company)) {
			$company_string = "'" . str_replace(",", "','", html_entity_decode($company)) . "'";
			$tran_datas_week_sql .= " AND LOWER(`company_id`) IN (" . strtolower($company_string) . ") ";
		}
		$tran_datas_week = $this->db->query($tran_datas_week_sql)->rows;
		// echo '<pre>';
		// print_r($tran_datas_week);
		// exit;
		foreach($tran_datas_week as $tkey => $tvalue){
			if($tvalue['shift_intime'] == '09:30:00'){
				if(strtotime($tvalue['working_time']) >= strtotime('06:30:00')){
					$firsthalf_status = '1';
					$secondhalf_status = '1';
				} elseif(strtotime($tvalue['working_time']) >= strtotime('04:30:00')){
					$firsthalf_status = '1';
					$secondhalf_status = '0';
				} else {
					$firsthalf_status = '0';
					$secondhalf_status = '0';
				}
			} else {
				if(strtotime($tvalue['working_time']) >= strtotime('08:30:00')){
					$firsthalf_status = '1';
					$secondhalf_status = '1';
				} elseif(strtotime($tvalue['working_time']) >= strtotime('06:00:00')){
					$firsthalf_status = '1';
					$secondhalf_status = '0';
				} else {
					$firsthalf_status = '0';
					$secondhalf_status = '0';
				}
			}
			if($firsthalf_status == 1 && $secondhalf_status == 1){
				$present_status = 1;
				$absent_status = 0;
			} elseif($firsthalf_status == 1 && $secondhalf_status == 0){
				$present_status = 0.5;
				$absent_status = 0.5;
			} elseif($firsthalf_status == 0 && $secondhalf_status == 1){
				$present_status = 0.5;
				$absent_status = 0.5;
			} else {
				$present_status = 0;
				$absent_status = 1;
			}
			if($tvalue['leave_status'] == '1'){
				$firsthalf_status = $tvalue['firsthalf_status'];
				$secondhalf_status = $tvalue['secondhalf_status'];
			} elseif($tvalue['leave_status'] == '0.5'){
				$leave_datas = $this->db->query("SELECT * FROM `oc_leave_transaction` WHERE `emp_id` = '".$tvalue['emp_id']."' AND `date` = '".$tvalue['date']."' AND `a_status` = '1' AND `p_status` = '1' ");
				if($leave_datas->num_rows > 0){
					$leave_data = $leave_datas->row;
					if($leave_data['leave_amount'] == '1'){
						$firsthalf_status = $tvalue['firsthalf_status'];
					} elseif($leave_data['leave_amount'] == '2'){
						$secondhalf_status = $tvalue['secondhalf_status'];
					}
				}
			}
			$reset_holi_week_sql = "UPDATE oc_transaction SET `weekly_off` = '0', `firsthalf_status` = '".$firsthalf_status."', `secondhalf_status` = '".$secondhalf_status."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."'  WHERE `transaction_id` = '".$tvalue['transaction_id']."' ";
			// echo $reset_holi_week_sql;
			// echo '<br />';
			// exit;
			$this->db->query($reset_holi_week_sql);
		}
		*/
		
		$employee_datas_sql = "SELECT `emp_code`, `shift_id`, `doj` FROM `oc_employee` WHERE 1=1 ";
		if (!empty($filter_name_id)) {
			$employee_datas_sql .= " AND `emp_code` = '" . $this->db->escape(strtolower($filter_name_id)) . "'";
		}

		if (!empty($unit)) {
			$employee_datas_sql .= " AND `unit_id` = '" . $this->db->escape(($unit)) . "'";
		}

		if (!empty($department)) {
			$employee_datas_sql .= " AND (`department_id`) = '" . $this->db->escape(($department)) . "'";
		}

		if (!empty($division)) {
			$employee_datas_sql .= " AND (`division_id`) = '" . $this->db->escape(($division)) . "'";
		}

		if (!empty($region)) {
			$employee_datas_sql .= " AND (`region_id`) = '" . $this->db->escape(($region)) . "'";
		}

		if (!empty($company)) {
			$company_string = "'" . str_replace(",", "','", html_entity_decode($company)) . "'";
			$employee_datas_sql .= " AND LOWER(`company_id`) IN (" . strtolower($company_string) . ") ";
		}
		//echo $employee_datas_sql;exit;
		$employee_datas = $this->db->query($employee_datas_sql);
		$day = array();
		$days = $this->GetDays($filter_date_start, $filter_date_end);
		foreach ($days as $dkey => $dvalue) {
			$dates = explode('-', $dvalue);
			$day[$dkey]['day'] = ltrim($dates[2], '0');
			$day[$dkey]['month'] = $dates[1];
			$day[$dkey]['year'] = $dates[0];
			$day[$dkey]['date'] = $dvalue;
		}
		if($employee_datas->num_rows > 0){
			// echo '<pre>';
			// print_r($day);
			// exit;
			foreach($employee_datas->rows as $ekey => $evalue){
				$emp_code = $evalue['emp_code'];
				foreach($day as $dkey => $dvalue){
					// echo '<pre>';
					// print_r($dvalue);
					// exit;
					$tran_datas_week_sql = "SELECT `transaction_id`, `emp_id`, `date`, `shift_intime`, `firsthalf_status`, `secondhalf_status`, `working_time`, `leave_status` FROM `oc_transaction` WHERE `date` = '".$dvalue['date']."' AND `emp_id` = '".$emp_code."' AND `weekly_off` = '1' ";
					$tran_datas_week = $this->db->query($tran_datas_week_sql);
					// echo '<pre>';
					// print_r($tran_datas_week);
					// exit;
					if($tran_datas_week->num_rows > 0){
						$tvalue = $tran_datas_week->row;
						if($tvalue['shift_intime'] == '09:30:00'){
							if(strtotime($tvalue['working_time']) >= strtotime('06:30:00')){
								$firsthalf_status = '1';
								$secondhalf_status = '1';
							} elseif(strtotime($tvalue['working_time']) >= strtotime('04:30:00')){
								$firsthalf_status = '1';
								$secondhalf_status = '0';
							} else {
								$firsthalf_status = '0';
								$secondhalf_status = '0';
							}
						} else {
							if(strtotime($tvalue['working_time']) >= strtotime('08:30:00')){
								$firsthalf_status = '1';
								$secondhalf_status = '1';
							} elseif(strtotime($tvalue['working_time']) >= strtotime('06:00:00')){
								$firsthalf_status = '1';
								$secondhalf_status = '0';
							} else {
								$firsthalf_status = '0';
								$secondhalf_status = '0';
							}
						}
						if($firsthalf_status == 1 && $secondhalf_status == 1){
							$present_status = 1;
							$absent_status = 0;
						} elseif($firsthalf_status == 1 && $secondhalf_status == 0){
							$present_status = 0.5;
							$absent_status = 0.5;
						} elseif($firsthalf_status == 0 && $secondhalf_status == 1){
							$present_status = 0.5;
							$absent_status = 0.5;
						} else {
							$present_status = 0;
							$absent_status = 1;
						}
						if($tvalue['leave_status'] == '1'){
							$firsthalf_status = $tvalue['firsthalf_status'];
							$secondhalf_status = $tvalue['secondhalf_status'];
						} elseif($tvalue['leave_status'] == '0.5'){
							$leave_datas = $this->db->query("SELECT * FROM `oc_leave_transaction` WHERE `emp_id` = '".$tvalue['emp_id']."' AND `date` = '".$tvalue['date']."' AND `a_status` = '1' AND `p_status` = '1' ");
							if($leave_datas->num_rows > 0){
								$leave_data = $leave_datas->row;
								if($leave_data['leave_amount'] == '1'){
									$firsthalf_status = $tvalue['firsthalf_status'];
								} elseif($leave_data['leave_amount'] == '2'){
									$secondhalf_status = $tvalue['secondhalf_status'];
								}
							}
						}
						$reset_holi_week_sql = "UPDATE oc_transaction SET `weekly_off` = '0', `firsthalf_status` = '".$firsthalf_status."', `secondhalf_status` = '".$secondhalf_status."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."'  WHERE `transaction_id` = '".$tvalue['transaction_id']."' ";
						// echo $reset_holi_week_sql;
						// echo '<br />';
						// exit;
						$this->db->query($reset_holi_week_sql);
					}	
					//echo 'out';exit;				

					$shift_sql = "SELECT `".$dvalue['day']."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$emp_code."' AND `month` = '".$dvalue['month']."' AND `year` = '".$dvalue['year']."' ";
					//echo $shift_sql;exit;
					$shift_datas = $this->db->query($shift_sql);
					// echo '<pre>';
					// print_r($shift_datas);
					// exit;
					if($shift_datas->num_rows > 0){
						$shift_data = $shift_datas->row[$dvalue['day']];
						$shift_exp = explode('_', $shift_data);
						if($shift_exp[0] == 'W' && strtotime($dvalue['date']) >= strtotime($evalue['doj']) ){
							$tran_datas = $this->db->query("SELECT `transaction_id`, `leave_status`, `date` FROM `oc_transaction` WHERE `emp_id` = '".$emp_code."' AND `date` = '".$dvalue['date']."' ");
							if($tran_datas->num_rows > 0){
								$tran_data = $tran_datas->row;
								if($tran_data['leave_status'] != '1'){
									$update_sql = "UPDATE `oc_transaction` SET `weekly_off` = '1', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `absent_status` = '0' WHERE `transaction_id` = '".$tran_data['transaction_id']."' AND `date` = '".$tran_data['date']."' ";
									//echo $update_sql;
									//echo '<br />';
									//exit;
									$this->db->query($update_sql);
								}
							}
						}
					}	
				}

				$tran_datas_holiday_sql = "SELECT `transaction_id`, `emp_id`, `date`, `shift_intime`, `firsthalf_status`, `secondhalf_status`, `working_time`, `leave_status` FROM `oc_transaction` WHERE `date` >= '".$filter_date_start."' AND `date` <= '".$filter_date_end."' AND `emp_id` = '".$emp_code."' AND ((`firsthalf_status` = 'HLD' AND `secondhalf_status` = 'HLD') OR `holiday_id` = '1') ";
				//echo $tran_datas_holiday_sql;exit;
				$tran_datas_holiday = $this->db->query($tran_datas_holiday_sql)->rows;
				// echo '<pre>';
				// print_r($tran_datas_holiday);
				// exit;
				foreach($tran_datas_holiday as $tkey => $tvalue){
					if($tvalue['shift_intime'] == '09:30:00'){
						if(strtotime($tvalue['working_time']) >= strtotime('06:30:00')){
							$firsthalf_status = '1';
							$secondhalf_status = '1';
						} elseif(strtotime($tvalue['working_time']) >= strtotime('04:30:00')){
							$firsthalf_status = '1';
							$secondhalf_status = '0';
						} else {
							$firsthalf_status = '0';
							$secondhalf_status = '0';
						}
					} else {
						if(strtotime($tvalue['working_time']) >= strtotime('08:30:00')){
							$firsthalf_status = '1';
							$secondhalf_status = '1';
						} elseif(strtotime($tvalue['working_time']) >= strtotime('06:00:00')){
							$firsthalf_status = '1';
							$secondhalf_status = '0';
						} else {
							$firsthalf_status = '0';
							$secondhalf_status = '0';
						}
					}
					if($firsthalf_status == 1 && $secondhalf_status == 1){
						$present_status = 1;
						$absent_status = 0;
					} elseif($firsthalf_status == 1 && $secondhalf_status == 0){
						$present_status = 0.5;
						$absent_status = 0.5;
					} elseif($firsthalf_status == 0 && $secondhalf_status == 1){
						$present_status = 0.5;
						$absent_status = 0.5;
					} else {
						$present_status = 0;
						$absent_status = 1;
					}
					if($tvalue['leave_status'] == '1'){
						$firsthalf_status = $tvalue['firsthalf_status'];
						$secondhalf_status = $tvalue['secondhalf_status'];
					} elseif($tvalue['leave_status'] == '0.5'){
						$leave_datas = $this->db->query("SELECT * FROM `oc_leave_transaction` WHERE `emp_id` = '".$tvalue['emp_id']."' AND `date` = '".$tvalue['date']."' AND `a_status` = '1' AND `p_status` = '1' ");
						if($leave_datas->num_rows > 0){
							$leave_data = $leave_datas->row;
							if($leave_data['leave_amount'] == '1'){
								$firsthalf_status = $tvalue['firsthalf_status'];
							} elseif($leave_data['leave_amount'] == '2'){
								$secondhalf_status = $tvalue['secondhalf_status'];
							}
						}
					}
					$reset_holi_week_sql = "UPDATE oc_transaction SET `holiday_id` = '0', `firsthalf_status` = '".$firsthalf_status."', `secondhalf_status` = '".$secondhalf_status."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."' WHERE `transaction_id` = '".$tvalue['transaction_id']."' ";
					// echo $reset_holi_week_sql;
					// echo '<br />';
					// exit;
					$this->db->query($reset_holi_week_sql);
				}

				$holiday_dates = $this->db->query("SELECT `date` FROM `oc_holiday` WHERE `date` >= '".$filter_date_start."' AND `date` <= '".$filter_date_end."' ");
				if($holiday_dates->num_rows > 0){
					foreach($holiday_dates->rows as $hkey => $hvalue){
						$date = $hvalue['date'];
						if($date < date('Y-m-d')){
							//echo 'aaaa';exit;
							$in = 1;
							$month = date('n', strtotime($date));
							$year = date('Y', strtotime($date));
							$day = date('j', strtotime($date));
							$sql = "SELECT `transaction_id`, `emp_id`, `emp_name` FROM `oc_transaction` WHERE `date` = '".$date."' ";
							if (!empty($filter_name_id)) {
								$sql .= " AND `emp_id` = '" . $this->db->escape(strtolower($filter_name_id)) . "'";
							}

							if (!empty($unit)) {
								$sql .= " AND LOWER(`unit_id`) = '" . $this->db->escape(strtolower($unit)) . "'";
							}

							if (!empty($department)) {
								$sql .= " AND LOWER(`department_id`) = '" . $this->db->escape(strtolower($department)) . "'";
							}

							if (!empty($division)) {
								$sql .= " AND LOWER(`division_id`) = '" . $this->db->escape(strtolower($division)) . "'";
							}

							if (!empty($region)) {
								$sql .= " AND LOWER(`region_id`) = '" . $this->db->escape(strtolower($region)) . "'";
							}

							if (!empty($company)) {
								$company_string = "'" . str_replace(",", "','", html_entity_decode($company)) . "'";
								$sql .= " AND LOWER(`company_id`) IN (" . strtolower($company_string) . ") ";
							}
							$resultss = $this->db->query($sql)->rows;
							// echo '<pre>';
							// print_r($resultss);
							// exit;
							foreach($resultss as $rkeys => $rvalues){
								$shift_sql = "SELECT `".$day."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalues['emp_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
								$shift_datas = $this->db->query($shift_sql);
								if($shift_datas->num_rows > 0){
									$shift_data = $shift_datas->row[$day];
									$shift_exp = explode('_', $shift_data);
									if($shift_exp[0] == 'H' && strtotime($date) >= strtotime($evalue['doj']) ){
										$tran_datas = $this->db->query("SELECT `leave_status`, `firsthalf_status`, `secondhalf_status` FROM `oc_transaction` WHERE `transaction_id` = '".$rvalues['transaction_id']."' ");
										if($tran_datas->num_rows > 0){
											$tran_data = $tran_datas->row;
											if($tran_data['leave_status'] != '1'){
												$update_sql = "UPDATE `oc_transaction` SET `holiday_id` = '1', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `absent_status` = '0' WHERE `transaction_id` = '".$rvalues['transaction_id']."' AND `emp_id` = '".$rvalues['emp_id']."' ";
												//echo $update_sql;
												//echo '<br />';
												//exit;
												$this->db->query($update_sql);
											}
										}
									}
								}	
							}
						}
					}
				}
			}
		}

		/*
		$tran_datas_holiday_sql = "SELECT `transaction_id`, `emp_id`, `date`, `shift_intime`, `firsthalf_status`, `secondhalf_status`, `working_time`, `leave_status` FROM `oc_transaction` WHERE `date` >= '".$filter_date_start."' AND `date` <= '".$filter_date_end."' AND ((`firsthalf_status` = 'HLD' AND `secondhalf_status` = 'HLD') OR `holiday_id` = '1') ";
		if (!empty($filter_name_id)) {
			$tran_datas_holiday_sql .= " AND `emp_id` = '" . $this->db->escape(strtolower($filter_name_id)) . "'";
		}

		if (!empty($unit)) {
			$tran_datas_holiday_sql .= " AND (`unit_id`) = '" . $this->db->escape(($unit)) . "'";
		}

		if (!empty($department)) {
			$tran_datas_holiday_sql .= " AND (`department_id`) = '" . $this->db->escape(($department)) . "'";
		}

		if (!empty($division)) {
			$tran_datas_holiday_sql .= " AND (`division_id`) = '" . $this->db->escape(($division)) . "'";
		}

		if (!empty($region)) {
			$tran_datas_holiday_sql .= " AND (`region_id`) = '" . $this->db->escape(($region)) . "'";
		}

		if (!empty($company)) {
			$company_string = "'" . str_replace(",", "','", html_entity_decode($company)) . "'";
			$tran_datas_holiday_sql .= " AND LOWER(`company_id`) IN (" . strtolower($company_string) . ") ";
		}
		//echo $tran_datas_holiday_sql;exit;
		$tran_datas_holiday = $this->db->query($tran_datas_holiday_sql)->rows;
		// echo '<pre>';
		// print_r($tran_datas_holiday);
		// exit;
		foreach($tran_datas_holiday as $tkey => $tvalue){
			if($tvalue['shift_intime'] == '09:30:00'){
				if(strtotime($tvalue['working_time']) >= strtotime('06:30:00')){
					$firsthalf_status = '1';
					$secondhalf_status = '1';
				} elseif(strtotime($tvalue['working_time']) >= strtotime('04:30:00')){
					$firsthalf_status = '1';
					$secondhalf_status = '0';
				} else {
					$firsthalf_status = '0';
					$secondhalf_status = '0';
				}
			} else {
				if(strtotime($tvalue['working_time']) >= strtotime('08:30:00')){
					$firsthalf_status = '1';
					$secondhalf_status = '1';
				} elseif(strtotime($tvalue['working_time']) >= strtotime('06:00:00')){
					$firsthalf_status = '1';
					$secondhalf_status = '0';
				} else {
					$firsthalf_status = '0';
					$secondhalf_status = '0';
				}
			}
			if($firsthalf_status == 1 && $secondhalf_status == 1){
				$present_status = 1;
				$absent_status = 0;
			} elseif($firsthalf_status == 1 && $secondhalf_status == 0){
				$present_status = 0.5;
				$absent_status = 0.5;
			} elseif($firsthalf_status == 0 && $secondhalf_status == 1){
				$present_status = 0.5;
				$absent_status = 0.5;
			} else {
				$present_status = 0;
				$absent_status = 1;
			}
			if($tvalue['leave_status'] == '1'){
				$firsthalf_status = $tvalue['firsthalf_status'];
				$secondhalf_status = $tvalue['secondhalf_status'];
			} elseif($tvalue['leave_status'] == '0.5'){
				$leave_datas = $this->db->query("SELECT * FROM `oc_leave_transaction` WHERE `emp_id` = '".$tvalue['emp_id']."' AND `date` = '".$tvalue['date']."' AND `a_status` = '1' AND `p_status` = '1' ");
				if($leave_datas->num_rows > 0){
					$leave_data = $leave_datas->row;
					if($leave_data['leave_amount'] == '1'){
						$firsthalf_status = $tvalue['firsthalf_status'];
					} elseif($leave_data['leave_amount'] == '2'){
						$secondhalf_status = $tvalue['secondhalf_status'];
					}
				}
			}
			$reset_holi_week_sql = "UPDATE oc_transaction SET `holiday_id` = '0', `firsthalf_status` = '".$firsthalf_status."', `secondhalf_status` = '".$secondhalf_status."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."' WHERE `transaction_id` = '".$tvalue['transaction_id']."' ";
			// echo $reset_holi_week_sql;
			// echo '<br />';
			// exit;
			$this->db->query($reset_holi_week_sql);
		}
		
		
		$holiday_dates = $this->db->query("SELECT `date` FROM `oc_holiday` WHERE `date` >= '".$filter_date_start."' AND `date` <= '".$filter_date_end."' ");
		if($holiday_dates->num_rows > 0){
			foreach($holiday_dates->rows as $hkey => $hvalue){
				$date = $hvalue['date'];
				if($date < date('Y-m-d')){
					//echo 'aaaa';exit;
					$in = 1;
					$month = date('n', strtotime($date));
					$year = date('Y', strtotime($date));
					$day = date('j', strtotime($date));
					$sql = "SELECT `transaction_id`, `emp_id`, `emp_name` FROM `oc_transaction` WHERE `date` = '".$date."' ";
					if (!empty($filter_name_id)) {
						$sql .= " AND `emp_id` = '" . $this->db->escape(strtolower($filter_name_id)) . "'";
					}

					if (!empty($unit)) {
						$sql .= " AND LOWER(`unit_id`) = '" . $this->db->escape(strtolower($unit)) . "'";
					}

					if (!empty($department)) {
						$sql .= " AND LOWER(`department_id`) = '" . $this->db->escape(strtolower($department)) . "'";
					}

					if (!empty($division)) {
						$sql .= " AND LOWER(`division_id`) = '" . $this->db->escape(strtolower($division)) . "'";
					}

					if (!empty($region)) {
						$sql .= " AND LOWER(`region_id`) = '" . $this->db->escape(strtolower($region)) . "'";
					}

					if (!empty($company)) {
						$company_string = "'" . str_replace(",", "','", html_entity_decode($company)) . "'";
						$sql .= " AND LOWER(`company_id`) IN (" . strtolower($company_string) . ") ";
					}
					$resultss = $this->db->query($sql)->rows;
					// echo '<pre>';
					// print_r($resultss);
					// exit;
					foreach($resultss as $rkeys => $rvalues){
						$shift_sql = "SELECT `".$day."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalues['emp_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
						$shift_datas = $this->db->query($shift_sql);
						if($shift_datas->num_rows > 0){
							$shift_data = $shift_datas->row[$day];
							$shift_exp = explode('_', $shift_data);
							if($shift_exp[0] == 'H'){
								$tran_datas = $this->db->query("SELECT `leave_status`, `firsthalf_status`, `secondhalf_status` FROM `oc_transaction` WHERE `transaction_id` = '".$rvalues['transaction_id']."' ");
								if($tran_datas->num_rows > 0){
									$tran_data = $tran_datas->row;
									if($tran_data['leave_status'] != '1'){
										$update_sql = "UPDATE `oc_transaction` SET `holiday_id` = '1', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `absent_status` = '0' WHERE `transaction_id` = '".$rvalues['transaction_id']."' AND `emp_id` = '".$rvalues['emp_id']."' ";
										//echo $update_sql;
										//echo '<br />';
										//exit;
										$this->db->query($update_sql);
									}
								}
							}
						}	
					}
				}
			}
		}
		*/
		
		//echo 'out';exit;
		//echo 'out';exit;
		/*
		if($current_processed_data){
			$final_datas = array();
			$done_transaction_id = array();
			foreach($current_processed_data as $ckey => $cvalue){
				$transaction_ids = $this->db->query("SELECT `transaction_id` FROM `oc_attendance` WHERE `id` = '".$cvalue."' ");
				if($transaction_ids->num_rows > 0){
					$transaction_id = $transaction_ids->row['transaction_id'];
					if(!isset($done_transaction_id[$transaction_id])){
						$done_transaction_id[$transaction_id] = $transaction_id;
						$transaction_datass = $this->db->query("SELECT * FROM `oc_transaction` WHERE `transaction_id` = '".$transaction_id."' ");
						if($transaction_datass->num_rows > 0){
							$transaction_datas = $transaction_datass->rows;
							foreach($transaction_datas as $tkey => $tvalue){
								$emp_data = $this->getEmployees_dat($tvalue['emp_id']);
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['EmployeeId'] = $tvalue['emp_id'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['EmployeeName'] = $tvalue['emp_name'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['PunchID'] = $tvalue['emp_id'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['Department'] = $tvalue['department'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['Site'] = $emp_data['unit'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['Region'] = $emp_data['region'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['Division'] = $emp_data['division'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['AttendDate'] = date('Y-m-d', strtotime($tvalue['date']));
								
								$date_to_compare = $tvalue['date'];
								//if(strtotime($emp_data['doj']) > strtotime($date_to_compare)){
									//$status = 'X';
								//} else
								if($tvalue['leave_status'] != '0'){
									if($tvalue['leave_status'] == '0.5'){
										$status = 'PL';
									} else {
										$status = 'PL';
									}
								} elseif($tvalue['weekly_off'] != '0'){
									$working_times = explode(':', $tvalue['working_time']);
									$working_hours = $working_times[0];
									if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
									//if($working_hours >= '06'){
										$status = 'PW';
									} else {
										$status = 'W';	
									}
								} elseif($tvalue['holiday_id'] != '0'){
									$working_times = explode(':', $tvalue['working_time']);
									$working_hours = $working_times[0];
									if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
									//if($working_hours >= '06'){
										$status = 'PPH';
									} else {
										$status = 'H';
									}
								} elseif($tvalue['present_status'] == '1'){
									$status = 'P';
								} elseif($tvalue['absent_status'] == '1'){
									$status = 'A';
								} elseif($tvalue['present_status'] == '0.5' || $tvalue['absent_status'] == '0.5'){
									$status = 'HD';
								}

								$final_datas[$tvalue['date']][$tvalue['emp_id']]['Status'] = $status;
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['InTime'] = $tvalue['act_intime'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['OutTime'] = $tvalue['act_outtime'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['TotaHour'] = $tvalue['working_time'];
							}
						}
					}
				}
			}
			// echo '<pre>';
			// print_r($final_datas);
			// exit;
			foreach($final_datas as $fkeys => $fvalues){
				foreach($fvalues as $fkey => $fvalue){
					$sql = "SELECT * FROM [dbo].[Transaction_JMC] WHERE EmployeeId = '".$fvalue['EmployeeId']."' AND AttendDate = '".$fvalue['AttendDate']."' ";
					$params = array();
					$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
					$stmt = sqlsrv_query($conn1, $sql, $params, $options);
					if( $stmt === false) {
						// echo 'Not Executed Start';
						// echo '<br />';
						// echo "SELECT * FROM [dbo].[Transaction_JMC] WHERE EmployeeId = '".$fvalue['EmployeeId']."' AND AttendDate = '".$fvalue['AttendDate']."' ";
						// echo '<br />';
						// echo '<pre>';
						// print_r(sqlsrv_errors());
						// echo '<br />';
						// echo 'Not Executed End';
						// echo '<br />';
					}
					$is_exist = 0;
					$id = 0;
					while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
						$is_exist = 1;
						$id = $row['Id'];
					}
					
					$current_date = date('Y-m-d H:i:s');
					if($is_exist == 1){
						$update_sql = "UPDATE [dbo].[Transaction_JMC] SET Status = '".$fvalue['Status']."', InTime = '".$fvalue['InTime']."', OutTime = '".$fvalue['OutTime']."', TotaHour = '".$fvalue['TotaHour']."', JmcDate = '".$current_date."', ProcessStatus = '0' WHERE Id = '".$id."' ";
						$stmt = sqlsrv_query($conn1, $update_sql);
						if($stmt == false){
							//echo 'Not Executed Start';
							//echo '<br />';
							//echo "UPDATE [dbo].[Transaction_JMC] SET Status = '".$fvalue['Status']."', InTime = '".$fvalue['InTime']."', OutTime = '".$fvalue['OutTime']."', TotaHour = '".$fvalue['TotaHour']."', JmcDate = '0000-00-00 00:00:00' WHERE Id = '".$id."' ";
							//echo '<br />';
							//echo '<pre>';
							//print_r(sqlsrv_errors());
							//echo '<br />';
							//echo 'Not Executed End';
							//echo '<br />';
						}
					} else {
						$insert_sql = "INSERT INTO [dbo].[Transaction_JMC] (EmployeeId, EmployeeName, PunchID, Department, Site, Region, Division, AttendDate, Status, InTime, OutTime, TotaHour, JmcDate, ProcessStatus) VALUES ('".$fvalue['EmployeeId']."', '".$fvalue['EmployeeName']."', '".$fvalue['PunchID']."', '".$fvalue['Department']."', '".$fvalue['Site']."', '".$fvalue['Region']."', '".$fvalue['Division']."', '".$fvalue['AttendDate']."', '".$fvalue['Status']."', '".$fvalue['InTime']."', '".$fvalue['OutTime']."', '".$fvalue['TotaHour']."', '".$current_date."', '0') ";
						$stmt = sqlsrv_query($conn1, $insert_sql);
						if($stmt == false){
							// echo 'Not Executed Start';
							// echo '<br />';
							// echo "INSERT INTO [dbo].[Transaction_JMC] (EmployeeId, EmployeeName, PunchID, Department, Site, Region, Division, AttendDate, Status, InTime, OutTime, TotaHour, JmcDate, ProcessStatus) VALUES ('".$fvalue['EmployeeId']."', '".$fvalue['EmployeeName']."', '".$fvalue['PunchID']."', '".$fvalue['Department']."', '".$fvalue['Site']."', '".$fvalue['Region']."', '".$fvalue['Division']."', '".$fvalue['AttendDate']."', '".$fvalue['Status']."', '".$fvalue['InTime']."', '".$fvalue['OutTime']."', '".$fvalue['TotaHour']."', null, '0') ";
							// echo '<br />';
							// echo '<pre>';
							// print_r(sqlsrv_errors());
							// echo '<br />';
							// echo 'Not Executed End';
							// echo '<br />';
						}
					}
				}	
			}
		}
		*/
		$this->session->data['success'] = 'You are done with process';
		$this->redirect($this->url->link('tool/recalculate', 'token=' . $this->session->data['token'], 'SSL'));
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}
}
?>