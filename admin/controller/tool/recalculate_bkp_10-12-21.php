<?php 
class ControllerToolRecalculate extends Controller { 
	private $error = array();
	public function index() {	
		if(isset($this->request->get['filter_date_start'])){
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = date('Y-m-01');
		}

		if(isset($this->request->get['filter_date_end'])){
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = 0;
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['filter_division'])) {
			$filter_division = html_entity_decode($this->request->get['filter_division']);
		} else {
			$filter_division = 0;
		}

		if (isset($this->request->get['filter_region'])) {
			$filter_region = html_entity_decode($this->request->get['filter_region']);
		} else {
			$filter_region = 0;
		}

		if (isset($this->request->get['filter_company'])) {
			$filter_company = html_entity_decode($this->request->get['filter_company']);
		} else {
			$filter_company = 0;
		}

		if (isset($this->request->get['filter_contractor'])) {
			$filter_contractor = html_entity_decode($this->request->get['filter_contractor']);
		} else {
			$filter_contractor = 0;
		}

		$this->language->load('tool/recalculate');
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$this->data['entry_restore'] = $this->language->get('entry_restore');
		$this->data['entry_recalculate'] = $this->language->get('entry_recalculate');

		$this->data['button_recalculate'] = $this->language->get('button_recalculate');
		$this->data['button_restore'] = $this->language->get('button_restore');
		$this->data['button_revert'] = $this->language->get('button_revert');

		if (isset($this->session->data['error'])) {
			$this->data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('tool/recalculate', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('catalog/contractor');
		$contractor_datas = $this->model_catalog_contractor->getContractor_tots();
		$contractor_data = array();
		foreach ($contractor_datas as $dkey => $dvalue) {
			$contractor_data[$dvalue['contractor_id']] = $dvalue['contractor_name'].'-'.$dvalue['contractor_code'];
		}
		$this->data['contractor_data'] = $contractor_data;

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $filter_division,
			'filter_contractor_id' => $filter_contractor,
		);
		$site_datas = $this->model_catalog_unit->getUnits_by_contractor($data);
		$site_data = array();
		if($this->user->getId() == '1'){
			$site_data['0'] = 'All';
		}
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['unit_data'] = $site_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;


		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_data'] = $company_data;

		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $filter_region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;

		$this->data['token'] = $this->session->data['token'];
		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_department'] = $filter_department;
		$this->data['filter_division'] = $filter_division;
		$this->data['filter_region'] = $filter_region;
		$this->data['filter_company'] = explode(',', $filter_company);
		$this->data['filter_contractor'] = explode(',', $filter_contractor);

		$this->template = 'tool/recalculate.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}
	

	public function recalculate_data() {
		//echo 'aaaaa';exit;
		$this->language->load('tool/recalculate');
		
		if(isset($this->request->get['filter_date_start'])){
			$filter_date_start = $this->request->get['filter_date_start'];
			$filter_date_start_constant = $filter_date_start;
		} else {
			$filter_date_start = date('Y-m-01');//date('Y-m-d');
			$filter_date_start_constant = $filter_date_start;
		}

		if(isset($this->request->get['filter_date_end'])){
			$filter_date_end = $this->request->get['filter_date_end'];
			$filter_date_end_constant = $filter_date_end;
		} else {
			$filter_date_end = date('Y-m-d');//date('Y-m-d');
			$filter_date_end_constant = $filter_date_end;
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = 0;
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['filter_division'])) {
			$filter_division = html_entity_decode($this->request->get['filter_division']);
		} else {
			$filter_division = 0;
		}

		if (isset($this->request->get['filter_region'])) {
			$filter_region = html_entity_decode($this->request->get['filter_region']);
		} else {
			$filter_region = 0;
		}

		if (isset($this->request->get['filter_company'])) {
			$filter_company = html_entity_decode($this->request->get['filter_company']);
		} else {
			$filter_company = 0;
		}

		if (isset($this->request->get['filter_contractor'])) {
			$filter_contractor = html_entity_decode($this->request->get['filter_contractor']);
		} else {
			$filter_contractor = 0;
		}
		
	
		$serverName = "10.200.1.35";
		$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"avi", "PWD"=>"avi2123");
		//$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"avi", "PWD"=>"avi2123");
		//$connectionInfo = array("Database"=>"SmartOffice");
		$conn1 = sqlsrv_connect($serverName, $connectionInfo);
		if($conn1) {
			//echo "Connection established.<br />";exit;
		} else {
			// echo "Connection could not be established.<br />";
			// echo "<br />";
			$dat['connection'] = 'Connection could not be established';
			// echo '<pre>';
			// print_r(sqlsrv_errors());
			// exit;
			//die( print_r( sqlsrv_errors(), true));
		}

		$sql = "SELECT * FROM etimetracklite1.dbo.Staffdevicelogsnew WHERE CAST(LogDate as DATE) >= '".$filter_date_start."' AND CAST(LogDate as DATE) <= '".$filter_date_end."' ";
		if($filter_name_id){
			$sql .= " AND UserId = '".$filter_name_id."' ";
		}
		$sql .= " ORDER BY DeviceLogId ";
		//echo $sql;exit;
		$params = array();
		$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt = sqlsrv_query($conn1, $sql, $params, $options);
		if( $stmt === false) {
		   //echo'<pre>';
		   //print_r(sqlsrv_errors());
		   //exit;
		}
		$exp_datas = array();
		while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			$user_id = $row['UserId'];
			$device_id_check = $row['DeviceId'];
			if($device_id_check != '' || $device_id_check != null){
				$device_id = $row['DeviceId'];
			} else {
				$device_id = 0;
			}
			// $user_id = sprintf('%04d', $row['UserId']);
			// $device_id = sprintf('%02d', $row['DeviceId']);
			$log_id = $row['DeviceLogId'];
			$logdates = $row['LogDate'];

			if($logdates != null && strlen($user_id) <= 12){	
				$logdate = $logdates->format("Y-m-d");
				$logyear = $logdates->format("Y");
				$logtimes = $row['LogDate'];
				$logtime = $logtimes->format("H:i:s");
				if($logyear >= '2018' && $logtime != '00:00:00'){
					$download_dates = $row['DownloadDate'];
					//$download_date = $download_dates->format("Y-m-d");
					if($download_dates != '' || $download_dates != null){
						$download_date = $download_dates->format("Y-m-d");
					} else {
						$download_date = '0000-00-00';
					}
					$exp_datas[] = array(
						'user_id' => $user_id,
						'log_id' => $log_id,
						'download_date' => $download_date,
						'date' => $logdate,
						'time' => $logtime,
						'device_id' => $device_id,
					);
				}
				//break;
			}
		}

		// echo '<pre>';
		// print_r($exp_datas);
		// exit;

		$employees = array();
		$employees_final = array();
		foreach($exp_datas as $data) {
			$emp_id  = $data['user_id'];//substr($tdata, 0, 5);
			$in_time = $data['time'];//substr($tdata, 6, 5);
			if($emp_id != '') {
				$result = $this->getEmployees_dat($emp_id);
				if(isset($result['emp_code'])){
					$employees[] = array(
						'employee_id' => $result['emp_code'],
						'emp_name' => $result['name'],
						'card_id' => $result['emp_code'],
						'department' => $result['department'],
						'unit' => $result['unit'],
						'unit_id' => $result['unit_id'],
						'group' => '',//$result['group'],
						'in_time' => $in_time,
						'device_id' => $data['device_id'],
						'punch_date' => $data['date'],
						'download_date' => $data['download_date'],
						'log_id' => $data['log_id'],
						'fdate' => date('Y-m-d H:i:s', strtotime($data['date'].' '.$in_time))
					);
				} else {
					
					$employees[] = array(
						'employee_id' => $emp_id,
						'emp_name' => '',
						'card_id' => $emp_id,
						'department' => '',
						'unit' => '',
						'unit_id' => 0,
						'group' => '',//$result['group'],
						'in_time' => $in_time,
						'device_id' => $data['device_id'],
						'punch_date' => $data['date'],
						'download_date' => $data['download_date'],
						'log_id' => $data['log_id'],
						'fdate' => date('Y-m-d H:i:s', strtotime($data['date'].' '.$in_time))
					);
					
				}
			}
		}
		usort($employees, array($this, "sortByOrder"));
		$o_emp = array();
		foreach ($employees as $ekey => $evalue) {
			$employees_final[] = $evalue;
		}

		//$filter_unit = 0;
		if(isset($employees_final) && count($employees_final) > 0){
			foreach ($employees_final as $fkey => $employee) {
				$exist = $this->getorderhistory($employee);
				if($exist == 0){
					$mystring = $employee['in_time'];
					$findme   = ':';
					$pos = strpos($mystring, $findme);
					if($pos !== false){
						$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 3, 2). ":" . substr($employee['in_time'], 6, 2);
					} else {
						$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 2, 2). ":" . substr($employee['in_time'], 4, 2);
					}
					$employee['in_time'] = $in_times;
					$this->insert_attendance_data($employee);
				}
			}
		}
		
		
		//$is_month_closed = $this->db->query("SELECT `date` FROM `oc_transaction` WHERE `date` = '".$filter_date_start."' AND `month_close_status` = '1' LIMIT 1");
		// echo '<pre>';
		// print_r($is_month_closed);
		// exit;
		//if($is_month_closed->num_rows == 0){
			$stats_sql = "SELECT * FROM `oc_status` WHERE 1=1 ";
			if(!empty($filter_unit)){
				$stats_sql .= " AND `unit_id` = '".$filter_unit."' ";
			}
			if(!empty($filter_division)){
				$stats_sql .= " AND `division_id` = '".$filter_division."' ";
			}
			$stats_data = $this->db->query($stats_sql);
			if($stats_data->num_rows == 0){
				$sql = "INSERT INTO `oc_status` SET `process_status` = '1' ";
				if(!empty($filter_unit)){
					$sql .= " , `unit_id` = '".$filter_unit."' ";
				} else {
					$sql .= " , `unit_id` = '0' ";
				}
				if(!empty($filter_division)){
					$sql .= " , `division_id` = '".$filter_division."' ";
				} else {
					$sql .= " , `division_id` = '0' ";
				}
				$this->db->query($sql);
				$employee_datas_sql = "SELECT `name`, `emp_code`, `contractor`, `contractor_code`, `contractor_id`, `company`, `company_id`, `region`, `region_id`, `division`, `division_id`, `department`, `department_id`, `unit`, `unit_id`, `category`, `category_id`, `designation_id`, `designation` FROM `oc_employee` WHERE 1=1 AND (`dol` = '0000-00-00' OR `dol` > '".$filter_date_start."')";
				if (!empty($filter_name_id)) {
					$employee_datas_sql .= " AND `emp_code` = '" . $this->db->escape(strtolower($filter_name_id)) . "'";
				}

				if (!empty($filter_company)) {
					$company_string = str_replace("multiselect-all,", "", html_entity_decode($filter_company));
					$company_string = "'" . str_replace(",", "','", $company_string) . "'";
					$employee_datas_sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
				}

				if (!empty($filter_contractor)) {
					$contractor_string = str_replace("multiselect-all,", "", html_entity_decode($filter_contractor));
					$contractor_string = "'" . str_replace(",", "','", $contractor_string) . "'";
					$employee_datas_sql .= " AND `contractor_id` IN (" . strtolower($contractor_string) . ") ";
				}

				if (!empty($filter_region)) {
					//$employee_datas_sql .= " AND `region_id` = '" . $this->db->escape(strtolower($filter_region)) . "'";
				}

				if (!empty($filter_division)) {
					$employee_datas_sql .= " AND `division_id` = '" . $this->db->escape(strtolower($filter_division)) . "'";
				}

				if (!empty($filter_department)) {
					$employee_datas_sql .= " AND `department_id` = '" . $this->db->escape(strtolower($filter_department)) . "'";
				}

				if (!empty($filter_unit)) {
					$employee_datas_sql .= " AND `unit_id` = '" . $this->db->escape(strtolower($filter_unit)) . "'";
				}

				$employee_datas_sql .= " ORDER BY `emp_code`";
				//$employee_datas_sql .= " ORDER BY `emp_code` LIMIT 0, 100";
				//echo $employee_datas_sql;exit;
				$employee_datas = $this->db->query($employee_datas_sql);
				
				if($employee_datas->num_rows > 0){
					$days = array();
					$dayss = $this->GetDays($filter_date_start_constant, $filter_date_end_constant);
					foreach ($dayss as $dkey => $dvalue) {
						if(strtotime($dvalue) < strtotime(date('Y-m-d'))){
							$dates = explode('-', $dvalue);
							$days[$dkey]['day'] = ltrim($dates[2], '0');
							$days[$dkey]['month'] = $dates[1];
							$days[$dkey]['year'] = $dates[0];
							$days[$dkey]['date'] = $dvalue;
							$exceed = 0;
						} else {
							$exceed = 1;
						}
					}
					
					if (!empty($filter_name_id)) {
						// $this->log->write('Recalculate Start For Emp Code ' . $filter_name_id . ' And Date ' . $filter_date_start_constant . ' - ' . $filter_date_end_constant);
					} else {
						// $this->log->write('Recalculate Start For Date ' . $filter_date_start_constant . ' - ' . $filter_date_end_constant);
					}
					$cnt1 = 0;
					$cnt = 0;
					$date_string = '';
					$employee_string = '';
					$punch_date_array = array();
					foreach($employee_datas->rows as $rkey => $rvalue){
						$employee_string .= $rvalue['emp_code'].', ';
					}
					$employee_string = rtrim($employee_string, ', ');
					$date_string = rtrim($date_string, ', ');
					$date_string = "'" . str_replace(",", "','", $date_string) . "'";
					//$reset_attendance_sql = "UPDATE `oc_attendance` SET `status` = '0', `transaction_id` = '0' WHERE `emp_id` IN (" . $employee_string . ") AND `punch_date` >= '".$filter_date_start_constant."' AND `punch_date` <= '".$filter_date_end_constant."' ";
					$reset_attendance_sql = "UPDATE `oc_attendance` SET `status` = '0', `transaction_id` = '0' WHERE `emp_id` IN (" . $employee_string . ") AND ((`punch_date` >= '".$filter_date_start_constant."' AND `punch_time` >= '05:00:00') OR (`punch_date` <= '".$filter_date_end_constant."' AND `punch_time` <= '04:00:00'))";
					if($filter_unit){
						$reset_attendance_sql .= " AND `unit_id` = '".$filter_unit."' ";
					}
					//echo $reset_attendance_sql;exit;
					$this->log->write("update_status_zero");
					$this->log->write($reset_attendance_sql);
					$this->log->write("update_status_zero");
					$this->db->query($reset_attendance_sql);

					//$reset_transaction = "UPDATE `oc_transaction` SET `act_intime` = '00:00:00', `act_outtime` = '00:00:00', `date_out` = '0000-00-00', `late_time` = '00:00:00', `early_time` = '00:00:00', `late_mark` = '0', `early_mark` = '0', `working_time` = '00:00:00', `leave_status` = '0', `absent_status` = '1', `present_status` = '0', `abnormal_status` = '0', `batch_id` = '0', `weekly_off` = '0', `holiday_id` = '0', `halfday_status` = '0', `day_close_status` = '0', `firsthalf_status` = '0', `secondhalf_status` = '0' WHERE `date` >= '".$filter_date_start_constant."' AND `date` <= '".$filter_date_end_constant."' AND `emp_id` IN (" . strtolower($employee_string) . ") AND (`leave_status` = '0' AND `manual_status` = '0')";
					$reset_transaction = "UPDATE `oc_transaction` SET `act_intime` = '00:00:00', `act_outtime` = '00:00:00', `date_out` = '0000-00-00', `late_time` = '00:00:00', `early_time` = '00:00:00', `late_mark` = '0', `early_mark` = '0', `working_time` = '00:00:00', `leave_status` = '0', `absent_status` = '1', `present_status` = '0', `abnormal_status` = '0', `batch_id` = '0', `weekly_off` = '0', `holiday_id` = '0', `halfday_status` = '0', `day_close_status` = '0', `firsthalf_status` = '0', `secondhalf_status` = '0' WHERE `date` >= '".$filter_date_start_constant."' AND `date` <= '".$filter_date_end_constant."' AND `emp_id` IN (" . strtolower($employee_string) . ") AND (`manual_status` = '0')";
					if($filter_unit){
						$reset_transaction .= " AND `unit_id` = '".$filter_unit."' ";
					}
					// $this->log->write($reset_transaction);
					$this->db->query($reset_transaction);
					//echo 'out';exit;
					
					$batch_id = 0;
					$current_processed_data = array();
					$current_processed_data_date = array();
					foreach($employee_datas->rows as $rkey => $rvalue){
						$current_processed_data_date = array();
						$emp_code = $rvalue['emp_code'];
						$unprocessed_dates = $this->db->query("SELECT  a.`id`, a.`emp_id`, a.`punch_date`, a.`punch_time`, a.`device_id` FROM `oc_attendance` a LEFT JOIN `oc_employee` e ON(e.`emp_code` = a.`emp_id`) WHERE (e.`dol` = '0000-00-00' OR e.`dol` > '".$filter_date_start_constant."') AND a.`status` = '0' AND a.`punch_date` >= '".$filter_date_start_constant."' AND  a.`punch_date` <= '".$filter_date_end_constant."' AND a.`emp_id` = '".$rvalue['emp_code']."' ORDER BY a.`punch_date` ASC, a.`punch_time` ASC ");
						//echo "<pre>";print_r($unprocessed_dates->rows);exit;

						$this->log->write("start_here");
						$this->log->write("SELECT  a.`id`, a.`emp_id`, a.`punch_date`, a.`punch_time`, a.`device_id` FROM `oc_attendance` a LEFT JOIN `oc_employee` e ON(e.`emp_code` = a.`emp_id`) WHERE (e.`dol` = '0000-00-00' OR e.`dol` > '".$filter_date_start_constant."') AND a.`status` = '0' AND a.`punch_date` >= '".$filter_date_start_constant."' AND  a.`punch_date` <= '".$filter_date_end_constant."' AND a.`emp_id` = '".$rvalue['emp_code']."' ORDER BY a.`punch_date` ASC, a.`punch_time` ASC");
						foreach($unprocessed_dates->rows as $dkey => $dvalue){
							$filter_date_start = $dvalue['punch_date'];
							$current_processed_data[$dvalue['id']] = $dvalue['id'];
							$current_processed_data_date[$dvalue['id']] = $filter_date_start;
							$data['filter_name_id'] = $dvalue['emp_id']; 
							$rvalue = $this->getempdata($dvalue['emp_id'], $dvalue['punch_date']);
							$device_id = $dvalue['device_id'];
							$is_processed_status = $this->db->query("SELECT `status` FROM `oc_attendance` WHERE `id` = '".$dvalue['id']."' ")->row['status'];
							if(isset($rvalue['name']) && $rvalue['name'] != '' && $is_processed_status == '0'){
								// $this->log->write('---start----');

								$emp_name = $rvalue['name'];
								$department = $rvalue['department'];
								$unit = $rvalue['unit'];
								$group = '';

								$day_date = date('j', strtotime($filter_date_start));
								$day = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));

								// $this->log->write($filter_date_start);
								// $this->log->write($dvalue['punch_time']);

								$punch_time_exp = explode(':', $dvalue['punch_time']);
								if($punch_time_exp[0] >= '00' && $punch_time_exp[0] <= '04'){
									$filter_date_start = date('Y-m-d', strtotime($filter_date_start .' -1 day'));
									$day_date = date('j', strtotime($filter_date_start));
									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));				
								}

								// $this->log->write($filter_date_start);
								// $this->log->write($day);
								// $this->log->write($month);
								// $this->log->write($year);

								//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
								//$shift_schedule = $this->db->query($update3)->row;
								//if(!isset($shift_schedule[$day_date])){
									$shift_schedule[$day_date] = 'S_1';	
								//}
								$schedule_raw = explode('_', $shift_schedule[$day_date]);
								if(!isset($schedule_raw[2])){
									$schedule_raw[2] = 1;
								}

								// $this->log->write($schedule_raw[0]);
								
								$transaction_id = 0;
								if($schedule_raw[0] == 'S'){
									$shift_data = $this->getshiftdata($schedule_raw[1]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									$shift_code = $shift_data['shift_code'];
									$shift_id = $shift_data['shift_id'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$act_in_punch_date = $filter_date_start;
									$act_out_punch_date = $filter_date_start;
									
									$trans_exist_sql = "SELECT `transaction_id`, `act_intime`, `date`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									
									if($trans_exist->num_rows == 0){
										$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									} else {
										$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									}


									$act_outtimes = $this->getrawattendance_out_time($rvalue['emp_code'], $filter_date_start, $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}

									// $this->log->write($act_in_punch_date);
									// $this->log->write($act_intime);
									
									// $this->log->write($act_out_punch_date);
									// $this->log->write($act_outtime);

									if($act_intime == $act_outtime){
										$act_outtime = '00:00:00';
									}

									$abnormal_status = 0;
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									$early_time = '00:00:00';
									$working_time = '00:00:00';
									$late_mark = 0;
									$early_mark = 0;
									$shift_early = 0;
									if($act_intime != '00:00:00'){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										if($since_start->h > 0){
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										} else {
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										}
										$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
										if($since_start->invert == 1){
											$shift_early = 1;
											$late_time = '00:00:00';
										} else {
											$late_mark = 0;
										}
										$late_time = '00:00:00';
										
										if($act_outtime != '00:00:00'){
											if($shift_early == 1){
												if(strtotime($act_outtime) < strtotime($shift_intime)){
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											}
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											$first_half = 1;
											$second_half = 1;
											$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
										} else {
											$first_half = 1;
											$second_half = 1;
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
											$early_time = '00:00:00';
										}					
									} else {
										$first_half = 0;
										$second_half = 0;
									}
									
									if($working_time == '00:00:00'){
										$late_time = '00:00:00';
										$early_time = '00:00:00';
									}

									if($first_half == 1 && $second_half == 1){
										$present_status = 1;
										$absent_status = 0;
									} elseif($first_half == 1 && $second_half == 0){
										$present_status = 0.5;
										$absent_status = 0.5;
									} elseif($first_half == 0 && $second_half == 1){
										$present_status = 0.5;
										$absent_status = 0.5;
									} else {
										$present_status = 0;
										$absent_status = 1;
									}

									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));
									if($trans_exist->num_rows > 0){
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$this->log->write($rvalue['emp_code']);
											$this->log->write('UPDATES');
											$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$filter_date_start."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										} else {
											$this->log->write($rvalue['emp_code']);
											$this->log->write('NOT UPDATES');
											$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										}
									}
								} elseif ($schedule_raw[0] == 'W') {
									$shift_data = $this->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									$shift_code = $shift_data['shift_code'];
									$shift_id = $shift_data['shift_id'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$act_in_punch_date = $filter_date_start;
									$act_out_punch_date = $filter_date_start;
									
									$trans_exist_sql = "SELECT `transaction_id`, `act_intime`, `date`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									
									if($trans_exist->num_rows == 0){
										$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									} else {
										$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									}
									
									$act_outtimes = $this->getrawattendance_out_time($rvalue['emp_code'], $filter_date_start, $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
									
									if($act_intime == $act_outtime){
										$act_outtime = '00:00:00';
									}

									$abnormal_status = 0;
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									$early_time = '00:00:00';
									$working_time = '00:00:00';
									$late_mark = 0;
									$early_mark = 0;
									$shift_early = 0;
									if($act_intime != '00:00:00'){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										if($since_start->h > 0){
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										} else {
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										}
										$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
										if($since_start->invert == 1){
											$shift_early = 1;
											$late_time = '00:00:00';
										} else {
											$late_mark = 0;
										}
										$late_time = '00:00:00';
										
										if($act_outtime != '00:00:00'){
											if($shift_early == 1){
												if(strtotime($act_outtime) < strtotime($shift_intime)){
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											}
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											$first_half = 1;
											$second_half = 1;
											$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
										} else {
											$first_half = 1;
											$second_half = 1;
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
											$early_time = '00:00:00';
										}					
									} else {
										$first_half = 0;
										$second_half = 0;
									}
									
									if($working_time == '00:00:00'){
										$late_time = '00:00:00';
										$early_time = '00:00:00';
									}

									if($first_half == 1 && $second_half == 1){
										$present_status = 1;
										$absent_status = 0;
									} elseif($first_half == 1 && $second_half == 0){
										$present_status = 0.5;
										$absent_status = 0.5;
									} elseif($first_half == 0 && $second_half == 1){
										$present_status = 0.5;
										$absent_status = 0.5;
									} else {
										$present_status = 0;
										$absent_status = 1;
									}

									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));
									if($trans_exist->num_rows > 0){
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$this->log->write($rvalue['emp_code']);
											$this->log->write('NOT UPDATEW');
											$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$filter_date_start."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `halfday_status` = '0', `device_id` = '".$device_id."', `batch_id` = '".$batch_id."', `late_mark` = '".$late_mark."', `early_mark` = '".$early_mark."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										} else {
											$this->log->write($rvalue['emp_code']);
											$this->log->write('NOT UPDATEW');
											$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										}
									}
								} elseif ($schedule_raw[0] == 'H') {
									$shift_data = $this->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									$shift_code = $shift_data['shift_code'];
									$shift_id = $shift_data['shift_id'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$act_in_punch_date = $filter_date_start;
									$act_out_punch_date = $filter_date_start;
									
									$trans_exist_sql = "SELECT `transaction_id`, `act_intime`, `date`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									
									if($trans_exist->num_rows == 0){
										$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									} else {
										$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									}
									
									$act_outtimes = $this->getrawattendance_out_time($rvalue['emp_code'], $filter_date_start, $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
									
									if($act_intime == $act_outtime){
										$act_outtime = '00:00:00';
									}

									$abnormal_status = 0;
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									$early_time = '00:00:00';
									$working_time = '00:00:00';
									$late_mark = 0;
									$early_mark = 0;
									$shift_early = 0;
									if($act_intime != '00:00:00'){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										if($since_start->h > 0){
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										} else {
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										}
										$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
										if($since_start->invert == 1){
											$shift_early = 1;
											$late_time = '00:00:00';
										} else {
											$late_mark = 0;
										}
										$late_time = '00:00:00';
										
										if($act_outtime != '00:00:00'){
											if($shift_early == 1){
												if(strtotime($act_outtime) < strtotime($shift_intime)){
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											}
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											$first_half = 1;
											$second_half = 1;
											$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
										} else {
											$first_half = 1;
											$second_half = 1;
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
											$early_time = '00:00:00';
										}					
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									if($first_half == 1 && $second_half == 1){
										$present_status = 1;
										$absent_status = 0;
									} elseif($first_half == 1 && $second_half == 0){
										$present_status = 0.5;
										$absent_status = 0.5;
									} elseif($first_half == 0 && $second_half == 1){
										$present_status = 0.5;
										$absent_status = 0.5;
									} else {
										$present_status = 0;
										$absent_status = 1;
									}

									if($working_time == '00:00:00'){
										$late_time = '00:00:00';
										$early_time = '00:00:00';
									}

									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));
									if($trans_exist->num_rows > 0){
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$this->log->write($rvalue['emp_code']);
											$this->log->write('NOT UPDATEH');
											$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$filter_date_start."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										} else {
											$this->log->write($rvalue['emp_code']);
											$this->log->write('NOT UPDATEH');
											$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										}
									}
								} elseif($schedule_raw[0] == 'HD'){
									$shift_data = $this->getshiftdata($schedule_raw[1]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									$shift_code = $shift_data['shift_code'];
									$shift_id = $shift_data['shift_id'];
									
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$act_in_punch_date = $filter_date_start;
									$act_out_punch_date = $filter_date_start;
									
									$trans_exist_sql = "SELECT `transaction_id`, `act_intime`, `date`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									
									if($trans_exist->num_rows == 0){
										$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									} else {
										$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									}
									
									$act_outtimes = $this->getrawattendance_out_time($rvalue['emp_code'], $filter_date_start, $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
									
									if($act_intime == $act_outtime){
										$act_outtime = '00:00:00';
									}

									$abnormal_status = 0;
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									$early_time = '00:00:00';
									$working_time = '00:00:00';
									$late_mark = 0;
									$early_mark = 0;
									$shift_early = 0;
									if($act_intime != '00:00:00'){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										if($since_start->h > 0){
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										} else {
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										}
										$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
										if($since_start->invert == 1){
											$shift_early = 1;
											$late_time = '00:00:00';
										} else {
											$late_mark = 0;
										}
										$late_time = '00:00:00';
										
										if($act_outtime != '00:00:00'){
											if($shift_early == 1){
												if(strtotime($act_outtime) < strtotime($shift_intime)){
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											}
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											$first_half = 1;
											$second_half = 1;
											$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
										} else {
											$first_half = 1;
											$second_half = 1;
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
											$early_time = '00:00:00';
										}					
									} else {
										$first_half = 0;
										$second_half = 0;
									}
									
									if($first_half == 1 && $second_half == 1){
										$present_status = 1;
										$absent_status = 0;
									} elseif($first_half == 1 && $second_half == 0){
										$present_status = 0.5;
										$absent_status = 0.5;
									} elseif($first_half == 0 && $second_half == 1){
										$present_status = 0.5;
										$absent_status = 0.5;
									} else {
										$present_status = 0;
										$absent_status = 1;
									}

									if($working_time == '00:00:00'){
										$late_time = '00:00:00';
										$early_time = '00:00:00';
									}

									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));
									if($trans_exist->num_rows > 0){
											$this->log->write($rvalue['emp_code']);
											$this->log->write('NOT UPDATEHD');
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$filter_date_start."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										} else {
											$this->log->write($rvalue['emp_code']);
											$this->log->write('NOT UPDATEHD');
											$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										}
									}
								}

								if($act_in_punch_date == $act_out_punch_date) {
									if(($act_intime != '00:00:00') && ($act_outtime != '00:00:00')){
										$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
										$this->db->query($update);
										// $this->log->write($update);
									} elseif(($act_intime != '00:00:00') && ($act_outtime == '00:00:00')) {
										$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` = '".$act_intime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
										$this->db->query($update);
										// $this->log->write($update);
									} elseif(($act_intime == '00:00:00') && ($act_outtime != '00:00:00')) {
										$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` = '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
										$this->db->query($update);
										// $this->log->write($update);
									}
								} else {
									$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
									$this->db->query($update);
									// $this->log->write($update);
									
									$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
									$this->db->query($update);
									// $this->log->write($update);
								}
							}
							// $this->log->write('---end----');
						}

						foreach($days as $dkey => $dvalue){
							if(!in_array($dvalue['date'], $current_processed_data_date)){
								$filter_date_start = $dvalue['date'];
								
								$day_date = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));

								//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
								//$shift_schedule = $this->db->query($update3)->row;
								$shift_schedule[$day_date] = 'S_1';
								$schedule_raw = explode('_', $shift_schedule[$day_date]);
								if(!isset($schedule_raw[2])){
									$schedule_raw[2]= 1;
								}
								
								if($schedule_raw[0] == 'S'){
									$shift_data = $this->getshiftdata($schedule_raw[1]);
									if(isset($shift_data['shift_id'])){
										$shift_intime = $shift_data['in_time'];
										$shift_outtime = $shift_data['out_time'];
										$shift_code = $shift_data['shift_code'];
										$shift_id = $shift_data['shift_id'];
										$day = date('j', strtotime($filter_date_start));
										$month = date('n', strtotime($filter_date_start));
										$year = date('Y', strtotime($filter_date_start));
										$trans_exist_sql = "SELECT `transaction_id`, `leave_status`, `manual_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
										$trans_exist = $this->db->query($trans_exist_sql);
										if($trans_exist->num_rows > 0){
											$transaction_id = $trans_exist->row['transaction_id'];
											$leave_status = $trans_exist->row['leave_status'];
											$manual_status = $trans_exist->row['manual_status'];
											if($leave_status == 0 && $manual_status == 0){
												$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$this->db->escape($rvalue['name'])."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
												$this->db->query($sql);
												// $this->log->write($sql);
											} else {
												$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
												$this->db->query($sql);
												// $this->log->write($sql);
											}
										}
									}
								} elseif ($schedule_raw[0] == 'W') {
									$shift_data = $this->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									$shift_code = $shift_data['shift_code'];
									$shift_id = $shift_data['shift_id'];
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$trans_exist_sql = "SELECT `transaction_id`, `leave_status`, `manual_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows > 0){
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$sql = "UPDATE `oc_transaction` SET `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `absent_status` = '0', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										} else {
											$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										}
									}
								} elseif ($schedule_raw[0] == 'H') {
									$shift_data = $this->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									$shift_code = $shift_data['shift_code'];
									$shift_id = $shift_data['shift_id'];
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$trans_exist_sql = "SELECT `transaction_id`, `leave_status`, `manual_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows > 0){
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$sql = "UPDATE `oc_transaction` SET `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."', `absent_status` = '0', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										} else {
											$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										}
									}
								} elseif ($schedule_raw[0] == 'HD') {
									$shift_data = $this->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									$shift_code = $shift_data['shift_code'];
									$shift_id = $shift_data['shift_id'];	
									
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$trans_exist_sql = "SELECT `transaction_id`, `leave_status`, `manual_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows > 0){
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$sql = "UPDATE `oc_transaction` SET `firsthalf_status` = '0', `secondhalf_status` = '0', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										} else {
											$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											// $this->log->write($sql);
										}
									}
								}
							}
						}
					}
				}
			} else {
				$this->session->data['error'] = 'Attendance process already running';
				$this->redirect($this->url->link('tool/recalculate', 'token=' . $this->session->data['token'], 'SSL'));
			}
		//} else {
			//$this->session->data['error'] = 'Month Closed For Dates';
			//$this->redirect($this->url->link('tool/recalculate', 'token=' . $this->session->data['token'], 'SSL'));
		//}
		$sql = "DELETE FROM `oc_status` WHERE 1=1 ";
		if(!empty($filter_unit)){
			$sql .= " AND `unit_id` = '".$filter_unit."' ";
		}
		if(!empty($filter_division)){
			$sql .= " AND `division_id` = '".$filter_division."' ";
		}
		$this->db->query($sql);	
		//$sql = "UPDATE `oc_status` SET `process_status` = '0', `update_date_time_reprocess` = '".date('Y-m-d H:i:s')."' ";
		//$this->db->query($sql);	
		//echo 'out';exit;
		//echo 'out';exit;
		$this->session->data['success'] = 'You are done with process';
		$this->redirect($this->url->link('tool/recalculate', 'token=' . $this->session->data['token'], 'SSL'));
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function getEmployees_dat($emp_code) {
		$sql = "SELECT `division`, `region`, `unit`, `doj`, `emp_code`, `name`, `department`, `unit_id`, `card_number` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getempdata($emp_code, $punch_date) {
		$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name`, `company`, `company_id`, `contractor`, `contractor_id`, `contractor_code`, `category`, `category_id`, `designation_id`, `designation` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' AND (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$punch_date."') ";
		//$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name`, `company`, `company_id` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function insert_attendance_data($data) {
		$this->db->query("INSERT INTO `oc_attendance` SET `emp_id` = '".$data['employee_id']."', `card_id` = '".$data['card_id']."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `device_id` = '".$data['device_id']."', `status` = '0', `download_date` = '".$data['download_date']."', `new_log_id` = '".$data['log_id']."', `unit_id` = '".$data['unit_id']."' ");
		
	}

	public function getshiftdata($shift_id) {
		$shift_data = array(
			'shift_id' => 1,
			'name' => '08:00:00 - 20:00:00',
			'in_time' => '08:00:00',
			'out_time' => '20:00:00',
			'shift_code' => 'GS'
		);
		return $shift_data;
		// $query = $this->db->query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ");
		// if($query->num_rows > 0){
		// 	return $query->row;
		// } else {
		// 	return array();
		// }
	}

	public function getorderhistory($data){
		$sql = "SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$data['punch_date']."' AND `punch_time` = '".$data['in_time']."' AND `emp_id` = '".$data['employee_id']."' ";
		$query = $this->db->query($sql);
		if($query->num_rows > 0){
			return 1;
		} else {
			return 0;
		}
	}
	
	public function getrawattendance_in_time($emp_id, $punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		$query = $this->db->query("SELECT * FROM oc_attendance WHERE REPLACE(emp_id, ' ','') = '".$emp_id."' AND ((`punch_date` = '".$punch_date."' AND `punch_time` >= '05:00:00') OR (`punch_date` = '".$future_date."' AND `punch_time` <= '04:00:00')) ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ");
		
		// $this->log->write("SELECT * FROM oc_attendance WHERE REPLACE(emp_id, ' ','') = '".$emp_id."' AND ((`punch_date` = '".$punch_date."' AND `punch_time` >= '05:00:00') OR (`punch_date` = '".$future_date."' AND `punch_time` <= '04:00:00')) ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ");

		$array = $query->row;
		return $array;
	}

	public function getrawattendance_out_time($emp_id, $punch_date, $act_intime, $act_punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		$query = $this->db->query("SELECT * FROM oc_attendance WHERE REPLACE(emp_id, ' ','') = '".$emp_id."' AND ((`punch_date` = '".$punch_date."' AND `punch_time` >= '05:00:00') OR (`punch_date` = '".$future_date."' AND `punch_time` <= '04:00:00')) ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC");
		
		// $this->log->write("SELECT * FROM oc_attendance WHERE REPLACE(emp_id, ' ','') = '".$emp_id."' AND ((`punch_date` = '".$punch_date."' AND `punch_time` >= '05:00:00') OR (`punch_date` = '".$future_date."' AND `punch_time` <= '04:00:00')) ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC");

		$act_outtime = array();
		if($query->num_rows > 0){
			//echo '<pre>';
			//print_r($act_intime);

			$first_punch = $query->rows['0'];
			$array = $query->rows;
			$comp_array = array();
			$hour_array = array();
			
			//echo '<pre>';
			//print_r($array);

			//echo '<pre>';
			//print_r($array);

			foreach ($array as $akey => $avalue) {
				$start_date = new DateTime($act_punch_date.' '.$act_intime);
				$since_start = $start_date->diff(new DateTime($avalue['punch_date'].' '.$avalue['punch_time']));
				
				//echo '<pre>';
				//print_r($since_start);

				if($since_start->d == 0){
					$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					$hour_array[] = $since_start->h;
					$act_array[] = $avalue;
				}
			}

			// echo '<pre>';
			// print_r($hour_array);

			// echo '<pre>';
			// print_r($comp_array);

			foreach ($hour_array as $ckey => $cvalue) {
				/*if($cvalue > 20){
					unset($hour_array[$ckey]);
				}*/

				if($cvalue > 23){
					unset($hour_array[$ckey]);
				}
			}

			// echo '<pre>';
			// print_r($hour_array);
			
			$act_outtimes = '0';
			if($hour_array){
				$act_outtimes = max($hour_array);
			}

			// echo '<pre>';
			// print_r($act_outtimes);

			foreach ($hour_array as $akey => $avalue) {
				if($avalue == $act_outtimes){
					$act_outtime = $act_array[$akey];
				}
			}
		}
		// echo '<pre>';
		// print_r($act_outtime);
		// exit;		
		return $act_outtime;
	}
}
?>