<?php 
class ControllerToolEmpimport extends Controller { 
	private $error = array();
	public function index() {		
		$this->language->load('tool/empimport');
		$this->document->setTitle($this->language->get('heading_title'));
		//$this->load->model('tool/empimport');
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->user->hasPermission('modify', 'tool/empimport')) {
			if (is_uploaded_file($this->request->files['import_data']['tmp_name'])) {
				$content = file_get_contents($this->request->files['import_data']['tmp_name']);
			} else {
				$content = false;
			}
			if ($content) {
				$this->import_data($content);
				$this->session->data['success'] = 'Employee Imported Successfully';
				$this->redirect($this->url->link('tool/empimport', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->error['warning'] = $this->language->get('error_empty');
			}
		}

		$this->data['error_list'] = '0'; 
		if(isset($this->session->data['final_employee_error_data']) && !empty($this->session->data['final_employee_error_data'])){
			//echo '<pre>';
			//print_r($this->session->data['final_employee_error_data']);
			//exit;
			$this->data['error_list'] = '1'; 
			$this->data['employees'] = $this->session->data['final_employee_error_data'];
			$employee_total = 0;
			foreach($this->data['employees'] as $keys => $values){
				foreach($values as $key => $value){
					$employee_total ++;
				}
			}
			unset($this->session->data['final_employee_error_data']);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$this->data['entry_restore'] = $this->language->get('entry_restore');
		$this->data['entry_backup'] = $this->language->get('entry_backup');

		$this->data['button_backup'] = $this->language->get('button_backup');
		$this->data['button_restore'] = $this->language->get('button_restore');
		$this->data['button_revert'] = $this->language->get('button_revert');

		if (isset($this->session->data['error'])) {
			$this->data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('tool/backup', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['restore'] = $this->url->link('tool/empimport', 'token=' . $this->session->data['token'], 'SSL');

		$this->template = 'tool/empimport.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}
	
	public function import_data($content) {
		$valid = 0;
		// echo '<pre>';
		// print_r($this->request->files);
		// exit;
		if(isset($this->request->files['import_data']['name'])){
			$file_name = $this->request->files['import_data']['name'];
			$file_name_exp = explode('.', $file_name);
			if($file_name_exp[1] == 'csv'){
				$valid = 1;
			}
		}
		//echo $valid;exit;
		if(isset($this->request->files['import_data']['tmp_name']) && $this->request->files['import_data']['tmp_name'] != '' && $valid == 1){
			$file=fopen($this->request->files['import_data']['tmp_name'],"r");
			$i=1;
			$company_data = array();
			$contrator_data = array();
			$region_data = array();
			$division_data = array();
			$state_data = array();
			$site_data = array();
			$category_data = array();
			$employement_data = array();
			$grade_data = array();
			$designtion_data = array();
			$department_data = array();
			
			/*
			$this->db->query("TRUNCATE TABLE `oc_employee`");
			$this->db->query("TRUNCATE TABLE `oc_company`");
			$this->db->query("TRUNCATE TABLE `oc_contractor`");
			$this->db->query("TRUNCATE TABLE `oc_division`");
			$this->db->query("TRUNCATE TABLE `oc_region`");
			$this->db->query("TRUNCATE TABLE `oc_state`");
			$this->db->query("TRUNCATE TABLE `oc_unit`");
			$this->db->query("TRUNCATE TABLE `oc_category`");
			$this->db->query("TRUNCATE TABLE `oc_employement`");
			$this->db->query("TRUNCATE TABLE `oc_grade`");
			$this->db->query("TRUNCATE TABLE `oc_designation`");
			$this->db->query("TRUNCATE TABLE `oc_department`");
			$this->db->query("TRUNCATE TABLE `oc_shift_schedule`");
			$this->db->query("TRUNCATE TABLE `oc_shift_unit`");
			$this->db->query("TRUNCATE TABLE `oc_leave`");
			*/
			while(($var=fgetcsv($file,100000,","))!==FALSE){
				if($i != 1) {
					// echo '<pre>';
					// print_r($var);
					// exit;
					if($var[5] != ''){
						if(!isset($company_data[$var[5]])){
							$company_data[$var[5]] = html_entity_decode(strtolower(trim($var[5])));
						}
					}
					if($var[13] != ''){
						if(!isset($contractor_data[$var[14]])){
							$contractor_data[$var[14]]['name'] = html_entity_decode(strtolower(trim($var[13])));
							$contractor_data[$var[14]]['code'] = html_entity_decode(strtolower(trim($var[14])));
						}
					}
					// if($var[17] != ''){
					// 	if(!isset($region_data[$var[17]])){
					// 		$region_data[$var[17]] = html_entity_decode(strtolower(trim($var[17])));
					// 	}
					// } else {
					// 	$region_data['WIO'] = 'wio';
					// }
					if($var[8] != ''){
						if(!isset($division_data[$var[8]])){
							$division_data[$var[8]] = html_entity_decode(strtolower(trim($var[8])));
						}
					}
					// if($var[14] != ''){
					// 	if(!isset($state_data[$var[14]])){
					// 		$state_data[$var[14]] = html_entity_decode(strtolower(trim($var[14])));
					// 	}
					// } else {
					// 	$state_data['MAHARASHTRA'] = 'maharashtra';
					// }
					if($var[7] != ''){
						if(!isset($site_data[$var[7]])){
							$site_data[$var[7]] = html_entity_decode(strtolower(trim($var[7])));
						}
					}
					if($var[10] != ''){
						if(!isset($category_data[$var[10]])){
							$category_data[$var[10]] = html_entity_decode(strtolower(trim($var[10])));
						}
					}
					if($var[11] != ''){
						if(!isset($employement_data[$var[11]])){
							$employement_data[$var[11]] = html_entity_decode(strtolower(trim($var[11])));
						}
					}
					if($var[9] != ''){
						if(!isset($grade_data[$var[9]])){
							$grade_data[$var[9]] = html_entity_decode(strtolower(trim($var[9])));
						}
					}
					if($var[2] != ''){
						if(!isset($designtion_data[$var[2]])){
							$designtion_data[$var[2]] = html_entity_decode(strtolower(trim($var[2])));
						}
					}
					if($var[6] != ''){
						if(!isset($department_data[$var[6]])){
							$department_data[$var[6]] = html_entity_decode(strtolower(trim($var[6])));
						}
					}
				}
				$i ++;
			}
			fclose($file);
			
			$company_data_linked = array();
			foreach($company_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `company_id` FROM `oc_company` WHERE `company` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_company` SET `company` = '".$this->db->escape($dkey)."' ";
					//$this->db->query($sql);
					//$company_id = $this->db->getLastId();
					$company_id = 0;
				} else {
					$company_id = $is_exist->row['company_id'];
				}
				$company_data_linked[$dvalue] = $company_id;
			}

			$contractor_data_linked = array();
			// echo '<pre>';
			// print_r($contractor_data);
			// exit;
			foreach($contractor_data as $ckey => $cvalue){
				$code = trim(preg_replace('/\s+/',' ',$ckey));
				$name = trim(preg_replace('/\s+/',' ',$cvalue['name']));
				$name = strtoupper($name);

				$is_exist = $this->db->query("SELECT `contractor_id` FROM `oc_contractor` WHERE `contractor_code` = '".$this->db->escape($code)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_contractor` SET `contractor_name` = '".$this->db->escape($name)."', `contractor_code` = '".$this->db->escape($code)."' ";
					//$this->db->query($sql);
					//$contractor_id = $this->db->getLastId();
					$contractor_id = 0;
				} else {
					$contractor_id = $is_exist->row['contractor_id'];
				}
				$contractor_data_linked[$cvalue['code']] = $contractor_id;
				$contractor_data_linked_1[$cvalue['name']] = $contractor_id;
			}

			$region_data_linked = array();
			foreach($region_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `region_id` FROM `oc_region` WHERE `region` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_region` SET `region` = '".$this->db->escape($dkey)."' ";
					//$this->db->query($sql);
					//$region_id = $this->db->getLastId();
					$region_id = 0;
				} else {
					$region_id = $is_exist->row['region_id'];
				}
				$region_data_linked[$dvalue] = $region_id;
			}

			$division_data_linked = array();
			foreach($division_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `division_id` FROM `oc_division` WHERE `division` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_division` SET `division` = '".$this->db->escape($dkey)."' ";
					//$this->db->query($sql);
					//$division_id = $this->db->getLastId();
					$division_id = 0;
				} else {
					$division_id = $is_exist->row['division_id'];
				}
				$division_data_linked[$dvalue] = $division_id;
			}

			$state_data_linked = array();
			foreach($state_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `state_id` FROM `oc_state` WHERE `state` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_state` SET `state` = '".$this->db->escape($dkey)."' ";
					//$this->db->query($sql);
					//$state_id = $this->db->getLastId();
					$state_id = 0;
				} else {
					$state_id = $is_exist->row['state_id'];
				}
				$state_data_linked[$dvalue] = $state_id;
			}

			$site_data_linked = array();
			foreach($site_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `unit_id` FROM `oc_unit` WHERE `unit` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_unit` SET `unit` = '".$this->db->escape($dkey)."' ";
					//$this->db->query($sql);
					//$site_id = $this->db->getLastId();
					$site_id = 0;
				} else {
					$site_id = $is_exist->row['unit_id'];
				}
				$site_data_linked[$dvalue] = $site_id;
			}

			$category_data_linked = array();
			foreach($category_data as $ckey => $cvalue){
				$ckey = trim(preg_replace('/\s+/',' ',$ckey));
				$is_exist = $this->db->query("SELECT `category_id` FROM `oc_category` WHERE `category_name` = '".$this->db->escape($ckey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_category` SET `category_name` = '".$this->db->escape($ckey)."' ";
					//$this->db->query($sql);
					//$category_id = $this->db->getLastId();
					$category_id = 0;
				} else {
					$category_id = $is_exist->row['category_id'];
				}
				$category_data_linked[$cvalue] = $category_id;
			}

			$employement_data_linked = array();
			// echo '<pre>';
			// print_r($employement_data);
			// exit;
			foreach($employement_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$dkeys = strtolower($dkey);
				if($dkeys == 'p' || $dkeys == 'permanent'){
					$dkey = 'PERMANENT';
				} elseif($dkeys == 'c' || $dkeys == 'contract'){
					$dkey = 'CONTRACT';
				} elseif($dkeys == 't' || $dkeys == 'trainee'){
					$dkey = 'TRAINEE';
				} elseif($dkeys == 'l' || $dkeys == 'local'){
					$dkey = 'LOCAL';
				} elseif($dkeys == 't' || $dkeys == 'temporary'){
					$dkey = 'TEMPORARY';
				} else {
					$dkey = '';
				}
				if($dkey != ''){
					$is_exist = $this->db->query("SELECT `employement_id` FROM `oc_employement` WHERE `employement` = '".$this->db->escape($dkey)."' ");
					if($is_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_employement` SET `employement` = '".$this->db->escape($dkey)."' ";
						//$this->db->query($sql);
						//$employement_id = $this->db->getLastId();
						$employement_id = 0;
					} else {
						$employement_id = $is_exist->row['employement_id'];
					}	
					$employement_data_linked[$dvalue] = $employement_id;
				}
			}

			$grade_data_linked = array();
			foreach($grade_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `grade_id` FROM `oc_grade` WHERE `g_code` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_grade` SET `g_name` = '".$this->db->escape($dkey)."', `g_code` = '".$this->db->escape($dkey)."' ";
					//$this->db->query($sql);
					//$grade_id = $this->db->getLastId();
					$grade_id = 0;
				} else {
					$grade_id = $is_exist->row['grade_id'];
				}
				$grade_data_linked[$dvalue] = $grade_id;
			}

			$designation_data_linked = array();
			foreach($designtion_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `designation_id` FROM `oc_designation` WHERE `d_name` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_designation` SET `d_name` = '".$this->db->escape($dkey)."', `status` = '1' ";
					//$this->db->query($sql);
					//$designation_id = $this->db->getLastId();
					$designation_id = 0;
				} else {
					$designation_id = $is_exist->row['designation_id'];
				}
				$designation_data_linked[$dvalue] = $designation_id;
			}
			
			$department_data_linked = array();
			foreach($department_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `department_id` FROM `oc_department` WHERE `d_name` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_department` SET `d_name` = '".$this->db->escape($dkey)."', `status` = '1' ";
					//$this->db->query($sql);
					//$department_id = $this->db->getLastId();
					$department_id = 0;
				} else {
					$department_id = $is_exist->row['department_id'];
				}
				$department_data_linked[$dvalue] = $department_id;
			}
			//echo 'out';exit;

			$file=fopen($this->request->files['import_data']['tmp_name'],"r");
			$i=1;
			$months_array = array(
				'1' => '1',
				'2' => '2',
				'3' => '3',
				'4' => '4',
				'5' => '5',
				'6' => '6',
				'7' => '7',
				'8' => '8',
				'9' => '9',
				'10' => '10',
				'11' => '11',
				'12' => '12',
			);
			$shortmonthsarray = array(
				'Jan' => '1',
				'Feb' => '2',
				'Mar' => '3',
				'Apr' => '4',
				'May' => '5',
				'Jun' => '6',
				'Jul' => '7',
				'Aug' => '8',
				'Sep' => '9',
				'Oct' => '10',
				'Nov' => '11',
				'Dec' => '12',	
			);
			$final_employee_error_data = array();
			$employee_error_data = array();
			while(($var=fgetcsv($file,100000,","))!==FALSE){
				if($i != 1) {
					$employee_error_data = array();
					$emp_codes=addslashes($var[0]);//emp_code
					$findme = '~';
					$pos_emp_codes = strpos($emp_codes, $findme);
					if($pos_emp_codes !== false){
						$emp_code = substr($emp_codes, 1);
						$emp_code = preg_replace('/[^0-9]/', '', $emp_code);
					} else {
						$emp_code = $emp_codes;
						$emp_code = preg_replace('/[^0-9]/', '', $emp_code);
					}
					$emp_name = html_entity_decode(trim($var[1]));//name
					if($emp_code != '' && !isset($emp_code_exist[$emp_code])){
						if($emp_name == ''){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Employee Name Cannot be Blank',
							);
						}

						$device_code = '';
						//$device_codes = addslashes($var[2]);//device_code
						//$findme = '~';
						//$pos_device_codes = strpos($device_codes, $findme);
						//if($pos_device_codes !== false){
							//$device_code = substr($device_codes, 1);
						//} else {
							//$device_code = $device_codes;
						//}

						$designation_name = html_entity_decode(trim(addslashes($var[2])));//designation
						$designations = html_entity_decode(trim(strtolower(trim($var[2]))));//designation
						$designation_id = 0;
						if($designations != ''){
							if(isset($designation_data_linked[$designations])){
								$designation_id = $designation_data_linked[$designations];
								if($designation_id == '0'){
									$employee_error_data[] = array(
										'line_number' => $i,
										'reason' => 'Designation '.$designation_name.' Does Not Exist',
									);
								}
							} else {
								$employee_error_data[] = array(
									'line_number' => $i,
									'reason' => 'Designation '.$designation_name.' Does Not Exist',
								);
							}
						} else {
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Designation '.$designation_name.' Cannot be Blank',
							);
						}

						/*
						$dobs=addslashes($var[3]);//dob
						if($dobs != ''){
							$dobs_exp = explode('-', $dobs); // yyyy/mm/dd
							if(isset($dobs_exp[2])){
								$year_string = strlen($dobs_exp[0]);
								$month_string = strlen(sprintf('%02d', $dobs_exp[1]));
								$day_string = strlen(sprintf('%02d', $dobs_exp[2]));
								if($year_string == '4' && $month_string == '2' && $day_string == '2'){
									//echo 'in';exit;
									//if($month_string <= '12' && $day_string <= '31'){
										$dob = Date('Y-m-d', strtotime($dobs));//dob
									//} else {
										//$var3 = '0000-00-00';	
									//}
								} else {
									$dobs_exp = explode('-', $dobs);// dd/mm/yyyy
									if(isset($var33_exp[2])){
										$year_string = strlen($dobs_exp[2]);
										$month_string = strlen(sprintf('%02d', $dobs_exp[1]));
										$day_string = strlen(sprintf('%02d', $dobs_exp[0]));
										if($year_string == '4' && $month_string == '2' && $day_string == '2'){
											//if($month_string <= '12' && $day_string <= '31'){
												$dob = Date('Y-m-d', strtotime($dobs));//dob
											//} else {
												//$var3 = '0000-00-00';	
											//}
										} else {
											$dob = '0000-00-00';
										}
									} else {
										$dob = '0000-00-00';		
									}
								}
							} else {
								$dob = '0000-00-00';	
							}
						} else {
							$dob = '0000-00-00';
						}
						if($dob == '0000-00-00'){
							// $employee_error_data[] = array(
							// 	'line_number' => $i,
							// 	'reason' => 'Date Of Birth Blank / Invalid'
							// );
						}
						*/
						$dob = '0000-00-00';
						
						$dojs=addslashes($var[3]);//doj
						if($dojs != '' && $dojs != '0000-00-00' && $dojs != '00-00-0000'){
							$dojs_exp = explode('-', $dojs);// yyyy/mm/dd
							if(isset($dojs_exp[2])){
								$year_string = strlen($dojs_exp[0]);
								$month_string = strlen(sprintf('%02d', $dojs_exp[1]));
								$day_string = strlen(sprintf('%02d', $dojs_exp[2]));
								if($year_string == '4' && $month_string == '2' && $day_string == '2'){
									//if($month_string <= '12' && $day_string <= '31'){
										$doj = Date('Y-m-d', strtotime($dojs));//doj
										//if(strtotime($var4) > strtotime(date('Y-m-d'))){
											//$var4 = '0000-00-00';	
										//}
									//} else {
										//$var4 = '0000-00-00';	
									//}
								} else {
									$dojs_exp = explode('-', $dojs);// dd/mm/yyyy
									if(isset($dojs_exp[2])){
										$year_string = strlen($dojs_exp[2]);
										$month_string = strlen(sprintf('%02d', $dojs_exp[1]));
										$day_string = strlen(sprintf('%02d', $dojs_exp[0]));
										if($year_string == '4' && $month_string == '2' && $day_string == '2'){
											//if($month_string <= '12' && $day_string <= '31'){
												$doj = Date('Y-m-d', strtotime($dojs));//doj
												//if(strtotime($var4) > strtotime(date('Y-m-d'))){
													//$var4 = '0000-00-00';	
												//}
											//} else {
												//$var4 = '0000-00-00';	
											//}
										} else {
											$doj = '0000-00-00';		
										}
									} else {
										$doj = '0000-00-00';
									}
								}
							} else {
								$doj = '0000-00-00';	
							}
						} else {
							$doj = '0000-00-00';
						}
						if($doj == '0000-00-00'){
							//$employee_error_data[] = array(
								//'line_number' => $i,
								//'reason' => 'Date Of Joining Blank / Invalid'
							//);
						}

						$docs=addslashes($var[4]);//doj
						if($docs != '' && $docs != '0000-00-00' && $docs != '00-00-0000'){
							$docs_exp = explode('-', $docs);// yyyy/mm/dd
							if(isset($docs_exp[2])){
								$year_string = strlen($docs_exp[0]);
								$month_string = strlen(sprintf('%02d', $docs_exp[1]));
								$day_string = strlen(sprintf('%02d', $docs_exp[2]));
								if($year_string == '4' && $month_string == '2' && $day_string == '2'){
									//if($month_string <= '12' && $day_string <= '31'){
										$doc = Date('Y-m-d', strtotime($docs));//doj
										//if(strtotime($var4) > strtotime(date('Y-m-d'))){
											//$var4 = '0000-00-00';	
										//}
									//} else {
										//$var4 = '0000-00-00';	
									//}
								} else {
									$docs_exp = explode('-', $docs);// dd/mm/yyyy
									if(isset($docs_exp[2])){
										$year_string = strlen($docs_exp[2]);
										$month_string = strlen(sprintf('%02d', $docs_exp[1]));
										$day_string = strlen(sprintf('%02d', $docs_exp[0]));
										if($year_string == '4' && $month_string == '2' && $day_string == '2'){
											//if($month_string <= '12' && $day_string <= '31'){
												$doc = Date('Y-m-d', strtotime($docs));//doj
												//if(strtotime($var4) > strtotime(date('Y-m-d'))){
													//$var4 = '0000-00-00';	
												//}
											//} else {
												//$var4 = '0000-00-00';	
											//}
										} else {
											$doc = '0000-00-00';		
										}
									} else {
										$doc = '0000-00-00';
									}
								}
							} else {
								$doc = '0000-00-00';	
							}
						} else {
							$doc = '0000-00-00';
						}
						if($doc == '0000-00-00'){
							//$employee_error_data[] = array(
								//'line_number' => $i,
								//'reason' => 'Date Of Confirmation Blank / Invalid'
							//);
						}
						$dol = '0000-00-00';
						
						$company_name = html_entity_decode(trim(addslashes($var[5])));//company
						$companys = html_entity_decode(trim(strtolower(trim($var[5]))));//company
						$company_id = 0;
						if($companys != ''){
							if(isset($company_data_linked[$companys])){
								$company_id = $company_data_linked[$companys];
								if($company_id == 0){
									$employee_error_data[] = array(
										'line_number' => $i,
										'reason' => 'Company '.$companys.' Does Not Exist',
									);
								}
							} else {
								$employee_error_data[] = array(
									'line_number' => $i,
									'reason' => 'Company '.$companys.' Does Not Exist',
								);	
							}
						} else {
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Company '.$companys.' Cannot be Blank',
							);	
						}

						$department_name = html_entity_decode(trim(addslashes($var[6])));//department
						$departments = html_entity_decode(trim(strtolower(trim($var[6]))));//department
						$department_id = 0;
						if($departments != ''){
							if(isset($department_data_linked[$departments])){
								$department_id = $department_data_linked[$departments];
								if($department_id == '0'){
									$employee_error_data[] = array(
										'line_number' => $i,
										'reason' => 'Department '.$department_name.' Does Not Exist',
									);
								}
							} else {
								$employee_error_data[] = array(
									'line_number' => $i,
									'reason' => 'Department '.$department_name.' Does Not Exist',
								);	
							}
						} else {
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Department '.$department_name.' Cannot be Blank',
							);
						}

						$unit_name = html_entity_decode(trim(addslashes($var[7])));//site
						$units = html_entity_decode(trim(strtolower(trim($var[7]))));//site
						$unit_id = 0;
						if($units != ''){
							if(isset($site_data_linked[$units])){
								$unit_id = $site_data_linked[$units];
								if($unit_id == '0'){
									$employee_error_data[] = array(
										'line_number' => $i,
										'reason' => 'Site '.$unit_name.' Does Not Exist',
									);
								}
							} else {
								$employee_error_data[] = array(
									'line_number' => $i,
									'reason' => 'Site '.$unit_name.' Does Not Exist',
								);	
							}
						} else {
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Site '.$unit_name.' Cannot be Blank',
							);
						}

						$region_id = 0;
						$region_name = '';
						/*
						$region_name = html_entity_decode(trim(addslashes($var[17])));//region
						$regions = html_entity_decode(trim(strtolower(trim($var[17]))));//region
						if($regions != ''){
							$region_id = $region_data_linked[$regions];
						} else {
							$region_id = 0;
						}
						if($region_id == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Region '.$region_name.' Does Not Exist',
							);
						}
						*/

						$division_name = html_entity_decode(trim(addslashes($var[8])));//division
						$divisions = html_entity_decode(trim(strtolower(trim($var[8]))));//division
						$division_id = 0;
						if($divisions != ''){
							if(isset($division_data_linked[$divisions])){
								$division_id = $division_data_linked[$divisions];
								if($division_id == '0'){
									$employee_error_data[] = array(
										'line_number' => $i,
										'reason' => 'Division '.$division_name.' Does Not Exist',
									);
								}
							} else {
								$employee_error_data[] = array(
									'line_number' => $i,
									'reason' => 'Division '.$division_name.' Does Not Exist',
								);	
							}
						} else {
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Division '.$division_name.' Cannot be Blank',
							);
						}

						$grade_name = html_entity_decode(trim(addslashes($var[9])));//grade
						$grades = html_entity_decode(trim(strtolower(trim($var[9]))));//grade
						$grade_id = 0;
						if($grades != ''){
							if(isset($grade_data_linked[$grades])){
								$grade_id = $grade_data_linked[$grades];
								if($grade_id == '0'){
									$employee_error_data[] = array(
										'line_number' => $i,
										'reason' => 'Grade '.$grade_name.' Does Not Exist',
									);
								}
							} else {
								$employee_error_data[] = array(
									'line_number' => $i,
									'reason' => 'Grade '.$grade_name.' Does Not Exist',
								);
							}
						} else {
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Grade '.$grade_name.' Cannot be Blank',
							);
						}

						$category_name = html_entity_decode(trim(addslashes($var[10])));//category
						$categorys = html_entity_decode(trim(strtolower(trim($var[10]))));//category
						$category_id = 0;
						if($categorys != ''){
							if(isset($category_data_linked[$categorys])){
								$category_id = $category_data_linked[$categorys];
								if($category_id == '0'){
									$employee_error_data[] = array(
										'line_number' => $i,
										'reason' => 'Category '.$category_name.' Does Not Exist',
									);
								}
							} else {
								$employee_error_data[] = array(
									'line_number' => $i,
									'reason' => 'Category '.$category_name.' Does Not Exist',
								);
							}
						} else {
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Category '.$category_name.' Cannot be Blank',
							);	
						}

						$employeement_name = html_entity_decode(trim(addslashes($var[11])));//employeement
						$employeements = html_entity_decode(trim(strtolower(trim($var[11]))));//employeement
						$employeement_id = 0;
						if($employeements != ''){
							if(isset($employement_data_linked[$employeements])){
								$employeement_id = $employement_data_linked[$employeements];
								if($employeement_id == '0'){
									$employee_error_data[] = array(
										'line_number' => $i,
										'reason' => 'Employement '.$employeement_name.' Does Not Exist',
									);
								}
							} else {
								$employee_error_data[] = array(
									'line_number' => $i,
									'reason' => 'Employement '.$employeement_name.' Does Not Exist',
								);
							}
						} else {
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Employement '.$employeement_name.' Cannot be Blank',
							);	
						}

						$genders = addslashes(trim(strtolower($var[12])));//gender
						if($genders == 'm' || $genders == 'male'){
							$gender = 'M';
						} elseif($genders == 'f' || $genders == 'female'){
							$gender = 'F';
						} else {
							$gender = '';
						}
						if($gender == ''){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Gender Value is Blank / Invalid',
							);
						}

						$contractor_name = html_entity_decode(trim(addslashes($var[13])));//contractor_name
						$contractor_code = html_entity_decode(trim(addslashes($var[14])));//contractor_code
						$contractors = html_entity_decode(trim(strtolower(trim($var[14]))));//contractor_code
						$contractor_id = 0;
						if($contractors != ''){
							if(isset($contractor_data_linked[$contractors])){
								$contractor_id = $contractor_data_linked[$contractors];
								if($contractor_id == '0'){
									$employee_error_data[] = array(
										'line_number' => $i,
										'reason' => 'Contractor '.$contractor_name.' Does Not Exist',
									);
								}
							} else {
								$employee_error_data[] = array(
									'line_number' => $i,
									'reason' => 'Contractor '.$contractor_name.' Does Not Exist',
								);
							}
						} else {
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Contractor '.$contractor_name.' Cannot be Blank',
							);
						}

						$state_name = '';
						$state_id = '0';
						
						$statuss = addslashes(strtolower($var[15]));//status
						if($statuss == 'working' || $statuss == 'active'){
							$status = '1';
						} else {
							$status = '0';
						}
						
						$user_name = $emp_code;
						$salt = substr(md5(uniqid(rand(), true)), 0, 9);
						$password = sha1($salt . sha1($salt . sha1($emp_code)));
						$shift_id = 1;
						
						// echo '<pre>';
						// print_r($employee_error_data);
						// exit;

						if(empty($employee_error_data)){
							$is_exist = $this->db->query("SELECT `emp_code`, `shift_id`, `unit_id`, `doj` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ");
							if($is_exist->num_rows == 0){
								$db_shift_id = 0;
								$db_unit_id = 0;
								$db_sat_status = 0;
								$db_doj = '0000-00-00';
								$insert = "INSERT INTO `oc_employee` SET 
											`emp_code` = '".$emp_code."', 
											`name` = '".$this->db->escape($emp_name)."', 
											`device_code` = '".$device_code."', 
											`gender` = '".$gender."', 
											`dob` = '".$dob."', 
											`doj` = '".$doj."', 
											`doc` = '".$doc."', 
											`dol` = '".$dol."', 
											`company` = '".$company_name."',
											`company_id` = '".$company_id."',
											`contractor` = '".$contractor_name."',
											`contractor_code` = '".$contractor_code."',
											`contractor_id` = '".$contractor_id."',
											`region` = '".$region_name."',
											`region_id` = '".$region_id."',
											`division` = '".$division_name."',
											`division_id` = '".$division_id."',
											`state` = '".$state_name."',
											`state_id` = '".$state_id."',
											`unit` = '".$unit_name."',
											`unit_id` = '".$unit_id."',
											`category` = '".$category_name."',
											`category_id` = '".$category_id."',
											`employement` = '".$employeement_name."',
											`employement_id` = '".$employeement_id."',
											`department` = '".$department_name."',
											`department_id` = '".$department_id."',
											`grade` = '".$grade_name."',
											`grade_id` = '".$grade_id."',
											`designation` = '".$designation_name."', 
											`designation_id` = '".$designation_id."', 
											`status` = '".$status."', 
											`shift_type` = 'F',
											`user_group_id` = '11',
											`username` = '".$user_name."', 
											`password` = '".$password."', 
											`shift_id` = '".$shift_id."', 
											`sat_status` = '0', 
											`sun_status` = '0', 
											`is_new` = '0',
											`salt` = '".$salt."' "; 
								$this->db->query($insert);

								$check_new_emp = $this->db->query("select emp_id,device_id from oc_attendance where `emp_id` = '".$emp_code."' AND  is_new_employee = 1 group by emp_id");
								if($check_new_emp->num_rows > 0){
									$this->db->query("UPDATE `oc_attendance` SET `is_new_employee` = '0' WHERE `emp_id` = '".$check_new_emp->row['emp_id']."' ");
								}
								// echo $insert;
								// echo '<br />';
								// exit;
								$shift_id_data = 'S_'.$shift_id;
								foreach ($months_array as $key => $value) {
									$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$emp_code."' AND `month` = '".$key."' AND `year` = '".date('Y')."' ");
									if($is_exist->num_rows == 0){
										$insert1 = "INSERT INTO `oc_shift_schedule` SET 
													`emp_code` = '".$emp_code."',
													`1` = '".$shift_id_data."',
													`2` = '".$shift_id_data."',
													`3` = '".$shift_id_data."',
													`4` = '".$shift_id_data."',
													`5` = '".$shift_id_data."',
													`6` = '".$shift_id_data."',
													`7` = '".$shift_id_data."',
													`8` = '".$shift_id_data."',
													`9` = '".$shift_id_data."',
													`10` = '".$shift_id_data."',
													`11` = '".$shift_id_data."',
													`12` = '".$shift_id_data."', 
													`13` = '".$shift_id_data."', 
													`14` = '".$shift_id_data."', 
													`15` = '".$shift_id_data."', 
													`16` = '".$shift_id_data."', 
													`17` = '".$shift_id_data."', 
													`18` = '".$shift_id_data."', 
													`19` = '".$shift_id_data."', 
													`20` = '".$shift_id_data."', 
													`21` = '".$shift_id_data."', 
													`22` = '".$shift_id_data."', 
													`23` = '".$shift_id_data."', 
													`24` = '".$shift_id_data."', 
													`25` = '".$shift_id_data."', 
													`26` = '".$shift_id_data."', 
													`27` = '".$shift_id_data."', 
													`28` = '".$shift_id_data."', 
													`29` = '".$shift_id_data."', 
													`30` = '".$shift_id_data."', 
													`31` = '".$shift_id_data."',
													`month` = '".$key."',
													`year` = '".date('Y')."',
													`status` = '1' ,
													`unit` = '".$unit_name."',
													`unit_id` = '".$unit_id."' ";
										$this->db->query($insert1);
									} else {
										$shift_schedule_data = $is_exist->row;
										$shift = $shift_id_data;
										$insert1 = "UPDATE `oc_shift_schedule` SET 
													`emp_code` = '".$emp_code."',
													`1` = '".$shift_id_data."',
													`2` = '".$shift_id_data."',
													`3` = '".$shift_id_data."',
													`4` = '".$shift_id_data."',
													`5` = '".$shift_id_data."',
													`6` = '".$shift_id_data."',
													`7` = '".$shift_id_data."',
													`8` = '".$shift_id_data."',
													`9` = '".$shift_id_data."',
													`10` = '".$shift_id_data."',
													`11` = '".$shift_id_data."',
													`12` = '".$shift_id_data."', 
													`13` = '".$shift_id_data."', 
													`14` = '".$shift_id_data."', 
													`15` = '".$shift_id_data."', 
													`16` = '".$shift_id_data."', 
													`17` = '".$shift_id_data."', 
													`18` = '".$shift_id_data."', 
													`19` = '".$shift_id_data."', 
													`20` = '".$shift_id_data."', 
													`21` = '".$shift_id_data."', 
													`22` = '".$shift_id_data."', 
													`23` = '".$shift_id_data."', 
													`24` = '".$shift_id_data."', 
													`25` = '".$shift_id_data."', 
													`26` = '".$shift_id_data."', 
													`27` = '".$shift_id_data."', 
													`28` = '".$shift_id_data."', 
													`29` = '".$shift_id_data."', 
													`30` = '".$shift_id_data."', 
													`31` = '".$shift_id_data."',
													`month` = '".$key."',
													`year` = '".date('Y')."',
													`status` = '1' ,
													`unit` = '".$unit_name."',
													`unit_id` = '".$unit_id."' 
													WHERE id = '".$shift_schedule_data['id']."' ";
										$this->db->query($insert1);
									} 
								}

								$current_date = date('Y-m-d');
								$current_month = date('n');
								if($current_month == 1 || $current_month == 2){
									$start_date = date('Y-02-01');
								} else {
									$start_date = date('Y-m-d', strtotime($current_date . ' -60 day'));//'2017-06-26';
								}
								$end_date = date('Y-m-d', strtotime($current_date . ' +1 day'));//'2017-06-30'
								$day = array();
								$days = $this->GetDays($start_date, $end_date);
								foreach ($days as $dkey => $dvalue) {
									$dates = explode('-', $dvalue);
									$day[$dkey]['day'] = $dates[2];
									$day[$dkey]['date'] = $dvalue;
								}
								$batch_id = '0';
								foreach($day as $dkeys => $dvalues){
									$filter_date_start = $dvalues['date'];
									$results = $this->getemployee_code($emp_code);
									// echo '<pre>';
									// print_r($results);
									// exit;
									foreach ($results as $rkey => $rvalue) {
										if(isset($rvalue['name']) && $rvalue['name'] != ''){
											$emp_name = $rvalue['name'];
											$department = $rvalue['department'];
											$unit = $rvalue['unit'];
											$group = '';
											$day_date = date('j', strtotime($filter_date_start));
											$month = date('n', strtotime($filter_date_start));
											$year = date('Y', strtotime($filter_date_start));
											$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
											//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
											$shift_schedule = $this->db->query($update3)->row;
											$schedule_raw = explode('_', $shift_schedule[$day_date]);
											if(!isset($schedule_raw[2])){
												$schedule_raw[2]= 1;
											}
											/*echo '<pre>';
											print_r($schedule_raw);
											exit;*/
											if($schedule_raw[0] == 'S'){
												$shift_data = $this->getshiftdata_data($schedule_raw[1]);
												if(isset($shift_data['shift_id'])){
													$shift_intime = $shift_data['in_time'];
													$shift_outtime = $shift_data['out_time'];
													$shift_code = $shift_data['shift_code'];
													$shift_id = $shift_data['shift_id'];
													$day = date('j', strtotime($filter_date_start));
													$month = date('n', strtotime($filter_date_start));
													$year = date('Y', strtotime($filter_date_start));
													$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
													$trans_exist = $this->db->query($trans_exist_sql);
													if($trans_exist->num_rows == 0){
														$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
														//echo $sql;exit;
														$this->db->query($sql);
													}
												} else {
													$shift_data = $this->getshiftdata_data('1');
													$shift_intime = $shift_data['in_time'];
													$shift_outtime = $shift_data['out_time'];
													$shift_code = $shift_data['shift_code'];
													$shift_id = $shift_data['shift_id'];
													$day = date('j', strtotime($filter_date_start));
													$month = date('n', strtotime($filter_date_start));
													$year = date('Y', strtotime($filter_date_start));
													$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
													$trans_exist = $this->db->query($trans_exist_sql);
													if($trans_exist->num_rows == 0){
														$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
														$this->db->query($sql);
													}
												}
											} elseif ($schedule_raw[0] == 'W') {
												$shift_data = $this->getshiftdata_data($schedule_raw[2]);
												if(!isset($shift_data['shift_id'])){
													$shift_data = $this->getshiftdata_data('1');
												}
												$shift_intime = $shift_data['in_time'];
												$shift_outtime = $shift_data['out_time'];
												$shift_code = $shift_data['shift_code'];
												$shift_id = $shift_data['shift_id'];
												$act_intime = '00:00:00';
												$act_outtime = '00:00:00';
												$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
												$trans_exist = $this->db->query($trans_exist_sql);
												if($trans_exist->num_rows == 0){
													$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
													$this->db->query($sql);
												}
											} elseif ($schedule_raw[0] == 'H') {
												$shift_data = $this->getshiftdata_data($schedule_raw[2]);
												if(!isset($shift_data['shift_id'])){
													$shift_data = $this->getshiftdata_data('1');
												}
												$shift_intime = $shift_data['in_time'];
												$shift_outtime = $shift_data['out_time'];
												$shift_code = $shift_data['shift_code'];
												$shift_id = $shift_data['shift_id'];
												$act_intime = '00:00:00';
												$act_outtime = '00:00:00';
												$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
												$trans_exist = $this->db->query($trans_exist_sql);
												if($trans_exist->num_rows == 0){
													$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
													$this->db->query($sql);
												}
											} elseif ($schedule_raw[0] == 'HD') {
												$shift_data = $this->getshiftdata_data($schedule_raw[2]);
												if(!isset($shift_data['shift_id'])){
													$shift_data = $this->getshiftdata_data('1');
												}
												$shift_intime = $shift_data['in_time'];
												if($shift_data['shift_id'] == '1'){
													if($rvalue['sat_status'] == '1'){
														$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
														$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
													} else {
														$shift_outtime = $shift_data['out_time'];
													}
												} else {
													$shift_intime = $shift_data['in_time'];
													$shift_outtime = $shift_data['out_time'];
												}
												$shift_code = $shift_data['shift_code'];
												$shift_id = $shift_data['shift_id'];
												$act_intime = '00:00:00';
												$act_outtime = '00:00:00';
												$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
												$trans_exist = $this->db->query($trans_exist_sql);
												if($trans_exist->num_rows == 0){
													$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
													$this->db->query($sql);
												}
											}
										}	
									}
								}

							} else {
								$employee_error_data[] = array(
									'line_number' => $i,
									'reason' => 'Employee Code Exist in Database'
								);
								$final_employee_error_data[] = $employee_error_data;	
							}
						} else {
							$final_employee_error_data[] = $employee_error_data;	
						}
						$emp_code_exist[$emp_code] = $emp_code;
						$emp_name_exist[$emp_name] = $emp_name;
					} else {
						if(isset($emp_code_exist[$var0])){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Employee Code Duplicate'
							);
						}
						if($emp_code == '' || $emp_code == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Employee Code Empty'
							);
						}
						$final_employee_error_data[] = $employee_error_data;
					}
				}
				$i ++;	
			}
			fclose($file);
			$url = $this->url->link('tool/empimport', 'token=' . $this->session->data['token'], true);
			$url = str_replace('&amp;', '&', $url);
			if(empty($final_employee_error_data)){
				$this->session->data['success'] = 'Employees Imported Successfully';
			} else {
				$this->session->data['warning'] = 'Employees Imported with following Errors';
			}
			$this->session->data['final_employee_error_data'] = $final_employee_error_data;
			$this->response->redirect($url);
		} else {
			$url = $this->url->link('tool/empimport', 'token=' . $this->session->data['token'], true);
			$url = str_replace('&amp;', '&', $url);
			$this->session->data['warning'] = 'Please Select the Valid CSV File';
			$this->response->redirect($url);
		}
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}
	public function getEmployees_dat($emp_code) {
		$sql = "SELECT `division`, `region`, `unit`, `doj` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getempdata($emp_code) {
		$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function insert_attendance_data($data) {
		$sql = $this->db->query("INSERT INTO `oc_attendance` SET `emp_id` = '".$data['employee_id']."', `card_id` = '".$data['card_id']."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `device_id` = '".$data['device_id']."', `status` = '0', `download_date` = '".$data['download_date']."', `log_id` = '".$data['log_id']."' ");
	}
	public function getemployees($data) {
		$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name` FROM `oc_employee` WHERE `status` = '1' ";
		if(!empty($data['filter_name_id'])){
			$sql .= " AND `emp_code` = '".$data['filter_name_id']."' ";
		}
		$sql .= " ORDER BY `shift_type` ";		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getrawattendance_group_date_custom($emp_code, $date) {
		$query = $this->db->query("SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$date."' AND `emp_id` = '".$emp_code."' GROUP by `punch_date` ");
		return $query->row;
	}
	public function getshiftdata($shift_id) {
		$shift_data = array();
		if($shift_id == '1'){
			$shift_data['shift_id'] = '1';
			$shift_data['in_time'] = '09:30:00';
			$shift_data['out_time'] = '18:30:00';
		} else {
			$shift_data['shift_id'] = '2';
			$shift_data['in_time'] = '08:00:00';
			$shift_data['out_time'] = '20:00:00';
		}
		return $shift_data;
		// $query = $this->db->query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ");
		// if($query->num_rows > 0){
		// 	return $query->row;
		// } else {
		// 	return array();
		// }
	}

	public function getrawattendance_in_time($emp_id, $punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		//$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ", $conn);
		//$query = $this->db->query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ");
		$query = $this->db->query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC ");
		$array = $query->row;
		return $array;
	}

	public function getrawattendance_out_time($emp_id, $punch_date, $act_intime, $act_punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		//$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC", $conn);
		//$query = $this->db->query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC");
		$query = $this->db->query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC");
		$act_outtime = array();
		if($query->num_rows > 0){
			//echo '<pre>';
			//print_r($act_intime);

			$first_punch = $query->rows['0'];
			$array = $query->rows;
			$comp_array = array();
			$hour_array = array();
			
			//echo '<pre>';
			//print_r($array);

			foreach ($array as $akey => $avalue) {
				$start_date = new DateTime($act_punch_date.' '.$act_intime);
				$since_start = $start_date->diff(new DateTime($avalue['punch_date'].' '.$avalue['punch_time']));
				
				//echo '<pre>';
				//print_r($since_start);

				if($since_start->d == 0){
					$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					$hour_array[] = $since_start->h;
					$act_array[] = $avalue;
				}
			}

			//echo '<pre>';
			//print_r($hour_array);

			//echo '<pre>';
			//print_r($comp_array);

			foreach ($hour_array as $ckey => $cvalue) {
				if($cvalue > 15){
					unset($hour_array[$ckey]);
				}
			}

			//echo '<pre>';
			//print_r($hour_array);
			$act_outtimes = '0';
			if($hour_array){
				$act_outtimes = max($hour_array);
			}
			//echo '<pre>';
			//print_r($act_outtimes);

			foreach ($hour_array as $akey => $avalue) {
				if($avalue == $act_outtimes){
					$act_outtime = $act_array[$akey];
				}
			}
		}
		//echo '<pre>';
		//print_r($act_outtime);
		//exit;		
		return $act_outtime;
	}

	public function getorderhistory($data){
		$sql = "SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$data['punch_date']."' AND `punch_time` = '".$data['in_time']."' AND `emp_id` = '".$data['employee_id']."' ";
		$query = $this->db->query($sql);
		if($query->num_rows > 0){
			return 1;
		} else {
			return 0;
		}
	}

	public function getshiftdata_data($shift_id) {
		$query = $this->db->query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ");
		if($query->num_rows > 0){
			return $query->row;
		} else {
			return array();
		}
	}

	public function getemployee_code($emp_code) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "employee WHERE emp_code = '" . (int)$emp_code . "'");
		return $query->rows;
	}
}
?>