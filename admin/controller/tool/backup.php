<?php 
class ControllerToolBackup extends Controller { 
	private $error = array();
	public function index() {		
		$this->language->load('tool/backup');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('tool/backup');
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->user->hasPermission('modify', 'tool/backup')) {
			if (is_uploaded_file($this->request->files['import']['tmp_name'])) {
				$content = file_get_contents($this->request->files['import']['tmp_name']);
			} else {
				$content = false;
			}
			if ($content) {
				$this->import_data($content);
				$this->session->data['success'] = 'Data Imported Successfully';
				$this->redirect($this->url->link('tool/backup', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->error['warning'] = $this->language->get('error_empty');
			}
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$this->data['entry_restore'] = $this->language->get('entry_restore');
		$this->data['entry_backup'] = $this->language->get('entry_backup');

		$this->data['button_backup'] = $this->language->get('button_backup');
		$this->data['button_restore'] = $this->language->get('button_restore');
		$this->data['button_revert'] = $this->language->get('button_revert');

		if (isset($this->session->data['error'])) {
			$this->data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('tool/backup', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['restore'] = $this->url->link('tool/backup', 'token=' . $this->session->data['token'], 'SSL');

		$this->template = 'tool/backup.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}
	
	public function import_data($content) {
		$this->language->load('tool/backup');
		$this->load->model('tool/backup');
		
		$batch_ids = $this->db->query("SELECT `batch_id` FROM `oc_transaction` ORDER BY `batch_id` DESC LIMIT 1");
		if($batch_ids->num_rows > 0){
			$batch_id = $batch_ids->row['batch_id'] + 1;
		} else {
			$batch_id = 1;
		}
		$exp_datas = array();
		$exp_datas = explode(PHP_EOL, $content);
		$this->data['employees'] = array();
		$employees = array();
		$this->load->model('catalog/employee');		
		$employees = array();
		$employees_final = array();
		foreach($exp_datas as $data) { //echo "<pre>";print_r($data);exit;
			$tdata = $data;
			// $emp_id  = trim(substr($tdata, 0, 14));
			// $in_date_raw = substr($tdata, 15, 10);
			// $in_date = date('Y-m-d', strtotime($in_date_raw));
			// $in_time_hour = substr($tdata, 26, 2);
			// $in_time_min = substr($tdata, 29, 2);
			// $in_time_sec = substr($tdata, 32, 2);
			// $in_time = $in_time_hour.':'.$in_time_min.':'.$in_time_sec;
			
			//echo '<pre>';print_r($tdata);

			$tdatass  = trim(str_replace("\t","/",$tdata));
			// echo '<pre>';print_r('---------');
			// echo '<pre>';print_r($tdatass);
			$tdatass = explode("/",$tdatass);
			// echo '<pre>';print_r($tdatass);
			// exit;
			$emp_id  = '';
			$in_date = '0000-00-00';
			$in_time = '00:00:00';

			if(isset($tdatass[1])){
				$emp_id  = trim($tdatass[0]);
				$in_date_raw =  trim($tdatass[1]);
				$in_date = date('Y-m-d', strtotime($in_date_raw));
				$in_time = date('H:i:s', strtotime($in_date_raw));
			}

			// echo '<pre>';print_r($emp_id);
			// echo '<pre>';print_r($in_date);
			// echo '<pre>';print_r($in_time);

			// echo '<pre>';print_r('--------------------');exit;

			// $in_date_raw = substr($tdata, 10, 10);
			// $in_date = date('Y-m-d', strtotime($in_date_raw));
			// $in_time_hour = substr($tdata, 21, 2);
			// $in_time_min = substr($tdata, 24, 2);
			// $in_time_sec = substr($tdata, 27, 2);
			// $in_time = $in_time_hour.':'.$in_time_min.':'.$in_time_sec;

			// echo $tdata;
			// echo '<br />';
			// echo $emp_id;
			// echo '<br />';
			// echo $in_date_raw;
			// echo '<br />';
			// echo $in_date;
			// echo '<br />';
			// echo $in_time;
			// echo '<br />';
			// exit;
			if($in_date != '1900-01-01' && $in_date != '1970-01-01' && $in_date != '0000-00-00'){
				if($emp_id != '') {
					// echo $emp_id;
					// echo '<br />';
					$result = $this->getEmployees_dat($emp_id);
					// echo '<pre>';
					// print_r($result);
					// exit;
					if(isset($result['emp_code'])){
						$employees[] = array(
							'employee_id' => $result['emp_code'],
							'emp_name' => $result['name'],
							'card_id' => $result['card_number'],
							'department' => $result['department'],
							'unit' => $result['unit'],
							'unit_id' => $result['unit_id'],
							'group' => '',//$result['group'],
							'in_time' => $in_time,
							'device_id' => 0,
							'punch_date' => $in_date,
							'download_date' => date('Y-m-d'),
							'log_id' => 0,
							'is_new_employee' => 0,
							'fdate' => date('Y-m-d H:i:s', strtotime($in_date.' '.$in_time))
						);
					} else {
						
						$employees[] = array(
							'employee_id' => $emp_id,
							'emp_name' => '',
							'card_id' => $emp_id,
							'department' => '',
							'unit' => '',
							'unit_id' => 0,
							'group' => '',//$result['group'],
							'in_time' => $in_time,
							'device_id' => 0,
							'punch_date' => $in_date,
							'download_date' => date('Y-m-d'),
							'log_id' => 0,
							'is_new_employee' => 1,
							'fdate' => date('Y-m-d H:i:s', strtotime($in_date.' '.$in_time))
						);
					}
				}
			}
		}

		usort($employees, array($this, "sortByOrder"));
		$o_emp = array();
		foreach ($employees as $ekey => $evalue) {
			$employees_final[] = $evalue;
		}

		// echo '<pre>';
		// print_r($employees_final);
		// exit;
		$filter_unit = 0;
		if(isset($employees_final) && count($employees_final) > 0){
			foreach ($employees_final as $fkey => $employee) {
				$exist = $this->getorderhistory($employee);
				if($exist == 0){
					$mystring = $employee['in_time'];
					$findme   = ':';
					//echo '<pre>';print_r($mystring);
					$pos = strpos($mystring, $findme);
					/*if($pos !== false){
						$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 3, 2). ":" . substr($employee['in_time'], 6, 2);
					} else {
						$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 2, 2). ":" . substr($employee['in_time'], 4, 2);
					}*/
					//$employee['in_time'] = $in_times;
					//echo '<pre>';print_r($in_times);
					// exit;
					$this->insert_attendance_data($employee);
				}
			}
		}
		//exit;
		//$unprocessed_dates = $this->db->query("SELECT a.`id`, a.`emp_id`, a.`punch_date`, a.`punch_time`, a.`device_id` FROM `oc_attendance` a LEFT JOIN `oc_employee` e ON(e.`emp_code` = a.`emp_id`) WHERE e.`status` = '1' AND a.`status` = '0' AND a.`punch_date` >= '2017-06-26' AND a.`log_id` = '0' ORDER BY a.`punch_date` ASC ");
		$current_date = date('Y-m-d');
		$filter_date_start = date('Y-m-d', strtotime($current_date .' -100 day'));
		$unprocessed_dates = $this->db->query("SELECT  a.`id`, a.`emp_id`, a.`punch_date`, a.`punch_time`, a.`device_id` FROM `oc_attendance` a LEFT JOIN `oc_employee` e ON(e.`emp_code` = a.`emp_id`) WHERE (e.`dol` = '0000-00-00' OR e.`dol` > '".$filter_date_start."') AND a.`punch_date` >= '".$filter_date_start."' AND a.`status` = '0' AND a.`log_id` = '0' ORDER BY a.`punch_date` ASC, a.`punch_time` ASC ");
		// echo '<pre>';
		// print_r($unprocessed_dates);
		// exit;
		$current_processed_data = array();
		$current_processed_data_date = array();
		$data = array();
		foreach($unprocessed_dates->rows as $dkey => $dvalue){
			$filter_date_start = $dvalue['punch_date'];
			$current_processed_data[$dvalue['id']] = $dvalue['id'];
			$current_processed_data_date[$dvalue['id']] = $filter_date_start;
			$data['filter_name_id'] = $dvalue['emp_id']; 
			$rvalue = $this->getempdata($dvalue['emp_id'], $dvalue['punch_date']);
			$device_id = $dvalue['device_id'];
			$is_processed_status = $this->db->query("SELECT `status` FROM `oc_attendance` WHERE `id` = '".$dvalue['id']."' ")->row['status'];
			if(isset($rvalue['name']) && $rvalue['name'] != '' && $is_processed_status == '0'){
				$this->log->write('---start----');

				$emp_name = $rvalue['name'];
				$department = $rvalue['department'];
				$unit = $rvalue['unit'];
				$group = '';

				$day_date = date('j', strtotime($filter_date_start));
				$day = date('j', strtotime($filter_date_start));
				$month = date('n', strtotime($filter_date_start));
				$year = date('Y', strtotime($filter_date_start));

				$punch_time_exp = explode(':', $dvalue['punch_time']);
				if($punch_time_exp[0] >= '00' && $punch_time_exp[0] <= '04'){
					$filter_date_start = date('Y-m-d', strtotime($filter_date_start .' -1 day'));
					$day_date = date('j', strtotime($filter_date_start));
					$day = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));				
				}

				$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
				$shift_schedule = $this->db->query($update3)->row;
				if(!isset($shift_schedule[$day_date])){
					$shift_schedule[$day_date] = 'S_1';	
				}
				$schedule_raw = explode('_', $shift_schedule[$day_date]);
				if(!isset($schedule_raw[2])){
					$schedule_raw[2] = 1;
				}
				
				$transaction_id = 0;
				if($schedule_raw[0] == 'S'){
					$shift_data = $this->getshiftdata($schedule_raw[1]);
					if(!isset($shift_data['shift_id'])){
						$shift_data = $this->getshiftdata('1');
					}
					
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
					$shift_code = $shift_data['shift_code'];
					$shift_id = $shift_data['shift_id'];

					$act_intime = '00:00:00';
					$act_outtime = '00:00:00';
					$act_in_punch_date = $filter_date_start;
					$act_out_punch_date = $filter_date_start;
					
					$trans_exist_sql = "SELECT `transaction_id`, `act_intime`, `date`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = $this->db->query($trans_exist_sql);
					
					if($trans_exist->num_rows == 0){
						$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
						if(isset($act_intimes['punch_time'])) {
							$act_intime = $act_intimes['punch_time'];
							$act_in_punch_date = $act_intimes['punch_date'];
						}
					} else {
						$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
						if(isset($act_intimes['punch_time'])) {
							$act_intime = $act_intimes['punch_time'];
							$act_in_punch_date = $act_intimes['punch_date'];
						}
					}
					
					$act_outtimes = $this->getrawattendance_out_time($rvalue['emp_code'], $filter_date_start, $act_intime, $act_in_punch_date);
					if(isset($act_outtimes['punch_time'])) {
						$act_outtime = $act_outtimes['punch_time'];
						$act_out_punch_date = $act_outtimes['punch_date'];
					}
					
					if($act_intime == $act_outtime){
						$act_outtime = '00:00:00';
					}

					$abnormal_status = 0;
					$first_half = 0;
					$second_half = 0;
					$late_time = '00:00:00';
					$early_time = '00:00:00';
					$working_time = '00:00:00';
					$late_mark = 0;
					$early_mark = 0;
					$shift_early = 0;
					if($act_intime != '00:00:00'){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
						if($since_start->h > 12){
							$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
						}
						if($since_start->h > 0){
							$late_hour = $since_start->h;
							$late_min = $since_start->i;
							$late_sec = $since_start->s;
						} else {
							$late_hour = $since_start->h;
							$late_min = $since_start->i;
							$late_sec = $since_start->s;
						}
						$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
						if($since_start->invert == 1){
							$shift_early = 1;
							$late_time = '00:00:00';
						} else {
							$late_mark = 0;
						}
						$late_time = '00:00:00';
						
						if($act_outtime != '00:00:00'){
							if($shift_early == 1){
								if(strtotime($act_outtime) < strtotime($shift_intime)){
									$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								} else {
									$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								}
							} else {
								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
							}
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
							$first_half = 1;
							$second_half = 1;
							$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
						} else {
							$first_half = 1;
							$second_half = 1;
						}
					} else {
						$first_half = 0;
						$second_half = 0;
					}

					$early_time = '00:00:00';
					if($abnormal_status == 0){ //if abnormal status is zero calculate further 
						$early_time = '00:00:00';
						if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
							$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
							if($since_start->h > 12){
								$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
							}
							$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
							if($since_start->invert == 0){
								$early_time = '00:00:00';
							}
							$early_time = '00:00:00';
						}					
					} else {
						$first_half = 0;
						$second_half = 0;
					}
					
					if($working_time == '00:00:00'){
						$late_time = '00:00:00';
						$early_time = '00:00:00';
					}

					if($first_half == 1 && $second_half == 1){
						$present_status = 1;
						$absent_status = 0;
					} elseif($first_half == 1 && $second_half == 0){
						$present_status = 0.5;
						$absent_status = 0.5;
					} elseif($first_half == 0 && $second_half == 1){
						$present_status = 0.5;
						$absent_status = 0.5;
					} else {
						$present_status = 0;
						$absent_status = 1;
					}

					$day = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));
					if($trans_exist->num_rows > 0){
						$transaction_id = $trans_exist->row['transaction_id'];
						$leave_status = $trans_exist->row['leave_status'];
						$manual_status = $trans_exist->row['manual_status'];
						if($leave_status == 0 && $manual_status == 0){
							$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
							$this->db->query($sql);
							$this->log->write($sql);
						} else {
							$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
							$this->db->query($sql);
							$this->log->write($sql);
						}
					}
				} elseif ($schedule_raw[0] == 'W') {
					$shift_data = $this->getshiftdata($schedule_raw[2]);
					if(!isset($shift_data['shift_id'])){
						$shift_data = $this->getshiftdata('1');
					}
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
					$shift_code = $shift_data['shift_code'];
					$shift_id = $shift_data['shift_id'];

					$act_intime = '00:00:00';
					$act_outtime = '00:00:00';
					$act_in_punch_date = $filter_date_start;
					$act_out_punch_date = $filter_date_start;
					
					$trans_exist_sql = "SELECT `transaction_id`, `act_intime`, `date`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = $this->db->query($trans_exist_sql);
					
					if($trans_exist->num_rows == 0){
						$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
						if(isset($act_intimes['punch_time'])) {
							$act_intime = $act_intimes['punch_time'];
							$act_in_punch_date = $act_intimes['punch_date'];
						}
					} else {
						$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
						if(isset($act_intimes['punch_time'])) {
							$act_intime = $act_intimes['punch_time'];
							$act_in_punch_date = $act_intimes['punch_date'];
						}
					}
					
					$act_outtimes = $this->getrawattendance_out_time($rvalue['emp_code'], $filter_date_start, $act_intime, $act_in_punch_date);
					if(isset($act_outtimes['punch_time'])) {
						$act_outtime = $act_outtimes['punch_time'];
						$act_out_punch_date = $act_outtimes['punch_date'];
					}
					
					if($act_intime == $act_outtime){
						$act_outtime = '00:00:00';
					}

					$abnormal_status = 0;
					$first_half = 0;
					$second_half = 0;
					$late_time = '00:00:00';
					$early_time = '00:00:00';
					$working_time = '00:00:00';
					$late_mark = 0;
					$early_mark = 0;
					$shift_early = 0;
					if($act_intime != '00:00:00'){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
						if($since_start->h > 12){
							$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
						}
						if($since_start->h > 0){
							$late_hour = $since_start->h;
							$late_min = $since_start->i;
							$late_sec = $since_start->s;
						} else {
							$late_hour = $since_start->h;
							$late_min = $since_start->i;
							$late_sec = $since_start->s;
						}
						$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
						if($since_start->invert == 1){
							$shift_early = 1;
							$late_time = '00:00:00';
						} else {
							$late_mark = 0;
						}
						$late_time = '00:00:00';
						
						if($act_outtime != '00:00:00'){
							if($shift_early == 1){
								if(strtotime($act_outtime) < strtotime($shift_intime)){
									$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								} else {
									$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								}
							} else {
								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
							}
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
							$first_half = 1;
							$second_half = 1;
							$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
						} else {
							$first_half = 1;
							$second_half = 1;
						}
					} else {
						$first_half = 0;
						$second_half = 0;
					}

					$early_time = '00:00:00';
					if($abnormal_status == 0){ //if abnormal status is zero calculate further 
						$early_time = '00:00:00';
						if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
							$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
							if($since_start->h > 12){
								$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
							}
							$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
							if($since_start->invert == 0){
								$early_time = '00:00:00';
							}
							$early_time = '00:00:00';
						}					
					} else {
						$first_half = 0;
						$second_half = 0;
					}
					
					if($working_time == '00:00:00'){
						$late_time = '00:00:00';
						$early_time = '00:00:00';
					}

					if($first_half == 1 && $second_half == 1){
						$present_status = 1;
						$absent_status = 0;
					} elseif($first_half == 1 && $second_half == 0){
						$present_status = 0.5;
						$absent_status = 0.5;
					} elseif($first_half == 0 && $second_half == 1){
						$present_status = 0.5;
						$absent_status = 0.5;
					} else {
						$present_status = 0;
						$absent_status = 1;
					}

					$day = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));
					if($trans_exist->num_rows > 0){
						$transaction_id = $trans_exist->row['transaction_id'];
						$leave_status = $trans_exist->row['leave_status'];
						$manual_status = $trans_exist->row['manual_status'];
						if($leave_status == 0 && $manual_status == 0){
							$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `halfday_status` = '0', `device_id` = '".$device_id."', `batch_id` = '".$batch_id."', `late_mark` = '".$late_mark."', `early_mark` = '".$early_mark."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
							$this->db->query($sql);
							$this->log->write($sql);
						} else {
							$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
							$this->db->query($sql);
							$this->log->write($sql);
						}
					}
				} elseif ($schedule_raw[0] == 'H') {
					$shift_data = $this->getshiftdata($schedule_raw[2]);
					if(!isset($shift_data['shift_id'])){
						$shift_data = $this->getshiftdata('1');
					}
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
					$shift_code = $shift_data['shift_code'];
					$shift_id = $shift_data['shift_id'];

					$act_intime = '00:00:00';
					$act_outtime = '00:00:00';
					$act_in_punch_date = $filter_date_start;
					$act_out_punch_date = $filter_date_start;
					
					$trans_exist_sql = "SELECT `transaction_id`, `act_intime`, `date`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = $this->db->query($trans_exist_sql);
					
					if($trans_exist->num_rows == 0){
						$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
						if(isset($act_intimes['punch_time'])) {
							$act_intime = $act_intimes['punch_time'];
							$act_in_punch_date = $act_intimes['punch_date'];
						}
					} else {
						$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
						if(isset($act_intimes['punch_time'])) {
							$act_intime = $act_intimes['punch_time'];
							$act_in_punch_date = $act_intimes['punch_date'];
						}
					}
					
					$act_outtimes = $this->getrawattendance_out_time($rvalue['emp_code'], $filter_date_start, $act_intime, $act_in_punch_date);
					if(isset($act_outtimes['punch_time'])) {
						$act_outtime = $act_outtimes['punch_time'];
						$act_out_punch_date = $act_outtimes['punch_date'];
					}
					
					if($act_intime == $act_outtime){
						$act_outtime = '00:00:00';
					}

					$abnormal_status = 0;
					$first_half = 0;
					$second_half = 0;
					$late_time = '00:00:00';
					$early_time = '00:00:00';
					$working_time = '00:00:00';
					$late_mark = 0;
					$early_mark = 0;
					$shift_early = 0;
					if($act_intime != '00:00:00'){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
						if($since_start->h > 12){
							$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
						}
						if($since_start->h > 0){
							$late_hour = $since_start->h;
							$late_min = $since_start->i;
							$late_sec = $since_start->s;
						} else {
							$late_hour = $since_start->h;
							$late_min = $since_start->i;
							$late_sec = $since_start->s;
						}
						$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
						if($since_start->invert == 1){
							$shift_early = 1;
							$late_time = '00:00:00';
						} else {
							$late_mark = 0;
						}
						$late_time = '00:00:00';
						
						if($act_outtime != '00:00:00'){
							if($shift_early == 1){
								if(strtotime($act_outtime) < strtotime($shift_intime)){
									$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								} else {
									$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								}
							} else {
								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
							}
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
							$first_half = 1;
							$second_half = 1;
							$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
						} else {
							$first_half = 1;
							$second_half = 1;
						}
					} else {
						$first_half = 0;
						$second_half = 0;
					}

					$early_time = '00:00:00';
					if($abnormal_status == 0){ //if abnormal status is zero calculate further 
						$early_time = '00:00:00';
						if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
							$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
							if($since_start->h > 12){
								$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
							}
							$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
							if($since_start->invert == 0){
								$early_time = '00:00:00';
							}
							$early_time = '00:00:00';
						}					
					} else {
						$first_half = 0;
						$second_half = 0;
					}

					if($first_half == 1 && $second_half == 1){
						$present_status = 1;
						$absent_status = 0;
					} elseif($first_half == 1 && $second_half == 0){
						$present_status = 0.5;
						$absent_status = 0.5;
					} elseif($first_half == 0 && $second_half == 1){
						$present_status = 0.5;
						$absent_status = 0.5;
					} else {
						$present_status = 0;
						$absent_status = 1;
					}

					if($working_time == '00:00:00'){
						$late_time = '00:00:00';
						$early_time = '00:00:00';
					}

					$day = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));
					if($trans_exist->num_rows > 0){
						$transaction_id = $trans_exist->row['transaction_id'];
						$leave_status = $trans_exist->row['leave_status'];
						$manual_status = $trans_exist->row['manual_status'];
						if($leave_status == 0 && $manual_status == 0){
							$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
							$this->db->query($sql);
							$this->log->write($sql);
						} else {
							$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
							$this->db->query($sql);
							$this->log->write($sql);
						}
					}
				} elseif($schedule_raw[0] == 'HD'){
					$shift_data = $this->getshiftdata($schedule_raw[1]);
					if(!isset($shift_data['shift_id'])){
						$shift_data = $this->getshiftdata('1');
					}
					
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
					$shift_code = $shift_data['shift_code'];
					$shift_id = $shift_data['shift_id'];
					
					$act_intime = '00:00:00';
					$act_outtime = '00:00:00';
					$act_in_punch_date = $filter_date_start;
					$act_out_punch_date = $filter_date_start;
					
					$trans_exist_sql = "SELECT `transaction_id`, `act_intime`, `date`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = $this->db->query($trans_exist_sql);
					
					if($trans_exist->num_rows == 0){
						$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
						if(isset($act_intimes['punch_time'])) {
							$act_intime = $act_intimes['punch_time'];
							$act_in_punch_date = $act_intimes['punch_date'];
						}
					} else {
						$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start);
						if(isset($act_intimes['punch_time'])) {
							$act_intime = $act_intimes['punch_time'];
							$act_in_punch_date = $act_intimes['punch_date'];
						}
					}
					
					$act_outtimes = $this->getrawattendance_out_time($rvalue['emp_code'], $filter_date_start, $act_intime, $act_in_punch_date);
					if(isset($act_outtimes['punch_time'])) {
						$act_outtime = $act_outtimes['punch_time'];
						$act_out_punch_date = $act_outtimes['punch_date'];
					}
					
					if($act_intime == $act_outtime){
						$act_outtime = '00:00:00';
					}

					$abnormal_status = 0;
					$first_half = 0;
					$second_half = 0;
					$late_time = '00:00:00';
					$early_time = '00:00:00';
					$working_time = '00:00:00';
					$late_mark = 0;
					$early_mark = 0;
					$shift_early = 0;
					if($act_intime != '00:00:00'){
						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
						if($since_start->h > 12){
							$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
						}
						if($since_start->h > 0){
							$late_hour = $since_start->h;
							$late_min = $since_start->i;
							$late_sec = $since_start->s;
						} else {
							$late_hour = $since_start->h;
							$late_min = $since_start->i;
							$late_sec = $since_start->s;
						}
						$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
						if($since_start->invert == 1){
							$shift_early = 1;
							$late_time = '00:00:00';
						} else {
							$late_mark = 0;
						}
						$late_time = '00:00:00';
						
						if($act_outtime != '00:00:00'){
							if($shift_early == 1){
								if(strtotime($act_outtime) < strtotime($shift_intime)){
									$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								} else {
									$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								}
							} else {
								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
							}
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
							$first_half = 1;
							$second_half = 1;
							$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
						} else {
							$first_half = 1;
							$second_half = 1;
						}
					} else {
						$first_half = 0;
						$second_half = 0;
					}

					$early_time = '00:00:00';
					if($abnormal_status == 0){ //if abnormal status is zero calculate further 
						$early_time = '00:00:00';
						if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
							$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
							if($since_start->h > 12){
								$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
							}
							$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
							if($since_start->invert == 0){
								$early_time = '00:00:00';
							}
							$early_time = '00:00:00';
						}					
					} else {
						$first_half = 0;
						$second_half = 0;
					}
					
					if($first_half == 1 && $second_half == 1){
						$present_status = 1;
						$absent_status = 0;
					} elseif($first_half == 1 && $second_half == 0){
						$present_status = 0.5;
						$absent_status = 0.5;
					} elseif($first_half == 0 && $second_half == 1){
						$present_status = 0.5;
						$absent_status = 0.5;
					} else {
						$present_status = 0;
						$absent_status = 1;
					}

					if($working_time == '00:00:00'){
						$late_time = '00:00:00';
						$early_time = '00:00:00';
					}

					$day = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));
					if($trans_exist->num_rows > 0){
						$transaction_id = $trans_exist->row['transaction_id'];
						$leave_status = $trans_exist->row['leave_status'];
						$manual_status = $trans_exist->row['manual_status'];
						if($leave_status == 0 && $manual_status == 0){
							$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
							$this->db->query($sql);
							$this->log->write($sql);
						} else {
							$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
							$this->db->query($sql);
							$this->log->write($sql);
						}
					}
				}

				if($act_in_punch_date == $act_out_punch_date) {
					if(($act_intime != '00:00:00') && ($act_outtime != '00:00:00')){
						$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
						$this->db->query($update);
						$this->log->write($update);
					} elseif(($act_intime != '00:00:00') && ($act_outtime == '00:00:00')) {
						$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` = '".$act_intime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
						$this->db->query($update);
						$this->log->write($update);
					} elseif(($act_intime == '00:00:00') && ($act_outtime != '00:00:00')) {
						$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` = '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
						$this->db->query($update);
						$this->log->write($update);
					}
				} else {
					$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
					$this->db->query($update);
					$this->log->write($update);
					
					$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
					$this->db->query($update);
					$this->log->write($update);
				}
			}
		}
		//echo 'Done';exit;
		$this->session->data['success'] = 'You are done with process';
		$this->redirect($this->url->link('tool/backup', 'token=' . $this->session->data['token'], 'SSL'));
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}
	
	public function getEmployees_dat($emp_code) {
		$sql = "SELECT `division`, `region`, `unit`, `doj`, `emp_code`, `name`, `department`, `unit_id`, `card_number` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getempdata($emp_code, $punch_date) {
		$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name`, `company`, `company_id`, `contractor`, `contractor_id`, `contractor_code`, `category`, `category_id`, `designation_id`, `designation` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' AND (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$punch_date."') ";
		//$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name`, `company`, `company_id` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function insert_attendance_data($data) {
		$this->db->query("INSERT INTO `oc_attendance` SET `emp_id` = '".$data['employee_id']."', `card_id` = '".$data['card_id']."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `device_id` = '".$data['device_id']."', `status` = '0', `download_date` = '".$data['download_date']."', `log_id` = '".$data['log_id']."', `unit_id` = '".$data['unit_id']."', `is_new_employee` = '".$data['is_new_employee']."' ");
	}

	public function getshiftdata($shift_id) {
		$query = $this->db->query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ");
		if($query->num_rows > 0){
			return $query->row;
		} else {
			return array();
		}
	}

	public function getorderhistory($data){
		$sql = "SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$data['punch_date']."' AND `punch_time` = '".$data['in_time']."' AND `emp_id` = '".$data['employee_id']."' ";
		$query = $this->db->query($sql);
		if($query->num_rows > 0){
			return 1;
		} else {
			return 0;
		}
	}
	
	public function getrawattendance_in_time($emp_id, $punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		$query = $this->db->query("SELECT * FROM oc_attendance WHERE REPLACE(emp_id, ' ','') = '".$emp_id."' AND ((`punch_date` = '".$punch_date."' AND `punch_time` >= '05:00:00') OR (`punch_date` = '".$future_date."' AND `punch_time` <= '04:00:00')) ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ");
		$array = $query->row;
		return $array;
	}

	public function getrawattendance_out_time($emp_id, $punch_date, $act_intime, $act_punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		$query = $this->db->query("SELECT * FROM oc_attendance WHERE REPLACE(emp_id, ' ','') = '".$emp_id."' AND ((`punch_date` = '".$punch_date."' AND `punch_time` >= '05:00:00') OR (`punch_date` = '".$future_date."' AND `punch_time` <= '04:00:00')) ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC");
		$act_outtime = array();
		if($query->num_rows > 0){
			//echo '<pre>';
			//print_r($act_intime);

			$first_punch = $query->rows['0'];
			$array = $query->rows;
			$comp_array = array();
			$hour_array = array();
			
			//echo '<pre>';
			//print_r($array);

			foreach ($array as $akey => $avalue) {
				$start_date = new DateTime($act_punch_date.' '.$act_intime);
				$since_start = $start_date->diff(new DateTime($avalue['punch_date'].' '.$avalue['punch_time']));
				
				//echo '<pre>';
				//print_r($since_start);

				if($since_start->d == 0){
					$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					$hour_array[] = $since_start->h;
					$act_array[] = $avalue;
				}
			}

			// echo '<pre>';
			// print_r($hour_array);

			// echo '<pre>';
			// print_r($comp_array);

			foreach ($hour_array as $ckey => $cvalue) {
				if($cvalue > 20){
					unset($hour_array[$ckey]);
				}
			}

			// echo '<pre>';
			// print_r($hour_array);
			
			$act_outtimes = '0';
			if($hour_array){
				$act_outtimes = max($hour_array);
			}

			// echo '<pre>';
			// print_r($act_outtimes);

			foreach ($hour_array as $akey => $avalue) {
				if($avalue == $act_outtimes){
					$act_outtime = $act_array[$akey];
				}
			}
		}
		// echo '<pre>';
		// print_r($act_outtime);
		// exit;		
		return $act_outtime;
	}
}
?>