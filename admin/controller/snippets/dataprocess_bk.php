<?php    
class ControllerSnippetsDataprocess extends Controller { 
	private $error = array();

	public function index() {
		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');
		$this->load->model('catalog/employee');		
		
		$devices = $this->db->query("SELECT * FROM `oc_devices`")->rows;

		$batch_ids = $this->db->query("SELECT `batch_id` FROM `oc_transaction` ORDER BY `batch_id` DESC LIMIT 1");
		if($batch_ids->num_rows > 0){
			$batch_id = $batch_ids->row['batch_id'] + 1;
		} else {
			$batch_id = 1;
		}

		foreach($devices as $dkeys => $db_device_id){
			
			$device_last_dates = $this->db->query("SELECT `date` FROM `oc_transaction` WHERE `device_id` = '".$db_device_id['device_id']."' ORDER BY `date` DESC LIMIT 1");
			if($device_last_dates->num_rows > 0){
				$device_last_date = $device_last_dates->row['date'];
			} else {
				$device_last_date = '2017-03-31';
			}
			$next_date = date('Y-m-d', strtotime($device_last_date . ' +1 day'));
			//$today_date = date('Y-m-d');
			$today_date = '2017-04-01';

			$day = array();
			$days = $this->GetDays($next_date, $today_date);
	        foreach ($days as $dkey => $dvalue) {
	        	$dates = explode('-', $dvalue);
	        	$day[$dkey]['day'] = $dates[2];
	        	$day[$dkey]['date'] = $dvalue;
	        }
	        // echo '<pre>';
	        // print_r($day);
	        // exit;
	        foreach($day as $dkeys => $dvalues){
				$filter_date_start = $dvalues['date'];
				// $day = date('d', strtotime($filter_date_start));
				// $month = date('m', strtotime($filter_date_start));
				// $year = date('Y', strtotime($filter_date_start));

				// $serverName = "HP\SQLEXPRESS";
				// $connectionInfo = array("Database"=>"eSSLSmartOffice", "UID"=>"sa", "PWD"=>"1234");
				// $conn = sqlsrv_connect($serverName, $connectionInfo);
				// if($conn) {
				//      //echo "Connection established.<br />";
				// }else{
				//      //echo "Connection could not be established.<br />";
				//      $dat['connection'] = 'Connection could not be established';
				//      //echo '<pre>';
				//      //print_r(sqlsrv_errors());
				//      //exit;
				//      //die( print_r( sqlsrv_errors(), true));
				// }
				// $table_name = "dbo.DeviceLogs_".date('n').'_'.date('Y');
				// $sql = "SELECT * FROM ".$table_name." WHERE (DATEPART(yy, LogDate) = '".$year."' AND DATEPART(mm, LogDate) = '".$month."' AND DATEPART(dd, LogDate) = '".$day."')";
				// $params = array();
				// $options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
				// $stmt = sqlsrv_query($conn, $sql, $params, $options);
				// if( $stmt === false) {
				//    //echo'<pre>';
				//    //print_r(sqlsrv_errors());
				//    //exit;
				// }
				
				// $exp_datas = array();
				// while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
				//   //echo '<pre>';
				//   //print_r($row);
				//   //exit;
				//   	$user_id = sprintf('%04d', $row['UserId']);
				//   	$device_id = sprintf('%02d', $row['DeviceId']);
				//   	$dates = $row['LogDate'];
				//   	$date = $dates->format("Y-m-d");
				//   	$times = $row['LogDate'];
				//   	$time = $times->format("H:i:s");
				//  	$exp_datas[] = array(
				//  		'user_id' => $user_id,
				//  		'date' => $date,
				//  		'time' => $time,
				//  		'device_id' => $device_id,
				//  	);
				// }

				//echo '<pre>';
				//print_r($data);
				//exit;
				$exp_datas = array();
				if($db_device_id['device_id'] == '1'){
					$exp_datas['0']['emp_name'] = 'Aaron Fargose';
					$exp_datas['0']['user_id'] = '1001';
					$exp_datas['0']['date'] = '2017-04-01';
					$exp_datas['0']['time'] = '09:59:00';
					$exp_datas['0']['device_id'] = '1';

					$exp_datas['1']['emp_name'] = 'Aaron Fargose';
					$exp_datas['1']['user_id'] = '1001';
					$exp_datas['1']['date'] = '2017-04-01';
					$exp_datas['1']['time'] = '19:45:00';
					$exp_datas['1']['device_id'] = '1';


					$exp_datas['2']['emp_name'] = 'Sophia Fargose';
					$exp_datas['2']['user_id'] = '1003';
					$exp_datas['2']['date'] = '2017-04-01';
					$exp_datas['2']['time'] = '10:45:00';
					$exp_datas['2']['device_id'] = '1';

					$exp_datas['3']['emp_name'] = 'Sophia Fargose';
					$exp_datas['3']['user_id'] = '1003';
					$exp_datas['3']['date'] = '2017-04-01';
					$exp_datas['3']['time'] = '20:50:00';
					$exp_datas['3']['device_id'] = '1';
				} else {
					$exp_datas['0']['emp_name'] = 'Eric Fargose';
					$exp_datas['0']['user_id'] = '1002';
					$exp_datas['0']['time'] = '09:15:15';
					$exp_datas['0']['date'] = '2017-04-01';
					$exp_datas['0']['device_id'] = '2';

					$exp_datas['1']['emp_name'] = 'Sneha Fargose';
					$exp_datas['1']['user_id'] = '1000';
					$exp_datas['1']['time'] = '08:45:15';
					$exp_datas['1']['date'] = '2017-04-01';
					$exp_datas['1']['device_id'] = '2';

					$exp_datas['2']['emp_name'] = 'Eric Fargose';
					$exp_datas['2']['user_id'] = '1002';
					$exp_datas['2']['time'] = '17:45:15';
					$exp_datas['2']['date'] = '2017-04-01';
					$exp_datas['2']['device_id'] = '2';
				}
				

				$employees = array();
				$employees_final = array();

				foreach($exp_datas as $data) {
					//$tdata = trim($data);
					$emp_id  = $data['user_id'];//substr($tdata, 0, 5);
					$in_time = $data['time'];//substr($tdata, 6, 5);
					if($emp_id != '') {
						$result = $this->model_transaction_transaction->getEmployees_dat($emp_id);
						if(isset($result['emp_code'])){
							$employees[] = array(
								'employee_id' => $result['emp_code'],
								'emp_name' => $result['name'],
								'card_id' => $result['card_number'],
								'department' => $result['department'],
								'unit' => $result['unit'],
								'group' => $result['group'],
								'in_time' => $in_time,
								'device_id' => $data['device_id'],
								'punch_date' => $filter_date_start,
								'fdate' => date('Y-m-d H:i:s', strtotime($filter_date_start.' '.$in_time))
							);
						}
					}
				}

				usort($employees, array($this, "sortByOrder"));
				
				$o_emp = array();
				foreach ($employees as $ekey => $evalue) {
					//if(!isset($o_emp[$evalue['employee_id']])){
						//$o_emp[$evalue['employee_id']] = $evalue['employee_id'];
						$employees_final[] = $evalue;
					//} 
				}
				
				if (isset($this->request->get['unit'])) {
					$filter_unit = $this->request->get['unit'];
				} else {
					$filter_unit = 0;
				}

				if(isset($employees_final) && count($employees_final) > 0){
					//$this->model_transaction_transaction->deleteorderhistory($filter_date_start);
					//$this->model_transaction_transaction->deleteorderhistory($next_date);
					foreach ($employees_final as $fkey => $employee) {
						$exist = $this->model_transaction_transaction->getorderhistory($employee);
						if($exist == 0){
							$mystring = $employee['in_time'];
							$findme   = ':';
							$pos = strpos($mystring, $findme);
							if($pos !== false){
								$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 3, 2). ":" . substr($employee['in_time'], 6, 2);
							} else {
								$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 2, 2). ":" . substr($employee['in_time'], 4, 2);
							}
							if($in_times != ':'){
								$in_time = date("h:i:s", strtotime($in_times));
							} else {
								$in_time = '';
							}
							$employee['in_time'] = $in_times;
							// echo '<pre>';
							// print_r($employee);
							// exit;
							$this->model_transaction_transaction->insert_attendance_data($employee);
						}
					}
				}
				$response = array();
				$is_exist = $this->model_report_common_report->getattendance_exist($filter_date_start);
				if($is_exist == 1){
					$data = array();
					$data['unit'] = $filter_unit;
					$data['device_id'] = $db_device_id['device_id'];
					$results = $this->model_report_common_report->getemployees($data);
					foreach ($results as $rkey => $rvalue) {
						//$rvalue['emp_code'] = '1003';
						$rvaluess = $this->model_transaction_transaction->getrawattendance_group_date_custom($rvalue['emp_code'], $filter_date_start);
						// echo '<pre>';
						// print_r($rvaluess);
						// exit;
						if($rvaluess) {
							$emp_data = $this->model_transaction_transaction->getempdata($rvaluess['emp_id']);
							$device_id = $rvaluess['device_id'];
							if(isset($emp_data['name']) && $emp_data['name'] != ''){
								
								$emp_name = $emp_data['name'];
								$department = $emp_data['department'];
								$unit = $emp_data['unit'];
								$group = $emp_data['group'];

								$day_date = date('j', strtotime($filter_date_start));
								$day = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));

								$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvaluess['emp_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
								//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvaluess['emp_id']."' ";
								$shift_schedule = $this->db->query($update3)->row;
								$schedule_raw = explode('_', $shift_schedule[$day_date]);
								if(!isset($schedule_raw[2])){
									$schedule_raw[2] = 1;
								}
								// echo '<pre>';
								// print_r($schedule_raw);
								// exit;
								if($schedule_raw[0] == 'S'){
									$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->model_transaction_transaction->getshiftdata('1');
									}
									if(isset($shift_data['shift_id'])){
										$shift_intime = $shift_data['in_time'];
										$shift_outtime = $shift_data['out_time'];

										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$act_in_punch_date = $filter_date_start;
										$act_out_punch_date = $filter_date_start;
										
										$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
										$trans_exist = $this->db->query($trans_exist_sql);
										if($trans_exist->num_rows == 0){ 
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
											$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
											if(isset($act_outtimes['punch_time'])) {
												$act_outtime = $act_outtimes['punch_time'];
												$act_out_punch_date = $act_outtimes['punch_date'];
											}
										} else {
											if($trans_exist->row['act_intime'] == '00:00:00'){
												$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
												$new_data_exist = $this->db->query($new_data_sql);
												if($new_data_exist->num_rows > 0){	
													$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
													if(isset($act_intimes['punch_time'])) {
														$act_intime = $act_intimes['punch_time'];
														$act_in_punch_date = $act_intimes['punch_date'];
													}
												}	
											} else {
												$act_intime = $trans_exist->row['act_intime'];
												$act_in_punch_date = $trans_exist->row['date'];	
											}
										
											$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
											$new_data_exist = $this->db->query($new_data_sql);
											if($new_data_exist->num_rows > 0){
												$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
												if(isset($act_outtimes['punch_time'])) {
													$act_outtime = $act_outtimes['punch_time'];
													$act_out_punch_date = $act_outtimes['punch_date'];
												}
											} else {
												$act_outtime = $trans_exist->row['act_outtime'];
												$act_out_punch_date = $trans_exist->row['date_out'];
											}
										}
										if($act_intime == $act_outtime){
											$act_outtime = '00:00:00';
										}
										$abnormal_status = 0;
										$first_half = 0;
										$second_half = 0;
										$late_time = '00:00:00';
										//$act_intime = '09:59:00';
										//$act_outtime = '19:45:00';
										$late_mark = 0;
										$early_mark = 0;
										$shift_early = 0;
										if($act_intime != '00:00:00'){
											$shift_intime_plus_one = Date('H:i:s', strtotime($shift_intime .' +1 hours'));
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											if($since_start->h > 0){
												$late_hour = $since_start->h - 1;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											} else {
												$late_hour = $since_start->h;
												$late_min = 0;
												$late_sec = 0;
											}
											$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
											if($since_start->invert == 1){
												$shift_early = 1;
												$late_time = '00:00:00';
											} else {
												if($late_min > 0){
													$late_mark = 1;
												} else {
													$late_mark = 0;
												}
											}
											if($since_start->invert == 1 && $since_start->h >= 6){ //abnormal status condition for morning early comming
												$abnormal_status = 1;
											} else {
												if( (($since_start->h == 2 && $since_start->i >= 1) || $since_start->h > 2) && $since_start->invert == 0){
													$first_half = 0;
													$late_mark = 0;
													if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
														if($shift_early == 1){
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														}
														//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
														if($since_start->h >= 9){
															$second_half = 1;
														} elseif( ($since_start->h == 6 && $since_start->i >= 31) || $since_start->h > 6){
															$second_half = 1;
															$early_mark = 1;
														} elseif($since_start->h >= 3){
															$second_half = 1;
														} else {
															$second_half = 0;
														}
														//if( (($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4) && $since_start->invert == 0){
														// if( $since_start->h >= 3 && $since_start->invert == 0){
														// 	$second_half = 1;
														// } else {
														// 	$second_half = 0;
														// }
													} else {
														$second_half = 0;
													}
												} else {
													if($act_outtime != '00:00:00'){
														if($shift_early == 1){
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														}
														$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
														if($since_start->h >= 9){
															$first_half = 1;
															$second_half = 1;
														} elseif( ($since_start->h == 6 && $since_start->i >= 31) || $since_start->h > 6){
															$early_mark = 1;
															$first_half = 1;
															$second_half = 1;
														} elseif($since_start->h >= 3){
															$first_half = 1;
															$second_half = 0;
														} else {
															$first_half = 0;
															$second_half = 0;
														}
													} else {
														$first_half = 0;
														$second_half = 0;
													}
												}
											}
											//echo 'out';exit;
										} else {
											$first_half = 0;
											$second_half = 0;
										}

										$early_time = '00:00:00';
										if($abnormal_status == 0){ //if abnormal status is zero calculate further 
											$early_time = '00:00:00';
											if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if($since_start->h > 12){
													$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
													$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
												}
												$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
												if($since_start->invert == 0){
													$early_time = '00:00:00';
												}
											}					
										} else {
											$first_half = 0;
											$second_half = 0;
										}

										if($first_half == 1){
											$secs = strtotime($late_time)-strtotime("00:00:00");
											$total_late_early_time = date("H:i:s",strtotime($early_time)+$secs);
											$total_late_early_time_exp = explode(':', $total_late_early_time);
											if( ($total_late_early_time_exp[0] == '01' && $total_late_early_time_exp[1] == '00') || $total_late_early_time_exp[0] > '01'){
												$second_half = 0;
											}
										}

										// $month = 1;
										// $day = 26;
										// $year = 2018;
										if($day <= 25){
											if($month == 1){
												$c_month = 12;
												$c_year = $year - 1;
											} else {
												$c_month = $month - 1;
												$c_year = $year;
											}
										} else {
											$c_month = $month;
											$c_year = $year;
										}

										$from_date = $c_year.'-'.$c_month.'-26';
										$to_date = date('Y-m-d', strtotime($filter_date_start . ' -1 day'));
										$late_datas = $this->db->query("SELECT COUNT(*) as total_late_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `late_mark` = '1' ")->row;
										$early_datas = $this->db->query("SELECT COUNT(*) as total_early_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `late_mark` = '1' ")->row;
										$db_late_mark = $late_datas['total_late_mark'];
										if($late_datas['total_late_mark'] <= 2){
											if($late_mark == 1){
												if($late_datas['total_late_mark'] == 2 && $early_datas['total_early_mark'] == 1){
													$first_half = 0;
												} else {
													//$late_mark_var = 1;
													//$this->db->query("UPDATE `oc_early_late` SET late_mark = (late_mark + ".(int)$late_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
													$db_late_mark = $late_datas['total_late_mark'] + 1;
													if($db_late_mark == 3){
														$first_half = 0;
													}
												}
											}
										} else {
											if($late_mark == 1){
												$first_half = 0;	
											}
										}
										if($early_datas['total_early_mark'] == 0){
											if($early_mark == 1){
												if($db_late_mark == 2){
													//$early_mark_var = 1;
													//$this->db->query("UPDATE `oc_early_late` SET early_mark = (early_mark + ".(int)$early_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
													$second_half = 0;
												} else {
													$second_half = 0;
												}
											}
										} else {
											if($early_mark == 1){
												$second_half = 0;	
											}	
										}

										// echo $first_half;
										// echo '<br/ >';
										// echo $second_half;
										// echo '<br/ >';
										// echo $late_time;
										// echo '<br/ >';
										// echo $early_time;
										// echo '<br/ >';
										// echo $late_mark;
										// echo '<br/ >';
										// echo $early_mark;
										// echo '<br/ >';
										// echo $total_late_early_time;
										// echo '<br/ >';
										// exit;

										$working_time = '00:00:00';
										if($abnormal_status == 0){ //if abnormal status is zero calculate further 
											$working_time = '00:00:00';
											if($act_outtime != '00:00:00'){//for getting working time
												if($shift_early == 1){
													$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											}
										} else {
											$first_half = 0;
											$second_half = 0;
										}

										$abs_stat = 0;
										// if($filter_date_start != $act_in_punch_date){
										// 	$abs_stat = 1;
										// 	$act_in_punch_date = $filter_date_start;
										// }

										// if($abs_stat == 0){
										// 	if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
										// 		$abnormal_status = 1;
										// 	}
										// }

										// if($working_time != '00:00:00'){
										// 	$work_exp = explode(':', $working_time);
										// 	if($work_exp[0] >= 18){
										// 		$abnormal_status = 1;
										// 	}
										// }

										// if($early_time != '00:00:00'){
										// 	$early_exp = explode(':', $early_time);
										// 	if($early_exp[0] >= 6){
										// 		$abnormal_status = 1;
										// 	}
										// }

										// if($late_time != '00:00:00'){
										// 	$late_exp = explode(':', $late_time);
										// 	if($late_exp[0] >= 6){
										// 		$abnormal_status = 1;
										// 	}
										// }

										// if($abnormal_status == 1){
										// 	$first_half = 0;
										// 	$second_half = 0;										
										// 	//$early_time = '00:00:00';
										// 	//$late_time = '00:00:00';
										// 	//$working_time = '00:00:00';										
										// }

										if($first_half == 1 && $second_half == 1){
											$present_status = 1;
											$absent_status = 0;
										} elseif($first_half == 1 && $second_half == 0){
											$present_status = 0.5;
											$absent_status = 0.5;
										} elseif($first_half == 0 && $second_half == 1){
											$present_status = 0.5;
											$absent_status = 0.5;
										} else {
											$present_status = 0;
											$absent_status = 1;
										}

										$day = date('j', strtotime($rvaluess['punch_date']));
										$month = date('n', strtotime($rvaluess['punch_date']));
										$year = date('Y', strtotime($rvaluess['punch_date']));
										//echo 'out';exit;
										if($trans_exist->num_rows == 0){
											if($abs_stat == 1){
												$act_intime = '00:00:00';
												$act_outtime = '00:00:00';
												$abnormal_status = 0;
												$act_out_punch_date = $filter_date_start;
												$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' ";
											} else {
												$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' ";
											}
											$this->db->query($sql);
											$transaction_id = $this->db->getLastId();
										} else {
											if($abs_stat == 1){
												$act_intime = '00:00:00';
												$act_outtime = '00:00:00';
												$abnormal_status = 0;
												$act_out_punch_date = $filter_date_start;
												$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
											} else {
												$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
											}
											$this->db->query($sql);
											$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
										}
										// echo $sql.';';
										// echo '<br />';
										// exit;
										
									}
									//echo 'out1';exit;
								} elseif ($schedule_raw[0] == 'W') {
									$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->model_transaction_transaction->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$act_in_punch_date = $filter_date_start;
									$act_out_punch_date = $filter_date_start;

									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){ 
										$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										if($trans_exist->row['act_intime'] == '00:00:00'){
											$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
											$new_data_exist = $this->db->query($new_data_sql);
											if($new_data_exist->num_rows > 0){	
												$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
												if(isset($act_intimes['punch_time'])) {
													$act_intime = $act_intimes['punch_time'];
													$act_in_punch_date = $act_intimes['punch_date'];
												}
											}	
										} else {
											$act_intime = $trans_exist->row['act_intime'];
											$act_in_punch_date = $trans_exist->row['date'];	
										}
										
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){
											$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
											if(isset($act_outtimes['punch_time'])) {
												$act_outtime = $act_outtimes['punch_time'];
												$act_out_punch_date = $act_outtimes['punch_date'];
											}
										} else {
											$act_outtime = $trans_exist->row['act_outtime'];
											$act_out_punch_date = $trans_exist->row['date_out'];
										}
									}
									if($act_intime == $act_outtime){
										$act_outtime = '00:00:00';
									}
									$abnormal_status = 0;
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									//$act_intime = '09:59:00';
									//$act_outtime = '19:45:00';
									$late_mark = 0;
									$early_mark = 0;
									$shift_early = 0;
									if($act_intime != '00:00:00'){
										$shift_intime_plus_one = Date('H:i:s', strtotime($shift_intime .' +1 hours'));
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										if($since_start->h > 0){
											$late_hour = $since_start->h - 1;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										} else {
											$late_hour = $since_start->h;
											$late_min = 0;
											$late_sec = 0;
										}
										$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
										if($since_start->invert == 1){
											$shift_early = 1;
											$late_time = '00:00:00';
										} else {
											if($late_min > 0){
												$late_mark = 1;
											} else {
												$late_mark = 0;
											}
										}
										if($since_start->invert == 1 && $since_start->h >= 6){ //abnormal status condition for morning early comming
											$abnormal_status = 1;
										} else {
											if( (($since_start->h == 2 && $since_start->i >= 1) || $since_start->h > 2) && $since_start->invert == 0){
												$first_half = 0;
												$late_mark = 0;
												if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
													if($shift_early == 1){
														$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													}
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													if($since_start->h >= 9){
														$second_half = 1;
													} elseif( ($since_start->h == 6 && $since_start->i >= 31) || $since_start->h > 6){
														$second_half = 1;
														$early_mark = 1;
													} elseif($since_start->h >= 3){
														$second_half = 1;
													} else {
														$second_half = 0;
													}
												} else {
													$second_half = 0;
												}
											} else {
												if($act_outtime != '00:00:00'){
													if($shift_early == 1){
														$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													}
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													if($since_start->h >= 9){
														$first_half = 1;
														$second_half = 1;
													} elseif( ($since_start->h == 6 && $since_start->i >= 31) || $since_start->h > 6){
														$early_mark = 1;
														$first_half = 1;
														$second_half = 1;
													} elseif($since_start->h >= 3){
														$first_half = 1;
														$second_half = 0;
													} else {
														$first_half = 0;
														$second_half = 0;
													}
												} else {
													$first_half = 0;
													$second_half = 0;
												}
											}
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
										}					
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									if($first_half == 1){
										$secs = strtotime($late_time)-strtotime("00:00:00");
										$total_late_early_time = date("H:i:s",strtotime($early_time)+$secs);
										$total_late_early_time_exp = explode(':', $total_late_early_time);
										if( ($total_late_early_time_exp[0] == '01' && $total_late_early_time_exp[1] == '00') || $total_late_early_time_exp[0] > '01'){
											$second_half = 0;
										}
									}
									if($day <= 25){
										if($month == 1){
											$c_month = 12;
											$c_year = $year - 1;
										} else {
											$c_month = $month - 1;
											$c_year = $year;
										}
									} else {
										$c_month = $month;
										$c_year = $year;
									}
									$from_date = $c_year.'-'.$c_month.'-26';
									$to_date = date('Y-m-d', strtotime($filter_date_start . ' -1 day'));
									$late_datas = $this->db->query("SELECT COUNT(*) as total_late_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `late_mark` = '1' ")->row;
									$early_datas = $this->db->query("SELECT COUNT(*) as total_early_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `late_mark` = '1' ")->row;
									$db_late_mark = $late_datas['total_late_mark'];
									if($late_datas['total_late_mark'] <= 2){
										if($late_mark == 1){
											if($late_datas['total_late_mark'] == 2 && $early_datas['total_early_mark'] == 1){
												$first_half = 0;
											} else {
												//$late_mark_var = 1;
												//$this->db->query("UPDATE `oc_early_late` SET late_mark = (late_mark + ".(int)$late_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
												$db_late_mark = $late_datas['total_late_mark'] + 1;
												if($db_late_mark == 3){
													$first_half = 0;
												}
											}
										}
									} else {
										if($late_mark == 1){
											$first_half = 0;	
										}
									}
									if($early_datas['total_early_mark'] == 0){
										if($early_mark == 1){
											if($db_late_mark == 2){
												//$early_mark_var = 1;
												//$this->db->query("UPDATE `oc_early_late` SET early_mark = (early_mark + ".(int)$early_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
												$second_half = 0;
											} else {
												$second_half = 0;
											}
										}
									} else {
										if($early_mark == 1){
											$second_half = 0;	
										}	
									}

									$working_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$working_time = '00:00:00';
										if($act_outtime != '00:00:00'){//for getting working time
											if($shift_early == 1){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											}
											//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}
									$abs_stat = 0;
									if($first_half == 1 && $second_half == 1){
										$present_status = 1;
										$absent_status = 0;
									} elseif($first_half == 1 && $second_half == 0){
										$present_status = 0.5;
										$absent_status = 0.5;
									} elseif($first_half == 0 && $second_half == 1){
										$present_status = 0.5;
										$absent_status = 0.5;
									} else {
										$present_status = 0;
										$absent_status = 1;
									}
									$day = date('j', strtotime($rvaluess['punch_date']));
									$month = date('n', strtotime($rvaluess['punch_date']));
									$year = date('Y', strtotime($rvaluess['punch_date']));
									if($trans_exist->num_rows == 0){
										if($abs_stat == 1){
											$act_intime = '00:00:00';
											$act_outtime = '00:00:00';
											$abnormal_status = 0;
											$act_out_punch_date = $filter_date_start;
											$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' ";
										} else {
											$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' ";
										}
										$this->db->query($sql);
										$transaction_id = $this->db->getLastId();
									} else {
										if($abs_stat == 1){
											$act_intime = '00:00:00';
											$act_outtime = '00:00:00';
											$abnormal_status = 0;
											$act_out_punch_date = $filter_date_start;
											$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', halfday_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
										} else {
											$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', halfday_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
										}
										$this->db->query($sql);
										$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
									}
									//echo $sql.';';
									//echo '<br />';
								} elseif ($schedule_raw[0] == 'H') {
									$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->model_transaction_transaction->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$act_in_punch_date = $filter_date_start;
									$act_out_punch_date = $filter_date_start;
									
									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){ 
										$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										if($trans_exist->row['act_intime'] == '00:00:00'){
											$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
											$new_data_exist = $this->db->query($new_data_sql);
											if($new_data_exist->num_rows > 0){	
												$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
												if(isset($act_intimes['punch_time'])) {
													$act_intime = $act_intimes['punch_time'];
													$act_in_punch_date = $act_intimes['punch_date'];
												}
											}	
										} else {
											$act_intime = $trans_exist->row['act_intime'];
											$act_in_punch_date = $trans_exist->row['date'];	
										}


										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){
											$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
											if(isset($act_outtimes['punch_time'])) {
												$act_outtime = $act_outtimes['punch_time'];
												$act_out_punch_date = $act_outtimes['punch_date'];
											}
										} else {
											$act_outtime = $trans_exist->row['act_outtime'];
											$act_out_punch_date = $trans_exist->row['date_out'];
										}
									}
									if($act_intime == $act_outtime){
										$act_outtime = '00:00:00';
									}
									$abnormal_status = 0;
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									//$act_intime = '09:59:00';
									//$act_outtime = '19:45:00';
									$late_mark = 0;
									$early_mark = 0;
									$shift_early = 0;
									if($act_intime != '00:00:00'){
										$shift_intime_plus_one = Date('H:i:s', strtotime($shift_intime .' +1 hours'));
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										if($since_start->h > 0){
											$late_hour = $since_start->h - 1;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										} else {
											$late_hour = $since_start->h;
											$late_min = 0;
											$late_sec = 0;
										}
										$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
										if($since_start->invert == 1){
											$shift_early = 1;
											$late_time = '00:00:00';
										} else {
											if($late_min > 0){
												$late_mark = 1;
											} else {
												$late_mark = 0;
											}
										}
										if($since_start->invert == 1 && $since_start->h >= 6){ //abnormal status condition for morning early comming
											$abnormal_status = 1;
										} else {
											if( (($since_start->h == 2 && $since_start->i >= 1) || $since_start->h > 2) && $since_start->invert == 0){
												$first_half = 0;
												$late_mark = 0;
												if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
													if($shift_early == 1){
														$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													}
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													if($since_start->h >= 9){
														$second_half = 1;
													} elseif( ($since_start->h == 6 && $since_start->i >= 31) || $since_start->h > 6){
														$second_half = 1;
														$early_mark = 1;
													} elseif($since_start->h >= 3){
														$second_half = 1;
													} else {
														$second_half = 0;
													}
												} else {
													$second_half = 0;
												}
											} else {
												if($act_outtime != '00:00:00'){
													if($shift_early == 1){
														$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													}
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													if($since_start->h >= 9){
														$first_half = 1;
														$second_half = 1;
													} elseif( ($since_start->h == 6 && $since_start->i >= 31) || $since_start->h > 6){
														$early_mark = 1;
														$first_half = 1;
														$second_half = 1;
													} elseif($since_start->h >= 3){
														$first_half = 1;
														$second_half = 0;
													} else {
														$first_half = 0;
														$second_half = 0;
													}
												} else {
													$first_half = 0;
													$second_half = 0;
												}
											}
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
										}					
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									if($first_half == 1){
										$secs = strtotime($late_time)-strtotime("00:00:00");
										$total_late_early_time = date("H:i:s",strtotime($early_time)+$secs);
										$total_late_early_time_exp = explode(':', $total_late_early_time);
										if( ($total_late_early_time_exp[0] == '01' && $total_late_early_time_exp[1] == '00') || $total_late_early_time_exp[0] > '01'){
											$second_half = 0;
										}
									}
									if($day <= 25){
										if($month == 1){
											$c_month = 12;
											$c_year = $year - 1;
										} else {
											$c_month = $month - 1;
											$c_year = $year;
										}
									} else {
										$c_month = $month;
										$c_year = $year;
									}
									$from_date = $c_year.'-'.$c_month.'-26';
									$to_date = date('Y-m-d', strtotime($filter_date_start . ' -1 day'));
									$late_datas = $this->db->query("SELECT COUNT(*) as total_late_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `late_mark` = '1' ")->row;
									$early_datas = $this->db->query("SELECT COUNT(*) as total_early_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `late_mark` = '1' ")->row;
									$db_late_mark = $late_datas['total_late_mark'];
									if($late_datas['total_late_mark'] <= 2){
										if($late_mark == 1){
											if($late_datas['total_late_mark'] == 2 && $early_datas['total_early_mark'] == 1){
												$first_half = 0;
											} else {
												//$late_mark_var = 1;
												//$this->db->query("UPDATE `oc_early_late` SET late_mark = (late_mark + ".(int)$late_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
												$db_late_mark = $late_datas['total_late_mark'] + 1;
												if($db_late_mark == 3){
													$first_half = 0;
												}
											}
										}
									} else {
										if($late_mark == 1){
											$first_half = 0;	
										}
									}
									if($early_datas['total_early_mark'] == 0){
										if($early_mark == 1){
											if($db_late_mark == 2){
												//$early_mark_var = 1;
												//$this->db->query("UPDATE `oc_early_late` SET early_mark = (early_mark + ".(int)$early_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
												$second_half = 0;
											} else {
												$second_half = 0;
											}
										}
									} else {
										if($early_mark == 1){
											$second_half = 0;	
										}	
									}
									$working_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$working_time = '00:00:00';
										if($act_outtime != '00:00:00'){//for getting working time
											if($shift_early == 1){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											}
											//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}
									$abs_stat = 0;
									if($first_half == 1 && $second_half == 1){
										$present_status = 1;
										$absent_status = 0;
									} elseif($first_half == 1 && $second_half == 0){
										$present_status = 0.5;
										$absent_status = 0.5;
									} elseif($first_half == 0 && $second_half == 1){
										$present_status = 0.5;
										$absent_status = 0.5;
									} else {
										$present_status = 0;
										$absent_status = 1;
									}
									$day = date('j', strtotime($rvaluess['punch_date']));
									$month = date('n', strtotime($rvaluess['punch_date']));
									$year = date('Y', strtotime($rvaluess['punch_date']));

									if($trans_exist->num_rows == 0){
										if($abs_stat == 1){
											$act_intime = '00:00:00';
											$act_outtime = '00:00:00';
											$abnormal_status = 0;
											$act_out_punch_date = $filter_date_start;
											$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' ";
										} else {
											$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' ";
										}
										$this->db->query($sql);
										$transaction_id = $this->db->getLastId();
									} else {
										if($abs_stat == 1){
											$act_intime = '00:00:00';
											$act_outtime = '00:00:00';
											$abnormal_status = 0;
											$act_out_punch_date = $filter_date_start;
											$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
										} else {
											$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
										}
										$this->db->query($sql);
										$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
									}
									//echo $sql.';';
									//echo '<br />';
								}
								if($act_in_punch_date == $act_out_punch_date) {
									if($act_intime != '00:00:00' && $act_outtime != '00:00:00'){
										$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
										//echo $update;exit;
										$this->db->query($update);
										$this->log->write($update);
									} elseif($act_intime != '00:00:00' && $act_outtime == '00:00:00') {
										$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` = '".$act_intime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
										//echo $update;exit;
										$this->db->query($update);
										$this->log->write($update);
									} elseif($act_intime == '00:00:00' && $act_outtime != '00:00:00') {
										$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` = '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
										//echo $update;exit;
										$this->db->query($update);
										$this->log->write($update);
									}
								} else {
									$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
									$this->db->query($update);
									$this->log->write($update);

									$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
									$this->db->query($update);
									$this->log->write($update);
								}
							}
						} else {
							$emp_data = $this->model_transaction_transaction->getempdata($rvalue['emp_code']);
							if(isset($emp_data['name']) && $emp_data['name'] != ''){
								$emp_name = $emp_data['name'];
								$department = $emp_data['department'];
								$unit = $emp_data['unit'];
								$group = $emp_data['group'];

								$day_date = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));

								$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' ";
								//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
								$shift_schedule = $this->db->query($update3)->row;
								$schedule_raw = explode('_', $shift_schedule[$day_date]);
								if($schedule_raw[0] == 'S'){
									$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
									if(isset($shift_data['shift_id'])){
										$shift_intime = $shift_data['in_time'];
										$shift_outtime = $shift_data['out_time'];
										
										$day = date('j', strtotime($filter_date_start));
										$month = date('n', strtotime($filter_date_start));
										$year = date('Y', strtotime($filter_date_start));

										$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
										$trans_exist = $this->db->query($trans_exist_sql);
										if($trans_exist->num_rows == 0){
											$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' ";
											//echo $sql.';';
											//echo '<br />';
											$this->db->query($sql);
										} else {
											$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."'";
											//echo $sql.';';
											//echo '<br />';
											//$this->db->query($sql);
										}
									} else {
										$shift_data = $this->model_transaction_transaction->getshiftdata('1');
										$shift_intime = $shift_data['in_time'];
										$shift_outtime = $shift_data['out_time'];
										
										$day = date('j', strtotime($filter_date_start));
										$month = date('n', strtotime($filter_date_start));
										$year = date('Y', strtotime($filter_date_start));

										$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
										$trans_exist = $this->db->query($trans_exist_sql);
										if($trans_exist->num_rows == 0){
											$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' ";
											//echo $sql.';';
											//echo '<br />';
											$this->db->query($sql);
										} else {
											$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
											//echo $sql.';';
											//echo '<br />';
											//$this->db->query($sql);
										}
									}
								} elseif ($schedule_raw[0] == 'W') {
									$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->model_transaction_transaction->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									
									if($trans_exist->num_rows == 0){
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' ";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
										//echo $sql.';';
										//echo '<br />';
										//$this->db->query($sql);
									}
								} elseif ($schedule_raw[0] == 'H') {
									$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->model_transaction_transaction->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									
									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."'  ";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
										//echo $sql.';';
										//echo '<br />';
										//$this->db->query($sql);
									}
								} elseif ($schedule_raw[0] == 'HD') {
									$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->model_transaction_transaction->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
									//$shift_outtime = $shift_data['out_time'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									
									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '".$schedule_raw[1]."', batch_id = '".$batch_id."' ";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '".$schedule_raw[1]."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
										//echo $sql.';';
										//echo '<br />';
										//$this->db->query($sql);
									}
								}  elseif ($schedule_raw[0] == 'C') {
									$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->model_transaction_transaction->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									
									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', compli_status = '".$schedule_raw[1]."', batch_id = '".$batch_id."' ";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', compli_status = '".$schedule_raw[1]."', batch_id = '".$batch_id."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
										//echo $sql.';';
										//echo '<br />';
										//$this->db->query($sql);
									}
								}
							}	
						}
					}
					$this->load->model('transaction/dayprocess');
					$absent = $this->model_transaction_dayprocess->getAbsent($filter_date_start,$filter_unit);
					foreach($absent as $data) {
						$check_leave = $this->model_transaction_dayprocess->checkLeave($data['date'],$data['emp_id']);
						//$this->model_transaction_dayprocess->checkLeave_1($data['date'],$data['emp_id']);
					}
					$this->model_transaction_dayprocess->update_close_day($filter_date_start, $filter_unit);
					//$this->db->query("commit;");
					//$response['success'] = 'Transaction Generated';
				} else {
					//$this->db->query("commit;");
					//$response['warning'] = 'No Data available';
				}
			}
		}

		$this->load->model('transaction/leaveprocess');
		$this->load->model('transaction/dayprocess');
		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = '';
		}
		$unprocessed = $this->model_transaction_leaveprocess->getUnprocessedLeaveTillDate_2($unit);
		foreach($unprocessed as $data) {
			$is_closed = $this->model_transaction_dayprocess->is_closed_stat($data['date'], $data['emp_id']); 
			if($is_closed == 1){
				$this->model_transaction_dayprocess->checkLeave($data['date'], $data['emp_id']);
			}
		}
		//echo 'out';exit;

		$transaction_datas = $this->db->query("SELECT * FROM `oc_transaction` WHERE `batch_id` = '".$batch_id."' ")->rows;
		$custom_data = array();
		foreach($transaction_datas as $tkeys => $tvalues){
			$emp_datas = $this->db->query("SELECT `emp_code`, `name`, `department`, `unit`, `division`, `region`, `doj` FROM `oc_employee` WHERE `emp_code` = '".$tvalues['emp_id']."' ");
			if($emp_datas->num_rows > 0){
				$emp_data = $emp_datas->row;
				$emp_code = $emp_data['emp_code'];
				$emp_name = $emp_data['name'];
				$department = $emp_data['department'];
				$unit = $emp_data['unit'];
				$division = $emp_data['division'];
				$region = $emp_data['region'];
				$doj = $emp_data['doj'];
			}

			$date_to_compare = $filter_date_start;
			if(strtotime($doj) > strtotime($date_to_compare)){
				$status = 'x';
			} elseif($tvalues['present_status'] == '1' || $tvalues['present_status'] == '0.5'){
				$status = 'p';
			} elseif($tvalues['weekly_off'] != '0'){
				$working_times = explode(':', $tvalues['working_time']);
				$working_hours = $working_times[0];
				if($tvalues['present_status'] == '1' || $tvalues['present_status'] == '0.5'){
				//if($working_hours >= '06'){
					$status = 'PW';
				} else {
					$status = 'W';	
				}
			} elseif($tvalues['holiday_id'] != '0'){
				$working_times = explode(':', $tvalues['working_time']);
				$working_hours = $working_times[0];
				if($tvalues['present_status'] == '1' || $tvalues['present_status'] == '0.5'){
				//if($working_hours >= '06'){
					$status = 'PPH';
				} else {
					$status = 'PH';
				}
			} elseif($tvalues['act_intime'] != '00:00:00' && $tvalues['act_outtime'] == '00:00:00'){
				$status = 'E';
			} elseif($tvalues['leave_status'] != '0'){
				if($tvalues['leave_status'] == '0.5'){
					$status = 'PL';
				} else {
					$status = 'PL';
				}
			} elseif($tvalues['absent_status'] == '1' || $tvalues['absent_status'] == '0.5'){
				$status = 'A';
			}

			$custom_data[] = array(
				'emp_id' => $emp_code,
				'emp_name' => $emp_name,
				'punch_id' => $emp_code,
				'department' => $department,
				'site' => $unit,
				'region' => $region,
				'division' => $division,
				'attend_date' => $filter_date_start,
				'status' => $status,
				'in_time' => $tvalues['act_intime'],
				'out_time' => $tvalues['act_outtime'],
				'total_hour' => $tvalues['working_time'],
			);
		}
		echo '<pre>';
		print_r($custom_data);
		exit;
		
		//$this->session->data['success'] = 'Employess Updated Sucessfully';
		$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));

	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
	   	$v2 = strtotime($b['fdate']);
	   	return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
	 		//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
	 	//}
	 	//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}
}
?>
