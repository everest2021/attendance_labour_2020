<?php
class ControllerReportLeaveReport extends Controller { 
	public function index() {  

		if(isset($this->session->data['emp_code'])){
			$emp_code = $this->session->data['emp_code'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_dept'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_super'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		}

		$this->language->load('report/leave_report');
		$this->load->model('report/common_report');

		$this->document->setTitle($this->language->get('heading_title'));


		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			if($filter_year == date('Y')){
				if(date('n') <= 3){
					$year = date('Y',strtotime("-1 year"));
				} else {
					$year = date('Y');
				}
				$filter_date_start = $year.'-03-26';
			} else {
				$year = $filter_year;
				$filter_date_start = $year.'-03-26';
			}
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		}  else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = array();
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			//$filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $unit);
			//if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
				//$filter_date_end = $filter_date_end1;
			//}
		} else {
			$filter_year_plus_one = $filter_year + 1;
			$filter_date_end = $filter_year_plus_one.'-03-25';//date('Y-m-d');
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} elseif(isset($this->session->data['dept_name'])){
			$department = $this->session->data['dept_name'];
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['filter_leave'])) {
			$filter_leave = $this->request->get['filter_leave'];
			$filter_leave = explode(',', $filter_leave);
		} else {
			$filter_leave = array();
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['filter_leave'])) {
			$url .= '&filter_leave=' . $this->request->get['filter_leave'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}

		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}

		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}

		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}

		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}

		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}

		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/leave_report', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');
		$this->load->model('transaction/transaction');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_year'	     	 => $filter_year,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'company'			 	 => $company,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);
		$final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		//$from_year = date('Y', strtotime($filter_date_start));
		$from_year = $filter_year;

		$leavess = $this->db->query("SELECT * FROM `oc_leavemaster` WHERE 1=1 ")->rows;
		$leaves = array();
		//$leaves[0]['leave_id'] = '0';
		//$leaves[0]['leave_name'] = 'All';
		$icnt = 1;
		$icnt1 = 1;
		$leaves_heading = array();
		foreach($leavess as $lkey => $lvalue){
			if(empty($filter_leave) || in_array($lvalue['leavemaster'], $filter_leave) ){
				$leaves[$icnt] = array(
					'leave_id' => $lvalue['leavemaster'],
					'leave_name' => $lvalue['leavemaster'],
				);
				$leaves_heading[$icnt1] = $lvalue['leavemaster'].'-Opening Allowance';
				$icnt1 ++;
				$leaves_heading[$icnt1] = $lvalue['leavemaster'].'-Credit';
				$icnt1 ++;
				$leaves_heading[$icnt1] = $lvalue['leavemaster'].'-Debit';
				$icnt1 ++;
				$leaves_heading[$icnt1] = $lvalue['leavemaster'].'-Closing Balance';
				$icnt1 ++;
				$icnt ++;
			}
		}
		$this->data['leaves_heading'] = $leaves_heading;
		// echo '<pre>';
		// print_r($leaves_heading);
		// exit;
		// $leaves = array(
		// 	'0'  => 'All',
		// 	'PL' => 'PL',
		// 	'BL' => 'BL',
		// 	'SL' => 'SL',
		// 	'ML' => 'ML',
		// 	'MAL' => 'MAL',
		// 	'PAL' => 'PAL',
		// 	'LWP' => 'LWP', 
		// );
		$this->data['leaves_display'] = $leaves;
		// echo '<pre>';
		// print_r($leaves);
		// exit;


		if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
			$results = $this->model_report_common_report->getemployees($data);
			// echo '<pre>';
			// print_r($results);
			// exit;
			foreach ($results as $rkey => $rvalue) {
				$performance_data = array();
				$transaction_datas = $this->model_report_common_report->getleave_transaction_data_group($rvalue['emp_code'], $data);
				// echo '<pre>';
				// print_r($transaction_datas);
				// exit;
				$leave_datas = $this->model_report_common_report->getleave_data($rvalue['emp_code'], $from_year);
				if(isset($leave_datas['leave_id'])){
					$emp_code = $rvalue['emp_code'];
					$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
					$final_datas[$rvalue['emp_code']]['basic_data']['department'] = $rvalue['department'];
					$final_datas[$rvalue['emp_code']]['basic_data']['designation'] = $rvalue['designation'];
					$final_datas[$rvalue['emp_code']]['basic_data']['region'] = $rvalue['region'];
					$final_datas[$rvalue['emp_code']]['basic_data']['division'] = $rvalue['division'];
					$final_datas[$rvalue['emp_code']]['basic_data']['unit'] = $rvalue['unit'];
					$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];
					
					$leave_opening = array();
					$leave_credit = array();
					$leave_transaction_datas = array();
					$total_leave_transaction = array();
					$leave_transaction = array();
					$total_un_leave_transaction = array();
					$un_leave_transaction = array();
					$closing_leave_transaction = array();
					$un_total_bal_transaction = array();
					foreach($leaves as $lkey => $lvalue){
						if($lvalue['leave_id'] != '0'){
							$acc_value = $this->model_transaction_transaction->getacc_value_opening($leave_datas['leave_id'], $lvalue['leave_name']);
							$leave_opening[$lvalue['leave_id']] = $acc_value;
							$leave_transaction_datas[$lvalue['leave_id']]['leave_open'] = $acc_value;
							
							$current_year = date('Y');
							$start_month_credit = '4';
							$start_year_credit = $filter_year;
						
							$end_month_credit = date('n', strtotime($data['filter_date_end']));
							$end_year_credit = date('Y', strtotime($data['filter_date_end']));
							
							$leave_credit_value = 0;
							if($lvalue['leave_id'] == 'PL'){
								if($filter_year == $end_year_credit){
									$leave_credits = $this->db->query("SELECT SUM(leave_value) as leave_credit FROM `oc_leave_credit_transaction` WHERE ((`emp_code` = '".$rvalue['emp_code']."') AND ((`month` >= '".$start_month_credit."' AND `year` = '".$start_year_credit."') AND (`month` <= '".$end_month_credit."' AND `year` = '".$end_year_credit."')))");	
								} else {
									$leave_credits = $this->db->query("SELECT SUM(leave_value) as leave_credit FROM `oc_leave_credit_transaction` WHERE ((`emp_code` = '".$rvalue['emp_code']."') AND ((`month` >= '".$start_month_credit."' AND `year` = '".$start_year_credit."') OR (`month` <= '".$end_month_credit."' AND `year` = '".$end_year_credit."')))");	
								}
								$leave_credit_value = 0;
								if($leave_credits->num_rows > 0){
									$leave_credit_value = $leave_credits->row['leave_credit'];
									if($leave_credit_value == '' || $leave_credit_value == null){
										$leave_credit_value = 0;
									}
								}
								$leave_transaction_datas[$lvalue['leave_id']]['leave_credit'] = $leave_credit_value;
								$leave_credit[$lvalue['leave_id']] = $leave_credit_value;
							} else {
								$leave_transaction_datas[$lvalue['leave_id']]['leave_credit'] = $leave_credit_value;
								$leave_credit[$lvalue['leave_id']] = $leave_credit_value;
							}
						}						
					}
					$final_datas[$rvalue['emp_code']]['basic_data']['leave_opening'] = $leave_opening;
					$final_datas[$rvalue['emp_code']]['basic_data']['leave_credit'] = $leave_credit;

					$a = $filter_year;
					if($a == '2015'){
						$final_datas[$rvalue['emp_code']]['basic_data']['open_date'] = '01/11/2015';
					} else {
						$final_datas[$rvalue['emp_code']]['basic_data']['open_date'] = '01/04/'.$filter_year;
					}
					foreach($leaves as $lkey => $lvalue){
						if($lvalue['leave_id'] != '0'){
							$leave_transaction_datas[$lvalue['leave_id']]['leave_taken'] = '';
							$total_leave_transaction[$lvalue['leave_id']]  = '';
							$leave_transaction[$lvalue['leave_id']]  = '';
							$total_un_leave_transaction[$lvalue['leave_id']]  = '';
							$un_leave_transaction[$lvalue['leave_id']]  = '';
							$closing_leave_transaction[$lvalue['leave_id']] = '';
							$un_total_bal_transaction[$lvalue['leave_id']] = '';
 						}
					}
					// echo '<pre>';
					// print_r($rvalue);
					// exit;
					if($transaction_datas){
						foreach($transaction_datas as $tkey => $tvalue){
							foreach($leaves as $lkey => $lvalue){
								if($lvalue['leave_id'] != '0'){
									$leave_transaction[$lvalue['leave_id']]  = '';
								}
							}		
							$t_datas = $this->model_report_common_report->getleave_transaction_data($tvalue['batch_id']);	
							usort($t_datas, array($this, "date_compare"));
							end($t_datas);
							$e_key = key($t_datas);
							$start_date = $t_datas[0]['date'];
							$end_date = $t_datas[$e_key]['date'];
							// echo '<pre>';
							// print_r($t_datas);
							// exit;
							$days = 0;
							foreach ($t_datas as $ttkey => $ttvalue) {
								if($ttvalue['p_status'] == 1){
									$days ++;
								}
							}

							if($t_datas[0]['type'] != ''){
								$leave_types = $t_datas[0]['type'];
							} else {
								$leave_types = 'F';
							}

							foreach($leaves as $lkey => $lvalue){
								if($lvalue['leave_id'] != '0'){
									if($t_datas[0]['type'] != ''){
										if($t_datas[0]['type'] == 'F'){
											$days_lv = 1;
										} else {
											$days_lv = 0.5;
										}
									} else {
										$days_lv = $days;
									}
									if($t_datas[0]['leave_type'] == $lvalue['leave_name']){
										$total_leave_transaction[$t_datas[0]['leave_type']] += $days_lv;
										$leave_transaction_datas[$t_datas[0]['leave_type']]['leave_taken'] += $days_lv;
										$leave_transaction[$t_datas[0]['leave_type']] = $days_lv;
									}
								}						
							}
							$performance_data['action'][] = array(
								'start_date' => date('d/m/Y', strtotime($start_date)),
								'end_date' => date('d/m/Y', strtotime($end_date)),
								'leave_transaction' => $leave_transaction,
								'leave_types' => $leave_types,
								'performance_stat' => '1'
							);
						}
					}

					$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
					$a = date('Y', strtotime($data['filter_date_start']));					
					$a = $a + 1;
					//$filter_end = $a.'-03-31';
					$filter_end = $filter_date_end;
					if (!empty($data['filter_date_start'])) {
						$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
					}
					if (!empty($data['filter_date_end'])) {
						$sql .= " AND DATE(`date`) <= '" . $this->db->escape($filter_end) . "'";
					}
					if (!empty($data['unit'])) {
						$sql .= " AND LOWER(`unit_id`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
					}
					// if (isset($data['filter_leave']) && !empty($data['filter_leave'])) {
					// 	$sql .= " AND LOWER(`leave_type`) = '" . $this->db->escape(strtolower($data['filter_leave'])) . "'";
					// }
					$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' GROUP BY `batch_id` ";
					//echo $sql;exit;
					$un_transaction_datas = $this->db->query($sql)->rows;
					
					foreach($un_transaction_datas as $tkey => $tvalue){
						$un_t_datas = $this->model_report_common_report->getleave_transaction_data($tvalue['batch_id']);	
						usort($un_t_datas, array($this, "date_compare"));
						end($un_t_datas);
						$un_e_key = key($un_t_datas);
						$un_start_date = $un_t_datas[0]['date'];
						$un_end_date = $un_t_datas[$un_e_key]['date'];
						// echo '<pre>';
						// print_r($t_datas);
						// exit;
						$un_days = 0;
						foreach ($un_t_datas as $ttkey => $ttvalue) {
							if($ttvalue['p_status'] == 0){
								$un_days ++;
							}
						}

						if($un_t_datas[0]['type'] != ''){
							$un_leave_types = $un_t_datas[0]['type'];
						} else {
							$un_leave_types = 'F';
						}				
						
						foreach($leaves as $lkey => $lvalue){
							if($lvalue['leave_id'] != '0'){
								if($un_t_datas[0]['type'] != ''){
									if($un_t_datas[0]['type'] == 'F'){
										$days_lv = 1;
									} else {
										$days_lv = 0.5;
									}
								} else {
									$days_lv = $un_days;
								}
								if($un_t_datas[0]['leave_type'] == $lvalue['leave_name']){
									$total_un_leave_transaction[$un_t_datas[0]['leave_type']] += $days_lv;
									$un_leave_transaction[$un_t_datas[0]['leave_type']] = $days_lv;
									$leave_transaction_datas[$un_t_datas[0]['leave_type']]['leave_taken'] += $days_lv;
								}
							}						
						}
					}
					
					foreach($leaves as $lkey => $lvalue){
						if($lvalue['leave_id'] != '0'){
							$closing_leave_transaction[$lvalue['leave_name']] = ($leave_opening[$lvalue['leave_name']] + $leave_credit[$lvalue['leave_name']]) - $total_leave_transaction[$lvalue['leave_name']];
							$un_total_bal_transaction[$lvalue['leave_name']] = $closing_leave_transaction[$lvalue['leave_name']] - $total_un_leave_transaction[$lvalue['leave_name']];
 							$leave_transaction_datas[$lvalue['leave_name']]['leave_balance'] = $un_total_bal_transaction[$lvalue['leave_name']];
 						}						
					}
					$final_datas[$rvalue['emp_code']]['basic_data']['total_leave_transaction'] = $total_leave_transaction;
					$final_datas[$rvalue['emp_code']]['basic_data']['total_un_leave_transaction'] = $total_un_leave_transaction;
					$final_datas[$rvalue['emp_code']]['basic_data']['leave_transaction_datas'] = $leave_transaction_datas;
					$final_datas[$rvalue['emp_code']]['basic_data']['closing_leave_transaction'] = $closing_leave_transaction;
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_transaction'] = $un_total_bal_transaction;
					$final_datas[$rvalue['emp_code']]['basic_data']['edit_href'] = $this->url->link('transaction/leave', 'token=' . $this->session->data['token'].'&filter_name='.$rvalue['name'].'&filter_name_id='.$rvalue['emp_code'].'&filter_date_start='.$filter_date_start.'&filter_date_end='.$filter_date_end, 'SSL');
					$final_datas[$rvalue['emp_code']]['tran_data'] = $performance_data;
				}
			}
			//exit;
		}
		//$final_datas = array();
		$this->data['final_datas'] = $final_datas;
		
		// echo '<pre>';
		// print_r($this->data['final_datas']);
		// exit;
		$leaves = array(
			'1' => 'Sr No',
			'2' => 'Employee Code',
			'3' => 'Punch Id',
			'4' => 'Employee Name',
			'5' => 'Region',
			'6' => 'Division',
			'7' => 'Site',
			'8' => 'Department',
			'9' => 'Designation',
		);
		//$this->data['fields_data'] = $fields_data;

		$leavess = $this->db->query("SELECT * FROM `oc_leavemaster` WHERE 1=1 ")->rows;
		//$leaves = array();
		//$leaves[0]['leave_id'] = '0';
		//$leaves[0]['leave_name'] = 'All';
		$icnt = 10;
		foreach($leavess as $lkey => $lvalue){
			$leaves[$lvalue['leavemaster']] = $lvalue['leavemaster'];
			$icnt ++;
		}
		$this->data['leaves'] = $leaves;

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $division,
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		$site_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['unit_data'] = $site_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;


		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_data'] = $company_data;

		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;
		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		$group_data['1'] = 'All WITHOUT OFFICIALS';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		// if (isset($this->request->get['filter_month'])) {
		// 	$url .= '&filter_month=' . $this->request->get['filter_month'];
		// }

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['filter_leave'])) {
			$url .= '&filter_leave=' . $this->request->get['filter_leave'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}

		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}

		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}

		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}

		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}

		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		$months = array(
			'01' => 'January',
			'02' => 'Feburary',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);

		$this->data['months'] = $months;

		if(isset($this->session->data['is_dept']) && $this->session->data['is_dept'] == '1'){
			$user_dept = 1;
			$is_dept = 1;
		} elseif(isset($this->session->data['is_user'])){
			$user_dept = 1;
			$is_dept = 0;
		} else {
			$user_dept = 0;
			$is_dept = 0;
		}
		$this->data['user_dept'] = $user_dept;
		$this->data['is_dept'] = $is_dept;

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		// $this->data['filter_month'] = $filter_month;
		$this->data['filter_year'] = $filter_year;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_leave'] = $filter_leave;
		$this->data['filter_fields'] = $filter_fields;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['division'] = $division;
		$this->data['region'] = $region;
		$this->data['company'] = explode(',', $company);
		$this->data['group'] = $group;

		// echo '<pre>';
		// print_r($filter_leave);
		// echo '<pre>';
		// print_r($leaves);
		// exit;

		$this->template = 'report/leave_report.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function date_compare($a, $b){
	    $t1 = strtotime($a['date']);
	    $t2 = strtotime($b['date']);
	    return $t1 - $t2;
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function export(){
		$this->language->load('report/leave_report');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			if($filter_year == date('Y')){
				if(date('n') <= 3){
					$year = date('Y',strtotime("-1 year"));
				} else {
					$year = date('Y');
				}
				$filter_date_start = $year.'-03-26';
			} else {
				$year = $filter_year;
				$filter_date_start = $year.'-03-26';
			}
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		}  else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = array();
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			//$filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $unit);
			//if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
				//$filter_date_end = $filter_date_end1;
			//}
		} else {
			$filter_year_plus_one = $filter_year + 1;
			$filter_date_end = $filter_year_plus_one.'-03-25';//date('Y-m-d');
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} elseif(isset($this->session->data['dept_name'])){
			$department = $this->session->data['dept_name'];
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['filter_leave'])) {
			$filter_leave = $this->request->get['filter_leave'];
			$filter_leave = explode(',', $filter_leave);
		} else {
			$filter_leave = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		// if (isset($this->request->get['filter_month'])) {
		// 	$url .= '&filter_month=' . $this->request->get['filter_month'];
		// }

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['filter_leave'])) {
			$url .= '&filter_leave=' . $this->request->get['filter_leave'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}

		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}

		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}

		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}

		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}

		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		$url .= '&once=1';

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');
		$this->load->model('transaction/transaction');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_year'	     	 => $filter_year,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'company'			 	 => $company,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		
        $final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		//$from_year = date('Y', strtotime($filter_date_start));
		$from_year = $filter_year;
		
		$leavess = $this->db->query("SELECT * FROM `oc_leavemaster` WHERE 1=1 ")->rows;
		$leaves = array();
		//$leaves[0]['leave_id'] = '0';
		//$leaves[0]['leave_name'] = 'All';
		$icnt = 1;
		$icnt1 = 1;
		$leaves_heading = array();
		foreach($leavess as $lkey => $lvalue){
			if(empty($filter_leave) || in_array($lvalue['leavemaster'], $filter_leave) ){
				$leaves[$icnt] = array(
					'leave_id' => $lvalue['leavemaster'],
					'leave_name' => $lvalue['leavemaster'],
				);
				$leaves_heading[$icnt1] = $lvalue['leavemaster'].'-Opening Allowance';
				$icnt1 ++;
				$leaves_heading[$icnt1] = $lvalue['leavemaster'].'-Credit';
				$icnt1 ++;
				$leaves_heading[$icnt1] = $lvalue['leavemaster'].'-Debit';
				$icnt1 ++;
				$leaves_heading[$icnt1] = $lvalue['leavemaster'].'-Closing Balance';
				$icnt1 ++;
				$icnt ++;
			}
		}
		$this->data['leaves'] = $leaves;

		$results = $this->model_report_common_report->getemployees($data);
		// echo '<pre>';
		// print_r($results);
		// exit;
		foreach ($results as $rkey => $rvalue) {
			$performance_data = array();
			$transaction_datas = $this->model_report_common_report->getleave_transaction_data_group($rvalue['emp_code'], $data);
			// echo '<pre>';
			// print_r($transaction_datas);
			// exit;
			$leave_datas = $this->model_report_common_report->getleave_data($rvalue['emp_code'], $from_year);
			if(isset($leave_datas['leave_id'])){
				$emp_code = $rvalue['emp_code'];
				$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['basic_data']['department'] = $rvalue['department'];
				$final_datas[$rvalue['emp_code']]['basic_data']['designation'] = $rvalue['designation'];
				$final_datas[$rvalue['emp_code']]['basic_data']['region'] = $rvalue['region'];
				$final_datas[$rvalue['emp_code']]['basic_data']['division'] = $rvalue['division'];
				$final_datas[$rvalue['emp_code']]['basic_data']['unit'] = $rvalue['unit'];
				$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];
				
				$leave_opening = array();
				$leave_credit = array();
				$leave_transaction_datas = array();
				$total_leave_transaction = array();
				$leave_transaction = array();
				$total_un_leave_transaction = array();
				$un_leave_transaction = array();
				$closing_leave_transaction = array();
				$un_total_bal_transaction = array();
				foreach($leaves as $lkey => $lvalue){
					if($lvalue['leave_id'] != '0'){
						$acc_value = $this->model_transaction_transaction->getacc_value_opening($leave_datas['leave_id'], $lvalue['leave_name']);
						$leave_opening[$lvalue['leave_id']] = $acc_value;
						$leave_transaction_datas[$lvalue['leave_id']]['leave_open'] = $acc_value;
						
						$current_year = date('Y');
						$start_month_credit = '4';
						$start_year_credit = $filter_year;
					
						$end_month_credit = date('n', strtotime($data['filter_date_end']));
						$end_year_credit = date('Y', strtotime($data['filter_date_end']));
						
						$leave_credit_value = 0;
						if($lvalue['leave_id'] == 'PL'){
							if($filter_year == $end_year_credit){
								$leave_credits = $this->db->query("SELECT SUM(leave_value) as leave_credit FROM `oc_leave_credit_transaction` WHERE ((`emp_code` = '".$rvalue['emp_code']."') AND ((`month` >= '".$start_month_credit."' AND `year` = '".$start_year_credit."') AND (`month` <= '".$end_month_credit."' AND `year` = '".$end_year_credit."')))");	
							} else {
								$leave_credits = $this->db->query("SELECT SUM(leave_value) as leave_credit FROM `oc_leave_credit_transaction` WHERE ((`emp_code` = '".$rvalue['emp_code']."') AND ((`month` >= '".$start_month_credit."' AND `year` = '".$start_year_credit."') OR (`month` <= '".$end_month_credit."' AND `year` = '".$end_year_credit."')))");	
							}
							$leave_credit_value = 0;
							if($leave_credits->num_rows > 0){
								$leave_credit_value = $leave_credits->row['leave_credit'];
								if($leave_credit_value == '' || $leave_credit_value == null){
									$leave_credit_value = 0;
								}
							}
							$leave_transaction_datas[$lvalue['leave_id']]['leave_credit'] = $leave_credit_value;
							$leave_credit[$lvalue['leave_id']] = $leave_credit_value;
						} else {
							$leave_transaction_datas[$lvalue['leave_id']]['leave_credit'] = $leave_credit_value;
							$leave_credit[$lvalue['leave_id']] = $leave_credit_value;
						}
					}						
				}
				$final_datas[$rvalue['emp_code']]['basic_data']['leave_opening'] = $leave_opening;
				$final_datas[$rvalue['emp_code']]['basic_data']['leave_credit'] = $leave_credit;

				$a = $filter_year;
				if($a == '2015'){
					$final_datas[$rvalue['emp_code']]['basic_data']['open_date'] = '01/11/2015';
				} else {
					$final_datas[$rvalue['emp_code']]['basic_data']['open_date'] = '01/04/'.$filter_year;
				}
				foreach($leaves as $lkey => $lvalue){
					if($lvalue['leave_id'] != '0'){
						$leave_transaction_datas[$lvalue['leave_id']]['leave_taken'] = '';
						$total_leave_transaction[$lvalue['leave_id']]  = '';
						$leave_transaction[$lvalue['leave_id']]  = '';
						$total_un_leave_transaction[$lvalue['leave_id']]  = '';
						$un_leave_transaction[$lvalue['leave_id']]  = '';
						$closing_leave_transaction[$lvalue['leave_id']] = '';
						$un_total_bal_transaction[$lvalue['leave_id']] = '';
						}
				}
				// echo '<pre>';
				// print_r($rvalue);
				// exit;
				if($transaction_datas){
					foreach($transaction_datas as $tkey => $tvalue){
						foreach($leaves as $lkey => $lvalue){
							if($lvalue['leave_id'] != '0'){
								$leave_transaction[$lvalue['leave_id']]  = '';
							}
						}		
						$t_datas = $this->model_report_common_report->getleave_transaction_data($tvalue['batch_id']);	
						usort($t_datas, array($this, "date_compare"));
						end($t_datas);
						$e_key = key($t_datas);
						$start_date = $t_datas[0]['date'];
						$end_date = $t_datas[$e_key]['date'];
						// echo '<pre>';
						// print_r($t_datas);
						// exit;
						$days = 0;
						foreach ($t_datas as $ttkey => $ttvalue) {
							if($ttvalue['p_status'] == 1){
								$days ++;
							}
						}

						if($t_datas[0]['type'] != ''){
							$leave_types = $t_datas[0]['type'];
						} else {
							$leave_types = 'F';
						}

						foreach($leaves as $lkey => $lvalue){
							if($lvalue['leave_id'] != '0'){
								if($t_datas[0]['type'] != ''){
									if($t_datas[0]['type'] == 'F'){
										$days_lv = 1;
									} else {
										$days_lv = 0.5;
									}
								} else {
									$days_lv = $days;
								}
								if($t_datas[0]['leave_type'] == $lvalue['leave_name']){
									$total_leave_transaction[$t_datas[0]['leave_type']] += $days_lv;
									$leave_transaction_datas[$t_datas[0]['leave_type']]['leave_taken'] += $days_lv;
									$leave_transaction[$t_datas[0]['leave_type']] = $days_lv;
								}
							}						
						}
						$performance_data['action'][] = array(
							'start_date' => date('d/m/Y', strtotime($start_date)),
							'end_date' => date('d/m/Y', strtotime($end_date)),
							'leave_transaction' => $leave_transaction,
							'leave_types' => $leave_types,
							'performance_stat' => '1'
						);
					}
				}

				$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
				$a = date('Y', strtotime($data['filter_date_start']));					
				$a = $a + 1;
				//$filter_end = $a.'-03-31';
				$filter_end = $filter_date_end;
				if (!empty($data['filter_date_start'])) {
					$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
				}
				if (!empty($data['filter_date_end'])) {
					$sql .= " AND DATE(`date`) <= '" . $this->db->escape($filter_end) . "'";
				}
				if (!empty($data['unit'])) {
					$sql .= " AND LOWER(`unit_id`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
				}
				// if (isset($data['filter_leave']) && !empty($data['filter_leave'])) {
				// 	$sql .= " AND LOWER(`leave_type`) = '" . $this->db->escape(strtolower($data['filter_leave'])) . "'";
				// }
				$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' GROUP BY `batch_id` ";
				//echo $sql;exit;
				$un_transaction_datas = $this->db->query($sql)->rows;
				
				foreach($un_transaction_datas as $tkey => $tvalue){
					$un_t_datas = $this->model_report_common_report->getleave_transaction_data($tvalue['batch_id']);	
					usort($un_t_datas, array($this, "date_compare"));
					end($un_t_datas);
					$un_e_key = key($un_t_datas);
					$un_start_date = $un_t_datas[0]['date'];
					$un_end_date = $un_t_datas[$un_e_key]['date'];
					// echo '<pre>';
					// print_r($t_datas);
					// exit;
					$un_days = 0;
					foreach ($un_t_datas as $ttkey => $ttvalue) {
						if($ttvalue['p_status'] == 0){
							$un_days ++;
						}
					}

					if($un_t_datas[0]['type'] != ''){
						$un_leave_types = $un_t_datas[0]['type'];
					} else {
						$un_leave_types = 'F';
					}				
					
					foreach($leaves as $lkey => $lvalue){
						if($lvalue['leave_id'] != '0'){
							if($un_t_datas[0]['type'] != ''){
								if($un_t_datas[0]['type'] == 'F'){
									$days_lv = 1;
								} else {
									$days_lv = 0.5;
								}
							} else {
								$days_lv = $un_days;
							}
							if($un_t_datas[0]['leave_type'] == $lvalue['leave_name']){
								$total_un_leave_transaction[$un_t_datas[0]['leave_type']] += $days_lv;
								$un_leave_transaction[$un_t_datas[0]['leave_type']] = $days_lv;
								$leave_transaction_datas[$un_t_datas[0]['leave_type']]['leave_taken'] += $days_lv;
							}
						}						
					}
				}
				
				foreach($leaves as $lkey => $lvalue){
					if($lvalue['leave_id'] != '0'){
						$closing_leave_transaction[$lvalue['leave_name']] = ($leave_opening[$lvalue['leave_name']] + $leave_credit[$lvalue['leave_name']]) - $total_leave_transaction[$lvalue['leave_name']];
						$un_total_bal_transaction[$lvalue['leave_name']] = $closing_leave_transaction[$lvalue['leave_name']] - $total_un_leave_transaction[$lvalue['leave_name']];
							$leave_transaction_datas[$lvalue['leave_name']]['leave_balance'] = $un_total_bal_transaction[$lvalue['leave_name']];
						}						
				}
				$final_datas[$rvalue['emp_code']]['basic_data']['total_leave_transaction'] = $total_leave_transaction;
				$final_datas[$rvalue['emp_code']]['basic_data']['total_un_leave_transaction'] = $total_un_leave_transaction;
				$final_datas[$rvalue['emp_code']]['basic_data']['leave_transaction_datas'] = $leave_transaction_datas;
				$final_datas[$rvalue['emp_code']]['basic_data']['closing_leave_transaction'] = $closing_leave_transaction;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_transaction'] = $un_total_bal_transaction;
				$final_datas[$rvalue['emp_code']]['basic_data']['edit_href'] = $this->url->link('transaction/leave', 'token=' . $this->session->data['token'].'&filter_name='.$rvalue['name'].'&filter_name_id='.$rvalue['emp_code'].'&filter_date_start='.$filter_date_start.'&filter_date_end='.$filter_date_end, 'SSL');
				$final_datas[$rvalue['emp_code']]['tran_data'] = $performance_data;
			}
		}

		//$final_datas = array();
		//$final_datass = array_chunk($final_datas, 3);

		// echo '<pre>';
		// print_r($final_datass);
		// exit;
		
		if($final_datas){
			if (isset($this->request->get['unit'])) {
				$unit_names = $this->db->query("SELECT `unit` FROM `oc_unit` WHERE `unit_id` = '".$this->request->get['unit']."' ");
				if($unit_names->num_rows > 0){
					$unit_name = $unit_names->row['unit'];
				} else {
					$unit_name = '';
				}
				$filter_unit = html_entity_decode($unit_name);
			} else {
				$filter_unit = 'All';
			}
			if (isset($this->request->get['department'])) {
				$department_names = $this->db->query("SELECT `d_name` FROM `oc_department` WHERE `department_id` = '".$this->request->get['department']."' ");
				if($department_names->num_rows > 0){
					$department_name = $department_names->row['d_name'];
				} else {
					$department_name = '';
				}
				$filter_department = html_entity_decode($department_name);
			} else {
				$filter_department = 'All';
			}
			if (isset($this->request->get['division'])) {
				$division_names = $this->db->query("SELECT `division` FROM `oc_division` WHERE `division_id` = '".$this->request->get['division']."' ");
				if($division_names->num_rows > 0){
					$division_name = $division_names->row['division'];
				} else {
					$division_name = '';
				}
				$filter_division = html_entity_decode($division_name);
			} else {
				$filter_division = 'All';
			}

			if (isset($this->request->get['region'])) {
				$region_names = $this->db->query("SELECT `region` FROM `oc_region` WHERE `region_id` = '".$this->request->get['region']."' ");
				if($region_names->num_rows > 0){
					$region_name = $region_names->row['region'];
				} else {
					$region_name = '';
				}
				$filter_region = html_entity_decode($region_name);
			} else {
				$filter_region = 'All';
			}

			if (isset($this->request->get['company'])) {
				$company_names = $this->db->query("SELECT `company` FROM `oc_company` WHERE `company_id` = '".$this->request->get['company']."' ");
				if($company_names->num_rows > 0){
					$company_name = $company_names->row['company'];
				} else {
					$company_name = '';
				}
				$filter_company = html_entity_decode($company_name);
			} else {
				$filter_company = 'All';
			}

			//echo count($leaves);exit;

			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$filter_year1 = $filter_date_start;
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['filter_date_start'] = date('d-F-Y', strtotime($filter_year1));
			$template->data['filter_date_end'] = date('d-F-Y', strtotime($filter_date_end));
			$template->data['filter_year'] = $filter_year;
			$template->data['filter_leave'] = $filter_leave;
			$template->data['filter_division'] = $filter_division;
			$template->data['filter_region'] = $filter_region;
			$template->data['filter_company'] = $filter_company;
			$template->data['filter_unit'] = $filter_unit;
			$template->data['filter_department'] = $filter_department;
			$template->data['leaves_display'] = $leaves;
			$template->data['leaves_heading'] = $leaves_heading;
			//$template->data['report_type'] = $report_type;
			$template->data['title'] = 'Leave Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/leave_report_html.tpl');
			//echo $html;exit;
			$filename = "Leave_Report";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/leave_report', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
	
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_names'])){
				$filter_departments = html_entity_decode($this->session->data['dept_names']);
				$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
			} else {
				$filter_departments = '';
			}

			if($filter_departments == ''){
				if(isset($this->session->data['dept_name'])){
					$filter_department = $this->session->data['dept_name'];
				} else {
					$filter_department = '';
				}
			} else {
				$filter_department = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_departments' => $filter_departments,
				'filter_department' => $filter_department,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>