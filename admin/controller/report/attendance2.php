<?php
class ControllerReportAttendance2 extends Controller { 
	public function index() {  
		date_default_timezone_set("Asia/Kolkata");
		$this->language->load('report/attendance2');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/attendance2', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	    => $filter_date_start,
			'filter_unit'			=> $filter_unit,
			'start'                 => ($page - 1) * 7000,
			'limit'                 => 7000
		);

		$this->data['export'] = $this->url->link('report/attendance2/export', 'token=' . $this->session->data['token'] . $url, 'SSL');

		
		$serverName = "JMCFRM\ATTENDANCE";
		$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"avi", "PWD"=>"avi2123");
		//$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"staff", "PWD"=>"staff");
		// $serverName = "HP";
		// $connectionInfo = array("Database"=>"SmartOffice");
		$conn = sqlsrv_connect($serverName, $connectionInfo);
		if($conn) {
			//echo "Connection established.<br />";exit;
		} else {
			// echo '<pre>';
			// print_r(sqlsrv_errors());
			// exit;
		}
		
		$this->data['final_datas'] = array();
		$this->data['total_shift_datas_assign'] = array();
		if(isset($this->request->get['once'])){
			$final_datas = array();
			$sql1 = "SELECT ShiftId, ShiftName, ShiftCode, BeginTime, EndTime FROM [etimetracklite1].[dbo].[Shifts] WHERE 1=1 AND BeginTime <> '00:00' AND EndTime <> '00:00' ORDER BY BeginTime ";
			//echo $sql;exit;
			$params1 = array();
			$options1 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
			$stmt1 = sqlsrv_query($conn, $sql1, $params1, $options1);
			$shift_datas = array();
			$shift_datas_assign = array();
			$shift_datas_1 = array();
			$cnt = 0;
			while($row1 = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)) {
				$shift_datas[$cnt] = $row1;
				$shift_datas_1[$row1['ShiftId']] = $row1;
				$shift_datas_assign[$row1['ShiftId']] = 0;
				$total_shift_datas_assign[$row1['ShiftId']] = 0;
				$cnt ++;
			}

			// echo '<pre>';
			// print_r($shift_datas);
			// echo '<pre>';
			// print_r($shift_datas_assign);
			// exit;
			$month = date('m', strtotime($filter_date_start));
			$year = date('Y', strtotime($filter_date_start));
			$table_name = 'DeviceLogs_'.$month.'_'.$year;
			$sql = "SELECT * FROM [etimetracklite1].[dbo].[Employees] emp LEFT JOIN [etimetracklite1].[dbo].[".$table_name."] d ON emp.EmployeeCode = d.UserId WHERE 1=1 AND location <> 'All' AND location <> '' ";
			if($filter_date_start){
				$sql .= " AND CAST(d.LogDate as DATE) = '".$filter_date_start."' ";
			}
			if($filter_unit){
				$sql .= " AND emp.Location = '".$filter_unit."' ";
			}
			$sql .= " ORDER BY emp.Location, d.UserId, CAST(d.LogDate as DATE), CAST(d.LogDate as TIME) ";
			//echo $sql;exit;
			$params = array();
			$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
			$stmt = sqlsrv_query($conn, $sql, $params, $options);
			//$row_count = sqlsrv_num_rows($stmt);
			//echo $row_count;exit;
			$attendance_raw_datas = array();
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
				$emp_id = $row['UserId'];
				$location = $row['Location'];
				$log_dates = $row['LogDate'];
				$log_date = '';
				$log_time = '';
				$log_date_time = '';
				if($log_dates != null){	
					$log_date = $log_dates->format("Y-m-d");
					$log_time = $log_dates->format("H:i:s");
					$log_date_time = $log_dates->format("Y-m-d H:i:s");
				}
				if($log_date_time != ''){
					$attendance_raw_datas[] = array(
						'emp_id' => $emp_id,
						'location' => $location,
						'log_date_time' => $log_date_time,
						'log_date' => $log_date,
						'log_time' => $log_time,
					);
				}
			}
			// echo '<pre>';
			// print_r($attendance_raw_datas);
			// exit;
			$current_emp_id = '';
			$current_location_id = '';
			$cnt = 1;
			$cnt1 = 1;
			$attendance_datas = array();
			foreach($attendance_raw_datas as $tkey => $tvalue){
				// echo '<pre>';
				// print_r($tvalue);
				// exit;
				$emp_change = 0;
				if($current_emp_id != $tvalue['emp_id']){
					$emp_change = 1;
					$current_emp_id = $tvalue['emp_id'];
					$attendance_datas = array();
					$cnt = 1;
				}
				if(strtolower(trim($current_location_id)) != strtolower(trim($tvalue['location']))){
					$current_location_id = strtolower(trim($tvalue['location']));
					foreach($shift_datas_1 as $skey1 => $svalue1){
						$shift_datas_assign[$skey1] = 0;
					}
					$attendance_datas = array();
					$cnt = 1;
					$cnt1 ++;
				}
				$prev_key = $cnt - 1;
				if(isset($attendance_datas[$prev_key])){
					$prev_date_time = $attendance_datas[$prev_key]['log_date_time'];
					$current_date_time = $tvalue['log_date_time'];
					$start_date = new DateTime($prev_date_time);
					$since_start = $start_date->diff(new DateTime($current_date_time));
					$in = 1;
					if($since_start->d == 0 && $since_start->h == 0 && $since_start->i < 5){
						$in = 0;
					}
					if($since_start->d >= 1){
						$in = 0;
					}
					if($in == 1){
						$attendance_datas[$cnt] = array(
							'log_date_time' => $tvalue['log_date_time'],
							'log_date' => $tvalue['log_date'],
							'log_time' => $tvalue['log_time'],
						);
					}
				} else {
					$attendance_datas[$cnt] = array(
						'log_date_time' => $tvalue['log_date_time'],
						'log_date' => $tvalue['log_date'],
						'log_time' => $tvalue['log_time'],
					);	
				}

				$next_key_2 = $tkey + 1;
				if( (isset($attendance_raw_datas[$next_key_2]['emp_id']) && $tvalue['emp_id'] != $attendance_raw_datas[$next_key_2]['emp_id']) || !isset($attendance_raw_datas[$next_key_2]['emp_id']) ){
					if(count($attendance_datas) == 1){
						$attendance_datas[2] = array(
							'log_date_time' => date('Y-m-d H:i:s'),
							'log_date' => date('Y-m-d'),
							'log_time' => date('H:i:s'),	
						);
					}
					end($attendance_datas);
					$max_count = key($attendance_datas);
					$in_date_time = $attendance_datas[1]['log_date_time'];
					$in_date = $attendance_datas[1]['log_date'];
					$in_time = $attendance_datas[1]['log_time'];
					
					$out_date_time = $attendance_datas[$max_count]['log_date_time'];
					$start_date = new DateTime($in_date_time);
					$since_start = $start_date->diff(new DateTime($out_date_time));
					$working_hours = sprintf("%02d", $since_start->h);
					
					$cal_shift_datass = $this->get_shift_datas($shift_datas, $tvalue['emp_id'], $in_date, $in_time, $in_date_time, $conn);
					$shift_key = $cal_shift_datass['key'];
					$cal_shift_datas = $cal_shift_datass['shift_worked_data'];

					$shift_begin_time = $in_date.' '.$cal_shift_datas['BeginTime'];
					$shift_end_time = $in_date.' '.$cal_shift_datas['EndTime'];
					$start_date = new DateTime($shift_begin_time);
					$since_start = $start_date->diff(new DateTime($shift_end_time));
					$shift_working_hours = sprintf("%02d", $since_start->h);

					// echo $shift_begin_time;
					// echo '<br />';
					// echo $shift_end_time;
					// echo '<br />';
					// echo '<pre>';
					// print_r($since_start);
					// exit;
					
					$difference_hours = $working_hours - $shift_working_hours;
					if($difference_hours > 1){
						$shift_id = $cal_shift_datas['ShiftId'];
						$shift_datas_assign[$shift_id] += 1;
						$total_shift_datas_assign[$shift_id] += 1;
						$next_key = $shift_key + 1;
						if(isset($shift_datas[$next_key])){
							$next_shift_id = $shift_datas[$next_key]['ShiftId'];
							$shift_datas_assign[$next_shift_id] += 1;
							$total_shift_datas_assign[$next_shift_id] += 1;
						}
					} else {
						$shift_id = $cal_shift_datas['ShiftId'];
						$shift_datas_assign[$shift_id] += 1;
						$total_shift_datas_assign[$shift_id] += 1;
					}
					// echo $tvalue['emp_id'];
					// echo '<br />';
					// echo '<pre>';
					// print_r($attendance_datas);
					// echo '<br/ >';
					// echo $in_date_time;
					// echo '<br />';
					// echo $out_date_time;
					// echo '<br />';
					// echo '<pre>';
					// print_r($working_hours);
					// exit;
				}
				if( ( isset($attendance_raw_datas[$next_key_2]['location']) && ( strtolower(trim($tvalue['location'])) != strtolower(trim($attendance_raw_datas[$next_key_2]['location'])) ) ) || !isset($attendance_raw_datas[$next_key_2]['location']) ){
					$assign_shift_datas = array();
					foreach($shift_datas_assign as $sskey => $ssvalue){
						$shift_code = $shift_datas_1[$sskey]['ShiftCode'];
						$shift_time = $shift_datas_1[$sskey]['BeginTime'].'-'.$shift_datas_1[$sskey]['EndTime'];
						$assign_shift_datas[$shift_code.' - '.$shift_time] = $ssvalue;						
					}
					// if(strtolower(trim($tvalue['location'])) != strtolower(trim($attendance_raw_datas[$next_key_2]['location']))){
					// 	$change = 1;
					// } else {
					// 	$change = 0;
					// }
					// echo '<pre>';
					// print_r($shift_datas_1);
					// echo 'Change : ' . $change;
					// echo '<br />';
					// echo '<pre>';
					// print_r(strtolower(trim($tvalue['location'])).' - '.strtolower(trim($attendance_raw_datas[$next_key_2]['location'])));
					// echo '<pre>';
					// print_r($attendance_raw_datas[$next_key_2]['location']);
					// echo '<pre>';
					// print_r($assign_shift_datas);
					// echo '<br />';
					// echo '<br />';
					//exit;
					$final_datas[] = array(
						'location' => $tvalue['location'],
						'assign_shift_datas' => $assign_shift_datas,
					);
				}
				$cnt ++;
			}
			//exit;
			// echo '<pre>';
			// print_r($final_datas);
			// exit;
			$this->data['final_datas'] = $final_datas;
			$this->data['total_shift_datas_assign'] = $total_shift_datas_assign;
		}

		
		$sql = "SELECT Location FROM [etimetracklite1].[dbo].[Employees] WHERE 1=1 AND Location IS NOT NULL ";
		$sql .= " GROUP BY Location ";
		//echo $sql;exit;
		$params = array();
		$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt = sqlsrv_query($conn, $sql, $params, $options);
		$location_datas = array();
		
		$location_datas[] = array(
			'location_id' => '',
			'location_name' => 'All',
		);
		
		while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			 $location_datas[] = array(
				'location_id' => $row['Location'],
				'location_name' => $row['Location'],
			);
		}
		/*
		$location_datas[] = array(
			'location_id' => '1',
			'location_name' => 'CHURCHGATE',
		);
		$location_datas[] = array(
			'location_id' => '2',
			'location_name' => 'MUMBAI CENTRAL',
		);
		*/
		$this->data['location_datas'] = $location_datas;
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->data['warning'])){
			$this->data['error_warning'] = $this->data['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		
		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_unit'] = $filter_unit;

		$this->template = 'report/attendance2.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function get_shift_datas($shift_datas, $emp_id, $in_date, $in_time, $in_date_time, $conn){
		date_default_timezone_set("Asia/Kolkata");
		$time_array = array();
		$hour_array = array();
		$act_array = array();
		$act_array_min = array();
		$min_array = array();
		$zero_hour_count = 0;
		foreach ($shift_datas as $skey => $svalue) {
			$shift_date_time = $in_date.' '.$svalue['BeginTime'];
			$start_date = new DateTime($shift_date_time);
			$since_start = $start_date->diff(new DateTime($in_date_time));
			if($since_start->d == 0){
				$time_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
				$hour_array[] = $since_start->h;
				$act_array[] = $svalue;
				if($since_start->h == 0){
					$act_array_min[] = $svalue;
					$min_array[] = $since_start->i;
					$zero_hour_count ++;
				}
			}
		}
		$akey = 0;
		if($zero_hour_count >= 2){
			$shift_work_hour = '0';
			if($min_array){
				$shift_work_hour = min($min_array);
			}
			$shift_worked_data = array();
			foreach ($min_array as $akey => $avalue) {
				if($avalue == $shift_work_hour){
					$shift_key = $akey;
					$shift_worked_data = $act_array_min[$akey];
				}
			}
		} else {
			$shift_work_hour = '0';
			if($hour_array){
				$shift_work_hour = min($hour_array);
			}
			$shift_worked_data = array();
			foreach ($hour_array as $akey => $avalue) {
				if($avalue == $shift_work_hour){
					$shift_key = $akey;
					$shift_worked_data = $act_array[$akey];
				}
			}	
		}
		$shift_datass['key'] = $akey;
		$shift_datass['shift_worked_data'] = $shift_worked_data;
		return $shift_datass;	
	}

	function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function export(){
		$this->language->load('report/attendance2');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		$data = array(
			'filter_date_start'	    => $filter_date_start,
			'filter_unit'			=> $filter_unit,
			'start'                 => ($page - 1) * 7000,
			'limit'                 => 7000
		);

		
		$serverName = "JMCFRM\ATTENDANCE";
		
		$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"avi", "PWD"=>"avi2123");
		//$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"staff", "PWD"=>"staff");
		// $serverName = "HP";
		// $connectionInfo = array("Database"=>"SmartOffice");
		$conn = sqlsrv_connect($serverName, $connectionInfo);
		if($conn) {
			//echo "Connection established.<br />";exit;
		} else {
			// echo '<pre>';
			// print_r(sqlsrv_errors());
			// exit;
		}
		
		$this->data['final_datas'] = array();
		$this->data['total_shift_datas_assign'] = array();
		$final_datas = array();
		$sql1 = "SELECT ShiftId, ShiftName, ShiftCode, BeginTime, EndTime FROM [etimetracklite1].[dbo].[Shifts] WHERE 1=1 AND BeginTime <> '00:00' AND EndTime <> '00:00' ORDER BY BeginTime ";
		//echo $sql;exit;
		$params1 = array();
		$options1 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt1 = sqlsrv_query($conn, $sql1, $params1, $options1);
		$shift_datas = array();
		$shift_datas_assign = array();
		$shift_datas_1 = array();
		$cnt = 0;
		while($row1 = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)) {
			$shift_datas[$cnt] = $row1;
			$shift_datas_1[$row1['ShiftId']] = $row1;
			$shift_datas_assign[$row1['ShiftId']] = 0;
			$total_shift_datas_assign[$row1['ShiftId']] = 0;
			$cnt ++;
		}

		// echo '<pre>';
		// print_r($shift_datas);
		// echo '<pre>';
		// print_r($shift_datas_assign);
		// exit;
		$month = date('m', strtotime($filter_date_start));
		$year = date('Y', strtotime($filter_date_start));
		$table_name = 'DeviceLogs_'.$month.'_'.$year;
		$sql = "SELECT * FROM [etimetracklite1].[dbo].[Employees] emp LEFT JOIN [etimetracklite1].[dbo].[".$table_name."] d ON emp.EmployeeCode = d.UserId WHERE 1=1 AND location <> 'All' AND location <> '' ";
		if($filter_date_start){
			$sql .= " AND CAST(d.LogDate as DATE) = '".$filter_date_start."' ";
		}
		if($filter_unit){
			$sql .= " AND emp.Location = '".$filter_unit."' ";
		}
		$sql .= " ORDER BY emp.Location, d.UserId, CAST(d.LogDate as DATE), CAST(d.LogDate as TIME) ";
		//echo $sql;exit;
		$params = array();
		$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt = sqlsrv_query($conn, $sql, $params, $options);
		//$row_count = sqlsrv_num_rows($stmt);
		//echo $row_count;exit;
		$attendance_raw_datas = array();
		while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			$emp_id = $row['UserId'];
			$location = $row['Location'];
			$log_dates = $row['LogDate'];
			$log_date = '';
			$log_time = '';
			$log_date_time = '';
			if($log_dates != null){	
				$log_date = $log_dates->format("Y-m-d");
				$log_time = $log_dates->format("H:i:s");
				$log_date_time = $log_dates->format("Y-m-d H:i:s");
			}
			if($log_date_time != ''){
				$attendance_raw_datas[] = array(
					'emp_id' => $emp_id,
					'location' => $location,
					'log_date_time' => $log_date_time,
					'log_date' => $log_date,
					'log_time' => $log_time,
				);
			}
		}
		// echo '<pre>';
		// print_r($attendance_raw_datas);
		// exit;
		$current_emp_id = '';
		$current_location_id = '';
		$cnt = 1;
		$cnt1 = 1;
		$attendance_datas = array();
		foreach($attendance_raw_datas as $tkey => $tvalue){
			// echo '<pre>';
			// print_r($tvalue);
			// exit;
			$emp_change = 0;
			if($current_emp_id != $tvalue['emp_id']){
				$emp_change = 1;
				$current_emp_id = $tvalue['emp_id'];
				$attendance_datas = array();
				$cnt = 1;
			}
			if(strtolower(trim($current_location_id)) != strtolower(trim($tvalue['location']))){
				$current_location_id = strtolower(trim($tvalue['location']));
				foreach($shift_datas_1 as $skey1 => $svalue1){
					$shift_datas_assign[$skey1] = 0;
				}
				$attendance_datas = array();
				$cnt = 1;
				$cnt1 ++;
			}
			$prev_key = $cnt - 1;
			if(isset($attendance_datas[$prev_key])){
				$prev_date_time = $attendance_datas[$prev_key]['log_date_time'];
				$current_date_time = $tvalue['log_date_time'];
				$start_date = new DateTime($prev_date_time);
				$since_start = $start_date->diff(new DateTime($current_date_time));
				$in = 1;
				if($since_start->d == 0 && $since_start->h == 0 && $since_start->i < 5){
					$in = 0;
				}
				if($since_start->d >= 1){
					$in = 0;
				}
				if($in == 1){
					$attendance_datas[$cnt] = array(
						'log_date_time' => $tvalue['log_date_time'],
						'log_date' => $tvalue['log_date'],
						'log_time' => $tvalue['log_time'],
					);
				}
			} else {
				$attendance_datas[$cnt] = array(
					'log_date_time' => $tvalue['log_date_time'],
					'log_date' => $tvalue['log_date'],
					'log_time' => $tvalue['log_time'],
				);	
			}

			$next_key_2 = $tkey + 1;
			if( (isset($attendance_raw_datas[$next_key_2]['emp_id']) && $tvalue['emp_id'] != $attendance_raw_datas[$next_key_2]['emp_id']) || !isset($attendance_raw_datas[$next_key_2]['emp_id']) ){
				if(count($attendance_datas) == 1){
					$attendance_datas[2] = array(
						'log_date_time' => date('Y-m-d H:i:s'),
						'log_date' => date('Y-m-d'),
						'log_time' => date('H:i:s'),	
					);
				}
				end($attendance_datas);
				$max_count = key($attendance_datas);
				$in_date_time = $attendance_datas[1]['log_date_time'];
				$in_date = $attendance_datas[1]['log_date'];
				$in_time = $attendance_datas[1]['log_time'];
				
				$out_date_time = $attendance_datas[$max_count]['log_date_time'];
				$start_date = new DateTime($in_date_time);
				$since_start = $start_date->diff(new DateTime($out_date_time));
				$working_hours = sprintf("%02d", $since_start->h);
				
				$cal_shift_datass = $this->get_shift_datas($shift_datas, $tvalue['emp_id'], $in_date, $in_time, $in_date_time, $conn);
				$shift_key = $cal_shift_datass['key'];
				$cal_shift_datas = $cal_shift_datass['shift_worked_data'];

				$shift_begin_time = $in_date.' '.$cal_shift_datas['BeginTime'];
				$shift_end_time = $in_date.' '.$cal_shift_datas['EndTime'];
				$start_date = new DateTime($shift_begin_time);
				$since_start = $start_date->diff(new DateTime($shift_end_time));
				$shift_working_hours = sprintf("%02d", $since_start->h);
				
				$difference_hours = $working_hours - $shift_working_hours;
				if($difference_hours > 1){
					$shift_id = $cal_shift_datas['ShiftId'];
					$shift_datas_assign[$shift_id] += 1;
					$total_shift_datas_assign[$shift_id] += 1;
					$next_key = $shift_key + 1;
					if(isset($shift_datas[$next_key])){
						$next_shift_id = $shift_datas[$next_key]['ShiftId'];
						$shift_datas_assign[$next_shift_id] += 1;
						$total_shift_datas_assign[$next_shift_id] += 1;
					}
				} else {
					$shift_id = $cal_shift_datas['ShiftId'];
					$shift_datas_assign[$shift_id] += 1;
					$total_shift_datas_assign[$shift_id] += 1;
				}
			}
			if( ( isset($attendance_raw_datas[$next_key_2]['location']) && ( strtolower(trim($tvalue['location'])) != strtolower(trim($attendance_raw_datas[$next_key_2]['location'])) ) ) || !isset($attendance_raw_datas[$next_key_2]['location']) ){
				$assign_shift_datas = array();
				foreach($shift_datas_assign as $sskey => $ssvalue){
					$shift_code = $shift_datas_1[$sskey]['ShiftCode'];
					$shift_time = $shift_datas_1[$sskey]['BeginTime'].'-'.$shift_datas_1[$sskey]['EndTime'];
					$assign_shift_datas[$shift_code.' - '.$shift_time] = $ssvalue;						
				}
				$final_datas[] = array(
					'location' => $tvalue['location'],
					'assign_shift_datas' => $assign_shift_datas,
				);
			}
			$cnt ++;
		}
		
		if($final_datas){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['total_shift_datas_assign'] = $total_shift_datas_assign;
			$template->data['filter_date_start'] = date('d-M-Y', strtotime($filter_date_start));
			if($filter_unit == '' || $filter_unit == 'All'){
				$filter_unit = 'All';
			}
			$template->data['filter_unit'] = $filter_unit;
			$template->data['title'] = 'Attendane Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/attendance2_html.tpl');
			//echo $html;exit;
			$filename = "Attendance_Report_".$filter_date_start.'.html';
			header('Content-type: text/html');
			header('Content-Disposition: attachment; filename='.$filename);
			echo $html;
			exit;
			// $filename = $statusss."_".$filter_date_start;
			// header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			// header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			// header("Expires: 0");
			// header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			// header("Cache-Control: private",false);
			// echo $html;
			//exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/attendance2', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
}
?>