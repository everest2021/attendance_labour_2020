<?php
class ControllerReportPerformance extends Controller { 
	public function index() {  

		if(isset($this->session->data['emp_code'])){
			$emp_code = $this->session->data['emp_code'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_dept'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_super'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		}

		// echo '<pre>';
		// print_r($this->request->get);
		// exit;

		$this->language->load('report/performance');
		$this->load->model('report/common_report');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$filter_date_start = '';
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = array();
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			//$filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $unit);
			//if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
				//$filter_date_end = $filter_date_end1;
			//}
		} else {
			$filter_date_end = date('Y-m-d');
		}
		
		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} elseif(isset($this->session->data['dept_name'])){
			$department = $this->session->data['dept_name'];
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = '0';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/performance', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/attendance');
		$this->load->model('transaction/transaction');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'company'			 	 => $company,
			'group'					 => $group,
			'day_close'              => 1,
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));

		$day = array();
		$days = $this->GetDays($filter_date_start, $filter_date_end);
		foreach ($days as $dkey => $dvalue) {
			$dates = explode('-', $dvalue);
			$day[$dkey]['day'] = $dates[2];
			$day[$dkey]['date'] = $dvalue;
		}
		$this->data['days'] = $day;

		//foreach ($day as $dkey => $dvalue) {
			//$results = $this->model_report_common_report->getAttendance($data);
		//}
		$final_datas = array();
		if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
			$results = $this->model_report_common_report->getemployees($data);
			foreach ($results as $rkey => $rvalue) {
				$performance_data = array();
				$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
				
				$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['basic_data']['department'] = $rvalue['department'];
				$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];

				$transaction_inn = '0';
				
				if($transaction_datas){
					foreach($transaction_datas as $tkey => $tvalue){
						$shift_inn = '';
						$leave_inn = '';
						$transaction_inn = '1';
						
						if($tvalue['firsthalf_status'] == '1'){
							$firsthalf_status = 'Present';
						} elseif($tvalue['firsthalf_status'] == '0'){
							$firsthalf_status = 'Absent';
						} else {
							$firsthalf_status = $tvalue['firsthalf_status'];
						}

						if($tvalue['secondhalf_status'] == '1'){
							$secondhalf_status = 'Present';
						} elseif($tvalue['secondhalf_status'] == '0'){
							$secondhalf_status = 'Absent';
						} else {
							$secondhalf_status = $tvalue['secondhalf_status'];
						}

						if($tvalue['firsthalf_status'] == 'WO') {
							if($tvalue['present_status'] == '1'){
								$firsthalf_status = 'W';
								$secondhalf_status = 'W';
							} elseif($tvalue['present_status'] == '0.5'){
								$firsthalf_status = 'W';
								$secondhalf_status = 'W';
							} else {
								$firsthalf_status = 'W';
								$secondhalf_status = 'W';
							}
						} elseif($tvalue['firsthalf_status'] == 'HLD') {
							if($tvalue['present_status'] == '1'){
								$firsthalf_status = 'PH';
								$seconhalf_status = 'PH';
							} elseif($tvalue['present_status'] == '0.5'){
								$firsthalf_status = 'PH';
								$seconhalf_status = 'H';
							} else {
								$firsthalf_status = 'H';
								$secondhalf_status = 'H';
							}
						}
						if($tvalue['shift_intime'] != '00:00:00' && $tvalue['shift_outtime'] != '00:00:00'){
							if($tvalue['leave_status'] == '0'){
								$shift_intime = $tvalue['shift_intime'];
								$shift_outtime = $tvalue['shift_outtime'];
								$shift_inn = '1';
							} else {
								$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$tvalue['date']."' AND `p_status` = '1' AND `a_status` = '1' ";
								$leave_data_res = $this->db->query($leave_data_sql);
								if($leave_data_res->num_rows > 0){
									//$shift_inn = '1';
									$leave_inn = '1';
									if($leave_data_res->row['type'] == ''){
										$shift_intime = $leave_data_res->row['leave_type'];
										$shift_outtime = $leave_data_res->row['leave_type'];
									} else {
										if($leave_data_res->row['type'] == 'F'){
											$shift_intime = $leave_data_res->row['leave_type'];
											$shift_outtime = $leave_data_res->row['leave_type'];
										} elseif($leave_data_res->row['type'] == '1'){
											$shift_intime = $leave_data_res->row['leave_type'];
											$shift_outtime = $secondhalf_status;
										} elseif($leave_data_res->row['type'] == '2'){
											$shift_intime = $firsthalf_status;
											$shift_outtime = $leave_data_res->row['leave_type'];
										}
									}
								} else {
									$shift_inn = '1';
									$shift_intime = $tvalue['shift_intime'];
									$shift_outtime = $tvalue['shift_outtime'];
								}
							}
						} else {
							$today_date = date('j', strtotime($tvalue['date']));
							$month = date('n', strtotime($tvalue['date']));
							$year = date('Y', strtotime($tvalue['date']));
							//$shift_data = $this->model_catalog_employee->getshift_id($rvalue['emp_code'], $today_date, $month, $year);
							//$shift_ids = explode('_', $shift_data);
							
							$shift_intime = '';
							$shift_outtime = '';
							$shift_inn = '';
							
							$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$tvalue['date']."' AND `p_status` = '1' AND `a_status` = '1' ";
							$leave_data_res = $this->db->query($leave_data_sql);
							if($leave_data_res->num_rows > 0){
								$leave_inn = '1';
								if($leave_data_res->row['type'] == ''){
									$shift_intime = $leave_data_res->row['leave_type'];
									$shift_outtime = $leave_data_res->row['leave_type'];
								} else {
									if($leave_data_res->row['type'] == 'F'){
										$shift_intime = $leave_data_res->row['leave_type'];
										$shift_outtime = $leave_data_res->row['leave_type'];
									} elseif($leave_data_res->row['type'] == '1'){
										$shift_intime = $leave_data_res->row['leave_type'];
										$shift_outtime = $secondhalf_status;
									} elseif($leave_data_res->row['type'] == '2'){
										$shift_intime = $firsthalf_status;
										$shift_outtime = $leave_data_res->row['leave_type'];
									}
								}
								// $shift_intime = $leave_data_res->row['leave_type'];
								// $shift_outtime = $leave_data_res->row['leave_type'];
							} else {
								if($tvalue['shift_intime'] != '00:00:00'){
									$shift_intime = $tvalue['shift_intime'];	
									$shift_outtime = $tvalue['shift_outtime'];
								} else {
									if($tvalue['firsthalf_status'] == '1'){
										$shift_intime = 'Pre';
									} elseif($tvalue['firsthalf_status'] == '0'){
										$shift_intime = 'Abs';
									} else {
										$shift_intime = $tvalue['firsthalf_status'];	
									}
									if($tvalue['secondhalf_status'] == '1'){
										$shift_outtime = 'Pre';
									} elseif($tvalue['secondhalf_status'] == '0'){
										$shift_outtime = 'Abs';
									} else {
										$shift_outtime = $tvalue['secondhalf_status'];
									}
								}
							}
						}

						$late_time_stat = '0';
						if($filter_type == 2){
							if(($shift_inn == '1' || $leave_inn == '1') && $transaction_inn == '1' && $shift_intime != '00:00:00' && $tvalue['act_intime'] != '00:00:00'){
								if($tvalue['late_time'] != '00:00:00'){
									$late_time_stat = 1;
								}
							}
						}

						$working_stat = 0;
						if($tvalue['absent_status'] == 1 || $tvalue['weekly_off'] <> 0 || $tvalue['holiday_id'] <> 0 || $tvalue['leave_status'] <> 0){
							$working_stat = 1;
						}

						if($filter_type == 3){
							if($shift_inn == '1' && $transaction_inn == '1' && $tvalue['act_outtime'] != '00:00:00' && $tvalue['act_intime'] != '00:00:00'){
								if($tvalue['late_time'] == '00:00:00' && $tvalue['early_time'] == '00:00:00'){
									$working_stat = 1;
								}
							}
						}

						$excess_stat = 0;
						if($filter_type == 5){
							if($shift_inn == '1' && $transaction_inn == '1' && $tvalue['act_outtime'] != '00:00:00' && $tvalue['act_intime'] != '00:00:00'){
								if(strtotime($tvalue['late_time']) > strtotime('00:00:00')){
									$excess_stat = 1;
								}
								if(strtotime($tvalue['early_time']) > strtotime('00:00:00')){
									$excess_stat = 1;
								}								
							}
						}

						$absent_stat = '0';
						if($filter_type == 4){
							if($tvalue['absent_status'] == 1 || $tvalue['absent_status'] == 0.5){
								$absent_stat = '1';
							}
						}

						$manual_stat = '0';
						if($filter_type == 6){
							if($tvalue['manual_status'] == 1){
								$manual_stat = '1';
							}
						}

						// if($tvalue['firsthalf_status'] == '1'){
						// 	$firsthalf_status = 'Pre';
						// } elseif($tvalue['firsthalf_status'] == '0'){
						// 	$firsthalf_status = 'Abs';
						// } else {
						// 	$firsthalf_status = $tvalue['firsthalf_status'];
						// }

						// if($tvalue['secondhalf_status'] == '1'){
						// 	$secondhalf_status = 'Pre';
						// } elseif($tvalue['secondhalf_status'] == '0'){
						// 	$secondhalf_status = 'Abs';
						// } else {
						// 	$secondhalf_status = $tvalue['secondhalf_status'];
						// }
						
						if($tvalue['manual_status'] == '1'){
							$firsthalf_status = $firsthalf_status.' (m)';
						}

						if($tvalue['shift_intime'] == '08:00:00'){
							$tvalue['late_time'] = '00:00:00';
							$tvalue['early_time'] = '00:00:00';
						}

						if(ctype_alpha($shift_intime) === false){
							$shift_intime = date('H:i:s', strtotime($shift_intime));
						}

						if(ctype_alpha($shift_outtime) === false){
							$shift_outtime = date('H:i:s', strtotime($shift_outtime));
						}

						if(ctype_alpha($tvalue['act_intime']) === false){
							$tvalue['act_intime'] = date('H:i:s', strtotime($tvalue['act_intime']));
						}

						if(ctype_alpha($tvalue['act_outtime']) === false){
							$tvalue['act_outtime'] = date('H:i:s', strtotime($tvalue['act_outtime']));
						}

						if(ctype_alpha($tvalue['late_time']) === false){
							$tvalue['late_time'] = date('H:i:s', strtotime($tvalue['late_time']));
						}

						if(ctype_alpha($tvalue['early_time']) === false){
							$tvalue['early_time'] = date('H:i:s', strtotime($tvalue['early_time']));
						}

						if(ctype_alpha($tvalue['working_time']) === false){
							$tvalue['working_time'] = date('H:i:s', strtotime($tvalue['working_time']));
						}

						if($tvalue['act_intime'] != '00:00:00' && $tvalue['act_outtime'] != '00:00:00'){
							$shift_name = 'GS';
						} else {
							$shift_name = 'NS';
						}

						$performance_data[$rvalue['emp_code']]['action'][$tvalue['date']] = array(
							'name' => $tvalue['emp_name'],
							'emp_code' => $tvalue['emp_id'],
							'shift_name' => $shift_name,
							'shift_intime' => $shift_intime,
							'shift_outtime' => $shift_outtime,
							'act_intime' => $tvalue['act_intime'],
							'act_outtime' => $tvalue['act_outtime'],
							'late_time' => $tvalue['late_time'],
							'early_time' => $tvalue['early_time'],
							'working_time' => $tvalue['working_time'],
							'date' => $tvalue['date'],
							'weekly_off' => $tvalue['weekly_off'],
							'holiday_id' => $tvalue['holiday_id'],
							'firsthalf_status' => $firsthalf_status,
							'secondhalf_status' => $secondhalf_status,
							'absent' => $tvalue['absent_status'],
							'present' => $tvalue['present_status'],
							'late_time_stat' => $late_time_stat,
							'working_stat' => $working_stat,
							'absent_stat' => $absent_stat,
							'excess_stat' => $excess_stat,
							'manual_stat' => $manual_stat
						);
					}
					//exit;
				} else {
					unset($final_datas[$rvalue['emp_code']]);
				}
				// echo '<pre>';
				// print_r($performance_data);
				// exit;
				
				if($performance_data){
					foreach ($day as $dkey => $dvalue) {
						foreach ($performance_data as $pkey => $pvalue) {
							if(isset($pvalue['action'][$dvalue['date']]['date'])){
							} else {
								$performance_data[$rvalue['emp_code']]['action'][$dvalue['date']] = array(
									'name' => $rvalue['name'],
									'emp_code' => $rvalue['emp_code'],
									'shift_name' => 'NS',
									'shift_intime' => '',
									'shift_outtime' => '',
									'act_intime' => '',
									'act_outtime' => '',
									'late_time' => '',
									'early_time' => '',
									'working_time' => '',
									'weekly_off' => 0,
									'holiday_id' => 0,
									'absent' => 1,
									'date' => $dvalue['date'],
									'late_time_stat' => 0,
									'firsthalf_status' => 'Absent',
									'secondhalf_status' => 'Absent',
									'present' => 0,
									'late_time_stat' => 0,
									'working_stat' => 0,
									'absent_stat' => 1,
									'excess_stat' => 0,
									'manual_stat' => 0
								);			
							}
						}
					}
				}
				// echo '<pre>';
				// print_r($performance_data);
				// exit;
				foreach ($performance_data as $pkey => $pvalue) {
					$per_sort_data = $this->array_sort($pvalue['action'], 'date', SORT_ASC);
					$late_inn = 0;
					$working_inn = 0;
					$abs_in = 0;
					$manual_in = 0;
					foreach($per_sort_data as $akey => $avalue){
						if($filter_type == 2 && $avalue['late_time_stat'] == 1){
							//$late_inn ++;
						} elseif($filter_type == 2 && $avalue['late_time_stat'] == 0) {
							$per_sort_data[$akey]['shift_name'] = '';
							$per_sort_data[$akey]['shift_intime'] = '';
							$per_sort_data[$akey]['shift_outtime'] = '';
							$per_sort_data[$akey]['act_intime'] = '';
							$per_sort_data[$akey]['act_outtime'] = '';
							$per_sort_data[$akey]['late_time'] = '';
							$per_sort_data[$akey]['early_time'] = '';
							$per_sort_data[$akey]['working_time'] = '';
							$per_sort_data[$akey]['firsthalf_status'] = '';
							$per_sort_data[$akey]['secondhalf_status'] = '';
							$late_inn ++;
							//$late_inn = 0;
							//unset($per_sort_data['action'][$akey]);
						}
						if($filter_type == 3 && $avalue['working_stat'] == 0){
							$working_inn ++;
						} elseif($filter_type == 3 && $avalue['working_stat'] == 1) {
							$per_sort_data[$akey]['shift_name'] = '';
							$per_sort_data[$akey]['shift_intime'] = '';
							$per_sort_data[$akey]['shift_outtime'] = '';
							$per_sort_data[$akey]['act_intime'] = '';
							$per_sort_data[$akey]['act_outtime'] = '';
							$per_sort_data[$akey]['late_time'] = '';
							$per_sort_data[$akey]['early_time'] = '';
							$per_sort_data[$akey]['working_time'] = '';
							$per_sort_data[$akey]['firsthalf_status'] = '';
							$per_sort_data[$akey]['secondhalf_status'] = '';
							//$working_inn = 0;
							//unset($per_sort_data['action'][$akey]);
						}

						if($filter_type == 4 && $avalue['absent_stat'] == 0) {
							$abs_in ++;
							$per_sort_data[$akey]['shift_name'] = '';
							$per_sort_data[$akey]['shift_intime'] = '';
							$per_sort_data[$akey]['shift_outtime'] = '';
							$per_sort_data[$akey]['act_intime'] = '';
							$per_sort_data[$akey]['act_outtime'] = '';
							$per_sort_data[$akey]['late_time'] = '';
							$per_sort_data[$akey]['early_time'] = '';
							$per_sort_data[$akey]['working_time'] = '';
							$per_sort_data[$akey]['firsthalf_status'] = '';
							$per_sort_data[$akey]['secondhalf_status'] = '';
							//$late_inn = 0;
							//unset($per_sort_data[$akey]);
						}

						if($filter_type == 6 && $avalue['manual_stat'] == 0) {
							$manual_in ++;
							$per_sort_data[$akey]['shift_name'] = '';
							$per_sort_data[$akey]['shift_intime'] = '';
							$per_sort_data[$akey]['shift_outtime'] = '';
							$per_sort_data[$akey]['act_intime'] = '';
							$per_sort_data[$akey]['act_outtime'] = '';
							$per_sort_data[$akey]['late_time'] = '';
							$per_sort_data[$akey]['early_time'] = '';
							$per_sort_data[$akey]['working_time'] = '';
							$per_sort_data[$akey]['firsthalf_status'] = '';
							$per_sort_data[$akey]['secondhalf_status'] = '';
							//$late_inn = 0;
							//unset($per_sort_data[$akey]);
						}

						if($filter_type == 5 && $avalue['excess_stat'] == 0) {
							$per_sort_data[$akey]['shift_name'] = '';
							$per_sort_data[$akey]['shift_intime'] = '';
							$per_sort_data[$akey]['shift_outtime'] = '';
							$per_sort_data[$akey]['act_intime'] = '';
							$per_sort_data[$akey]['act_outtime'] = '';
							$per_sort_data[$akey]['late_time'] = '';
							$per_sort_data[$akey]['early_time'] = '';
							$per_sort_data[$akey]['working_time'] = '';
							$per_sort_data[$akey]['firsthalf_status'] = '';
							$per_sort_data[$akey]['secondhalf_status'] = '';
							//$late_inn = 0;
							//unset($per_sort_data['action'][$akey]);
						}	
					}
					// echo $late_inn;
					// echo '<pre>';
					// print_r(count($per_sort_data));
					// exit;
					if($filter_type == 2){
						if($late_inn == count($per_sort_data)){
							unset($final_datas[$rvalue['emp_code']]);	
						} else {
							$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
						}
						//$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
					} elseif($working_inn > 3 && $filter_type == 3){
						$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
					} elseif($filter_type == 1 || $filter_type == 5) {
						$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
					} elseif($filter_type == 4) {
						if($abs_in == count($per_sort_data)){
							unset($final_datas[$rvalue['emp_code']]);	
						} else {
							$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
						}
					} elseif($filter_type == 6) {
						if($manual_in == count($per_sort_data)){
							unset($final_datas[$rvalue['emp_code']]);	
						} else {
							$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
						}
					} else {
						unset($final_datas[$rvalue['emp_code']]);
					}
				}
			}
			//exit;
		}

		$this->data['final_datas'] = $final_datas;
		// echo '<pre>';
  		// print_r($this->data['final_datas']);
  		// exit;
		$type_data = array(
			'1' => 'Default',
			'2' => 'Late',
			'3' => 'Loss',
			'4' => 'Absent',
			'5' => 'Excess',
			'6' => 'Manual'
		);
		$this->data['type_data'] = $type_data;


		$fields_data = array(
			'1' => 'Shift',
			'2' => 'Shift In',
			'3' => 'Shift Out',
			'4' => 'Attended In',
			'5' => 'Attended Out',
			'6' => 'First Half',
			'7' => 'Second Half',
			'8' => 'Late By',
			'9' => 'Early By',
			'10' => 'Working Hours',
		);
		$this->data['fields_data'] = $fields_data;

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $division,
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		$site_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['unit_data'] = $site_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;


		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_data'] = $company_data;

		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;
		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		$group_data['1'] = 'All WITHOUT OFFICIALS';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}

		if(isset($this->session->data['is_dept']) && $this->session->data['is_dept'] == '1'){
			$user_dept = 1;
			$is_dept = 1;
		} elseif(isset($this->session->data['is_user'])){
			$user_dept = 1;
			$is_dept = 0;
		} else {
			$user_dept = 0;
			$is_dept = 0;
		}
		$this->data['user_dept'] = $user_dept;
		$this->data['is_dept'] = $is_dept;

		// echo '<pre>';
		// print_r($filter_fields);
		// exit;

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_type'] = $filter_type;
		$this->data['filter_fields'] = $filter_fields;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['division'] = $division;
		$this->data['region'] = $region;
		$this->data['company'] = explode(',', $company);
		$this->data['group'] = $group;

		$this->template = 'report/performance.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function export(){
		$this->language->load('report/performance');
		$this->load->model('report/common_report');
		$this->load->model('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$from = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			// $filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start);
			// if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
			// 	$filter_date_end = $filter_date_end1;
			// }
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = array();
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'company'			 	 => $company,
			'group'					 => $group 
		);

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));

		$day = array();
		$days = $this->GetDays($filter_date_start, $filter_date_end);
		foreach ($days as $dkey => $dvalue) {
			$dates = explode('-', $dvalue);
			$day[$dkey]['day'] = $dates[2];
			$day[$dkey]['date'] = $dvalue;
		}

		$this->data['days'] = $day;

		//foreach ($day as $dkey => $dvalue) {
			//$results = $this->model_report_common_report->getAttendance($data);
		//}
		$final_datas = array();
		$results = $this->model_report_common_report->getemployees($data);
		foreach ($results as $rkey => $rvalue) {
			$performance_data = array();
			$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
			
			$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
			$final_datas[$rvalue['emp_code']]['basic_data']['department'] = $rvalue['department'];
			$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];

			$transaction_inn = '0';
			
			if($transaction_datas){
				foreach($transaction_datas as $tkey => $tvalue){
					$shift_inn = '';
					$leave_inn = '';
					$transaction_inn = '1';
					
					if($tvalue['firsthalf_status'] == '1'){
						$firsthalf_status = 'Present';
					} elseif($tvalue['firsthalf_status'] == '0'){
						$firsthalf_status = 'Absent';
					} else {
						$firsthalf_status = $tvalue['firsthalf_status'];
					}

					if($tvalue['secondhalf_status'] == '1'){
						$secondhalf_status = 'Present';
					} elseif($tvalue['secondhalf_status'] == '0'){
						$secondhalf_status = 'Absent';
					} else {
						$secondhalf_status = $tvalue['secondhalf_status'];
					}

					if($tvalue['firsthalf_status'] == 'WO') {
						if($tvalue['present_status'] == '1'){
							$firsthalf_status = 'W';
							$secondhalf_status = 'W';
						} elseif($tvalue['present_status'] == '0.5'){
							$firsthalf_status = 'W';
							$secondhalf_status = 'W';
						} else {
							$firsthalf_status = 'W';
							$secondhalf_status = 'W';
						}
					} elseif($tvalue['firsthalf_status'] == 'HLD') {
						if($tvalue['present_status'] == '1'){
							$firsthalf_status = 'PH';
							$seconhalf_status = 'PH';
						} elseif($tvalue['present_status'] == '0.5'){
							$firsthalf_status = 'PH';
							$seconhalf_status = 'H';
						} else {
							$firsthalf_status = 'H';
							$secondhalf_status = 'H';
						}
					}
					if($tvalue['shift_intime'] != '00:00:00' && $tvalue['shift_outtime'] != '00:00:00'){
						if($tvalue['leave_status'] == '0'){
							$shift_intime = $tvalue['shift_intime'];
							$shift_outtime = $tvalue['shift_outtime'];
							$shift_inn = '1';
						} else {
							$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$tvalue['date']."' AND `p_status` = '1' AND `a_status` = '1' ";
							$leave_data_res = $this->db->query($leave_data_sql);
							if($leave_data_res->num_rows > 0){
								//$shift_inn = '1';
								$leave_inn = '1';
								if($leave_data_res->row['type'] == ''){
									$shift_intime = $leave_data_res->row['leave_type'];
									$shift_outtime = $leave_data_res->row['leave_type'];
								} else {
									if($leave_data_res->row['type'] == 'F'){
										$shift_intime = $leave_data_res->row['leave_type'];
										$shift_outtime = $leave_data_res->row['leave_type'];
									} elseif($leave_data_res->row['type'] == '1'){
										$shift_intime = $leave_data_res->row['leave_type'];
										$shift_outtime = $secondhalf_status;
									} elseif($leave_data_res->row['type'] == '2'){
										$shift_intime = $firsthalf_status;
										$shift_outtime = $leave_data_res->row['leave_type'];
									}
								}
							} else {
								$shift_inn = '1';
								$shift_intime = $tvalue['shift_intime'];
								$shift_outtime = $tvalue['shift_outtime'];
							}
						}
					} else {
						$today_date = date('j', strtotime($tvalue['date']));
						$month = date('n', strtotime($tvalue['date']));
						$year = date('Y', strtotime($tvalue['date']));
						//$shift_data = $this->model_catalog_employee->getshift_id($rvalue['emp_code'], $today_date, $month, $year);
						//$shift_ids = explode('_', $shift_data);
						
						$shift_intime = '';
						$shift_outtime = '';
						$shift_inn = '';
						
						$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$tvalue['date']."' AND `p_status` = '1' AND `a_status` = '1' ";
						$leave_data_res = $this->db->query($leave_data_sql);
						if($leave_data_res->num_rows > 0){
							$leave_inn = '1';
							if($leave_data_res->row['type'] == ''){
								$shift_intime = $leave_data_res->row['leave_type'];
								$shift_outtime = $leave_data_res->row['leave_type'];
							} else {
								if($leave_data_res->row['type'] == 'F'){
									$shift_intime = $leave_data_res->row['leave_type'];
									$shift_outtime = $leave_data_res->row['leave_type'];
								} elseif($leave_data_res->row['type'] == '1'){
									$shift_intime = $leave_data_res->row['leave_type'];
									$shift_outtime = $secondhalf_status;
								} elseif($leave_data_res->row['type'] == '2'){
									$shift_intime = $firsthalf_status;
									$shift_outtime = $leave_data_res->row['leave_type'];
								}
							}
							// $shift_intime = $leave_data_res->row['leave_type'];
							// $shift_outtime = $leave_data_res->row['leave_type'];
						} else {
							if($tvalue['shift_intime'] != '00:00:00'){
								$shift_intime = $tvalue['shift_intime'];	
								$shift_outtime = $tvalue['shift_outtime'];
							} else {
								if($tvalue['firsthalf_status'] == '1'){
									$shift_intime = 'Pre';
								} elseif($tvalue['firsthalf_status'] == '0'){
									$shift_intime = 'Abs';
								} else {
									$shift_intime = $tvalue['firsthalf_status'];	
								}
								if($tvalue['secondhalf_status'] == '1'){
									$shift_outtime = 'Pre';
								} elseif($tvalue['secondhalf_status'] == '0'){
									$shift_outtime = 'Abs';
								} else {
									$shift_outtime = $tvalue['secondhalf_status'];
								}
							}
						}
					}

					$late_time_stat = '0';
					if($filter_type == 2){
						if(($shift_inn == '1' || $leave_inn == '1') && $transaction_inn == '1' && $shift_intime != '00:00:00' && $tvalue['act_intime'] != '00:00:00'){
							if($tvalue['late_time'] != '00:00:00'){
								$late_time_stat = 1;
							}
						}
					}

					$working_stat = 0;
					if($tvalue['absent_status'] == 1 || $tvalue['weekly_off'] <> 0 || $tvalue['holiday_id'] <> 0 || $tvalue['leave_status'] <> 0){
						$working_stat = 1;
					}

					if($filter_type == 3){
						if($shift_inn == '1' && $transaction_inn == '1' && $tvalue['act_outtime'] != '00:00:00' && $tvalue['act_intime'] != '00:00:00'){
							if($tvalue['late_time'] == '00:00:00' && $tvalue['early_time'] == '00:00:00'){
								$working_stat = 1;
							}
						}
					}

					$excess_stat = 0;
					if($filter_type == 5){
						if($shift_inn == '1' && $transaction_inn == '1' && $tvalue['act_outtime'] != '00:00:00' && $tvalue['act_intime'] != '00:00:00'){
							if(strtotime($tvalue['late_time']) > strtotime('00:00:00')){
								$excess_stat = 1;
							}
							if(strtotime($tvalue['early_time']) > strtotime('00:00:00')){
								$excess_stat = 1;
							}								
						}
					}

					$absent_stat = '0';
					if($filter_type == 4){
						if($tvalue['absent_status'] == 1 || $tvalue['absent_status'] == 0.5){
							$absent_stat = '1';
						}
					}

					$manual_stat = '0';
					if($filter_type == 6){
						if($tvalue['manual_status'] == 1){
							$manual_stat = '1';
						}
					}

					// if($tvalue['firsthalf_status'] == '1'){
					// 	$firsthalf_status = 'Pre';
					// } elseif($tvalue['firsthalf_status'] == '0'){
					// 	$firsthalf_status = 'Abs';
					// } else {
					// 	$firsthalf_status = $tvalue['firsthalf_status'];
					// }

					// if($tvalue['secondhalf_status'] == '1'){
					// 	$secondhalf_status = 'Pre';
					// } elseif($tvalue['secondhalf_status'] == '0'){
					// 	$secondhalf_status = 'Abs';
					// } else {
					// 	$secondhalf_status = $tvalue['secondhalf_status'];
					// }

					if($tvalue['shift_intime'] == '08:00:00'){
						$tvalue['late_time'] = '00:00:00';
						$tvalue['early_time'] = '00:00:00';
					}
					
					if($tvalue['manual_status'] == '1'){
						$firsthalf_status = $firsthalf_status.' (m)';
					}

					if(ctype_alpha($shift_intime) === false){
						$shift_intime = date('H:i:s', strtotime($shift_intime));
					}

					if(ctype_alpha($shift_outtime) === false){
						$shift_outtime = date('H:i:s', strtotime($shift_outtime));
					}

					if(ctype_alpha($tvalue['act_intime']) === false){
						$tvalue['act_intime'] = date('H:i:s', strtotime($tvalue['act_intime']));
					}

					if(ctype_alpha($tvalue['act_outtime']) === false){
						$tvalue['act_outtime'] = date('H:i:s', strtotime($tvalue['act_outtime']));
					}

					if(ctype_alpha($tvalue['late_time']) === false){
						$tvalue['late_time'] = date('H:i:s', strtotime($tvalue['late_time']));
					}

					if(ctype_alpha($tvalue['early_time']) === false){
						$tvalue['early_time'] = date('H:i:s', strtotime($tvalue['early_time']));
					}

					if(ctype_alpha($tvalue['working_time']) === false){
						$tvalue['working_time'] = date('H:i:s', strtotime($tvalue['working_time']));
					}

					if($tvalue['act_intime'] != '00:00:00' && $tvalue['act_outtime'] != '00:00:00'){
						$shift_name = 'GS';
					} else {
						$shift_name = 'NS';
					}

					$performance_data[$rvalue['emp_code']]['action'][$tvalue['date']] = array(
						'name' => $tvalue['emp_name'],
						'emp_code' => $tvalue['emp_id'],
						'shift_name' => $shift_name,
						'shift_intime' => $shift_intime,
						'shift_outtime' => $shift_outtime,
						'act_intime' => $tvalue['act_intime'],
						'act_outtime' => $tvalue['act_outtime'],
						'late_time' => $tvalue['late_time'],
						'early_time' => $tvalue['early_time'],
						'working_time' => $tvalue['working_time'],
						'date' => $tvalue['date'],
						'weekly_off' => $tvalue['weekly_off'],
						'holiday_id' => $tvalue['holiday_id'],
						'firsthalf_status' => $firsthalf_status,
						'secondhalf_status' => $secondhalf_status,
						'absent' => $tvalue['absent_status'],
						'present' => $tvalue['present_status'],
						'late_time_stat' => $late_time_stat,
						'working_stat' => $working_stat,
						'absent_stat' => $absent_stat,
						'excess_stat' => $excess_stat,
						'manual_stat' => $manual_stat
					);
				}
			} else {
				unset($final_datas[$rvalue['emp_code']]);
			}
			// echo '<pre>';
			// print_r($performance_data);
			// exit;
			
			if($performance_data){
				foreach ($day as $dkey => $dvalue) {
					foreach ($performance_data as $pkey => $pvalue) {
						if(isset($pvalue['action'][$dvalue['date']]['date'])){
						} else {
							$performance_data[$rvalue['emp_code']]['action'][$dvalue['date']] = array(
								'name' => $rvalue['name'],
								'emp_code' => $rvalue['emp_code'],
								'shift_name' => 'NS',
								'shift_intime' => '',
								'shift_outtime' => '',
								'act_intime' => '',
								'act_outtime' => '',
								'late_time' => '',
								'early_time' => '',
								'working_time' => '',
								'weekly_off' => 0,
								'holiday_id' => 0,
								'absent' => 1,
								'date' => $dvalue['date'],
								'late_time_stat' => 0,
								'firsthalf_status' => 'Absent',
								'secondhalf_status' => 'Absent',
								'present' => 0,
								'late_time_stat' => 0,
								'working_stat' => 0,
								'absent_stat' => 1,
								'excess_stat' => 0,
								'manual_stat' => 0
							);			
						}
					}
				}
			}
			// echo '<pre>';
			// print_r($performance_data);
			// exit;
			foreach ($performance_data as $pkey => $pvalue) {
				$per_sort_data = $this->array_sort($pvalue['action'], 'date', SORT_ASC);
				$late_inn = 0;
				$working_inn = 0;
				$abs_in = 0;
				$manual_in = 0;
				foreach($per_sort_data as $akey => $avalue){
					if($filter_type == 2 && $avalue['late_time_stat'] == 1){
						//$late_inn ++;
					} elseif($filter_type == 2 && $avalue['late_time_stat'] == 0) {
						$per_sort_data[$akey]['shift_name'] = '';
						$per_sort_data[$akey]['shift_intime'] = '';
						$per_sort_data[$akey]['shift_outtime'] = '';
						$per_sort_data[$akey]['act_intime'] = '';
						$per_sort_data[$akey]['act_outtime'] = '';
						$per_sort_data[$akey]['late_time'] = '';
						$per_sort_data[$akey]['early_time'] = '';
						$per_sort_data[$akey]['working_time'] = '';
						$per_sort_data[$akey]['firsthalf_status'] = '';
						$per_sort_data[$akey]['secondhalf_status'] = '';
						$late_inn ++;
						//$late_inn = 0;
						//unset($per_sort_data['action'][$akey]);
					}
					if($filter_type == 3 && $avalue['working_stat'] == 0){
						$working_inn ++;
					} elseif($filter_type == 3 && $avalue['working_stat'] == 1) {
						$per_sort_data[$akey]['shift_name'] = '';
						$per_sort_data[$akey]['shift_intime'] = '';
						$per_sort_data[$akey]['shift_outtime'] = '';
						$per_sort_data[$akey]['act_intime'] = '';
						$per_sort_data[$akey]['act_outtime'] = '';
						$per_sort_data[$akey]['late_time'] = '';
						$per_sort_data[$akey]['early_time'] = '';
						$per_sort_data[$akey]['working_time'] = '';
						$per_sort_data[$akey]['firsthalf_status'] = '';
						$per_sort_data[$akey]['secondhalf_status'] = '';
						//$working_inn = 0;
						//unset($per_sort_data['action'][$akey]);
					}

					if($filter_type == 4 && $avalue['absent_stat'] == 0) {
						$abs_in ++;
						$per_sort_data[$akey]['shift_name'] = '';
						$per_sort_data[$akey]['shift_intime'] = '';
						$per_sort_data[$akey]['shift_outtime'] = '';
						$per_sort_data[$akey]['act_intime'] = '';
						$per_sort_data[$akey]['act_outtime'] = '';
						$per_sort_data[$akey]['late_time'] = '';
						$per_sort_data[$akey]['early_time'] = '';
						$per_sort_data[$akey]['working_time'] = '';
						$per_sort_data[$akey]['firsthalf_status'] = '';
						$per_sort_data[$akey]['secondhalf_status'] = '';
						//$late_inn = 0;
						//unset($per_sort_data[$akey]);
					}

					if($filter_type == 6 && $avalue['manual_stat'] == 0) {
						$manual_in ++;
						$per_sort_data[$akey]['shift_name'] = '';
						$per_sort_data[$akey]['shift_intime'] = '';
						$per_sort_data[$akey]['shift_outtime'] = '';
						$per_sort_data[$akey]['act_intime'] = '';
						$per_sort_data[$akey]['act_outtime'] = '';
						$per_sort_data[$akey]['late_time'] = '';
						$per_sort_data[$akey]['early_time'] = '';
						$per_sort_data[$akey]['working_time'] = '';
						$per_sort_data[$akey]['firsthalf_status'] = '';
						$per_sort_data[$akey]['secondhalf_status'] = '';
						//$late_inn = 0;
						//unset($per_sort_data[$akey]);
					}

					if($filter_type == 5 && $avalue['excess_stat'] == 0) {
						$per_sort_data[$akey]['shift_name'] = '';
						$per_sort_data[$akey]['shift_intime'] = '';
						$per_sort_data[$akey]['shift_outtime'] = '';
						$per_sort_data[$akey]['act_intime'] = '';
						$per_sort_data[$akey]['act_outtime'] = '';
						$per_sort_data[$akey]['late_time'] = '';
						$per_sort_data[$akey]['early_time'] = '';
						$per_sort_data[$akey]['working_time'] = '';
						$per_sort_data[$akey]['firsthalf_status'] = '';
						$per_sort_data[$akey]['secondhalf_status'] = '';
						//$late_inn = 0;
						//unset($per_sort_data['action'][$akey]);
					}	
				}
				// echo $late_inn;
				// echo '<pre>';
				// print_r(count($per_sort_data));
				// exit;
				if($filter_type == 2){
					if($late_inn == count($per_sort_data)){
						unset($final_datas[$rvalue['emp_code']]);	
					} else {
						$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
					}
					//$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
				} elseif($working_inn > 3 && $filter_type == 3){
					$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
				} elseif($filter_type == 1 || $filter_type == 5) {
					$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
				} elseif($filter_type == 4) {
					if($abs_in == count($per_sort_data)){
						unset($final_datas[$rvalue['emp_code']]);	
					} else {
						$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
					}
				} elseif($filter_type == 6) {
					if($manual_in == count($per_sort_data)){
						unset($final_datas[$rvalue['emp_code']]);	
					} else {
						$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
					}
				} else {
					unset($final_datas[$rvalue['emp_code']]);
				}
			}
		}
		

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		$url .= '&once=1';

		$final_datas = array_chunk($final_datas, 3);

		// echo '<pre>';
		// print_r($final_datas);
		// exit;
		
		if($final_datas){
			if($filter_type == 1){
				$report_type = 'Defult';
			} elseif($filter_type == 2){
				$report_type = 'Late';
			} elseif ($filter_type == 3) {
				$report_type = 'Less Time';
			} elseif($filter_type == 4){
				$report_type = 'Absent';
			} elseif($filter_type == 5){
				$report_type = 'Excess';
			} elseif($filter_type == 6){
				$report_type = 'Manual';
			}

			if (isset($this->request->get['unit'])) {
				$unit_names = $this->db->query("SELECT `unit` FROM `oc_unit` WHERE `unit_id` = '".$this->request->get['unit']."' ");
				if($unit_names->num_rows > 0){
					$unit_name = $unit_names->row['unit'];
				} else {
					$unit_name = '';
				}
				$filter_unit = html_entity_decode($unit_name);
			} else {
				$filter_unit = 'All';
			}

			if (isset($this->request->get['department'])) {
				$department_names = $this->db->query("SELECT `d_name` FROM `oc_department` WHERE `department_id` = '".$this->request->get['department']."' ");
				if($department_names->num_rows > 0){
					$department_name = $department_names->row['d_name'];
				} else {
					$department_name = '';
				}
				$filter_department = html_entity_decode($department_name);
			} else {
				$filter_department = 'All';
			}

			if (isset($this->request->get['division'])) {
				$division_names = $this->db->query("SELECT `division` FROM `oc_division` WHERE `division_id` = '".$this->request->get['division']."' ");
				if($division_names->num_rows > 0){
					$division_name = $division_names->row['division'];
				} else {
					$division_name = '';
				}
				$filter_division = html_entity_decode($division_name);
			} else {
				$filter_division = 'All';
			}

			if (isset($this->request->get['region'])) {
				$region_names = $this->db->query("SELECT `region` FROM `oc_region` WHERE `region_id` = '".$this->request->get['region']."' ");
				if($region_names->num_rows > 0){
					$region_name = $region_names->row['region'];
				} else {
					$region_name = '';
				}
				$filter_region = html_entity_decode($region_name);
			} else {
				$filter_region = 'All';
			}

			if (isset($this->request->get['company'])) {
				$company_names = $this->db->query("SELECT `company` FROM `oc_company` WHERE `company_id` = '".$this->request->get['company']."' ");
				if($company_names->num_rows > 0){
					$company_name = $company_names->row['company'];
				} else {
					$company_name = '';
				}
				$filter_company = html_entity_decode($company_name);
			} else {
				$filter_company = 'All';
			}

			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datass'] = $final_datas;
			$template->data['date_start'] = date('d-F-Y', strtotime($filter_date_start));
			$template->data['date_end'] = date('d-F-Y', strtotime($filter_date_end));
			$template->data['days'] = $day;
			$template->data['report_type'] = $report_type;
			$template->data['filter_fields'] = $filter_fields;
			$template->data['filter_division'] = $filter_division;
			$template->data['filter_region'] = $filter_region;
			$template->data['filter_company'] = $filter_company;
			$template->data['filter_unit'] = $filter_unit;
			$template->data['filter_department'] = $filter_department;
			$template->data['title'] = 'Monthly Detailed Attendance Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/performance_html.tpl');
			//echo $html;exit;
			$filename = "Periodic_Report";
			
			if($filter_type == 6){
				header('Content-type: text/html');
				header('Content-Disposition: attachment; filename='.$filename.".html");
				echo $html;
			} else {
				header("Content-Type: application/vnd.ms-excel; charset=utf-8");
				header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private",false);
				echo $html;
				exit;
			}		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/performance', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function export_hrms(){
		$this->language->load('report/performance');
		$this->load->model('report/common_report');
		$this->load->model('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$from = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			// $filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start);
			// if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
			// 	$filter_date_end = $filter_date_end1;
			// }
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'group'					 => $group 
		);
		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));
		// $day = array();
		// $days = $this->GetDays($filter_date_start, $filter_date_end);
		// foreach ($days as $dkey => $dvalue) {
		// 	$dates = explode('-', $dvalue);
		// 	$day[$dkey]['day'] = $dates[2];
		// 	$day[$dkey]['date'] = $dvalue;
		// }
		// $this->data['days'] = $day;
		$final_datas = array();
		$results = $this->model_report_common_report->getemployees($data);
		foreach ($results as $rkey => $rvalue) {
			$performance_data = array();
			$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
			if($transaction_datas){
				foreach($transaction_datas as $tkey => $tvalue){
					$final_datas[$rvalue['emp_code']]['EmployeeId'] = $rvalue['emp_code'];
					$final_datas[$rvalue['emp_code']]['EmployeeName'] = $rvalue['name'];
					$final_datas[$rvalue['emp_code']]['PunchID'] = $rvalue['emp_code'];
					$final_datas[$rvalue['emp_code']]['Department'] = $rvalue['department'];
					$final_datas[$rvalue['emp_code']]['Site'] = $rvalue['unit'];
					$final_datas[$rvalue['emp_code']]['Region'] = $rvalue['region'];
					$final_datas[$rvalue['emp_code']]['Division'] = $rvalue['division'];
					$final_datas[$rvalue['emp_code']]['AttendDate'] = date('m/d/Y', strtotime($tvalue['date']));
					
					$date_to_compare = $tvalue['date'];
					if(strtotime($rvalue['doj']) > strtotime($date_to_compare)){
						$status = 'x';
					} elseif($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
						$status = 'p';
					} elseif($tvalue['weekly_off'] != '0'){
						$working_times = explode(':', $tvalue['working_time']);
						$working_hours = $working_times[0];
						if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
						//if($working_hours >= '06'){
							$status = 'W';
						} else {
							$status = 'W';	
						}
					} elseif($tvalue['holiday_id'] != '0'){
						$working_times = explode(':', $tvalue['working_time']);
						$working_hours = $working_times[0];
						if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
						//if($working_hours >= '06'){
							$status = 'PPH';
						} else {
							$status = 'PH';
						}
					} elseif($tvalue['act_intime'] != '00:00:00' && $tvalue['act_outtime'] == '00:00:00'){
						$status = 'E';
					} elseif($tvalue['leave_status'] != '0'){
						if($tvalue['leave_status'] == '0.5'){
							$status = 'PL';
						} else {
							$status = 'PL';
						}
					} elseif($tvalue['absent_status'] == '1' || $tvalue['absent_status'] == '0.5'){
						$status = 'A';
					}

					$final_datas[$rvalue['emp_code']]['Status'] = $status;
					$final_datas[$rvalue['emp_code']]['InTime'] = $tvalue['act_intime'];
					$final_datas[$rvalue['emp_code']]['OutTime'] = $tvalue['act_outtime'];
					$final_datas[$rvalue['emp_code']]['TotaHour'] = $tvalue['working_time'];
				}
			}
		}
		// echo '<pre>';
		// print_r($final_datas);
		// exit;
		$url = '';
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		$url .= '&once=1';
		//$final_datas = array_chunk($final_datas, 3);
		if($final_datas){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			/*
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['filter_date_start'] = date('d-m-Y', strtotime($filter_date_start));
			$template->data['filter_date_end'] = date('d-m-Y', strtotime($filter_date_end));
			$template->data['title'] = 'Periodic Report HRMS';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/performance_hrms_html.tpl');
			//echo $html;exit;
			$filename = "Periodic_Report_HRMS";
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename.".html");
			// echo $html;
			//header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename=".$filename.".csv");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
			*/
			$fp = fopen(DIR_DOWNLOAD . "Periodic_Report_HRMS.csv", "w"); 
			$line = "";
			$comma = "";
			foreach($final_datas as $key => $values) {
			    foreach($values as $key => $value){
			        $line .= $comma . '"' . str_replace('"', '""', $key) . '"';
			        $comma = ",";
			    }
			    break;
			}
			//echo $line;exit;
			$line .= "\n";
			fputs($fp, $line);
			foreach($final_datas as $key => $value) {
			    $comma = "";
			    $line = "";
			    $line .= $comma . '"' . str_replace('"', '""', $value['EmployeeId']) . '"';
			    $comma = ",";
			    $line .= $comma . '"' . str_replace('"', '""', $value['EmployeeName']) . '"';
			    $line .= $comma . '"' . str_replace('"', '""', $value['PunchID']) . '"';
			    $line .= $comma . '"' . str_replace('"', '""', $value['Department']) . '"';
			    $line .= $comma . '"' . str_replace('"', '""', $value['Site']) . '"';
			    $line .= $comma . '"' . str_replace('"', '""', $value['Region']) . '"';
			    $line .= $comma . '"' . str_replace('"', '""', $value['Division']) . '"';
			    $line .= $comma . '"' . str_replace('"', '""', $value['AttendDate']) . '"';
			    $line .= $comma . '"' . str_replace('"', '""', $value['Status']) . '"';
			    $line .= $comma . '"' . str_replace('"', '""', $value['InTime']) . '"';
			    $line .= $comma . '"' . str_replace('"', '""', $value['OutTime']) . '"';
			    $line .= $comma . '"' . str_replace('"', '""', $value['TotaHour']) . '"';
			    $line .= "\n";
			    //echo $line;exit;
			    fputs($fp, $line);
			}
			//echo $line;exit;
			fclose($fp);
			$filename = "Periodic_Report_HRMS.csv";
			$file_path = DIR_DOWNLOAD . $filename;
			$html = file_get_contents($file_path);
			header('Content-type: text/csv');
			header('Content-Disposition: attachment; filename='.$filename);
			echo $html;
			exit;
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/performance', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_name'])){
				$filter_department = $this->session->data['dept_name'];
			} else {
				$filter_department = null;
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_department' => $filter_department,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>