<?php
class ControllerReportAttendance extends Controller { 
	public function index() {  
		$this->language->load('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = '0';
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = array();
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = 1;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/attendance', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/attendance');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'group'					 => $group,
			'status'			     => $status, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		if($url == ''){
			$this->data['generate'] = $this->url->link('transaction/transaction/generate_filter_date', 'token=' . $this->session->data['token'] . '&filter_date_start=' . $filter_date_start, 'SSL');
		} else {
			$this->data['generate'] = $this->url->link('transaction/transaction/generate_filter_date', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}

		$this->data['export'] = $this->url->link('report/attendance/export', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$start_time = strtotime($filter_date_start);
		$compare_time = strtotime(date('Y-m-d'));

		$results = array();
		if($start_time <= $compare_time) {
			if($data['status'] == 2){
				//$tran_data = $this->model_report_attendance->getclose_status($data);
				//if(isset($tran_data[0]['transaction_id'])){
					$results = $this->model_report_attendance->getAttendance($data);
				//} else {
					//$this->data['warning'] = 'Please Process the Day before getting absent report';
				//}
			} else {
				$results = $this->model_report_attendance->getAttendance($data);
			}
		} else {
			
			$results = array();	
		}

		foreach($results as $rkey => $rvalue){
			$query = $this->db->query("SELECT * FROM ".DB_PREFIX."leave_transaction WHERE `emp_id` = '".$rvalue['emp_id']."' AND `date` = '".$rvalue['date']."' AND (`p_status` = '0' OR `p_status` = '1') AND `a_status` = '1' ");
			if($query->num_rows > 0){
				$leave_data = $query->row;
				if($leave_data['type'] != ''){
					$results[$rkey]['leave_status'] = 1;
					if($leave_data['type'] == 'F'){
						$results[$rkey]['firsthalf_status'] = $leave_data['leave_type'];
						$results[$rkey]['secondhalf_status'] = $leave_data['leave_type'];
					} elseif($leave_data['type'] == '1'){
						$results[$rkey]['firsthalf_status'] = $leave_data['leave_type'];
						if($rvalue['halfday_status'] != 0){
							$results[$rkey]['secondhalf_status'] = 'Half Day';
						} elseif($results[$rkey]['secondhalf_status'] == 1){
							$results[$rkey]['secondhalf_status'] = 'Present';
						} elseif($results[$rkey]['secondhalf_status'] == 0){
							$results[$rkey]['secondhalf_status'] = 'Absent';
						}  
					}  elseif($leave_data['type'] == '2'){
						$results[$rkey]['secondhalf_status'] = $leave_data['leave_type'];
						if($rvalue['halfday_status'] != 0){
							$results[$rkey]['firsthalf_status'] = 'Half Day';
						} elseif($results[$rkey]['firsthalf_status'] == 1){
							$results[$rkey]['firsthalf_status'] = 'Present';
						} elseif($results[$rkey]['firsthalf_status'] == 0){
							$results[$rkey]['firsthalf_status'] = 'Absent';
						}
					}
				} else {
					$results[$rkey]['leave_status'] = 1;
					$results[$rkey]['firsthalf_status'] = $leave_data['leave_type'];
					$results[$rkey]['secondhalf_status'] = $leave_data['leave_type'];
				}
			} else {
				$results[$rkey]['leave_status'] = 0;
			}
			$results[$rkey]['remove_href'] = $this->url->link('report/attendance/remove_transaction', 'token=' . $this->session->data['token'] . '&transaction_id=' . $rvalue['transaction_id'].$url, 'SSL');
		}

		$this->data['results'] = $results;


		$fields_data = array(
			'1' => 'Punch Id',
			'1' => 'Emp Id',
			'1' => 'Name',
			'2' => 'Shift Assigned',
			'3' => 'Shift Attended',
			'4' => 'Working Hours',
			'5' => 'Late Time',
			'6' => 'Early Time',
			'10' => 'First Half',
			'11' => 'Second Half',
		);
		$this->data['fields_data'] = $fields_data;
		
		$this->load->model('catalog/unit');
		$site_datas = $this->model_catalog_unit->getUnits($data = array());
		$site_data = array();
		$site_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			$dvalue['unit'] = html_entity_decode($dvalue['unit']);
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit']] = $dvalue['unit'];	
			}
		}
		$this->data['unit_data'] = $site_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$dvalue['d_name'] = html_entity_decode($dvalue['d_name']);
			$department_data[$dvalue['d_name']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;

		
		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			$dvalue['region'] = html_entity_decode($dvalue['region']);
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;


		$this->load->model('catalog/division');
		$division_datas = $this->model_catalog_division->getDivisions($data = array());
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			$dvalue['division'] = html_entity_decode($dvalue['division']);
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_data'] = $company_data;

		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;

		$statuses = array(
			'0' => 'All',
			'1' => 'Present',
			'2' => 'Absent'
		);
		$this->data['statuses'] = $statuses;
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->data['warning'])){
			$this->data['error_warning'] = $this->data['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['division'] = $division;
		$this->data['region'] = $region;
		$this->data['group'] = $group;
		$this->data['status'] = $status;
		$this->data['filter_fields'] = $filter_fields;

		$this->template = 'report/attendance.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function export(){
		$this->language->load('report/attendance');
		$this->load->model('report/common_report');
		$this->load->model('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = 1;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'group'					 => $group,
			'status'			     => $status, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		$start_time = strtotime($filter_date_start);
		$compare_time = strtotime(date('Y-m-d'));

		$results = array();
		if($start_time <= $compare_time) {
			if($data['status'] == 2){
				//$tran_data = $this->model_report_attendance->getclose_status($data);
				//if(isset($tran_data[0]['transaction_id'])){
					$results = $this->model_report_attendance->getAttendance($data);
				//} else {
					//$this->data['warning'] = 'Please Process the Day before getting absent report';
				//}
			} else {
				$results = $this->model_report_attendance->getAttendance($data);
			}
		} else {
			$results = array();	
		}

		foreach($results as $rkey => $rvalue){
			$query = $this->db->query("SELECT * FROM ".DB_PREFIX."leave_transaction WHERE `emp_id` = '".$rvalue['emp_id']."' AND `date` = '".$rvalue['date']."' AND (`p_status` = '0' OR `p_status` = '1') AND `a_status` = '1' ");
			if($query->num_rows > 0){
				$leave_data = $query->row;
				if($leave_data['type'] != ''){
					$results[$rkey]['leave_status'] = 1;
					if($leave_data['type'] == 'F'){
						$results[$rkey]['firsthalf_status'] = $leave_data['leave_type'];
						$results[$rkey]['secondhalf_status'] = $leave_data['leave_type'];
					} elseif($leave_data['type'] == '1'){
						$results[$rkey]['firsthalf_status'] = $leave_data['leave_type'];
						if($rvalue['halfday_status'] != 0){
							$results[$rkey]['secondhalf_status'] = 'Half Day';
						} elseif($results[$rkey]['secondhalf_status'] == 1){
							$results[$rkey]['secondhalf_status'] = 'Present';
						} elseif($results[$rkey]['secondhalf_status'] == 0){
							$results[$rkey]['secondhalf_status'] = 'Absent';
						}  
					}  elseif($leave_data['type'] == '2'){
						$results[$rkey]['secondhalf_status'] = $leave_data['leave_type'];
						if($rvalue['halfday_status'] != 0){
							$results[$rkey]['firsthalf_status'] = 'Half Day';
						} elseif($results[$rkey]['firsthalf_status'] == 1){
							$results[$rkey]['firsthalf_status'] = 'Present';
						} elseif($results[$rkey]['firsthalf_status'] == 0){
							$results[$rkey]['firsthalf_status'] = 'Absent';
						}
					}
				} else {
					$results[$rkey]['leave_status'] = 1;
					$results[$rkey]['firsthalf_status'] = $leave_data['leave_type'];
					$results[$rkey]['secondhalf_status'] = $leave_data['leave_type'];
				}
			} else {
				$results[$rkey]['leave_status'] = 0;
			}
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		
		if($results){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			if($status == 1 || $status == 0){
				$statuss = 'Present Report';
				$statusss = 'Present_Report';
			} else {
				$statuss = 'Absent Report';
				$statusss = 'Absent_Report';
			}
			$template = new Template();		
			$template->data['results'] = $results;
			$template->data['filter_date_start'] = $filter_date_start;
			$template->data['filter_department'] = $department;
			$template->data['status'] = $status;
			$template->data['title'] = $statuss;
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/attendance_html.tpl');
			//echo $html;exit;
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename);
			// echo $html;
			$filename = $statusss."_".$filter_date_start;
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function remove_transaction(){
		$this->language->load('report/attendance');
		$this->load->model('report/common_report');
		$this->load->model('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = 1;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['transaction_id'])) {
			$transaction_id = $this->request->get['transaction_id'];
		} else {
			$transaction_id = 0;
		}

		$sql = "DELETE FROM `oc_transaction` WHERE `transaction_id` = '".$transaction_id."'";
		//echo $sql;exit;
		$this->db->query($sql);

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->session->data['success'] = 'Transaction Removed Succesfully';
		$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
	}
}
?>