<?php
class ControllerReportDaily extends Controller { 
	public function index() {  

		if(isset($this->session->data['emp_code'])){
			$emp_code = $this->session->data['emp_code'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_dept'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_super'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		}

		// echo '<pre>';
		// print_r($this->request->get);
		// exit;

		$this->language->load('report/daily');
		$this->load->model('report/common_report');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$filter_date_start = '';
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-01', strtotime($from . "-0 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = 0;
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = array();
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			//$filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $unit);
			//if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
				//$filter_date_end = $filter_date_end1;
			//}
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} elseif(isset($this->session->data['dept_name'])){
			$filter_department = $this->session->data['dept_name'];
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['filter_division'])) {
			$filter_division = html_entity_decode($this->request->get['filter_division']);
		} else {
			$filter_division = 0;
		}

		if (isset($this->request->get['filter_region'])) {
			$filter_region = html_entity_decode($this->request->get['filter_region']);
		} else {
			$filter_region = 0;
		}

		if (isset($this->request->get['filter_company'])) {
			$filter_company = html_entity_decode($this->request->get['filter_company']);
		} else {
			$filter_company = 1;
		}

		if (isset($this->request->get['filter_contractor'])) {
			$filter_contractor = html_entity_decode($this->request->get['filter_contractor']);
		} else {
			$filter_contractor = 0;
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			//$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = '1,2,8,9,10,11,14,15,16';
		}
		
		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}
		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}
		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}
		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/daily', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/attendance');
		$this->load->model('transaction/transaction');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_unit'			 => $filter_unit,
			'filter_department'		 => $filter_department,
			'filter_division'		 => $filter_division,
			'filter_region'			 => $filter_region,
			'filter_company'		 => $filter_company,
			'filter_contractor'		 => $filter_contractor,
			'status'				 => $status,
			'day_close'              => 1,
			'start'                  => ($page - 1) * 5,
			'limit'                  => 5
		);

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));

		$day = array();
		$days = $this->GetDays($filter_date_start, $filter_date_end);
		foreach ($days as $dkey => $dvalue) {
			$dates = explode('-', $dvalue);
			$day[$dkey]['day'] = $dates[2];
			$day[$dkey]['date'] = $dvalue;
		}
		$this->data['days'] = $day;
		//echo "<pre>";print_r($day);exit;

		
		$employee_total = 0;
		// echo '<pre>';
		// print_r($day);
		// exit;
		$final_datas = array();
		if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
			$employee_total = 0;
			$data = array(
				'filter_date_start'	     => $filter_date_start,
				'filter_date_end'	     => $filter_date_end,
				'filter_name'	     	 => $filter_name,
				'filter_name_id'	     => $filter_name_id,
				'filter_unit'			 => $filter_unit,
				'filter_department'		 => $filter_department,
				'filter_division'		 => $filter_division,
				'filter_region'			 => $filter_region,
				'filter_company'		 => $filter_company,
				'filter_contractor'		 => $filter_contractor,
				'status'				 => $status,
				'day_close'              => 1,
				'start'                  => ($page - 1) * 5,
				'limit'                  => 5,
				'filter_daily'           => 1,
			);

			$all_results = $this->model_report_common_report->gettransaction_data_group_new($data);
			$all_employee_transection = array();
			$all_employee = array();
			if(!empty($all_results)){
				foreach ($all_results as $akey => $avalue) {
					//$all_employee_transection[$avalue['date']][] = $avalue;
					$all_employee_transection[$avalue['emp_id']][$avalue['date']] = $avalue;
				}
			}
			//$employee_datas_sql = $this->db->query("SELECT `name`, `emp_code`,  `division`,  `region`,`unit`,  `department`, `designation`, `contractor`, `contractor_id`,`contractor_code`, `category`, `category_id`,`company` FROM `oc_employee` WHERE 1=1");
			$employee_datas_sql = $this->getemployees_active($data);
			$employee_array = array();//echo "<pre>";print_r($employee_array);exit;
			foreach ($employee_datas_sql as $ekey => $evalue) {
				$employee_array[$evalue['emp_code']] = $evalue;
			}

			$all_contracter_sql = $this->db->query("SELECT `contractor_name`, `contractor_code`,`contractor_id` FROM `oc_contractor` WHERE 1=1");
			$all_contracter_array = array();
			foreach ($all_contracter_sql->rows as $ckey => $cvalue) {
				$all_contracter_array[$cvalue['contractor_id']] = $cvalue;
			}
			foreach ($day as $key => $value) {
				foreach ($employee_array as $ekey => $evalue) {
					if(isset($all_employee_transection[$evalue['emp_code']][$value['date']])){
						$tvalue = $all_employee_transection[$evalue['emp_code']][$value['date']];
						$statuss = 'A';
						$day_off = 0;
						if($tvalue['leave_status'] == '1') { 
							if($tvalue['firsthalf_status'] != '0'){
								$statuss = $tvalue['firsthalf_status'];
							}
						} elseif($tvalue['leave_status'] == '0.5') { 
							$statuss = 'HL';
						} elseif($tvalue['weekly_off'] != '0') { 
							$day_off = 1;
							if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
								$statuss = 'W';
							} else {
								$statuss = 'W';
							}
						} elseif($tvalue['holiday_id'] != '0') {
							$day_off = 1;
							if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
								$statuss = 'PH';
							} else {
								$statuss = 'H';
							}
						} elseif($tvalue['present_status'] == '1') {
							$statuss = 'P';
						} elseif($tvalue['absent_status'] == '1') {
							if($tvalue['act_intime'] != '00:00:00' && $tvalue['act_outtime'] == '00:00:00'){
								$statuss = 'E';
							} else {
								$statuss = 'A';
							}
						} elseif($tvalue['present_status'] == '0.5' || $tvalue['absent_status'] == '0.5') {
							$statuss = 'HD';
						}
						if($tvalue['working_time'] != '00:00:00'){
							$working_array[] = $tvalue['working_time'];
						}
						if($tvalue['act_intime'] != '00:00:00'){
							$tvalue['act_intime'] = date('H:i', strtotime($tvalue['act_intime']));
						} else {
							$tvalue['act_intime'] = '00:00';
						}
						if($tvalue['act_outtime'] != '00:00:00'){
							$tvalue['act_outtime'] = date('H:i', strtotime($tvalue['act_outtime']));
						} else {
							$tvalue['act_outtime'] = '00:00';
						}
						if($tvalue['late_time'] != '00:00:00' && $day_off == '0'){
							$tvalue['late_time'] = date('H:i', strtotime($tvalue['late_time']));
						} else {
							$tvalue['late_time'] = '00:00';
						}
						if($tvalue['early_time'] != '00:00:00' && $day_off == '0'){
							$tvalue['early_time'] = date('H:i', strtotime($tvalue['early_time']));
						} else {
							$tvalue['early_time'] = '00:00';
						}
						if($tvalue['working_time'] != '00:00:00'){
							$tvalue['working_time'] = date('H:i', strtotime($tvalue['working_time']));
						} else {
							$tvalue['late_time'] = '00:00';
							$tvalue['early_time'] = '00:00';	
							$tvalue['working_time'] = '00:00';
						}
						$remark = '';
						$daily_data[$tvalue['contractor_id']][$value['date']][$evalue['emp_code']] = array(
							'emp_name'    => $tvalue['emp_name'],
							'emp_id'   => $tvalue['emp_id'],
							'shift_code'   => $tvalue['shift_code'],
							'division'   => $tvalue['division'],
							'region'   => $tvalue['region'],
							'unit'   => $tvalue['unit'],
							'department'   => $tvalue['department'],
							'designation'   => $tvalue['designation'],
							'contractor'   => $tvalue['contractor'],
							'contractor_code'   => $tvalue['contractor_code'],
							'category'   => $tvalue['category'],
							'date_compare' => $tvalue['date'],
							'date'		 => date('d-F-Y', strtotime($tvalue['date'])),
							'date_comp'		 => date('Y-m-d', strtotime($tvalue['date'])),
							'act_intime' => $tvalue['act_intime'],
							'act_outtime' => $tvalue['act_outtime'],
							'late_time' => $tvalue['late_time'],
							'early_time' => $tvalue['early_time'],
							'working_time' => $tvalue['working_time'],
							'status_name' => $statuss,
							'remark' => $remark,
							'leave_status' => $tvalue['leave_status'],
							'present_status' => $tvalue['present_status'],
							'holiday_id' => $tvalue['holiday_id'],
							'weekly_off' => $tvalue['weekly_off'],
						);
						//}
					} else {
						if(!isset($daily_data[$evalue['emp_code'].'_'.$value['date']]) && (isset($data['status']) && $data['status'] != 1)){
							$daily_data[$evalue['contractor_id']][$value['date']][$evalue['emp_code']] = array(
								'emp_name'    => $evalue['name'],
								'emp_id'   => $evalue['emp_code'],
								'shift_code'   =>  'GS',
								'division'   => $evalue['division'],
								'region'   => $evalue['region'],
								'unit'   => $evalue['unit'],
								'department'   => $evalue['department'],
								'designation'   => $evalue['designation'],
								'contractor'   => $evalue['contractor'],
								'contractor_code'   => $evalue['contractor_code'],
								'category'   => $evalue['category'],
								'date_compare' => $value['date'],
								'date'		 => date('d-F-Y', strtotime($value['date'])),
								'date_comp'		 => date('Y-m-d', strtotime($value['date'])),
								'act_intime' => '00:00',
								'act_outtime' => '00:00',
								'late_time' => '00:00',
								'early_time' => '00:00',
								'working_time' => '00:00',
								'status_name'	=> 'A',
								'remark' => '',
								'leave_status' => '0',
								'present_status' =>'0',
								'holiday_id' => '0',
								'weekly_off' => '0',
							);	
							
						}
					}
				}
			}
			//$final_datas = array();
			if(!empty($daily_data)){
				foreach ($daily_data as $contractor_id_key => $contractor_id_value) {
					if(isset($all_contracter_array[$contractor_id_key])){
						$contracter_name= $all_contracter_array[$contractor_id_key]['contractor_name'];
						$contracter_code= $all_contracter_array[$contractor_id_key]['contractor_code'];
						$all_employee_array = array();
						foreach ($contractor_id_value as $cdkey => $cdvalue) {//date
							$working_array = array();
							$present_count = 0;
							$employee_array = array();
							if(!empty($cdvalue)){
									foreach ($cdvalue as $realkey => $realvalue) { //emploeee
										if($realvalue['working_time'] != '00:00:00'){
											$working_array[] = $realvalue['working_time'];
										}
										if ($realvalue['leave_status'] == 1) {
											$present_count ++;
										} else if($realvalue['leave_status'] == 0.5){
											$present_count = $present_count  + 0.5;
											if ($realvalue['present_status'] == 0.5) {
												$present_count = $present_count  + 0.5;
											}
										} else if($realvalue['present_status'] == 1){
												$present_count ++;
										} else if($realvalue['present_status'] == 0.5){
											$present_count = $present_count  + 0.5;
										} else if($realvalue['holiday_id'] != 0){
											$present_count ++;
										} else if($realvalue['weekly_off'] != 0){
											$present_count ++;
										}
								}
								$employee_array = $cdvalue;
							}
							$time = 0;
							$hours = 0;
							$minutes = 0;
							foreach ($working_array as $time_val) {
								$times = explode(':', $time_val);
								$hours += $times[0];
								$minutes += $times[1];
							}
							$min_min = 00;
							$min_hours = 0;
							if($minutes > 0){
								$min_hours = floor($minutes / 60);
								$min_min = ($minutes % 60);
								$min_min = sprintf('%02d', $min_min);
							} else {
								$min_min = sprintf('%02d', $min_min);
							}
							$total_working_hours = sprintf('%02d', ($hours + $min_hours)).':'.$min_min;
								
							$final_datas[$cdkey][$contractor_id_key]['tran_datas'] = $employee_array;
							$final_datas[$cdkey][$contractor_id_key]['total_present_count'] = $present_count;

							$final_datas[$cdkey][$contractor_id_key]['total_working_hours'] = $total_working_hours;	
								
							$final_datas[$cdkey][$contractor_id_key]['emp_code'] = '';
							$final_datas[$cdkey][$contractor_id_key]['name'] = '';
							$final_datas[$cdkey][$contractor_id_key]['company'] = '';
							$final_datas[$cdkey][$contractor_id_key]['division'] = '';
							$final_datas[$cdkey][$contractor_id_key]['region'] = '';
							$final_datas[$cdkey][$contractor_id_key]['unit'] = '';
							$final_datas[$cdkey][$contractor_id_key]['department'] = '';
							$final_datas[$cdkey][$contractor_id_key]['designation'] = '';
							$final_datas[$cdkey][$contractor_id_key]['contractor'] = $contracter_name;
							$final_datas[$cdkey][$contractor_id_key]['contractor_code'] = $contracter_code;
							$final_datas[$cdkey][$contractor_id_key]['category'] = '';
						}
					}
				}
			}

			//echo "<pre>";print_r($final_datas);exit;
		}

		
		$this->data['final_datass'] = $final_datas;
		// echo '<pre>';
  // 		print_r($this->data['final_datass']);
  // 		exit;
		$fields_data = array(
			'1' => 'Punch Id',
			'2' => 'Employee Name',
			'3' => 'Category',
			'4' => 'Site',
			'5' => 'Region',
			'6' => 'Division',
			'7' => 'Department',
			'8' => 'Shift Code',
			'9' => 'Date',
			'10' => 'In Time',
			'11' => 'Out Time',
			'12' => 'Late Time',
			'13' => 'Early Time',
			'14' => 'Total Time',
			'15' => 'Status',
			'16' => 'Remark',
			'17'=>'Designation',
		);
		$this->data['fields_data'] = $fields_data;

		$statuses = array(
			'0' => 'All',
			'1' => 'Present',
			'2' => 'Absent',
		);
		$this->data['statuses'] = $statuses;

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $filter_division,
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		if($this->user->getId() == '1'){
			$site_data['0'] = 'All';
		}
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['unit_data'] = $site_data;

		$this->load->model('catalog/contractor');
		if(($filter_unit != '' && $filter_unit != '0') || $site_string != ''){
			$data = array(
				'filter_unit_id' => $filter_unit,
				'filter_site_string' => $site_string,
				'filter_master' => 2,
			);
			$contractor_datas = $this->model_catalog_contractor->getContractors_by_unit($data);
			foreach ($contractor_datas as $ckey => $cvalue) {
				$contractor_data[$cvalue['contractor_id']] = $cvalue['contractor_name'];
			}
		} else {
			$contractor_data = array();
			$contractor_datas = $this->model_catalog_contractor->getContractor_tots();
			foreach ($contractor_datas as $ckey => $cvalue) {
				$contractor_data[$cvalue['contractor_id']] = $cvalue['contractor_name'].'-'.$cvalue['contractor_code'];
			}
		}
		$this->data['contractor_data'] = $contractor_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;

		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_data'] = $company_data;

		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $filter_region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;
		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		$group_data['1'] = 'All WITHOUT OFFICIALS';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}
		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}
		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}
		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url .= '&once=1';

		if(isset($this->session->data['is_dept']) && $this->session->data['is_dept'] == '1'){
			$user_dept = 1;
			$is_dept = 1;
		} elseif(isset($this->session->data['is_user'])){
			$user_dept = 1;
			$is_dept = 0;
		} else {
			$user_dept = 0;
			$is_dept = 0;
		}
		$this->data['user_dept'] = $user_dept;
		$this->data['is_dept'] = $is_dept;

		$pagination = new Pagination();
		$pagination->total = $employee_total;
		$pagination->page = $page;
		$pagination->limit = 5;
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('report/daily', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		// echo '<pre>';
		// print_r($filter_fields);
		// exit;

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_type'] = $filter_type;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_department'] = $filter_department;
		$this->data['filter_division'] = $filter_division;
		$this->data['filter_region'] = $filter_region;
		$this->data['filter_fields'] = explode(',', $filter_fields);
		$this->data['filter_company'] = explode(',', $filter_company);
		$this->data['filter_contractor'] = explode(',', $filter_contractor);
		$this->data['status'] = $status;

		$this->template = 'report/daily.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function export(){
		$this->language->load('report/daily');
		$this->load->model('report/common_report');
		$this->load->model('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$filter_date_start = '';
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-01', strtotime($from . "-0 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = 0;
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = array();
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			//$filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $unit);
			//if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
				//$filter_date_end = $filter_date_end1;
			//}
		} else {
			$filter_date_end = date('Y-m-d');
		}
		
		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} elseif(isset($this->session->data['dept_name'])){
			$filter_department = $this->session->data['dept_name'];
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['filter_division'])) {
			$filter_division = html_entity_decode($this->request->get['filter_division']);
		} else {
			$filter_division = 0;
		}

		if (isset($this->request->get['filter_region'])) {
			$filter_region = html_entity_decode($this->request->get['filter_region']);
		} else {
			$filter_region = 0;
		}

		if (isset($this->request->get['filter_company'])) {
			$filter_company = html_entity_decode($this->request->get['filter_company']);
		} else {
			$filter_company = 1;
		}

		if (isset($this->request->get['filter_contractor'])) {
			$filter_contractor = html_entity_decode($this->request->get['filter_contractor']);
		} else {
			$filter_contractor = 0;
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			//$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = '1,2,8,9,10,11,14,15,16';
		}
		
		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}
		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}
		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}
		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url .= '&once=1';

		$this->load->model('report/attendance');
		$this->load->model('transaction/transaction');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$day = array();
		$days = $this->GetDays($filter_date_start, $filter_date_end);
		foreach ($days as $dkey => $dvalue) {
			$dates = explode('-', $dvalue);
			$day[$dkey]['day'] = $dates[2];
			$day[$dkey]['date'] = $dvalue;
		}
		$this->data['days'] = $day;
		//echo "<pre>";print_r($day);exit;

		//s$final_datas = array();
		$employee_total = 0;
		$data = array(
				'filter_date_start'	     => $filter_date_start,
				'filter_date_end'	     => $filter_date_end,
				'filter_name'	     	 => $filter_name,
				'filter_name_id'	     => $filter_name_id,
				'filter_unit'			 => $filter_unit,
				'filter_department'		 => $filter_department,
				'filter_division'		 => $filter_division,
				'filter_region'			 => $filter_region,
				'filter_company'		 => $filter_company,
				'filter_contractor'		 => $filter_contractor,
				'status'				 => $status,
				'day_close'              => 1,
				'start'                  => ($page - 1) * 5,
				'limit'                  => 5,
				'filter_daily'           => 1,
			);	
		
		$all_results = $this->model_report_common_report->gettransaction_data_group_new($data);
		$all_employee_transection = array();
		$all_employee = array();
		if(!empty($all_results)){
			foreach ($all_results as $akey => $avalue) {
				//$all_employee_transection[$avalue['date']][] = $avalue;
				$all_employee_transection[$avalue['emp_id']][$avalue['date']] = $avalue;
			}
		}
		//$employee_datas_sql = $this->db->query("SELECT `name`, `emp_code`,  `division`,  `region`,`unit`,  `department`, `designation`, `contractor`, `contractor_id`,`contractor_code`, `category`, `category_id`,`company` FROM `oc_employee` WHERE 1=1");
		$employee_datas_sql = $this->getemployees_active($data);
		$employee_array = array();//echo "<pre>";print_r($employee_array);exit;
		foreach ($employee_datas_sql as $ekey => $evalue) {
			$employee_array[$evalue['emp_code']] = $evalue;
		}

		$all_contracter_sql = $this->db->query("SELECT `contractor_name`, `contractor_code`,`contractor_id` FROM `oc_contractor` WHERE 1=1");
		$all_contracter_array = array();
		foreach ($all_contracter_sql->rows as $ckey => $cvalue) {
			$all_contracter_array[$cvalue['contractor_id']] = $cvalue;
		}
		foreach ($day as $key => $value) {
			foreach ($employee_array as $ekey => $evalue) {
				if(isset($all_employee_transection[$evalue['emp_code']][$value['date']])){
					$tvalue = $all_employee_transection[$evalue['emp_code']][$value['date']];
					$statuss = 'A';
					$day_off = 0;
					if($tvalue['leave_status'] == '1') { 
						if($tvalue['firsthalf_status'] != '0'){
							$statuss = $tvalue['firsthalf_status'];
						}
					} elseif($tvalue['leave_status'] == '0.5') { 
						$statuss = 'HL';
					} elseif($tvalue['weekly_off'] != '0') { 
						$day_off = 1;
						if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
							$statuss = 'W';
						} else {
							$statuss = 'W';
						}
					} elseif($tvalue['holiday_id'] != '0') {
						$day_off = 1;
						if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
							$statuss = 'PH';
						} else {
							$statuss = 'H';
						}
					} elseif($tvalue['present_status'] == '1') {
						$statuss = 'P';
					} elseif($tvalue['absent_status'] == '1') {
						if($tvalue['act_intime'] != '00:00:00' && $tvalue['act_outtime'] == '00:00:00'){
							$statuss = 'E';
						} else {
							$statuss = 'A';
						}
					} elseif($tvalue['present_status'] == '0.5' || $tvalue['absent_status'] == '0.5') {
						$statuss = 'HD';
					}
					if($tvalue['working_time'] != '00:00:00'){
						$working_array[] = $tvalue['working_time'];
					}
					if($tvalue['act_intime'] != '00:00:00'){
						$tvalue['act_intime'] = date('H:i', strtotime($tvalue['act_intime']));
					} else {
						$tvalue['act_intime'] = '00:00';
					}
					if($tvalue['act_outtime'] != '00:00:00'){
						$tvalue['act_outtime'] = date('H:i', strtotime($tvalue['act_outtime']));
					} else {
						$tvalue['act_outtime'] = '00:00';
					}
					if($tvalue['late_time'] != '00:00:00' && $day_off == '0'){
						$tvalue['late_time'] = date('H:i', strtotime($tvalue['late_time']));
					} else {
						$tvalue['late_time'] = '00:00';
					}
					if($tvalue['early_time'] != '00:00:00' && $day_off == '0'){
						$tvalue['early_time'] = date('H:i', strtotime($tvalue['early_time']));
					} else {
						$tvalue['early_time'] = '00:00';
					}
					if($tvalue['working_time'] != '00:00:00'){
						$tvalue['working_time'] = date('H:i', strtotime($tvalue['working_time']));
					} else {
						$tvalue['late_time'] = '00:00';
						$tvalue['early_time'] = '00:00';	
						$tvalue['working_time'] = '00:00';
					}
					$remark = '';
					$daily_data[$tvalue['contractor_id']][$value['date']][$evalue['emp_code']] = array(
						'emp_name'    => $tvalue['emp_name'],
						'emp_id'   => $tvalue['emp_id'],
						'shift_code'   => $tvalue['shift_code'],
						'division'   => $tvalue['division'],
						'region'   => $tvalue['region'],
						'unit'   => $tvalue['unit'],
						'department'   => $tvalue['department'],
						'designation'   => $tvalue['designation'],
						'contractor'   => $tvalue['contractor'],
						'contractor_code'   => $tvalue['contractor_code'],
						'category'   => $tvalue['category'],
						'date_compare' => $tvalue['date'],
						'date'		 => date('d-F-Y', strtotime($tvalue['date'])),
						'date_comp'		 => date('Y-m-d', strtotime($tvalue['date'])),
						'act_intime' => $tvalue['act_intime'],
						'act_outtime' => $tvalue['act_outtime'],
						'late_time' => $tvalue['late_time'],
						'early_time' => $tvalue['early_time'],
						'working_time' => $tvalue['working_time'],
						'status_name' => $statuss,
						'remark' => $remark,
						'leave_status' => $tvalue['leave_status'],
						'present_status' => $tvalue['present_status'],
						'holiday_id' => $tvalue['holiday_id'],
						'weekly_off' => $tvalue['weekly_off'],
					);
					//}
				} else {
					if(!isset($daily_data[$evalue['emp_code'].'_'.$value['date']]) && (isset($data['status']) && $data['status'] != 1)){
						$daily_data[$evalue['contractor_id']][$value['date']][$evalue['emp_code']] = array(
							'emp_name'    => $evalue['name'],
							'emp_id'   => $evalue['emp_code'],
							'shift_code'   =>  'GS',
							'division'   => $evalue['division'],
							'region'   => $evalue['region'],
							'unit'   => $evalue['unit'],
							'department'   => $evalue['department'],
							'designation'   => $evalue['designation'],
							'contractor'   => $evalue['contractor'],
							'contractor_code'   => $evalue['contractor_code'],
							'category'   => $evalue['category'],
							'date_compare' => $value['date'],
							'date'		 => date('d-F-Y', strtotime($value['date'])),
							'date_comp'		 => date('Y-m-d', strtotime($value['date'])),
							'act_intime' => '00:00',
							'act_outtime' => '00:00',
							'late_time' => '00:00',
							'early_time' => '00:00',
							'working_time' => '00:00',
							'status_name'	=> 'A',
							'remark' => '',
							'leave_status' => '0',
							'present_status' =>'0',
							'holiday_id' => '0',
							'weekly_off' => '0',
						);	
						
					}
				}
			}
		}
		$final_datas = array();
		if(!empty($daily_data)){
			foreach ($daily_data as $contractor_id_key => $contractor_id_value) {
				if(isset($all_contracter_array[$contractor_id_key])){
					$contracter_name= $all_contracter_array[$contractor_id_key]['contractor_name'];
					$contracter_code= $all_contracter_array[$contractor_id_key]['contractor_code'];
					$all_employee_array = array();
					foreach ($contractor_id_value as $cdkey => $cdvalue) {//date
						$working_array = array();
						$present_count = 0;
						$employee_array = array();
						if(!empty($cdvalue)){
								foreach ($cdvalue as $realkey => $realvalue) { //emploeee
									if($realvalue['working_time'] != '00:00:00'){
										$working_array[] = $realvalue['working_time'];
									}
									if ($realvalue['leave_status'] == 1) {
										$present_count ++;
									} else if($realvalue['leave_status'] == 0.5){
										$present_count = $present_count  + 0.5;
										if ($realvalue['present_status'] == 0.5) {
											$present_count = $present_count  + 0.5;
										}
									} else if($realvalue['present_status'] == 1){
											$present_count ++;
									} else if($realvalue['present_status'] == 0.5){
										$present_count = $present_count  + 0.5;
									} else if($realvalue['holiday_id'] != 0){
										$present_count ++;
									} else if($realvalue['weekly_off'] != 0){
										$present_count ++;
									}
							}
							$employee_array = $cdvalue;
						}
						$time = 0;
						$hours = 0;
						$minutes = 0;
						foreach ($working_array as $time_val) {
							$times = explode(':', $time_val);
							$hours += $times[0];
							$minutes += $times[1];
						}
						$min_min = 00;
						$min_hours = 0;
						if($minutes > 0){
							$min_hours = floor($minutes / 60);
							$min_min = ($minutes % 60);
							$min_min = sprintf('%02d', $min_min);
						} else {
							$min_min = sprintf('%02d', $min_min);
						}
						$total_working_hours = sprintf('%02d', ($hours + $min_hours)).':'.$min_min;
							
						$final_datas[$cdkey][$contractor_id_key]['tran_datas'] = $employee_array;
						$final_datas[$cdkey][$contractor_id_key]['total_present_count'] = $present_count;

						$final_datas[$cdkey][$contractor_id_key]['total_working_hours'] = $total_working_hours;	
							
						$final_datas[$cdkey][$contractor_id_key]['emp_code'] = '';
						$final_datas[$cdkey][$contractor_id_key]['name'] = '';
						$final_datas[$cdkey][$contractor_id_key]['company'] = '';
						$final_datas[$cdkey][$contractor_id_key]['division'] = '';
						$final_datas[$cdkey][$contractor_id_key]['region'] = '';
						$final_datas[$cdkey][$contractor_id_key]['unit'] = '';
						$final_datas[$cdkey][$contractor_id_key]['department'] = '';
						$final_datas[$cdkey][$contractor_id_key]['designation'] = '';
						$final_datas[$cdkey][$contractor_id_key]['contractor'] = $contracter_name;
						$final_datas[$cdkey][$contractor_id_key]['contractor_code'] = $contracter_code;
						$final_datas[$cdkey][$contractor_id_key]['category'] = '';
					}
				}
			}
		}
		
		//$final_datas = array_chunk($final_datas, 3);
		if($final_datas){
			if (isset($this->request->get['filter_unit'])) {
				$unit_names = $this->db->query("SELECT `unit` FROM `oc_unit` WHERE `unit_id` = '".$this->request->get['filter_unit']."' ");
				if($unit_names->num_rows > 0){
					$unit_name = $unit_names->row['unit'];
				} else {
					$unit_name = '';
				}
				$filter_unit = html_entity_decode($unit_name);
			} else {
				$filter_unit = 'All';
			}
			if (isset($this->request->get['filter_department'])) {
				$department_names = $this->db->query("SELECT `d_name` FROM `oc_department` WHERE `department_id` = '".$this->request->get['filter_department']."' ");
				if($department_names->num_rows > 0){
					$department_name = $department_names->row['d_name'];
				} else {
					$department_name = '';
				}
				$filter_department = html_entity_decode($department_name);
			} else {
				$filter_department = 'All';
			}
			if (isset($this->request->get['filter_division'])) {
				$division_names = $this->db->query("SELECT `division` FROM `oc_division` WHERE `division_id` = '".$this->request->get['filter_division']."' ");
				if($division_names->num_rows > 0){
					$division_name = $division_names->row['division'];
				} else {
					$division_name = '';
				}
				$filter_division = html_entity_decode($division_name);
			} else {
				$filter_division = 'All';
			}

			if (isset($this->request->get['filter_region'])) {
				$region_names = $this->db->query("SELECT `region` FROM `oc_region` WHERE `region_id` = '".$this->request->get['filter_region']."' ");
				if($region_names->num_rows > 0){
					$region_name = $region_names->row['region'];
				} else {
					$region_name = '';
				}
				$filter_region = html_entity_decode($region_name);
			} else {
				$filter_region = 'All';
			}

			if (isset($this->request->get['filter_company'])) {
				$company_string = "'" . str_replace(",", "','", html_entity_decode($this->request->get['filter_company'])) . "'";
				$company_names = $this->db->query("SELECT `company` FROM `oc_company` WHERE `company_id` IN (" . strtolower($company_string) . ") ");
				if($company_names->num_rows > 0){
					$company_name = '';
					foreach($company_names->rows as $ckey => $cvalue){
						$company_name .= $cvalue['company'].", ";
					}
					$company_name = rtrim($company_name, ', ');
				} else {
					$company_name = '';
				}
				$filter_company = html_entity_decode($company_name);
			} else {
				$filter_company = 'All';
			}

			if (isset($this->request->get['filter_contractor'])) {
				$contractor_string = "'" . str_replace(",", "','", html_entity_decode($this->request->get['filter_contractor'])) . "'";
				$contractor_datass = $this->db->query("SELECT `contractor_name` FROM `oc_contractor` WHERE `contractor_id` IN (" . strtolower($contractor_string) . ") ");
				if($contractor_datass->num_rows > 0){
					$contractor_name = '';
					foreach($contractor_datass->rows as $ckey => $cvalue){
						$contractor_name .= $cvalue['contractor_name'].", ";
					}
					$contractor_name = rtrim($contractor_name, ', ');
					
				} else {
					$contractor_name = '';
				}
				$filter_contractor = html_entity_decode($contractor_name);
			} else {
				$filter_contractor = 'All';
			}

			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datass'] = $final_datas;
			$template->data['month_of'] = date('F-Y', strtotime($filter_date_end));
			$template->data['date_start'] = date('d-F-Y', strtotime($filter_date_start));
			$template->data['date_end'] = date('d-F-Y', strtotime($filter_date_end));
			$template->data['filter_company'] = $filter_company;
			$template->data['filter_contractor'] = $filter_contractor;
			$template->data['filter_division'] = $filter_division;
			$template->data['filter_region'] = $filter_region;
			$template->data['filter_unit'] = $filter_unit;
			$template->data['filter_department'] = $filter_department;
			$template->data['filter_fields'] = explode(',', $filter_fields);
			$template->data['title'] = 'Daily In/Out Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/daily_html.tpl');
			//echo $html;exit;
			$filename = "Daily_In/Out_Report";
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename.".html");
			// echo $html;
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/daily', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_name'])){
				$filter_department = $this->session->data['dept_name'];
			} else {
				$filter_department = null;
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_department' => $filter_department,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
	public function getemployees_active($data = array()) {//echo "<pre>";print_r($data);exit;
		$sql = "SELECT `name`, `emp_code`,  `division`,  `region`,`unit`,  `department`, `designation`, `contractor`, `contractor_id`,`contractor_code`, `category`, `category_id`,`company` FROM " . DB_PREFIX . "employee WHERE 1=1 ";
		
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_code` = '" . $this->db->escape(strtolower($data['filter_name_id'])) . "'";
		}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND LOWER(`unit_id`) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "'";
		}

		if (!empty($data['filter_department'])) {
			$sql .= " AND LOWER(`department_id`) = '" . $this->db->escape(strtolower($data['filter_department'])) . "'";
		}

		if (!empty($data['filter_division'])) {
			$sql .= " AND LOWER(`division_id`) = '" . $this->db->escape(strtolower($data['filter_division'])) . "'";
		}

		if (!empty($data['filter_region'])) {
			$sql .= " AND LOWER(`region_id`) = '" . $this->db->escape(strtolower($data['filter_region'])) . "'";
		}

		if (!empty($data['filter_company'])) {
			$company_string = "'" . str_replace(",", "','", html_entity_decode($data['filter_company'])) . "'";
			$sql .= " AND LOWER(`company_id`) IN (" . strtolower($company_string) . ") ";
		}

		if (!empty($data['filter_contractor'])) {
			$contractor_string = "'" . str_replace(",", "','", html_entity_decode($data['filter_contractor'])) . "'";
			$sql .= " AND (`contractor_id`) IN (" . ($contractor_string) . ") ";
		}

		// if (isset($data['device_id']) && !empty($data['device_id'])) {
		// 	$sql .= " AND `device_id` = '" . $this->db->escape(strtolower($data['device_id'])) . "'";
		// }


		$division_string = $this->user->getdivision();
		$region_string = $this->user->getregion();
		$site_string = $this->user->getsite();
		$company_string = $this->user->getCompanyId();
			
		if($company_string != ''){
			$company_string = "'" . str_replace(",", "','", html_entity_decode($company_string)) . "'";
			if (isset($data['filter_company']) && !empty($data['filter_company'])) {
			} else {
				$sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
			}
		}
		
		if($division_string != ''){
			$division_string = "'" . str_replace(",", "','", html_entity_decode($division_string)) . "'";
			if (isset($data['filter_division']) && !empty($data['filter_division'])) {
			} else {
				$sql .= " AND `division_id` IN (" . strtolower($division_string) . ") ";
			}
		}

		if($region_string != ''){
			$region_string = "'" . str_replace(",", "','", html_entity_decode($region_string)) . "'";
			if (isset($data['filter_region']) && !empty($data['filter_region'])) {
			} else {
				//$sql .= " AND `region_id` IN (" . strtolower($region_string) . ") ";
			}
		}

		if($site_string != ''){
			$site_string = "'" . str_replace(",", "','", html_entity_decode($site_string)) . "'";
			if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			} else {
				$sql .= " AND `unit_id` IN (" . strtolower($site_string) . ") ";
			}
		}
		$sql .= " AND `contractor_id` <> '0' ORDER BY `contractor_id`, `emp_code` ASC ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
}
?>