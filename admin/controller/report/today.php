<?php
class ControllerReportToday extends Controller { 
	public function index() {  
		date_default_timezone_set("Asia/Kolkata");
		$this->language->load('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$filter_date_start = '';
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-01', strtotime($from . "-0 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = 0;
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			//$filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $unit);
			//if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
				//$filter_date_end = $filter_date_end1;
			//}
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} elseif(isset($this->session->data['dept_name'])){
			$filter_department = $this->session->data['dept_name'];
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['filter_division'])) {
			$filter_division = html_entity_decode($this->request->get['filter_division']);
		} else {
			$filter_division = 0;
		}

		if (isset($this->request->get['filter_region'])) {
			$filter_region = html_entity_decode($this->request->get['filter_region']);
		} else {
			$filter_region = 0;
		}

		if (isset($this->request->get['filter_company'])) {
			$filter_company = html_entity_decode($this->request->get['filter_company']);
		} else {
			$filter_company = 1;
		}

		if (isset($this->request->get['filter_contractor'])) {
			$filter_contractor = html_entity_decode($this->request->get['filter_contractor']);
		} else {
			$filter_contractor = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}
		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}
		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}
		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/today', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		$this->data['generate_today'] = $this->url->link('transaction/transaction/generate_today', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['export'] = $this->url->link('report/today/export', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->load->model('report/attendance');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_unit'			 => $filter_unit,
			'filter_department'		 => $filter_department,
			'filter_division'		 => $filter_division,
			'filter_region'			 => $filter_region,
			'filter_company'		 => $filter_company,
			'filter_contractor'		 => $filter_contractor,
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		$results = $this->model_report_attendance->getTodaysAttendance_new($data);
		
		$this->data['results'] = $results;
		
		// $locations = $this->db->query("SELECT * FROM `oc_unit` ")->rows;
		// $unit_data['0'] = 'All'; 
		// foreach($locations as $lkey => $lvalue){
		// 	$unit_data[$lvalue['unit']] = $lvalue['unit'];
		// }
		// $this->data['unit_data'] = $unit_data;

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $filter_division,
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		if($this->user->getId() == '1'){
			$site_data['0'] = 'All';
		}
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['unit_data'] = $site_data;

		$this->load->model('catalog/contractor');
		if(($filter_unit != '' && $filter_unit != '0') || $site_string != ''){
			$data = array(
				'filter_unit_id' => $filter_unit,
				'filter_site_string' => $site_string,
				'filter_master' => 2,
			);
			$contractor_datas = $this->model_catalog_contractor->getContractors_by_unit($data);
			foreach ($contractor_datas as $ckey => $cvalue) {
				$contractor_data[$cvalue['contractor_id']] = $cvalue['contractor_name'];
			}
		} else {
			$contractor_data = array();
			$contractor_datas = $this->model_catalog_contractor->getContractor_tots();
			foreach ($contractor_datas as $ckey => $cvalue) {
				$contractor_data[$cvalue['contractor_id']] = $cvalue['contractor_name'].'-'.$cvalue['contractor_code'];
			}
		}
		$this->data['contractor_data'] = $contractor_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;


		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_data'] = $company_data;

		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $filter_region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;	


		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}
		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}
		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}
		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_department'] = $filter_department;
		$this->data['filter_division'] = $filter_division;
		$this->data['filter_region'] = $filter_region;
		$this->data['filter_company'] = explode(',', $filter_company);
		$this->data['filter_contractor'] = explode(',', $filter_contractor);

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->template = 'report/today.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function export(){
		$this->language->load('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$filter_date_start = '';
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-01', strtotime($from . "-0 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = 0;
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			//$filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $unit);
			//if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
				//$filter_date_end = $filter_date_end1;
			//}
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = html_entity_decode($this->request->get['filter_department']);
		} elseif(isset($this->session->data['dept_name'])){
			$filter_department = $this->session->data['dept_name'];
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['filter_division'])) {
			$filter_division = html_entity_decode($this->request->get['filter_division']);
		} else {
			$filter_division = 0;
		}

		if (isset($this->request->get['filter_region'])) {
			$filter_region = html_entity_decode($this->request->get['filter_region']);
		} else {
			$filter_region = 0;
		}

		if (isset($this->request->get['filter_company'])) {
			$filter_company = html_entity_decode($this->request->get['filter_company']);
		} else {
			$filter_company = 1;
		}

		if (isset($this->request->get['filter_contractor'])) {
			$filter_contractor = html_entity_decode($this->request->get['filter_contractor']);
		} else {
			$filter_contractor = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}
		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}
		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}
		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->load->model('report/attendance');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_unit'			 => $filter_unit,
			'filter_department'		 => $filter_department,
			'filter_division'		 => $filter_division,
			'filter_region'			 => $filter_region,
			'filter_company'		 => $filter_company,
			'filter_contractor'		 => $filter_contractor,
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		$final_datas = array();
		$final_datas = $this->model_report_attendance->getTodaysAttendance_new($data);
		
		$final_datas = array_chunk($final_datas, 15000);

		if($final_datas){
			if (isset($this->request->get['filter_unit'])) {
				$unit_names = $this->db->query("SELECT `unit` FROM `oc_unit` WHERE `unit_id` = '".$this->request->get['filter_unit']."' ");
				if($unit_names->num_rows > 0){
					$unit_name = $unit_names->row['unit'];
				} else {
					$unit_name = '';
				}
				$filter_unit = html_entity_decode($unit_name);
			} else {
				$filter_unit = 'All';
			}
			if (isset($this->request->get['filter_department'])) {
				$department_names = $this->db->query("SELECT `d_name` FROM `oc_department` WHERE `department_id` = '".$this->request->get['filter_department']."' ");
				if($department_names->num_rows > 0){
					$department_name = $department_names->row['d_name'];
				} else {
					$department_name = '';
				}
				$filter_department = html_entity_decode($department_name);
			} else {
				$filter_department = 'All';
			}
			if (isset($this->request->get['filter_division'])) {
				$division_names = $this->db->query("SELECT `division` FROM `oc_division` WHERE `division_id` = '".$this->request->get['filter_division']."' ");
				if($division_names->num_rows > 0){
					$division_name = $division_names->row['division'];
				} else {
					$division_name = '';
				}
				$filter_division = html_entity_decode($division_name);
			} else {
				$filter_division = 'All';
			}

			if (isset($this->request->get['filter_region'])) {
				$region_names = $this->db->query("SELECT `region` FROM `oc_region` WHERE `region_id` = '".$this->request->get['filter_region']."' ");
				if($region_names->num_rows > 0){
					$region_name = $region_names->row['region'];
				} else {
					$region_name = '';
				}
				$filter_region = html_entity_decode($region_name);
			} else {
				$filter_region = 'All';
			}

			if (isset($this->request->get['filter_company'])) {
				$company_string = "'" . str_replace(",", "','", html_entity_decode($this->request->get['filter_company'])) . "'";
				$company_names = $this->db->query("SELECT `company` FROM `oc_company` WHERE `company_id` IN (" . strtolower($company_string) . ") ");
				if($company_names->num_rows > 0){
					$company_name = '';
					foreach($company_names->rows as $ckey => $cvalue){
						$company_name .= $cvalue['company'].", ";
					}
					$company_name = rtrim($company_name, ', ');
				} else {
					$company_name = '';
				}
				$filter_company = html_entity_decode($company_name);
			} else {
				$filter_company = 'All';
			}

			if (isset($this->request->get['filter_contractor'])) {
				$contractor_string = "'" . str_replace(",", "','", html_entity_decode($this->request->get['filter_contractor'])) . "'";
				$contractor_datass = $this->db->query("SELECT `contractor_name` FROM `oc_contractor` WHERE `contractor_id` IN (" . strtolower($contractor_string) . ") ");
				if($contractor_datass->num_rows > 0){
					$contractor_name = '';
					foreach($contractor_datass->rows as $ckey => $cvalue){
						$contractor_name .= $cvalue['contractor_name'].", ";
					}
					$contractor_name = rtrim($contractor_name, ', ');
					
				} else {
					$contractor_name = '';
				}
				$filter_contractor = html_entity_decode($contractor_name);
			} else {
				$filter_contractor = 'All';
			}
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			//$tdays = cal_days_in_month(CAL_GREGORIAN, $filter_month, $filter_year);
			$template = new Template();		
			$template->data['final_datass'] = $final_datas;
			$template->data['date_start'] = date('d-M-Y', strtotime($filter_date_start));
			$template->data['filter_company'] = $filter_company;
			$template->data['filter_contractor'] = $filter_contractor;
			$template->data['filter_division'] = $filter_division;
			$template->data['filter_region'] = $filter_region;
			$template->data['filter_unit'] = $filter_unit;
			$template->data['filter_department'] = $filter_department;
			$template->data['title'] = 'Todays Attendance Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/today_html.tpl');
			//echo $html;exit;
			//$filename = "Todays_Attendance_".$department.".html";
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename);
			// echo $html;
			$filename = "Todays_Attendance";
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/today', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
}
?>
