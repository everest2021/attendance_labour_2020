<?php    
class ControllerCatalogHoliday extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/holiday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/holiday');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/holiday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/holiday');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_holiday->addholiday($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/holiday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/holiday');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_holiday->editholiday($this->request->get['holiday_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function export() { //echo "string";// exit;
		$this->language->load('catalog/holiday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/holiday');

		if(1==1){
			$data['filter_name'] = '';
			$data['sort'] = 'holiday_id';
			$holiday_datas = $this->model_catalog_holiday->getholidays($data);
		//	echo "<pre>"; print_r($holiday_datas); exit;
			$final_datas = array();
			foreach($holiday_datas as $skey => $svalue){
				$final_datas[$skey]['holiday_name'] = $svalue['name'];
				$final_datas[$skey]['holiday_date'] = $svalue['date'];
				//$final_datas[$skey]['shift_outtime'] = $svalue['out_time'];

				$units = $this->db->query("SELECT `unit` from oc_unit");
				foreach ($units->rows as $ukey => $uvalue) {
					$dept_datas = $this->db->query("SELECT * from oc_holiday_loc WHERE `location` = '". $uvalue['unit']."' AND `holiday_id` = '". $svalue['holiday_id']."'");
					if ($dept_datas->num_rows>0) {
						foreach ($dept_datas->rows as $key => $value) {
							$unit_name=$value['location'];
							$unit_dept= unserialize($value['value']);
							$dept_unit_data = '';
						
							if($unit_dept){
								foreach ($unit_dept as $dkey => $dvalue) {
									$dept_unit_data .= $dvalue . ', '; 
								}

							}
							$final_datas[$skey]['unit_data'][$unit_name] =$dept_unit_data;
						}
					} else {
							$final_datas[$skey]['unit_data'][$uvalue['unit']] ="";
					}
				}
				
			}
			// echo '<pre>';
			// print_r($final_datas);
			// exit;

			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			//$template->data['filter_year'] = $filter_year;
			$template->data['title'] = 'Holidays Schedule';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('catalog/holidays_schedule_html.tpl');
			//echo $html;exit;
			$filename = "Holiday_Schedule";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
		} else {
			$this->session->data['warning'] = 'No Data';
			//$this->redirect($this->url->link('catalog/shift', 'token=' . $this->session->data['token'], 'SSL'));
			$this->getList();
		}
	}

	public function delete() {
		$this->language->load('catalog/holiday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/holiday');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $holiday_id) {
				$this->model_catalog_holiday->deleteholiday($holiday_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['holiday_id']) && $this->validateDelete()){
			$this->model_catalog_holiday->deleteholiday($this->request->get['holiday_id']);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/holiday/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/holiday/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['export'] = $this->url->link('catalog/holiday/export', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['holidays'] = array();

		$data = array(
			'filter_name' => $filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$holiday_total = $this->model_catalog_holiday->getTotalholidays($data);

		$results = $this->model_catalog_holiday->getholidays($data);

		// echo '<pre>';
		// print_r($results);
		// exit;

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/holiday/update', 'token=' . $this->session->data['token'] . '&holiday_id=' . $result['holiday_id'] . $url, 'SSL')
			);

			// $action[] = array(
			// 	'text' => 'Delete',
			// 	'href' => $this->url->link('catalog/holiday/delete', 'token=' . $this->session->data['token'] . '&holiday_id=' . $result['holiday_id'] . $url, 'SSL')
			// );

			$this->data['holidays'][] = array(
				'holiday_id' => $result['holiday_id'],
				'name' => $result['name'],
				'date'            => $result['date'],
				'selected'        => isset($this->request->post['selected']) && in_array($result['holiday_id'], $this->request->post['selected']),
				'action'          => $action
			);
		}

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . '&sort=date' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		$pagination = new Pagination();
		$pagination->total = $holiday_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		
		$this->template = 'catalog/holiday_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['date'])) {
			$this->data['error_date'] = $this->error['date'];
		} else {
			$this->data['error_date'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['holiday_id'])) {
			$this->data['action'] = $this->url->link('catalog/holiday/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
			
			$this->data['process_holiday'] = '';
		} else {
			$this->data['action'] = $this->url->link('catalog/holiday/update', 'token=' . $this->session->data['token'] . '&holiday_id=' . $this->request->get['holiday_id'] . $url, 'SSL');
			
			$this->data['process_holiday'] = $this->url->link('catalog/holiday/process_holiday', 'token=' . $this->session->data['token'] . $url . '&holiday_id=' . $this->request->get['holiday_id'], 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/holiday', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['holiday_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$holiday_info = $this->model_catalog_holiday->getholiday($this->request->get['holiday_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		$this->load->model('report/attendance');

		// $unit_data = array(
		// 	'Mumbai' => 'Mumbai',	
		// 	'Pune' => 'Pune',
		// 	'Moving' => 'Moving' 
		// );

		//$this->data['unit_data'] = $unit_data;
		
		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		foreach ($department_datas as $dkey => $dvalue) {
			$dvalue['d_name'] = html_entity_decode($dvalue['d_name']);
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;

		$this->load->model('catalog/division');
		$division_datas = $this->model_catalog_division->getDivisions($data = array());
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$dvalue['division'] = html_entity_decode($dvalue['division']);
					$division_data[$dvalue['division_id']]['name'] = $dvalue['division'];
					$division_data[$dvalue['division_id']]['id'] = $dvalue['division_id'];
				}
			} else {
				$dvalue['division'] = html_entity_decode($dvalue['division']);
				$division_data[$dvalue['division_id']]['name'] = $dvalue['division'];
				$division_data[$dvalue['division_id']]['id'] = $dvalue['division_id'];
			}
		}
		$this->data['division_data'] = $division_data;
		// echo '<pre>';
		// print_r($division_data);
		// exit;

		if (isset($this->request->post['holi_datas'])) {
			$this->data['holi_datas'] = $this->request->post['holi_datas'];
		} elseif (!empty($holiday_info['holiday_id'])) {
			$units = $this->db->query("SELECT * FROM `oc_unit`")->rows;
			foreach ($units as $key => $unit) {
				$unit_values = $this->db->query("SELECT * FROM `oc_holiday_loc` where holiday_id='".$this->request->get['holiday_id']."' AND location_id = '".$unit['unit_id']."' ");
				if ($unit_values->num_rows > 0) { 
					$this->data['holi_datas'][$unit['unit_id']] = unserialize($unit_values->row['value']);
					foreach($this->data['holi_datas'][$unit['unit_id']] as $key => $value){
						$this->data['holi_datas'][$unit['unit_id']][$key] = html_entity_decode(strtolower(trim($value)));
					}
				} else {
					$this->data['holi_datas'][$unit['unit_id']] = array();
				}
			}
			// echo "<pre>";
			// print_r($this->data['holi_datas']);
			// exit;
		} else {
			$units = $this->db->query("SELECT * FROM `oc_unit`")->rows;
			//if (isset($this->request->get['shift_id'])) {
				//$shift_id=$this->request->get['shift_id'];
			//} else {
				//$shift_id='';
			//}
			foreach ($units as $key => $unit) {
				//$unit_values = $this->db->query("SELECT * FROM `oc_shift_unit` where shift_id='".$shift_id."' AND name='".$unit['unit']."' ");
				$this->data['holi_datas'][$unit['unit_id']] = array();
			}
		}

		// echo '<pre>';
		// print_r($this->data['holi_datas']);
		// exit;

		$unit_datas = $this->db->query("SELECT `unit`, `unit_id`, `division_id`, `division_name` FROM `oc_unit` GROUP BY `unit` ORDER BY `division_name` ")->rows;
		$unit_data = array();
		//$department_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($unit_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$unit_data[$dvalue['unit_id']]['name'] = $dvalue['unit'];
					$unit_data[$dvalue['unit_id']]['id'] = $dvalue['unit_id'];
					$unit_data[$dvalue['unit_id']]['division_id'] = $dvalue['division_id'];
					$unit_data[$dvalue['unit_id']]['division_name'] = $dvalue['division_name'];
					
					if(isset($this->data['holi_datas'][$dvalue['unit_id']]) && !empty($this->data['holi_datas'][$dvalue['unit_id']]) ){
						$unit_data[$dvalue['unit_id']]['selected'] = 1;	
					} else {
						$unit_data[$dvalue['unit_id']]['selected'] = 0;	
					}
				}
			} else {
				$unit_data[$dvalue['unit_id']]['name'] = $dvalue['unit'];
				$unit_data[$dvalue['unit_id']]['id'] = $dvalue['unit_id'];
				$unit_data[$dvalue['unit_id']]['division_id'] = $dvalue['division_id'];
				$unit_data[$dvalue['unit_id']]['division_name'] = $dvalue['division_name'];

				if(isset($this->data['holi_datas'][$dvalue['unit_id']]) && !empty($this->data['holi_datas'][$dvalue['unit_id']]) ){
					$unit_data[$dvalue['unit_id']]['selected'] = 1;	
				} else {
					$unit_data[$dvalue['unit_id']]['selected'] = 0;	
				}
			}
		}
		$this->data['unit_data'] = $unit_data;
		
		// echo '<pre>';
		// print_r($unit_data);
		// exit;
		
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($holiday_info)) {
			$this->data['name'] = $holiday_info['name'];
		} else {	
			$this->data['name'] = '';
		}

		if (isset($this->request->post['date'])) {
			$this->data['date'] = $this->request->post['date'];
		} elseif (!empty($holiday_info)) {
			$this->data['date'] = $holiday_info['date'];
		} else {	
			$this->data['date'] = '';
		}

		// if (isset($this->request->post['holi_datas'])) {
		// 	$this->data['holi_datas'] = $this->request->post['holi_datas'];
		// } elseif (!empty($holiday_info['holiday_id'])) {
		// 	$units = $this->db->query("SELECT * FROM `oc_unit`")->rows;
		// 	foreach ($units as $key => $unit) {
		// 		$unit_values = $this->db->query("SELECT * FROM `oc_holiday_loc` where holiday_id='".$this->request->get['holiday_id']."' AND location='".$unit['unit']."' ");
		// 			if ($unit_values->num_rows > 0) { 
		// 				$this->data['unit_shift'][$unit['unit']]=  unserialize($unit_values->row['value']);
		// 				foreach($this->data['unit_shift'][$unit['unit']] as $key => $value){
		// 					$this->data['unit_shift'][$unit['unit']][$key] = html_entity_decode(strtolower(trim($value)));
		// 				}
		// 			} else {
		// 				$this->data['unit_shift'][$unit['unit']] = array();
		// 			}
		// 	}
		// 		echo "<pre>";
		// 		print_r($this->data['unit_shift']);
		// 		exit;
		// } else {
		// 	$units = $this->db->query("SELECT * FROM `oc_unit`")->rows;
		// 	if (isset($this->request->get['shift_id'])) {
		// 		$shift_id=$this->request->get['shift_id'];
		// 	} else {
		// 		$shift_id='';
		// 	}
		// 	foreach ($units as $key => $unit) {
		// 		$unit_values = $this->db->query("SELECT * FROM `oc_shift_unit` where shift_id='".$shift_id."' AND name='".$unit['unit']."' ");
		// 		$this->data['unit_shift'][$unit['unit']] = array();
		// 	}
		// }

		if (isset($this->request->post['holi_division_datas'])) {
			$this->data['holi_division_datas'] = $this->request->post['holi_division_datas'];
		} elseif (!empty($holiday_info['holiday_id'])) {
			if($holiday_info['division_datas'] != ''){
				$this->data['holi_division_datas'] = unserialize($holiday_info['division_datas']);
				foreach($this->data['holi_division_datas'] as $key => $value){
					$this->data['holi_division_datas'][$key] = html_entity_decode(strtolower(trim($value)));
				}
			} else {
				$this->data['holi_division_datas'] = array();	
			}
			// echo "<pre>";
			// print_r($this->data['holi_datas']);
			// exit;
		} else {
			$this->data['holi_division_datas'] = array();
		}

		$this->template = 'catalog/holiday_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		$this->load->model('catalog/holiday');
		if(isset($this->request->get['holiday_id'])){
			if (!$this->user->hasPermission('modify', 'catalog/holiday')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		} else {
			if (!$this->user->hasPermission('add', 'catalog/holiday')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		}

		if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = 'Please Enter Holiday Name';
		}

		if($this->request->post['date'] != '' && !isset($this->request->get['holiday_id'])){
			$is_exist = $this->model_catalog_holiday->getholiday_exist($this->request->post['date']);
			if($is_exist == 1){
				$this->error['warning'] = 'Holiday With Same Date Present';
			}
		}

		// $date = $this->request->post['date'];
		// if($this->user->getId() == 3){
		// 	if(isset($this->request->post['dept_holiday_mumbai'])){
		// 		$sql = "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `day_close_status` = '1' AND LOWER(`unit`) = 'mumbai' ";
		// 		$query = $this->db->query($sql);
		// 		if($query->num_rows > 0) {
		// 			$this->error['warning'] = 'Day Closed for Mumbai on date : ' . $this->request->post['date'];		
		// 		}
		// 	}
		// }
		// if ((utf8_strlen($this->request->post['holiday_code']) < 1) || (utf8_strlen($this->request->post['holiday_code']) > 64)) {
		// 	$this->error['holiday_code'] = 'holiday Code is Required';
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('delete', 'catalog/holiday')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// $this->load->model('catalog/horse');

		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $holiday_id) {
		// 		$horse_total = $this->model_catalog_horse->getTotalhorsesByholidayId($holiday_id);

		// 		if ($horse_total) {
		// 			$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 		}	
		// 	}
		// } elseif(isset($this->request->get['holiday_id'])){
		// 	$horse_total = $this->model_catalog_horse->getTotalhorsesByholidayId($this->request->get['holiday_id']);

		// 	if ($horse_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}  
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/holiday');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_holiday->getholidays($data);

			foreach ($results as $result) {
				$json[] = array(
					'holiday_id' => $result['holiday_id'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function process_holiday() {
		$this->language->load('catalog/holiday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/holiday');

		// echo '<pre>';
		// print_r($this->request->post);
		// exit;
		$in = 0;
		if(isset($this->request->get['holiday_id'])){
			$holiday_id = $this->request->get['holiday_id'];
			$holiday_dates = $this->db->query("SELECT `date` FROM `oc_holiday` WHERE `holiday_id` = '".$holiday_id."' ");
			if($holiday_dates->num_rows > 0){
				$date = $holiday_dates->row['date'];
				
				if($date < date('Y-m-d')){
					//echo 'aaaa';exit;
					$in = 1;
					$month = date('n', strtotime($date));
					$year = date('Y', strtotime($date));
					$day = date('j', strtotime($date));
					$sql = "SELECT `transaction_id`, `emp_id`, `emp_name` FROM `oc_transaction` WHERE `date` = '".$date."' ";
					$resultss = $this->db->query($sql)->rows;
					// echo '<pre>';
					// print_r($resultss);
					// exit;
					foreach($resultss as $rkeys => $rvalues){
						$shift_sql = "SELECT `".$day."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalues['emp_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
						$shift_datas = $this->db->query($shift_sql);
						if($shift_datas->num_rows > 0){
							$shift_data = $shift_datas->row[$day];
							$shift_exp = explode('_', $shift_data);
							if($shift_exp[0] == 'H'){
								$tran_datas = $this->db->query("SELECT `leave_status`, `firsthalf_status`, `secondhalf_status` FROM `oc_transaction` WHERE `transaction_id` = '".$rvalues['transaction_id']."' ");
								if($tran_datas->num_rows > 0){
									$tran_data = $tran_datas->row;
									// echo '<pre>';
									// print_r($tran_data);
									// exit;
									if($tran_data['leave_status'] != '1'){
										$update_sql = "UPDATE `oc_transaction` SET `holiday_id` = '1', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `absent_status` = '0' WHERE `transaction_id` = '".$rvalues['transaction_id']."' AND `emp_id` = '".$rvalues['emp_id']."' ";
										//echo $update_sql;
										//echo '<br />';
										//exit;
										$this->db->query($update_sql);
										$this->log->write($update_sql);
									}
								}
							}
						}	
					}
				} else {
					$this->session->data['warning'] = 'Post Dated Holiday Cannot be Processed';
				}
			}
		}
		//echo 'out';exit;
		if($in == 1){
			$this->session->data['success'] = 'Holiday Processed Successfully';
		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->redirect($this->url->link('catalog/holiday/update', 'token=' . $this->session->data['token'] . $url.'&holiday_id=' . $this->request->get['holiday_id'], 'SSL'));

	}	
}
?>