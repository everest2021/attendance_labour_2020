<?php
class ControllerCatalogUnit extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/unit');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/unit');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/unit');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/unit');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_unit->addUnit($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/unit', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/unit');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/unit');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_unit->editUnit($this->request->get['unit_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/unit', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function export() { 
		$this->language->load('catalog/unit');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/unit');

		if(1==1){
			$data['filter_name'] = '';
			$data['sort'] = 'unit_id';
			$regions_datas = $this->model_catalog_unit->getUnits($data);

			$final_datas = array();
			foreach($regions_datas as $skey => $svalue){
				$final_datas[$skey]['name'] = $svalue['unit'];
				$final_datas[$skey]['code'] = $svalue['unit_code'];
				$final_datas[$skey]['division_name'] = $svalue['division_name'];
				$final_datas[$skey]['state_name'] = $svalue['state_name'];
			}
			// echo '<pre>';
			// print_r($final_datas);
			// exit;

			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			//$template->data['filter_year'] = $filter_year;
			$template->data['title'] = 'Sites';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('catalog/unit_html.tpl');
			//echo $html;exit;
			$filename = "Sites";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
		} else {
			$this->session->data['warning'] = 'No Data';
			//$this->redirect($this->url->link('catalog/shift', 'token=' . $this->session->data['token'], 'SSL'));
			$this->getList();
		}
	}

	public function delete() {
		$this->language->load('catalog/unit');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/unit');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $unit_id) {
				$this->model_catalog_unit->deleteUnit($unit_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/unit', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_division'])) {
			$filter_division = $this->request->get['filter_division'];
		} else {
			$filter_division = '';
		}

		if (isset($this->request->get['filter_division_id'])) {
			$filter_division_id = $this->request->get['filter_division_id'];
		} else {
			$filter_division_id = '';
		}

		if (isset($this->request->get['filter_state_id'])) {
			$filter_state_id = $this->request->get['filter_state_id'];
		} else {
			$filter_state_id = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'unit';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}

		if (isset($this->request->get['filter_division_id'])) {
			$url .= '&filter_division_id=' . $this->request->get['filter_division_id'];
		}

		if (isset($this->request->get['filter_state_id'])) {
			$url .= '&filter_state_id=' . $this->request->get['filter_state_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/unit', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/unit/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['export'] = $this->url->link('catalog/unit/export', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['delete'] = $this->url->link('catalog/unit/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['units'] = array();

		$data = array(
			'filter_name'  => $filter_name,
			'filter_name_id'  => $filter_name_id,
			'filter_division'  => $filter_division,
			'filter_division_id'  => $filter_division_id,
			'filter_state_id'  => $filter_state_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$unit_total = $this->model_catalog_unit->getTotalUnits($data);

		$results = $this->model_catalog_unit->getUnits($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/unit/update', 'token=' . $this->session->data['token'] . '&unit_id=' . $result['unit_id'] . $url, 'SSL')
			);

			$this->data['units'][] = array(
				'unit_id' => $result['unit_id'],
				'unit'        => $result['unit'],
				'unit_code'        => $result['unit_code'],
				'division_name'        => $result['division_name'],
				'state_name'        => $result['state_name'],
				'selected'       => isset($this->request->post['selected']) && in_array($result['unit_id'], $this->request->post['selected']),
				'action'         => $action
			);
		}

		$this->load->model('catalog/division');
		$division_datas = $this->model_catalog_division->getDivisions($data = array());
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			$dvalue['division'] = html_entity_decode($dvalue['division']);
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;

		$this->load->model('catalog/state');
		$state_datas = $this->model_catalog_state->getStates();
		$state_data = array();
		$state_data['0'] = 'All';
		foreach ($state_datas as $dkey => $dvalue) {
			$state_data[$dvalue['state_id']] = $dvalue['state'];
		}
		$this->data['state_data'] = $state_data;	

		$this->data['token'] = $this->session->data['token'];

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_title'] = $this->language->get('column_title');
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
		$this->data['column_action'] = $this->language->get('column_action');		

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}

		if (isset($this->request->get['filter_division_id'])) {
			$url .= '&filter_division_id=' . $this->request->get['filter_division_id'];
		}

		if (isset($this->request->get['filter_state_id'])) {
			$url .= '&filter_state_id=' . $this->request->get['filter_state_id'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_unit'] = $this->url->link('catalog/unit', 'token=' . $this->session->data['token'] . '&sort=unit' . $url, 'SSL');
		$this->data['sort_unit_code'] = $this->url->link('catalog/unit', 'token=' . $this->session->data['token'] . '&sort=unit_code' . $url, 'SSL');
		$this->data['sort_division_name'] = $this->url->link('catalog/unit', 'token=' . $this->session->data['token'] . '&sort=division_name' . $url, 'SSL');
		$this->data['sort_state_name'] = $this->url->link('catalog/unit', 'token=' . $this->session->data['token'] . '&sort=state_name' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}

		if (isset($this->request->get['filter_division_id'])) {
			$url .= '&filter_division_id=' . $this->request->get['filter_division_id'];
		}

		if (isset($this->request->get['filter_state_id'])) {
			$url .= '&filter_state_id=' . $this->request->get['filter_state_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $unit_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/unit', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_division'] = $filter_division;
		$this->data['filter_division_id'] = $filter_division_id;
		$this->data['filter_state_id'] = $filter_state_id;
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/unit_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_bottom'] = $this->language->get('entry_bottom');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['unit'])) {
			$this->data['error_unit'] = $this->error['unit'];
		} else {
			$this->data['error_unit'] = array();
		}

		if (isset($this->error['division'])) {
			$this->data['error_division'] = $this->error['division'];
		} else {
			$this->data['error_division'] = '';
		}

		if (isset($this->error['state'])) {
			$this->data['error_state'] = $this->error['state'];
		} else {
			$this->data['error_state'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}

		if (isset($this->request->get['filter_division_id'])) {
			$url .= '&filter_division_id=' . $this->request->get['filter_division_id'];
		}

		if (isset($this->request->get['filter_state_id'])) {
			$url .= '&filter_state_id=' . $this->request->get['filter_state_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/unit', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['unit_id'])) {
			$this->data['action'] = $this->url->link('catalog/unit/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/unit/update', 'token=' . $this->session->data['token'] . '&unit_id=' . $this->request->get['unit_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/unit', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['unit_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$unit_info = $this->model_catalog_unit->getUnit($this->request->get['unit_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['unit'])) {
			$this->data['unit'] = $this->request->post['unit'];
		} elseif (!empty($unit_info)) {
			$this->data['unit'] = $unit_info['unit'];
		} else {
			$this->data['unit'] = '';
		}

		if (isset($this->request->post['unit_code'])) {
			$this->data['unit_code'] = $this->request->post['unit_code'];
		} elseif (!empty($unit_info)) {
			$this->data['unit_code'] = $unit_info['unit_code'];
		} else {
			$this->data['unit_code'] = '';
		}

		if (isset($this->request->post['division_id'])) {
			$this->data['division_id'] = $this->request->post['division_id'];
		} elseif (!empty($unit_info)) {
			$this->data['division_id'] = $unit_info['division_id'];
		} else {
			$this->data['division_id'] = '';
		}

		if (isset($this->request->post['division_name'])) {
			$this->data['division_name'] = $this->request->post['division_name'];
		} elseif (!empty($unit_info)) {
			$this->data['division_name'] = $unit_info['division_name'];
		} else {
			$this->data['division_name'] = '';
		}

		$this->load->model('catalog/division');
		$division_datas = $this->model_catalog_division->getDivisions($data = array());
		$division_data = array();
		$division_data['0'] = 'Please Select';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			$dvalue['division'] = html_entity_decode($dvalue['division']);
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;

		if (isset($this->request->post['state_id'])) {
			$this->data['state_id'] = $this->request->post['state_id'];
		} elseif (!empty($unit_info)) {
			$this->data['state_id'] = $unit_info['state_id'];
		} else {
			$this->data['state_id'] = '';
		}

		if (isset($this->request->post['state_name'])) {
			$this->data['state_name'] = $this->request->post['state_name'];
		} elseif (!empty($unit_info)) {
			$this->data['state_name'] = $unit_info['state_name'];
		} else {
			$this->data['state_name'] = '';
		}

		$this->load->model('catalog/state');
		$state_datas = $this->model_catalog_state->getStates();
		$state_data = array();
		$state_data['0'] = 'Please Select';
		foreach ($state_datas as $dkey => $dvalue) {
			$state_data[$dvalue['state_id']] = $dvalue['state'];
		}
		$this->data['state_data'] = $state_data;

		$this->template = 'catalog/unit_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if(isset($this->request->get['unit_id'])){
			if (!$this->user->hasPermission('modify', 'catalog/unit')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		} else {
			if (!$this->user->hasPermission('add', 'catalog/unit')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		}

		$units = $this->db->query("SELECT * FROM oc_unit WHERE unit_code = '".$this->request->post['unit']."' ");
		if ($units->num_rows > 0) {
			$this->error['unit'] = 'This Unit Is Already Exist';
		}

		if ((utf8_strlen($this->request->post['unit']) < 1) || (utf8_strlen($this->request->post['unit']) > 64)) {
			$this->error['unit'] = 'Plese Enter Unit Name';
		}

		if ($this->request->post['division_id'] == '' || $this->request->post['division_id'] == '0') {
			$this->error['division'] = 'Plese Enter Division Name';
		}

		if ($this->request->post['state_id'] == '' || $this->request->post['state_id'] == '0') {
			$this->error['state'] = 'Plese Enter State Name';
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('delete', 'catalog/unit')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/unit');
			if(isset($this->request->get['filter_division_id']) && $this->request->get['filter_division_id'] != '' && $this->request->get['filter_division_id'] != '0'){
				$filter_division_id = $this->request->get['filter_division_id'];
			} else {
				$filter_division_id = '';
			}
			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_division_id' => $filter_division_id,
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_unit->getUnits($data);
			foreach ($results as $result) {
				$json[] = array(
					'unit_id' => $result['unit_id'],
					'unit'    => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['unit'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}
}
?>