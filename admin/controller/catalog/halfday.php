<?php    
class ControllerCatalogHalfday extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/halfday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/halfday');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/halfday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/halfday');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_halfday->addhalfday($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/halfday', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/halfday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/halfday');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_halfday->edithalfday($this->request->get['halfday_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/halfday', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/halfday');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/halfday');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $halfday_id) {
				$this->model_catalog_halfday->deletehalfday($halfday_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/halfday', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['halfday_id']) && $this->validateDelete()){
			$this->model_catalog_halfday->deletehalfday($this->request->get['halfday_id']);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/halfday', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/halfday', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/halfday/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/halfday/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['halfdays'] = array();

		$data = array(
			'filter_name' => $filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$halfday_total = $this->model_catalog_halfday->getTotalhalfdays($data);

		$results = $this->model_catalog_halfday->gethalfdays($data);

		// echo '<pre>';
		// print_r($results);
		// exit;

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/halfday/update', 'token=' . $this->session->data['token'] . '&halfday_id=' . $result['halfday_id'] . $url, 'SSL')
			);

			// $action[] = array(
			// 	'text' => 'Delete',
			// 	'href' => $this->url->link('catalog/halfday/delete', 'token=' . $this->session->data['token'] . '&halfday_id=' . $result['halfday_id'] . $url, 'SSL')
			// );

			$this->data['halfdays'][] = array(
				'halfday_id' => $result['halfday_id'],
				'name' => $result['name'],
				'date'            => $result['date'],
				'selected'        => isset($this->request->post['selected']) && in_array($result['halfday_id'], $this->request->post['selected']),
				'action'          => $action
			);
		}

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/halfday', 'token=' . $this->session->data['token'] . '&sort=date' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		$pagination = new Pagination();
		$pagination->total = $halfday_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/halfday', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		
		$this->template = 'catalog/halfday_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['date'])) {
			$this->data['error_date'] = $this->error['date'];
		} else {
			$this->data['error_date'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/halfday', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['halfday_id'])) {
			$this->data['action'] = $this->url->link('catalog/halfday/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/halfday/update', 'token=' . $this->session->data['token'] . '&halfday_id=' . $this->request->get['halfday_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/halfday', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['halfday_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$halfday_info = $this->model_catalog_halfday->gethalfday($this->request->get['halfday_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		$this->load->model('report/attendance');

		//$unit_data = array(
		//	'Mumbai' => 'Mumbai',
		//	'Pune' => 'Pune',
		//	'Moving' => 'Moving' 
		//);

//		//$this->data['unit_data'] = $unit_data;

//
		$unit_datas = $this->model_report_attendance->getunit_list();

//		//echo '<pre>';
		//print_r($unit_datas);
		//exit();
		$unit_data = array();
		//$unit_data['0'] = 'All';
		foreach ($unit_datas as $dkey => $dvalue) {
			$unit_data[strtolower(trim($dvalue['unit']))] = $dvalue['unit'];
			//echo($dvalue['unit']);
		}
		$this->data['unit_data'] = $unit_datas;

		//echo '<pre>';
		//print_r($unit_datas);
		//exit();

		
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		//$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[strtolower($dvalue['department'])] = $dvalue['department'];
		}
		$this->data['department_data'] = $department_data;
		
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($halfday_info)) {
			$this->data['name'] = $halfday_info['name'];
		} else {	
			$this->data['name'] = '';
		}

		if (isset($this->request->post['date'])) {
			$this->data['date'] = $this->request->post['date'];
		} elseif (!empty($halfday_info)) {
			$this->data['date'] = $halfday_info['date'];
		} else {	
			$this->data['date'] = '';
		}

		if (isset($this->request->post['loc_holiday'])) {
			$this->data['loc_holiday'] = $this->request->post['loc_holiday'];
		} elseif (!empty($halfday_info['location'])) {
			$this->data['loc_holiday'] = unserialize($halfday_info['location']);
			//$this->data['loc_holiday'] = array();
		} else {	
			$this->data['loc_holiday'] = array();
		}

		// echo '<pre>';
		// print_r($halfday_info);
		// exit;

		if (isset($this->request->post['dept_holiday_mumbai'])) {
			$this->data['dept_holiday_mumbai'] = $this->request->post['dept_holiday_mumbai'];
		} elseif (!empty($halfday_info['department_mumbai'])) {
			$this->data['dept_holiday_mumbai'] = unserialize($halfday_info['department_mumbai']);
			foreach ($this->data['dept_holiday_mumbai'] as $key => $value) {
				$this->data['dept_holiday_mumbai'][$key] = html_entity_decode(strtolower(trim($value)));
			}
			//$this->data['dept_holiday'] = array();
		} else {	
			$this->data['dept_holiday_mumbai'] = array();
		}

		if (isset($this->request->post['dept_holiday_pune'])) {
			$this->data['dept_holiday_pune'] = $this->request->post['dept_holiday_pune'];
		} elseif (!empty($halfday_info['department_pune'])) {
			$this->data['dept_holiday_pune'] = unserialize($halfday_info['department_pune']);
			foreach ($this->data['dept_holiday_pune'] as $key => $value) {
				$this->data['dept_holiday_pune'][$key] = html_entity_decode(strtolower(trim($value)));
			}
			//$this->data['dept_holiday'] = array();
		} else {	
			$this->data['dept_holiday_pune'] = array();
		}

		if (isset($this->request->post['dept_holiday_moving'])) {
			$this->data['dept_holiday_moving'] = $this->request->post['dept_holiday_moving'];
		} elseif (!empty($halfday_info['department_moving'])) {
			$this->data['dept_holiday_moving'] = unserialize($halfday_info['department_moving']);
			foreach ($this->data['dept_holiday_moving'] as $key => $value) {
				$this->data['dept_holiday_moving'][$key] = html_entity_decode(strtolower(trim($value)));
			}
			//$this->data['dept_holiday'] = array();
		} else {	
			$this->data['dept_holiday_moving'] = array();
		}

		$this->template = 'catalog/halfday_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		$this->load->model('catalog/halfday');

		if (!$this->user->hasPermission('modify', 'catalog/halfday')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if($this->request->post['date'] != '' && !isset($this->request->get['halfday_id'])){
			$is_exist = $this->model_catalog_halfday->gethalfday_exist($this->request->post['date']);
			if($is_exist == 1){
				$this->error['warning'] = 'Half Day With Same Date Present';
			}
		}

		$date = $this->request->post['date'];
		if($this->user->getId() == 3){
			if(isset($this->request->post['dept_holiday_mumbai'])){
				$sql = "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `day_close_status` = '1' AND LOWER(`unit`) = 'mumbai' ";
				$query = $this->db->query($sql);
				if($query->num_rows > 0) {
					$this->error['warning'] = 'Day Closed for Mumbai on date : ' . $this->request->post['date'];		
				}
			}
		}

		if($this->user->getId() == 4){
			if(isset($this->request->post['dept_holiday_pune'])){
				$sql = "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `day_close_status` = '1' AND LOWER(`unit`) = 'pune' ";
				$query = $this->db->query($sql);
				if($query->num_rows > 0) {
					$this->error['warning'] = 'Day Closed for Pune on date : ' . $this->request->post['date'];		
				}
			}
		}

		if($this->user->getId() == 7){
			if(isset($this->request->post['dept_holiday_moving'])){
				$sql = "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `day_close_status` = '1' AND LOWER(`unit`) = 'moving' ";
				$query = $this->db->query($sql);
				if($query->num_rows > 0) {
					$this->error['warning'] = 'Day Closed for Moving on date : ' . $this->request->post['date'];		
				}
			}
		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/halfday')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// $this->load->model('catalog/horse');

		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $halfday_id) {
		// 		$horse_total = $this->model_catalog_horse->getTotalhorsesByhalfdayId($halfday_id);

		// 		if ($horse_total) {
		// 			$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 		}	
		// 	}
		// } elseif(isset($this->request->get['halfday_id'])){
		// 	$horse_total = $this->model_catalog_horse->getTotalhorsesByhalfdayId($this->request->get['halfday_id']);

		// 	if ($horse_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}  
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/halfday');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_halfday->gethalfdays($data);

			foreach ($results as $result) {
				$json[] = array(
					'halfday_id' => $result['halfday_id'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}	
}
?>