<?php    
class ControllerCatalogEmployee extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/employee');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/employee');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/employee');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/employee');

		// echo '<pre>';
		// print_r($this->request->post);
		// exit;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_employee->addemployee($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_code'])) {
				$url .= '&filter_code=' . $this->request->get['filter_code'];
			}

			if (isset($this->request->get['filter_department'])) {
				$url .= '&filter_department=' . $this->request->get['filter_department'];
			}

			if (isset($this->request->get['filter_division'])) {
				$url .= '&filter_division=' . $this->request->get['filter_division'];
			}

			if (isset($this->request->get['filter_region'])) {
				$url .= '&filter_region=' . $this->request->get['filter_region'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_company'])) {
				$url .= '&filter_company=' . $this->request->get['filter_company'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url . '&filter_name=' . $this->request->post['name'], 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/employee');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/employee');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_catalog_employee->editemployee($this->request->get['employee_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_code'])) {
				$url .= '&filter_code=' . $this->request->get['filter_code'];
			}

			if (isset($this->request->get['filter_department'])) {
				$url .= '&filter_department=' . $this->request->get['filter_department'];
			}

			if (isset($this->request->get['filter_division'])) {
				$url .= '&filter_division=' . $this->request->get['filter_division'];
			}

			if (isset($this->request->get['filter_region'])) {
				$url .= '&filter_region=' . $this->request->get['filter_region'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_company'])) {
				$url .= '&filter_company=' . $this->request->get['filter_company'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['return'])){
				$this->redirect($this->url->link('report/employee_data', 'token=' . $this->session->data['token'].'&h_name='.$this->request->post['name'].'&h_name_id='.$this->request->get['employee_id'], 'SSL'));
			} else {
				$this->redirect($this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url , 'SSL'));
			}
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/employee');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/employee');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $employee_id) {
				$this->model_catalog_employee->deleteemployee($employee_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_code'])) {
				$url .= '&filter_code=' . $this->request->get['filter_code'];
			}

			if (isset($this->request->get['filter_department'])) {
				$url .= '&filter_department=' . $this->request->get['filter_department'];
			}

			if (isset($this->request->get['filter_division'])) {
				$url .= '&filter_division=' . $this->request->get['filter_division'];
			}

			if (isset($this->request->get['filter_region'])) {
				$url .= '&filter_region=' . $this->request->get['filter_region'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_company'])) {
				$url .= '&filter_company=' . $this->request->get['filter_company'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['employee_id']) && $this->validateDelete()){
			$this->model_catalog_employee->deleteemployee($this->request->get['employee_id']);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_code'])) {
				$url .= '&filter_code=' . $this->request->get['filter_code'];
			}

			if (isset($this->request->get['filter_department'])) {
				$url .= '&filter_department=' . $this->request->get['filter_department'];
			}

			if (isset($this->request->get['filter_division'])) {
				$url .= '&filter_division=' . $this->request->get['filter_division'];
			}

			if (isset($this->request->get['filter_region'])) {
				$url .= '&filter_region=' . $this->request->get['filter_region'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_company'])) {
				$url .= '&filter_company=' . $this->request->get['filter_company'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function export_depthead(){
		$this->language->load('catalog/employee');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->data['attendace'] = array();

		
		$final_datas = array();
		$results = $this->db->query("SELECT * FROM `oc_employee` ")->rows;

		foreach ($results as $rkey => $rvalue) {
			$shift_data = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$rvalue['emp_code'] . "' ");
			foreach($shift_data->rows as $skey => $svalue){
				$icnt = 0;
				foreach ($svalue as $fkey => $fvalue) {
					if($icnt > 1 && $icnt <= 32){
						$s_data_exp = explode('_', $fvalue);
						if($s_data_exp[0] == 'S'){
							$query=$this->db->query("SELECT `name` FROM oc_shift WHERE `shift_id` = '".$s_data_exp[1]."' ");
							$shift = $query->row['name'];
						}
					}
					$icnt ++;
				}
			}
			$dept = $rvalue['department'];
			if($rvalue['status'] == '1'){
				$status = 'Active';
			} else {
				$status = 'Inactive';
			}
			if($rvalue['dob'] != '0000-00-00'){
				$rvalue['dob'] = date('d-m-Y', strtotime($rvalue['dob']));
			} else {
				$rvalue['dob'] = '00-00-0000';
			}
			if($rvalue['doj'] != '0000-00-00'){
				$rvalue['doj'] = date('d-m-Y', strtotime($rvalue['doj']));
			} else {
				$rvalue['doj'] = '00-00-0000';
			}
			if($rvalue['doc'] != '0000-00-00'){
				$rvalue['doc'] = date('d-m-Y', strtotime($rvalue['doc']));
			} else {
				$rvalue['doc'] = '00-00-0000';
			}
			if($rvalue['dol'] != '0000-00-00'){
				$rvalue['dol'] = date('d-m-Y', strtotime($rvalue['dol']));
			} else {
				$rvalue['dol'] = '00-00-0000';
			}
			$final_datas[] = array(
				'employee_id' => $rvalue['employee_id'],
				'code' 	      => $rvalue['emp_code'],
				'name'        => $rvalue['name'],
				'gender'	  => $rvalue['gender'],
				'dob'		  => $rvalue['dob'],
				'doj'		  => $rvalue['doj'],
				'doc'		  => $rvalue['doc'],
				'employement' => $rvalue['employement'],
				'grade'       => $rvalue['grade'],
				'dol'		  => $rvalue['dol'],
				'designation' => $rvalue['designation'],
				'department'  => $dept,
				'unit'		  => $rvalue['unit'],
				'division'	  => $rvalue['division'],
				'region'	  => $rvalue['region'],
				'state'       => $rvalue['state'],
				'shift'		  => $shift,
				'status'      => $status,
				'company' 	  => $rvalue['company'],
			);
		}
		
		// $final_datas = array_chunk($final_datas, 15);
		// echo '<pre>';
		// print_r($final_datas);
		// exit;
		
		if($final_datas){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['title'] = 'Employee List';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('catalog/export_depthead.tpl');
			//echo $html;exit;
			$filename = "Employee_List";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;

			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename);
			// echo $html;
			// exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('catalog/employee', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}


	public function export_dept(){
		$this->language->load('catalog/employee');
		$this->load->model('catalog/employee');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->data['attendace'] = array();

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_code'])) {
			$filter_code = $this->request->get['filter_code'];
		} else {
			$filter_code = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['filter_company'])) {
			$filter_company = $this->request->get['filter_company'];
		} else {
			$filter_company = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}

		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		$this->data['employees'] = array();
		$data = array(
			'filter_name' => $filter_name,
			'filter_code' => $filter_code,
			'filter_department' => $filter_department,
			'filter_unit' => $filter_unit,
			'filter_company' => $filter_company,
			'sort'  => $sort,
			'order' => $order,
			//'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			//'limit' => $this->config->get('config_admin_limit')
		);
		//$employee_total = $this->model_catalog_employee->getTotalemployees($data);
		$fp = fopen(DIR_DOWNLOAD . "Employee_list.csv", "w"); 
		$row = array();
		$row = $this->model_catalog_employee->getemployees_export($data);
		$line = "";
		$comma = "";
		
		//$line .= $comma . '"' . str_replace('"', '""', "Sr. No") . '"';
		//$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Emp_Code") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Emp_Name") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Designation") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Date of Joining") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Date of Confirmation") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Company") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Department") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Site") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Division") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Grade") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Category") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Employment") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Gender") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Contractor") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Contractor Code") . '"';
		$comma = ",";
		$line .= $comma . '"' . str_replace('"', '""', "Status") . '"';
		$comma = ",";

		$line .= "\n";
		fputs($fp, $line);
		$i ='1';
		foreach($row as $key => $value) { 
			$status_value= $value['status'];
			if($status_value=='1') {
				$status='Active';
			} else {
				$status='Inactive';
			} 	

			if($value['doj'] != '0000-00-00'){
				$doj_act = new DateTime($value['doj']);
				$doj = date_format($doj_act,"d-m-Y");
			} else {
				$doj = '';
			}
			
			if($value['doc'] != '0000-00-00'){
				$doc_act = new DateTime($value['doc']);
				$doc = date_format($doc_act,"d-m-Y");
			} else {
				$doc = '';
			}

			if($value['gender'] == 'M'){
				$value['gender'] = 'M';
			} elseif($value['gender'] == 'F'){
				$value['gender'] = 'F';
			} else {
				$value['gender'] = '';
			}
			
			$comma = "";
			$line = "";
			$line .= $comma . '"' . str_replace('"', '""', "~".$value['emp_code']) . '"';
			$comma = ",";
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['name'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['designation'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', $doj) . '"';
			$line .= $comma . '"' . str_replace('"', '""', $doc) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['company'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['department'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['unit'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['division'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['grade'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['category'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['employement'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['gender'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['contractor'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', html_entity_decode($value['contractor_code'])) . '"';
			$line .= $comma . '"' . str_replace('"', '""', $status) . '"';
			$i++;
			$line .= "\n";
			fputs($fp, $line);
		}
		fclose($fp);
		$filename = "Employee_list.csv";
		$file_path = DIR_DOWNLOAD . $filename;
		//$file_path = '/Library/WebServer/Documents/riskmanagement/'.$filename;
		$html = file_get_contents($file_path);
		header('Content-type: text/html');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $html;
		exit;
	}


	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_code'])) {
			$filter_code = $this->request->get['filter_code'];
		} else {
			$filter_code = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = '';
		}

		if (isset($this->request->get['filter_division'])) {
			$filter_division = $this->request->get['filter_division'];
		} else {
			$filter_division = '';
		}

		if (isset($this->request->get['filter_region'])) {
			$filter_region = $this->request->get['filter_region'];
		} else {
			$filter_region = '';
		}

		if (isset($this->request->get['filter_company'])) {
			$filter_company = $this->request->get['filter_company'];
		} else {
			$filter_company = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['filter_contractor'])) {
			$filter_contractor = $this->request->get['filter_contractor'];
		} else {
			$filter_contractor = '';
		}

		if (isset($this->request->get['filter_contractor_id'])) {
			$filter_contractor_id = $this->request->get['filter_contractor_id'];
		} else {
			$filter_contractor_id = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}

		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}

		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}

		if (isset($this->request->get['filter_contractor_id'])) {
			$url .= '&filter_contractor_id=' . $this->request->get['filter_contractor_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/employee/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/employee/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['export_depthead'] = $this->url->link('catalog/employee/export_dept', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['import_process'] = $this->url->link('catalog/employee/imports', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['employees'] = array();

		if(isset($this->session->data['final_employee_error_data']) && !empty($this->session->data['final_employee_error_data'])){
			//echo '<pre>';
			//print_r($this->session->data['final_employee_error_data']);
			//exit;
			$this->data['error_list'] = '1'; 
			$this->data['employees'] = $this->session->data['final_employee_error_data'];
			$employee_total = 0;
			foreach($this->data['employees'] as $keys => $values){
				foreach($values as $key => $value){
					$employee_total ++;
				}
			}
			unset($this->session->data['final_employee_error_data']);
		} else {
			$this->data['error_list'] = '0';
			$data = array(
				'filter_name' => $filter_name,
				'filter_code' => $filter_code,
				'filter_department' => $filter_department,
				'filter_division' => $filter_division,
				'filter_region' => $filter_region,
				'filter_company' => $filter_company,
				'filter_contractor' => $filter_contractor,
				'filter_contractor_id' => $filter_contractor_id,
				'filter_unit' => $filter_unit,
				'filter_jmc' => '1',
				'sort'  => $sort,
				'order' => $order,
				'start' => ($page - 1) * $this->config->get('config_admin_limit'),
				'limit' => $this->config->get('config_admin_limit')
			);

			$employee_total = $this->model_catalog_employee->getTotalemployees($data);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$action = array();

				$action[] = array(
					'text' => $this->language->get('text_edit'),
					'href' => $this->url->link('catalog/employee/update', 'token=' . $this->session->data['token'] . '&employee_id=' . $result['employee_id'] . $url, 'SSL')
				);

				// $action[] = array(
				// 	'text' => $this->language->get('text_delete'),
				// 	'href' => $this->url->link('catalog/employee/delete', 'token=' . $this->session->data['token'] . '&employee_id=' . $result['employee_id'] . $url, 'SSL')
				// );

				$today_date = date('j');
				$month = date('n');
				$year = date('Y');
				// $shift_data = $this->model_catalog_employee->getshift_id($result['emp_code'], $today_date, $month, $year);
				// $shift_ids = explode('_', $shift_data);
				// if(isset($shift_ids[1])){
				// 	if($shift_ids[0] == 'S'){
				// 		$shift_id = $shift_ids['1'];
				// 		$shift_name = $this->model_catalog_employee->getshiftdata($shift_id);
				// 	} elseif($shift_ids[0] == 'W') {
				// 		$shift_id = $shift_ids['1'];
				// 		//$shift_name = $this->model_catalog_employee->getweekname($shift_id);
				// 		$shift_name = 'Weekly Off';
				// 	} elseif($shift_ids[0] == 'H'){
				// 		$shift_id = $shift_ids['1'];
				// 		//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
				// 		$shift_name = 'Holiday';
				// 	} elseif($shift_ids[0] == 'HD'){
				// 		$shift_id = $shift_ids['1'];
				// 		//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
				// 		$shift_name = 'Half Day';
				// 	} elseif($shift_ids[0] == 'C'){
				// 		$shift_id = $shift_ids['1'];
				// 		//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
				// 		$shift_name = 'Complimentary Off';
				// 	}
				// } else {
				// 	$shift_id = 0;
				// 	$shift_name = '';
				// }
				if($result['shift_id'] == '1'){
					$shift_name = '08:00:00 - 20:00:00';
				} elseif($result['shift_id'] == '2'){
					$shift_name = '09:30:00 - 18:30:00';
				}

				//$shift = $this->model_catalog_employee->getshiftname($result['shift']);
				$location = $result['unit'];//$this->model_catalog_employee->getlocname($result['location']); //'Mumbai'
				//$category = $result['category'];//$this->model_catalog_employee->getcatname($result['category']); //'PLD';
				$dept = $result['department'];//$this->model_catalog_employee->getdeptname($result['department']); //'Personal';

				if($result['is_dept'] == '1'){
					$dept_head = 'Department head';
				} else {
					$dept_head = '';
				}

				$this->data['employees'][] = array(
					'employee_id' => $result['employee_id'],
					'name'        => $result['name'],
					'code' 	      => $result['emp_code'],
					'division' 	  => $result['division'],
					'region' 	  => $result['region'],
					'company' 	  => $result['company'],
					'contractor'  => $result['contractor'].'-'.$result['contractor_code'],
					'location' 	  => $location,
					'department'  => $dept,
					'shift'       => $shift_name,
					'dept_head'   => $dept_head,
					'selected'    => isset($this->request->post['selected']) && in_array($result['employee_id'], $this->request->post['selected']),
					'action'      => $action
				);
			}
		}

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_trainer'] = $this->language->get('column_trainer');
		$this->data['column_action'] = $this->language->get('column_action');		

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} elseif (isset($this->session->data['warning'])) {
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}

		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}

		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}

		if (isset($this->request->get['filter_contractor_id'])) {
			$url .= '&filter_contractor_id=' . $this->request->get['filter_contractor_id'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_code'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=emp_code' . $url, 'SSL');
		$this->data['sort_department'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=department' . $url, 'SSL');
		$this->data['sort_division'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=division' . $url, 'SSL');
		$this->data['sort_region'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=region' . $url, 'SSL');
		$this->data['sort_company'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=company' . $url, 'SSL');
		$this->data['sort_unit'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=unit' . $url, 'SSL');
		$this->data['sort_contractor'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=contractor' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}

		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}

		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}

		if (isset($this->request->get['filter_contractor_id'])) {
			$url .= '&filter_contractor_id=' . $this->request->get['filter_contractor_id'];
		}

		$this->load->model('report/attendance');

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $filter_division,
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		$site_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['site_data'] = $site_data;

		$this->load->model('catalog/contractor');
		if($filter_unit != '' && $filter_unit != '0'){
			$data = array(
				'filter_unit_id' => $filter_unit,
				'filter_master' => 0,
			);
			$contractor_datas = $this->model_catalog_contractor->getContractors_by_unit($data);
			foreach ($contractor_datas as $ckey => $cvalue) {
				$contractor_data[$cvalue['contractor_id']] = $cvalue['contractor_name'];
			}
		} else {
			$contractor_data = array();
			$contractor_data['0'] = 'All';
			$contractor_datas = $this->model_catalog_contractor->getContractor_tots();
			foreach ($contractor_datas as $ckey => $cvalue) {
				$contractor_data[$cvalue['contractor_id']] = $cvalue['contractor_name'].'-'.$cvalue['contractor_code'];
			}
		}
		
		$this->data['contractor_data'] = $contractor_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;

		

		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;


		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $filter_region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;		

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array) && $dvalue['company_id'] == '1'){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				// if($dvalue['company_id'] == '1'){
					$company_data[$dvalue['company_id']] = $dvalue['company'];
				// }
			}
		}
		$this->data['company_data'] = $company_data;
		// echo '<pre>';
		// print_r($this->data['division']);
		// exit;
		
		$pagination = new Pagination();
		$pagination->total = $employee_total;
		$pagination->page = $page;
		if($this->data['error_list'] == '1'){
			$pagination->limit = 7777;
		} else {
			$pagination->limit = $this->config->get('config_admin_limit');
		}
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_code'] = $filter_code;
		$this->data['filter_department'] = $filter_department;
		$this->data['filter_region'] = $filter_region;
		$this->data['filter_division'] = $filter_division;
		$this->data['filter_company'] = $filter_company;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_contractor'] = $filter_contractor;
		$this->data['filter_contractor_id'] = $filter_contractor_id;

		$this->template = 'catalog/employee_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_dob'] = $this->language->get('entry_dob');
		$this->data['entry_doj'] = $this->language->get('entry_doj');
		$this->data['entry_action'] = $this->language->get('entry_action');
		$this->data['entry_assign'] = $this->language->get('entry_assign');
		$this->data['entry_remove'] = $this->language->get('entry_remove');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['emp_code'])) {
			$this->data['error_emp_code'] = $this->error['emp_code'];
		} else {
			$this->data['error_emp_code'] = '';
		}

		if (isset($this->error['doj'])) {
			$this->data['error_doj'] = $this->error['doj'];
		} else {
			$this->data['error_doj'] = '';
		}

		if (isset($this->error['change_date'])) {
			$this->data['error_change_date'] = $this->error['change_date'];
		} else {
			$this->data['error_change_date'] = '';
		}

		if (isset($this->error['dol'])) {
			$this->data['error_dol'] = $this->error['dol'];
		} else {
			$this->data['error_dol'] = '';
		}

		if (isset($this->error['designation'])) {
			$this->data['error_designation'] = $this->error['designation'];
		} else {
			$this->data['error_designation'] = '';
		}

		if (isset($this->error['department'])) {
			$this->data['error_department'] = $this->error['department'];
		} else {
			$this->data['error_department'] = '';
		}

		if (isset($this->error['contractor'])) {
			$this->data['error_contractor'] = $this->error['contractor'];
		} else {
			$this->data['error_contractor'] = '';
		}

		if (isset($this->error['site'])) {
			$this->data['error_site'] = $this->error['site'];
		} else {
			$this->data['error_site'] = '';
		}

		if (isset($this->error['division'])) {
			$this->data['error_division'] = $this->error['division'];
		} else {
			$this->data['error_division'] = '';
		}

		if (isset($this->error['state'])) {
			$this->data['error_state'] = $this->error['state'];
		} else {
			$this->data['error_state'] = '';
		}

		if (isset($this->error['company'])) {
			$this->data['error_company'] = $this->error['company'];
		} else {
			$this->data['error_company'] = '';
		}

		if (isset($this->error['contractor'])) {
			$this->data['error_contractor'] = $this->error['contractor'];
		} else {
			$this->data['error_contractor'] = '';
		}

		if (isset($this->error['category'])) {
			$this->data['error_category'] = $this->error['category'];
		} else {
			$this->data['error_category'] = '';
		}

		if (isset($this->error['grade'])) {
			$this->data['error_grade'] = $this->error['grade'];
		} else {
			$this->data['error_grade'] = '';
		}

		if (isset($this->error['employement'])) {
			$this->data['error_employement'] = $this->error['employement'];
		} else {
			$this->data['error_employement'] = '';
		}

		if (isset($this->error['region'])) {
			$this->data['error_region'] = $this->error['region'];
		} else {
			$this->data['error_region'] = '';
		}

		if (isset($this->error['card_number'])) {
			$this->data['error_card_number'] = $this->error['card_number'];
		} else {
			$this->data['error_card_number'] = '';
		}

		if (isset($this->error['daily_punch'])) {
			$this->data['error_daily_punch'] = $this->error['daily_punch'];
		} else {
			$this->data['error_daily_punch'] = '';
		}

		if (isset($this->error['weekly_off'])) {
			$this->data['error_weekly_off'] = $this->error['weekly_off'];
		} else {
			$this->data['error_weekly_off'] = '';
		}

		if (isset($this->error['status'])) {
			$this->data['error_status'] = $this->error['status'];
		} else {
			$this->data['error_status'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}

		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}

		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}


		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (isset($this->request->get['employee_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$employee_info = $this->model_catalog_employee->getemployee($this->request->get['employee_id']);
		}

		//echo '<pre>';print_r($employee_info);exit;
		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($employee_info)) {
			$this->data['name'] = $employee_info['name'];
		} else {	
			$this->data['name'] = '';
		}

		if (isset($this->request->post['emp_code'])) {
			$this->data['emp_code'] = $this->request->post['emp_code'];
			$this->data['hidden_emp_code'] = $this->request->post['hidden_emp_code'];
		} elseif (!empty($employee_info)) {
			$this->data['emp_code'] = $employee_info['emp_code'];
			$this->data['hidden_emp_code'] = $employee_info['emp_code'];
		} else {	
			$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` ORDER BY `emp_code` DESC LIMIT 1");
			if($emp_codes->num_rows > 0){
				$emp_code = $emp_codes->row['emp_code'] + 1;
			} else {
				$emp_code = '1000';
			}
			$this->data['emp_code'] = $emp_code;
			$this->data['hidden_emp_code'] = $emp_code;
		}

		if (isset($this->request->post['grade'])) {
			$this->data['grade'] = $this->request->post['grade'];
			$this->data['hidden_grade'] = $this->request->post['hidden_grade'];
		} elseif (!empty($employee_info)) {
			$this->data['grade'] = $employee_info['grade'];
			$this->data['hidden_grade'] = $employee_info['grade'];
		} else {	
			$this->data['grade'] = '';
			$this->data['hidden_grade'] = '';
		}

		if (isset($this->request->post['grade_id'])) {
			$this->data['grade_id'] = $this->request->post['grade_id'];
			$this->data['hidden_grade_id'] = $this->request->post['hidden_grade_id'];
		} elseif (!empty($employee_info)) {
			$this->data['grade_id'] = $employee_info['grade_id'];
			$this->data['hidden_grade_id'] = $employee_info['grade_id'];
		} else {	
			$this->data['grade_id'] = '0';
			$this->data['hidden_grade_id'] = '0';
		}

		if (isset($this->request->post['department'])) {
			$this->data['department'] = $this->request->post['department'];
			$this->data['hidden_department'] = $this->request->post['hidden_department'];
		} elseif (!empty($employee_info)) {
			$this->data['department'] = $employee_info['department'];
			$this->data['hidden_department'] = $employee_info['department'];
		} else {	
			$this->data['department'] = '';
			$this->data['hidden_department'] = '';
		}

		if (isset($this->request->post['department_id'])) {
			$this->data['department_id'] = $this->request->post['department_id'];
			$this->data['hidden_department_id'] = $this->request->post['hidden_department_id'];
		} elseif (!empty($employee_info)) {
			$this->data['department_id'] = $employee_info['department_id'];
			$this->data['hidden_department_id'] = $employee_info['department_id'];
		} else {	
			$this->data['department_id'] = '0';
			$this->data['hidden_department_id'] = '0';
		}
		//contractor
		if (isset($this->request->post['contractor'])) {
			$this->data['contractor'] = $this->request->post['contractor'];
			$this->data['hidden_contractor'] = $this->request->post['hidden_contractor'];
		} elseif (!empty($employee_info)) {
			$this->data['contractor'] = $employee_info['contractor'];
			$this->data['hidden_contractor'] = $employee_info['contractor'];
		} else {	
			$this->data['contractor'] = '';
			$this->data['hidden_contractor'] = '';
		}

		if (isset($this->request->post['contractor_id'])) {
			$this->data['contractor_id'] = $this->request->post['contractor_id'];
			$this->data['hidden_contractor_id'] = $this->request->post['hidden_contractor_id'];
		} elseif (!empty($employee_info)) {
			$this->data['contractor_id'] = $employee_info['contractor_id'];
			$this->data['hidden_contractor_id'] = $employee_info['contractor_id'];
		} else {	
			$this->data['contractor_id'] = '0';
			$this->data['hidden_contractor_id'] = '0';
		}

		//
		//category
		if (isset($this->request->post['category'])) {
			$this->data['category'] = $this->request->post['category'];
			$this->data['hidden_category'] = $this->request->post['hidden_category'];
		} elseif (!empty($employee_info)) {
			$this->data['category'] = $employee_info['category'];
			$this->data['hidden_category'] = $employee_info['category'];
		} else {	
			$this->data['category'] = '';
			$this->data['hidden_category'] = '';
		}

		if (isset($this->request->post['category_id'])) {
			$this->data['category_id'] = $this->request->post['category_id'];
			$this->data['hidden_category_id'] = $this->request->post['hidden_category_id'];
		} elseif (!empty($employee_info)) {
			$this->data['category_id'] = $employee_info['category_id'];
			$this->data['hidden_category_id'] = $employee_info['category_id'];
		} else {	
			$this->data['category_id'] = '0';
			$this->data['hidden_category_id'] = '0';
		}

		//

		if (isset($this->request->post['division'])) {
			$this->data['division'] = $this->request->post['division'];
			$this->data['hidden_division'] = $this->request->post['hidden_division'];
		} elseif (!empty($employee_info)) {
			$this->data['division'] = $employee_info['division'];
			$this->data['hidden_division'] = $employee_info['division'];
		} else {	
			$this->data['division'] = '';
			$this->data['hidden_division'] = '';
		}

		if (isset($this->request->post['division_id'])) {
			$this->data['division_id'] = $this->request->post['division_id'];
			$this->data['hidden_division_id'] = $this->request->post['hidden_division_id'];
		} elseif (!empty($employee_info)) {
			$this->data['division_id'] = $employee_info['division_id'];
			$this->data['hidden_division_id'] = $employee_info['division_id'];
		} else {	
			$this->data['division_id'] = '';
			$this->data['hidden_division_id'] = '';
		}

		if (isset($this->request->post['region'])) {
			$this->data['region'] = $this->request->post['region'];
			$this->data['hidden_region'] = $this->request->post['hidden_region'];
		} elseif (!empty($employee_info)) {
			$this->data['region'] = $employee_info['region'];
			$this->data['hidden_region'] = $employee_info['region'];
		} else {	
			$this->data['region'] = '';
			$this->data['hidden_region'] = '';
		}

		if (isset($this->request->post['region_id'])) {
			$this->data['region_id'] = $this->request->post['region_id'];
			$this->data['hidden_region_id'] = $this->request->post['hidden_region_id'];
		} elseif (!empty($employee_info)) {
			$this->data['region_id'] = $employee_info['region_id'];
			$this->data['hidden_region_id'] = $employee_info['region_id'];
		} else {	
			$this->data['region_id'] = '';
			$this->data['hidden_region_id'] = '';
		}

		if (isset($this->request->post['unit'])) {
			$this->data['unit'] = $this->request->post['unit'];
			$this->data['hidden_unit'] = $this->request->post['hidden_unit'];
		} elseif (!empty($employee_info)) {
			$this->data['unit'] = $employee_info['unit'];
			$this->data['hidden_unit'] = $employee_info['unit'];
		} else {	
			$this->data['unit'] = '';
			$this->data['hidden_unit'] = '';
		}

		if (isset($this->request->post['unit_id'])) {
			$this->data['unit_id'] = $this->request->post['unit_id'];
			$this->data['hidden_unit_id'] = $this->request->post['hidden_unit_id'];
		} elseif (!empty($employee_info)) {
			$this->data['unit_id'] = $employee_info['unit_id'];
			$this->data['hidden_unit_id'] = $employee_info['unit_id'];
		} else {	
			$this->data['unit_id'] = '';
			$this->data['hidden_unit_id'] = '';
		}

		if (isset($this->request->post['designation'])) {
			$this->data['designation'] = $this->request->post['designation'];
			$this->data['hidden_designation'] = $this->request->post['hidden_designation'];
		} elseif (!empty($employee_info)) {
			$this->data['designation'] = $employee_info['designation'];
			$this->data['hidden_designation'] = $employee_info['designation'];
		} else {	
			$this->data['designation'] = '';
			$this->data['hidden_designation'] = '';
		}

		if (isset($this->request->post['designation_id'])) {
			$this->data['designation_id'] = $this->request->post['designation_id'];
			$this->data['hidden_designation_id'] = $this->request->post['hidden_designation_id'];
		} elseif (!empty($employee_info)) {
			$this->data['designation_id'] = $employee_info['designation_id'];
			$this->data['hidden_designation_id'] = $employee_info['designation_id'];
		} else {	
			$this->data['designation_id'] = '';
			$this->data['hidden_designation_id'] = '';
		}

		if (isset($this->request->post['company'])) {
			$this->data['company'] = $this->request->post['company'];
			$this->data['hidden_company'] = $this->request->post['hidden_company'];
		} elseif (!empty($employee_info)) {
			$this->data['company'] = $employee_info['company'];
			$this->data['hidden_company'] = $employee_info['company'];
		} else {	
			$this->data['company'] = '';
			$this->data['hidden_company'] = '';
		}

		if (isset($this->request->post['company_id'])) {
			$this->data['company_id'] = $this->request->post['company_id'];
			$this->data['hidden_company_id'] = $this->request->post['hidden_company_id'];
		} elseif (!empty($employee_info)) {
			$this->data['company_id'] = $employee_info['company_id'];
			$this->data['hidden_company_id'] = $employee_info['company_id'];
		} else {	
			$this->data['company_id'] = '';
			$this->data['hidden_company_id'] = '';
		}

		if (isset($this->request->post['state'])) {
			$this->data['state'] = $this->request->post['state'];
			$this->data['hidden_state'] = $this->request->post['hidden_state'];
		} elseif (!empty($employee_info)) {
			$this->data['state'] = $employee_info['state'];
			$this->data['hidden_state'] = $employee_info['state'];
		} else {	
			$this->data['state'] = '';
			$this->data['hidden_state'] = '';
		}

		if (isset($this->request->post['state_id'])) {
			$this->data['state_id'] = $this->request->post['state_id'];
			$this->data['hidden_state_id'] = $this->request->post['hidden_state_id'];
		} elseif (!empty($employee_info)) {
			$this->data['state_id'] = $employee_info['state_id'];
			$this->data['hidden_state_id'] = $employee_info['state_id'];
		} else {	
			$this->data['state_id'] = '';
			$this->data['hidden_state_id'] = '';
		}

		if (isset($this->request->post['employement'])) {
			$this->data['employement'] = $this->request->post['employement'];
			$this->data['hidden_employement'] = $this->request->post['hidden_employement'];
		} elseif (!empty($employee_info)) {
			$this->data['employement'] = $employee_info['employement'];
			$this->data['hidden_employement'] = $employee_info['employement'];
		} else {	
			$this->data['employement'] = '';
			$this->data['hidden_employement'] = '';
		}

		if (isset($this->request->post['employement_id'])) {
			$this->data['employement_id'] = $this->request->post['employement_id'];
			$this->data['hidden_employement_id'] = $this->request->post['hidden_employement_id'];
		} elseif (!empty($employee_info)) {
			$this->data['employement_id'] = $employee_info['employement_id'];
			$this->data['hidden_employement_id'] = $employee_info['employement_id'];
		} else {	
			$this->data['employement_id'] = '';
			$this->data['hidden_employement_id'] = '';
		}

		if (isset($this->request->post['shift_id'])) {
			$this->data['shift_id'] = $this->request->post['shift_id'];
			$this->data['hidden_shift_id'] = $this->request->post['hidden_shift_id'];
		} elseif (!empty($employee_info)) {
			$this->data['shift_id'] = $employee_info['shift_id'];
			$this->data['hidden_shift_id'] = $employee_info['shift_id'];
		} else {	
			$this->data['shift_id'] = '1';
			$this->data['hidden_shift_id'] = '1';
		}

		if (isset($this->request->post['sat_status'])) {
			$this->data['sat_status'] = $this->request->post['sat_status'];
			$this->data['hidden_sat_status'] = $this->request->post['hidden_sat_status'];
		} elseif (!empty($employee_info)) {
			$this->data['sat_status'] = $employee_info['sat_status'];
			$this->data['hidden_sat_status'] = $employee_info['sat_status'];
		} else {	
			$this->data['sat_status'] = '1';
			$this->data['hidden_sat_status'] = '1';
		}

		$shift_datas = $this->db->query("SELECT * FROM `oc_shift` WHERE (`shift_id` = '1' OR `shift_id` = '2') ")->rows;
		$shift_data = array();
		foreach($shift_datas as $skey => $svalue){
			$shift_data[$svalue['shift_id']] = $svalue['name'];
		}
		// echo '<pre>';
		// print_r($shift_data);
		// exit;
		$this->data['shift_data'] = $shift_data;

		if (isset($this->request->post['dob'])) {
			$this->data['dob'] = $this->request->post['dob'];
		} elseif (!empty($employee_info)) {
			if($employee_info['dob'] != '0000-00-00'){
				$this->data['dob'] = date('d-m-Y', strtotime($employee_info['dob']));
			} else {
				$this->data['dob'] = '';
			}
		} else {	
			$this->data['dob'] = '';
		}

		if (isset($this->request->post['doj'])) {
			$this->data['doj'] = $this->request->post['doj'];
			$this->data['hidden_doj'] = $this->request->post['hidden_doj'];
		} elseif (!empty($employee_info)) {
			if($employee_info['doj'] != '0000-00-00'){
				$this->data['doj'] = date('d-m-Y', strtotime($employee_info['doj']));
				$this->data['hidden_doj'] = date('d-m-Y', strtotime($employee_info['doj']));
			} else {
				$this->data['doj'] = '';
				$this->data['hidden_doj'] = '';
			}

		} else {	
			$this->data['doj'] = '';
			$this->data['hidden_doj'] = '';
		}

		if (isset($this->request->post['doc'])) {
			$this->data['doc'] = $this->request->post['doc'];
		} elseif (!empty($employee_info)) {
			if($employee_info['doc'] != '0000-00-00'){
				$this->data['doc'] = date('d-m-Y', strtotime($employee_info['doc']));
			} else {
				$this->data['doc'] = '';
			}
		} else {	
			$this->data['doc'] = '';
		}

		if (isset($this->request->post['dol'])) {
			$this->data['dol'] = $this->request->post['dol'];
			$this->data['hidden_dol'] = $this->request->post['hidden_dol'];
		} elseif (!empty($employee_info)) {
			if($employee_info['dol'] != '0000-00-00'){
				$this->data['dol'] = date('d-m-Y', strtotime($employee_info['dol']));
				$this->data['hidden_dol'] = date('d-m-Y', strtotime($employee_info['dol']));
			} else {
				$this->data['dol'] = '';
				$this->data['hidden_dol'] = '';
			}
		} else {	
			$this->data['dol'] = '';
			$this->data['hidden_dol'] = '';
		}

		// if (isset($this->request->post['shift_name'])) {
		// 	$this->data['shift_name'] = $this->request->post['shift_name'];
		// 	$this->data['shift_id'] = $this->request->post['shift_id'];
		// } elseif (!empty($employee_info)) {
		// 	$today_date = date('j');
		// 	$month = date('n');
		// 	$year = date('Y');
		// 	$shift_data = $this->model_catalog_employee->getshift_id($employee_info['emp_code'], $today_date, $month, $year);
		// 	$shift_ids = explode('_', $shift_data);
		// 	if(isset($shift_ids[1])){
		// 		if($shift_ids[0] == 'S'){
		// 			$shift_id = $shift_ids['1'];
		// 			$shift_name = $this->model_catalog_employee->getshiftdata($shift_id);
		// 		} elseif($shift_ids[0] == 'W') {
		// 			$shift_id = $shift_ids['1'];
		// 			//$shift_name = $this->model_catalog_employee->getweekname($shift_id);
		// 			$shift_name = 'Weekly Off';
		// 		} elseif($shift_ids[0] == 'H'){
		// 			$shift_id = $shift_ids['1'];
		// 			//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
		// 			$shift_name = 'Holiday';
		// 		} elseif($shift_ids[0] == 'HD'){
		// 			$shift_id = $shift_ids['1'];
		// 			//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
		// 			$shift_name = 'Half Day';
		// 		} elseif($shift_ids[0] == 'C'){
		// 			$shift_id = $shift_ids['1'];
		// 			//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
		// 			$shift_name = 'Complimentary Off';
		// 		}
		// 	} else {
		// 		$shift_id = 0;
		// 		$shift_name = '';
		// 	}
		// 	$this->data['shift_id'] = $shift_id;
		// 	$this->data['shift_name'] = $shift_name;
		// } else {	
		// 	$this->data['shift_name'] = '';
		// 	$this->data['shift_id'] = 0;
		// }

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
			$this->data['hidden_status'] = $this->request->post['hidden_status'];
		} elseif (!empty($employee_info)) {
			$this->data['status'] = $employee_info['status'];
			$this->data['hidden_status'] = $employee_info['status'];
		} else {	
			$this->data['status'] = 1;
			$this->data['hidden_status'] = 1;
		}

		if (isset($this->request->post['gender'])) {
			$this->data['gender'] = $this->request->post['gender'];
		} elseif (!empty($employee_info)) {
			$this->data['gender'] = $employee_info['gender'];
		} else {	
			$this->data['gender'] = '';
		}

		$this->data['genders'] = array(
			'M' => 'Male',
			'F' => 'Female'
		);

		if (isset($this->request->post['card_number'])) {
			$this->data['card_number'] = $this->request->post['card_number'];
		} elseif (!empty($employee_info)) {
			$this->data['card_number'] = $employee_info['card_number'];
		} else {	
			$this->data['card_number'] = '';
		}

		if (isset($this->request->post['shift_type'])) {
			$this->data['shift_type'] = $this->request->post['shift_type'];
		} elseif (!empty($employee_info)) {
			$this->data['shift_type'] = $employee_info['shift_type'];
		} else {	
			$this->data['shift_type'] = 'F';
		}

		if (isset($this->request->post['is_dept'])) {
			$this->data['is_dept'] = $this->request->post['is_dept'];
		} elseif (!empty($employee_info)) {
			$this->data['is_dept'] = $employee_info['is_dept'];
		} else {	
			$this->data['is_dept'] = '0';
		}

		if (isset($this->request->post['is_super'])) {
			$this->data['is_super'] = $this->request->post['is_super'];
		} elseif (!empty($employee_info)) {
			$this->data['is_super'] = $employee_info['is_super'];
		} else {	
			$this->data['is_super'] = '0';
		}

		if (isset($this->request->post['password_reset'])) {
			$this->data['password_reset'] = $this->request->post['password_reset'];
		} else {	
			$this->data['password_reset'] = '0';
		}

		if (isset($this->request->post['change_date'])) {
			$this->data['change_date'] = $this->request->post['change_date'];
		} else {	
			$this->data['change_date'] = '';//date('d-m-Y');
		}

		$this->load->model('report/attendance');

		$this->load->model('catalog/designation');
		$data = array(
			'filter_grade' => $this->data['grade_id'],
		);
		$designation_datas = $this->model_catalog_designation->getDesignations();
		$designation_data = array();
		$designation_data['0'] = 'Please Select';
		foreach ($designation_datas as $dkey => $dvalue) {
			$designation_data[$dvalue['designation_id']] = $dvalue['d_name'];
		}
		$this->data['designation_data'] = $designation_data;
		
		$this->load->model('catalog/contractor');
		if($this->data['unit_id'] != '' && $this->data['unit_id'] != '0'){
			$data = array(
				'filter_unit_id' => $this->data['unit_id'],
				'filter_master' => 1,
			);
			$contractor_datas = $this->model_catalog_contractor->getContractors_by_unit($data);
			foreach ($contractor_datas as $ckey => $cvalue) {
				$contractor_data[$cvalue['contractor_id']] = $cvalue['contractor_name'];
			}
		} else {
			$contractor_data = array();
			$contractor_data['0'] = 'Please Select';
			$contractor_datas = $this->model_catalog_contractor->getContractor_tots();
			foreach ($contractor_datas as $ckey => $cvalue) {
				$contractor_data[$cvalue['contractor_id']] = $cvalue['contractor_name'].'-'.$cvalue['contractor_code'];
			}
		}
		
		$this->data['contractor_data'] = $contractor_data;
		
		//category
		$this->load->model('catalog/category');
		$category_datas = $this->model_catalog_category->getcategorys();
		//echo '<pre>';print_r($category_datas);exit;
		$category_data = array();
		$category_data['0'] = 'Please Select';
		foreach ($category_datas as $ckey => $cvalue) {
			$category_data[$cvalue['category_id']] = $cvalue['category_name'];
		}
		$this->data['category_data'] = $category_data;
		
		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'Please Select';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;

		$this->load->model('catalog/state');
		$state_datas = $this->model_catalog_state->getStates();
		$state_data = array();
		$state_data['0'] = 'Please Select';
		foreach ($state_datas as $dkey => $dvalue) {
			$state_data[$dvalue['state_id']] = $dvalue['state'];
		}
		$this->data['state_data'] = $state_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		$company_data['0'] = 'Please Select';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array) && ($dvalue['company_id'] == '1' || $dvalue['company_id'] == '2')){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				// if($dvalue['company_id'] == '1'){
					$company_data[$dvalue['company_id']] = $dvalue['company'];
				// }
			}
		}
		$this->data['company_data'] = $company_data;

		$this->load->model('catalog/grade');
		$grade_datas = $this->model_catalog_grade->getGrades();
		$grade_data = array();
		$grade_data['0'] = 'Please Select';
		foreach ($grade_datas as $dkey => $dvalue) {
			$grade_data[$dvalue['grade_id']] = $dvalue['g_name'];
		}
		$this->data['grade_data'] = $grade_data;

		$grade_z25_ids = $this->db->query("SELECT `grade_id` FROM `oc_grade` WHERE `g_name` = 'Z25' ");
		$grade_z25_id = 0;
		if($grade_z25_ids->num_rows > 0){
			$grade_z25_id = $grade_z25_ids->row['grade_id'];
		}
		$this->data['grade_z25_id'] = $grade_z25_id;

		$this->load->model('catalog/employement');
		$employement_datas = $this->model_catalog_employement->getEmployements();
		$employement_data = array();
		$employement_data['0'] = 'Please Select';
		foreach ($employement_datas as $dkey => $dvalue) {
			$employement_data[$dvalue['employement_id']] = $dvalue['employement'];
		}
		$this->data['employement_data'] = $employement_data;

		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'Please Select';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;


		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $this->data['region_id'],
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'Please Select';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $this->data['division_id'],
			//'filter_state_id' => $this->data['state_id'],
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		$site_data['0'] = 'Please Select';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['site_data'] = $site_data;

		$department_history = array();
		$unit_history = array();
		if(isset($this->request->get['employee_id'])){
			$emp_code = $this->data['emp_code'];
			$department_historys = $this->db->query("SELECT * FROM `oc_employee_change_history` WHERE `emp_code` = '".$emp_code."' AND `type` = 'Department' ");
			foreach($department_historys->rows as $dkey => $dvalue){
				if($dvalue['from_date'] != '0000-00-00'){
					$from_date = date('d-m-Y', strtotime($dvalue['from_date']));
				} else {
					$from_date = '00-00-0000';
				}
				if($dvalue['to_date'] != '0000-00-00'){
					$to_date = date('d-m-Y', strtotime($dvalue['to_date']));
				} else {
					$to_date = '00-00-0000';
				}
				$department_history[] = array(
					'value' => $dvalue['value'],
					'from_date' => $from_date,
					'to_date' => $to_date,
				);
			}

			$unit_historys = $this->db->query("SELECT * FROM `oc_employee_change_history` WHERE `emp_code` = '".$emp_code."' AND `type` = 'Unit' ");
			foreach($unit_historys->rows as $dkey => $dvalue){
				if($dvalue['from_date'] != '0000-00-00'){
					$from_date = date('d-m-Y', strtotime($dvalue['from_date']));
				} else {
					$from_date = '00-00-0000';
				}
				if($dvalue['to_date'] != '0000-00-00'){
					$to_date = date('d-m-Y', strtotime($dvalue['to_date']));
				} else {
					$to_date = '00-00-0000';
				}
				$unit_history[] = array(
					'value' => $dvalue['value'],
					'from_date' => $from_date,
					'to_date' => $to_date,
				);
			}
		}
		$this->data['department_history'] = $department_history;
		$this->data['unit_history'] = $unit_history;

		// if (isset($this->request->post['category_name'])) {
		// 	$this->data['category_id'] = $this->request->post['category_id'];			
		// 	$this->data['category_name'] = $this->request->post['category_name'];
		// } elseif (!empty($employee_info)) {
		// 	$this->data['category_id'] = $employee_info['category'];
		// 	$category_name = $this->model_catalog_employee->getcatname($employee_info['category']);
		// 	$this->data['category_name'] = $category_name;
		// } else {	
		// 	$this->data['category_id'] = '';
		// 	$this->data['category_name'] = 0;
		// }

		// if (isset($this->request->post['department_name'])) {
		// 	$this->data['department_name'] = $this->request->post['department_name'];
		// 	$this->data['department_id'] = $this->request->post['department_id'];
		// } elseif (!empty($employee_info)) {
		// 	$this->data['department_id'] = $employee_info['department'];
		// 	$department_name = $this->model_catalog_employee->getdeptname($employee_info['department']);
		// 	$this->data['department_name'] = $department_name;
		// } else {	
		// 	$this->data['department_name'] = '';
		// 	$this->data['department_id'] = 0;
		// }

		// if (isset($this->request->post['location_name'])) {
		// 	$this->data['location_name'] = $this->request->post['location_name'];
		// 	$this->data['location_id'] = $this->request->post['location_id'];
		// } elseif (!empty($employee_info)) {
		// 	$this->data['location_id'] = $employee_info['location'];
		// 	$location_name = $this->model_catalog_employee->getlocname($employee_info['location']);
		// 	$this->data['location_name'] = $location_name;
		// } else {	
		// 	$this->data['location_name'] = '';
		// 	$this->data['location_id'] = 0;
		// }

		

		if (!isset($this->request->get['employee_id'])) {
			$this->data['action'] = $this->url->link('catalog/employee/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			if(isset($this->request->get['return'])){
				$this->data['action'] = $this->url->link('catalog/employee/update', 'token=' . $this->session->data['token'] . '&employee_id=' . $this->request->get['employee_id'] . '&return=1', 'SSL');
			} else {
				$this->data['action'] = $this->url->link('catalog/employee/update', 'token=' . $this->session->data['token'] . '&employee_id=' . $this->request->get['employee_id'] . $url, 'SSL');
			}
		}

		if(isset($this->request->get['return'])){
			$this->data['cancel'] = $this->url->link('report/employee_data', 'token=' . $this->session->data['token'] . '&h_name=' . $this->data['name'] . '&h_name_id=' . $this->request->get['employee_id'], 'SSL');
		} else {
			$this->data['cancel'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}

		$this->data['weeks'] = array(
			'1' => 'Sunday',
			'2' => 'Monday',
			'3' => 'Tuesday',
			'4' => 'Wednesday',
			'5' => 'Thursday',
			'6' => 'Friday',
			'7' => 'Saturday',
		);
		
		$this->template = 'catalog/employee_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		$this->load->model('catalog/employee');

		// echo '<pre>';
		// print_r($this->request->post);
		// exit;

		if(isset($this->request->get['employee_id'])){
			if (!$this->user->hasPermission('modify', 'catalog/employee')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		} else {
			if (!$this->user->hasPermission('add', 'catalog/employee')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		}

		if(strlen(utf8_decode(trim($this->request->post['name']))) < 1 || strlen(utf8_decode(trim($this->request->post['name']))) > 255){
			$this->error['name'] = $this->language->get('error_name');
		}

		if(strlen(utf8_decode(trim($this->request->post['emp_code']))) < 12 || strlen(utf8_decode(trim($this->request->post['emp_code']))) > 12){
			$this->error['emp_code'] = 'Please Enter Valid Employee Code';
		} else {
			if(isset($this->request->get['employee_id'])){
				$is_exist = $this->db->query("SELECT COUNT(*) as total FROM `oc_employee` WHERE `emp_code` = '".$this->request->post['emp_code']."' AND `emp_code` <> '".$this->request->post['hidden_emp_code']."' ")->row['total'];
				// echo '<pre>';
				// print_r($is_exist);
				// exit;
				if($is_exist){
					$this->error['emp_code'] = 'Employee Code Exists';	
				}
			} else {
				$is_exist = $this->db->query("SELECT COUNT(*) as total FROM `oc_employee` WHERE `emp_code` = '".$this->request->post['emp_code']."' ")->row['total'];
				if($is_exist){
					$this->error['emp_code'] = 'Employee Code Exists';	
				}
			}
		}

		if($this->request->post['doj'] == '' || $this->request->post['doj'] == '00-00-0000'){
			$this->error['doj'] = 'Please Enter Valid Date of Joining';
		}

		if($this->request->post['change_date'] == '' || $this->request->post['change_date'] == '00-00-0000'){
			$this->error['change_date'] = 'Please Enter Valid Change Date';
		}

		if($this->request->post['hidden_status'] == '1' && $this->request->post['status'] == '0'){
			if($this->request->post['dol'] == '' || $this->request->post['dol'] == '00-00-0000'){
				$this->error['dol'] = 'Please Enter Valid Date of Exit';
			}
		}

		if($this->request->post['designation'] == '' || $this->request->post['designation_id'] == '' || $this->request->post['designation_id'] == '0'){
			$this->error['designation'] = 'Please Enter Valid Designation';
		} else {
			$designation_name = html_entity_decode($this->request->post['designation']);
			$designation_name = trim(preg_replace('/\s+/',' ',$designation_name));
			$is_exist = $this->db->query("SELECT `designation_id` FROM `oc_designation` WHERE `d_name` = '".$this->db->escape($designation_name)."' ");
			if($is_exist->num_rows == 0){
				$this->error['designation'] = 'Please Enter Valid Designation';
			}
		}

		if($this->request->post['department'] == '' || $this->request->post['department_id'] == '' || $this->request->post['department_id'] == '0'){
			$this->error['department'] = 'Please Enter Valid Department';
		} else {
			$department_name = html_entity_decode($this->request->post['department']);
			$department_name = trim(preg_replace('/\s+/',' ',$department_name));
			//echo $department_name;exit;
			//echo "SELECT `department_id` FROM `oc_department` WHERE `d_name` = '".$this->db->escape($department_name)."' ";exit;
			$is_exist = $this->db->query("SELECT `department_id` FROM `oc_department` WHERE `d_name` = '".$this->db->escape($department_name)."' ");
			if($is_exist->num_rows == 0){
				$this->error['department'] = 'Please Enter Valid Department';
			}
		}

		if($this->request->post['unit'] == '' || $this->request->post['unit_id'] == '' || $this->request->post['unit_id'] == '0'){
			$this->error['site'] = 'Please Enter Valid Site';
		} else {
			$unit_name = html_entity_decode($this->request->post['unit']);
			$unit_name = trim(preg_replace('/\s+/',' ',$unit_name));
			$is_exist = $this->db->query("SELECT `unit_id` FROM `oc_unit` WHERE `unit` = '".$this->db->escape($unit_name)."' ");
			if($is_exist->num_rows == 0){
				$this->error['site'] = 'Please Enter Valid Unit';
			}
		}

		if($this->request->post['division'] == '' || $this->request->post['division_id'] == '' || $this->request->post['division_id'] == '0'){
			$this->error['division'] = 'Please Enter Valid Division';
		} else {
			$division_name = html_entity_decode($this->request->post['division']);
			$division_name = trim(preg_replace('/\s+/',' ',$division_name));
			$is_exist = $this->db->query("SELECT `division_id` FROM `oc_division` WHERE `division` = '".$this->db->escape($division_name)."' ");
			if($is_exist->num_rows == 0){
				$this->error['division'] = 'Please Enter Valid Division';
			}
		}

		if($this->request->post['state'] == '' || $this->request->post['state_id'] == '' || $this->request->post['state_id'] == '0'){
			//$this->error['state'] = 'Please Enter Valid State';
		} else {
			$state_name = html_entity_decode($this->request->post['state']);
			$state_name = trim(preg_replace('/\s+/',' ',$state_name));
			$is_exist = $this->db->query("SELECT `state_id` FROM `oc_state` WHERE `state` = '".$this->db->escape($state_name)."' ");
			if($is_exist->num_rows == 0){
				//$this->error['state'] = 'Please Enter Valid State';
			}
		}

		if($this->request->post['company'] == '' || $this->request->post['company_id'] == '' || $this->request->post['company_id'] == '0'){
			$this->error['company'] = 'Please Enter Valid Company';
		} else {
			$company_name = html_entity_decode($this->request->post['company']);
			$company_name = trim(preg_replace('/\s+/',' ',$company_name));
			$is_exist = $this->db->query("SELECT `company_id` FROM `oc_company` WHERE `company` = '".$this->db->escape($company_name)."' ");
			if($is_exist->num_rows == 0){
				$this->error['company'] = 'Please Enter Valid Company';
			}
		}

		if($this->request->post['contractor'] == '' || $this->request->post['contractor_id'] == '' || $this->request->post['contractor_id'] == '0'){
			$this->error['contractor'] = 'Please Enter Valid Contractor';
		} else {
			$contractor_name = html_entity_decode($this->request->post['contractor']);
			$contractor_name = trim(preg_replace('/\s+/',' ',$company_name));
			$is_exist = $this->db->query("SELECT `contractor_id` FROM `oc_contractor` WHERE `contractor_id` = '".$this->db->escape($this->request->post['contractor_id'])."' ");
			if($is_exist->num_rows == 0){
				$this->error['contractor'] = 'Please Enter Valid Contractor';
			}
		}

		if($this->request->post['grade'] == '' || $this->request->post['grade_id'] == '' || $this->request->post['grade_id'] == '0'){
			$this->error['grade'] = 'Please Enter Valid Grade';
		} else {
			$grade_name = html_entity_decode($this->request->post['grade']);
			$grade_name = trim(preg_replace('/\s+/',' ',$grade_name));
			$is_exist = $this->db->query("SELECT `grade_id` FROM `oc_grade` WHERE `g_name` = '".$this->db->escape($grade_name)."' ");
			// echo '<pre>';
			// print_r($grade_name);
			// echo '<pre>';
			// print_r($is_exist);
			// exit;
			if($is_exist->num_rows == 0){
				$this->error['grade'] = 'Please Enter Valid Grade';
			}
		}

		if($this->request->post['employement'] == '' || $this->request->post['employement_id'] == '' || $this->request->post['employement_id'] == '0'){
			$this->error['employement'] = 'Please Enter Valid Employement';
		} else {
			$employement_name = html_entity_decode($this->request->post['employement']);
			$employement_name = trim(preg_replace('/\s+/',' ',$employement_name));
			$is_exist = $this->db->query("SELECT `employement_id` FROM `oc_employement` WHERE `employement` = '".$this->db->escape($employement_name)."' ");
			if($is_exist->num_rows == 0){
				$this->error['employement'] = 'Please Enter Valid Employeement';
			}
		}

		if($this->request->post['category'] == '' || $this->request->post['category_id'] == '' || $this->request->post['category_id'] == '0'){
			$this->error['category'] = 'Please Enter Valid Category';
		} else {
			$category_name = html_entity_decode($this->request->post['category']);
			$category_name = trim(preg_replace('/\s+/',' ',$category_name));
			$is_exist = $this->db->query("SELECT `category_id` FROM `oc_category` WHERE `category_name` = '".$this->db->escape($category_name)."' ");
			// echo '<pre>';
			// print_r($is_exist);
			// exit;
			if($is_exist->num_rows == 0){
				$this->error['category'] = 'Please Enter Valid Category';
			}
		}

		/*
		if($this->request->post['region'] == '' || $this->request->post['region_id'] == '' || $this->request->post['region_id'] == '0'){
			$this->error['region'] = 'Please Enter Valid Region';
		} else {
			$region_name = html_entity_decode($this->request->post['region']);
			$region_name = trim(preg_replace('/\s+/',' ',$region_name));
			$is_exist = $this->db->query("SELECT `region_id` FROM `oc_region` WHERE `region` = '".$this->db->escape($region_name)."' ");
			if($is_exist->num_rows == 0){
				$this->error['region'] = 'Please Enter Valid Region';
			}
		}
		*/

		$datas = $this->request->post;

		if(isset($this->request->get['employee_id'])){
			if($datas['region_id'] != $datas['hidden_region_id']){
				if(!isset($this->error['division'])){
					if($datas['division_id'] == $datas['hidden_division_id'] && $datas['hidden_division_id'] != '0'){
						$this->error['division'] = 'Please Change the Division';		
					}
				}
				
				if(!isset($this->error['site'])){
					if($datas['unit_id'] == $datas['hidden_unit_id'] && $datas['hidden_unit_id'] != '0'){
						$this->error['site'] = 'Please Change the Site';		
					}
				}
			}
			if($datas['division_id'] != $datas['hidden_division_id']){
				if(!isset($this->error['site'])){
					if($datas['unit_id'] == $datas['hidden_unit_id']){
						$this->error['site'] = 'Please Change the Site';		
					}
				}
			}
		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/employee')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/employee');

		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $employee_id) {
		// 		$employee_total = $this->model_catalog_employee->getTotaltreatmentByemployeeId($employee_id);

		// 		if ($employee_total) {
		// 			$this->error['warning'] = sprintf($this->language->get('error_employee'), $employee_total);
		// 		}	
		// 	}
		// } elseif(isset($this->request->get['employee_id'])){
		// 	$employee_total = $this->model_catalog_employee->getTotaltreatmentByemployeeId($this->request->get['employee_id']);

		// 	if ($employee_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_employee'), $employee_total);
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}  
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function contautocomplete() {
		$json = array();

		if (isset($this->request->get['filter_contractor'])) {
			$this->load->model('catalog/employee');

			$user_id = $this->user->getId();

			$data = array(
				'filter_contractor' => $this->request->get['filter_contractor'],
				'user_id' => $user_id,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->filter_contractor($data);

			foreach ($results as $result) {
				$json[] = array(
					'contractor_id' => $result['contractor_id'],
					'contractor_code' => $result['contractor_code'],
					'contractor_cname' => strip_tags(html_entity_decode($result['contractor_name'], ENT_QUOTES, 'UTF-8')).' '.$result['contractor_code'],
					'name'            => strip_tags(html_entity_decode($result['contractor_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function getdivision_location() {
		$json = array();
		$division_datas = array();
		$unit_datas = array();
		$device_datas = array();
		if (isset($this->request->get['filter_region_id']) || isset($this->request->get['filter_region_ids'])) {
			$this->load->model('catalog/division');
			$this->load->model('catalog/unit');
			$this->load->model('catalog/device');
			if(isset($this->request->get['filter_region_id'])){
				$data = array(
					'filter_region_id' => $this->request->get['filter_region_id'],
				);
			} else {
				$data = array(
					'filter_region_ids' => $this->request->get['filter_region_ids'],
				);
			}
			$results = $this->model_catalog_division->getDivisions($data);
			$division_datas[] = array(
				'division_id' => '',
				'division' => 'All'
			);
			$unit_datas[] = array(
				'unit_id' => '',
				'unit' => 'All'
			);
			$device_datas[] = array(
				'device_id' => '',
				'device_name' => 'All'
			);
			$division_string = $this->user->getdivision();
			$division_array = array();
			if($division_string != ''){
				$division_array = explode(',', $division_string);
			}
			foreach ($results as $result) {
				if(!empty($division_array)){
					if(in_array($result['division_id'], $division_array)){
						$division_datas[] = array(
							'division_id' => $result['division_id'],
							'division' => strip_tags(html_entity_decode($result['division'], ENT_QUOTES, 'UTF-8'))
						);
					}
				} else {
					$division_datas[] = array(
						'division_id' => $result['division_id'],
						'division' => strip_tags(html_entity_decode($result['division'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
			if($division_datas){
				$data = array();
				foreach($division_datas as $dkey => $dvalue){
					if($dvalue['division_id'] != ''){
						$data = array(
							'filter_division_id' => $dvalue['division_id'],
						);
						if(isset($this->request->get['filter_state_id'])){
							//$data['filter_state_id'] = $this->request->get['filter_state_id'];
						}
						$results = $this->model_catalog_unit->getUnits($data);
						$site_string = $this->user->getsite();
						$site_array = array();
						if($site_string != ''){
							$site_array = explode(',', $site_string);
						}
						foreach ($results as $result) {
							if(!empty($site_array)){
								if(in_array($result['unit_id'], $site_array)){
									$unit_datas[] = array(
										'unit_id' => $result['unit_id'],
										'unit' => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8'))
									);
								}
							} else {
								$unit_datas[] = array(
									'unit_id' => $result['unit_id'],
									'unit' => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8'))
								);
							}
						}
					}
				}
			}
			if($unit_datas){
				foreach($unit_datas as $ukey => $uvalue){
					if($uvalue['unit_id'] != ''){
						$data = array(
							'filter_unit' => $uvalue['unit'],
						);
						$results = $this->model_catalog_device->getDevices($data);
						foreach ($results as $result) {
							$device_datas[] = array(
								'device_id' => $result['device_id'],
								'device_name' => strip_tags(html_entity_decode($result['device_name'], ENT_QUOTES, 'UTF-8'))
							);
						}	
					}
				}
			}		
		}
		$json['division_datas'] = $division_datas;
		$json['unit_datas'] = $unit_datas;
		$json['device_datas'] = $device_datas;
		// $sort_order = array();
		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['name'];
		// }
		//array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function getlocation() {
		$json = array();
		$division_datas = array();
		$unit_datas = array();
		$device_datas = array();
		if (isset($this->request->get['filter_division_id']) || isset($this->request->get['filter_division_ids'])) {
			$this->load->model('catalog/unit');
			$this->load->model('catalog/device');
			if(isset($this->request->get['filter_division_id'])){
				$data = array(
					'filter_division_id' => $this->request->get['filter_division_id'],
				);
			} else {
				$data = array(
					'filter_division_ids' => $this->request->get['filter_division_ids'],
				);
			}
			if(isset($this->request->get['filter_state_id'])){
				//$data['filter_state_id'] = $this->request->get['filter_state_id'];
			}
			$results = $this->model_catalog_unit->getUnits($data);
			$unit_datas[] = array(
				'unit_id' => '',
				'unit' => 'All'
			);
			$device_datas[] = array(
				'device_id' => '',
				'device_name' => 'All'
			);
			$site_string = $this->user->getsite();
			$site_array = array();
			if($site_string != ''){
				$site_array = explode(',', $site_string);
			}
			foreach ($results as $result) {
				if(!empty($site_array)){
					if(in_array($result['unit_id'], $site_array)){
						$unit_datas[] = array(
							'unit_id' => $result['unit_id'],
							'unit' => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8'))
						);
					}
				} else {
					$unit_datas[] = array(
						'unit_id' => $result['unit_id'],
						'unit' => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
			if($unit_datas){
				foreach($unit_datas as $ukey => $uvalue){
					if($uvalue['unit_id'] != ''){
						$data = array(
							'filter_unit' => $uvalue['unit'],
						);
						$results = $this->model_catalog_device->getDevices($data);
						foreach ($results as $result) {
							$device_datas[] = array(
								'device_id' => $result['device_id'],
								'device_name' => strip_tags(html_entity_decode($result['device_name'], ENT_QUOTES, 'UTF-8'))
							);
						}	
					}
				}
			}
		}
		$json['unit_datas'] = $unit_datas;
		$json['device_datas'] = $device_datas;
		// $sort_order = array();
		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['name'];
		// }
		//array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function getlocation_master() {
		$json = array();
		$division_datas = array();
		$unit_datas = array();
		$device_datas = array();
		if (isset($this->request->get['filter_division_id']) || isset($this->request->get['filter_division_ids'])) {
			$this->load->model('catalog/unit');
			$this->load->model('catalog/device');
			if(isset($this->request->get['filter_division_id'])){
				$data = array(
					'filter_division_id' => $this->request->get['filter_division_id'],
				);
			} else {
				$data = array(
					'filter_division_ids' => $this->request->get['filter_division_ids'],
				);
			}
			if(isset($this->request->get['filter_state_id'])){
				//$data['filter_state_id'] = $this->request->get['filter_state_id'];
			}
			$results = $this->model_catalog_unit->getUnits($data);
			$unit_datas[] = array(
				'unit_id' => '',
				'unit' => 'Please Select'
			);
			$device_datas[] = array(
				'device_id' => '',
				'device_name' => 'Please Select'
			);
			$site_string = $this->user->getsite();
			$site_array = array();
			if($site_string != ''){
				$site_array = explode(',', $site_string);
			}
			foreach ($results as $result) {
				if(!empty($site_array)){
					if(in_array($result['unit_id'], $site_array)){
						$unit_datas[] = array(
							'unit_id' => $result['unit_id'],
							'unit' => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8'))
						);
					}
				} else {
					$unit_datas[] = array(
						'unit_id' => $result['unit_id'],
						'unit' => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
			if($unit_datas){
				foreach($unit_datas as $ukey => $uvalue){
					if($uvalue['unit_id'] != ''){
						$data = array(
							'filter_unit' => $uvalue['unit'],
						);
						$results = $this->model_catalog_device->getDevices($data);
						foreach ($results as $result) {
							$device_datas[] = array(
								'device_id' => $result['device_id'],
								'device_name' => strip_tags(html_entity_decode($result['device_name'], ENT_QUOTES, 'UTF-8'))
							);
						}	
					}
				}
			}
		}
		$json['unit_datas'] = $unit_datas;
		$json['device_datas'] = $device_datas;
		// $sort_order = array();
		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['name'];
		// }
		//array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function getcontractors_by_unit() {
		$json = array();
		$contractor_datas = array();
		if (isset($this->request->get['filter_unit_id'])) {
			$this->load->model('catalog/contractor');
			$data = array(
				'filter_unit_id' => $this->request->get['filter_unit_id'],
				'filter_master' => 0,
			);
			$contractor_datas = $this->model_catalog_contractor->getContractors_by_unit($data);
		}
		$json['contractor_datas'] = $contractor_datas;
		$this->response->setOutput(json_encode($json));
	}

	public function getcontractors_by_unit_master() {
		$json = array();
		$contractor_datas = array();
		if (isset($this->request->get['filter_unit_id'])) {
			$this->load->model('catalog/contractor');
			$data = array(
				'filter_unit_id' => $this->request->get['filter_unit_id'],
				'filter_master' => 1,
			);
			$contractor_datas = $this->model_catalog_contractor->getContractors_by_unit($data);
			
		}
		$json['contractor_datas'] = $contractor_datas;
		$this->response->setOutput(json_encode($json));
	}

	public function getcontractors_by_unit_report() {
		$json = array();
		$contractor_datas = array();
		if (isset($this->request->get['filter_unit_id'])) {
			$this->load->model('catalog/contractor');
			$data = array(
				'filter_unit_id' => $this->request->get['filter_unit_id'],
			);
			$contractor_datas = $this->model_catalog_contractor->getContractors_by_unit($data);
		}
		$json['contractor_datas'] = $contractor_datas;
		$this->response->setOutput(json_encode($json));
	}

	public function getcontractor_name() {
		$json = array();
		$contractor_name = '';
		if (isset($this->request->get['filter_contractor_id'])) {
			$this->load->model('catalog/contractor');
			$data['filter_contractor_id'] = $this->request->get['filter_contractor_id'];
			$results = $this->model_catalog_contractor->getContractor($data['filter_contractor_id']);
			if(isset($results['contractor_id'])){
				$contractor_name = $results['contractor_name'];
			}
		}
		$json['contractor_name'] = $contractor_name;
		$this->response->setOutput(json_encode($json));
	}

	public function getdevices() {
		$json = array();
		$device_datas = array();
		if (isset($this->request->get['filter_unit'])) {
			$this->load->model('catalog/device');
			$filter_unit_exp = explode(',', $this->request->get['filter_unit']);
			$site_name_string = '';
			foreach($filter_unit_exp as $key => $value){
				$site_names = $this->db->query("SELECT `unit` FROM `oc_unit` WHERE `unit_id` = '".$value."' ");
				if($site_names->num_rows > 0){
					$site_name_string .= $site_names->row['unit'].',';
				}
			}
			$site_name_string = rtrim($site_name_string, ',');
			$data = array(
				'filter_units' => $site_name_string,
			);
			$results = $this->model_catalog_device->getDevices($data);
			$device_datas[] = array(
				'device_id' => '',
				'device_name' => 'All'
			);
			foreach ($results as $result) {
				$device_datas[] = array(
					'device_id' => $result['device_id'],
					'device_name' => strip_tags(html_entity_decode($result['device_name'], ENT_QUOTES, 'UTF-8'))
				);
			}	
		}
		$json['device_datas'] = $device_datas;
		$this->response->setOutput(json_encode($json));
	}

	public function getdesignation() {
		$json = array();
		$designation_datas = array();
		if (isset($this->request->get['filter_grade_id'])) {
			$this->load->model('catalog/designation');
			$data = array(
				'filter_grade' => $this->request->get['filter_grade_id'],
			);
			$results = $this->model_catalog_designation->getDesignations($data);
			$designation_datas[] = array(
				'designation_id' => '',
				'designation' => 'Please Select'
			);
			foreach ($results as $result) {
				$designation_datas[] = array(
					'designation_id' => $result['designation_id'],
					'designation' => strip_tags(html_entity_decode($result['d_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}
		$json['designation_datas'] = $designation_datas;
		$this->response->setOutput(json_encode($json));
	}

	public function getstate() {
		$json = array();
		$state_datas = array();
		if (isset($this->request->get['filter_unit_id'])) {
			$this->load->model('catalog/unit');
			$data['filter_name_id'] = $this->request->get['filter_unit_id'];
			$results = $this->model_catalog_unit->getUnits($data);
			foreach ($results as $result) {
				$state_datas[] = array(
					'unit_id' => $result['unit_id'],
					'state_id' => $result['state_id'],
					'state' => strip_tags(html_entity_decode($result['state_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}
		$json['state_datas'] = $state_datas;
		$this->response->setOutput(json_encode($json));
	}

	public function getgrade() {
		$json = array();
		$grade_datas = array();
		if (isset($this->request->get['filter_designation_id'])) {
			$this->load->model('catalog/designation');
			$data['filter_name_id'] = $this->request->get['filter_designation_id'];
			$results = $this->model_catalog_designation->getDesignations($data);
			foreach ($results as $result) {
				$grade_datas[] = array(
					'designation_id' => $result['designation_id'],
					'grade_id' => $result['grade_id'],
					'grade' => strip_tags(html_entity_decode($result['grade_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}
		$json['grade_datas'] = $grade_datas;
		$this->response->setOutput(json_encode($json));
	}

	public function getstate_locaton() {
		$json = array();
		$unit_datas = array();
		$device_datas = array();
		if (isset($this->request->get['filter_state_id'])) {
			$this->load->model('catalog/unit');
			$this->load->model('catalog/device');
			$data['filter_state_id'] = $this->request->get['filter_state_id'];
			$data['filter_division_id'] = $this->request->get['filter_division_id'];
			$results = $this->model_catalog_unit->getUnits($data);
			$unit_datas[] = array(
				'unit_id' => '',
				'unit' => 'Please Select'
			);
			$device_datas[] = array(
				'device_id' => '',
				'device_name' => 'Please Select'
			);
			$site_string = $this->user->getsite();
			$site_array = array();
			if($site_string != ''){
				$site_array = explode(',', $site_string);
			}
			foreach ($results as $result) {
				if(!empty($site_array)){
					if(in_array($result['unit_id'], $site_array)){
						$unit_datas[] = array(
							'unit_id' => $result['unit_id'],
							'unit' => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8'))
						);
					}
				} else {
					$unit_datas[] = array(
						'unit_id' => $result['unit_id'],
						'unit' => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
			if($unit_datas){
				foreach($unit_datas as $ukey => $uvalue){
					if($uvalue['unit_id'] != ''){
						$data = array(
							'filter_unit' => $uvalue['unit'],
						);
						$results = $this->model_catalog_device->getDevices($data);
						foreach ($results as $result) {
							$device_datas[] = array(
								'device_id' => $result['device_id'],
								'device_name' => strip_tags(html_entity_decode($result['device_name'], ENT_QUOTES, 'UTF-8'))
							);
						}	
					}
				}
			}
		}
-		$json['unit_datas'] = $unit_datas;
		$json['device_datas'] = $device_datas;
		$this->response->setOutput(json_encode($json));
	}

	public function imports(){
		//echo '<pre>';
		//print_r($this->request->files);
		//exit;
		$valid = 0;
		if(isset($this->request->files['import_data']['name'])){
			$file_name = $this->request->files['import_data']['name'];
			$file_name_exp = explode('.', $file_name);
			if(isset($file_name_exp[1]) && $file_name_exp[1] == 'csv'){
				$valid = 1;
			}
		}
		//echo $valid;exit;
		if(isset($this->request->files['import_data']['tmp_name']) && $this->request->files['import_data']['tmp_name'] != '' && $valid == 1){
			$file=fopen($this->request->files['import_data']['tmp_name'],"r");
			$i=1;
			$employement_data = array();
			$grade_data = array();
			$designtion_data = array();
			$department_data = array();
			$site_data = array();
			$division_data = array();
			$region_data = array();
			$state_data = array();
			$company_data = array();
			// $this->db->query("TRUNCATE TABLE `oc_employee`");
			// $this->db->query("TRUNCATE TABLE `oc_employement`");
			// $this->db->query("TRUNCATE TABLE `oc_grade`");
			// $this->db->query("TRUNCATE TABLE `oc_designation`");
			// $this->db->query("TRUNCATE TABLE `oc_department`");
			// $this->db->query("TRUNCATE TABLE `oc_unit`");
			// $this->db->query("TRUNCATE TABLE `oc_division`");
			// $this->db->query("TRUNCATE TABLE `oc_region`");
			// $this->db->query("TRUNCATE TABLE `oc_state`");
			// $this->db->query("TRUNCATE TABLE `oc_company`");
			// $this->db->query("TRUNCATE TABLE `oc_shift_schedule`");
			// $this->db->query("TRUNCATE TABLE `oc_shift_unit`");
			// $this->db->query("TRUNCATE TABLE `oc_leave`");
			while(($var=fgetcsv($file,1000,","))!==FALSE){
				if($i != 1) {
					// echo '<pre>';
					// print_r($var);
					// exit;
					if($var[6] != ''){
						if(!isset($employement_data[$var[6]])){
							$employement_data[$var[6]] = html_entity_decode(strtolower(trim($var[6])));
						}
					}
					if($var[7] != ''){
						if(!isset($grade_data[$var[7]])){
							$grade_data[$var[7]] = html_entity_decode(strtolower(trim($var[7])));
						}
					}
					if($var[9] != ''){
						if(!isset($designtion_data[$var[9]])){
							$designtion_data[$var[9]] = html_entity_decode(strtolower(trim($var[9])));
						}
					}
					if($var[10] != ''){
						if(!isset($department_data[$var[10]])){
							$department_data[$var[10]] = html_entity_decode(strtolower(trim($var[10])));
						}
					}
					if($var[11] != ''){
						if(!isset($site_data[$var[11]])){
							$site_data[$var[11]] = html_entity_decode(strtolower(trim($var[11])));
						}
					}
					if($var[12] != ''){
						if(!isset($division_data[$var[12]])){
							$division_data[$var[12]] = html_entity_decode(strtolower(trim($var[12])));
						}
					}
					if($var[13] != ''){
						if(!isset($region_data[$var[13]])){
							$region_data[$var[13]] = html_entity_decode(strtolower(trim($var[13])));
						}
					}
					if($var[14] != ''){
						if(!isset($state_data[$var[14]])){
							$state_data[$var[14]] = html_entity_decode(strtolower(trim($var[14])));
						}
					}
					if($var[17] != ''){
						if(!isset($company_data[$var[17]])){
							$company_data[$var[17]] = html_entity_decode(strtolower(trim($var[17])));
						}
					}
				}
				$i ++;
			}
			fclose($file);
			
			$employement_data_linked = array();
			foreach($employement_data as $dkey => $dvalue){
				if($dkey == 'P' || $dkey == 'PERMANENT'){
					$dkey = 'PERMANENT';
				} elseif($dkey == 'C' || $dkey == 'CONTRACT'){
					$dkey = 'CONTRACT';
				} elseif($dkey == 'T' || $dkey == 'TRAINEE'){
					$dkey = 'TRAINEE';
				} else {
					$dkey = '';
				}
				$is_exist = $this->db->query("SELECT `employement_id` FROM `oc_employement` WHERE `employement` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_employement` SET `employement` = '".$this->db->escape($dkey)."' ";
					$this->db->query($sql);
					$employement_id = $this->db->getLastId();
					//$employement_id = 0;
				} else {
					$employement_id = $is_exist->row['employement_id'];
				}	
				$employement_data_linked[$dkey] = $employement_id;
			}

			$grade_data_linked = array();
			foreach($grade_data as $dkey => $dvalue){
				$is_exist = $this->db->query("SELECT `grade_id` FROM `oc_grade` WHERE `g_code` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_grade` SET `g_name` = '".$this->db->escape($dkey)."', `g_code` = '".$this->db->escape($dkey)."' ";
					$this->db->query($sql);
					$grade_id = $this->db->getLastId();
					//$grade_id = 0;
				} else {
					$grade_id = $is_exist->row['grade_id'];
				}
				$grade_data_linked[$dvalue] = $grade_id;
			}

			$designation_data_linked = array();
			foreach($designtion_data as $dkey => $dvalue){
				$is_exist = $this->db->query("SELECT `designation_id` FROM `oc_designation` WHERE `d_name` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_designation` SET `d_name` = '".$this->db->escape($dkey)."', `status` = '1' ";
					$this->db->query($sql);
					$designation_id = $this->db->getLastId();
					//$designation_id = 0;
				} else {
					$designation_id = $is_exist->row['designation_id'];
				}
				$designation_data_linked[$dvalue] = $designation_id;
			}

			$department_data_linked = array();
			foreach($department_data as $dkey => $dvalue){
				$is_exist = $this->db->query("SELECT `department_id` FROM `oc_department` WHERE `d_name` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_department` SET `d_name` = '".$this->db->escape($dkey)."', `status` = '1' ";
					$this->db->query($sql);
					$department_id = $this->db->getLastId();
					//$department_id = 0;
				} else {
					$department_id = $is_exist->row['department_id'];
				}
				$department_data_linked[$dvalue] = $department_id;
			}

			$site_data_linked = array();
			foreach($site_data as $dkey => $dvalue){
				$is_exist = $this->db->query("SELECT `unit_id` FROM `oc_unit` WHERE `unit` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_unit` SET `unit` = '".$this->db->escape($dkey)."' ";
					$this->db->query($sql);
					$site_id = $this->db->getLastId();
					//$site_id = 0;
				} else {
					$site_id = $is_exist->row['unit_id'];
				}
				$site_data_linked[$dvalue] = $site_id;
			}

			$division_data_linked = array();
			foreach($division_data as $dkey => $dvalue){
				$is_exist = $this->db->query("SELECT `division_id` FROM `oc_division` WHERE `division` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_division` SET `division` = '".$this->db->escape($dkey)."' ";
					$this->db->query($sql);
					$division_id = $this->db->getLastId();
					//$division_id = 0;
				} else {
					$division_id = $is_exist->row['division_id'];
				}
				$division_data_linked[$dvalue] = $division_id;
			}

			$region_data_linked = array();
			foreach($region_data as $dkey => $dvalue){
				$is_exist = $this->db->query("SELECT `region_id` FROM `oc_region` WHERE `region` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_region` SET `region` = '".$this->db->escape($dkey)."' ";
					$this->db->query($sql);
					$region_id = $this->db->getLastId();
					//$region_id = 0;
				} else {
					$region_id = $is_exist->row['region_id'];
				}
				$region_data_linked[$dvalue] = $region_id;
			}

			$state_data_linked = array();
			foreach($state_data as $dkey => $dvalue){
				$is_exist = $this->db->query("SELECT `state_id` FROM `oc_state` WHERE `state` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_state` SET `state` = '".$this->db->escape($dkey)."' ";
					$this->db->query($sql);
					$state_id = $this->db->getLastId();
					//$state_id = 0;
				} else {
					$state_id = $is_exist->row['state_id'];
				}
				$state_data_linked[$dvalue] = $state_id;
			}

			$company_data_linked = array();
			foreach($company_data as $dkey => $dvalue){
				$is_exist = $this->db->query("SELECT `company_id` FROM `oc_company` WHERE `company` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_company` SET `company` = '".$this->db->escape($dkey)."' ";
					$this->db->query($sql);
					$company_id = $this->db->getLastId();
					//$company_id = 0;
				} else {
					$company_id = $is_exist->row['company_id'];
				}
				$company_data_linked[$dvalue] = $company_id;
			}

			$file=fopen($this->request->files['import_data']['tmp_name'],"r");
			$i=1;
			$months_array = array(
				'1' => '1',
				'2' => '2',
				'3' => '3',
				'4' => '4',
				'5' => '5',
				'6' => '6',
				'7' => '7',
				'8' => '8',
				'9' => '9',
				'10' => '10',
				'11' => '11',
				'12' => '12',
			);
			$shortmonthsarray = array(
				'Jan' => '1',
				'Feb' => '2',
				'Mar' => '3',
				'Apr' => '4',
				'May' => '5',
				'Jun' => '6',
				'Jul' => '7',
				'Aug' => '8',
				'Sep' => '9',
				'Oct' => '10',
				'Nov' => '11',
				'Dec' => '12',	
			);
			$final_employee_error_data = array();
			$employee_error_data = array();
			while(($var=fgetcsv($file,1000,","))!==FALSE){
				if($i != 1) {
					$employee_error_data = array();
					$var0=addslashes($var[0]);//emp_code
					if($var0 != '' && !isset($emp_code_exist[$var0])){
						$var1 = html_entity_decode(trim($var[1]));//name
						if($var1 == ''){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Employee Name Blank'
							);
						}
						$var2=addslashes($var[2]);//gender
						// if($var22 == 'Male'){
						// 	$var2 = 'M';
						// } elseif($var22 == 'Female'){
						// 	$var2 = 'F';
						// }
						$var33=addslashes($var[3]);//dob
						if($var33 != ''){
							$var3 = Date('Y-m-d', strtotime($var33));//dob
						} else {
							$var3 = '0000-00-00';
						}

						$var44=addslashes($var[4]);//doj
						if($var44 != ''){
							$var4 = Date('Y-m-d', strtotime($var44));//doj
						} else {
							$var4 = '0000-00-00';
						}

						$var55=addslashes($var[5]);//doc
						if($var55 != ''){
							$var5 = Date('Y-m-d', strtotime($var55));//doc
						} else {
							$var5 = '0000-00-00';
						}

						$var88=addslashes($var[8]);//dol
						if($var88 != ''){
							$var8 = Date('Y-m-d', strtotime($var88));//dol
						} else {
							$var8 = '0000-00-00';
						}

						/*
						$var33=addslashes($var[3]);//dob
						if($var33 != ''){
							$var3_exp = explode('-', $var33);
							$day = $var3_exp[0];
							$month = $shortmonthsarray[$var3_exp[1]];
							if($var3_exp[2] > 0 && $var3_exp[2] <= 17){
								$year = '20'.$var3_exp[2];
							} else {
								$year = '19'.$var3_exp[2];	
							}
							$var3 = $year.'-'.$month.'-'.$day;
							$var3 = Date('Y-m-d', strtotime($var3));//dob
						} else {
							$var3 = '0000-00-00';
						}
						
						$var44=addslashes($var[4]);//doj
						if($var44 != ''){
							$var4_exp = explode('-', $var44);
							$day = $var4_exp[0];
							$month = $shortmonthsarray[$var4_exp[1]];
							if($var4_exp[2] > 0 && $var4_exp[2] <= 17){
								$year = '20'.$var4_exp[2];
							} else {
								$year = '19'.$var4_exp[2];	
							}
							$var4 = $year.'-'.$month.'-'.$day;
							$var4 = Date('Y-m-d', strtotime($var4));//doj
						} else {
							$var4 = '0000-00-00';
						}

						$var55=addslashes($var[5]);//doc
						if($var55 != ''){
							$var5_exp = explode('-', $var55);
							$day = $var5_exp[0];
							$month = $shortmonthsarray[$var5_exp[1]];
							if($var5_exp[2] > 0 && $var5_exp[2] <= 17){
								$year = '20'.$var5_exp[2];
							} else {
								$year = '19'.$var5_exp[2];	
							}
							$var5 = $year.'-'.$month.'-'.$day;
							$var5 = Date('Y-m-d', strtotime($var5));//doc
						} else {
							$var5 = '0000-00-00';
						}
						
						$var88=addslashes($var[8]);//dol
						if($var88 != ''){
							$var8_exp = explode('-', $var88);
							$day = $var8_exp[0];
							$month = $shortmonthsarray[$var8_exp[1]];
							if($var8_exp[2] > 0 && $var8_exp[2] <= 17){
								$year = '20'.$var8_exp[2];
							} else {
								$year = '19'.$var8_exp[2];	
							}
							$var8 = $year.'-'.$month.'-'.$day;
							$var8 = Date('Y-m-d', strtotime($var8));//dol
						} else {
							$var8 = '0000-00-00';
						}
						*/

						if($var4 == '0000-00-00'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Date Of Joining Blank'
							);
						}
						
						$var61=html_entity_decode(addslashes($var[6]));//employement
						if($var61 == 'P' || $var61 == 'PERMANENT'){
							$var61 = 'PERMANENT';
						} elseif($var61 == 'C' || $var61 == 'CONTRACT'){
							$var61 = 'CONTRACT';
						} elseif($var61 == 'T' || $var61 == 'TRAINEE'){
							$var61 = 'TRAINEE';
						} else {
							$var61 = $var[6];
						}
						$var6=html_entity_decode(strtolower(trim($var[6])));//employement
						if($var6 != ''){
							if($var6 == 'p' || $var6 == 'permanent'){
								$var6 = 'PERMANENT';
							} elseif($var6 == 'c' || $var6 == 'contract'){
								$var6 = 'CONTRACT';
							} elseif($var6 == 't' || $var6 == 'trainee'){
								$var6 = 'TRAINEE';
							} else {
								$var6 = '';
							}
							$var6=$employement_data_linked[$var6];
						}
						if($var6 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Employement Type '.$var61.' Does Not Exist',
							);
						}

						$var171=html_entity_decode(addslashes($var[17]));//company
						$var17=html_entity_decode(strtolower(trim($var[17])));//company
						if($var17 != ''){
							$var17=$company_data_linked[$var17];
						}
						if($var17 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Company '.$var171.' Does Not Exist',
							);
						}

						if($var17 == '1'){
							$var71=html_entity_decode(addslashes($var[7]));//grade
							$var7=html_entity_decode(strtolower(trim($var[7])));//grade
							if($var7 != ''){
								$var7=$grade_data_linked[$var7];
							}
							if($var7 == '0'){
								$employee_error_data[] = array(
									'line_number' => $i,
									'reason' => 'Grade '.$var71.' Does Not Exist',
								);
							}
						} else {
							$var71 = 'Z25';
							$var7 = '36';
						}
						
						$var91=html_entity_decode(addslashes($var[9]));//designation
						$var9=html_entity_decode(strtolower(trim($var[9])));//designation
						if($var9 != ''){
							$var9=$designation_data_linked[$var9];
						}
						if($var9 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Designation '.$var91.' Does Not Exist',
							);
						}
						$var101=html_entity_decode(addslashes($var[10]));//department
						$var10=html_entity_decode(strtolower(trim($var[10])));//department
						if($var10 != ''){
							$var10=$department_data_linked[$var10];
						}
						if($var10 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Department '.$var101.' Does Not Exist',
							);
						}
						$var111=html_entity_decode(addslashes($var[11]));//site
						$var11=html_entity_decode(strtolower(trim($var[11])));//site
						if($var11 != ''){
							$var11=$site_data_linked[$var11];
						}
						if($var11 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Site '.$var111.' Does Not Exist',
							);
						}
						$var121=html_entity_decode(addslashes($var[12]));//division
						$var12=html_entity_decode(strtolower(trim($var[12])));//division
						if($var12 != ''){
							$var12=$division_data_linked[$var12];
						}
						if($var12 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Division '.$var121.' Does Not Exist',
							);
						}
						$var131=html_entity_decode(addslashes($var[13]));//region
						$var13=html_entity_decode(strtolower(trim($var[13])));//region
						if($var13 != ''){
							$var13=$region_data_linked[$var13];
						}
						if($var13 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Region '.$var131.' Does Not Exist',
							);
						}
						$var141=html_entity_decode(addslashes($var[14]));//state
						$var14=html_entity_decode(strtolower(trim($var[14])));//state
						if($var14 != ''){
							$var14=$state_data_linked[$var14];
						}
						if($var14 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'State '.$var141.' Does Not Exist',
							);
						}
						$var16=addslashes($var[16]);//status
						if($var16 == 'Active'){
							$var16 = '1';
						} else {
							$var16 = '0';
						}
						
						$emp_code = $var0;
						$user_name = $emp_code;
						$salt = substr(md5(uniqid(rand(), true)), 0, 9);
						$password = sha1($salt . sha1($salt . sha1($emp_code)));
						
						$var151=addslashes($var[15]);//shift_id
						$var15 = '0';
						if($var151 == '09:30:00 - 18:30:00'){
							$var15 = '1';
						} elseif($var151 == '08:00:00 - 20:00:00') {
							$var15 = '2';
						}

						if($var15 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Shift '.$var151.' Does Not Exist',
							);	
						}

						$var18=addslashes($var[18]);//weekly_off
						if($var18 == '1'){
							$sun_status = '0';
							$sat_status = '1';
						} else {
							$sun_status = '0';
							$sat_status = '0';
						}

						// echo '<pre>';
						// print_r($employee_error_data);
						// exit;

						if(empty($employee_error_data)){
							$is_exist = $this->db->query("SELECT `emp_code`, `shift_id`, `unit_id`, `sat_status` FROM `oc_employee` WHERE `emp_code` = '".$var0."' ");
							if($is_exist->num_rows == 0){
								$db_shift_id = 0;
								$db_unit_id = 0;
								$db_sat_status = 0;
								$insert = "INSERT INTO `oc_employee` SET 
											`emp_code` = '".$var0."', 
											`name` = '".$this->db->escape($var1)."', 
											`gender` = '".$var2."', 
											`dob` = '".$var3."', 
											`doj` = '".$var4."', 
											`doc` = '".$var5."', 
											`employement` = '".$var61."',
											`employement_id` = '".$var6."',
											`grade` = '".$var71."',
											`grade_id` = '".$var7."',
											`dol` = '".$var8."', 
											`designation` = '".$var91."', 
											`designation_id` = '".$var9."', 
											`department` = '".$var101."',
											`department_id` = '".$var10."',
											`unit` = '".$var111."',
											`unit_id` = '".$var11."',
											`division` = '".$var121."',
											`division_id` = '".$var12."',
											`region` = '".$var131."',
											`region_id` = '".$var13."',
											`state` = '".$var141."',
											`state_id` = '".$var14."',
											`status` = '".$var16."', 
											`company` = '".$var171."',
											`company_id` = '".$var17."',
											`shift_type` = 'F',
											`user_group_id` = '11',
											`username` = '".$user_name."', 
											`password` = '".$password."', 
											`shift_id` = '".$var15."', 
											`sat_status` = '".$sat_status."', 
											`sun_status` = '".$sun_status."', 
											`salt` = '".$salt."' "; 
								
								$this->db->query($insert);
								// echo $insert;
								// echo '<br />';
								// exit;
							} else {
								$db_emp_data = $is_exist->row;
								$db_shift_id = $db_emp_data['shift_id'];
								$db_unit_id = $db_emp_data['unit_id'];
								$db_sat_status = $db_emp_data['sat_status'];
								$update = "UPDATE `oc_employee` SET 
											`emp_code` = '".$var0."', 
											`name` = '".$this->db->escape($var1)."', 
											`gender` = '".$var2."', 
											`dob` = '".$var3."', 
											`doj` = '".$var4."', 
											`doc` = '".$var5."', 
											`employement` = '".$var61."',
											`employement_id` = '".$var6."',
											`grade` = '".$var71."',
											`grade_id` = '".$var7."',
											`dol` = '".$var8."', 
											`designation` = '".$var91."', 
											`designation_id` = '".$var9."', 
											`department` = '".$var101."',
											`department_id` = '".$var10."',
											`unit` = '".$var111."',
											`unit_id` = '".$var11."',
											`division` = '".$var121."',
											`division_id` = '".$var12."',
											`region` = '".$var131."',
											`region_id` = '".$var13."',
											`state` = '".$var141."',
											`state_id` = '".$var14."',
											`status` = '".$var16."', 
											`shift_id` = '".$var15."', 
											`sat_status` = '".$sat_status."', 
											`sun_status` = '".$sun_status."', 
											`company` = '".$var171."',
											`company_id` = '".$var17."'
											 WHERE `emp_code` = '".$var0."' "; 
								$this->db->query($update);
								// echo $insert;
								// echo '<br />';
								// exit;
							}
							$shift_id_data = 'S_'.$var15;
							foreach ($months_array as $key => $value) {
								$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$var0."' AND `month` = '".$key."' AND `year` = '".date('Y')."' ");
								if($is_exist->num_rows == 0){
									$insert1 = "INSERT INTO `oc_shift_schedule` SET 
												`emp_code` = '".$var0."',
												`1` = '".$shift_id_data."',
												`2` = '".$shift_id_data."',
												`3` = '".$shift_id_data."',
												`4` = '".$shift_id_data."',
												`5` = '".$shift_id_data."',
												`6` = '".$shift_id_data."',
												`7` = '".$shift_id_data."',
												`8` = '".$shift_id_data."',
												`9` = '".$shift_id_data."',
												`10` = '".$shift_id_data."',
												`11` = '".$shift_id_data."',
												`12` = '".$shift_id_data."', 
												`13` = '".$shift_id_data."', 
												`14` = '".$shift_id_data."', 
												`15` = '".$shift_id_data."', 
												`16` = '".$shift_id_data."', 
												`17` = '".$shift_id_data."', 
												`18` = '".$shift_id_data."', 
												`19` = '".$shift_id_data."', 
												`20` = '".$shift_id_data."', 
												`21` = '".$shift_id_data."', 
												`22` = '".$shift_id_data."', 
												`23` = '".$shift_id_data."', 
												`24` = '".$shift_id_data."', 
												`25` = '".$shift_id_data."', 
												`26` = '".$shift_id_data."', 
												`27` = '".$shift_id_data."', 
												`28` = '".$shift_id_data."', 
												`29` = '".$shift_id_data."', 
												`30` = '".$shift_id_data."', 
												`31` = '".$shift_id_data."',
												`month` = '".$key."',
												`year` = '".date('Y')."',
												`status` = '1' ,
												`unit` = '".$var111."',
												`unit_id` = '".$var11."' ";
									$this->db->query($insert1);
									
									$year = date('Y');
									if($key == 12){
										$mod_key = 1;
										$mod_year = $year + 1;
									} else {
										$mod_key = $key + 1;
										$mod_year = $year;
									}
									//echo $mod_key;exit;
									$start = new DateTime(date('Y').'-'.$key.'-01');
									$end   = new DateTime($mod_year.'-'.$mod_key.'-01');
									$interval = DateInterval::createFromDateString('1 day');
									$period = new DatePeriod($start, $interval, $end);
									$week_array = array();
									foreach ($period as $dt)
									{
									    if ($dt->format('N') == 3 || $dt->format('N') == 7 || $dt->format('N') == 6)
									    {
									    	$sunday = $dt->format('Y-m-d');
									    	$month_name = date('M', strtotime($sunday));
								    		$year = date('Y', strtotime($sunday));
									    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
									    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
									    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
									    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
									    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
									    	$first_saturday = date('j', strtotime('first sat of '.$month_name.' '.$year));
								    		$current_day = date('j', strtotime($sunday));
								    		if($first_saturday == $current_day){
								    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 1;
								    		} else {
								    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
								    		}

								    		$second_saturday = date('j', strtotime('second sat of '.$month_name.' '.$year));
								    		if($second_saturday == $current_day){
								    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 1;
								    		} else {
								    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
								    		}

								    		$third_saturday = date('j', strtotime('third sat of '.$month_name.' '.$year));
								    		if($third_saturday == $current_day){
								    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 1;
								    		} else {
								    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
								    		}

								    		$fourth_saturday = date('j', strtotime('fourth sat of '.$month_name.' '.$year));
								    		if($fourth_saturday == $current_day){
								    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 1;
								    		} else {
								    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
								    		}

								    		$fifth_saturday = date('j', strtotime('fifth sat of '.$month_name.' '.$year));
								    		if($fifth_saturday == $current_day){
								    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 1;
								    		} else {
								    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
								    		}
									        //$no++;
									    }
									}
								    foreach ($week_array as $wkey => $wvalue) {
										$week_string = 'W_1_'.$var15;
										$halfday_string = 'HD_1_'.$var15;
										$shift_string = 'S_'.$var15;
										if($var15 == '1'){
											if($sat_status == '0' && ($wvalue['second_saturday'] == '1' || $wvalue['fourth_saturday'] == '1') ){
												$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
								        		//echo $sql2;
								        		//echo '<br />';
								        		$this->db->query($sql2);
											}
											if($sat_status == '0' && ($wvalue['first_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
												//$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
								        		//echo $sql2;
								        		//echo '<br />';
								        		//$this->db->query($sql2);
											}
											if($sat_status == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
												$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
								        		//echo $sql2;
								        		//echo '<br />';
								        		$this->db->query($sql2);
											}
										}

										if($var111 == 'VITA' && $var15 == '2'){
											if($wvalue['day_name'] == 'Wed'){
												$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
								        		//echo $sql2;
								        		//echo '<br />';
								        		$this->db->query($sql2);
											}
											if($wvalue['day_name'] == 'Sun'){
												$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
								        		//echo $sql2;
								        		//echo '<br />';
								        		$this->db->query($sql2);
											}
										} else {
											if($wvalue['day_name'] == 'Sun'){
												$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
								        		//echo $sql2;
								        		//echo '<br />';
								        		$this->db->query($sql2);
											}
											if($wvalue['day_name'] == 'Wed'){
												$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
								        		//echo $sql2;
								        		//echo '<br />';
								        		$this->db->query($sql2);
											}
										}
									}
								} else {
									$shift_schedule_data = $is_exist->row;
									$shift = $shift_id_data;
									// echo '<pre>';
									// print_r($shift_schedule_data);
									// exit;
									if($db_unit_id == '0' || $db_shift_id == '0' || ($db_unit_id != $var11) || ($db_shift_id != $var15) || ($db_sat_status != $sat_status)){
										$insert1 = "UPDATE `oc_shift_schedule` SET 
													`emp_code` = '".$var0."',
													`1` = '".$shift_id_data."',
													`2` = '".$shift_id_data."',
													`3` = '".$shift_id_data."',
													`4` = '".$shift_id_data."',
													`5` = '".$shift_id_data."',
													`6` = '".$shift_id_data."',
													`7` = '".$shift_id_data."',
													`8` = '".$shift_id_data."',
													`9` = '".$shift_id_data."',
													`10` = '".$shift_id_data."',
													`11` = '".$shift_id_data."',
													`12` = '".$shift_id_data."', 
													`13` = '".$shift_id_data."', 
													`14` = '".$shift_id_data."', 
													`15` = '".$shift_id_data."', 
													`16` = '".$shift_id_data."', 
													`17` = '".$shift_id_data."', 
													`18` = '".$shift_id_data."', 
													`19` = '".$shift_id_data."', 
													`20` = '".$shift_id_data."', 
													`21` = '".$shift_id_data."', 
													`22` = '".$shift_id_data."', 
													`23` = '".$shift_id_data."', 
													`24` = '".$shift_id_data."', 
													`25` = '".$shift_id_data."', 
													`26` = '".$shift_id_data."', 
													`27` = '".$shift_id_data."', 
													`28` = '".$shift_id_data."', 
													`29` = '".$shift_id_data."', 
													`30` = '".$shift_id_data."', 
													`31` = '".$shift_id_data."',
													`month` = '".$key."',
													`year` = '".date('Y')."',
													`status` = '1' ,
													`unit` = '".$var111."',
													`unit_id` = '".$var11."' 
													WHERE id = '".$shift_schedule_data['id']."' ";
										$this->db->query($insert1);
										
										$year = date('Y');
										if($key == 12){
											$mod_key = 1;
											$mod_year = $year + 1;
										} else {
											$mod_key = $key + 1;
											$mod_year = $year;
										}
										//echo $mod_key;exit;
										$start = new DateTime(date('Y').'-'.$key.'-01');
										$end   = new DateTime($mod_year.'-'.$mod_key.'-01');
										$interval = DateInterval::createFromDateString('1 day');
										$period = new DatePeriod($start, $interval, $end);
										$week_array = array();
										foreach ($period as $dt)
										{
										    if ($dt->format('N') == 3 || $dt->format('N') == 7 || $dt->format('N') == 6)
										    {
										    	$sunday = $dt->format('Y-m-d');
										    	$month_name = date('M', strtotime($sunday));
									    		$year = date('Y', strtotime($sunday));
										    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
										    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
										    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
										    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
										    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
										    	$first_saturday = date('j', strtotime('first sat of '.$month_name.' '.$year));
									    		$current_day = date('j', strtotime($sunday));
									    		if($first_saturday == $current_day){
									    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 1;
									    		} else {
									    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
									    		}

									    		$second_saturday = date('j', strtotime('second sat of '.$month_name.' '.$year));
									    		if($second_saturday == $current_day){
									    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 1;
									    		} else {
									    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
									    		}

									    		$third_saturday = date('j', strtotime('third sat of '.$month_name.' '.$year));
									    		if($third_saturday == $current_day){
									    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 1;
									    		} else {
									    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
									    		}

									    		$fourth_saturday = date('j', strtotime('fourth sat of '.$month_name.' '.$year));
									    		if($fourth_saturday == $current_day){
									    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 1;
									    		} else {
									    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
									    		}

									    		$fifth_saturday = date('j', strtotime('fifth sat of '.$month_name.' '.$year));
									    		if($fifth_saturday == $current_day){
									    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 1;
									    		} else {
									    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
									    		}
										        //$no++;
										    }
										}
									    foreach ($week_array as $wkey => $wvalue) {
											$week_string = 'W_1_'.$var15;
											$halfday_string = 'HD_1_'.$var15;
											$shift_string = 'S_'.$var15;
											if($var15 == '1'){
												if($sat_status == '0' && ($wvalue['second_saturday'] == '1' || $wvalue['fourth_saturday'] == '1') ){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
												if($sat_status == '0' && ($wvalue['first_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
													//$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		//$this->db->query($sql2);
												}
												if($sat_status == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
											}

											if($var111 == 'VITA' && $var15 == '2'){
												if($wvalue['day_name'] == 'Wed'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
												if($wvalue['day_name'] == 'Sun'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
											} else {
												if($wvalue['day_name'] == 'Sun'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
												if($wvalue['day_name'] == 'Wed'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
											}
										}
									}
									/*
									if($db_unit_id == '0' || $db_shift_id == '0' || ($db_unit_id != $var11) || ($db_shift_id != $var15)){
										foreach($shift_schedule_data as $skey => $svalue){
											if($skey != 'id' && $skey != 'emp_code' && $skey != 'month' && $skey != 'year' && $skey != 'status' && $skey != 'unit' && $skey != 'unit_id'){
												$s_data_exp = explode("_", $svalue);
												if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
													foreach($s_data_exp as $sdxkey => $sdxvalue){
														if($sdxkey == 2){
															$s_data_exp[$sdxkey] = $var15;
														}
													}
													$svalue = implode('_', $s_data_exp);
													$sql = "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "', `unit` = '".$var111."', `unit_id` = '".$var11."', `emp_code` = '".$var0."' WHERE `id` = '" . (int)$shift_schedule_data['id'] . "' ";
												} else {
													$sql = "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($shift) . "', `unit` = '".$var111."', `unit_id` = '".$var11."', `emp_code` = '".$var0."' WHERE `id` = '" . (int)$shift_schedule_data['id'] . "' ";
												}
												$this->db->query($sql);	
												//$this->log->write($sql);	
											}
										}
									}
									*/
								} 
							}
							
							$is_exist = $this->db->query("SELECT `emp_id`, `leave_id` FROM `oc_leave` WHERE `emp_id` = '".$var0."' AND `year` = '".date('Y')."' ");
							if($is_exist->num_rows == 0){
								$insert2 = "INSERT INTO `oc_leave` SET 
											`emp_id` = '".$var0."', 
											`emp_name` = '".$this->db->escape($var1)."', 
											`emp_doj` = '".$var4."', 
											`year` = '".date('Y')."',
											`close_status` = '0' "; 
								$this->db->query($insert2);
								// echo $insert1;
								// echo '<br />';
								// exit;
							} else {
								$leave_data = $is_exist->row;
								$insert2 = "UPDATE `oc_leave` SET 
											`emp_id` = '".$var0."', 
											`emp_name` = '".$this->db->escape($var1)."', 
											`emp_doj` = '".$var4."' 
											WHERE `leave_id` = '".$leave_data['leave_id']."' "; 
								$this->db->query($insert2);
							}
						} else {
							$final_employee_error_data[] = $employee_error_data;	
						}
						$emp_code_exist[$var0] = $var0;
					} else {
						if(isset($emp_code_exist[$var0])){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Employee Code Duplicate'
							);
						}
						if($var0 == '' || $var0 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Employee Code Empty'
							);
						}
						$final_employee_error_data[] = $employee_error_data;
					}
				}
				$i ++;	
			}
			//echo 'out';
			//exit;
			fclose($file);
			$url = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'], true);
			//exit;
			$url = str_replace('&amp;', '&', $url);
			//echo $url;exit;
			if(empty($final_employee_error_data)){
				$this->session->data['success'] = 'Employees Imported Successfully';
			} else {
				$this->session->data['warning'] = 'Employees Imported with following Errors';
			}
			$this->session->data['final_employee_error_data'] = $final_employee_error_data;
			$this->response->redirect($url);
		} else {
			$url = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'], true);
			//exit;
			$url = str_replace('&amp;', '&', $url);
			$this->session->data['warning'] = 'Please Select the Valid CSV File';
			$this->response->redirect($url);
		}
		
	}	
}
?>