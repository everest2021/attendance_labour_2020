<?php
class ControllerCatalogDevice extends Controller { 
	private $error = array();

	public function index() {
		date_default_timezone_set("Asia/Kolkata");
		$this->language->load('catalog/device');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/device');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/device');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/device');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_device->addDevice($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/device', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/device');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/device');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_device->editDevice($this->request->get['device_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/device', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function export() { 
		$this->language->load('catalog/device');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/device');

		if(1==1){
			$data['filter_name'] = '';
			$data['sort'] = 'device_id';
			$regions_datas = $this->model_catalog_device->getDevices($data);

			$final_datas = array();
			foreach($regions_datas as $skey => $svalue){
				$final_datas[$skey]['name'] = $svalue['device'];
			}
			// echo '<pre>';
			// print_r($final_datas);
			// exit;

			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			//$template->data['filter_year'] = $filter_year;
			$template->data['title'] = 'Sites';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('catalog/device_html.tpl');
			//echo $html;exit;
			$filename = "Sites";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
		} else {
			$this->session->data['warning'] = 'No Data';
			//$this->redirect($this->url->link('catalog/shift', 'token=' . $this->session->data['token'], 'SSL'));
			$this->getList();
		}
	}

	public function delete() {
		$this->language->load('catalog/device');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/device');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $device_id) {
				$this->model_catalog_device->deleteDevice($device_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/device', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_direction'])) {
			$filter_direction = $this->request->get['filter_direction'];
		} else {
			$filter_direction = '';
		}

		if (isset($this->request->get['filter_direction_id'])) {
			$filter_direction_id = $this->request->get['filter_direction_id'];
		} else {
			$filter_direction_id = '';
		}

		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
		} else {
			$filter_location = '';
		}

		if (isset($this->request->get['filter_location_id'])) {
			$filter_location_id = $this->request->get['filter_location_id'];
		} else {
			$filter_location_id = '';
		}

		if (isset($this->request->get['filter_download_date'])) {
			$filter_download_date = $this->request->get['filter_download_date'];
		} else {
			$filter_download_date = '';
		}

		if (isset($this->request->get['filter_ping_date'])) {
			$filter_ping_date = $this->request->get['filter_ping_date'];
		} else {
			$filter_ping_date = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'DeviceFName';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_direction'])) {
			$url .= '&filter_direction=' . $this->request->get['filter_direction'];
		}

		if (isset($this->request->get['filter_direction_id'])) {
			$url .= '&filter_direction_id=' . $this->request->get['filter_direction_id'];
		}

		if (isset($this->request->get['filter_location'])) {
			$url .= '&filter_location=' . $this->request->get['filter_location'];
		}

		if (isset($this->request->get['filter_location_id'])) {
			$url .= '&filter_location_id=' . $this->request->get['filter_location_id'];
		}

		if (isset($this->request->get['filter_download_date'])) {
			$url .= '&filter_download_date=' . $this->request->get['filter_download_date'];
		}

		if (isset($this->request->get['filter_ping_date'])) {
			$url .= '&filter_ping_date=' . $this->request->get['filter_ping_date'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/device', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/device/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['export'] = $this->url->link('catalog/device/export', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['delete'] = $this->url->link('catalog/device/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['devices'] = array();

		$data = array(
			'filter_name'  => $filter_name,
			'filter_name_id'  => $filter_name_id,
			'filter_direction'  => $filter_direction,
			'filter_direction_id'  => $filter_direction_id,
			'filter_location'  => $filter_location,
			'filter_location_id'  => $filter_location_id,
			'filter_download_date'  => $filter_download_date,
			'filter_ping_date'  => $filter_ping_date,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		$device_total = $this->model_catalog_device->getTotalDevices();
		$results = $this->model_catalog_device->getDevices($data);
		foreach ($results as $result) {
			$action = array();
			$today_date = date('Y-m-d');
			$today_time = date('H:i:s');
			if($result['last_ping_date'] != '0000-00-00' && $result['last_ping_date'] != '1900-01-01'){
				$start_date = new DateTime($today_date.' '.$today_time);
				$since_start = $start_date->diff(new DateTime($result['last_ping_date'].' '.$result['last_ping_time']));
				//echo '<pre>';
				//print_r($since_start);
				if($since_start->days == 0 && $since_start->h == 0 && $since_start->i <= 5){
					$status = 'Online';
				} else {
					$status = 'Offline';
				}
				$last_ping_datess = new DateTime($result['last_ping_date'].' '.$result['last_ping_time']);
				$last_ping_date = $last_ping_datess->format('d-M-Y h:i:s a');
			} else {
				$status = 'Offline';
				$last_ping_date = '';
			}
			if($result['last_download_date'] != '0000-00-00' && $result['last_download_date'] != '1900-01-01'){
				$last_download_datess = new DateTime($result['last_download_date'].' '.$result['last_download_time']);
				$last_download_date = $last_download_datess->format('d-M-Y h:i:s a');
			} else {
				$last_download_date = '';
			}
			$this->data['devices'][] = array(
				'device_id' => $result['device_id'],
				'device_name'        => $result['device_name'],
				'direction'        => $result['direction'],
				'location'        => $result['location'],
				'SerialNumber'   => $result['SerialNumber'],
				'company'        => $result['company'],
				'last_download_date'        => $last_download_date,
				'last_ping_date'        => $last_ping_date,
				'status'        => $status,
				'selected'       => isset($this->request->post['selected']) && in_array($result['device_id'], $this->request->post['selected']),
				'action'         => $action
			);
		}
		//exit;	

		$this->data['token'] = $this->session->data['token'];

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_title'] = $this->language->get('column_title');
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
		$this->data['column_action'] = $this->language->get('column_action');		

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_direction'])) {
			$url .= '&filter_direction=' . $this->request->get['filter_direction'];
		}

		if (isset($this->request->get['filter_direction_id'])) {
			$url .= '&filter_direction_id=' . $this->request->get['filter_direction_id'];
		}

		if (isset($this->request->get['filter_location'])) {
			$url .= '&filter_location=' . $this->request->get['filter_location'];
		}

		if (isset($this->request->get['filter_location_id'])) {
			$url .= '&filter_location_id=' . $this->request->get['filter_location_id'];
		}

		if (isset($this->request->get['filter_download_date'])) {
			$url .= '&filter_download_date=' . $this->request->get['filter_download_date'];
		}

		if (isset($this->request->get['filter_ping_date'])) {
			$url .= '&filter_ping_date=' . $this->request->get['filter_ping_date'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_device'] = $this->url->link('catalog/device', 'token=' . $this->session->data['token'] . '&sort=DeviceFName' . $url, 'SSL');
		$this->data['sort_direction'] = $this->url->link('catalog/device', 'token=' . $this->session->data['token'] . '&sort=DeviceDirection' . $url, 'SSL');
		$this->data['sort_location'] = $this->url->link('catalog/device', 'token=' . $this->session->data['token'] . '&sort=DeviceLocation' . $url, 'SSL');
		$this->data['sort_download_date'] = $this->url->link('catalog/device', 'token=' . $this->session->data['token'] . '&sort=LastLogDownloadDate' . $url, 'SSL');
		$this->data['sort_ping_date'] = $this->url->link('catalog/device', 'token=' . $this->session->data['token'] . '&sort=LastPing' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_direction'])) {
			$url .= '&filter_direction=' . $this->request->get['filter_direction'];
		}

		if (isset($this->request->get['filter_direction_id'])) {
			$url .= '&filter_direction_id=' . $this->request->get['filter_direction_id'];
		}

		if (isset($this->request->get['filter_location'])) {
			$url .= '&filter_location=' . $this->request->get['filter_location'];
		}

		if (isset($this->request->get['filter_location_id'])) {
			$url .= '&filter_location_id=' . $this->request->get['filter_location_id'];
		}

		if (isset($this->request->get['filter_download_date'])) {
			$url .= '&filter_download_date=' . $this->request->get['filter_download_date'];
		}

		if (isset($this->request->get['filter_ping_date'])) {
			$url .= '&filter_ping_date=' . $this->request->get['filter_ping_date'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $device_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/device', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_direction'] = $filter_direction;
		$this->data['filter_direction_id'] = $filter_direction_id;
		$this->data['filter_location'] = $filter_location;
		$this->data['filter_location_id'] = $filter_location_id;
		$this->data['filter_download_date'] = $filter_download_date;
		$this->data['filter_ping_date'] = $filter_ping_date;
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/device_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_bottom'] = $this->language->get('entry_bottom');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['device'])) {
			$this->data['error_device'] = $this->error['device'];
		} else {
			$this->data['error_device'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/device', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['device_id'])) {
			$this->data['action'] = $this->url->link('catalog/device/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/device/update', 'token=' . $this->session->data['token'] . '&device_id=' . $this->request->get['device_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/device', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['device_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$device_info = $this->model_catalog_device->getDevice($this->request->get['device_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['device'])) {
			$this->data['device'] = $this->request->post['device'];
		} elseif (!empty($device_info)) {
			$this->data['device'] = $device_info['device'];
		} else {
			$this->data['device'] = '';
		}

		$this->template = 'catalog/device_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/device')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['device']) < 1) || (utf8_strlen($this->request->post['device']) > 64)) {
			$this->error['device'] = 'Plese Enter Device Name';
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/device')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/device');
			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_device->getDevices($data);
			foreach ($results as $result) {
				$json[] = array(
					'device_id' => $result['device_id'],
					'device'    => strip_tags(html_entity_decode($result['device_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['device'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}
}
?>