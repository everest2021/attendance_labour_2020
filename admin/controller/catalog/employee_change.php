<?php    
class ControllerCatalogEmployeeChange extends Controller { 
	private $error = array();

	public function index() {
		//echo 'MainTaince';exit;
		$this->language->load('catalog/employee_change');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/employee');

		$this->getList();
	}

	public function update() {
		$this->language->load('catalog/employee_change');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/employee');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_catalog_employee->editemployee($this->request->get['employee_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_code'])) {
				$url .= '&filter_code=' . $this->request->get['filter_code'];
			}

			if (isset($this->request->get['filter_department'])) {
				$url .= '&filter_department=' . $this->request->get['filter_department'];
			}

			if (isset($this->request->get['filter_division'])) {
				$url .= '&filter_division=' . $this->request->get['filter_division'];
			}

			if (isset($this->request->get['filter_company'])) {
				$url .= '&filter_company=' . $this->request->get['filter_company'];
			}

			if (isset($this->request->get['filter_region'])) {
				$url .= '&filter_region=' . $this->request->get['filter_region'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('catalog/employee_change', 'token=' . $this->session->data['token'] . $url , 'SSL'));
		}

		$this->getForm();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_code'])) {
			$filter_code = $this->request->get['filter_code'];
		} else {
			$filter_code = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = '';
		}

		if (isset($this->request->get['filter_division'])) {
			$filter_division = $this->request->get['filter_division'];
		} else {
			$filter_division = '';
		}

		if (isset($this->request->get['filter_region'])) {
			$filter_region = $this->request->get['filter_region'];
		} else {
			$filter_region = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['filter_contractor'])) {
			$filter_contractor = $this->request->get['filter_contractor'];
		} else {
			$filter_contractor = '';
		}

		if (isset($this->request->get['filter_company'])) {
			$filter_company = $this->request->get['filter_company'];
		} else {
			$filter_company = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}

		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}

		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['employees'] = array();

		
		$this->data['error_list'] = '0';
		$data = array(
			'filter_name' => $filter_name,
			'filter_code' => $filter_code,
			'filter_department' => $filter_department,
			'filter_division' => $filter_division,
			'filter_region' => $filter_region,
			'filter_company' => $filter_company,
			'filter_unit' => $filter_unit,
			'filter_contractor' => $filter_contractor,
			'filter_transfer' => '1',
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$employee_total = $this->model_catalog_employee->getTotalemployees($data);

		$results = $this->model_catalog_employee->getemployees($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/employee_change/update', 'token=' . $this->session->data['token'] . '&employee_id=' . $result['employee_id'] . $url, 'SSL')
			);

			$today_date = date('j');
			$month = date('n');
			$year = date('Y');
			
			if($result['shift_id'] == '1'){
				$shift_name = '08:00:00 - 20:00:00';
			} elseif($result['shift_id'] == '2'){
				$shift_name = '09:30:00 - 18:30:00';
			}
			//$shift = $this->model_catalog_employee->getshiftname($result['shift']);
			$location = $result['unit'];//$this->model_catalog_employee->getlocname($result['location']); //'Mumbai'
			//$category = $result['category'];//$this->model_catalog_employee->getcatname($result['category']); //'PLD';
			$dept = $result['department'];//$this->model_catalog_employee->getdeptname($result['department']); //'Personal';
			if($result['is_dept'] == '1'){
				$dept_head = 'Department head';
			} else {
				$dept_head = '';
			}
			$this->data['employees'][] = array(
				'employee_id' => $result['employee_id'],
				'name'        => $result['name'],
				'code' 	      => $result['emp_code'],
				'division' 	  => $result['division'],
				'region' 	  => $result['region'],
				'company' 	  => $result['company'],
				'contractor'  => $result['contractor'].'-'.$result['contractor_code'],
				'location' 	  => $location,
				'department'  => $dept,
				'shift'       => $shift_name,
				'dept_head'   => $dept_head,
				'selected'    => isset($this->request->post['selected']) && in_array($result['employee_id'], $this->request->post['selected']),
				'action'      => $action
			);
		}
		
		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_trainer'] = $this->language->get('column_trainer');
		$this->data['column_action'] = $this->language->get('column_action');		

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} elseif (isset($this->session->data['warning'])) {
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}

		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}

		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_code'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=emp_code' . $url, 'SSL');
		$this->data['sort_department'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=department' . $url, 'SSL');
		$this->data['sort_division'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=division' . $url, 'SSL');
		$this->data['sort_region'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=region' . $url, 'SSL');
		$this->data['sort_unit'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=unit' . $url, 'SSL');
		$this->data['sort_company'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=company' . $url, 'SSL');
		$this->data['sort_contractor'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=contractor' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}

		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}

		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}

		$this->load->model('report/attendance');

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $filter_division,
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		$site_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['site_data'] = $site_data;

		$this->load->model('catalog/contractor');
		if($filter_unit != '' && $filter_unit != '0'){
			$data = array(
				'filter_unit_id' => $filter_unit,
				'filter_master' => 0,
			);
			$contractor_datas = $this->model_catalog_contractor->getContractors_by_unit($data);
			foreach ($contractor_datas as $ckey => $cvalue) {
				$contractor_data[$cvalue['contractor_id']] = $cvalue['contractor_name'];
			}
		} else {
			$contractor_data = array();
			$contractor_data['0'] = 'All';
			$contractor_datas = $this->model_catalog_contractor->getContractor_tots();
			foreach ($contractor_datas as $ckey => $cvalue) {
				$contractor_data[$cvalue['contractor_id']] = $cvalue['contractor_name'].'-'.$cvalue['contractor_code'];
			}
		}
		
		$this->data['contractor_data'] = $contractor_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;


		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;


		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $filter_region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;		
		// echo '<pre>';
		// print_r($this->data['division']);
		// exit;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			//if(!empty($company_array)){
				//if(in_array($dvalue['company_id'], $company_array)){
					//$company_data[$dvalue['company_id']] = $dvalue['company'];	
				//}
			//} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			//}
		}
		$this->data['company_data'] = $company_data;
		
		$pagination = new Pagination();
		$pagination->total = $employee_total;
		$pagination->page = $page;
		if($this->data['error_list'] == '1'){
			$pagination->limit = 7777;
		} else {
			$pagination->limit = $this->config->get('config_admin_limit');
		}
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_code'] = $filter_code;
		$this->data['filter_department'] = $filter_department;
		$this->data['filter_region'] = $filter_region;
		$this->data['filter_division'] = $filter_division;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_company'] = $filter_company;
		$this->data['filter_contractor'] = $filter_contractor;

		$this->template = 'catalog/employee_change_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_dob'] = $this->language->get('entry_dob');
		$this->data['entry_doj'] = $this->language->get('entry_doj');
		$this->data['entry_action'] = $this->language->get('entry_action');
		$this->data['entry_assign'] = $this->language->get('entry_assign');
		$this->data['entry_remove'] = $this->language->get('entry_remove');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['emp_code'])) {
			$this->data['error_emp_code'] = $this->error['emp_code'];
		} else {
			$this->data['error_emp_code'] = '';
		}

		if (isset($this->error['doj'])) {
			$this->data['error_doj'] = $this->error['doj'];
		} else {
			$this->data['error_doj'] = '';
		}

		if (isset($this->error['dol'])) {
			$this->data['error_dol'] = $this->error['dol'];
		} else {
			$this->data['error_dol'] = '';
		}

		if (isset($this->error['designation'])) {
			$this->data['error_designation'] = $this->error['designation'];
		} else {
			$this->data['error_designation'] = '';
		}

		if (isset($this->error['department'])) {
			$this->data['error_department'] = $this->error['department'];
		} else {
			$this->data['error_department'] = '';
		}

		if (isset($this->error['site'])) {
			$this->data['error_site'] = $this->error['site'];
		} else {
			$this->data['error_site'] = '';
		}

		if (isset($this->error['division'])) {
			$this->data['error_division'] = $this->error['division'];
		} else {
			$this->data['error_division'] = '';
		}

		if (isset($this->error['state'])) {
			$this->data['error_state'] = $this->error['state'];
		} else {
			$this->data['error_state'] = '';
		}

		if (isset($this->error['company'])) {
			$this->data['error_company'] = $this->error['company'];
		} else {
			$this->data['error_company'] = '';
		}

		if (isset($this->error['contractor'])) {
			$this->data['error_contractor'] = $this->error['contractor'];
		} else {
			$this->data['error_contractor'] = '';
		}

		if (isset($this->error['grade'])) {
			$this->data['error_grade'] = $this->error['grade'];
		} else {
			$this->data['error_grade'] = '';
		}

		if (isset($this->error['employement'])) {
			$this->data['error_employement'] = $this->error['employement'];
		} else {
			$this->data['error_employement'] = '';
		}

		if (isset($this->error['region'])) {
			$this->data['error_region'] = $this->error['region'];
		} else {
			$this->data['error_region'] = '';
		}

		if (isset($this->error['card_number'])) {
			$this->data['error_card_number'] = $this->error['card_number'];
		} else {
			$this->data['error_card_number'] = '';
		}

		if (isset($this->error['daily_punch'])) {
			$this->data['error_daily_punch'] = $this->error['daily_punch'];
		} else {
			$this->data['error_daily_punch'] = '';
		}

		if (isset($this->error['weekly_off'])) {
			$this->data['error_weekly_off'] = $this->error['weekly_off'];
		} else {
			$this->data['error_weekly_off'] = '';
		}

		if (isset($this->error['status'])) {
			$this->data['error_status'] = $this->error['status'];
		} else {
			$this->data['error_status'] = '';
		}

		if (isset($this->error['cdate'])) {
			$this->data['error_cdate'] = $this->error['cdate'];
		} else {
			$this->data['error_cdate'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}

		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}

		if (isset($this->request->get['filter_company'])) {
			$url .= '&filter_company=' . $this->request->get['filter_company'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}


		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (isset($this->request->get['employee_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$employee_info = $this->model_catalog_employee->getemployee($this->request->get['employee_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($employee_info)) {
			$this->data['name'] = $employee_info['title'].' '.$employee_info['name'];
		} else {	
			$this->data['name'] = '';
		}

		if (isset($this->request->post['emp_code'])) {
			$this->data['emp_code'] = $this->request->post['emp_code'];
			$this->data['hidden_emp_code'] = $this->request->post['hidden_emp_code'];
		} elseif (!empty($employee_info)) {
			$this->data['emp_code'] = $employee_info['emp_code'];
			$this->data['hidden_emp_code'] = $employee_info['emp_code'];
		} else {	
			$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` ORDER BY `emp_code` DESC LIMIT 1");
			if($emp_codes->num_rows > 0){
				$emp_code = $emp_codes->row['emp_code'] + 1;
			} else {
				$emp_code = '1000';
			}
			$this->data['emp_code'] = $emp_code;
			$this->data['hidden_emp_code'] = $emp_code;
		}

		if (isset($this->request->post['grade'])) {
			$this->data['grade'] = $this->request->post['grade'];
			$this->data['hidden_grade'] = $this->request->post['hidden_grade'];
		} elseif (!empty($employee_info)) {
			$this->data['grade'] = $employee_info['grade'];
			$this->data['hidden_grade'] = $employee_info['grade'];
		} else {	
			$this->data['grade'] = '';
			$this->data['hidden_grade'] = '';
		}

		if (isset($this->request->post['grade_id'])) {
			$this->data['grade_id'] = $this->request->post['grade_id'];
			$this->data['hidden_grade_id'] = $this->request->post['hidden_grade_id'];
		} elseif (!empty($employee_info)) {
			$this->data['grade_id'] = $employee_info['grade_id'];
			$this->data['hidden_grade_id'] = $employee_info['grade_id'];
		} else {	
			$this->data['grade_id'] = '0';
			$this->data['hidden_grade_id'] = '0';
		}

		if (isset($this->request->post['department'])) {
			$this->data['department'] = $this->request->post['department'];
			$this->data['hidden_department'] = $this->request->post['hidden_department'];
		} elseif (!empty($employee_info)) {
			$this->data['department'] = $employee_info['department'];
			$this->data['hidden_department'] = $employee_info['department'];
		} else {	
			$this->data['department'] = '';
			$this->data['hidden_department'] = '';
		}
		//contractor
		if (isset($this->request->post['contractor'])) {
			$this->data['contractor'] = $this->request->post['contractor'];
			$this->data['hidden_contractor'] = $this->request->post['hidden_contractor'];
		} elseif (!empty($employee_info)) {
			$this->data['contractor'] = $employee_info['contractor'];
			$this->data['hidden_contractor'] = $employee_info['contractor'];
		} else {	
			$this->data['contractor'] = '';
			$this->data['hidden_contractor'] = '';
		}

		if (isset($this->request->post['contractor_id'])) {
			$this->data['contractor_id'] = $this->request->post['contractor_id'];
			$this->data['hidden_contractor_id'] = $this->request->post['hidden_contractor_id'];
		} elseif (!empty($employee_info)) {
			$this->data['contractor_id'] = $employee_info['contractor_id'];
			$this->data['hidden_contractor_id'] = $employee_info['contractor_id'];
		} else {	
			$this->data['contractor_id'] = '0';
			$this->data['hidden_contractor_id'] = '0';
		}

		//
		//category
		if (isset($this->request->post['category'])) {
			$this->data['category'] = $this->request->post['category'];
			$this->data['hidden_category'] = $this->request->post['hidden_category'];
		} elseif (!empty($employee_info)) {
			$this->data['category'] = $employee_info['category'];
			$this->data['hidden_category'] = $employee_info['category'];
		} else {	
			$this->data['category'] = '';
			$this->data['hidden_category'] = '';
		}

		if (isset($this->request->post['category_id'])) {
			$this->data['category_id'] = $this->request->post['category_id'];
			$this->data['hidden_category_id'] = $this->request->post['hidden_category_id'];
		} elseif (!empty($employee_info)) {
			$this->data['category_id'] = $employee_info['category_id'];
			$this->data['hidden_category_id'] = $employee_info['category_id'];
		} else {	
			$this->data['category_id'] = '0';
			$this->data['hidden_category_id'] = '0';
		}

		//
		if (isset($this->request->post['department_id'])) {
			$this->data['department_id'] = $this->request->post['department_id'];
			$this->data['hidden_department_id'] = $this->request->post['hidden_department_id'];
		} elseif (!empty($employee_info)) {
			$this->data['department_id'] = $employee_info['department_id'];
			$this->data['hidden_department_id'] = $employee_info['department_id'];
		} else {	
			$this->data['department_id'] = '0';
			$this->data['hidden_department_id'] = '0';
		}

		if (isset($this->request->post['division'])) {
			$this->data['division'] = $this->request->post['division'];
			$this->data['hidden_division'] = $this->request->post['hidden_division'];
		} elseif (!empty($employee_info)) {
			$this->data['division'] = $employee_info['division'];
			$this->data['hidden_division'] = $employee_info['division'];
		} else {	
			$this->data['division'] = '';
			$this->data['hidden_division'] = '';
		}

		if (isset($this->request->post['division_id'])) {
			$this->data['division_id'] = $this->request->post['division_id'];
			$this->data['hidden_division_id'] = $this->request->post['hidden_division_id'];
		} elseif (!empty($employee_info)) {
			$this->data['division_id'] = $employee_info['division_id'];
			$this->data['hidden_division_id'] = $employee_info['division_id'];
		} else {	
			$this->data['division_id'] = '';
			$this->data['hidden_division_id'] = '';
		}

		if (isset($this->request->post['region'])) {
			$this->data['region'] = $this->request->post['region'];
			$this->data['hidden_region'] = $this->request->post['hidden_region'];
		} elseif (!empty($employee_info)) {
			$this->data['region'] = $employee_info['region'];
			$this->data['hidden_region'] = $employee_info['region'];
		} else {	
			$this->data['region'] = '';
			$this->data['hidden_region'] = '';
		}

		if (isset($this->request->post['region_id'])) {
			$this->data['region_id'] = $this->request->post['region_id'];
			$this->data['hidden_region_id'] = $this->request->post['hidden_region_id'];
		} elseif (!empty($employee_info)) {
			$this->data['region_id'] = $employee_info['region_id'];
			$this->data['hidden_region_id'] = $employee_info['region_id'];
		} else {	
			$this->data['region_id'] = '';
			$this->data['hidden_region_id'] = '';
		}

		if (isset($this->request->post['unit'])) {
			$this->data['unit'] = $this->request->post['unit'];
			$this->data['hidden_unit'] = $this->request->post['hidden_unit'];
		} elseif (!empty($employee_info)) {
			$this->data['unit'] = $employee_info['unit'];
			$this->data['hidden_unit'] = $employee_info['unit'];
		} else {	
			$this->data['unit'] = '';
			$this->data['hidden_unit'] = '';
		}

		if (isset($this->request->post['unit_id'])) {
			$this->data['unit_id'] = $this->request->post['unit_id'];
			$this->data['hidden_unit_id'] = $this->request->post['hidden_unit_id'];
		} elseif (!empty($employee_info)) {
			$this->data['unit_id'] = $employee_info['unit_id'];
			$this->data['hidden_unit_id'] = $employee_info['unit_id'];
		} else {	
			$this->data['unit_id'] = '';
			$this->data['hidden_unit_id'] = '';
		}

		if (isset($this->request->post['designation'])) {
			$this->data['designation'] = $this->request->post['designation'];
			$this->data['hidden_designation'] = $this->request->post['hidden_designation'];
		} elseif (!empty($employee_info)) {
			$this->data['designation'] = $employee_info['designation'];
			$this->data['hidden_designation'] = $employee_info['designation'];
		} else {	
			$this->data['designation'] = '';
			$this->data['hidden_designation'] = '';
		}

		if (isset($this->request->post['designation_id'])) {
			$this->data['designation_id'] = $this->request->post['designation_id'];
			$this->data['hidden_designation_id'] = $this->request->post['hidden_designation_id'];
		} elseif (!empty($employee_info)) {
			$this->data['designation_id'] = $employee_info['designation_id'];
			$this->data['hidden_designation_id'] = $employee_info['designation_id'];
		} else {	
			$this->data['designation_id'] = '';
			$this->data['hidden_designation_id'] = '';
		}

		if (isset($this->request->post['company'])) {
			$this->data['company'] = $this->request->post['company'];
			$this->data['hidden_company'] = $this->request->post['hidden_company'];
		} elseif (!empty($employee_info)) {
			$this->data['company'] = $employee_info['company'];
			$this->data['hidden_company'] = $employee_info['company'];
		} else {	
			$this->data['company'] = '';
			$this->data['hidden_company'] = '';
		}

		if (isset($this->request->post['company_id'])) {
			$this->data['company_id'] = $this->request->post['company_id'];
			$this->data['hidden_company_id'] = $this->request->post['hidden_company_id'];
		} elseif (!empty($employee_info)) {
			$this->data['company_id'] = $employee_info['company_id'];
			$this->data['hidden_company_id'] = $employee_info['company_id'];
		} else {	
			$this->data['company_id'] = '';
			$this->data['hidden_company_id'] = '';
		}

		if (isset($this->request->post['state'])) {
			$this->data['state'] = $this->request->post['state'];
			$this->data['hidden_state'] = $this->request->post['hidden_state'];
		} elseif (!empty($employee_info)) {
			$this->data['state'] = $employee_info['state'];
			$this->data['hidden_state'] = $employee_info['state'];
		} else {	
			$this->data['state'] = '';
			$this->data['hidden_state'] = '';
		}

		if (isset($this->request->post['state_id'])) {
			$this->data['state_id'] = $this->request->post['state_id'];
			$this->data['hidden_state_id'] = $this->request->post['hidden_state_id'];
		} elseif (!empty($employee_info)) {
			$this->data['state_id'] = $employee_info['state_id'];
			$this->data['hidden_state_id'] = $employee_info['state_id'];
		} else {	
			$this->data['state_id'] = '';
			$this->data['hidden_state_id'] = '';
		}

		if (isset($this->request->post['employement'])) {
			$this->data['employement'] = $this->request->post['employement'];
			$this->data['hidden_employement'] = $this->request->post['hidden_employement'];
		} elseif (!empty($employee_info)) {
			$this->data['employement'] = $employee_info['employement'];
			$this->data['hidden_employement'] = $employee_info['employement'];
		} else {	
			$this->data['employement'] = '';
			$this->data['hidden_employement'] = '';
		}

		if (isset($this->request->post['employement_id'])) {
			$this->data['employement_id'] = $this->request->post['employement_id'];
			$this->data['hidden_employement_id'] = $this->request->post['hidden_employement_id'];
		} elseif (!empty($employee_info)) {
			$this->data['employement_id'] = $employee_info['employement_id'];
			$this->data['hidden_employement_id'] = $employee_info['employement_id'];
		} else {	
			$this->data['employement_id'] = '';
			$this->data['hidden_employement_id'] = '';
		}

		if (isset($this->request->post['shift_id'])) {
			$this->data['shift_id'] = $this->request->post['shift_id'];
			$this->data['hidden_shift_id'] = $this->request->post['hidden_shift_id'];
		} elseif (!empty($employee_info)) {
			$this->data['shift_id'] = $employee_info['shift_id'];
			$this->data['hidden_shift_id'] = $employee_info['shift_id'];
		} else {	
			$this->data['shift_id'] = '1';
			$this->data['hidden_shift_id'] = '1';
		}

		if (isset($this->request->post['sat_status'])) {
			$this->data['sat_status'] = $this->request->post['sat_status'];
			$this->data['hidden_sat_status'] = $this->request->post['hidden_sat_status'];
		} elseif (!empty($employee_info)) {
			$this->data['sat_status'] = $employee_info['sat_status'];
			$this->data['hidden_sat_status'] = $employee_info['sat_status'];
		} else {	
			$this->data['sat_status'] = '1';
			$this->data['hidden_sat_status'] = '1';
		}

		$shift_datas = $this->db->query("SELECT * FROM `oc_shift` WHERE (`shift_id` = '1' OR `shift_id` = '2') ")->rows;
		$shift_data = array();
		foreach($shift_datas as $skey => $svalue){
			$shift_data[$svalue['shift_id']] = $svalue['name'];
		}
		// echo '<pre>';
		// print_r($shift_data);
		// exit;
		$this->data['shift_data'] = $shift_data;

		if (isset($this->request->post['dob'])) {
			$this->data['dob'] = $this->request->post['dob'];
		} elseif (!empty($employee_info)) {
			if($employee_info['dob'] != '0000-00-00'){
				$this->data['dob'] = date('d-m-Y', strtotime($employee_info['dob']));
			} else {
				$this->data['dob'] = '';
			}
		} else {	
			$this->data['dob'] = '';
		}

		if (isset($this->request->post['doj'])) {
			$this->data['doj'] = $this->request->post['doj'];
			$this->data['hidden_doj'] = $this->request->post['hidden_doj'];
		} elseif (!empty($employee_info)) {
			if($employee_info['doj'] != '0000-00-00'){
				$this->data['doj'] = date('d-m-Y', strtotime($employee_info['doj']));
				$this->data['hidden_doj'] = date('d-m-Y', strtotime($employee_info['doj']));
			} else {
				$this->data['doj'] = '';
				$this->data['hidden_doj'] = '';
			}

		} else {	
			$this->data['doj'] = '';
			$this->data['hidden_doj'] = '';
		}

		if (isset($this->request->post['doc'])) {
			$this->data['doc'] = $this->request->post['doc'];
		} elseif (!empty($employee_info)) {
			if($employee_info['doc'] != '0000-00-00'){
				$this->data['doc'] = date('d-m-Y', strtotime($employee_info['doc']));
			} else {
				$this->data['doc'] = '';
			}
		} else {	
			$this->data['doc'] = '';
		}

		if (isset($this->request->post['dol'])) {
			$this->data['dol'] = $this->request->post['dol'];
			$this->data['hidden_dol'] = $this->request->post['hidden_dol'];
		} elseif (!empty($employee_info)) {
			if($employee_info['dol'] != '0000-00-00'){
				$this->data['dol'] = date('d-m-Y', strtotime($employee_info['dol']));
				$this->data['hidden_dol'] = date('d-m-Y', strtotime($employee_info['dol']));
			} else {
				$this->data['dol'] = '';
				$this->data['hidden_dol'] = '';
			}
		} else {	
			$this->data['dol'] = '';
			$this->data['hidden_dol'] = '';
		}

		// if (isset($this->request->post['shift_name'])) {
		// 	$this->data['shift_name'] = $this->request->post['shift_name'];
		// 	$this->data['shift_id'] = $this->request->post['shift_id'];
		// } elseif (!empty($employee_info)) {
		// 	$today_date = date('j');
		// 	$month = date('n');
		// 	$year = date('Y');
		// 	$shift_data = $this->model_catalog_employee->getshift_id($employee_info['emp_code'], $today_date, $month, $year);
		// 	$shift_ids = explode('_', $shift_data);
		// 	if(isset($shift_ids[1])){
		// 		if($shift_ids[0] == 'S'){
		// 			$shift_id = $shift_ids['1'];
		// 			$shift_name = $this->model_catalog_employee->getshiftdata($shift_id);
		// 		} elseif($shift_ids[0] == 'W') {
		// 			$shift_id = $shift_ids['1'];
		// 			//$shift_name = $this->model_catalog_employee->getweekname($shift_id);
		// 			$shift_name = 'Weekly Off';
		// 		} elseif($shift_ids[0] == 'H'){
		// 			$shift_id = $shift_ids['1'];
		// 			//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
		// 			$shift_name = 'Holiday';
		// 		} elseif($shift_ids[0] == 'HD'){
		// 			$shift_id = $shift_ids['1'];
		// 			//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
		// 			$shift_name = 'Half Day';
		// 		} elseif($shift_ids[0] == 'C'){
		// 			$shift_id = $shift_ids['1'];
		// 			//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
		// 			$shift_name = 'Complimentary Off';
		// 		}
		// 	} else {
		// 		$shift_id = 0;
		// 		$shift_name = '';
		// 	}
		// 	$this->data['shift_id'] = $shift_id;
		// 	$this->data['shift_name'] = $shift_name;
		// } else {	
		// 	$this->data['shift_name'] = '';
		// 	$this->data['shift_id'] = 0;
		// }

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
			$this->data['hidden_status'] = $this->request->post['hidden_status'];
		} elseif (!empty($employee_info)) {
			$this->data['status'] = $employee_info['status'];
			$this->data['hidden_status'] = $employee_info['status'];
		} else {	
			$this->data['status'] = 1;
			$this->data['hidden_status'] = 1;
		}

		if (isset($this->request->post['gender'])) {
			$this->data['gender'] = $this->request->post['gender'];
		} elseif (!empty($employee_info)) {
			$this->data['gender'] = $employee_info['gender'];
		} else {	
			$this->data['gender'] = '';
		}

		$this->data['genders'] = array(
			'M' => 'Male',
			'F' => 'Female'
		);

		if (isset($this->request->post['card_number'])) {
			$this->data['card_number'] = $this->request->post['card_number'];
		} elseif (!empty($employee_info)) {
			$this->data['card_number'] = $employee_info['card_number'];
		} else {	
			$this->data['card_number'] = '';
		}

		if (isset($this->request->post['shift_type'])) {
			$this->data['shift_type'] = $this->request->post['shift_type'];
		} elseif (!empty($employee_info)) {
			$this->data['shift_type'] = $employee_info['shift_type'];
		} else {	
			$this->data['shift_type'] = 'F';
		}

		if (isset($this->request->post['is_dept'])) {
			$this->data['is_dept'] = $this->request->post['is_dept'];
		} elseif (!empty($employee_info)) {
			$this->data['is_dept'] = $employee_info['is_dept'];
		} else {	
			$this->data['is_dept'] = '0';
		}

		if (isset($this->request->post['is_super'])) {
			$this->data['is_super'] = $this->request->post['is_super'];
		} elseif (!empty($employee_info)) {
			$this->data['is_super'] = $employee_info['is_super'];
		} else {	
			$this->data['is_super'] = '0';
		}

		if (isset($this->request->post['password_reset'])) {
			$this->data['password_reset'] = $this->request->post['password_reset'];
		} else {	
			$this->data['password_reset'] = '0';
		}

		if (isset($this->request->post['change_date'])) {
			$this->data['change_date'] = $this->request->post['change_date'];
		} else {	
			$this->data['change_date'] = '';//date('d-m-Y');
		}

		$this->load->model('report/attendance');

		$this->load->model('catalog/designation');
		$data = array(
			'filter_grade' => $this->data['grade_id'],
		);
		$designation_datas = $this->model_catalog_designation->getDesignations();
		$designation_data = array();
		$designation_data['0'] = 'Please Select';
		foreach ($designation_datas as $dkey => $dvalue) {
			$designation_data[$dvalue['designation_id']] = $dvalue['d_name'];
		}
		$this->data['designation_data'] = $designation_data;
		
		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'Please Select';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;

		$this->load->model('catalog/state');
		$state_datas = $this->model_catalog_state->getStates();
		$state_data = array();
		$state_data['0'] = 'Please Select';
		foreach ($state_datas as $dkey => $dvalue) {
			$state_data[$dvalue['state_id']] = $dvalue['state'];
		}
		$this->data['state_data'] = $state_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		$company_data['0'] = 'Please Select';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			//if(!empty($company_array)){
				//if(in_array($dvalue['company_id'], $company_array)){
					//$company_data[$dvalue['company_id']] = $dvalue['company'];	
				//}
			//} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			//}
		}
		$this->data['company_data'] = $company_data;

		$this->load->model('catalog/grade');
		$grade_datas = $this->model_catalog_grade->getGrades();
		$grade_data = array();
		$grade_data['0'] = 'Please Select';
		foreach ($grade_datas as $dkey => $dvalue) {
			$grade_data[$dvalue['grade_id']] = $dvalue['g_name'];
		}
		$this->data['grade_data'] = $grade_data;

		$this->load->model('catalog/employement');
		$employement_datas = $this->model_catalog_employement->getEmployements();
		$employement_data = array();
		$employement_data['0'] = 'Please Select';
		foreach ($employement_datas as $dkey => $dvalue) {
			$employement_data[$dvalue['employement_id']] = $dvalue['employement'];
		}
		$this->data['employement_data'] = $employement_data;

		$this->load->model('catalog/contractor');
		if($this->data['unit_id'] != '' && $this->data['unit_id'] != '0'){
			$data = array(
				'filter_unit_id' => $this->data['unit_id'],
				'filter_master' => 1,
			);
			$contractor_datas = $this->model_catalog_contractor->getContractors_by_unit($data);
			foreach ($contractor_datas as $ckey => $cvalue) {
				$contractor_data[$cvalue['contractor_id']] = $cvalue['contractor_name'];
			}
		} else {
			$contractor_data = array();
			$contractor_data['0'] = 'Please Select';
			$contractor_datas = $this->model_catalog_contractor->getContractor_tots();
			foreach ($contractor_datas as $ckey => $cvalue) {
				$contractor_data[$cvalue['contractor_id']] = $cvalue['contractor_name'].'-'.$cvalue['contractor_code'];
			}
		}
		
		$this->data['contractor_data'] = $contractor_data;

		//category
		$this->load->model('catalog/category');
		$category_datas = $this->model_catalog_category->getcategorys();
		//echo '<pre>';print_r($category_datas);exit;
		$category_data = array();
		$category_data['0'] = 'Please Select';
		foreach ($category_datas as $ckey => $cvalue) {
			$category_data[$cvalue['category_id']] = $cvalue['category_name'];
		}
		$this->data['category_data'] = $category_data;

		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'Please Select';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;


		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $this->data['region_id'],
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'Please Select';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $this->data['division_id'],
			//'filter_state_id' => $this->data['state_id'],
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		$site_data['0'] = 'Please Select';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['site_data'] = $site_data;

		$department_history = array();
		$unit_history = array();
		if(isset($this->request->get['employee_id'])){
			$emp_code = $this->data['emp_code'];
			$department_historys = $this->db->query("SELECT * FROM `oc_employee_change_history` WHERE `emp_code` = '".$emp_code."' AND `type` = 'Department' ");
			foreach($department_historys->rows as $dkey => $dvalue){
				if($dvalue['from_date'] != '0000-00-00'){
					$from_date = date('d-m-Y', strtotime($dvalue['from_date']));
				} else {
					$from_date = '00-00-0000';
				}
				if($dvalue['to_date'] != '0000-00-00'){
					$to_date = date('d-m-Y', strtotime($dvalue['to_date']));
				} else {
					$to_date = '00-00-0000';
				}
				$department_history[] = array(
					'value' => $dvalue['value'],
					'from_date' => $from_date,
					'to_date' => $to_date,
				);
			}

			$unit_historys = $this->db->query("SELECT * FROM `oc_employee_change_history` WHERE `emp_code` = '".$emp_code."' AND `type` = 'Unit' ");
			foreach($unit_historys->rows as $dkey => $dvalue){
				if($dvalue['from_date'] != '0000-00-00'){
					$from_date = date('d-m-Y', strtotime($dvalue['from_date']));
				} else {
					$from_date = '00-00-0000';
				}
				if($dvalue['to_date'] != '0000-00-00'){
					$to_date = date('d-m-Y', strtotime($dvalue['to_date']));
				} else {
					$to_date = '00-00-0000';
				}
				$unit_history[] = array(
					'value' => $dvalue['value'],
					'from_date' => $from_date,
					'to_date' => $to_date,
				);
			}
		}
		$this->data['department_history'] = $department_history;
		$this->data['unit_history'] = $unit_history;

		// if (isset($this->request->post['category_name'])) {
		// 	$this->data['category_id'] = $this->request->post['category_id'];			
		// 	$this->data['category_name'] = $this->request->post['category_name'];
		// } elseif (!empty($employee_info)) {
		// 	$this->data['category_id'] = $employee_info['category'];
		// 	$category_name = $this->model_catalog_employee->getcatname($employee_info['category']);
		// 	$this->data['category_name'] = $category_name;
		// } else {	
		// 	$this->data['category_id'] = '';
		// 	$this->data['category_name'] = 0;
		// }

		// if (isset($this->request->post['department_name'])) {
		// 	$this->data['department_name'] = $this->request->post['department_name'];
		// 	$this->data['department_id'] = $this->request->post['department_id'];
		// } elseif (!empty($employee_info)) {
		// 	$this->data['department_id'] = $employee_info['department'];
		// 	$department_name = $this->model_catalog_employee->getdeptname($employee_info['department']);
		// 	$this->data['department_name'] = $department_name;
		// } else {	
		// 	$this->data['department_name'] = '';
		// 	$this->data['department_id'] = 0;
		// }

		// if (isset($this->request->post['location_name'])) {
		// 	$this->data['location_name'] = $this->request->post['location_name'];
		// 	$this->data['location_id'] = $this->request->post['location_id'];
		// } elseif (!empty($employee_info)) {
		// 	$this->data['location_id'] = $employee_info['location'];
		// 	$location_name = $this->model_catalog_employee->getlocname($employee_info['location']);
		// 	$this->data['location_name'] = $location_name;
		// } else {	
		// 	$this->data['location_name'] = '';
		// 	$this->data['location_id'] = 0;
		// }

		

		if (!isset($this->request->get['employee_id'])) {
			$this->data['action'] = $this->url->link('catalog/employee_change/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			if(isset($this->request->get['return'])){
				$this->data['action'] = $this->url->link('catalog/employee_change/update', 'token=' . $this->session->data['token'] . '&employee_id=' . $this->request->get['employee_id'] . '&return=1', 'SSL');
			} else {
				$this->data['action'] = $this->url->link('catalog/employee_change/update', 'token=' . $this->session->data['token'] . '&employee_id=' . $this->request->get['employee_id'] . $url, 'SSL');
			}
		}

		if(isset($this->request->get['return'])){
			$this->data['cancel'] = $this->url->link('report/employee_change', 'token=' . $this->session->data['token'] . '&h_name=' . $this->data['name'] . '&h_name_id=' . $this->request->get['employee_id'], 'SSL');
		} else {
			$this->data['cancel'] = $this->url->link('catalog/employee_change', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}

		$this->data['weeks'] = array(
			'1' => 'Sunday',
			'2' => 'Monday',
			'3' => 'Tuesday',
			'4' => 'Wednesday',
			'5' => 'Thursday',
			'6' => 'Friday',
			'7' => 'Saturday',
		);
		
		$this->template = 'catalog/employee_change_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		$this->load->model('catalog/employee');

		// echo '<pre>';
		// print_r($this->request->post);
		// exit;

		// if(isset($this->request->get['employee_id'])){
		// 	if (!$this->user->hasPermission('modify', 'catalog/employee_change')) {
		// 		$this->error['warning'] = $this->language->get('error_permission');
		// 	}
		// } else {
		// 	if (!$this->user->hasPermission('add', 'catalog/employee_change')) {
		// 		$this->error['warning'] = $this->language->get('error_permission');
		// 	}
		// }

		if(strlen(utf8_decode(trim($this->request->post['name']))) < 1 || strlen(utf8_decode(trim($this->request->post['name']))) > 255){
			$this->error['name'] = $this->language->get('error_name');
		}

		if(strlen(utf8_decode(trim($this->request->post['emp_code']))) < 12 || strlen(utf8_decode(trim($this->request->post['emp_code']))) > 12){
			$this->error['emp_code'] = 'Please Enter Valid Employee Code';
		} else {
			if(isset($this->request->get['employee_id'])){
				$is_exist = $this->db->query("SELECT COUNT(*) as total FROM `oc_employee` WHERE `emp_code` = '".$this->request->post['emp_code']."' AND `emp_code` <> '".$this->request->post['hidden_emp_code']."' ")->row['total'];
				// echo '<pre>';
				// print_r($is_exist);
				// exit;
				if($is_exist){
					$this->error['emp_code'] = 'Employee Code Exists';	
				}
			} else {
				$is_exist = $this->db->query("SELECT COUNT(*) as total FROM `oc_employee` WHERE `emp_code` = '".$this->request->post['emp_code']."' ")->row['total'];
				if($is_exist){
					$this->error['emp_code'] = 'Employee Code Exists';	
				}
			}
		}

		if($this->request->post['doj'] == '' || $this->request->post['doj'] == '00-00-0000'){
			$this->error['doj'] = 'Please Enter Valid Date of Joining';
		}

		if($this->request->post['hidden_status'] == '1' && $this->request->post['status'] == '0'){
			if($this->request->post['dol'] == '' || $this->request->post['dol'] == '00-00-0000'){
				$this->error['dol'] = 'Please Enter Valid Date of Exit';
			}
		}

		if($this->request->post['designation'] == '' || $this->request->post['designation_id'] == '' || $this->request->post['designation_id'] == '0'){
			$this->error['designation'] = 'Please Enter Valid Designation';
		} else {
			$designation_name = html_entity_decode($this->request->post['designation']);
			$designation_name = trim(preg_replace('/\s+/',' ',$designation_name));
			$is_exist = $this->db->query("SELECT `designation_id` FROM `oc_designation` WHERE `d_name` = '".$this->db->escape($designation_name)."' ");
			if($is_exist->num_rows == 0){
				$this->error['designation'] = 'Please Enter Valid Designation';
			}
		}

		if($this->request->post['department'] == '' || $this->request->post['department_id'] == '' || $this->request->post['department_id'] == '0'){
			$this->error['department'] = 'Please Enter Valid Department';
		} else {
			$department_name = html_entity_decode($this->request->post['department']);
			$department_name = trim(preg_replace('/\s+/',' ',$department_name));
			//echo $department_name;exit;
			//echo "SELECT `department_id` FROM `oc_department` WHERE `d_name` = '".$this->db->escape($department_name)."' ";exit;
			$is_exist = $this->db->query("SELECT `department_id` FROM `oc_department` WHERE `d_name` = '".$this->db->escape($department_name)."' ");
			if($is_exist->num_rows == 0){
				$this->error['department'] = 'Please Enter Valid Department';
			}
		}

		if($this->request->post['unit'] == '' || $this->request->post['unit_id'] == '' || $this->request->post['unit_id'] == '0'){
			$this->error['site'] = 'Please Enter Valid Site';
		} else {
			$unit_name = html_entity_decode($this->request->post['unit']);
			$unit_name = trim(preg_replace('/\s+/',' ',$unit_name));
			$is_exist = $this->db->query("SELECT `unit_id` FROM `oc_unit` WHERE `unit` = '".$this->db->escape($unit_name)."' ");
			if($is_exist->num_rows == 0){
				$this->error['site'] = 'Please Enter Valid Unit';
			}
		}

		if($this->request->post['division'] == '' || $this->request->post['division_id'] == '' || $this->request->post['division_id'] == '0'){
			$this->error['division'] = 'Please Enter Valid Division';
		} else {
			$division_name = html_entity_decode($this->request->post['division']);
			$division_name = trim(preg_replace('/\s+/',' ',$division_name));
			$is_exist = $this->db->query("SELECT `division_id` FROM `oc_division` WHERE `division` = '".$this->db->escape($division_name)."' ");
			if($is_exist->num_rows == 0){
				$this->error['division'] = 'Please Enter Valid Division';
			}
		}

		if($this->request->post['state'] == '' || $this->request->post['state_id'] == '' || $this->request->post['state_id'] == '0'){
			//$this->error['state'] = 'Please Enter Valid State';
		} else {
			$state_name = html_entity_decode($this->request->post['state']);
			$state_name = trim(preg_replace('/\s+/',' ',$state_name));
			$is_exist = $this->db->query("SELECT `state_id` FROM `oc_state` WHERE `state` = '".$this->db->escape($state_name)."' ");
			if($is_exist->num_rows == 0){
				//$this->error['state'] = 'Please Enter Valid State';
			}
		}

		if($this->request->post['company'] == '' || $this->request->post['company_id'] == '' || $this->request->post['company_id'] == '0'){
			$this->error['company'] = 'Please Enter Valid Company';
		} else {
			$company_name = html_entity_decode($this->request->post['company']);
			$company_name = trim(preg_replace('/\s+/',' ',$company_name));
			$is_exist = $this->db->query("SELECT `company_id` FROM `oc_company` WHERE `company` = '".$this->db->escape($company_name)."' ");
			if($is_exist->num_rows == 0){
				$this->error['company'] = 'Please Enter Valid Company';
			}
		}

		if($this->request->post['contractor'] == '' || $this->request->post['contractor_id'] == '' || $this->request->post['contractor_id'] == '0'){
			$this->error['contractor'] = 'Please Enter Valid Contractor';
		} else {
			$contractor_name = html_entity_decode($this->request->post['contractor']);
			$contractor_name = trim(preg_replace('/\s+/',' ',$company_name));
			$is_exist = $this->db->query("SELECT `contractor_id` FROM `oc_contractor` WHERE `contractor_id` = '".$this->db->escape($this->request->post['contractor_id'])."' ");
			if($is_exist->num_rows == 0){
				$this->error['contractor'] = 'Please Enter Valid Contractor';
			}
		}

		if($this->request->post['grade'] == '' || $this->request->post['grade_id'] == '' || $this->request->post['grade_id'] == '0'){
			$this->error['grade'] = 'Please Enter Valid Grade';
		} else {
			$grade_name = html_entity_decode($this->request->post['grade']);
			$grade_name = trim(preg_replace('/\s+/',' ',$grade_name));
			$is_exist = $this->db->query("SELECT `grade_id` FROM `oc_grade` WHERE `g_name` = '".$this->db->escape($grade_name)."' ");
			// echo '<pre>';
			// print_r($grade_name);
			// echo '<pre>';
			// print_r($is_exist);
			// exit;
			if($is_exist->num_rows == 0){
				$this->error['grade'] = 'Please Enter Valid Grade';
			}
		}

		if($this->request->post['employement_id'] == '' || $this->request->post['employement_id'] == '0'){
			$this->error['employement'] = 'Please Enter Valid Employement';
		} else {
			$employement_name = html_entity_decode($this->request->post['employement']);
			$employement_name = trim(preg_replace('/\s+/',' ',$employement_name));
			$is_exist = $this->db->query("SELECT `employement_id` FROM `oc_employement` WHERE `employement` = '".$this->db->escape($employement_name)."' ");
			if($is_exist->num_rows == 0){
				$this->error['employement'] = 'Please Enter Valid Grade';
			}
		}

		if($this->request->post['region'] == '' || $this->request->post['region_id'] == '' || $this->request->post['region_id'] == '0'){
			//$this->error['region'] = 'Please Enter Valid Region';
		} else {
			$region_name = html_entity_decode($this->request->post['region']);
			$region_name = trim(preg_replace('/\s+/',' ',$region_name));
			$is_exist = $this->db->query("SELECT `region_id` FROM `oc_region` WHERE `region` = '".$this->db->escape($region_name)."' ");
			if($is_exist->num_rows == 0){
				//$this->error['region'] = 'Please Enter Valid Region';
			}
		}

		$datas = $this->request->post;

		if(isset($this->request->get['employee_id'])){
			if($datas['region_id'] != $datas['hidden_region_id']){
				if(!isset($this->error['division'])){
					if($datas['division_id'] == $datas['hidden_division_id']){
						$this->error['division'] = 'Please Change the Division';		
					}
				}
				
				if(!isset($this->error['site'])){
					if($datas['unit_id'] == $datas['hidden_unit_id']){
						$this->error['site'] = 'Please Change the Site';		
					}
				}
			}
			if($datas['division_id'] != $datas['hidden_division_id']){
				if(!isset($this->error['site'])){
					if($datas['unit_id'] == $datas['hidden_unit_id']){
						$this->error['site'] = 'Please Change the Site';		
					}
				}
			}
		}

		if($this->request->post['change_date'] == '' || $this->request->post['change_date'] == '00-00-0000'){
			$this->error['cdate'] = 'Please Enter Valid Changed Date';
		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/employee_change')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/employee');

		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $employee_id) {
		// 		$employee_total = $this->model_catalog_employee->getTotaltreatmentByemployeeId($employee_id);

		// 		if ($employee_total) {
		// 			$this->error['warning'] = sprintf($this->language->get('error_employee'), $employee_total);
		// 		}	
		// 	}
		// } elseif(isset($this->request->get['employee_id'])){
		// 	$employee_total = $this->model_catalog_employee->getTotaltreatmentByemployeeId($this->request->get['employee_id']);

		// 	if ($employee_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_employee'), $employee_total);
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}  
	}
}
?>