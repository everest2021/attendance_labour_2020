<?php
// Heading
$_['heading_title']      = 'Employee';

// Text
$_['text_success']       = 'Success: You have modified employee!';

// Column
$_['column_name']        = 'Employee Name';
$_['column_trainer']        = 'Trainer';
$_['column_action']      = 'Action';

$_['button_filter']      = 'Filter';

// Entry
$_['entry_name']         = 'Employee Name:';
$_['entry_trainer']       = 'Trainer Name:';
$_['entry_dob']       	 = 'Date of Birth:';
$_['entry_doj']       	 = 'Date Of Joining:';
$_['entry_owner']        = 'select owner';
$_['entry_share']        = 'Entry Share';
$_['entry_action']       = 'Action';
$_['entry_assign']       = 'Assign Owner';
$_['entry_remove']       = 'Remove';

$_['text_delete']        = 'Delete';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify Employee!';
$_['error_name']         = 'Employee Name must be between 3 and 64 characters!';
$_['error_trainer']      = 'Please Enter Trainer Name!';
$_['error_owner']      	 = 'Please Assign Owners!';
$_['error_trainer_exist']  = 'Please Enter Valid Trainer Name!';
$_['error_share']      	 = 'Share Total Should match 100!';
$_['error_owner_name']  = 'Please Enter Owner name!';
$_['error_share_name']  = 'Please Enter Share!';
$_['error_owner_exist']  = 'Please Enter Valid Owner Name!';
$_['error_employee']      = 'Warning: This Employee cannot be deleted as it is currently assigned to %s Invoices!';
?>
