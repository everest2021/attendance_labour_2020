<?php
session_start();
if($_SERVER['HTTP_HOST'] == 'localhost:4370'){
	$response = 1;
}elseif($_SERVER['HTTP_HOST'] == '10.200.1.35:4370'){
	$response = 2;
} elseif($_SERVER['HTTP_HOST'] == '124.124.8.44:4370') {
	$response = 3;
} elseif($_SERVER['HTTP_HOST'] == 'localhost:8012'){
	$response = 4;
} else {
	$response = 5;
}
date_default_timezone_set("Asia/Kolkata");
if ($response == 1 || $response == 5){
	// HTTP
	define('HTTP_SERVER', 'http://localhost:4370/labour/admin/');
	define('HTTP_CATALOG', 'http://localhost:4370/labour/');

	// HTTPS
	define('HTTPS_SERVER', 'http://localhost:4370/labour/admin/');
	define('HTTPS_CATALOG', 'http://localhost:4370/labour/');
} elseif($response == 2){
	// HTTP
	define('HTTP_SERVER', 'http://10.200.1.35:4370/labour/admin/');
	define('HTTP_CATALOG', 'http://10.200.1.35:4370/labour/');

	// HTTPS
	define('HTTPS_SERVER', 'http://10.200.1.35:4370/labour/admin/');
	define('HTTPS_CATALOG', 'http://10.200.1.35:4370/labour/');
} elseif($response == 3){
	// HTTP
	define('HTTP_SERVER', 'http://124.124.8.44:4370/labour/admin/');
	define('HTTP_CATALOG', 'http://124.124.8.44:4370/labour/');

	// HTTPS
	define('HTTPS_SERVER', 'http://124.124.8.44:4370/labour/admin/');
	define('HTTPS_CATALOG', 'http://124.124.8.44:4370/labour/');
} elseif($response == 4){
	// HTTP
	define('HTTP_SERVER', 'http://localhost:8012/labour/admin/');
	define('HTTP_CATALOG', 'http://localhost:8012/labour/');

	// HTTPS
	define('HTTPS_SERVER', 'http://localhost:8012/labour/admin/');
	define('HTTPS_CATALOG', 'http://localhost:8012/labour/');
}
// DIR
if($response == 4){
	define('DIR_APPLICATION', 'C:\xampp\htdocs\attendance_labour_2020\admin/');
	define('DIR_SYSTEM', 'C:\xampp\htdocs\attendance_labour_2020\system/');
	define('DIR_DATABASE', 'C:\xampp\htdocs\attendance_labour_2020\system/database/');
	define('DIR_LANGUAGE', 'C:\xampp\htdocs\attendance_labour_2020\admin/language/');
	define('DIR_TEMPLATE', 'C:\xampp\htdocs\attendance_labour_2020\admin/view/template/');
	define('DIR_CONFIG', 'C:\xampp\htdocs\attendance_labour_2020\system/config/');
	define('DIR_IMAGE', 'C:\xampp\htdocs\attendance_labour_2020\image/');
	define('DIR_CACHE', 'C:\xampp\htdocs\attendance_labour_2020\system/cache/');
	define('DIR_DOWNLOAD', 'C:\xampp\htdocs\attendance_labour_2020\download/');
	define('DIR_LOGS', 'C:\xampp\htdocs\attendance_labour_2020\system/logs/');
	define('DIR_CATALOG', 'C:\xampp\htdocs\attendance_labour_2020\catalog/');

	// DB
	define('DB_DRIVER', 'mysqli');
	define('DB_HOSTNAME', 'localhost');
	define('DB_USERNAME', 'root');
	define('DB_PASSWORD', '');
	define('DB_DATABASE', 'db_attendance_jmc_labour');
	define('DB_PREFIX', 'oc_');
} else {
	define('DIR_APPLICATION', 'C:\inetpub\wwwroot\attendance_labour_2020\admin/');
	define('DIR_SYSTEM', 'C:\inetpub\wwwroot\attendance_labour_2020\system/');
	define('DIR_DATABASE', 'C:\inetpub\wwwroot\attendance_labour_2020\system/database/');
	define('DIR_LANGUAGE', 'C:\inetpub\wwwroot\attendance_labour_2020\admin/language/');
	define('DIR_TEMPLATE', 'C:\inetpub\wwwroot\attendance_labour_2020\admin/view/template/');
	define('DIR_CONFIG', 'C:\inetpub\wwwroot\attendance_labour_2020\system/config/');
	define('DIR_IMAGE', 'C:\inetpub\wwwroot\attendance_labour_2020\image/');
	define('DIR_CACHE', 'C:\inetpub\wwwroot\attendance_labour_2020\system/cache/');
	define('DIR_DOWNLOAD', 'C:\inetpub\wwwroot\attendance_labour_2020\download/');
	define('DIR_LOGS', 'C:\inetpub\wwwroot\attendance_labour_2020\system/logs/');
	define('DIR_CATALOG', 'C:\inetpub\wwwroot\attendance_labour_2020\catalog/');

	// DB
	define('DB_DRIVER', 'mysqli');
	define('DB_HOSTNAME', 'localhost');
	define('DB_USERNAME', 'root');
	define('DB_PASSWORD', 'JmC@2018');
	define('DB_DATABASE', 'db_attendance_jmc_labour');
	define('DB_PREFIX', 'oc_');
}
?>