<?php
class ModelReportAttendance extends Model {
	public function getAttendance($data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) = '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}
		if (!empty($data['division'])) {
			$sql .= " AND LOWER(`division`) = '" . $this->db->escape(strtolower($data['division'])) . "'";
		}
		if (!empty($data['region'])) {
			$sql .= " AND LOWER(`region`) = '" . $this->db->escape(strtolower($data['region'])) . "'";
		}
		if (!empty($data['group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
		}
		if (!empty($data['company'])) {
			$company_string = "'" . str_replace(",", "','", html_entity_decode($data['company'])) . "'";
			$sql .= " AND LOWER(`company_id`) IN (" . strtolower($company_string) . ") ";
		}

		$division_string = $this->user->getdivision();
		$region_string = $this->user->getregion();
		$site_string = $this->user->getsite();
		$company_string = $this->user->getCompanyId();
			
		if($company_string != ''){
			$company_string = "'" . str_replace(",", "','", html_entity_decode($company_string)) . "'";
			if (isset($data['company']) && !empty($data['company'])) {
			} else {
				$sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
			}
		}
		
		if($division_string != ''){
			$division_string = "'" . str_replace(",", "','", html_entity_decode($division_string)) . "'";
			if (isset($data['division']) && !empty($data['division'])) {
			} else {
				$sql .= " AND `division_id` IN (" . strtolower($division_string) . ") ";
			}
		}

		if($region_string != ''){
			$region_string = "'" . str_replace(",", "','", html_entity_decode($region_string)) . "'";
			if (isset($data['region']) && !empty($data['region'])) {
			} else {
				//$sql .= " AND `region_id` IN (" . strtolower($region_string) . ") ";
			}
		}

		if($site_string != ''){
			$site_string = "'" . str_replace(",", "','", html_entity_decode($site_string)) . "'";
			if (isset($data['unit']) && !empty($data['unit'])) {
			} else {
				$sql .= " AND `unit_id` IN (" . strtolower($site_string) . ") ";
			}
		}

		if ($data['status'] == 1) {
			$sql .= " AND (`present_status` = '1' OR `present_status` = '0.5' OR `weekly_off` <> '0' OR `holiday_id` <> '0' OR (`halfday_status` <> '0' AND `absent_status` = '0') OR `leave_status` <> '0' OR `compli_status` <> '0')  ";
		} elseif($data['status'] == 2) {
			$sql .= " AND (`absent_status` = '1' OR `absent_status` = '0.5')";
		} else {
			//$sql .= " AND (`present_status` = '1' OR `present_status` = '0.5' OR  `weekly_off` <> '0' OR `holiday_id` <> '0' OR `halfday_status` <> '0' OR `leave_status` <> '0')  ";
		}
		//$sql .= " AND `emp_id` = '21463' ";
		$sql .= ' ORDER BY `act_intime` DESC';	
		//echo $sql;exit;	
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getclose_status($data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) = '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}
		if (!empty($data['group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
		}
		$sql .= " AND `day_close_status` = 1";
		$sql .= " ORDER BY `act_intime` DESC";	
		//echo $sql;exit;	
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTodaysAttendance($data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		$sql .= " AND DATE(`date`) = '".date('Y-m-d')."' AND `act_intime` <> '00:00:00' ";
		
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}
		if (!empty($data['group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
		}

		$sql .= ' ORDER BY `act_intime` DESC';
		//echo $sql;exit;		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTodaysAttendance_new($data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		$sql .= " AND DATE(`date`) = '".date('Y-m-d')."' AND `act_intime` <> '00:00:00' ";
		//$sql .= " AND DATE(`date`) = '2019-02-01' AND `act_intime` <> '00:00:00' ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape(strtolower($data['filter_name_id'])) . "'";
		}
		if (!empty($data['filter_unit'])) {
			$sql .= " AND (`unit_id`) = '" . $this->db->escape(($data['filter_unit'])) . "'";
		}
		if (!empty($data['filter_department'])) {
			$sql .= " AND (`department_id`) = '" . $this->db->escape(($data['filter_department'])) . "'";
		}
		if (!empty($data['filter_company'])) {
			$company_string = "'" . str_replace(",", "','", html_entity_decode($data['filter_company'])) . "'";
			$sql .= " AND (`company_id`) IN (" . ($company_string) . ") ";
		}
		if (!empty($data['filter_division'])) {
			$sql .= " AND (`division_id`) = '" . $this->db->escape(($data['filter_division'])) . "'";
		}
		if (!empty($data['region'])) {
			$sql .= " AND (`region_id`) = '" . $this->db->escape(($data['filter_region'])) . "'";
		}
		if (!empty($data['filter_contractor'])) {
			$contractor_string = "'" . str_replace(",", "','", html_entity_decode($data['filter_contractor'])) . "'";
			$sql .= " AND (`contractor_id`) IN (" . strtolower($contractor_string) . ") ";
		}

		$division_string = $this->user->getdivision();
		$region_string = $this->user->getregion();
		$site_string = $this->user->getsite();
		$company_string = $this->user->getCompanyId();
			
		if($company_string != ''){
			$company_string = "'" . str_replace(",", "','", html_entity_decode($company_string)) . "'";
			if (isset($data['filter_company']) && !empty($data['filter_company'])) {
			} else {
				$sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
			}
		}
		
		if($division_string != ''){
			$division_string = "'" . str_replace(",", "','", html_entity_decode($division_string)) . "'";
			if (isset($data['filter_division']) && !empty($data['filter_division'])) {
			} else {
				$sql .= " AND `division_id` IN (" . strtolower($division_string) . ") ";
			}
		}

		if($region_string != ''){
			$region_string = "'" . str_replace(",", "','", html_entity_decode($region_string)) . "'";
			if (isset($data['filter_region']) && !empty($data['filter_region'])) {
			} else {
				//$sql .= " AND `region_id` IN (" . strtolower($region_string) . ") ";
			}
		}

		if($site_string != ''){
			$site_string = "'" . str_replace(",", "','", html_entity_decode($site_string)) . "'";
			if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			} else {
				$sql .= " AND `unit_id` IN (" . strtolower($site_string) . ") ";
			}
		}
		$sql .= ' ORDER BY `act_intime` DESC';
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		//echo $sql;exit;		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getdepartment_list() {
		$sql = "SELECT `department` FROM `oc_employee` GROUP BY `department` ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getunit_list() {
		$sql = "SELECT * FROM `oc_unit` WHERE 1=1";
		//$sql = "SELECT `unit` FROM `oc_unit` GROUP BY `unit` ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getgroup_list() {
		$sql = "SELECT `group` FROM `oc_employee` GROUP BY `group` ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getNextDate($unit) {
		$query = $this->db->query("SELECT `date` FROM " . DB_PREFIX . "transaction WHERE `day_close_status` = '1' AND `unit` = '".$unit."' GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1");
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date'] .' +1 day'));	
		} else {
			return date('Y-m-d', strtotime('2016-11-21'));
		}
		
	}

	public function getNextDate_1() {
		$query = $this->db->query("SELECT `date` FROM " . DB_PREFIX . "transaction WHERE `month_close_status` = '1' GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1");
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date'] .' +1 day'));	
		} else {
			return date('Y-m-d', strtotime('2016-11-21'));
		}
		
	}
}
?>