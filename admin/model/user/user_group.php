<?php
class ModelUserUserGroup extends Model {
	public function addUserGroup($data) {
		$company_datas = $this->db->query("SELECT * FROM `oc_company` WHERE 1=1 ")->rows;
		foreach($company_datas as $ckey => $cvalue){
			$permission_datas = array();
			if(isset($data['permission']['access'][$cvalue['company_id']])){
				$permission_datas['access'] = $data['permission']['access'][$cvalue['company_id']];
			} else {
				$permission_datas['access'] = array();
			}
			if(isset($data['permission']['add'][$cvalue['company_id']])){
				$permission_datas['add'] = $data['permission']['add'][$cvalue['company_id']];
			} else {
				$permission_datas['add'] = array();
			}
			if(isset($data['permission']['modify'][$cvalue['company_id']])){
				$permission_datas['modify'] = $data['permission']['modify'][$cvalue['company_id']];
			} else {
				$permission_datas['modify'] = array();
			}	
			if(isset($data['permission']['delete'][$cvalue['company_id']])){
				$permission_datas['delete'] = $data['permission']['delete'][$cvalue['company_id']];
			} else {
				$permission_datas['delete'] = array();
			}
			// if($user_group_id == '1'){
			// 	$permission_datas['access'][] = 'snippets/snippet';
			// 	$permission_datas['access'][] = 'snippets/snippet_1';
			// 	$permission_datas['access'][] = 'user/user_permission';
			// }
			$permission_datas['access'][] = 'user/user';
			$permission_datas['access'][] = 'tool/error_log';
			$permission_datas['access'][] = 'catalog/in_process';
			$permission_datas['access'][] = 'common/home';
			$permission_datas['access'][] = 'transaction/transaction';
			$permission_datas['access'][] = 'transaction/transaction/generate_today';
			$permission_datas['access'][] = 'tool/recalculate';
			$permission_datas['access'][] = 'snippets/yearprocess';

			// if($user_group_id == '1'){
			// 	$permission_datas['add'][] = 'snippets/snippet';
			// 	$permission_datas['add'][] = 'snippets/snippet_1';
			// 	$permission_datas['add'][] = 'user/user_permission';
			// }
			$permission_datas['add'][] = 'user/user';
			$permission_datas['add'][] = 'tool/error_log';
			$permission_datas['add'][] = 'common/home';
			$permission_datas['add'][] = 'transaction/transaction';
			$permission_datas['add'][] = 'transaction/transaction/generate_today';
			$permission_datas['add'][] = 'tool/recalculate';
			$permission_datas['add'][] = 'snippets/yearprocess';

			// if($user_group_id == '1'){
			// 	$permission_datas['modify'][] = 'snippets/snippet';
			// 	$permission_datas['modify'][] = 'snippets/snippet_1';
			// 	$permission_datas['modify'][] = 'user/user_permission';
			// }
			$permission_datas['modify'][] = 'user/user';
			$permission_datas['modify'][] = 'tool/error_log';
			$permission_datas['modify'][] = 'common/home';
			$permission_datas['modify'][] = 'transaction/transaction';
			$permission_datas['modify'][] = 'transaction/transaction/generate_today';
			$permission_datas['modify'][] = 'tool/recalculate';
			$permission_datas['modify'][] = 'snippets/yearprocess';

			// if($user_group_id == '1'){
			// 	$permission_datas['delete'][] = 'snippets/snippet';
			// 	$permission_datas['delete'][] = 'snippets/snippet_1';
			// 	$permission_datas['delete'][] = 'user/user_permission';
			// }
			$permission_datas['delete'][] = 'user/user';
			$permission_datas['delete'][] = 'tool/error_log';
			$permission_datas['delete'][] = 'common/home';
			$permission_datas['delete'][] = 'transaction/transaction';
			$permission_datas['delete'][] = 'transaction/transaction/generate_today';
			$permission_datas['delete'][] = 'tool/recalculate';
			$permission_datas['delete'][] = 'snippets/yearprocess';
			//echo '<pre>';
			//print_r($permission_datas);
			//exit;
			$this->db->query("INSERT INTO " . DB_PREFIX . "user_group SET name = '" . $this->db->escape($data['name']) . "', permission = '" . (isset($permission_datas) ? serialize($permission_datas) : '') . "', `company_id` = '".$cvalue['company_id']."', `uid` = '".$data['uid']."' ");
		}
	}

	public function editUserGroup($user_group_id, $data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		$company_datas = $this->db->query("SELECT * FROM `oc_company` WHERE 1=1 ")->rows;
		foreach($company_datas as $ckey => $cvalue){
			$permission_datas = array();
			if(isset($data['permission']['access'][$cvalue['company_id']])){
				$permission_datas['access'] = $data['permission']['access'][$cvalue['company_id']];
			} else {
				$permission_datas['access'] = array();
			}
			if(isset($data['permission']['add'][$cvalue['company_id']])){
				$permission_datas['add'] = $data['permission']['add'][$cvalue['company_id']];
			} else {
				$permission_datas['add'] = array();
			}
			if(isset($data['permission']['modify'][$cvalue['company_id']])){
				$permission_datas['modify'] = $data['permission']['modify'][$cvalue['company_id']];
			} else {
				$permission_datas['modify'] = array();
			}	
			if(isset($data['permission']['delete'][$cvalue['company_id']])){
				$permission_datas['delete'] = $data['permission']['delete'][$cvalue['company_id']];
			} else {
				$permission_datas['delete'] = array();
			}
			if($user_group_id == '1'){
				$permission_datas['access'][] = 'snippets/snippet';
				$permission_datas['access'][] = 'snippets/snippet_1';
				$permission_datas['access'][] = 'snippets/weekoff';
				$permission_datas['access'][] = 'user/user_permission';
				$permission_datas['access'][] = 'tool/recalculate';
			}
			$permission_datas['access'][] = 'user/user';
			$permission_datas['access'][] = 'tool/error_log';
			$permission_datas['access'][] = 'catalog/in_process';
			$permission_datas['access'][] = 'common/home';
			$permission_datas['access'][] = 'transaction/transaction';
			$permission_datas['access'][] = 'transaction/transaction/generate_today';
			$permission_datas['access'][] = 'snippets/yearprocess';

			if($user_group_id == '1'){
				$permission_datas['add'][] = 'snippets/snippet';
				$permission_datas['add'][] = 'snippets/snippet_1';
				$permission_datas['add'][] = 'snippets/weekoff';
				$permission_datas['add'][] = 'user/user_permission';
				$permission_datas['add'][] = 'tool/recalculate';
			}
			$permission_datas['add'][] = 'user/user';
			$permission_datas['add'][] = 'tool/error_log';
			$permission_datas['add'][] = 'common/home';
			$permission_datas['add'][] = 'transaction/transaction';
			$permission_datas['add'][] = 'transaction/transaction/generate_today';
			$permission_datas['add'][] = 'snippets/yearprocess';

			if($user_group_id == '1'){
				$permission_datas['modify'][] = 'snippets/snippet';
				$permission_datas['modify'][] = 'snippets/snippet_1';
				$permission_datas['modify'][] = 'snippets/weekoff';
				$permission_datas['modify'][] = 'user/user_permission';
				$permission_datas['modify'][] = 'tool/recalculate';
			}
			$permission_datas['modify'][] = 'user/user';
			$permission_datas['modify'][] = 'tool/error_log';
			$permission_datas['modify'][] = 'common/home';
			$permission_datas['modify'][] = 'transaction/transaction';
			$permission_datas['modify'][] = 'transaction/transaction/generate_today';
			$permission_datas['modify'][] = 'snippets/yearprocess';

			if($user_group_id == '1'){
				$permission_datas['delete'][] = 'snippets/snippet';
				$permission_datas['delete'][] = 'snippets/snippet_1';
				$permission_datas['delete'][] = 'snippets/weekoff';
				$permission_datas['delete'][] = 'user/user_permission';
				$permission_datas['delete'][] = 'tool/recalculate';
			}
			$permission_datas['delete'][] = 'user/user';
			$permission_datas['delete'][] = 'tool/error_log';
			$permission_datas['delete'][] = 'common/home';
			$permission_datas['delete'][] = 'transaction/transaction';
			$permission_datas['delete'][] = 'transaction/transaction/generate_today';
			$permission_datas['delete'][] = 'snippets/yearprocess';
			// echo '<pre>';
			// print_r($permission_datas);
			// exit;
			$is_exist = $this->db->query("SELECT * FROM `oc_user_group` WHERE `company_id` = '".$cvalue['company_id']."' AND `uid` = '".$data['uid']."' ");
			if($is_exist->num_rows == 0){
				$this->db->query("INSERT INTO " . DB_PREFIX . "user_group SET name = '" . $this->db->escape($data['name']) . "', permission = '" . (isset($permission_datas) ? serialize($permission_datas) : '') . "', `company_id` = '".$cvalue['company_id']."', `uid` = '".$data['uid']."' ");
			} else {
				$user_group_id1 = $is_exist->row['user_group_id'];
				$this->db->query("UPDATE " . DB_PREFIX . "user_group SET name = '" . $this->db->escape($data['name']) . "', permission = '" . (isset($permission_datas) ? serialize($permission_datas) : '') . "', `company_id` = '".$cvalue['company_id']."', `uid` = '".$data['uid']."' WHERE user_group_id = '" . (int)$user_group_id1 . "' ");
			}
		}
		//exit;
	}

	public function deleteUserGroup($user_group_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");
	}

	public function addPermission($user_id, $type, $page) {
		$user_query = $this->db->query("SELECT DISTINCT user_group_id FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$user_id . "'");

		if ($user_query->num_rows) {
			$user_group_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

			if ($user_group_query->num_rows) {
				$data = unserialize($user_group_query->row['permission']);

				$data[$type][] = $page;

				$this->db->query("UPDATE " . DB_PREFIX . "user_group SET permission = '" . serialize($data) . "' WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");
			}
		}
	}

	public function getUserGroup($user_group_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");

		$user_group = array(
			'name'       => $query->row['name'],
			'uid'       => $query->row['uid'],
			'permission' => unserialize($query->row['permission'])
		);

		return $user_group;
	}

	public function getUserGroups($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "user_group WHERE `company_id` = '1' ";

		$sql .= " ORDER BY name";	

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalUserGroups() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "user_group");

		return $query->row['total'];
	}	
}
?>