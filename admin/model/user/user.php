<?php
class ModelUserUser extends Model {
	public function addUser($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "user` SET username = '" . $this->db->escape($data['username']) . "', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', user_group_id = '" . (int)$data['user_group_id'] . "', status = '" . (int)$data['status'] . "', date_added = NOW(), division = '" . $this->db->escape((isset($data['division'])) ? implode(',', $data['division']) : '') . "', region = '" . $this->db->escape((isset($data['region'])) ? implode(',', $data['region']) : '') . "', site = '" . $this->db->escape((isset($data['site'])) ? implode(',', $data['site']) : '') . "', device = '" . $this->db->escape((isset($data['device'])) ? implode(',', $data['device']) : '') . "', company_id = '" . $this->db->escape((isset($data['company_id'])) ? implode(',', $data['company_id']) : '') . "' ");
		//$this->db->query("INSERT INTO `" . DB_PREFIX . "user` SET username = '" . $this->db->escape($data['username']) . "', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', user_group_id = '" . (int)$data['user_group_id'] . "', status = '" . (int)$data['status'] . "', date_added = NOW(), division = '" . $this->db->escape((isset($data['division'])) ? implode(',', $data['division']) : '') . "', region = '" . $this->db->escape((isset($data['region'])) ? implode(',', $data['region']) : '') . "', site = '" . $this->db->escape((isset($data['site'])) ? implode(',', $data['site']) : '') . "', device = '" . $this->db->escape((isset($data['device'])) ? implode(',', $data['device']) : '') . "', company_id = '" . $this->db->escape($data['company_id']) . "' ");
		$user_id = $this->db->getLastId();
		// foreach($data['user_group_datas'] as $ukey => $uvalue){
		// 	$this->db->query("UPDATE `" . DB_PREFIX . "user` SET `".$ukey."` = '1' WHERE user_id = '" . (int)$user_id . "'");
		// }
	}

	public function editUser($user_id, $data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		if($this->user->getId() == '1'){
			$this->db->query("UPDATE `" . DB_PREFIX . "user` SET username = '" . $this->db->escape($data['username']) . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', user_group_id = '" . (int)$data['user_group_id'] . "', status = '" . (int)$data['status'] . "', division = '" . $this->db->escape((isset($data['division'])) ? implode(',', $data['division']) : '') . "', region = '" . $this->db->escape((isset($data['region'])) ? implode(',', $data['region']) : '') . "', site = '" . $this->db->escape((isset($data['site'])) ? implode(',', $data['site']) : '') . "', device = '" . $this->db->escape((isset($data['device'])) ? implode(',', $data['device']) : '') . "', company_id = '" . $this->db->escape((isset($data['company_id'])) ? implode(',', $data['company_id']) : '') . "' WHERE user_id = '" . (int)$user_id . "'");
			//$this->db->query("UPDATE `" . DB_PREFIX . "user` SET username = '" . $this->db->escape($data['username']) . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', user_group_id = '" . (int)$data['user_group_id'] . "', status = '" . (int)$data['status'] . "', division = '" . $this->db->escape((isset($data['division'])) ? implode(',', $data['division']) : '') . "', region = '" . $this->db->escape((isset($data['region'])) ? implode(',', $data['region']) : '') . "', site = '" . $this->db->escape((isset($data['site'])) ? implode(',', $data['site']) : '') . "', device = '" . $this->db->escape((isset($data['device'])) ? implode(',', $data['device']) : '') . "', company_id = '" . $this->db->escape($data['company_id']) . "' WHERE user_id = '" . (int)$user_id . "'");
		} else {
			$this->db->query("UPDATE `" . DB_PREFIX . "user` SET username = '" . $this->db->escape($data['username']) . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "' WHERE user_id = '" . (int)$user_id . "'");
		}
		//$this->db->query("UPDATE `" . DB_PREFIX . "user` SET `add` = '0', `edit` = '0', `view` = '0' WHERE user_id = '" . (int)$user_id . "'");
		// foreach($data['user_group_datas'] as $ukey => $uvalue){
		// 	$this->db->query("UPDATE `" . DB_PREFIX . "user` SET `".$ukey."` = '1' WHERE user_id = '" . (int)$user_id . "'");
		// }
		if ($data['password']) {
			$this->db->query("UPDATE `" . DB_PREFIX . "user` SET salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' WHERE user_id = '" . (int)$user_id . "'");
		}
	}

	public function editPassword($user_id, $password) {
		$this->db->query("UPDATE `" . DB_PREFIX . "user` SET salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', code = '' WHERE user_id = '" . (int)$user_id . "'");
	}

	public function editUser_pass($emp_code, $data) {
		if ($data['password']) {
			$this->db->query("UPDATE `" . DB_PREFIX . "employee` SET salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', is_set = '1' WHERE emp_code = '" . (int)$emp_code . "'");
		}
	}

	public function editCode($email, $code) {
		$this->db->query("UPDATE `" . DB_PREFIX . "user` SET code = '" . $this->db->escape($code) . "' WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function deleteUser($user_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "user` WHERE user_id = '" . (int)$user_id . "'");
	}

	public function getUser($user_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "user` WHERE user_id = '" . (int)$user_id . "'");

		return $query->row;
	}

	public function getemployee($employee_code) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "employee WHERE emp_code = '" . (int)$employee_code . "'");
		return $query->row;
	}

	public function getUserByUsername($username) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "user` WHERE username = '" . $this->db->escape($username) . "'");

		return $query->row;
	}

	public function getUserByCode($code) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "user` WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

		return $query->row;
	}

	public function getUsers($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "user` WHERE 1=1 ";

		if (isset($data['user_id']) && $data['user_id'] != 0) {
			$sql .= "AND user_id = '".$data['user_id']."' ";
		}

		$user_id = $this->user->getId();
		$in = 0;
		if($user_id != '1'){
			$in = 1;
		} else {
			$in = 0;
		}
		if($in == 1){
			$sql .= " AND `user_id` = '".$user_id."' ";
		}

		$sort_data = array(
			'username',
			'status',
			'date_added'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY username";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalUsers($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user` WHERE 1=1 ";

		if (isset($data['user_id']) && $data['user_id'] != 0) {
			$sql .= "AND user_id = '".$data['user_id']."' ";
		}

		$user_id = $this->user->getId();
		$in = 0;
		if($user_id != '1'){
			$in = 1;
		} else {
			$in = 0;
		}
		if($in == 1){
			$sql .= " AND `user_id` = '".$user_id."' ";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getTotalUsersByGroupId($user_group_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user` WHERE user_group_id = '" . (int)$user_group_id . "'");

		return $query->row['total'];
	}

	public function getTotalUsersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user` WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}	
}
?>