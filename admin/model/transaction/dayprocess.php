<?php
class ModelTransactionDayprocess extends Model {
	
	public function getNextDate($unit) {
		$sql = "SELECT `date` FROM " . DB_PREFIX . "transaction WHERE `day_close_status` = '1' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$sql .= " GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1 ";
		$query = $this->db->query($sql);
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date'] .' +1 day'));	
		} else {
			return date('Y-m-d', strtotime('2016-11-21'));
		}
		
	}

	public function getData($date, $unit) {
		//echo "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `unit` = '".$unit."' AND `abnormal_status` = '1' ORDER BY `date` ASC";
		//exit; 
		$sql = "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `abnormal_status` = '1' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$sql .= " ORDER BY `date` ASC";
		$query = $this->db->query($sql);
		if(isset($query->rows)) {
			return $query->rows;	
		} else {
			return 0;
		}
		
	}

	public function update_close_day($date, $unit) {
		$sql = "UPDATE " . DB_PREFIX . "transaction SET `day_close_status` = '1' WHERE `date` = '".$date."'";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$this->db->query($sql);
	}

	public function getAbsent($date, $unit) {
		//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `unit` = '".$unit."' AND `emp_id` = '21463' AND `absent_status` = '1' ORDER BY `date` ASC");
		$sql = "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `absent_status` = '1' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$sql .= " ORDER BY `date` ASC";
		$query = $this->db->query($sql);
		if(isset($query->rows)) {
			return $query->rows;	
		} else {
			return 0;
		}
	}

	public function is_closed_stat($date, $emp_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `day_close_status` = '1' ");
		//echo "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `day_close_status` = '1' ";
		if($query->num_rows > 0) {
			return 1;	
		} else {
			return 0;
		}
	}

	public function checkLeave($date, $emp_id) {
		$this->load->model('transaction/transaction');
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."leave_transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' AND `p_status` = '0' AND `a_status` = '1' ");
		//$query = $this->db->query("SELECT * FROM ".DB_PREFIX."leave_transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' AND `a_status` = '1' ");
		if($query->num_rows > 0) {
			$this->log->write('----start---');
			if($query->row['type'] != ''){
				//echo 'aaaa';exit;
				//$sql = "SELECT * FROM `oc_leave` WHERE `emp_id` = '".$emp_id."' AND `close_status` = '0' ";
				//$query1 = $this->db->query($sql);
				$trans_data = $this->db->query("SELECT * FROM ".DB_PREFIX."transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' ")->row;
				if($query->row['type'] == 'F'){
					$this->db->query("UPDATE `" . DB_PREFIX . "transaction` SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
					$this->log->write("UPDATE `" . DB_PREFIX . "transaction` SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
					
					$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");
				
					$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");
					
				} elseif($query->row['type'] == '1'){
					$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					if($trans_data['halfday_status'] != 0){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] == 'COF'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] != '1' && $trans_data['secondhalf_status'] != '0'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] == '1'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] == '0'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} 
					$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");
					
					$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");
				
				} elseif($query->row['type'] == '2'){
					$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					if($trans_data['halfday_status'] != 0){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] == 'COF'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] != '1' && $trans_data['firsthalf_status'] != '0'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] == '1'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] == '0'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					}
					$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");

					$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");
				}
			} else {
				$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
				$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
				
				$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
				$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
				
				$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");	
				$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");	
			}
			$this->log->write('----end---');
			//echo 'out';exit;
			//return $query->rows;	
		} else {
			return 0;
		}	
	}

	public function checkLeave_1($date, $emp_id) {
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."leave_transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' AND `a_status` = '1' AND `p_status` = '1' ");
		if($query->num_rows > 0) {
			$this->log->write('----start---');
			if($query->row['type'] != ''){
				$trans_data = $this->db->query("SELECT * FROM ".DB_PREFIX."transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' ")->row;
				if($query->row['type'] == 'F'){
					$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
					$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
				} elseif($query->row['type'] == '1'){
					$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					if($trans_data['halfday_status'] != 0){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] == 'COF'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] != '1' && $trans_data['secondhalf_status'] != '0'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] == '1'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] == '0'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} 
				} elseif($query->row['type'] == '2'){
					$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					if($trans_data['halfday_status'] != 0){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] == 'COF'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] != '1' && $trans_data['firsthalf_status'] != '0'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] == '1'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] == '0'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					}
				}
			} else {
				$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
				$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
			}
			$this->log->write('----end---');
			//echo 'out';exit;
			//return $query->rows;	
		} else {
			return 0;
		}
		//echo 'out';exit;	
	}

}	

?>