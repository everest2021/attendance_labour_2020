<?php
class ModelTransactionLeaveprocess extends Model {
	
	public function getNextDate($unit) {
		$query = $this->db->query("SELECT `date` FROM " . DB_PREFIX . "transaction WHERE `day_close_status` = '0' AND `unit` = '".$unit."' ORDER BY `date` ASC LIMIT 0,1");
		if(isset($query->rows[0]['date'])) {
			return $query->rows[0]['date'];	
		} else {
			return 0;
		}
		
	}

	public function getData($date, $unit) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `unit` = '".$unit."' AND `abnormal_status` = '1' ORDER BY `date` ASC");
		if(isset($query->rows)) {
			return $query->rows;	
		} else {
			return 0;
		}
		
	}

	public function getUnprocessedLeaveTillDate($data = array()) {
		$sql = "SELECT lt.*, (SELECT e.name FROM `oc_employee` e WHERE e.emp_code = lt.emp_id ) AS ename FROM " . DB_PREFIX . "leave_transaction lt WHERE lt.p_status = '0' AND (lt.a_status = '1') ";
		if(!empty($data['unit'])){
			$sql .= " AND lt.unit_id = '".$data['unit']."' ";
		}
		if(!empty($data['region'])){
			$sql .= " AND lt.region_id = '".$data['region']."' ";
		}
		if(!empty($data['dvision'])){
			$sql .= " AND lt.division_id = '".$data['division']."' ";
		}
		if(!empty($data['department'])){
			$sql .= " AND lt.department_id = '".$data['department']."' ";
		}
		if(!empty($data['company'])){
			$sql .= " AND lt.company_id = '".$data['company']."' ";
		}
		$sql .= " ORDER BY `a_status` ASC";
		//echo $sql;exit;
		$data = $this->db->query($sql);
		if($data->rows) {
			return $data->rows;
		} else {
			return array();
		}
	}

	public function getUnprocessedLeaveTillDate_2($data = array()) {
		$sql = "SELECT lt.*, (SELECT e.name FROM `oc_employee` e WHERE e.emp_code = lt.emp_id ) AS ename FROM " . DB_PREFIX . "leave_transaction lt WHERE lt.p_status = '0' AND (lt.a_status = '1') ";
		//$sql = "SELECT lt.*, (SELECT e.name FROM `oc_employee` e WHERE e.emp_code = lt.emp_id ) AS ename FROM " . DB_PREFIX . "leave_transaction lt WHERE lt.a_status = '1' AND `date` >= '2017-09-01' ";
		if(!empty($data['unit'])){
			$sql .= " AND lt.unit_id = '".$data['unit']."' ";
		}
		if(!empty($data['region'])){
			$sql .= " AND lt.region_id = '".$data['region']."' ";
		}
		if(!empty($data['dvision'])){
			$sql .= " AND lt.division_id = '".$data['division']."' ";
		}
		if(!empty($data['department'])){
			$sql .= " AND lt.department_id = '".$data['department']."' ";
		}
		if(!empty($data['company'])){
			$sql .= " AND lt.company_id = '".$data['company']."' ";
		}
		//$sql .= " AND lt.emp_id = '50750007' ";
		//echo $sql;exit;
		$data = $this->db->query($sql);
		if($data->rows) {
			return $data->rows;
		} else {
			return array();
		}

	}

	public function getUnprocessedLeaveTillDate_1() {
		$sql = "SELECT lt.*, (SELECT e.name FROM `oc_employee` e WHERE e.emp_code = lt.emp_id ) AS ename FROM " . DB_PREFIX . "leave_transaction lt WHERE lt.p_status = '1' AND lt.a_status = '1' ";
		$data = $this->db->query($sql);
		if($data->rows) {
			return $data->rows;
		} else {
			return array();
		}

	}

	public function is_closed_stat($date, $emp_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `day_close_status` = '1' ");
		if($query->num_rows > 0) {
			return 1;	
		} else {
			return 0;
		}
		
	}

	public function update_close_day($date, $unit) {
		$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `day_close_status` = '1' WHERE `date` = '".$date."' AND `unit` = '".$unit."'");
	}

	public function getAbsent($date, $unit) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `unit` = '".$unit."' AND `absent_status` = '1' ORDER BY `date` ASC");
		if(isset($query->rows)) {
			return $query->rows;	
		} else {
			return 0;
		}
	}

	public function checkLeave($date, $emp_id) {
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."leave_transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."'");
		if($query->rows) {
			$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `leave_status` = '1', absent_status = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
			$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
			$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
			//return $query->rows;	
		} else {
			return 0;
		}	
	}

}	

?>
