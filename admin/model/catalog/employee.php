<?php
class ModelCatalogEmployee extends Model {
	public function addemployee($data) {
		if($data['dob'] != '00-00-0000' && $data['dob'] != ''){
			$data['dob'] = date('Y-m-d', strtotime($data['dob']));
		} else {
			$data['dob'] = '0000-00-00';
		}

		if($data['doj'] != '00-00-0000' && $data['doj'] != ''){
			$data['doj'] = date('Y-m-d', strtotime($data['doj']));
			$doj = date('Y-m-d', strtotime($data['doj']));
		} else {
			$data['doj'] = '0000-00-00';
			$doj = '0000-00-00';
		}

		if($data['doc'] != '00-00-0000' && $data['doc'] != ''){
			$data['doc'] = date('Y-m-d', strtotime($data['doc']));
		} else {
			$data['doc'] = '0000-00-00';
		}

		if($data['dol'] != '00-00-0000' && $data['dol'] != ''){
			$data['dol'] = date('Y-m-d', strtotime($data['dol']));
		} else {
			$data['dol'] = '0000-00-00';
		}
		//echo '<pre>';print_r($data);exit;
		$contractor_code = $this->db->query("SELECT `contractor_code` FROM `oc_contractor` WHERE `contractor_id` = '".$this->db->escape($data['contractor_id'])."'")->row['contractor_code'];
		$data['contractor_code'] = $contractor_code;
		//echo $contractor_code;exit;
		$data['name'] = trim($data['name']," ");
		$this->db->query("INSERT INTO " . DB_PREFIX . "employee SET 
							name = '" . $this->db->escape($data['name']) . "', 
							emp_code = '" . $this->db->escape($data['emp_code']) . "', 
							gender = '" . $this->db->escape($data['gender']) . "', 
							dob = '" . $this->db->escape($data['dob']) . "', 
							doj = '" . $this->db->escape($data['doj']) . "',  
							doc = '" . $this->db->escape($data['doc']) . "',  
							employement = '" . $this->db->escape(html_entity_decode($data['employement'])) . "',  
							employement_id = '" . $this->db->escape($data['employement_id']) . "',  
							grade = '" . $this->db->escape(html_entity_decode($data['grade'])) . "', 
							grade_id = '" . $this->db->escape($data['grade_id']) . "',
							dol = '" . $this->db->escape($data['dol']) . "',  
							designation = '" . $this->db->escape(html_entity_decode($data['designation'])) . "', 
							designation_id = '" . $this->db->escape($data['designation_id']) . "',
							department = '" . $this->db->escape(html_entity_decode($data['department'])) . "', 
							department_id = '" . $this->db->escape($data['department_id']) . "',
							unit = '" . $this->db->escape(html_entity_decode($data['unit'])) . "',  
							unit_id = '" . $this->db->escape($data['unit_id']) . "',
							division = '" . $this->db->escape(html_entity_decode($data['division'])) . "',  
							division_id = '" . $this->db->escape($data['division_id']) . "',  
							region = '" . $this->db->escape(html_entity_decode($data['region'])) . "',  
							region_id = '" . $this->db->escape($data['region_id']) . "',
							state = '" . $this->db->escape(html_entity_decode($data['state'])) . "',  
							state_id = '" . $this->db->escape($data['state_id']) . "',
							status = '" . $this->db->escape($data['status']) . "',
							company = '" . $this->db->escape(html_entity_decode($data['company'])) . "',  
							company_id = '" . $this->db->escape($data['company_id']) . "',
							contractor = '" . $this->db->escape(html_entity_decode($data['contractor'])) . "',  
							contractor_id = '" . $this->db->escape($data['contractor_id']) . "',
							contractor_code = '" .$contractor_code. "',
							category = '" . $this->db->escape(html_entity_decode($data['category'])) . "',  
							category_id = '" . $this->db->escape($data['category_id']) . "',
							shift_id = '" . $this->db->escape($data['shift_id']) . "',
							sat_status = '" . $this->db->escape($data['sat_status']) . "',
							shift_type = 'F',
							is_new = '0' ");
		$employee_id = $this->db->getLastId();
		$check_new_emp = $this->db->query("select emp_id,device_id from oc_attendance where `emp_id` = '".$data['emp_code']."' AND  is_new_employee = 1 group by emp_id");
		if($check_new_emp->num_rows > 0){
			$this->db->query("UPDATE `oc_attendance` SET `is_new_employee` = '0' WHERE `emp_id` = '".$check_new_emp->row['emp_id']."' ");
		}
		$emp_code = $data['emp_code'];
		$user_name = $emp_code;
		$salt = substr(md5(uniqid(rand(), true)), 0, 9);
		$password = sha1($salt . sha1($salt . sha1($emp_code)));
		$sql = "UPDATE `oc_employee` set `username` = '".$this->db->escape($emp_code)."', `password` = '".$this->db->escape($password)."', `salt` = '".$this->db->escape($salt)."', `is_set` = '0' WHERE `employee_id` = '".$employee_id."' ";
		$this->db->query($sql);

		$open_years = $this->db->query("SELECT `year` FROM `oc_leave` WHERE `close_status` = '0' GROUP BY `year` ORDER BY `year` DESC LIMIT 1");
		if($open_years->num_rows > 0){
			$open_year = $open_years->row['year'];
		} else {
			$open_year = date('Y');
		}

		$is_exist = $this->db->query("SELECT `emp_id` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `year` = '".$open_year."' ");
		if($is_exist->num_rows == 0){
			$insert2 = "INSERT INTO `oc_leave` SET 
						`emp_id` = '".$emp_code."', 
						`emp_name` = '".$this->db->escape($data['name'])."', 
						`emp_doj` = '".$data['doj']."', 
						`year` = '".$open_year."',
						`close_status` = '0' "; 
			$this->db->query($insert2);
			// echo $insert1;
			// echo '<br />';
			// exit;
		}
		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);
		$shift_id_data = 'S_'.$data['shift_id'];
		foreach ($months_array as $key => $value) {
			$insert1 = "INSERT INTO `oc_shift_schedule` SET 
				`emp_code` = '".$data['emp_code']."',
				`1` = '".$shift_id_data."',
				`2` = '".$shift_id_data."',
				`3` = '".$shift_id_data."',
				`4` = '".$shift_id_data."',
				`5` = '".$shift_id_data."',
				`6` = '".$shift_id_data."',
				`7` = '".$shift_id_data."',
				`8` = '".$shift_id_data."',
				`9` = '".$shift_id_data."',
				`10` = '".$shift_id_data."',
				`11` = '".$shift_id_data."',
				`12` = '".$shift_id_data."', 
				`13` = '".$shift_id_data."', 
				`14` = '".$shift_id_data."', 
				`15` = '".$shift_id_data."', 
				`16` = '".$shift_id_data."', 
				`17` = '".$shift_id_data."', 
				`18` = '".$shift_id_data."', 
				`19` = '".$shift_id_data."', 
				`20` = '".$shift_id_data."', 
				`21` = '".$shift_id_data."', 
				`22` = '".$shift_id_data."', 
				`23` = '".$shift_id_data."', 
				`24` = '".$shift_id_data."', 
				`25` = '".$shift_id_data."', 
				`26` = '".$shift_id_data."', 
				`27` = '".$shift_id_data."', 
				`28` = '".$shift_id_data."', 
				`29` = '".$shift_id_data."', 
				`30` = '".$shift_id_data."', 
				`31` = '".$shift_id_data."', 
				`month` = '".$key."',
				`year` = '".date('Y')."',
				`status` = '1' ,
				`unit` = '".$data['unit']."',
				`unit_id` = '".$data['unit_id']."' "; 
				// echo "<pre>";
				// echo $insert1;
			$this->db->query($insert1);
		}

		foreach ($months_array as $mkey => $mvalue) {
			if($mkey == '12'){
				$insert_query2 = "INSERT INTO `oc_shift_schedule` SET 
				`emp_code` = '".$data['emp_code']."',
				`1` = '".$shift_id_data."',
				`2` = '".$shift_id_data."',
				`3` = '".$shift_id_data."',
				`4` = '".$shift_id_data."',
				`5` = '".$shift_id_data."',
				`6` = '".$shift_id_data."',
				`7` = '".$shift_id_data."',
				`8` = '".$shift_id_data."',
				`9` = '".$shift_id_data."',
				`10` = '".$shift_id_data."',
				`11` = '".$shift_id_data."',
				`12` = '".$shift_id_data."', 
				`13` = '".$shift_id_data."', 
				`14` = '".$shift_id_data."', 
				`15` = '".$shift_id_data."', 
				`16` = '".$shift_id_data."', 
				`17` = '".$shift_id_data."', 
				`18` = '".$shift_id_data."', 
				`19` = '".$shift_id_data."', 
				`20` = '".$shift_id_data."', 
				`21` = '".$shift_id_data."', 
				`22` = '".$shift_id_data."', 
				`23` = '".$shift_id_data."', 
				`24` = '".$shift_id_data."', 
				`25` = '".$shift_id_data."', 
				`26` = '".$shift_id_data."', 
				`27` = '".$shift_id_data."', 
				`28` = '".$shift_id_data."', 
				`29` = '".$shift_id_data."', 
				`30` = '".$shift_id_data."', 
				`31` = '".$shift_id_data."', 
				`month` = '".$key."',
				`year` = '2019',
				`status` = '1' ,
				`unit` = '".$data['unit']."',
				`unit_id` = '".$data['unit_id']."' "; 
				// echo "<pre>";
				// echo $insert1;
				$this->db->query($insert_query2);
			}
		}

		$current_date = date('Y-m-d');
		$current_month = date('n');
		if($current_month == 1 || $current_month == 2 || $current_month == 3 || $current_month == 4 || $current_month == 5){
			//$start_date = date('Y-02-01');
			$back_year = date("Y") - 1;
			$start_date = $back_year.'-12-26';
		} else {
			$start_date = date('Y-m-d', strtotime($current_date . ' -60 day'));//'2017-06-26';
		}

		$end_date = date('Y-m-d', strtotime($current_date . ' +1 day'));//'2017-06-30'
		$day = array();
		$days = $this->GetDays($start_date, $end_date);
		foreach ($days as $dkey => $dvalue) {
			$dates = explode('-', $dvalue);
			$day[$dkey]['day'] = $dates[2];
			$day[$dkey]['date'] = $dvalue;
		}
		$batch_id = '0';
		foreach($day as $dkeys => $dvalues){
			$filter_date_start = $dvalues['date'];
			$results = $this->getemployee_code($emp_code);
			// echo '<pre>';
			// print_r($results);
			// exit;
			foreach ($results as $rkey => $rvalue) {
				if(isset($rvalue['name']) && $rvalue['name'] != ''){
					$emp_name = $rvalue['name'];
					$department = $rvalue['department'];
					$unit = $rvalue['unit'];
					$group = '';
					$day_date = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));
					$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
					//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
					$shift_schedule = $this->db->query($update3)->row;
					$schedule_raw = explode('_', $shift_schedule[$day_date]);
					if(!isset($schedule_raw[2])){
						$schedule_raw[2]= 1;
					}
					/*echo '<pre>';
					print_r($schedule_raw);
					exit;*/
					if($schedule_raw[0] == 'S'){
						$shift_data = $this->getshiftdata_data($schedule_raw[1]);
						if(isset($shift_data['shift_id'])){
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
							$shift_code = $shift_data['shift_code'];
							$shift_id = $shift_data['shift_id'];
							$day = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_id = '".$rvalue['contractor_id']."', contractor_code = '".$rvalue['contractor_code']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
								//echo $sql;exit;
								$this->db->query($sql);
							}
						} else {
							$shift_data = $this->getshiftdata_data('1');
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
							$shift_code = $shift_data['shift_code'];
							$shift_id = $shift_data['shift_id'];
							$day = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_id = '".$rvalue['contractor_id']."', contractor_code = '".$rvalue['contractor_code']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
								$this->db->query($sql);
							}
						}
					} elseif ($schedule_raw[0] == 'W') {
						$shift_data = $this->getshiftdata_data($schedule_raw[2]);
						if(!isset($shift_data['shift_id'])){
							$shift_data = $this->getshiftdata_data('1');
						}
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = $shift_data['out_time'];
						$shift_code = $shift_data['shift_code'];
						$shift_id = $shift_data['shift_id'];
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
						$trans_exist = $this->db->query($trans_exist_sql);
						if($trans_exist->num_rows == 0){
							$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_id = '".$rvalue['contractor_id']."', contractor_code = '".$rvalue['contractor_code']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
							$this->db->query($sql);
						}
					} elseif ($schedule_raw[0] == 'H') {
						$shift_data = $this->getshiftdata_data($schedule_raw[2]);
						if(!isset($shift_data['shift_id'])){
							$shift_data = $this->getshiftdata_data('1');
						}
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = $shift_data['out_time'];
						$shift_code = $shift_data['shift_code'];
						$shift_id = $shift_data['shift_id'];
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
						$trans_exist = $this->db->query($trans_exist_sql);
						if($trans_exist->num_rows == 0){
							$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_id = '".$rvalue['contractor_id']."', contractor_code = '".$rvalue['contractor_code']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
							$this->db->query($sql);
						}
					} elseif ($schedule_raw[0] == 'HD') {
						$shift_data = $this->getshiftdata_data($schedule_raw[2]);
						if(!isset($shift_data['shift_id'])){
							$shift_data = $this->getshiftdata_data('1');
						}
						$shift_intime = $shift_data['in_time'];
						if($shift_data['shift_id'] == '1'){
							if($rvalue['sat_status'] == '1'){
								$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
								$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
							} else {
								$shift_outtime = $shift_data['out_time'];
							}
						} else {
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
						}
						$shift_code = $shift_data['shift_code'];
						$shift_id = $shift_data['shift_id'];
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
						$trans_exist = $this->db->query($trans_exist_sql);
						if($trans_exist->num_rows == 0){
							$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_id = '".$rvalue['contractor_id']."', contractor_code = '".$rvalue['contractor_code']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
							$this->db->query($sql);
						}
					}
				}	
			}
		}
	}

	public function editemployee($employee_id, $data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		if($data['dob'] != '00-00-0000' && $data['dob'] != ''){
			$data['dob'] = date('Y-m-d', strtotime($data['dob']));
		} else {
			$data['dob'] = '0000-00-00';
		}

		if($data['doj'] != '00-00-0000' && $data['doj'] != ''){
			$data['doj'] = date('Y-m-d', strtotime($data['doj']));
			$doj = date('Y-m-d', strtotime($data['doj']));
		} else {
			$data['doj'] = '0000-00-00';
			$doj = '0000-00-00';
		}

		if($data['hidden_doj'] != '00-00-0000' && $data['hidden_doj'] != ''){
			$data['hidden_doj'] = date('Y-m-d', strtotime($data['hidden_doj']));
			$hidden_doj = date('Y-m-d', strtotime($data['hidden_doj']));
		} else {
			$data['hidden_doj'] = '0000-00-00';
			$hidden_doj = '0000-00-00';
		}

		if($data['doc'] != '00-00-0000' && $data['doc'] != ''){
			$data['doc'] = date('Y-m-d', strtotime($data['doc']));
		} else {
			$data['doc'] = '0000-00-00';
		}

		if($data['dol'] != '00-00-0000' && $data['dol'] != ''){
			$data['dol'] = date('Y-m-d', strtotime($data['dol']));
		} else {
			$data['dol'] = '0000-00-00';
		}

		if($data['hidden_dol'] != '00-00-0000' && $data['hidden_dol'] != ''){
			$data['hidden_dol'] = date('Y-m-d', strtotime($data['hidden_dol']));
		} else {
			$data['hidden_dol'] = '0000-00-00';
		}

		$data['name'] = trim($data['name']," ");
		if($data['hidden_status'] == '1' && $data['status'] == '0'){
			$from_date = '0000-00-00';
			$to_date = date('Y-m-d', strtotime($data['dol']));
			$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."',`contractor` = '".$this->db->escape(html_entity_decode($data['contractor']))."', `contractor_id` = '".$this->db->escape($data['contractor_id'])."' ";
			$this->db->query($sql);
		}

		$contractor_code = $this->db->query("SELECT `contractor_code` FROM `oc_contractor` WHERE `contractor_id` = '".$this->db->escape($data['contractor_id'])."'")->row['contractor_code'];
		$data['contractor_code'] = $contractor_code;

		if($data['change_date'] != '00-00-0000' && $data['change_date'] != ''){
			if($data['region_id'] != $data['hidden_region_id']){
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Region' AND `current_value_id` = '".$data['hidden_region_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Region', `value` = '".$this->db->escape(html_entity_decode($data['hidden_region']))."', `value_id` = '".$this->db->escape($data['hidden_region_id'])."', `current_value_id` = '".$this->db->escape($data['region_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."',`contractor` = '".$this->db->escape(html_entity_decode($data['contractor']))."', `contractor_id` = '".$this->db->escape($data['contractor_id'])."' ";
				$this->db->query($sql);

				$sql = "UPDATE `oc_transaction` SET `region_id` = '".$data['region_id']."', `region` = '".$this->db->escape($data['region'])."' WHERE `region_id` = '".$data['hidden_region_id']."' AND  `date` >= '".$to_date."' AND `emp_id` = '".$data['emp_code']."' ";
				$this->db->query($sql);				
			}
			if($data['division_id'] != $data['hidden_division_id']){
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Division' AND `current_value_id` = '".$data['hidden_division_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Division', `value` = '".$this->db->escape(html_entity_decode($data['hidden_division']))."', `value_id` = '".$this->db->escape($data['hidden_division_id'])."', `current_value_id` = '".$this->db->escape($data['division_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."',`contractor` = '".$this->db->escape(html_entity_decode($data['contractor']))."', `contractor_id` = '".$this->db->escape($data['contractor_id'])."' ";
				$this->db->query($sql);

				$sql = "UPDATE `oc_transaction` SET `division_id` = '".$data['division_id']."', `division` = '".$this->db->escape($data['division'])."' WHERE `division_id` = '".$data['hidden_division_id']."' AND  `date` >= '".$to_date."' AND `emp_id` = '".$data['emp_code']."' ";
				$this->db->query($sql);
			}
			if($data['unit_id'] != $data['hidden_unit_id']){
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Unit' AND `current_value_id` = '".$data['hidden_unit_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Unit', `value` = '".$this->db->escape(html_entity_decode($data['hidden_unit']))."', `value_id` = '".$this->db->escape($data['hidden_unit_id'])."', `current_value_id` = '".$this->db->escape($data['unit_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."' ,`contractor` = '".$this->db->escape(html_entity_decode($data['contractor']))."', `contractor_id` = '".$this->db->escape($data['contractor_id'])."' ";
				$this->db->query($sql);

				$sql = "UPDATE `oc_transaction` SET `unit_id` = '".$data['unit_id']."', `unit` = '".$this->db->escape($data['unit'])."' WHERE `unit_id` = '".$data['hidden_unit_id']."' AND  `date` >= '".$to_date."' AND `emp_id` = '".$data['emp_code']."' ";
				$this->db->query($sql);

				$sql = "UPDATE `oc_attendance` SET `unit_id` = '".$data['unit_id']."' WHERE `unit_id` = '".$data['hidden_unit_id']."' AND `emp_id` = '".$data['emp_code']."' ";
				$this->db->query($sql);
			}
			if($data['designation_id'] != $data['hidden_designation_id']){
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Designation' AND `current_value_id` = '".$data['hidden_designation_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Designation', `value` = '".$this->db->escape(html_entity_decode($data['hidden_designation']))."', `value_id` = '".$this->db->escape($data['hidden_designation_id'])."', `current_value_id` = '".$this->db->escape($data['designation_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."',`contractor` = '".$this->db->escape(html_entity_decode($data['contractor']))."', `contractor_id` = '".$this->db->escape($data['contractor_id'])."' ";
				$this->db->query($sql);

				$sql = "UPDATE `oc_transaction` SET `designation_id` = '".$data['designation_id']."', `designation` = '".$this->db->escape($data['designation'])."' WHERE `designation_id` = '".$data['hidden_designation_id']."' AND  `date` >= '".$to_date."' AND `emp_id` = '".$data['emp_code']."' ";
				$this->db->query($sql);
			}
			if($data['contractor_id'] != $data['hidden_contractor_id']){
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Contractor' AND `current_value_id` = '".$data['hidden_contractor_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				//echo '<pre>';print_r($data);exit;
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Contractor', `value` = '".$this->db->escape(html_entity_decode($data['hidden_contractor']))."', `value_id` = '".$this->db->escape($data['hidden_contractor_id'])."', `current_value_id` = '".$this->db->escape($data['contractor_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."',`contractor` = '".$this->db->escape(html_entity_decode($data['contractor']))."', `contractor_id` = '".$this->db->escape($data['contractor_id'])."' ";
				$this->db->query($sql);

				$sql = "UPDATE `oc_transaction` SET `contractor_id` = '".$data['contractor_id']."', `contractor_code` = '".$this->db->escape($data['contractor_code'])."', `contractor` = '".$this->db->escape($data['contractor'])."' WHERE `contractor_id` = '".$data['hidden_contractor_id']."' AND `date` >= '".$to_date."' AND `emp_id` = '".$data['emp_code']."' ";
				$this->db->query($sql);
			}
			if($data['grade_id'] != $data['hidden_grade_id']){
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Grade' AND `current_value_id` = '".$data['hidden_grade_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Grade', `value` = '".$this->db->escape(html_entity_decode($data['hidden_grade']))."', `value_id` = '".$this->db->escape($data['hidden_grade_id'])."', `current_value_id` = '".$this->db->escape($data['grade_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."',`contractor` = '".$this->db->escape(html_entity_decode($data['contractor']))."', `contractor_id` = '".$this->db->escape($data['contractor_id'])."' ";
				$this->db->query($sql);
			}		
			if($data['department_id'] != $data['hidden_department_id']){
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Department' AND `current_value_id` = '".$data['hidden_department_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Department', `value` = '".$this->db->escape(html_entity_decode($data['hidden_department']))."', `value_id` = '".$this->db->escape($data['hidden_department_id'])."', `current_value_id` = '".$this->db->escape($data['department_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."',`contractor` = '".$this->db->escape(html_entity_decode($data['contractor']))."', `contractor_id` = '".$this->db->escape($data['contractor_id'])."' ";
				$this->db->query($sql);

				$sql = "UPDATE `oc_transaction` SET `department_id` = '".$data['department_id']."', `department` = '".$this->db->escape($data['department'])."' WHERE `department_id` = '".$data['hidden_department_id']."' AND  `date` >= '".$to_date."' AND `emp_id` = '".$data['emp_code']."' ";
				$this->db->query($sql);
			}
			if($data['employement_id'] != $data['hidden_employement_id']){
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Employment' AND `current_value_id` = '".$data['hidden_employement_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Employment', `value` = '".$this->db->escape(html_entity_decode($data['hidden_employement']))."', `value_id` = '".$this->db->escape($data['hidden_employement_id'])."', `current_value_id` = '".$this->db->escape($data['employement_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."',`contractor` = '".$this->db->escape(html_entity_decode($data['contractor']))."', `contractor_id` = '".$this->db->escape($data['contractor_id'])."' ";
				$this->db->query($sql);
			}
			if($data['company_id'] != $data['hidden_company_id']){
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Company' AND `current_value_id` = '".$data['hidden_company_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Company', `value` = '".$this->db->escape(html_entity_decode($data['hidden_company']))."', `value_id` = '".$this->db->escape($data['hidden_company_id'])."', `current_value_id` = '".$this->db->escape($data['company_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."',`contractor` = '".$this->db->escape(html_entity_decode($data['contractor']))."', `contractor_id` = '".$this->db->escape($data['contractor_id'])."' ";
				$this->db->query($sql);

				$sql = "UPDATE `oc_transaction` SET `company_id` = '".$data['company_id']."', `company` = '".$this->db->escape($data['company'])."' WHERE `company_id` = '".$data['hidden_company_id']."' AND  `date` >= '".$to_date."' AND `emp_id` = '".$data['emp_code']."' ";
				$this->db->query($sql);
			}
			if($data['category_id'] != $data['hidden_category_id']){
				$sql = "UPDATE `oc_transaction` SET `category_id` = '".$data['category_id']."', `category` = '".$this->db->escape($data['category'])."' WHERE `category_id` = '".$data['hidden_category_id']."' AND  `date` >= '".$to_date."' AND `emp_id` = '".$data['emp_code']."' ";
				$this->db->query($sql);
			}

		}
		//echo '<pre>';print_r($data);exit;
		//echo $is_exist;exit;	
		$this->db->query("UPDATE " . DB_PREFIX . "employee SET 
							name = '" . $this->db->escape($data['name']) . "', 
							emp_code = '" . $this->db->escape($data['emp_code']) . "', 
							gender = '" . $this->db->escape($data['gender']) . "', 
							dob = '" . $this->db->escape($data['dob']) . "', 
							doj = '" . $this->db->escape($data['doj']) . "',  
							doc = '" . $this->db->escape($data['doc']) . "',  
							employement = '" . $this->db->escape(html_entity_decode($data['employement'])) . "',  
							employement_id = '" . $this->db->escape($data['employement_id']) . "',  
							grade = '" . $this->db->escape(html_entity_decode($data['grade'])) . "', 
							grade_id = '" . $this->db->escape($data['grade_id']) . "',
							dol = '" . $this->db->escape($data['dol']) . "',  
							designation = '" . $this->db->escape(html_entity_decode($data['designation'])) . "', 
							designation_id = '" . $this->db->escape($data['designation_id']) . "',
							department = '" . $this->db->escape(html_entity_decode($data['department'])) . "', 
							department_id = '" . $this->db->escape($data['department_id']) . "',
							unit = '" . $this->db->escape(html_entity_decode($data['unit'])) . "',  
							unit_id = '" . $this->db->escape($data['unit_id']) . "',
							division = '" . $this->db->escape(html_entity_decode($data['division'])) . "',  
							division_id = '" . $this->db->escape($data['division_id']) . "',  
							region = '" . $this->db->escape(html_entity_decode($data['region'])) . "',  
							region_id = '" . $this->db->escape($data['region_id']) . "',
							state = '" . $this->db->escape(html_entity_decode($data['state'])) . "',  
							state_id = '" . $this->db->escape($data['state_id']) . "',
							status = '" . $this->db->escape($data['status']) . "',
							company = '" . $this->db->escape(html_entity_decode($data['company'])) . "',  
							company_id = '" . $this->db->escape($data['company_id']) . "',
							contractor = '" . $this->db->escape(html_entity_decode($data['contractor'])) . "',  
							contractor_id = '" . $this->db->escape($data['contractor_id']) . "',
							contractor_code = '" .$contractor_code. "',
							category = '" . $this->db->escape(html_entity_decode($data['category'])) . "',  
							category_id = '" . $this->db->escape($data['category_id']) . "',
							shift_id = '" . $this->db->escape($data['shift_id']) . "',
							sat_status = '" . $this->db->escape($data['sat_status']) . "'
							WHERE employee_id = '" . (int)$employee_id . "'");
		
		if($data['emp_code'] != $data['hidden_emp_code']){
			$shift_schedule_sql = $this->db->query("UPDATE `oc_shift_schedule` SET `emp_code` = '".$data['emp_code']."' WHERE `emp_code` = '".$data['hidden_emp_code']."' ");
			$leave_sql = $this->db->query("UPDATE `oc_leave` SET `emp_id` = '".$data['emp_code']."' WHERE `emp_id` = '".$data['hidden_emp_code']."' ");
			$leave_emp_sql = $this->db->query("UPDATE `oc_leave_employee` SET `emp_id` = '".$data['emp_code']."' WHERE `emp_id` = '".$data['hidden_emp_code']."' ");
			$leave_transaction_sql = $this->db->query("UPDATE `oc_leave_transaction` SET `emp_id` = '".$data['emp_code']."' WHERE `emp_id` = '".$data['hidden_emp_code']."' ");
			$transaction_sql = $this->db->query("UPDATE `oc_transaction` SET `emp_id` = '".$data['emp_code']."' WHERE `emp_id` = '".$data['hidden_emp_code']."' ");
		}

		$check_new_emp = $this->db->query("select emp_id,device_id from oc_attendance where `emp_id` = '".$data['emp_code']."' AND  is_new_employee = 1 group by emp_id");
		if($check_new_emp->num_rows > 0){
			$this->db->query("UPDATE `oc_attendance` SET `is_new_employee` = '0' WHERE `emp_id` = '".$check_new_emp->row['emp_id']."' ");
		}

		$emp_code = $data['emp_code'];
		if($data['password_reset'] == '1'){
			$user_name = $emp_code;
			$salt = substr(md5(uniqid(rand(), true)), 0, 9);
			$password = sha1($salt . sha1($salt . sha1($emp_code)));

			$sql = "UPDATE `oc_employee` set `username` = '".$this->db->escape($emp_code)."', `password` = '".$this->db->escape($password)."', `salt` = '".$this->db->escape($salt)."', `is_set` = '0' WHERE `employee_id` = '".$employee_id."' ";
			$this->db->query($sql);
		}

		$open_years = $this->db->query("SELECT `year` FROM `oc_leave` WHERE `close_status` = '0' GROUP BY `year` ORDER BY `year` DESC LIMIT 1");
		if($open_years->num_rows > 0){
			$open_year = $open_years->row['year'];
		} else {
			$open_year = date('Y');
		}

		$is_exist = $this->db->query("SELECT `emp_id` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `year` = '".$open_year."' ");
		if($is_exist->num_rows == 0){
			$insert2 = "INSERT INTO `oc_leave` SET 
						`emp_id` = '".$emp_code."', 
						`emp_name` = '".$this->db->escape($data['name'])."', 
						`emp_doj` = '".$data['doj']."', 
						`year` = '".$open_year."',
						`close_status` = '0' "; 
			$this->db->query($insert2);
			// echo $insert1;
			// echo '<br />';
			// exit;
		}

		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);

		$shift_id_data = 'S_'.$data['shift_id'];
		foreach ($months_array as $key => $value) {
			$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$data['emp_code']."' AND `month` = '".$key."' AND `year` = '".date('Y')."' ");
			if($is_exist->num_rows == 0){
				$insert1 = "INSERT INTO `oc_shift_schedule` SET 
							`emp_code` = '".$data['emp_code']."',
							`1` = '".$shift_id_data."',
							`2` = '".$shift_id_data."',
							`3` = '".$shift_id_data."',
							`4` = '".$shift_id_data."',
							`5` = '".$shift_id_data."',
							`6` = '".$shift_id_data."',
							`7` = '".$shift_id_data."',
							`8` = '".$shift_id_data."',
							`9` = '".$shift_id_data."',
							`10` = '".$shift_id_data."',
							`11` = '".$shift_id_data."',
							`12` = '".$shift_id_data."', 
							`13` = '".$shift_id_data."', 
							`14` = '".$shift_id_data."', 
							`15` = '".$shift_id_data."', 
							`16` = '".$shift_id_data."', 
							`17` = '".$shift_id_data."', 
							`18` = '".$shift_id_data."', 
							`19` = '".$shift_id_data."', 
							`20` = '".$shift_id_data."', 
							`21` = '".$shift_id_data."', 
							`22` = '".$shift_id_data."', 
							`23` = '".$shift_id_data."', 
							`24` = '".$shift_id_data."', 
							`25` = '".$shift_id_data."', 
							`26` = '".$shift_id_data."', 
							`27` = '".$shift_id_data."', 
							`28` = '".$shift_id_data."', 
							`29` = '".$shift_id_data."', 
							`30` = '".$shift_id_data."', 
							`31` = '".$shift_id_data."',
							`month` = '".$key."',
							`year` = '".date('Y')."',
							`status` = '1' ,
							`unit` = '".$data['unit']."',
							`unit_id` = '".$data['unit_id']."' ";
				$this->db->query($insert1);
			} else {
				$shift_schedule_data = $is_exist->row;
				$shift = $shift_id_data;
				// echo '<pre>';
				// print_r($shift_schedule_data);
				// exit;
				if($data['shift_id'] != $data['hidden_shift_id'] || $data['sat_status'] != $data['hidden_sat_status'] || $data['unit_id'] != $data['hidden_unit_id'] || strtotime($data['doj']) != strtotime($data['hidden_doj'])){
					$insert1 = "UPDATE `oc_shift_schedule` SET 
							`emp_code` = '".$data['emp_code']."',
							`1` = '".$shift_id_data."',
							`2` = '".$shift_id_data."',
							`3` = '".$shift_id_data."',
							`4` = '".$shift_id_data."',
							`5` = '".$shift_id_data."',
							`6` = '".$shift_id_data."',
							`7` = '".$shift_id_data."',
							`8` = '".$shift_id_data."',
							`9` = '".$shift_id_data."',
							`10` = '".$shift_id_data."',
							`11` = '".$shift_id_data."',
							`12` = '".$shift_id_data."', 
							`13` = '".$shift_id_data."', 
							`14` = '".$shift_id_data."', 
							`15` = '".$shift_id_data."', 
							`16` = '".$shift_id_data."', 
							`17` = '".$shift_id_data."', 
							`18` = '".$shift_id_data."', 
							`19` = '".$shift_id_data."', 
							`20` = '".$shift_id_data."', 
							`21` = '".$shift_id_data."', 
							`22` = '".$shift_id_data."', 
							`23` = '".$shift_id_data."', 
							`24` = '".$shift_id_data."', 
							`25` = '".$shift_id_data."', 
							`26` = '".$shift_id_data."', 
							`27` = '".$shift_id_data."', 
							`28` = '".$shift_id_data."', 
							`29` = '".$shift_id_data."', 
							`30` = '".$shift_id_data."', 
							`31` = '".$shift_id_data."',
							`month` = '".$key."',
							`year` = '".date('Y')."',
							`status` = '1' ,
							`unit` = '".$data['unit']."',
							`unit_id` = '".$data['unit_id']."' 
							WHERE id = '".$shift_schedule_data['id']."' ";
					$this->db->query($insert1);
				}
			}
		}

		$emp_code = $data['emp_code'];
		
		if($data['dol'] == '0000-00-00' && strtotime($data['hidden_dol']) != strtotime($data['dol'])){
			$current_date = date('Y-m-d');
			$current_month = date('n');
			if($current_month == 1 || $current_month == 2){
				$start_date = date('Y-02-01');
			} else {
				$start_date = date('Y-m-d', strtotime($current_date . ' -60 day'));//'2017-06-26';
			}
			$end_date = date('Y-m-d', strtotime($current_date . ' +1 day'));//'2017-06-30'
			$day = array();
			$days = $this->GetDays($start_date, $end_date);
			foreach ($days as $dkey => $dvalue) {
				$dates = explode('-', $dvalue);
				$day[$dkey]['day'] = $dates[2];
				$day[$dkey]['date'] = $dvalue;
			}
			$batch_id = '0';
			foreach($day as $dkeys => $dvalues){
				$filter_date_start = $dvalues['date'];
				$results = $this->getemployee_code($emp_code);
				// echo '<pre>';
				// print_r($results);
				// exit;
				foreach ($results as $rkey => $rvalue) {
					if(isset($rvalue['name']) && $rvalue['name'] != ''){
						$emp_name = $rvalue['name'];
						$department = $rvalue['department'];
						$unit = $rvalue['unit'];
						$group = '';
						$day_date = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));
						$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
						//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
						$shift_schedule = $this->db->query($update3)->row;
						$schedule_raw = explode('_', $shift_schedule[$day_date]);
						if(!isset($schedule_raw[2])){
							$schedule_raw[2]= 1;
						}
						/*echo '<pre>';
						print_r($schedule_raw);
						exit;*/
						if($schedule_raw[0] == 'S'){
							$shift_data = $this->getshiftdata_data($schedule_raw[1]);
							if(isset($shift_data['shift_id'])){
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];
								$shift_code = $shift_data['shift_code'];
								$shift_id = $shift_data['shift_id'];
								$day = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_id = '".$rvalue['contractor_id']."', contractor_code = '".$rvalue['contractor_code']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
									//echo $sql;exit;
									$this->db->query($sql);
								}
							} else {
								$shift_data = $this->getshiftdata_data('1');
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];
								$shift_code = $shift_data['shift_code'];
								$shift_id = $shift_data['shift_id'];
								$day = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_id = '".$rvalue['contractor_id']."', contractor_code = '".$rvalue['contractor_code']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
									$this->db->query($sql);
								}
							}
						} elseif ($schedule_raw[0] == 'W') {
							$shift_data = $this->getshiftdata_data($schedule_raw[2]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->getshiftdata_data('1');
							}
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
							$shift_code = $shift_data['shift_code'];
							$shift_id = $shift_data['shift_id'];
							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_id = '".$rvalue['contractor_id']."', contractor_code = '".$rvalue['contractor_code']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
								$this->db->query($sql);
							}
						} elseif ($schedule_raw[0] == 'H') {
							$shift_data = $this->getshiftdata_data($schedule_raw[2]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->getshiftdata_data('1');
							}
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
							$shift_code = $shift_data['shift_code'];
							$shift_id = $shift_data['shift_id'];
							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_id = '".$rvalue['contractor_id']."', contractor_code = '".$rvalue['contractor_code']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
								$this->db->query($sql);
							}
						} elseif ($schedule_raw[0] == 'HD') {
							$shift_data = $this->getshiftdata_data($schedule_raw[2]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->getshiftdata_data('1');
							}
							$shift_intime = $shift_data['in_time'];
							if($shift_data['shift_id'] == '1'){
								if($rvalue['sat_status'] == '1'){
									$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
									$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
								} else {
									$shift_outtime = $shift_data['out_time'];
								}
							} else {
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];
							}
							$shift_code = $shift_data['shift_code'];
							$shift_id = $shift_data['shift_id'];
							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_id = '".$rvalue['contractor_id']."', contractor_code = '".$rvalue['contractor_code']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
								$this->db->query($sql);
							}
						}
					}	
				}
			}
		}
		//$this->db->query("UPDATE " . DB_PREFIX . "employee SET name = '" . $this->db->escape($data['name']) . "', grade = '" . $this->db->escape($data['grade']) . "', branch = '" . $this->db->escape($data['branch']) . "', department = '" . $this->db->escape($data['department']) . "', division = '" . $this->db->escape($data['division']) . "', group = '" . $this->db->escape($data['group']) . "', category = '" . $this->db->escape($data['category']) . "', unit = '" . $this->db->escape($data['unit']) . "', designation = '" . $this->db->escape($data['designation']) . "', dob = '" . $this->db->escape($data['dob']) . "', doj = '" . $this->db->escape($data['doj']) . "', doc = '" . $this->db->escape($data['doc']) . "', dol = '" . $this->db->escape($data['dol']) . "', emp_code = '" . $this->db->escape($data['emp_code']) . "', shift = '" . $this->db->escape($data['shift_id']) . "', card_number = '" . $this->db->escape($data['card_number']) . "', status = '" . $this->db->escape($data['status']) . "', daily_punch = '" . $this->db->escape($data['daily_punch']) . "', weekly_off = '" . $this->db->escape($data['weekly_off']) . "', gender = '" . $this->db->escape($data['gender']) . "' WHERE employee_id = '" . (int)$employee_id . "'");
		//echo "UPDATE " . DB_PREFIX . "employee SET card_number = '" . $this->db->escape($data['card_number']) . "', status = '" . $this->db->escape($data['status']) . "', daily_punch = '" . $this->db->escape($data['daily_punch']) . "', weekly_off = '" . $this->db->escape($data['weekly_off']) . "' WHERE employee_id = '" . (int)$employee_id . "'";
		//exit;
		//$this->log->write("UPDATE " . DB_PREFIX . "employee SET name = '" . $this->db->escape($data['name']) . "', location = '" . $this->db->escape($data['location_id']) . "', category = '" . $this->db->escape($data['category_id']) . "', emp_code = '" . $this->db->escape($data['emp_code']) . "', department = '" . $this->db->escape($data['department_id']) . "', shift = '" . $this->db->escape($data['shift_id']) . "', card_number = '" . $this->db->escape($data['card_number']) . "', status = '" . $this->db->escape($data['status']) . "', daily_punch = '" . $this->db->escape($data['daily_punch']) . "', weekly_off = '" . $this->db->escape($data['weekly_off']) . "', gender = '" . $this->db->escape($data['gender']) . "' WHERE employee_id = '" . (int)$employee_id . "'");
		//location = '" . $this->db->escape($data['location_id']) . "', category = '" . $this->db->escape($data['category_id']) . "', 
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function getshiftdata_data($shift_id) {
		$query = $this->db->query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ");
		if($query->num_rows > 0){
			return $query->row;
		} else {
			return array();
		}
	}

	public function deleteemployee($employee_id) {
		$emp_data = $this->getemployee($employee_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$emp_data['emp_code'] . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "attendance WHERE emp_id = '" . (int)$emp_data['emp_code'] . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "transaction WHERE emp_id = '" . (int)$emp_data['emp_code'] . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "leave WHERE emp_id = '" . (int)$emp_data['emp_code'] . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "leave_employee WHERE emp_id = '" . (int)$emp_data['emp_code'] . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "leave_transaction WHERE emp_id = '" . (int)$emp_data['emp_code'] . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "employee WHERE employee_id = '" . (int)$employee_id . "'");
	}	

	public function getemployee($employee_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "employee WHERE employee_id = '" . (int)$employee_id . "'");
		return $query->row;
	}

	public function getemployee_code($emp_code) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "employee WHERE emp_code = '" . (int)$emp_code . "'");
		return $query->rows;
	}

	public function getcatname($category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "categories WHERE id = '" . (int)$category_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getdeptname($department_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department WHERE id = '" . (int)$department_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getshiftname($shift_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift WHERE shift_id = '" . (int)$shift_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getshiftdata($shift_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift WHERE shift_id = '" . (int)$shift_id . "'");
		if($query->num_rows > 0){
			$shift_name = $query->row['in_time'].'-'.$query->row['out_time'];
			return $shift_name;
		} else {
			return '';
		}
	}

	public function getweekname($week_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "week WHERE week_id = '" . (int)$week_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getholidayname($holiday_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "holiday WHERE holiday_id = '" . (int)$holiday_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getshift_id($emp_code, $day_date, $month, $year) {
		$query = $this->db->query("SELECT `".$day_date."` FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$emp_code . "' ");
		if($query->num_rows > 0){
			return $query->row[$day_date];
		} else {
			return '';
		}
	}

	public function getlocname($location_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE id = '" . (int)$location_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function filter_contractor($data = array()) {

		$site_sql = $this->db->query("SELECT site FROM oc_user WHERE user_id = '".$data['user_id']."' ");
		$all_sites = '';
		if ($site_sql->num_rows > 0) {
			$all_sites = $site_sql->row['site'];
		}

		$all_sites = ltrim($all_sites, 'multiselect-all,');

		$sql = "SELECT * FROM oc_contractor c LEFT JOIN oc_contractor_location cl ON(c.contractor_id = cl.contractor_id) WHERE cl.unit_id IN($all_sites) ";
		if (!empty($data['filter_contractor'])) {
			$sql .= " AND (LOWER(c.contractor_name) LIKE '%" . $this->db->escape(strtolower($data['filter_contractor'])) . "%' OR c.contractor_code LIKE '%" . $data['filter_contractor']."%') ";
		}

		$query = $this->db->query($sql);

		return $query->rows;

	}

	public function getemployees($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "employee WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			if(ctype_digit($data['filter_name'])){
				$sql .= " AND emp_code = '" . $data['filter_name'] . "' ";	
			} else {
				$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			}
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND emp_code = '" . $data['filter_name_id'] . "' ";
		}

		if (isset($data['filter_code']) && !empty($data['filter_code'])) {
			$sql .= " AND `emp_code` = '" . $this->db->escape($data['filter_code']) . "' ";
		}

		if (isset($data['filter_department']) && !empty($data['filter_department'])) {
			$sql .= " AND LOWER(department_id) = '" . $this->db->escape(strtolower($data['filter_department'])) . "' ";
		}

		if (isset($data['filter_region']) && !empty($data['filter_region'])) {
			$sql .= " AND LOWER(region_id) = '" . $this->db->escape(strtolower($data['filter_region'])) . "' ";
		}

		if (isset($data['filter_division']) && !empty($data['filter_division'])) {
			$sql .= " AND LOWER(division_id) = '" . $this->db->escape(strtolower($data['filter_division'])) . "' ";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND LOWER(unit_id) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "' ";
		}

		if (isset($data['filter_company']) && !empty($data['filter_company'])) {
			$sql .= " AND LOWER(company_id) = '" . $this->db->escape(strtolower($data['filter_company'])) . "' ";
		}

		if (isset($data['filter_contractor_id']) && !empty($data['filter_contractor_id'])) {
			$sql .= " AND LOWER(contractor_id) = '" . $this->db->escape(strtolower($data['filter_contractor_id'])) . "' ";
		}

		if(isset($data['filter_local'])){
			$sql .= " AND `company_id` <> '1' ";
		}

		$user_ids = $this->user->getId();
		/*if ($user_ids != 74 && $user_ids != 73) {//if not only for wepl login
			if(isset($data['filter_jmc'])){
				$sql .= " AND `company_id` = '1' ";
			}
		}//if not only for wepl login*/

		if (isset($data['filter_shift_type'])) {
			$sql .= " AND shift_type = 'R' ";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		//if($this->user->getId() != '1'){
			$division_string = $this->user->getdivision();
			$region_string = $this->user->getregion();
			$site_string = $this->user->getsite();
			$company_string = $this->user->getCompanyId();
			
			if($company_string != '' && !isset($data['filter_transfer'])){
				$company_string = "'" . str_replace(",", "','", html_entity_decode($company_string)) . "'";
				if (isset($data['filter_company']) && !empty($data['filter_company'])) {
				} else {
					$sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
				}
			}

			if($division_string != ''){
				$division_string = "'" . str_replace(",", "','", html_entity_decode($division_string)) . "'";
				if (isset($data['filter_division']) && !empty($data['filter_division'])) {
				} else {
					$sql .= " AND `division_id` IN (" . strtolower($division_string) . ") ";
				}
			}

			if($region_string != ''){
				$region_string = "'" . str_replace(",", "','", html_entity_decode($region_string)) . "'";
				if (isset($data['filter_region']) && !empty($data['filter_region'])) {
				} else {
					//$sql .= " AND `region_id` IN (" . strtolower($region_string) . ") ";
				}
			}

			if($site_string != ''){
				$site_string = "'" . str_replace(",", "','", html_entity_decode($site_string)) . "'";
				if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
				} else {
					$sql .= " AND `unit_id` IN (" . strtolower($site_string) . ") ";
				}
			}
		//}

		$sort_data = array(
			'name',
			'emp_code',
			'department',
			'division',
			'region',
			'company',
			'unit'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";//
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		//$this->log->write($sql);			
		//echo $sql;exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalemployees($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "employee WHERE 1=1 ";
		
		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			if(ctype_digit($data['filter_name'])){
				$sql .= " AND emp_code = '" . $data['filter_name'] . "' ";	
			} else {
				$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			}
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND emp_code = '" . $data['filter_name_id'] . "' ";
		}

		if (isset($data['filter_code']) && !empty($data['filter_code'])) {
			$sql .= " AND `emp_code` = '" . $this->db->escape($data['filter_code']) . "' ";
		}

		if (isset($data['filter_department']) && !empty($data['filter_department'])) {
			$sql .= " AND LOWER(department_id) = '" . $this->db->escape(strtolower($data['filter_department'])) . "' ";
		}

		if (isset($data['filter_region']) && !empty($data['filter_region'])) {
			$sql .= " AND LOWER(region_id) = '" . $this->db->escape(strtolower($data['filter_region'])) . "' ";
		}

		if (isset($data['filter_division']) && !empty($data['filter_division'])) {
			$sql .= " AND LOWER(division_id) = '" . $this->db->escape(strtolower($data['filter_division'])) . "' ";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND LOWER(unit_id) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "' ";
		}

		if (isset($data['filter_company']) && !empty($data['filter_company'])) {
			$sql .= " AND LOWER(company_id) = '" . $this->db->escape(strtolower($data['filter_company'])) . "' ";
		}

		if (isset($data['filter_contractor_id']) && !empty($data['filter_contractor_id'])) {
			$sql .= " AND LOWER(contractor_id) = '" . $this->db->escape(strtolower($data['filter_contractor_id'])) . "' ";
		}

		if(isset($data['filter_local'])){
			$sql .= " AND `company_id` <> '1' ";
		}

		// if(isset($data['filter_jmc'])){
		// 	$sql .= " AND `company_id` = '1' ";
		// }

		if (isset($data['filter_shift_type'])) {
			$sql .= " AND shift_type = 'R' ";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		//if($this->user->getId() != '1'){
			$division_string = $this->user->getdivision();
			$region_string = $this->user->getregion();
			$site_string = $this->user->getsite();
			$company_string = $this->user->getCompanyId();
			
			if($company_string != '' && !isset($data['filter_transfer'])){
				$company_string = "'" . str_replace(",", "','", html_entity_decode($company_string)) . "'";
				if (isset($data['filter_company']) && !empty($data['filter_company'])) {
				} else {
					$sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
				}
			}
			
			if($division_string != ''){
				$division_string = "'" . str_replace(",", "','", html_entity_decode($division_string)) . "'";
				if (isset($data['filter_division']) && !empty($data['filter_division'])) {
				} else {
					$sql .= " AND `division_id` IN (" . strtolower($division_string) . ") ";
				}
			}

			if($region_string != ''){
				$region_string = "'" . str_replace(",", "','", html_entity_decode($region_string)) . "'";
				if (isset($data['filter_region']) && !empty($data['filter_region'])) {
				} else {
					//$sql .= " AND `region_id` IN (" . strtolower($region_string) . ") ";
				}
			}

			if($site_string != ''){
				$site_string = "'" . str_replace(",", "','", html_entity_decode($site_string)) . "'";
				if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
				} else {
					$sql .= " AND `unit_id` IN (" . strtolower($site_string) . ") ";
				}
			}
		//}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getActiveEmployee() {
		$sql = "SELECT `emp_code`, `name`, `doj`, `grade`, `group` FROM ".DB_PREFIX."employee WHERE `status` = '1'";	
		$query = $this->db->query($sql);
		return $query->rows;	
	}

	public function getemployees_export($data = array()) {
		$sql = "SELECT `emp_code`, `name`, `gender`, `dob`, `doj`, `doc`, `dol`, `employement`, `grade`, `designation`, `department`, `unit`, `division`, `region`, `state`, `company`, `sat_status`, `status`, `category`, `contractor`, `contractor_code` FROM " . DB_PREFIX . "employee WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			if(ctype_digit($data['filter_name'])){
				$sql .= " AND emp_code = '" . $data['filter_name'] . "' ";	
			} else {
				$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			}
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND emp_code = '" . $data['filter_name_id'] . "' ";
		}

		if (isset($data['filter_code']) && !empty($data['filter_code'])) {
			$sql .= " AND `emp_code` = '" . $this->db->escape($data['filter_code']) . "' ";
		}

		if (isset($data['filter_department']) && !empty($data['filter_department'])) {
			$sql .= " AND LOWER(department_id) = '" . $this->db->escape(strtolower($data['filter_department'])) . "' ";
		}

		if (isset($data['filter_region']) && !empty($data['filter_region'])) {
			$sql .= " AND LOWER(region_id) = '" . $this->db->escape(strtolower($data['filter_region'])) . "' ";
		}

		if (isset($data['filter_division']) && !empty($data['filter_division'])) {
			$sql .= " AND LOWER(division_id) = '" . $this->db->escape(strtolower($data['filter_division'])) . "' ";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND LOWER(unit_id) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "' ";
		}

		if (isset($data['filter_company']) && !empty($data['filter_company'])) {
			$sql .= " AND LOWER(company_id) = '" . $this->db->escape(strtolower($data['filter_company'])) . "' ";
		}

		if(isset($data['filter_local'])){
			$sql .= " AND `company_id` <> '1' ";
		}

		if(isset($data['filter_jmc'])){
			$sql .= " AND `company_id` = '1' ";
		}

		if (isset($data['filter_shift_type'])) {
			$sql .= " AND shift_type = 'R' ";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		//if($this->user->getId() != '1'){
			$division_string = $this->user->getdivision();
			$region_string = $this->user->getregion();
			$site_string = $this->user->getsite();
			$company_string = $this->user->getCompanyId();
			
			if($company_string != ''){
				$company_string = "'" . str_replace(",", "','", html_entity_decode($company_string)) . "'";
				if (isset($data['filter_company']) && !empty($data['filter_company'])) {
				} else {
					$sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
				}
			}

			if($division_string != ''){
				$division_string = "'" . str_replace(",", "','", html_entity_decode($division_string)) . "'";
				if (isset($data['filter_division']) && !empty($data['filter_division'])) {
				} else {
					$sql .= " AND `division_id` IN (" . strtolower($division_string) . ") ";
				}
			}

			if($region_string != ''){
				$region_string = "'" . str_replace(",", "','", html_entity_decode($region_string)) . "'";
				if (isset($data['filter_region']) && !empty($data['filter_region'])) {
				} else {
					//$sql .= " AND `region_id` IN (" . strtolower($region_string) . ") ";
				}
			}

			if($site_string != ''){
				$site_string = "'" . str_replace(",", "','", html_entity_decode($site_string)) . "'";
				if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
				} else {
					$sql .= " AND `unit_id` IN (" . strtolower($site_string) . ") ";
				}
			}
		//}

		$sort_data = array(
			'name',
			'emp_code',
			'department',
			'division',
			'region',
			'company',
			'unit'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		//$this->log->write($sql);			
		//echo $sql;exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}
}
?>