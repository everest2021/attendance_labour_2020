<?php
class ModelCatalogState extends Model {
	public function addState($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "state` SET 
							`state` = '" . $this->db->escape(html_entity_decode($data['state'])) . "',
							`state_code` = '" . $this->db->escape(html_entity_decode($data['state_code'])) . "'
						");

		$state_id = $this->db->getLastId(); 
	}

	public function editState($state_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "state SET 
							`state` = '" . $this->db->escape(html_entity_decode($data['state'])) . "',
							`state_code` = '" . $this->db->escape(html_entity_decode($data['state_code'])) . "'
							WHERE state_id = '" . (int)$state_id . "'");
	}

	public function deleteState($state_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "state WHERE state_id = '" . (int)$state_id . "'");
	}	

	public function getState($state_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "state WHERE state_id = '" . (int)$state_id . "'");

		return $query->row;
	}

	public function getStates($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "state WHERE 1=1 ";

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND state_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(state) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'state',
			'state_code',
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY state";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalStates() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "state";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND state_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(state) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	
}
?>