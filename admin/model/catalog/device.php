<?php
class ModelCatalogDevice extends Model {
	public function addDevice($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "device` SET 
							`device` = '" . $this->db->escape($data['device']) . "'
						");

		$device_id = $this->db->getLastId(); 

		// $shifts = $this->db->query("SELECT `shift_id` FROM `oc_shift` ");
		// if($shifts->num_rows > 0){
		// 		$shift = $shifts->rows;
		// 		//echo "<pre>"; print_r($shift);exit;
		// 		foreach($shift as $ekeys => $evalues){
		// 			$sql = "INSERT INTO `oc_shift_device`  SET
		// 					`shift_id` = '" .$evalues['shift_id'] . "',
		// 					`name` = '" . $this->db->escape($data['device']) . "' ";
		// 			//echo $sql;
		// 			$this->db->query($sql);

		// 	}
		// }
	}

	public function editDevice($device_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "device SET 
							`device` = '" . $this->db->escape($data['device']) . "'
							WHERE device_id = '" . (int)$device_id . "'");

		// $this->db->query("DELETE FROM " . DB_PREFIX . "shift_device WHERE `name` = '" . $this->db->escape($data['device_old']) . "'
		// 				");
		// $shifts = $this->db->query("SELECT `shift_id` FROM `oc_shift` ");
		// if($shifts->num_rows > 0){
		// 		$shift = $shifts->rows;
		// 		//echo "<pre>"; print_r($shift);exit;
		// 		foreach($shift as $ekeys => $evalues){
		// 			$sql = "INSERT INTO `oc_shift_device`  SET
		// 					`shift_id` = '" .$evalues['shift_id'] . "',
		// 					`name` = '" . $this->db->escape($data['device']) . "' ";
		// 			//echo $sql;
		// 			$this->db->query($sql);

		// 	}
		// }

	}

	public function deleteDevice($device_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "device WHERE device_id = '" . (int)$device_id . "'");
	}	

	public function getDevice($device_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "device WHERE device_id = '" . (int)$device_id . "'");

		return $query->row;
	}

	public function getDevices($data = array()) {
		/*$serverName = "JMCFRM\ATTENDANCE";
		$connectionInfo = array("Database"=>"eSSLSmartOfficeAVI", "UID"=>"staff", "PWD"=>"staff");
		//$connectionInfo = array("Database"=>"SmartOffice");
		$conn = sqlsrv_connect($serverName, $connectionInfo);
		if($conn) {
		     //echo "Connection established.<br />";
		}else{
		     //echo "Connection could not be established.<br />";
		     $dat['connection'] = 'Connection could not be established';
		     // echo '<pre>';
		     // print_r(sqlsrv_errors());
		     // exit;
		     //die( print_r( sqlsrv_errors(), true));
		}*/
		$serverName = "10.200.1.35";
		$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"avi", "PWD"=>"avi2123");
		//$connectionInfo = array("Database"=>"eSSLSmartOfficeAVI", "UID"=>"avi", "PWD"=>"avi2123");
		//$connectionInfo = array("Database"=>"SmartOffice");
		$conn = sqlsrv_connect($serverName, $connectionInfo);
		if($conn) {
			//echo "Connection established.<br />";exit;
		} else {
			// echo "Connection could not be established.<br />";
			// echo "<br />";
			$dat['connection'] = 'Connection could not be established';
			// echo '<pre>';
			// print_r(sqlsrv_errors());
			// exit;
			//die( print_r( sqlsrv_errors(), true));
		}
		$sql = "SELECT * FROM dbo.Devices WHERE 1=1 ";
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND DeviceId = '" . $data['filter_name_id'] . "' ";
		}		
		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(DeviceFName) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		if (!empty($data['filter_direction'])) {
			$data['filter_direction'] = html_entity_decode($data['filter_direction']);
			$sql .= " AND LOWER(DeviceDirection) LIKE '%" . $this->db->escape(strtolower($data['filter_direction'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		if (!empty($data['filter_location'])) {
			$data['filter_location'] = html_entity_decode($data['filter_location']);
			$sql .= " AND LOWER(DeviceLocation) LIKE '%" . $this->db->escape(strtolower($data['filter_location'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		if (isset($data['filter_download_date']) && !empty($data['filter_download_date'])) {
			$day = date('d', strtotime($data['filter_download_date']));
			$month = date('m', strtotime($data['filter_download_date']));
			$year = date('Y', strtotime($data['filter_download_date']));
			$sql .= " AND (DATEPART(yy, LastLogDownloadDate) = '".$year."' AND DATEPART(mm, LastLogDownloadDate) = '".$month."' AND DATEPART(dd, LastLogDownloadDate) = '".$day."') ";
		}
		if (isset($data['filter_ping_date']) && !empty($data['filter_ping_date'])) {
			$day = date('d', strtotime($data['filter_download_date']));
			$month = date('m', strtotime($data['filter_download_date']));
			$year = date('Y', strtotime($data['filter_download_date']));
			$sql .= " AND (DATEPART(yy, LastPing) = '".$year."' AND DATEPART(mm, LastPing) = '".$month."' AND DATEPART(dd, LastPing) = '".$day."') ";
		}
		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$data['filter_unit'] = html_entity_decode($data['filter_unit']);
			$sql .= " AND LOWER(Site) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "'";
		}
		if (isset($data['filter_units']) && !empty($data['filter_units'])) {
			$unit_string = "'" . str_replace(",", "','", html_entity_decode($data['filter_units'])) . "'";
			$sql .= " AND Site IN (" . strtolower($unit_string) . ") ";
		}

		if($this->user->getId() != '1'){
			$device_string = $this->user->getdevice();
			if($device_string != ''){
				$device_string = str_replace("multiselect-all,", "", html_entity_decode($device_string));
				$device_string = "'" . str_replace(",", "','", html_entity_decode($device_string)) . "'";
				$sql .= " AND DeviceId IN (" . strtolower($device_string) . ") ";
			}
		}
		$sort_data = array(
			'DeviceFName',
			'DeviceDirection',
			'DeviceLocation',
			'LastLogDownloadDate',
			'LastPing',
		);		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY DeviceFName";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}		

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}	

		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }
		//$this->log->write($sql);
		$params = array();
		$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt = sqlsrv_query($conn, $sql, $params, $options);
		if( $stmt === false) {
		   //echo'<pre>';
		   //print_r(sqlsrv_errors());
		   //exit;
		}
		$devices = array();
		while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			$last_download_dates = $row['LastLogDownloadDate'];
			$last_download_times = $row['LastLogDownloadDate'];
		 	if($last_download_dates != NULL){
			 	$last_download_date = $last_download_dates->format("Y-m-d");
			 	$last_download_time = $last_download_times->format("H:i:s");
		 	} else {
		 		$last_download_date = '0000-00-00';
		 		$last_download_time = '00:00:00';
		 	}
		 	$last_ping_dates = $row['LastPing'];
		 	$last_ping_times = $row['LastPing'];
		 	if($last_ping_dates != NULL){
		 		$last_ping_date = $last_ping_dates->format("Y-m-d");
		 		$last_ping_time = $last_ping_times->format("H:i:s");
			} else {
				$last_ping_date = '0000-00-00';
		 		$last_ping_time = '00:00:00';
			}
			$devices[] = array(
				'device_id' => $row['DeviceId'],
				'device_name' => $row['DeviceFName'],
				'direction' => $row['DeviceDirection'],
				'location' => $row['DeviceLocation'],
				'company' => '',//$row['DeviceFName'],
				'last_download_date' => $last_download_date,
				'last_download_time' => $last_download_time,
				'last_ping_date' => $last_ping_date,
				'last_ping_time' => $last_ping_time,
				'SerialNumber'  => $row['SerialNumber'],
			);
		}
		return $devices;
	}

	public function getTotalDevices($data = array()) {
		/*$serverName = "JMCFRM\ATTENDANCE";

		$connectionInfo = array("Database"=>"eSSLSmartOfficeAVI", "UID"=>"staff", "PWD"=>"staff");
		//$connectionInfo = array("Database"=>"SmartOffice");
		$conn = sqlsrv_connect($serverName, $connectionInfo);
		if($conn) {
		} else {
			// echo '<pre>';
		 //    print_r(sqlsrv_errors());
		 //    exit;
			$dat['connection'] = 'Connection could not be established';
		}*/

		$serverName = "10.200.1.35";
		$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"avi", "PWD"=>"avi2123");
		//$connectionInfo = array("Database"=>"eSSLSmartOfficeAVI", "UID"=>"avi", "PWD"=>"avi2123");
		//$connectionInfo = array("Database"=>"SmartOffice");
		$conn = sqlsrv_connect($serverName, $connectionInfo);
		if($conn) {
			//echo "Connection established.<br />";exit;
		} else {
			// echo "Connection could not be established.<br />";
			// echo "<br />";
			$dat['connection'] = 'Connection could not be established';
			// echo '<pre>';
			// print_r(sqlsrv_errors());
			// exit;
			//die( print_r( sqlsrv_errors(), true));
		}
		$sql = "SELECT * FROM dbo.Devices WHERE 1=1 ";
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND DeviceId = '" . $data['filter_name_id'] . "' ";
		}		
		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(DeviceFName) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		if (!empty($data['filter_direction'])) {
			$data['filter_direction'] = html_entity_decode($data['filter_direction']);
			$sql .= " AND LOWER(DeviceDirection) LIKE '%" . $this->db->escape(strtolower($data['filter_direction'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		if (!empty($data['filter_location'])) {
			$data['filter_location'] = html_entity_decode($data['filter_location']);
			$sql .= " AND LOWER(DeviceLocation) LIKE '%" . $this->db->escape(strtolower($data['filter_location'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		if (isset($data['filter_download_date']) && !empty($data['filter_download_date'])) {
			$day = date('d', strtotime($data['filter_download_date']));
			$month = date('m', strtotime($data['filter_download_date']));
			$year = date('Y', strtotime($data['filter_download_date']));
			$sql .= " AND (DATEPART(yy, LastLogDownloadDate) = '".$year."' AND DATEPART(mm, LastLogDownloadDate) = '".$month."' AND DATEPART(dd, LastLogDownloadDate) = '".$day."') ";
		}
		if (isset($data['filter_ping_date']) && !empty($data['filter_ping_date'])) {
			$day = date('d', strtotime($data['filter_download_date']));
			$month = date('m', strtotime($data['filter_download_date']));
			$year = date('Y', strtotime($data['filter_download_date']));
			$sql .= " AND (DATEPART(yy, LastPing) = '".$year."' AND DATEPART(mm, LastPing) = '".$month."' AND DATEPART(dd, LastPing) = '".$day."') ";
		}
		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$data['filter_unit'] = html_entity_decode($data['filter_unit']);
			$sql .= " AND LOWER(Site) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "'";
		}
		if (isset($data['filter_units']) && !empty($data['filter_units'])) {
			$unit_string = "'" . str_replace(",", "','", html_entity_decode($data['filter_units'])) . "'";
			$sql .= " AND Site IN (" . strtolower($unit_string) . ") ";
		}

		if($this->user->getId() != '1'){
			$device_string = $this->user->getdevice();
			if($device_string != ''){
				$device_string = str_replace("multiselect-all,", "", html_entity_decode($device_string));
				$device_string = "'" . str_replace(",", "','", html_entity_decode($device_string)) . "'";
				$sql .= " AND DeviceId IN (" . strtolower($device_string) . ") ";
			}
		}
		//echo $sql;exit;
		$params = array();
		$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt = sqlsrv_query($conn, $sql, $params, $options);
		if( $stmt === false) {
		   //echo'<pre>';
		   //print_r(sqlsrv_errors());
		   //exit;
		}
		$total_count = 0;
		while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			$total_count ++;
		}
		return $total_count;
	}	
}
?>