<?php
class ModelCatalogContractor extends Model {
	public function addContractor($data) {
			//echo '<pre>';print_r($data);exit;
		$location = implode(",",$data['location']);
		$this->db->query("INSERT INTO `" . DB_PREFIX . "contractor` SET 
							`contractor_name` = '" . $this->db->escape(html_entity_decode($data['contractor_name'])) . "', 
							`contractor_code` = '" . $this->db->escape(html_entity_decode($data['contractor_code'])) . "'
						");

		$contractor_id = $this->db->getLastId();

		foreach ($data['location'] as $key => $value) {
			$unit_name=$this->db->query("SELECT `unit` from  `oc_unit` where `unit_id`='".$value."' ")->row['unit'];
		 	$this->db->query("INSERT INTO `" . DB_PREFIX . "contractor_location` SET 
							`contractor_id` = '" .$contractor_id. "', 
							`unit_id` = '" .$value. "',
							`unit` ='".$unit_name."'
						");
		 } 
	}

	public function editContractor($contractor_id, $data){
		$location = implode(",",$data['location']);

		$this->db->query("UPDATE " . DB_PREFIX . "contractor SET 
							`contractor_name` = '" . $this->db->escape(html_entity_decode($data['contractor_name'])) . "', 
							`contractor_code` = '" . $this->db->escape(html_entity_decode($data['contractor_code'])) . "'
							WHERE contractor_id = '" . (int)$contractor_id . "'");
		$this->db->query("DELETE FROM `oc_contractor_location` WHERE `contractor_id` = '" . (int)$contractor_id . "'");
		foreach ($data['location'] as $key => $value) {
			$unit_name=$this->db->query("SELECT `unit` from  `oc_unit` where `unit_id`='".$value."' ")->row['unit'];
		 	$this->db->query("INSERT INTO `" . DB_PREFIX . "contractor_location` SET 
							`contractor_id` = '" .$contractor_id. "', 
							`unit_id` = '" .$value. "',
							`unit` ='".$unit_name."'
						    ");
		}

		$this->db->query("UPDATE " . DB_PREFIX . "employee SET 
							`contractor` = '" . $this->db->escape(html_entity_decode($data['contractor_name'])) . "',
							`contractor_code` = '" . $this->db->escape(html_entity_decode($data['contractor_code'])) . "',
							`contractor_id` = '" . $this->db->escape(html_entity_decode($contractor_id)) . "'
							WHERE contractor_id = '" . (int)$contractor_id . "'"); 
	}

	public function deleteContractor($contractor_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "contractor WHERE contractor_id = '" . (int)$contractor_id . "'");
		$this->db->query("DELETE FROM `oc_contractor_location` WHERE `contractor_id` = '" . (int)$contractor_id . "'");
	}	

	public function getContractor($contractor_id) {
		$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "contractor WHERE contractor_id = '" . (int)$contractor_id . "'");

		return $query->row;
	}

	public function getContractor_tots($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "contractor WHERE 1=1 ";

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND contractor_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			if(ctype_digit($data['filter_name'])){
				$sql .= " AND contractor_code = '" . $data['filter_name'] . "' ";	
			} else {
				$sql .= " AND LOWER(contractor_name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			}
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'contractor_name',
			'code',
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY contractor_name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalContractor($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "contractor WHERE 1=1 ";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND contractor_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			if(ctype_digit($data['filter_name'])){
				$sql .= " AND contractor_code = '" . $data['filter_name'] . "' ";	
			} else {
				$sql .= " AND LOWER(contractor_name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			}
			//$sql .= " AND LOWER(contractor_name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	

	public function getContractors_by_unit($data){
		$sql = "SELECT `contractor_id` FROM " . DB_PREFIX . "contractor_location WHERE 1=1 ";
		if(!empty($data['filter_unit_id'])){
			$sql .= " AND unit_id = '".$data['filter_unit_id']."' ";
		}
		if(isset($data['filter_site_string']) && !empty($data['filter_site_string'])){
			$site_string = "'" . str_replace(",", "','", html_entity_decode($data['filter_site_string'])) . "'";
			$sql .= " AND `unit_id` IN (" . strtolower($site_string) . ") ";
		}
		$sql .= " GROUP BY `contractor_id` ";
		$contractor_location_datas = $this->db->query($sql)->rows;
		$contractor_datas = array();
		if(isset($data['filter_master']) && $data['filter_master'] == '1'){
			$contractor_datas[] = array(
				'contractor_id' => '',
				'contractor_name' => 'Please Select',
				'contractor_code' => ''
			);
		} else if(isset($data['filter_master']) && $data['filter_master'] == '0') {
			$contractor_datas[] = array(
				'contractor_id' => '',
				'contractor_name' => 'All',
				'contractor_code' => ''
			);
		} else if(isset($data['filter_master']) && $data['filter_master'] == '2') {
			// $contractor_datas[] = array(
			// 	'contractor_id' => '',
			// 	'contractor_name' => 'All',
			// 	'contractor_code' => ''
			// );
		}
		foreach($contractor_location_datas as $ckey => $cvalue){
			$sql = "SELECT * FROM `oc_contractor` WHERE `contractor_id` = '".$cvalue['contractor_id']."' ";	
			$contractor_datasss = $this->db->query($sql);
			if($contractor_datasss->num_rows > 0){
				$contractor_datass = $contractor_datasss->row;
				$contractor_datas[] = array(
					'contractor_id' => $contractor_datass['contractor_id'],
					'contractor_name' => $contractor_datass['contractor_name'].'-'.$contractor_datass['contractor_code'],
					'contractor_code' => $contractor_datass['contractor_code'],
				);
			}
		}
		return $contractor_datas;	
	}
}
?>