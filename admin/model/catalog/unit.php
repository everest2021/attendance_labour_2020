<?php
class ModelCatalogUnit extends Model {
	public function addUnit($data) {
		$division_name = $this->db->query("SELECT `division` FROM `oc_division` WHERE `division_id` = '".$data['division_id']."' ")->row['division'];
		$this->db->query("INSERT INTO `" . DB_PREFIX . "unit` SET 
							`unit` = '" . $this->db->escape(html_entity_decode($data['unit'])) . "',
							`unit_code` = '" . $this->db->escape(html_entity_decode($data['unit_code'])) . "',
							`division_id` = '" . $this->db->escape($data['division_id']) . "',
							`division_name` = '" . $this->db->escape(html_entity_decode($data['division_name'])) . "',
							`state_id` = '" . $this->db->escape($data['state_id']) . "',
							`state_name` = '" . $this->db->escape(html_entity_decode($data['state_name'])) . "'
						");

		$unit_id = $this->db->getLastId(); 

		// $shifts = $this->db->query("SELECT `shift_id` FROM `oc_shift` ");
		// if($shifts->num_rows > 0){
		// 		$shift = $shifts->rows;
		// 		//echo "<pre>"; print_r($shift);exit;
		// 		foreach($shift as $ekeys => $evalues){
		// 			$sql = "INSERT INTO `oc_shift_unit`  SET
		// 					`shift_id` = '" .$evalues['shift_id'] . "',
		// 					`name` = '" . $this->db->escape($data['unit']) . "' ";
		// 			//echo $sql;
		// 			$this->db->query($sql);

		// 	}
		// }
	}

	public function editUnit($unit_id, $data) {
		$division_name = $this->db->query("SELECT `division` FROM `oc_division` WHERE `division_id` = '".$data['division_id']."' ")->row['division'];
		$this->db->query("UPDATE " . DB_PREFIX . "unit SET 
							`unit` = '" . $this->db->escape(html_entity_decode($data['unit'])) . "',
							`unit_code` = '" . $this->db->escape(html_entity_decode($data['unit_code'])) . "',
							`division_id` = '" . $this->db->escape($data['division_id']) . "',
							`division_name` = '" . $this->db->escape(html_entity_decode($data['division_name'])) . "',
							`state_id` = '" . $this->db->escape($data['state_id']) . "',
							`state_name` = '" . $this->db->escape(html_entity_decode($data['state_name'])) . "'
							WHERE unit_id = '" . (int)$unit_id . "'");

		$this->db->query("UPDATE " . DB_PREFIX . "employee SET 
							`unit` = '" . $this->db->escape(html_entity_decode($data['unit'])) . "',
							`unit_id` = '" . $this->db->escape(html_entity_decode($unit_id)) . "'
							WHERE unit_id = '" . (int)$unit_id . "'");

		// $this->db->query("DELETE FROM " . DB_PREFIX . "shift_unit WHERE `name` = '" . $this->db->escape($data['unit_old']) . "'
		// 				");
		// $shifts = $this->db->query("SELECT `shift_id` FROM `oc_shift` ");
		// if($shifts->num_rows > 0){
		// 		$shift = $shifts->rows;
		// 		//echo "<pre>"; print_r($shift);exit;
		// 		foreach($shift as $ekeys => $evalues){
		// 			$sql = "INSERT INTO `oc_shift_unit`  SET
		// 					`shift_id` = '" .$evalues['shift_id'] . "',
		// 					`name` = '" . $this->db->escape($data['unit']) . "' ";
		// 			//echo $sql;
		// 			$this->db->query($sql);

		// 	}
		// }

	}

	public function deleteUnit($unit_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "unit WHERE unit_id = '" . (int)$unit_id . "'");
	}	

	public function getUnit($unit_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "unit WHERE unit_id = '" . (int)$unit_id . "'");

		return $query->row;
	}

	public function getUnits($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "unit WHERE 1=1 ";

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND unit_id = '" . $data['filter_name_id'] . "' ";
		}

		if (isset($data['filter_division_id']) && !empty($data['filter_division_id'])) {
			$sql .= " AND division_id = '" . $data['filter_division_id'] . "' ";
		}

		if (isset($data['filter_state_id']) && !empty($data['filter_state_id'])) {
			$sql .= " AND state_id = '" . $data['filter_state_id'] . "' ";
		}

		if (isset($data['filter_division_ids']) && !empty($data['filter_division_ids'])) {
			$division_string = "'" . str_replace(",", "','", html_entity_decode($data['filter_division_ids'])) . "'";
			$sql .= " AND division_id IN (" . strtolower($division_string) . ") ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(unit) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'unit',
			'unit_code',
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY unit";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;	
		//$this->log->write($sql);
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalUnits($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "unit WHERE 1=1 ";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND unit_id = '" . $data['filter_name_id'] . "' ";
		}

		if (isset($data['filter_division_id']) && !empty($data['filter_division_id'])) {
			$sql .= " AND division_id = '" . $data['filter_division_id'] . "' ";
		}

		if (isset($data['filter_state_id']) && !empty($data['filter_state_id'])) {
			$sql .= " AND state_id = '" . $data['filter_state_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(unit) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		if (isset($data['filter_division_ids']) && !empty($data['filter_division_ids'])) {
			$division_string = "'" . str_replace(",", "','", html_entity_decode($data['filter_division_ids'])) . "'";
			$sql .= " AND division_id IN (" . strtolower($division_string) . ") ";
		}
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getUnits_by_contractor($data){
		$sql = "SELECT * FROM " . DB_PREFIX . "contractor_location WHERE 1=1 ";
		if(!empty($data['filter_contractor_id'])){
			$contractor_string = "'" . str_replace(",", "','", html_entity_decode($data['filter_contractor_id'])) . "'";
			$sql .= " AND contractor_id IN (" . strtolower($contractor_string) . ") ";
		}
		$sql .= " GROUP BY `unit_id` ";
		$query = $this->db->query($sql);
		return $query->rows;	
	}	
}
?>