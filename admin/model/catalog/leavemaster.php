<?php
class ModelCatalogLeavemaster extends Model {
	public function addLeavemaster($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "leavemaster` SET 
							`leavemaster` = '" . $this->db->escape($data['leavemaster']) . "',
							`leavemaster_code` = '" . $this->db->escape($data['leavemaster_code']) . "'
						");

		$leavemaster_id = $this->db->getLastId(); 
	}

	public function editLeavemaster($leavemaster_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "leavemaster SET 
							`leavemaster` = '" . $this->db->escape($data['leavemaster']) . "',
							`leavemaster_code` = '" . $this->db->escape($data['leavemaster_code']) . "'
							WHERE leavemaster_id = '" . (int)$leavemaster_id . "'");
	}

	public function deleteLeavemaster($leavemaster_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "leavemaster WHERE leavemaster_id = '" . (int)$leavemaster_id . "'");
	}	

	public function getLeavemaster($leavemaster_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "leavemaster WHERE leavemaster_id = '" . (int)$leavemaster_id . "'");

		return $query->row;
	}

	public function getLeavemasters($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "leavemaster WHERE 1=1 ";

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND leavemaster_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(leavemaster) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'leavemaster',
			'leavemaster_code',
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY leavemaster";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalLeavemasters() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "leavemaster";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND leavemaster_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(leavemaster) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	
}
?>