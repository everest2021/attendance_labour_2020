<?php
class ModelCatalogDesignation extends Model {
	public function addDesignation($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "designation` SET 
							`d_name` = '" . $this->db->escape(html_entity_decode($data['d_name'])) . "',
							`d_code` = '" . $this->db->escape(html_entity_decode($data['d_code'])) . "', 
							`grade_id` = '" . $this->db->escape(html_entity_decode($data['grade_id'])) . "', 
							`grade_name` = '" . $this->db->escape(html_entity_decode($data['grade_name'])) . "', 
							`status` = '" . $data['status'] . "'
						");

		$designation_id = $this->db->getLastId(); 
	}

	public function editDesignation($designation_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "designation SET 
							`d_name` = '" . $this->db->escape(html_entity_decode($data['d_name'])) . "',
							`d_code` = '" . $this->db->escape(html_entity_decode($data['d_code'])) . "', 
							`grade_id` = '" . $this->db->escape(html_entity_decode($data['grade_id'])) . "', 
							`grade_name` = '" . $this->db->escape(html_entity_decode($data['grade_name'])) . "', 
							`status` = '" . $data['status'] . "' 
							WHERE designation_id = '" . (int)$designation_id . "'");

		$this->db->query("UPDATE " . DB_PREFIX . "employee SET 
							`designation` = '" . $this->db->escape(html_entity_decode($data['d_name'])) . "',
							`designation_id` = '" . $this->db->escape(html_entity_decode($designation_id)) . "'
							WHERE designation_id = '" . (int)$designation_id . "'"); 
	}

	public function deleteDesignation($designation_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "designation WHERE designation_id = '" . (int)$designation_id . "'");
	}	

	public function getDesignation($designation_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "designation WHERE designation_id = '" . (int)$designation_id . "'");

		return $query->row;
	}

	public function getDesignations($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "designation WHERE 1=1 ";

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND designation_id = '" . $data['filter_name_id'] . "' ";
		}

		if (isset($data['filter_grade']) && !empty($data['filter_grade'])) {
			$sql .= " AND grade_id = '" . $data['filter_grade'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(d_name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'd_name',
			'd_code',
			'grade_name',
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY d_name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		//echo $sql;exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalDesignations($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "designation WHERE 1=1 ";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND designation_id = '" . $data['filter_name_id'] . "' ";
		}

		if (isset($data['filter_grade']) && !empty($data['filter_grade'])) {
			$sql .= " AND grade_id = '" . $data['filter_grade'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(d_name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	
}
?>