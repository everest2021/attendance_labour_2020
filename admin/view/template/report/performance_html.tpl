<?php date_default_timezone_set("Asia/Kolkata"); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<?php foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
  <?php if($fkeyss == '0'){ ?>
    <h1 style="text-align:center;font-weight: bold;color: #000;">
      <?php echo $filter_company; ?><br />
      <?php echo $title; ?><br />
      <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
        <?php echo 'Period : '. $date_start . ' - ' . $date_end; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo 'Report Type : '. $report_type; ?><br />
      </span>
      <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
        <?php echo 'Division : '. $filter_division; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Region : '. $filter_region; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Site : '. $filter_unit; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Department : '. $filter_department; ?><br />
      </span>
      <span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
    </h1>
  <?php } ?>
  <table class="product" style="width:100% !important;">
    <tbody>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <td style="padding: 0px 9px;"></td>
            <?php foreach($days as $dkey => $dvalue) { ?>
              <td class="left" style="padding: 0px 9px;font-size:11px;"><?php echo $dvalue['day']; ?></td>
            <?php } ?>
          </tr>
          <tr>
            <td colspan = "<?php echo count($final_data['tran_data']) + 1; ?> " style="font-weight:bold;font-size:11px;">
              <?php echo $final_data['basic_data']['name']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo 'Emp Code : '.$final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo 'Department : '.$final_data['basic_data']['department']; ?>
            </td>
          </tr>
          <?php if(empty($filter_fields) || in_array('1', $filter_fields) ){ ?>
            <tr>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;width:10%;">
                <?php echo 'Shift'; ?>
              </td>
              <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $tvalue['shift_name']; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>

          <?php if(empty($filter_fields) || in_array('2', $filter_fields) ){ ?>
            <tr>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;width:10%;">
                <?php echo 'Shift In'; ?>
              </td>
              <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $tvalue['shift_intime']; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>
          
          <?php if(empty($filter_fields) || in_array('3', $filter_fields) ){ ?>
            <tr>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'Shift Out'; ?>
              </td>
              <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $tvalue['shift_outtime']; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>
          
          <?php if(empty($filter_fields) || in_array('4', $filter_fields) ){ ?>
            <tr>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'Attended In'; ?>
              </td>
              <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $tvalue['act_intime']; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>
          
          <?php if(empty($filter_fields) || in_array('5', $filter_fields) ){ ?>
            <tr>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'Attended Out'; ?>
              </td>
              <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $tvalue['act_outtime']; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>

          <?php if(empty($filter_fields) || in_array('6', $filter_fields) ){ ?>
            <tr>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'First Half Stat'; ?>
              </td>
              <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $tvalue['firsthalf_status']; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>

          <?php if(empty($filter_fields) || in_array('7', $filter_fields) ){ ?>
            <tr>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'Second Half Stat'; ?>
              </td>
              <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $tvalue['secondhalf_status']; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>

          <?php if(empty($filter_fields) || in_array('8', $filter_fields) ){ ?>
            <tr>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'Late'; ?>
              </td>
              <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $tvalue['late_time']; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>

          <?php if(empty($filter_fields) || in_array('9', $filter_fields) ){ ?>
            <tr>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'Early'; ?>
              </td>
              <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $tvalue['early_time']; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>

          <?php if(empty($filter_fields) || in_array('10', $filter_fields) ){ ?>
            <tr style="border-bottom:2px solid black;">
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'Working hours'; ?>
              </td>
              <?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $tvalue['working_time']; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>
          <tr style="border-bottom:2px solid black;">
            <td>
            </td>
            <td colspan = "<?php echo count($final_data['tran_data']); ?>" >
            </td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php } ?>
</body>
</html>