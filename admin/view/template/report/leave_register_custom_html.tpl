<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body style="width:100%;">
<?php //foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo 'VVMC'; ?><br /><?php echo 'LEAVE REGISTER'; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Period : '. $filter_date_start . ' - ' . $filter_date_end; ?></p><br/>
    <p style="text-align:center;font-size:15px;"><?php echo $filter_leave; ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <tr>
        <td class="left" style="font-weight:bold;font-size:12px;">Sr.No</td>
        <td class="left" style="font-weight:bold;font-size:12px;">Tkt No</td>
        <td class="left" style="font-weight:bold;font-size:12px;">Name</td>
        <td class="left" style="font-weight:bold;font-size:12px;">OP</td>
        <td class="left" style="font-weight:bold;font-size:12px;">Consumed</td>
        <td class="left" style="font-weight:bold;font-size:12px;">Closing</td>
      </tr>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $i; ?>
            </td>
            <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['emp_code']; ?>
            </td>
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['name']; ?>
            </td>
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['open']; ?>
            </td>
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['total_days_consumed']; ?>
            </td>
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['closing_bal']; ?>
            </td>
          </tr>
        <?php $i++ ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php //} ?>
</body>
</html>