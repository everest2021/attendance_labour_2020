<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
  	</div>
  	<?php if ($error_warning) { ?>
  		<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
	<?php if ($success) { ?>
		<div class="success"><?php echo $success; ?></div>
	<?php } ?>
  	<div class="box">
		<div class="heading">
	  		<h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
		</div>
	<div class="content sales-report">
	  	<table class="form">
			<tr>
				<td style="width:6%;"><?php echo "Date Start"; ?>
					<input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="8" />
		 	 	</td>
		 	 	<td style="width:6%;"><?php echo "Date End"; ?>
					<input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="8" />
		 	 	</td>
		 	 	<td style="width:6%">Division
					<select name="division" id="filter_division" style="width: 120px;">
					<?php foreach($division_datas as $key => $ud) { ?>
						<?php if($ud['division_id'] == $filter_division) { ?>
						  	<option value="<?php echo $ud['division_id']; ?>" selected="selected"><?php echo $ud['division_name']; ?></option> 
						<?php } else { ?>
						  	<option value="<?php echo $ud['division_id']; ?>"><?php echo $ud['division_name']; ?></option>
						<?php } ?>
					<?php } ?>
					</select>
				</td>
			  	<td style="width:6%">Site
					<select name="location" id="filter_location" style="width: 110px;">
				  	<?php foreach($location_datas as $key => $ud) { ?>
						<?php if($ud['location_id'] == $filter_location) { ?>
					  		<option value="<?php echo $ud['location_id']; ?>" selected="selected"><?php echo $ud['location_name']; ?></option> 
						<?php } else { ?>
					  		<option value="<?php echo $ud['location_id']; ?>"><?php echo $ud['location_name']; ?></option>
						<?php } ?>
				  	<?php } ?>
					</select>
			  	</td>
			 	<td style="width:6%">Contractor Name
					<select name="contractor" id="filter_contractor" style="width: 365px;">
			  		<?php foreach($contractor_datas as $ckey => $cd) { ?>
						<?php if($cd['contractor_name'] == $filter_contractor) { ?>
				  			<option value="<?php echo $cd['contractor_name']; ?>" selected="selected"><?php echo $cd['contractor_name'].'-'.$cd['contractor_code']; ?></option> 
						<?php } else { ?>
				  			<option value="<?php echo $cd['contractor_name']; ?>"><?php echo $cd['contractor_name'].'-'.$cd['contractor_code']; ?></option>
						<?php } ?>
			  		<?php } ?>
					</select>
		  		</td>
		  		<td style="width:9%">Labour Category
					<select name="category" id="filter_category" style="width: 125px;">
					<?php foreach($category_datas as $ckey => $cd) { ?>
						<?php if($cd['category_id'] == $filter_category) { ?>
						  	<option value="<?php echo $cd['category_id']; ?>" selected="selected"><?php echo $cd['category_name']; ?></option> 
						<?php } else { ?>
						  	<option value="<?php echo $cd['category_id']; ?>"><?php echo $cd['category_name']; ?></option>
						<?php } ?>
					 <?php } ?>
					</select>
				</td>
			  	<td style="width:6%">Designation
					<select name="designation" id="filter_designation" style="width:150px;">
				  	<?php foreach($designation_datas as $key => $dd) { ?>
						<?php if($dd['designation_id'] == $filter_designation) { ?>
					  		<option value="<?php echo $dd['designation_id']; ?>" selected="selected"><?php echo $dd['designation_name']; ?></option> 
						<?php } else { ?>
					  		<option value="<?php echo $dd['designation_id']; ?>"><?php echo $dd['designation_name']; ?></option>
						<?php } ?>
				  	<?php } ?>
					</select>
			  	</td>
				<td style="width:6%">Fields
            		<select multiple name="filter_fields[]" id="input-filter_fields" style="display:inline;">
	              	<?php foreach($fields_data as $tkey => $tvalue) { ?>
	                	<?php if (in_array($tkey, $filter_fields)) { ?>
	                  		<option value="<?php echo $tkey; ?>" selected="selected"><?php echo $tvalue; ?></option> 
	                	<?php } else { ?>
	                  		<option value="<?php echo $tkey; ?>"><?php echo $tvalue; ?></option>
	                	<?php } ?>
	              	<?php } ?>
	            	</select>
	          	</td>
		  		<td style="text-align: right;">
					<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
					<a style="padding: 13px 25px;" onclick="filter_export();" id="filter" class="button"><?php echo 'Export'; ?></a>
					<a style="padding: 13px 25px;display: none;" onclick="filter_hrms_export();" id="filter" class="button"><?php echo 'Export HRMS'; ?></a>
				</td>
			</tr>
	  	</table>
	  	<div style="width: 1459px;overflow-x: scroll;overflow-y: hidden;">
			<table class="list" style="width: 100%;">
			  	<?php $count = 0; ?>
			  	<?php if($final_datas) { ?>
				  	<thead>
						<tr>
				            <?php if(empty($filter_fields) || in_array('1', $filter_fields) ){ ?>
				                <?php $count ++; ?>
				                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Division</td>
				            <?php } ?>
				            <?php if(empty($filter_fields) || in_array('2', $filter_fields) ){ ?>
				                <?php $count ++; ?>
				                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Site</td>
				            <?php } ?>
				            <?php if(empty($filter_fields) || in_array('3', $filter_fields) ){ ?>
				                <?php $count ++; ?>
				                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Contractor Code</td>
				            <?php } ?>
				            <?php if(empty($filter_fields) || in_array('4', $filter_fields) ){ ?>
				                <?php $count ++; ?>
				                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Contractor Name</td>
				            <?php } ?>
				            <?php if(empty($filter_fields) || in_array('5', $filter_fields) ){ ?>
				                <?php $count ++; ?>
				                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Date</td>
				            <?php } ?>
				            <?php if(empty($filter_fields) || in_array('6', $filter_fields) ){ ?>
				                <?php $count ++; ?>
				                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Labour Category</td>
				            <?php } ?>
				            <?php if(empty($filter_fields) || in_array('7', $filter_fields) ){ ?>
				                <?php $count ++; ?>
				                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Designation</td>
				            <?php } ?>
				             <?php if(empty($filter_fields) || in_array('8', $filter_fields) ){ ?>
				                <?php $count ++; ?>
				                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Total Present Count</td>
				            <?php } ?>
			            </tr>
				  	</thead>
			  	<?php } ?>
			  	<tbody>
					<?php if($final_datas) { ?>
					<?php $i = 1; ?>
						<?php foreach($final_datas as $fkey => $final_data){ ?>
							<tr>
			                    <?php if(empty($filter_fields) || in_array('1', $filter_fields) ){ ?>
			                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['division'] ?></td>
			                    <?php } ?>
			                    <?php if(empty($filter_fields) || in_array('2', $filter_fields) ){ ?>
			                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['location'] ?></td>
			                    <?php } ?>
			                    <?php if(empty($filter_fields) || in_array('3', $filter_fields) ){ ?>
			                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['contractor_code'] ?></td>
			                    <?php } ?>
			                    <?php if(empty($filter_fields) || in_array('4', $filter_fields) ){ ?>
			                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['contractor_name'] ?></td>
			                    <?php } ?>
			                    <?php if(empty($filter_fields) || in_array('5', $filter_fields) ){ ?>
			                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['date'] ?></td>
			                    <?php } ?>
			                    <?php if(empty($filter_fields) || in_array('6', $filter_fields) ){ ?>
			                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['category_name'] ?></td>
			                    <?php } ?>
			                    <?php if(empty($filter_fields) || in_array('7', $filter_fields) ){ ?>
			                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['designation'] ?></td>
			                    <?php } ?>
			                    <?php if(empty($filter_fields) || in_array('8', $filter_fields) ){ ?>
			                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['total_count'] ?></td>
			                    <?php } ?>
		                  	</tr>
						<?php $i++; ?>
						<?php } ?>
					<?php } else { ?>
						<tr>
					  		<td class="center" colspan = "<?php echo $count; ?> "><?php echo $text_no_results; ?></td>
						</tr>
					<?php } ?>
			  	</tbody>
			</table>
			<!-- <div class="pagination"><?php echo $pagination; ?></div> -->
	  	</div>
	</div>
  	</div>
</div>
<script type="text/javascript"><!--
function filter() {
  	url = 'index.php?route=report/contractor&token=<?php echo $token; ?>';
  
  	var filter_contractor = $('#filter_contractor').val();
  	if (filter_contractor && filter_contractor != 'All') {
		url += '&filter_contractor=' + encodeURIComponent(filter_contractor);
  	}

  	var filter_date_start = $('#date-start').val();
  	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  	}

  	var filter_date_end = $('#date-end').val();
  	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  	}

 	var filter_designation = $('#filter_designation').val();
  	if (filter_designation && filter_designation != '0') {
		url += '&filter_designation=' + encodeURIComponent(filter_designation);
  	}

   	var filter_division = $('#filter_division').val();
  	if (filter_division && filter_division != '0') {
		url += '&filter_division=' + encodeURIComponent(filter_division);
  	}

   	var filter_location = $('#filter_location').val();
  	if (filter_location && filter_location != '0') {
		url += '&filter_location=' + encodeURIComponent(filter_location);
  	}

   	var filter_category = $('#filter_category').val();
  	if (filter_category && filter_category != '0') {
		url += '&filter_category=' + encodeURIComponent(filter_category);
  	}
 
  	filter_fields_selected = $('#input-filter_fields').val();
  	field_ids = '';
  	if(filter_fields_selected){
		max_length = filter_fields_selected.length - 1;
		for(i=0; i<filter_fields_selected.length; i++){
	  		if(max_length == i){  
				field_ids += filter_fields_selected[i];
	  		} else {
				field_ids += filter_fields_selected[i]+',';
	  		}
		}
		if (field_ids) {
	  		url += '&filter_fields=' + encodeURIComponent(field_ids);
		}
  	}
  	url += '&once=1';
  	//alert(url);
  	//return false;
  	location = url;
  	return false;
}

function filter_export() {
  	url = 'index.php?route=report/contractor/export&token=<?php echo $token; ?>';
  
  	var filter_contractor = $('#filter_contractor').val();
  	if (filter_contractor && filter_contractor != 'All') {
		url += '&filter_contractor=' + encodeURIComponent(filter_contractor);
  	}

  	var filter_date_start = $('#date-start').val();
  	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  	}

  	var filter_date_end = $('#date-end').val();
  	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  	}

 	var filter_designation = $('#filter_designation').val();
  	if (filter_designation && filter_designation != '0') {
		url += '&filter_designation=' + encodeURIComponent(filter_designation);
  	}

   	var filter_division = $('#filter_division').val();
  	if (filter_division && filter_division != '0') {
		url += '&filter_division=' + encodeURIComponent(filter_division);
  	}

   	var filter_location = $('#filter_location').val();
  	if (filter_location && filter_location != '0') {
		url += '&filter_location=' + encodeURIComponent(filter_location);
  	}

   	var filter_category = $('#filter_category').val();
  	if (filter_category && filter_category != '0') {
		url += '&filter_category=' + encodeURIComponent(filter_category);
  	}
 
  	filter_fields_selected = $('#input-filter_fields').val();
  	field_ids = '';
  	if(filter_fields_selected){
		max_length = filter_fields_selected.length - 1;
		for(i=0; i<filter_fields_selected.length; i++){
	  		if(max_length == i){  
				field_ids += filter_fields_selected[i];
	  		} else {
				field_ids += filter_fields_selected[i]+',';
	  		}
		}
		if (field_ids) {
	  		url += '&filter_fields=' + encodeURIComponent(field_ids);
		}
  	}  
  
  	location = url;
  	return false;
}

jQuery.browser = {};
(function () {
	jQuery.browser.msie = false;
	jQuery.browser.version = 0;
	if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
		jQuery.browser.msie = true;
		jQuery.browser.version = RegExp.$1;
	}
})();

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  	$('#input-filter_fields').multiselect({
		includeSelectAllOption: true,
		enableFiltering: true,
		maxHeight: 235,
  	});
  	$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  	$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script>
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
	_renderMenu: function(ul, items) {
		var self = this, currentCategory = '';
		$.each(items, function(index, item) {
		  	if (item.category != currentCategory) {
				//ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
				currentCategory = item.category;
		 	}
		  	self._renderItem(ul, item);
		});
  	}
});

$('#filter_division').on('change', function() {
	division = $(this).val();
	$.ajax({
		url: 'index.php?route=report/contractor/getlocation&token=<?php echo $token; ?>&filter_division_id=' +  encodeURIComponent(division),
		dataType: 'json',
		success: function(json) {   
			$('#filter_location').find('option').remove();
			if(json['location_datas']){
				$.each(json['location_datas'], function (i, item) {
					$('#filter_location').append($('<option>', { 
		  				value: item.location_id,
		  				text : item.location_name 
	  				}));
				});
			}
			//$('#filter_contractor').find('option').remove();
			if(json['contractor_datas']){
				$.each(json['contractor_datas'], function (i, item) {
					$('#filter_contractor').append($('<option>', { 
		  				value: item.contractor_id,
		  				text : item.contractor_name+'-'+item.contractor_code, 
	  				}));
				});
			}
		}
	});
});

$('#filter_location').on('change', function() {
	filter_location = $(this).val();
	$.ajax({
	url: 'index.php?route=report/contractor/getcontractors&token=<?php echo $token; ?>&filter_location_id=' +  encodeURIComponent(filter_location),
	dataType: 'json',
		success: function(json) {   
			//alert('aaaa');
			$('#filter_contractor').find('option').remove();
			if(json['contractor_datas']){
				$.each(json['contractor_datas'], function (i, item) {
					$('#filter_contractor').append($('<option>', { 
		  				value: item.contractor_id,
		  				text : item.contractor_name+'-'+item.contractor_code, 
	  				}));
				});
			}
		}
	});
});

//--></script>
<?php echo $footer; ?>