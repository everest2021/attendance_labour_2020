<?php date_default_timezone_set("Asia/Kolkata"); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  <h1 style="text-align:center;font-weight: bold;color: #000;">
    <?php echo $filter_company; ?><br />
    <?php echo $title; ?><br />
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
      <?php echo 'For Month : '. $month_of; ?><br />
    </span>
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
      <?php echo 'Division : '. $filter_division; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Region : '. $filter_region; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Site : '. $filter_unit; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Department : '. $filter_department; ?><br />
    </span>
    <span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
  </h1>
  <table class="product" style="width:100% !important;">
    <?php $count = 0; ?>
    <?php if($final_datas) { ?>
    <thead>
      <tr>
        <?php if(empty($filter_fields) || in_array('1', $filter_fields) ){ ?>
          <?php $count ++; ?>
          <td style="padding: 0px 9px;font-size: 11px">Employee Id</td>
        <?php } ?>
        <?php if(empty($filter_fields) || in_array('8', $filter_fields) ){ ?>
          <?php $count ++; ?>
          <td style="padding: 0px 9px;font-size: 11px">Punch Id</td>
        <?php } ?>
        <?php if(empty($filter_fields) || in_array('2', $filter_fields) ){ ?>
          <?php $count ++; ?>
          <td style="padding: 0px 9px;font-size: 11px">Employee Name</td>
        <?php } ?>
        <?php if(empty($filter_fields) || in_array('3', $filter_fields) ){ ?>
          <?php $count ++; ?>
          <td style="padding: 0px 9px;font-size: 11px">Punch Id</td>
        <?php } ?>
        <?php if(empty($filter_fields) || in_array('4', $filter_fields) ){ ?>
          <?php $count ++; ?>
          <td style="padding: 0px 9px;font-size: 11px">Division</td>
        <?php } ?>
        <?php if(empty($filter_fields) || in_array('5', $filter_fields) ){ ?>
          <?php $count ++; ?>
          <td style="padding: 0px 9px;font-size: 11px">Region</td>
        <?php } ?>
        <?php if(empty($filter_fields) || in_array('6', $filter_fields) ){ ?>
          <?php $count ++; ?>
          <td style="padding: 0px 9px;font-size: 11px">Site</td>
        <?php } ?>
        <?php if(empty($filter_fields) || in_array('7', $filter_fields) ){ ?>
          <?php $count ++; ?>
          <td style="padding: 0px 9px;font-size: 11px">Department</td>
        <?php } ?>
        <?php $count ++; ?>
        <td class="left" style="padding: 0px 9px;font-size:11px;">Avg Hours</td>
      </tr>
    </thead>
    <?php } ?>
    <tbody>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <?php if(empty($filter_fields) || in_array('1', $filter_fields) ){ ?>
              <td style="padding: 0px 9px;font-size: 10px;text-align:left;"><?php echo $final_data['emp_code'] ?></td>
            <?php } ?>
            <?php if(empty($filter_fields) || in_array('8', $filter_fields) ){ ?>
              <td style="padding: 0px 9px;font-size: 10px;text-align:left;"><?php echo $final_data['emp_code'] ?></td>
            <?php } ?>
            <?php if(empty($filter_fields) || in_array('2', $filter_fields) ){ ?>
              <td style="padding: 0px 9px;font-size: 10px;text-align:left;"><?php echo $final_data['name'] ?></td>
            <?php } ?>
            <?php if(empty($filter_fields) || in_array('3', $filter_fields) ){ ?>
              <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['emp_code'] ?></td>
            <?php } ?>
            <?php if(empty($filter_fields) || in_array('4', $filter_fields) ){ ?>
              <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['division'] ?></td>
            <?php } ?>
            <?php if(empty($filter_fields) || in_array('5', $filter_fields) ){ ?>
              <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['region'] ?></td>
            <?php } ?>
            <?php if(empty($filter_fields) || in_array('6', $filter_fields) ){ ?>
              <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['unit'] ?></td>
            <?php } ?>
            <?php if(empty($filter_fields) || in_array('7', $filter_fields) ){ ?>
              <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['department'] ?></td>
            <?php } ?>
            <td class="left" style="padding: 0px 9px;font-size:11px;"><?php echo $final_data['averageworking_time']; ?></td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="center" colspan = "<?php echo $count; ?>"><?php echo $text_no_results; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div></body></html>