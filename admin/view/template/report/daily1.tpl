<?php echo $header; ?>
<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<?php if ($success) { ?>
	<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	<div class="box">
		<div class="heading">
			<h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
		</div>
		<div class="content sales-report">
			<table class="form">
				<tr>
					<td style="width:10%;"><?php echo "Name / Emp Code"; ?>
						<?php if(isset($this->session->data['emp_code'])) { ?>
							<input readonly="readonly" type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="22" />
						<?php } else { ?>
							<input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="22" />
						<?php } ?>
						<input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
					</td>
					<td style="width:6%;"><?php echo "Date Start"; ?>
						<input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="8" />
					</td>
					<td style="width:6%;"><?php echo "Date End"; ?>
						<input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="8" />
					</td>
					<td style="width:6%">Company
						<select multiple name="filter_company[]" id="input-filter_company" style="display:inline;">
							<?php foreach($company_data as $key => $ud) { ?>
								<?php if (in_array($key, $filter_company)) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%;display: none;">Region
						<select name="filter_region" id="filter_region">
							<?php foreach($region_data as $key => $ud) { ?>
								<?php if($key == $filter_region) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%">Division
						<select name="filter_division" id="filter_division">
							<?php foreach($division_data as $key => $ud) { ?>
								<?php if($key == $filter_division) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%">Site
						<select name="filter_unit" id="filter_unit">
							<?php foreach($unit_data as $key => $ud) { ?>
								<?php if($key == $filter_unit) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%">Contractor
						<select multiple name="filter_contractor[]" id="input-filter_contractor" style="display:inline;">
							<?php foreach($contractor_data as $key => $ud) { ?>
								<?php if (in_array($key, $filter_contractor)) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%">Department
						<select name="filter_department" id="filter_department" style="width:100%;">
							<?php foreach($department_data as $key => $dd) { ?>
								<?php if($key == $filter_department) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%;">Status
						<select name="status" id="status">
							<?php foreach($statuses as $key => $gd) { ?>
								<?php if($key == $status) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $gd; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $gd; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>

					<td style="width:6%">Fields
						<select multiple name="filter_fields[]" id="input-filter_fields" style="display:inline;">
							<?php foreach($fields_data as $tkey => $tvalue) { ?>
								<?php if (in_array($tkey, $filter_fields)) { ?>
									<option value='<?php echo $tkey; ?>' selected="selected"><?php echo $tvalue; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $tkey; ?>'><?php echo $tvalue; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>

					<td style="text-align: right;">
						<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
						<a style="padding: 13px 25px;" onclick="filter_export();" id="filter" class="button"><?php echo 'Export'; ?></a>
						<a style="padding: 13px 25px;display: none;" onclick="filter_hrms_export();" id="filter" class="button"><?php echo 'Export HRMS'; ?></a>
					</td>
				</tr>
			</table>
			<div style="width: 1459px;overflow-x: scroll;overflow-y: hidden;">
				<table class="list" style="width: 100%;">
					<?php $count = 1; ?>
					<?php if($final_datass) { ?>
						<thead>
							<tr>
								<td style="padding: 0px 9px;font-size: 11px">Sr.No</td>
								<?php $count ++; ?>
								<?php if(empty($filter_fields) || in_array('1', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Punch Id</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('2', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Employee Name</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('3', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Category</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('4', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Site</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('5', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Region</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('6', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Division</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('7', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Department</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('8', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Designation</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('9', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Employement</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('10', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Contractor</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('11', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Contractor Code</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('12', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Shift Code</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('13', $filter_fields) ){ ?>
									<td style="padding: 0px 9px;font-size: 11px">Attend Date</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('14', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">In Time</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('15', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Out Time</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('16', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Late Time</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('17', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Early Time</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('18', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Total Hour</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('19', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Status</td>
								<?php } ?>
								<?php if(empty($filter_fields) || in_array('20', $filter_fields) ){ ?>
									<?php $count ++; ?>
									<td style="padding: 0px 9px;font-size: 11px">Remark</td>
								<?php } ?>
							</tr>
						</thead>
					<?php } ?>
					<tbody>
						<?php if($final_datass) { ?>
							<?php $i = 1; ?>
							<?php foreach ($final_datass as $fkeyss => $final_datas ) { ?>
								<?php foreach($final_datas as $final_dat) { ?>
									<?php foreach($final_dat['tran_datas'] as $tkey => $final_data){ ?>
										<tr>
											<td style="padding: 0px 9px;font-size: 10px"><?php echo $i; ?></td>
											<?php if(empty($filter_fields) || in_array('1', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['emp_id'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('2', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['emp_name'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('3', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['category'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('4', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['unit'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('5', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['region'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('6', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['division'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('7', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['department'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('8', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['designation'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('9', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['employement'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('10', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['contractor'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('11', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['contractor_code'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('12', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['shift_code'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('13', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['date'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('14', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['act_intime'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('15', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['act_outtime'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('16', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['late_time'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('17', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['early_time'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('18', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['working_time'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('19', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['status_name'] ?></td>
											<?php } ?>
											<?php if(empty($filter_fields) || in_array('20', $filter_fields) ){ ?>
												<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['remark'] ?></td>
											<?php } ?>
										</tr>
										<?php $i++; ?>
									<?php } ?>
								<?php } ?>
							<?php } ?>
						<?php } else { ?>
							<tr>
								<td class="center" colspan = "<?php echo $count; ?> "><?php echo $text_no_results; ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<!-- <div class="pagination"><?php //echo $pagination; ?></div> -->
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=report/daily1&token=<?php echo $token; ?>';
	
	var filter_name = $('#filter_name').val();
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
		var filter_name_id = $('#filter_name_id').val();
		if (filter_name_id) {
			url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
		} else {
			alert('Please Enter Correct Employee Name');
			return false;
		}
	}

	var filter_date_start = $('#date-start').val();
	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('#date-end').val();
	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}

	var filter_unit = $('#filter_unit').val();
	if (filter_unit && filter_unit != '0') {
		url += '&filter_unit=' + encodeURIComponent(filter_unit);
	}

	var filter_department = $('#filter_department').val();
	if (filter_department && filter_department != '0') {
		url += '&filter_department=' + encodeURIComponent(filter_department);
	}

	var filter_division = $('#filter_division').val();
	if (filter_division && filter_division != '0') {
		url += '&filter_division=' + encodeURIComponent(filter_division);
	}

	var filter_region = $('#filter_region').val();
	if (filter_region && filter_region != '0') {
		url += '&filter_region=' + encodeURIComponent(filter_region);
	}

	company_selected = $('#input-filter_company').val();
	field_ids = '';
	if(company_selected){
		max_length = company_selected.length - 1;
		for(i=0; i<company_selected.length; i++){
			if(max_length == i){  
				field_ids += company_selected[i];
			} else {
				field_ids += company_selected[i]+',';
			}
		}
		if (field_ids) {
			url += '&filter_company=' + encodeURIComponent(field_ids);
		}
	}

	contractor_selected = $('#input-filter_contractor').val();
	field_ids = '';
	if(contractor_selected){
		max_length = contractor_selected.length - 1;
		for(i=0; i<contractor_selected.length; i++){
			if(max_length == i){  
				field_ids += contractor_selected[i];
			} else {
				field_ids += contractor_selected[i]+',';
			}
		}
		if (field_ids) {
			url += '&filter_contractor=' + encodeURIComponent(field_ids);
		}
	}

	var status = $('#status').val();
	if (status) {
		url += '&status=' + encodeURIComponent(status);
	}

	filter_fields_selected = $('#input-filter_fields').val();
	field_ids = '';
	if(filter_fields_selected){
		max_length = filter_fields_selected.length - 1;
		for(i=0; i<filter_fields_selected.length; i++){
			if(max_length == i){  
				field_ids += filter_fields_selected[i];
			} else {
				field_ids += filter_fields_selected[i]+',';
			}
		}
		if (field_ids) {
			url += '&filter_fields=' + encodeURIComponent(field_ids);
		}
	}
	url += '&once=1';
	
	//alert(url);
	//return false;

	location = url;
	return false;
}

function filter_export() {
	url = 'index.php?route=report/daily1/export&token=<?php echo $token; ?>';
	
	var filter_name = $('#filter_name').val();
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
		var filter_name_id = $('#filter_name_id').val();
		if (filter_name_id) {
			url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
		} else {
			alert('Please Enter Correct Employee Name');
			return false;
		}
	}

	var filter_date_start = $('#date-start').val();
	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('#date-end').val();
	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}

	var filter_unit = $('#filter_unit').val();
	if (filter_unit && filter_unit != '0') {
		url += '&filter_unit=' + encodeURIComponent(filter_unit);
	}

	var filter_department = $('#filter_department').val();
	if (filter_department && filter_department != '0') {
		url += '&filter_department=' + encodeURIComponent(filter_department);
	}

	var filter_division = $('#filter_division').val();
	if (filter_division && filter_division != '0') {
		url += '&filter_division=' + encodeURIComponent(filter_division);
	}

	var filter_region = $('#filter_region').val();
	if (filter_region && filter_region != '0') {
		url += '&filter_region=' + encodeURIComponent(filter_region);
	}

	company_selected = $('#input-filter_company').val();
	field_ids = '';
	if(company_selected){
		max_length = company_selected.length - 1;
		for(i=0; i<company_selected.length; i++){
			if(max_length == i){  
				field_ids += company_selected[i];
			} else {
				field_ids += company_selected[i]+',';
			}
		}
		if (field_ids) {
			url += '&filter_company=' + encodeURIComponent(field_ids);
		}
	}

	contractor_selected = $('#input-filter_contractor').val();
	field_ids = '';
	if(contractor_selected){
		max_length = contractor_selected.length - 1;
		for(i=0; i<contractor_selected.length; i++){
			if(max_length == i){  
				field_ids += contractor_selected[i];
			} else {
				field_ids += contractor_selected[i]+',';
			}
		}
		if (field_ids) {
			url += '&filter_contractor=' + encodeURIComponent(field_ids);
		}
	}

	var status = $('#status').val();
	if (status) {
		url += '&status=' + encodeURIComponent(status);
	}

	filter_fields_selected = $('#input-filter_fields').val();
	field_ids = '';
	if(filter_fields_selected){
		max_length = filter_fields_selected.length - 1;
		for(i=0; i<filter_fields_selected.length; i++){
			if(max_length == i){  
				field_ids += filter_fields_selected[i];
			} else {
				field_ids += filter_fields_selected[i]+',';
			}
		}
		if (field_ids) {
			url += '&filter_fields=' + encodeURIComponent(field_ids);
		}
	}  
	location = url;
	return false;
}

jQuery.browser = {};
(function () {
		jQuery.browser.msie = false;
		jQuery.browser.version = 0;
		if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
				jQuery.browser.msie = true;
				jQuery.browser.version = RegExp.$1;
		}
})();

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	$('#input-filter_fields, #input-filter_company, #input-filter_contractor').multiselect({
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering : true,
		enableFiltering: true,
		maxHeight: 235,
	});
	//$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
	//$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

	$(function() {
			//$.datepicker.setDefaults($.datepicker.regional['en']);
			$('#date-start').datepicker({
						dateFormat: 'yy-mm-dd',
						onSelect: function(selectedDate) {
							var date = $(this).datepicker('getDate');
							$('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
							date.setDate(date.getDate() + 30); // Add 30 days
							$('#date-end').datepicker('setDate', date); // Set as default
						}
			});
			
			$('#date-end').datepicker({
						dateFormat: 'yy-mm-dd',
						onSelect: function(selectedDate) {
							$('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
						}
			});
			
	});
	
});
//--></script>
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
	_renderMenu: function(ul, items) {
		var self = this, currentCategory = '';
		$.each(items, function(index, item) {
			if (item.category != currentCategory) {
				//ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
				currentCategory = item.category;
			}
			self._renderItem(ul, item);
		});
	}
});

$('input[name=\'filter_name\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/daily/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {   
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.emp_code
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_name\']').val(ui.item.label);
		$('input[name=\'filter_name_id\']').val(ui.item.value);
		return false;
	},
	focus: function(event, ui) {
		return false;
	}
});

$('#filter_region').on('change', function() {
	filter_region = $(this).val();
	$.ajax({
		url: 'index.php?route=catalog/employee/getdivision_location&token=<?php echo $token; ?>&filter_region_id=' +  encodeURIComponent(filter_region),
		dataType: 'json',
		success: function(json) {   
			$('#filter_division').find('option').remove();
			if(json['division_datas']){
				$.each(json['division_datas'], function (i, item) {
					$('#filter_division').append($('<option>', { 
						value: item.division_id,
						text : item.division 
					}));
				});
			}
			$('#unit').find('option').remove();
			if(json['unit_datas']){
				$.each(json['unit_datas'], function (i, item) {
					$('#filter_unit').append($('<option>', { 
						value: item.unit_id,
						text : item.unit 
					}));
				});
			}
		}
	});
});

$('#filter_division').on('change', function() {
	filter_division = $(this).val();
	$.ajax({
		url: 'index.php?route=catalog/employee/getlocation&token=<?php echo $token; ?>&filter_division_id=' +  encodeURIComponent(filter_division),
		dataType: 'json',
		success: function(json) {   
			$('#filter_unit').find('option').remove();
			if(json['unit_datas']){
				$.each(json['unit_datas'], function (i, item) {
					$('#filter_unit').append($('<option>', { 
						value: item.unit_id,
						text : item.unit 
					}));
				});
			}
		}
	});
});

$('#filter_unit').on('change', function() {
	filter_unit = $(this).val();
	$.ajax({
		url: 'index.php?route=catalog/employee/getcontractors_by_unit_report&token=<?php echo $token; ?>&filter_unit_id=' +  encodeURIComponent(filter_unit),
		dataType: 'json',
		success: function(json) {   
			$("#input-filter_contractor").multiselect("destroy");
			$('#input-filter_contractor').find('option').remove();
			if(json['contractor_datas']){
				$.each(json['contractor_datas'], function (i, item) {
					// $('#input-filter_contractor').multiSelect('addOption', { value: 42, text: 'test 42', index: 0 });
					$('#input-filter_contractor').append($('<option>', { 
						value: item.contractor_id,
						text : item.contractor_name 
					}));
				});
			}
			$("#input-filter_contractor").multiselect({
				includeSelectAllOption: true,
				enableCaseInsensitiveFiltering : true,
				enableFiltering: true,
				maxHeight: 235,
			});
		}
	});
});

/*
$('#input-filter_contractor').on('change', function() {
	contractor_selected = $(this).val();
	field_ids = '';
	if(contractor_selected){
		max_length = contractor_selected.length - 1;
		for(i=0; i<contractor_selected.length; i++){
			if(max_length == i){  
				field_ids += contractor_selected[i];
			} else {
				field_ids += contractor_selected[i]+',';
			}
		}
	}
	filter_contractor = field_ids;
	$.ajax({
		url: 'index.php?route=catalog/employee/getlocation_by_contractor&token=<?php echo $token; ?>&filter_contractor_id=' +  encodeURIComponent(filter_contractor),
		dataType: 'json',
		success: function(json) {   
			$('#filter_unit').find('option').remove();
			if(json['unit_datas']){
				$.each(json['unit_datas'], function (i, item) {
					$('#filter_unit').append($('<option>', { 
						value: item.unit_id,
						text : item.unit 
					}));
				});
			}
		}
	});
});
*/

//--></script>
<?php echo $footer; ?>