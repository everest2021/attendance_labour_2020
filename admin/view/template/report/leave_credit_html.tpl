<?php date_default_timezone_set("Asia/Kolkata"); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  <table class="product" style="width:100% !important;">
    <tr>
      <td colspan="5">
        <h1 style="text-align:center;font-weight: bold;color: #000;">
          <?php echo $filter_company; ?><br />
          <?php echo $title; ?><br />
          <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
            <?php echo 'Division : '. $filter_division; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Region : '. $filter_region; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Site : '. $filter_unit; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Department : '. $filter_department; ?><br />
          </span>
          <span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
        </h1>
      </td>
    </tr>
    <?php if($final_datas) { ?>
      <?php $i = 1; ?>
      <?php foreach($final_datas as $final_data) { ?>
        <tr>
          <td class="left" style="width:20%;font-weight:bold;font-size:12px;">Employee Id : <?php echo $final_data['basic_data']['emp_code']; ?> &nbsp;-&nbsp; <?php echo $final_data['basic_data']['name']; ?></td>
          <td class="left" style="width:9%;font-weight:bold;font-size:12px;text-align: right;">Opening Balance</td>
          <td class="left" style="width:9%;font-weight:bold;font-size:12px;text-align: right;">PL Credit</td>
          <td class="left" style="width:9%;font-weight:bold;font-size:12px;text-align: right;">PL Availed</td>
          <td class="left" style="width:9%;font-weight:bold;font-size:12px;text-align: right;">Balance</td>
        </tr>
        <tr>
          <td class="left">
            Opening Balance
          </td>
          <td style="padding: 0px 9px;font-weight:bold;font-size:12px;text-align: right;">
            <?php echo $final_data['basic_data']['pl_open']; ?>
          </td>
          <td>
            &nbsp;
          </td>
          <td>
            &nbsp;
          </td>
          <td>
            &nbsp;
          </td>
        </tr>
        <?php 
          $total_pl_credit = 0;
          $total_pl_availed = 0;
        ?>
        <?php if(isset($final_data['tran_data']['action'])) { ?>
          <?php foreach($final_data['tran_data']['action'] as $tkey => $tvalue) { ?>
            <tr>
              <td style="padding: 0px 9px;font-size:11px;text-align: left;">
                <?php echo $tvalue['month_name']; ?>
              </td>
              <td style="padding: 0px 9px;font-size:11px;">
                &nbsp;
              </td>
              <td style="padding: 0px 9px;font-size:11px;text-align: right;">
                <?php echo $tvalue['credit_pl']; ?>
              </td>
              <td style="padding: 0px 9px;font-size:11px;text-align: right;">
                <?php echo $tvalue['taken_pl']; ?>
              </td>
              <td style="padding: 0px 9px;font-size:11px;text-align: right;">
                <?php echo $tvalue['balance_pl']; ?>
              </td>
            </tr>
            <?php 
              $total_pl_credit = $total_pl_credit + $tvalue['credit_pl'];
              $total_pl_availed = $total_pl_availed + $tvalue['taken_pl'];
            ?>
          <?php } ?>
        <?php } ?>
        <tr>
          <td style="padding: 0px 9px; font-weight:bold;font-size:12px;text-align: left;">
            <?php echo 'Total'; ?>
          </td>
          <td style="padding: 0px 9px;font-weight:bold;font-size:12px;text-align: right;">
            <?php echo $final_data['basic_data']['pl_open']; ?>
          </td>
          <td style="padding: 0px 9px;font-weight:bold;font-size:12px;text-align: right;">
            <?php echo $total_pl_credit; ?>
          </td>
          <td style="padding: 0px 9px;font-weight:bold;font-size:12px;text-align: right;">
            <?php echo $total_pl_availed; ?>
          </td>
          <td style="padding: 0px 9px;font-weight:bold;font-size:12px;text-align: right;">
            <?php echo $tvalue['balance_pl']; ?>
          </td>
        </tr>
        <tr style="border-bottom:2px solid black;">
          <td colspan = "5">
          </td>
        </tr>
        <?php $i++; ?>
      <?php } ?>
    <?php } else { ?>
    <tr>
      <td class="center" colspan = "1"><?php echo $text_no_results; ?></td>
    </tr>
    <?php } ?>
  </table>
</div></body></html>