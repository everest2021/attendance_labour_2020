<?php date_default_timezone_set("Asia/Kolkata"); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body style="width:100%;">
<?php //foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo 'VVMC'; ?><br /><?php echo 'LEAVE REGISTER'; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Period : '. $filter_date_start . ' - ' . $filter_date_end; ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <?php if($final_datas) { ?>
      <?php $i = 1; ?>
      <?php foreach($final_datas as $final_data) { ?>
        <tr>
          <td class="left" style="font-weight:bold;font-size:12px;">From Date</td>
          <td class="left" style="font-weight:bold;font-size:12px;">To Date</td>
          <td class="left" style="font-weight:bold;font-size:12px;">Break</td>
          <td class="left" colspan="3" style="text-align:center;font-weight:bold;font-size:12px;">Leave Detail</td>
        </tr>
        <tr style="font-weight:bold;font-size:11px;">
          <td colspan = "6">
            <?php echo 'Employee Code & Name : ' . $final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp; <?php echo $final_data['basic_data']['name']; ?> 
          </td>
        </tr>
        <tr>
          <td colspan="3">
          </td>
          <td style="padding: 0px 9px;font-weight:bold;font-size:12px;">
            CL
          </td>
          <td style="padding: 0px 9px;font-weight:bold;font-size:12px;">
            PL
          </td>
          <td style="padding: 0px 9px;font-weight:bold;font-size:12px;">
            SL
          </td>
        </tr>
        <tr>
          <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
            <?php echo 'opening Bal'; ?>
          </td>
          <td style="padding: 0px 9px;font-weight:bold;font-size:12px;">
            <?php echo $final_data['basic_data']['open_date']; ?>
          </td>
          <td style="padding: 0px 9px;font-weight:bold;font-size:12px;">
          </td>
          <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
            <?php echo $final_data['basic_data']['cl_open']; ?>
          </td>
          <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
            <?php echo $final_data['basic_data']['pl_open']; ?>
          </td>
          <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
            <?php echo $final_data['basic_data']['sl_open']; ?>
          </td>
        </tr>
        <tr>
          <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
            <?php echo 'Consumed Leave'; ?>
          </td>
          <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
            <?php echo $final_data['basic_data']['total_days_cl']; ?>
          </td>
          <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
            <?php echo $final_data['basic_data']['total_days_pl']; ?>
          </td>
          <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
            <?php echo $final_data['basic_data']['total_days_sl']; ?>
          </td>
        </tr>
        <tr>
          <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
            <?php echo 'Closing Balance'; ?>
          </td>
          <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
            <?php echo $final_data['basic_data']['closing_cl']; ?>
          </td>
          <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
            <?php echo $final_data['basic_data']['closing_pl']; ?>
          </td>
          <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
            <?php echo $final_data['basic_data']['closing_sl']; ?>
          </td>
        </tr>
        <tr style="border-bottom:2px solid black;">
          <td colspan = "6" >
          </td>
        </tr>
        <?php $i++; ?>
      <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php //} ?>
</body>
</html>