<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body style="width:100%;">
<?php //foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo 'VVMC'; ?><br /><?php echo 'Yearly Leave Summary'; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Year : '. $filter_year; ?></p><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Date : '. Date('d-m-Y'); ?></p><br />
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <?php if($final_datas) { ?>
        <tr>
          <td style="font-weight:bold;font-size:12px;">
            Status
          </td>
          <?php foreach($months as $mkey => $mvalue) { ?>
          <td style="font-weight:bold;font-size:12px;">
            <?php echo $mvalue; ?>
          </td>
          <?php } ?>
          <td style="font-weight:bold;font-size:12px;">
            TOTAL
          </td>
        </tr>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr style="font-weight:bold;font-size:12px;">
            <td colspan = "13">
              <?php echo 'Employee Code & Name : ' . $final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp; <?php echo $final_data['basic_data']['name']; ?> 
            </td>
          </tr>
          <tr>
            <td style="padding: 0px 9px;font-size:11px;">
              CL
            </td>
            <?php foreach($months as $mkey => $mvalue) { ?>
              <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $fvalue['cl_cnt']; ?>
                </td>
              <?php } ?>
            <?php } ?>
            <td style="padding: 0px 9px;font-size:11px;">
              <?php echo $final_data['basic_data']['g_cl_cnt']; ?>
            </td>
          </tr>
          <tr>
            <td style="padding: 0px 9px;font-size:11px;">
              PL
            </td>
            <?php foreach($months as $mkey => $mvalue) { ?>
              <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $fvalue['pl_cnt']; ?>
                </td>
              <?php } ?>
            <?php } ?>
            <td style="padding: 0px 9px;font-size:11px;">
              <?php echo $final_data['basic_data']['g_pl_cnt']; ?>
            </td>
          </tr>
          <tr>
            <td style="padding: 0px 9px;font-size:11px;">
              SL
            </td>
            <?php foreach($months as $mkey => $mvalue) { ?>
              <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $fvalue['sl_cnt']; ?>
                </td>
              <?php } ?>
            <?php } ?>
            <td style="padding: 0px 9px;font-size:11px;">
              <?php echo $final_data['basic_data']['g_sl_cnt']; ?>
            </td>
          </tr>
          <tr>
            <td style="padding: 0px 9px;font-size:11px;">
              PRE
            </td>
            <?php foreach($months as $mkey => $mvalue) { ?>
              <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $fvalue['pre_cnt']; ?>
                </td>
              <?php } ?>
            <?php } ?>
            <td style="padding: 0px 9px;font-size:11px;">
              <?php echo $final_data['basic_data']['g_pre_cnt']; ?>
            </td>
          </tr>
          <tr>
            <td style="padding: 0px 9px;font-size:11px;">
              ABS
            </td>
            <?php foreach($months as $mkey => $mvalue) { ?>
              <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $fvalue['abs_cnt']; ?>
                </td>
              <?php } ?>
            <?php } ?>
            <td style="padding: 0px 9px;font-size:11px;">
              <?php echo $final_data['basic_data']['g_abs_cnt']; ?>
            </td>
          </tr>
          <tr>
            <td style="padding: 0px 9px;font-size:11px;">
              HLD
            </td>
            <?php foreach($months as $mkey => $mvalue) { ?>
              <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $fvalue['hld_cnt']; ?>
                </td>
              <?php } ?>
            <?php } ?>
            <td style="padding: 0px 9px;font-size:11px;">
              <?php echo $final_data['basic_data']['g_hld_cnt']; ?>
            </td>
          </tr>
          <tr style="display:none;">
            <td style="padding: 0px 9px;font-size:11px;">
              HD
            </td>
            <?php foreach($months as $mkey => $mvalue) { ?>
              <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $fvalue['hd_cnt']; ?>
                </td>
              <?php } ?>
            <?php } ?>
            <td style="padding: 0px 9px;font-size:11px;">
              <?php echo $final_data['basic_data']['g_hd_cnt']; ?>
            </td>
          </tr>
          <tr>
            <td style="padding: 0px 9px;font-size:11px;">
              WO
            </td>
            <?php foreach($months as $mkey => $mvalue) { ?>
              <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $fvalue['wo_cnt']; ?>
                </td>
              <?php } ?>
            <?php } ?>
            <td style="padding: 0px 9px;font-size:11px;">
              <?php echo $final_data['basic_data']['g_wo_cnt']; ?>
            </td>
          </tr>
          <tr style="display:none;">
            <td style="padding: 0px 9px;font-size:11px;">
              COF
            </td>
            <?php foreach($months as $mkey => $mvalue) { ?>
              <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $fvalue['cof_cnt']; ?>
                </td>
              <?php } ?>
            <?php } ?>
            <td style="padding: 0px 9px;font-size:11px;">
              <?php echo $final_data['basic_data']['g_cof_cnt']; ?>
            </td>
          </tr>
          <tr style="display:none;">
            <td style="padding: 0px 9px;font-size:11px;">
              OD
            </td>
            <?php foreach($months as $mkey => $mvalue) { ?>
              <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $fvalue['od_cnt']; ?>
                </td>
              <?php } ?>
            <?php } ?>
            <td style="padding: 0px 9px;font-size:11px;">
              <?php echo $final_data['basic_data']['g_od_cnt']; ?>
            </td>
          </tr>
          <tr>
            <td style="padding: 0px 9px;font-size:11px;">
              TOTAL
            </td>
            <?php foreach($months as $mkey => $mvalue) { ?>
              <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $fvalue['total_cnt']; ?>
                </td>
              <?php } ?>
            <?php } ?>
            <td style="padding: 0px 9px;font-size:11px;">
              <?php echo $final_data['basic_data']['g_total']; ?>
            </td>
          </tr>
          <tr style="border-bottom:2px solid black;">
            <td colspan = "14" >
            </td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php //} ?>
</body>
</html>