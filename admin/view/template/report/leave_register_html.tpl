<?php date_default_timezone_set("Asia/Kolkata"); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body style="width:100%;">
<?php //foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
  <h1 style="text-align:center;font-weight: bold;color: #000;">
    <?php echo $filter_company; ?><br />
    <?php echo 'LEAVE REGISTER'; ?><br />
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
      <?php echo 'Period : '. $filter_date_start . ' - ' . $filter_date_end; ?><br />
    </span>
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
      <?php echo 'Division : '. $filter_division; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Region : '. $filter_region; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Site : '. $filter_unit; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Department : '. $filter_department; ?><br />
    </span>
    <span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <td class="left" style="width:9%;font-weight:bold;font-size:12px;">From Date</td>
            <td class="left" style="width:9%;font-weight:bold;font-size:12px;">To Date</td>
            <td class="left" style="width:9%;font-weight:bold;font-size:12px;">Break</td>
            <td class="left" colspan="<?php echo count($leaves_display); ?>" style="text-align:center;font-weight:bold;font-size:12px;">Leave Detail</td>
          </tr>
          <tr style="font-weight:bold;font-size:11px;">
            <td colspan = "<?php echo 3 + count($leaves_display); ?>">
              <?php echo 'Employee Code & Name : ' . $final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp; <?php echo $final_data['basic_data']['name']; ?> 
            </td>
          </tr>
          <tr>
            <td colspan="3">
            </td>
            <?php foreach($final_data['basic_data']['total_leave_transaction'] as $lkey => $lvalue){ ?>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;text-align: right;">
              <?php echo $lkey; ?>
            </td>
            <?php } ?>
          </tr>
          <tr>
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'opening Bal'; ?>
            </td>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['open_date']; ?>
            </td>
            <td>
            </td>
            <?php foreach($final_data['basic_data']['leave_opening'] as $lkey => $lvalue){ ?>
              <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $lvalue; ?>
              </td>
            <?php } ?>
          </tr>
          <?php if(isset($final_data['tran_data']['action'])) { ?>
            <?php foreach($final_data['tran_data']['action'] as $tkey => $tvalue) { ?>
              <?php if($tvalue['performance_stat'] == 1){ ?>
                <tr>
                  <td style="padding: 0px 9px;font-size:11px;">
                    <?php echo $tvalue['start_date']; ?>
                  </td>
                  <td style="padding: 0px 9px;font-size:11px;">
                    <?php echo $tvalue['end_date']; ?>
                  </td>
                  <td style="padding: 0px 9px;font-size:11px;">
                    <?php echo $tvalue['leave_types']; ?>
                  </td>
                  <?php foreach($tvalue['leave_transaction'] as $lkey => $lvalue){ ?>      
                  <td class="right" style="padding: 0px 9px;font-size:11px;text-align: right;">
                    <?php echo $lvalue; ?>
                  </td>
                  <?php } ?>
                </tr>
              <?php } ?>
            <?php } ?>
          <?php } ?>
          <tr>
            <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'Consumed Leave'; ?>
            </td>
            <?php foreach($final_data['basic_data']['total_leave_transaction'] as $lkey => $lvalue){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $lvalue; ?>
              </td>
            <?php } ?>
          </tr>
          <tr>
            <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'Closing Balance'; ?>
            </td>
            <?php foreach($final_data['basic_data']['closing_leave_transaction'] as $lkey => $lvalue){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $lvalue; ?>
              </td>
            <?php } ?>
          </tr>
          <?php if($filter_year == date('Y')) { ?>
            <tr>
              <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'UnProcessed Leave'; ?>
              </td>
              <?php foreach($final_data['basic_data']['total_un_leave_transaction'] as $lkey => $lvalue){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $lvalue; ?>
                </td>
              <?php } ?>
            </tr>
            <tr>
              <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'UnProcessed Balance'; ?>
              </td>
              <?php foreach($final_data['basic_data']['un_total_bal_transaction'] as $lkey => $lvalue){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $lvalue; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>
          <tr style="border-bottom:2px solid black;">
            <td colspan = "<?php echo 3 + count($leaves_display); ?>">
            </td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="center" colspan = "1"><?php echo $text_no_results; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php //} ?>
</body>
</html>