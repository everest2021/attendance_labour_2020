<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  <!-- <h1 style="text-align:center;">
    <?php echo $title; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Period : '. $filter_date_start . ' : ' . $filter_date_end; ?></p><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Date : '. Date('d-m-Y'); ?></p>
  </h1> -->
  <table class="product" style="width:100% !important;">
    <tbody>
      <tr>
        <td>
          EmployeeId
        </td>
        <td>
          EmployeeName 
        </td>
        <td>
          PunchId
        </td>
        <td>
          Department
        </td>
        <td>
          Site
        </td>
        <td>
          Region
        </td>
        <td>
          Division
        </td>
        <td>
          AttendDate
        </td>
        <td>
          Status
        </td>
        <td>
          In Time
        </td>
        <td>
          Out Time
        </td>
        <td>
          TotalHour
        </td>
      </tr>
      <?php if($final_datas) { ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr style="font-size:11px;">
            <td>
              <?php echo $final_data['emp_code']; ?>
            </td>
            <td>
              <?php echo $final_data['name']; ?>
            </td>
            <td>
              <?php echo $final_data['emp_code']; ?>
            </td>
            <td>
              <?php echo $final_data['department']; ?>
            </td>
            <td>
              <?php echo $final_data['unit']; ?>
            </td>
            <td>
              <?php echo $final_data['region']; ?>
            </td>
            <td>
              <?php echo $final_data['division']; ?>
            </td>
            <td>
              <?php echo $final_data['attend_date']; ?>
            </td>
            <td>
              <?php echo $final_data['status']; ?>
            </td>
            <td>
              <?php echo $final_data['act_intime']; ?>
            </td>
            <td>
              <?php echo $final_data['act_outtime']; ?>
            </td>
            <td>
              <?php echo $final_data['working_time']; ?>
            </td>
          </tr>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
</body>
</html>