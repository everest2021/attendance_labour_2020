<?php date_default_timezone_set("Asia/Kolkata"); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body style="width:100%;">
<?php //foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
  <h1 style="text-align:center;font-weight: bold;color: #000;">
    <?php echo $filter_company; ?><br />
    <?php echo 'LEAVE REPORT'; ?><br />
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
      <?php echo 'Period : '. $filter_date_start . ' - ' . $filter_date_end; ?><br />
    </span>
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
      <?php echo 'Division : '. $filter_division; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Region : '. $filter_region; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Site : '. $filter_unit; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Department : '. $filter_department; ?><br />
    </span>
    <span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <?php if($final_datas) { ?>
        <tr>
          <?php if(empty($filter_leave) || in_array('1', $filter_leave) ){ ?>
            <td class="left" style="font-weight:bold;font-size:12px;">Sr.No</td>
          <?php } ?>
          <?php if(empty($filter_leave) || in_array('2', $filter_leave) ){ ?>
            <td class="left" style="font-weight:bold;font-size:12px;">Employee Code</td>
          <?php } ?>
          <?php if(empty($filter_leave) || in_array('3', $filter_leave) ){ ?>
            <td class="left" style="font-weight:bold;font-size:12px;">Punch Id</td>
          <?php } ?>
          <?php if(empty($filter_leave) || in_array('4', $filter_leave) ){ ?>
            <td class="left" style="font-weight:bold;font-size:12px;">Employee Name</td>
          <?php } ?>
          <?php if(empty($filter_leave) || in_array('5', $filter_leave) ){ ?>
            <td class="left" style="font-weight:bold;font-size:12px;">Region</td>
          <?php } ?>
          <?php if(empty($filter_leave) || in_array('6', $filter_leave) ){ ?>
            <td class="left" style="font-weight:bold;font-size:12px;">Division</td>
          <?php } ?>
          <?php if(empty($filter_leave) || in_array('7', $filter_leave) ){ ?>
            <td class="left" style="font-weight:bold;font-size:12px;">Site</td>
          <?php } ?>
          <?php if(empty($filter_leave) || in_array('8', $filter_leave) ){ ?>
            <td class="left" style="font-weight:bold;font-size:12px;">Department</td>
          <?php } ?>
          <?php if(empty($filter_leave) || in_array('9', $filter_leave) ){ ?>
            <td class="left" style="font-weight:bold;font-size:12px;">Designation</td>
          <?php } ?>
          <?php foreach($leaves_heading as $lkey => $lvalue){ ?>
            <td class="left" style="font-weight:bold;font-size:12px;"><?php echo $lvalue; ?></td>
          <?php } ?>
        </tr>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <?php if(empty($filter_leave) || in_array('1', $filter_leave) ){ ?>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $i; ?>
              </td>
            <?php } ?>
            <?php if(empty($filter_leave) || in_array('2', $filter_leave) ){ ?>
              <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['emp_code']; ?>
              </td>
            <?php } ?>
            <?php if(empty($filter_leave) || in_array('3', $filter_leave) ){ ?>
              <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['emp_code']; ?>
              </td>
            <?php } ?>
            <?php if(empty($filter_leave) || in_array('4', $filter_leave) ){ ?>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['name']; ?>
              </td>
            <?php } ?>
            <?php if(empty($filter_leave) || in_array('5', $filter_leave) ){ ?>
              <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['region']; ?>
              </td>
            <?php } ?>
            <?php if(empty($filter_leave) || in_array('6', $filter_leave) ){ ?>
              <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['division']; ?>
              </td>
            <?php } ?>
            <?php if(empty($filter_leave) || in_array('7', $filter_leave) ){ ?>
              <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['unit']; ?>
              </td>
            <?php } ?>
            <?php if(empty($filter_leave) || in_array('8', $filter_leave) ){ ?>
              <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['department']; ?>
              </td>
            <?php } ?>
            <?php if(empty($filter_leave) || in_array('9', $filter_leave) ){ ?>
              <td class="left" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['designation']; ?>
              </td>
            <?php } ?>
            <?php foreach($final_data['basic_data']['leave_transaction_datas'] as $key => $value){ ?>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $value['leave_open']; ?>
              </td>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $value['leave_credit']; ?>
              </td>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $value['leave_taken']; ?>
              </td>
              <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $value['leave_balance']; ?>
              </td>
            <?php } ?>
          </tr>
        <?php $i++ ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php //} ?>
</body>
</html>