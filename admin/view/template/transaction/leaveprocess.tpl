<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:6%">Company
            <select name="company" id="company">
              <?php foreach($company_data as $key => $ud) { ?>
                <?php if($key == $company) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:6%">Region
            <select name="region" id="region">
              <?php foreach($region_data as $key => $ud) { ?>
                <?php if($key == $region) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:6%">Division
            <select name="division" id="division">
              <?php foreach($division_data as $key => $ud) { ?>
                <?php if($key == $division) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <?php if($user_dept == '0' || $is_dept == '1') { ?>
            <td style="width:6%">Site
              <select name="unit" id="unit">
                <?php foreach($unit_data as $key => $ud) { ?>
                  <?php if($key == $unit) { ?>
                    <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                  <?php } else { ?>
                    <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </td>
          <?php } ?>
          <?php if($user_dept == '0') { ?>
            <td style="width:8%">Department
              <select name="department" id="department" style="width:100%;">
                <?php foreach($department_data as $key => $dd) { ?>
                  <?php if($key == $department) { ?>
                    <option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
                  <?php } else { ?>
                    <option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </td>
          <?php } ?>
          <td style="text-align: right;">
            <a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <a style="padding: 13px 25px;" onclick="close_day();" id="closeday" class="button"><?php echo 'Leave Process'; ?></a>
          </td>
        </tr>
      </table>
      <table class="list">
        <tbody>
          <?php if($unprocessed) { ?>
            <tr>
              <td>
                Sr No
              </td>
              <td>
                Date
              </td>
              <td>
                Emp Name
              </td>
              <td>
                Leave type
              </td>
              <td style="display: none;">
                Action
              </td>
            </tr>
            <?php $i = 1; ?>
            <?php foreach($unprocessed as $data) { ?>
              <tr>
                <td>
                  <?php echo $i; ?>
                </td>
                <td>
                  <?php echo $data['date']; ?>
                </td>
                <td>
                  <?php echo $data['ename']; ?>
                </td>
                <td>
                  <?php echo $data['leave_type']; ?>
                </td>
                <td class="right" style="display: none;">
                  <?php /* foreach ($data['action'] as $action) { ?>
                    <?php if($action['href'] == '') { ?>
                      <p style="display:inline;cursor:default;"><?php echo $action['text']; ?></p>
                    <?php } else { ?>
                      [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                    <?php } ?>
                  <?php } */ ?>
                </td>
              </tr>
              <?php $i ++; ?>
            <?php } ?>
          <?php } else { ?>
            No data to process      
          <?php } ?>
        </tbody>
      </table>
      
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=transaction/leaveprocess&token=<?php echo $token; ?>';
  var unit = $('#unit').val();
  if (unit && unit != '0') {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('#department').val();
  if (department && department != '0') {
    url += '&department=' + encodeURIComponent(department);
  }

  var division = $('#division').val();
  if (division && division != '0') {
    url += '&division=' + encodeURIComponent(division);
  }

  var region = $('#region').val();
  if (region && region != '0') {
    url += '&region=' + encodeURIComponent(region);
  }

  var company = $('#company').val();
  if (company && company != '0') {
    url += '&company=' + encodeURIComponent(company);
  }
  
  url += '&once=1';
  location = url;
  return false;
}

function close_day() {
  url = 'index.php?route=transaction/leaveprocess/close_day&token=<?php echo $token; ?>';
  var unit = $('#unit').val();
  if (unit && unit != '0') {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('#department').val();
  if (department && department != '0') {
    url += '&department=' + encodeURIComponent(department);
  }

  var division = $('#division').val();
  if (division && division != '0') {
    url += '&division=' + encodeURIComponent(division);
  }

  var region = $('#region').val();
  if (region && region != '0') {
    url += '&region=' + encodeURIComponent(region);
  }

  var company = $('#company').val();
  if (company && company != '0') {
    url += '&company=' + encodeURIComponent(company);
  }
  location = url;
  return false;
}

$('#region').on('change', function() {
  region = $(this).val();
  $.ajax({
    url: 'index.php?route=catalog/employee/getdivision_location&token=<?php echo $token; ?>&filter_region_id=' +  encodeURIComponent(region),
    dataType: 'json',
    success: function(json) {   
      $('#division').find('option').remove();
      if(json['division_datas']){
        $.each(json['division_datas'], function (i, item) {
          $('#division').append($('<option>', { 
              value: item.division_id,
              text : item.division 
          }));
        });
      }
      $('#unit').find('option').remove();
      if(json['unit_datas']){
        $.each(json['unit_datas'], function (i, item) {
          $('#unit').append($('<option>', { 
              value: item.unit_id,
              text : item.unit 
          }));
        });
      }
    }
  });
});

$('#division').on('change', function() {
  division = $(this).val();
  $.ajax({
    url: 'index.php?route=catalog/employee/getlocation&token=<?php echo $token; ?>&filter_division_id=' +  encodeURIComponent(division),
    dataType: 'json',
    success: function(json) {   
      $('#unit').find('option').remove();
      if(json['unit_datas']){
        $.each(json['unit_datas'], function (i, item) {
          $('#unit').append($('<option>', { 
              value: item.unit_id,
              text : item.unit 
          }));
        });
      }
    }
  });
});

//--></script> 
<?php echo $footer; ?>