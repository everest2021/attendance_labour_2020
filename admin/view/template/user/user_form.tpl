<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><span class="required">*</span> <?php echo $entry_username; ?></td>
            <td><input type="text" name="username" value="<?php echo $username; ?>" />
              <?php if ($error_username) { ?>
              <span class="error"><?php echo $error_username; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
            <td><input type="text" name="firstname" value="<?php echo $firstname; ?>" />
              <?php if ($error_firstname) { ?>
              <span class="error"><?php echo $error_firstname; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
            <td><input type="text" name="lastname" value="<?php echo $lastname; ?>" />
              <?php if ($error_lastname) { ?>
              <span class="error"><?php echo $error_lastname; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_email; ?></td>
            <td><input type="text" name="email" value="<?php echo $email; ?>" /></td>
          </tr>
          <?php if($show == 1){ ?>
            <?php /* ?>
            <tr>
              <td><?php echo 'Company'; ?></td>
              <td>
                <select name="company_id" id="input-company_id">
                  <?php foreach ($company_datas as $skey => $svalue) { ?>
                    <?php if ($skey == $company_id) { ?>
                      <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <?php */ ?>
            <tr>
              <td><?php echo 'Company'; ?></td>
              <td>
                <select multiple name="company_id[]" style="display:inline;" id="input-company_id">
                  <?php foreach ($company_datas as $skey => $svalue) { ?>
                    <?php if (in_array($skey, $company_id)) { ?>
                      <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td><?php echo 'Region'; ?></td>
              <td>
                <select multiple name="region[]" style="display:inline;" id="input-region">
                  <?php foreach ($regions as $rkey => $rvalue) { ?>
                    <?php if (in_array($rkey, $region)) { ?>
                      <option value="<?php echo $rkey; ?>" selected="selected"><?php echo $rvalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $rkey; ?>"><?php echo $rvalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td><?php echo 'Division'; ?></td>
              <td>
                <select multiple name="division[]" style="display: inline;" id="input-division">
                  <?php foreach ($divisions as $dkey => $dvalue) { ?>
                    <?php if (in_array($dkey, $division)) { ?>
                      <option value="<?php echo $dkey; ?>" selected="selected"><?php echo $dvalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $dkey; ?>"><?php echo $dvalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td><?php echo 'Site'; ?></td>
              <td>
                <select multiple name="site[]" style="display:inline;" id="input-site">
                  <?php foreach ($sites as $skey => $svalue) { ?>
                    <?php if (in_array($skey, $site)) { ?>
                      <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $skey; ?>"><?php echo $svalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td><?php echo 'Device'; ?></td>
              <td>
                <select multiple name="device[]" style="display:inline;" id="input-device">
                  <?php foreach ($devices as $svalue) { ?>
                    <?php if (in_array($svalue['device_id'], $device)) { ?>
                      <option value="<?php echo $svalue['device_id']; ?>" selected="selected"><?php echo $svalue['device_name']; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $svalue['device_id']; ?>"><?php echo $svalue['device_name']; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td><?php echo 'Role'; ?></td>
              <td>
                <select name="user_group_id">
                  <?php foreach ($user_groups as $user_group) { ?>
                  <?php if ($user_group['user_group_id'] == $user_group_id) { ?>
                  <option value="<?php echo $user_group['user_group_id']; ?>" selected="selected"><?php echo $user_group['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $user_group['user_group_id']; ?>"><?php echo $user_group['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </td>
            </tr>
          <?php } ?>
          <tr>
            <td><?php echo $entry_password; ?></td>
            <td><input type="password" name="password" value="<?php echo $password; ?>"  />
              <?php if ($error_password) { ?>
              <span class="error"><?php echo $error_password; ?></span>
              <?php  } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_confirm; ?></td>
            <td><input type="password" name="confirm" value="<?php echo $confirm; ?>" />
              <?php if ($error_confirm) { ?>
              <span class="error"><?php echo $error_confirm; ?></span>
              <?php  } ?></td>
          </tr>
          <?php if($show == 1){ ?>
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select name="status">
                  <?php if ($status) { ?>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <?php } else { ?>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          <?php } ?>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
  $('#input-division').multiselect({
    includeSelectAllOption: true,
    enableFiltering: true,
    maxHeight: 235,
  });
  $('#input-region').multiselect({
    includeSelectAllOption: true,
    enableFiltering: true,
    maxHeight: 235,
  });
  $('#input-site').multiselect({
    includeSelectAllOption: true,
    enableFiltering: true,
    maxHeight: 235,
  });
  $('#input-device').multiselect({
    includeSelectAllOption: true,
    enableFiltering: true,
    maxHeight: 235,
  });
  $('#input-company_id').multiselect({
    includeSelectAllOption: true,
    enableFiltering: true,
    maxHeight: 235,
  });
});

$('#input-region').on('change', function() {
  filter_region_selected = $('#input-region').val();
  var regions_ids = '';
  if(filter_region_selected){
    max_length = filter_region_selected.length - 1;
    for(i=0; i<filter_region_selected.length; i++){
      if(max_length == i){  
        regions_ids += filter_region_selected[i];
      } else {
        regions_ids += filter_region_selected[i]+',';
      }
    }
  }
  
  filter_division_selected = $('#input-division').val();
  var division_ids1 = '';
  if(filter_division_selected){
    max_length = filter_division_selected.length - 1;
    for(i=0; i<filter_division_selected.length; i++){
      if(max_length == i){  
        division_ids1 += "'"+filter_division_selected[i]+"'";
      } else {
        division_ids1 += "'"+filter_division_selected[i]+"'"+', ';
      }
    }
  }
  region = regions_ids.replace(/,+$/,'');
  $.ajax({
    url: 'index.php?route=catalog/employee/getdivision_location&token=<?php echo $token; ?>&filter_region_ids=' +  encodeURIComponent(region),
    dataType: 'json',
    success: function(json) {   
      $('#input-division').multiselect('destroy').removeData();
      $('#input-division').find('option').remove();
      if(json['division_datas']){
        $.each(json['division_datas'], function (i, item) {
          if(i > 0){
            $('#input-division').append($('<option>', { 
                value: item.division_id,
                text : item.division 
            }));
          }
        });
        $('#input-division').multiselect({
          includeSelectAllOption: true,
          enableFiltering: true,
          maxHeight: 235,
        });
        //$('#input-division').multiselect('select', [division_ids1]);
      }
      
      $('#input-site').multiselect('destroy').removeData();
      $('#input-site').find('option').remove();
      if(json['unit_datas']){
        $.each(json['unit_datas'], function (i, item) {
          if(i > 0){
            $('#input-site').append($('<option>', { 
                value: item.unit_id,
                text : item.unit 
            }));
          }
        });
        $('#input-site').multiselect({
          includeSelectAllOption: true,
          enableFiltering: true,
          maxHeight: 235,
        });
      }

      $('#input-device').multiselect('destroy').removeData();
      $('#input-device').find('option').remove();
      if(json['device_datas']){
        $.each(json['device_datas'], function (i, item) {
          if(i > 0){
            $('#input-device').append($('<option>', { 
                value: item.device_id,
                text : item.device_name 
            }));
          }
        });
        $('#input-device').multiselect({
          includeSelectAllOption: true,
          enableFiltering: true,
          maxHeight: 235,
        });
      }
    }
  });
});

$('#input-division').on('change', function() {
  filter_division_selected = $('#input-division').val();
  division_ids = '';
  if(filter_division_selected){
    max_length = filter_division_selected.length - 1;
    for(i=0; i<filter_division_selected.length; i++){
      if(max_length == i){  
        division_ids += filter_division_selected[i];
      } else {
        division_ids += filter_division_selected[i]+',';
      }
    }
  }
  division = division_ids.replace(/,+$/,'');
  $.ajax({
    url: 'index.php?route=catalog/employee/getlocation&token=<?php echo $token; ?>&filter_division_ids=' +  encodeURIComponent(division),
    dataType: 'json',
    success: function(json) {   
      $('#input-site').multiselect('destroy').removeData();
      $('#input-site').find('option').remove();
      if(json['unit_datas']){
        $.each(json['unit_datas'], function (i, item) {
          if(i > 0){
            $('#input-site').append($('<option>', { 
                value: item.unit_id,
                text : item.unit 
            }));
          }
        });
        $('#input-site').multiselect({
          includeSelectAllOption: true,
          enableFiltering: true,
          maxHeight: 235,
        });
      }
      
      $('#input-device').multiselect('destroy').removeData();
      $('#input-device').find('option').remove();
      if(json['device_datas']){
        $.each(json['device_datas'], function (i, item) {
          if(i > 0){
            $('#input-device').append($('<option>', { 
                value: item.device_id,
                text : item.device_name 
            }));
          }
        });
        $('#input-device').multiselect({
          includeSelectAllOption: true,
          enableFiltering: true,
          maxHeight: 235,
        });
      }
    }
  });
});

$('#input-site').on('change', function() {
  filter_site_selected = $('#input-site').val();
  site_ids = '';
  if(filter_site_selected){
    max_length = filter_site_selected.length - 1;
    for(i=0; i<filter_site_selected.length; i++){
      if(max_length == i){  
        site_ids += filter_site_selected[i];
      } else {
        site_ids += filter_site_selected[i]+',';
      }
    }
  }
  site_id = site_ids.replace(/,+$/,'');
  //site_id = site_ids;//$(this).val();
  $.ajax({
    url: 'index.php?route=catalog/employee/getdevices&token=<?php echo $token; ?>&filter_unit=' +  encodeURIComponent(site_id),
    dataType: 'json',
    success: function(json) {   
      $('#input-device').multiselect('destroy').removeData();
      $('#input-device').find('option').remove();
      if(json['device_datas']){
        $.each(json['device_datas'], function (i, item) {
          if(i > 0){
            $('#input-device').append($('<option>', { 
                value: item.device_id,
                text : item.device_name 
            }));
          }
        });
        $('#input-device').multiselect({
          includeSelectAllOption: true,
          enableFiltering: true,
          maxHeight: 235,
        });
      }
    }
  });
});

</script>
<?php echo $footer; ?> 