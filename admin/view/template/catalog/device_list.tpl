<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <a style="display: none;" href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a>
        <a style="display: none;" onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a>
        <a style="display: none;" href="<?php echo $export; ?>" class="button"><?php echo 'Export'; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;display: none;">
                <input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
              </td>
              <td class="left">
                <?php if ($sort == 'DeviceFname') { ?>
                  <a href="<?php echo $sort_device; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Name'; ?></a>
                <?php } else { ?>
                  <a href="<?php echo $sort_device; ?>"><?php echo 'Name'; ?></a>
                <?php } ?>
              </td>
              <td class="left"  style="display: none;">
                <?php if ($sort == 'DeviceDirection') { ?>
                  <a href="<?php echo $sort_direction; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Direction'; ?></a>
                <?php } else { ?>
                  <a href="<?php echo $sort_direction; ?>"><?php echo 'Direction'; ?></a>
                <?php } ?>
              </td>
              <td class="left">
                <?php echo 'SerialNumber'; ?>
              </td>
              <td class="left">
                <?php if ($sort == 'DeviceLocation') { ?>
                  <a href="<?php echo $sort_location; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Location'; ?></a>
                <?php } else { ?>
                  <a href="<?php echo $sort_location; ?>"><?php echo 'Location'; ?></a>
                <?php } ?>
              </td>
              <td class="left"><?php echo 'Company'; ?></td>
              <td class="left">
                <?php if ($sort == 'LastLogDownloadDate') { ?>
                  <a href="<?php echo $sort_download_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Last Download Date'; ?></a>
                <?php } else { ?>
                  <a href="<?php echo $sort_download_date; ?>"><?php echo 'Last Download Date'; ?></a>
                <?php } ?>
              </td>
              <td class="left">
                <?php if ($sort == 'LastPing') { ?>
                  <a href="<?php echo $sort_ping_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Last Ping'; ?></a>
                <?php } else { ?>
                  <a href="<?php echo $sort_ping_date; ?>"><?php echo 'Last Ping'; ?></a>
                <?php } ?>
              </td>
              <td class="left"><?php echo 'Status'; ?></td>
              <td class="right" style="display: none;"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td style="display: none;">&nbsp;</td>
              <td>
                <input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name; ?>" style="width:210px;" />
                <input type="hidden" id="filter_name_id" name="filter_name_id" value="<?php echo $filter_name_id; ?>" />
              </td>
              <td style="">
                <input type="text" id="filter_direction" name="filter_direction" value="<?php echo $filter_direction; ?>" style="width:210px;" />
                <input type="hidden" id="filter_direction_id" name="filter_direction_id" value="<?php echo $filter_direction_id; ?>" />
              </td>
              <td style="">
                <input type="text" id="filter_location" name="filter_location" value="<?php echo $filter_location; ?>" style="width:210px;" />
                <input type="hidden" id="filter_location_id" name="filter_location_id" value="<?php echo $filter_location_id; ?>" />
              </td>
              <td style="">&nbsp;</td>
              <td style="">
                <input type="text" id="filter_download_date" name="filter_download_date" value="<?php echo $filter_download_date; ?>" class="date" style="width:210px;" />
              </td>
              <td style="">
                <input type="text" id="filter_ping_date" name="filter_ping_date" value="<?php echo $filter_ping_date; ?>" class="date" style="width:210px;" />
              </td>
              <td style="">
                <a onclick="filter();" class="button"><?php echo 'Filter'; ?></a>
              </td>
              <td align="right" style="display: none;"></td>
            </tr>
            <?php if ($devices) { ?>
            <?php foreach ($devices as $device) { ?>
            <tr>
              <td style="text-align: center;display: none;">
                <?php if ($device['selected']) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $device['device_id']; ?>" checked="checked" />
                <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $device['device_id']; ?>" />
                <?php } ?>
              </td>
              <td class="left"><?php echo $device['device_name']; ?></td>
              <td class="left" style="display: none;"><?php echo $device['direction']; ?></td>
              <td class="left"><?php echo $device['SerialNumber']; ?></td>
              <td class="left"><?php echo $device['location']; ?></td>
              <td class="left"><?php echo $device['company']; ?></td>
              <td class="left"><?php echo $device['last_download_date']; ?></td>
              <td class="left"><?php echo $device['last_ping_date']; ?></td>
              <?php if($device['status'] == 'Online'){ ?>
                <td class="left" style="color: green;"><?php echo $device['status']; ?></td>
              <?php } else { ?>
                <td class="left" style="color: red;"><?php echo $device['status']; ?></td>
              <?php } ?>
              <td class="right" style="display: none;"><?php foreach ($device['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="7"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/device&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  if (filter_name) {
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name=' + encodeURIComponent(filter_name);
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
      
    }
  }

  var filter_direction = $('input[name=\'filter_direction\']').attr('value');
  if (filter_direction) {
    url += '&filter_direction=' + encodeURIComponent(filter_direction);
  }

  var filter_location = $('input[name=\'filter_location\']').attr('value');
  if (filter_location) {
    url += '&filter_location=' + encodeURIComponent(filter_location);
  }

  var filter_download_date = $('input[name=\'filter_download_date\']').attr('value');
  if (filter_download_date) {
    url += '&filter_download_date=' + encodeURIComponent(filter_download_date);
  }

  var filter_ping_date = $('input[name=\'filter_ping_date\']').attr('value');
  if (filter_ping_date) {
    url += '&filter_ping_date=' + encodeURIComponent(filter_ping_date);
  }

  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--

$('.date').datepicker({dateFormat: 'dd-mm-yy'});

$('#filter_name').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/device/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.device,
            value: item.device_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<?php echo $footer; ?>