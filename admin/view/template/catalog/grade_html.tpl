<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo $title; ?><br />
    <p style="display:inline;font-size:15px;display:none;"><?php //echo 'Period : '. $filter_date_start . ' : ' . $filter_date_end; ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <tr>
        <td>
          Sr.No
        </td>
        <td>
          Grade
        </td>
        <td>
          Code
        </td>
      </tr>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr style="font-weight:bold;font-size:11px;">
            <td>
              <?php echo $i; ?>
            </td>
            <td>
              <?php echo $final_data['name']; ?>
            </td>
            <td>
              <?php echo $final_data['code']; ?>
            </td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
</body>
</html>