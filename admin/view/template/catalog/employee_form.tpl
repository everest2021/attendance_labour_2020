<?php echo $header; ?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
  $part = explode('/', $this->request->get['route']);

  if (isset($part[0])) {
  $route .= $part[0];
  }

  if (isset($part[1])) {
  $route .= '/' . $part[1];
  }
}
?>
<div id="content">
    <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
      <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="box">
    <div class="heading">
        <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
        <div class="buttons">
        <?php if($this->user->hasPermission('modify', $route) || ($this->user->hasPermission('add', $route) && !isset($this->request->get['employee_id'])) ){ ?>
            <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
        <?php } ?>
        <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
      </div>
    </div>
    <div class="content">
        <div id="tabs" class="htabs">
        <a href="#tab-general"><?php echo $tab_general; ?></a>
        <a href="#tab-history"><?php echo 'History'; ?></a>
        </div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
            <table class="form">
            <tr>
                <td style="width: 15%;"><span class="required">*</span> <?php echo $entry_name; ?></td>
                <td style="width: 30%;">
                <input oninput="this.value = this.value.toUpperCase()" type="text" name="name" id="name" value="<?php echo $name; ?>" size="50" />
                <?php if ($error_name) { ?>
                    <span class="error"><?php echo $error_name; ?></span>
                <?php } ?>
                </td>     
                <td style="width: 10%;">
                &nbsp;
                </td>
                <td style="width: 15%;">
                <span class="required">*</span> <?php echo "Emp Code"; ?>
                </td>
                <td style="width: 30%;">
                <input oninput="this.value = this.value.toUpperCase()" type="text" name="emp_code" value="<?php echo $emp_code; ?>" size="50" />
                <input type="hidden" name="hidden_emp_code" value="<?php echo $hidden_emp_code; ?>" size="50" />
                <?php if ($error_emp_code) { ?>
                    <span class="error"><?php echo $error_emp_code; ?></span>
                <?php } ?>
                </td>
            </tr>
            <tr>
                <td><?php echo "Gender"; ?></td>
                <td>
                <select name="gender">
                    <option value="0">None</option>
                    <?php foreach($genders as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $gender) { ?>
                        <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
                </td>
                <td>
                &nbsp;
                </td>
                <td>
                <?php echo "Date Of Birth"; ?>
                </td>
                <td>
                <input type="text" name="dob" value="<?php echo $dob; ?>" size="50" class="date" />
                </td>
            </tr>
            <tr>
                <td>
                <span class="required">*</span> <?php echo "Date Of Joining"; ?>
                </td>
                <td>
                <input type="text" name="doj" value="<?php echo $doj; ?>" size="50" class="date" />
                <input type="hidden" name="hidden_doj" value="<?php echo $hidden_doj; ?>" size="50" class="date" />
                <?php if ($error_doj) { ?>
                  <span class="error"><?php echo $error_doj; ?></span>
                <?php } ?>
                </td>
                <td>
                &nbsp;
                </td>
                <td>
                <?php echo "Date Of Confirmation"; ?>
                </td>
                <td>
                <input type="text" name="doc" value="<?php echo $doc; ?>" size="50" class="date" />
                </td>
            </tr>
            <tr>
                <td>
                <?php echo "Date Of Left"; ?>
                </td>
                <td>
                <input type="text" name="dol" value="<?php echo $dol; ?>" size="50" class="date" />
                <input type="hidden" name="hidden_dol" value="<?php echo $hidden_dol; ?>" size="50" class="date" />
                <?php if ($error_dol) { ?>
                  <span class="error"><?php echo $error_dol; ?></span>
                <?php } ?>
                </td>
                <td>
                &nbsp;
                </td>
                <td>
                  &nbsp;
                </td>
                <td>
                &nbsp;
                </td>
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo "Company"; ?></td>
                <td>
                <select name="company_id" id="company_id">
                    <?php foreach($company_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $company_id) { ?>
                        <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
                <input type="hidden" name="company" id="company" value="<?php echo $company; ?>" size="50" />
                <!-- <input type="hidden" name="company_id" id="company_id" value="<?php echo $company_id; ?>" /> -->
                <input type="hidden" name="hidden_company" id="hidden_company" value="<?php echo $hidden_company; ?>" size="50" />
                <input type="hidden" name="hidden_company_id" id="hidden_company_id" value="<?php echo $hidden_company_id; ?>" size="50" />
                <?php if ($error_company) { ?>
                    <span class="error"><?php echo $error_company; ?></span>
                <?php } ?>
                </td>
                <td>
                &nbsp;
                </td>
                <td><span class="required">*</span> <?php echo "Department"; ?></td>
                <td>
                <select name="department_id" id="department_id">
                    <?php foreach($department_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $department_id) { ?>
                        <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
                <input type="hidden" name="department" id="department" value="<?php echo $department; ?>" size="50" />
                <!-- <input type="hidden" name="department_id" id="department_id" value="<?php echo $department_id; ?>" /> -->
                <input type="hidden" name="hidden_department" id="hidden_department" value="<?php echo $hidden_department; ?>" size="50" />
                <input type="hidden" name="hidden_department_id" id="hidden_department_id" value="<?php echo $hidden_department_id; ?>" size="50" />
                <?php if ($error_department) { ?>
                    <span class="error"><?php echo $error_department; ?></span>
                <?php } ?>
                </td>  
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo "Designation"; ?></td>
                <td>
                <select name="designation_id" id="designation_id">
                    <?php foreach($designation_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $designation_id) { ?>
                        <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>   
                        <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
                <input type="hidden" name="designation" id="designation" value="<?php echo $designation; ?>" size="50" />
                <!-- <input type="hidden" name="designation_id" id="designation_id" value="<?php echo $designation_id; ?>" size="50" /> -->
                <input type="hidden" name="hidden_designation" id="hidden_designation" value="<?php echo $hidden_designation; ?>" size="50" />
                <input type="hidden" name="hidden_designation_id" id="hidden_designation_id" value="<?php echo $hidden_designation_id; ?>" size="50" />
                <?php if ($error_designation) { ?>
                    <span class="error"><?php echo $error_designation; ?></span>
                <?php } ?>
                </td>
                <td>
                &nbsp;
                </td>
                <td><span class="required">*</span> <?php echo "Grade"; ?></td>
                <td>
                <select name="grade_id" id="grade_id">
                    <?php foreach($grade_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $grade_id) { ?>
                        <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
                <input type="hidden" name="grade" id="grade" value="<?php echo $grade; ?>" size="50" />
                <!-- <input type="hidden" name="grade_id" id="grade_id" value="<?php echo $grade_id; ?>" size="50" /> -->
                <input type="hidden" name="hidden_grade" id="hidden_grade" value="<?php echo $hidden_grade; ?>" size="50" />
                <input type="hidden" name="hidden_grade_id" id="hidden_grade_id" value="<?php echo $hidden_grade_id; ?>" size="50" />
                <?php if ($error_grade) { ?>
                    <span class="error"><?php echo $error_grade; ?></span>
                <?php } ?>
                </td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo "Category"; ?></td>
              <td>
                <select name="category_id" id="category_id">
                    <?php foreach($category_data as $ckey => $gvalue) { ?>
                    <?php if ($ckey == $category_id) { ?>
                        <option value="<?php echo $ckey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $ckey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
                <input type="hidden" name="category" id="category" value="<?php echo $category; ?>" size="50" />
                <input type="hidden" name="hidden_category" id="hidden_category" value="<?php echo $hidden_category; ?>" size="50" />
                <input type="hidden" name="hidden_category_id" id="hidden_category_id" value="<?php echo $hidden_category_id; ?>" size="50" />
                <?php if ($error_category) { ?>
                    <span class="error"><?php echo $error_category; ?></span>
                <?php } ?>
              </td>
                <td style="display: none;"><span class="required">*</span> <?php echo "Region"; ?></td>
                <td style="display: none;">
                <select name="region_id" id="region_id">
                    <?php foreach($region_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $region_id) { ?>
                        <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
                <input type="hidden" name="region" id="region" value="<?php echo $region; ?>" size="50" />
                <!-- <input type="hidden" name="region_id" id="region_id" value="<?php echo $region_id; ?>" /> -->
                <input type="hidden" name="hidden_region" id="hidden_region" value="<?php echo $hidden_region; ?>" size="50" />
                <input type="hidden" name="hidden_region_id" id="hidden_region_id" value="<?php echo $hidden_region_id; ?>" size="50" />
                <?php if ($error_region) { ?>
                    <span class="error"><?php echo $error_region; ?></span>
                <?php } ?>
                </td>
                <td>
                &nbsp;
                </td>
                <td><span class="required">*</span> <?php echo "Division"; ?></td>
                <td>
                <select name="division_id" id="division_id">
                    <?php foreach($division_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $division_id) { ?>
                        <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
                <input type="hidden" name="division" id="division" value="<?php echo $division; ?>" size="50" />
                <!-- <input type="hidden" name="division_id" id="division_id" value="<?php echo $division_id; ?>" /> -->
                <input type="hidden" name="hidden_division" id="hidden_division" value="<?php echo $hidden_division; ?>" size="50" />
                <input type="hidden" name="hidden_division_id" id="hidden_division_id" value="<?php echo $hidden_division_id; ?>" size="50" />
                <?php if ($error_division) { ?>
                    <span class="error"><?php echo $error_division; ?></span>
                <?php } ?>
                </td>
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo "Site"; ?></td>
                <td>
                <select name="unit_id" id="unit_id">
                    <?php foreach($site_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $unit_id) { ?>
                        <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
                <input type="hidden" name="unit" id="unit" value="<?php echo $unit; ?>" size="50" />
                <!-- <input type="hidden" name="unit_id" id="unit_id" value="<?php echo $unit_id; ?>" /> -->
                <input type="hidden" name="hidden_unit" id="hidden_unit" value="<?php echo $hidden_unit; ?>" size="50" />
                <input type="hidden" name="hidden_unit_id" id="hidden_unit_id" value="<?php echo $hidden_unit_id; ?>" size="50" />
                <?php if ($error_site) { ?>
                    <span class="error"><?php echo $error_site; ?></span>
                <?php } ?>
                </td>
                <td>
                &nbsp;
                </td>
                <td><span class="required">*</span> <?php echo "Contractor"; ?></td>
              <td>
                <!-- <select name="contractor_id" id="contractor_id">
                    <?php foreach($contractor_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $contractor_id) { ?>
                        <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select> -->
                <input type="text" name="contractor" id="filter_contractor" value="<?php echo $contractor; ?>">
                <input type="hidden" name="contractor_id" id="filter_contractor_id" value="<?php echo $contractor_id; ?>">

                <?php if ($error_contractor) { ?>
                    <span class="error"><?php echo $error_contractor; ?></span>
                <?php } ?>
                <input type="hidden" name="contractor" id="contractor" value="<?php echo $contractor; ?>" size="50" />
                <input type="hidden" name="hidden_contractor" id="hidden_contractor" value="<?php echo $hidden_contractor; ?>" size="50" />
                <input type="hidden" name="hidden_contractor_id" id="hidden_contractor_id" value="<?php echo $hidden_contractor_id; ?>" size="50" />
              </td>
                <td style="display: none;"><span class="required">*</span> <?php echo "State"; ?></td>
                <td style="display: none;">
                <select name="state_id" id="state_id">
                    <?php foreach($state_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $state_id) { ?>
                        <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
                <input type="hidden" name="state" id="state" value="<?php echo $state; ?>" size="50" />
                <!-- <input type="hidden" name="state_id" id="state_id" value="<?php echo $state_id; ?>" size="50" /> -->
                <input type="hidden" name="hidden_state" id="hidden_state" value="<?php echo $hidden_state; ?>" size="50" />
                <input type="hidden" name="hidden_state_id" id="hidden_state_id" value="<?php echo $hidden_state_id; ?>" size="50" />
                <?php if ($error_state) { ?>
                    <span class="error"><?php echo $error_state; ?></span>
                <?php } ?>
                </td>
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo "Employment Type"; ?></td>
                <td>
                <select name="employement_id" id="employement_id">
                    <?php foreach($employement_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $employement_id) { ?>
                        <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
                <input type="hidden" name="employement" id="employement" value="<?php echo $employement; ?>" size="50" />
                <!-- <input type="hidden" name="employement_id" id="employement_id" value="<?php echo $employement_id; ?>" /> -->
                <input type="hidden" name="hidden_employement" id="hidden_employement" value="<?php echo $hidden_employement; ?>" size="50" />
                <input type="hidden" name="hidden_employement_id" id="hidden_employement_id" value="<?php echo $hidden_employement_id; ?>" size="50" />
                <?php if ($error_employement) { ?>
                    <span class="error"><?php echo $error_employement; ?></span>
                <?php } ?>
                </td>  
                <td>
                &nbsp;
                </td>
                <td><span class="required">*</span> <?php echo "Changed Date"; ?></td>
                <td>
                <input type="text" name="change_date" id="change_date" value="<?php echo $change_date; ?>" size="50" class="date" />
                <?php if ($error_change_date) { ?>
                    <span class="error"><?php echo $error_change_date; ?></span>
                <?php } ?>
                </td>
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo "Shift"; ?></td>
                <td>
                <select name="shift_id">
                    <?php foreach($shift_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $shift_id) { ?>
                        <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
                <input type="hidden" name="hidden_shift_id" value="<?php echo $hidden_shift_id; ?>" />
                <?php /* ?>
                  <input type="text" name="shift_name" value="<?php echo $shift_name; ?>" size="50" readonly="readonly" />
                  <input type="hidden" name="shift_id" value="<?php echo $shift_id; ?>" />
                <?php */ ?>
                </td>
                <td>
                &nbsp;
                </td>
                <td><span class="required">*</span> <?php echo 'Status'; ?></td>
                <td>
                <select name="status">
                    <?php if($status == 1){ ?>
                    <option value = "1" selected="selected"><?php echo 'Active'; ?></option>
                    <option value = "0"><?php echo 'Inactive'; ?></option>
                    <?php } else { ?>
                    <option value = "0" selected="selected"><?php echo 'Inactive'; ?></option>
                    <option value = "1"><?php echo 'Active'; ?></option>
                    <?php } ?>
                </select>
                <input type="hidden" name="hidden_status" value="<?php echo $hidden_status; ?>" />
                <?php if ($error_status) { ?>
                  <span class="error"><?php echo $error_status; ?></span>
                <?php } ?>
                </td>
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo 'All Saturday Working'; ?></td>
                <td>
                <select name="sat_status">
                    <?php if($sat_status == '1'){ ?>
                    <option value = "1" selected="selected"><?php echo 'Yes'; ?></option>
                    <option value = "0"><?php echo 'No'; ?></option>
                    <?php } else { ?>
                    <option value = "1"><?php echo 'Yes'; ?></option>
                    <option value = "0" selected="selected"><?php echo 'No'; ?></option>
                    <?php } ?>
                </select>
                <input type="hidden" name="hidden_sat_status" value="<?php echo $hidden_sat_status; ?>" />
                </td>
            </tr>
            <tr style="display: none;">
                <td><?php echo 'Department head'; ?></td>
                <td>
                <select name="is_dept">
                    <?php if($is_dept == '1'){ ?>
                    <option value = "1" selected="selected"><?php echo 'Yes'; ?></option>
                    <option value = "0"><?php echo 'No'; ?></option>
                    <?php } else { ?>
                    <option value = "0" selected="selected"><?php echo 'No'; ?></option>
                    <option value = "1"><?php echo 'Yes'; ?></option>
                    <?php } ?>
                </select>
                </td>
            </tr>
            <tr style="display: none;">
                <td><?php echo 'Password Reset'; ?></td>
                <td>
                <select name="password_reset">
                    <?php if($password_reset == '1'){ ?>
                    <option value = "1" selected="selected"><?php echo 'Yes'; ?></option>
                    <option value = "0"><?php echo 'No'; ?></option>
                    <?php } else { ?>
                    <option value = "0" selected="selected"><?php echo 'No'; ?></option>
                    <option value = "1"><?php echo 'Yes'; ?></option>
                    <?php } ?>
                </select>
                </td>
            </tr>
            <tr style="display: none;">
                <td><span class="required">*</span> <?php echo 'Card Number'; ?></td>
                <td>
                  <input type="text" name="card_number" value="<?php echo $card_number; ?>" size="100" />
                <?php if ($error_card_number) { ?>
                  <span class="error"><?php echo $error_card_number; ?></span>
                <?php } ?>
              </td>
            </tr>
            <tr style="display: none;">
                <td><span class="required">*</span> <?php echo 'Shift Type'; ?></td>
                <td>
                <select name="shift_type">
                    <?php if($shift_type == 'F' || $shift_type == ''){ ?>
                    <option value = "F" selected="selected"><?php echo 'Fixed'; ?></option>
                    <option value = "R"><?php echo 'Rotate'; ?></option>
                    <?php } else { ?>
                    <option value = "R" selected="selected"><?php echo 'Rotate'; ?></option>
                    <option value = "F"><?php echo 'Fixed'; ?></option>
                    <?php } ?>
                </select>
                </td>
            </tr>
            <tr style="display: none;">
                <td><?php echo 'Super User'; ?></td>
                <td>
                <select name="is_super">
                    <?php if($is_super == '1'){ ?>
                    <option value = "1" selected="selected"><?php echo 'Yes'; ?></option>
                    <option value = "0"><?php echo 'No'; ?></option>
                    <?php } else { ?>
                    <option value = "0" selected="selected"><?php echo 'No'; ?></option>
                    <option value = "1"><?php echo 'Yes'; ?></option>
                    <?php } ?>
                </select>
                </td>
            </tr>
            </table>
        </div>
        <div id="tab-history">
            <table class="form" style="width: 40%;">
              <tr>
              <td colspan="3" style="text-align: center;">
                  Department
              </td>
              </tr>
              <tr>
              <td><?php echo 'From Date'; ?></td>
              <td><?php echo 'To Date'; ?></td>
              <td><?php echo 'Department'; ?></td>
              </tr>
              <?php if($department_history){ ?>
              <?php foreach($department_history as $dkey => $dvalue){ ?>
                  <tr>
                  <td><?php echo $dvalue['from_date']; ?></td>
                  <td><?php echo $dvalue['to_date']; ?></td>
                  <td><?php echo $dvalue['value']; ?></td>
                  </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                  <td colspan="3" style="text-align: center;">
                  No Records Found
                  </td>
              </tr>
              <?php } ?>
            </table>
          <table class="form" style="width: 40%;">
              <tr>
              <td colspan="3" style="text-align: center;">
                  Site
              </td>
              </tr>
              <tr>
              <td><?php echo 'From Date'; ?></td>
              <td><?php echo 'To Date'; ?></td>
              <td><?php echo 'Site'; ?></td>
              </tr>
              <?php if($unit_history){ ?>
              <?php foreach($unit_history as $dkey => $dvalue){ ?>
                  <tr>
                  <td><?php echo $dvalue['from_date']; ?></td>
                  <td><?php echo $dvalue['to_date']; ?></td>
                  <td><?php echo $dvalue['value']; ?></td>
                  </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                  <td colspan="3" style="text-align: center;">
                  No Records Found
                  </td>
              </tr>
              <?php } ?>
            </table>
        </div>
        </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--

$('input[name=\'company11\']').autocomplete({
  delay: 500,
  source: function(request, response) {
  $.ajax({
    url: 'index.php?route=catalog/company/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
    dataType: 'json',
    success: function(json) {   
    response($.map(json, function(item) {
      return {
      label: item.company,
      value: item.company_id
      }
    }));
    }
  });
  }, 
  select: function(event, ui) {
  $('input[name=\'company\']').val(ui.item.label);
  $('input[name=\'company_id\']').val(ui.item.value);
  return false;
  },
  focus: function(event, ui) {
  return false;
  }
});

$('#company_id').on('change', function() {
  company_name = $('#company_id option:selected').text();
  $('#company').val(company_name);
  company_id = $('#company_id').val();
  if(company_id != '1'){
  grade_z25_id = '<?php echo $grade_z25_id; ?>';
  $('#grade_id').val(grade_z25_id);
  $('#grade').val('Z25');
  }
});

$('input[name=\'department11\']').autocomplete({
  delay: 500,
  source: function(request, response) {
  $.ajax({
    url: 'index.php?route=catalog/department/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
    dataType: 'json',
    success: function(json) {   
    response($.map(json, function(item) {
      return {
      label: item.d_name,
      value: item.department_id
      }
    }));
    }
  });
  }, 
  select: function(event, ui) {
  $('input[name=\'department\']').val(ui.item.label);
  $('input[name=\'department_id\']').val(ui.item.value);
  return false;
  },
  focus: function(event, ui) {
  return false;
  }
});

$('#department_id').on('change', function() {
  department_name = $('#department_id option:selected').text();
  $('#department').val(department_name);
});

//contractor
$('input[name=\'contractor11\']').autocomplete({
  delay: 500,
  source: function(request, response) {
  $.ajax({
    url: 'index.php?route=catalog/contractor/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
    dataType: 'json',
    success: function(json) {   
    response($.map(json, function(item) {
      return {
      label: item.contractor,
      value: item.contractor_id
      }
    }));
    }
  });
  }, 
  select: function(event, ui) {
  $('input[name=\'contractor\']').val(ui.item.label);
  $('input[name=\'contractor_id\']').val(ui.item.value);
  return false;
  },
  focus: function(event, ui) {
  return false;
  }
});

$('#contractor_id').on('change', function() {
    contractor_id = $('#contractor_id').val();
    $.ajax({
    url: 'index.php?route=catalog/employee/getcontractor_name&token=<?php echo $token; ?>&filter_contractor_id=' +  encodeURIComponent(contractor_id),
    dataType: 'json',
    success: function(json) {   
        if(json['contractor_name']){
        $('#contractor').val(json['contractor_name']);
        }
    }
    });
});

//category
$('input[name=\'category11\']').autocomplete({
  delay: 500,
  source: function(request, response) {
  $.ajax({
    url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
    dataType: 'json',
    success: function(json) {   
    response($.map(json, function(item) {
      return {
      label: item.category,
      value: item.category_id
      }
    }));
    }
  });
  }, 
  select: function(event, ui) {
  $('input[name=\'category\']').val(ui.item.label);
  $('input[name=\'category_id\']').val(ui.item.value);
  return false;
  },
  focus: function(event, ui) {
  return false;
  }
});

$('#category_id').on('change', function() {
  category_name = $('#category_id option:selected').text();
  $('#category').val(category_name);
});
//category end

$('input[name=\'designation11\']').autocomplete({
  delay: 500,
  source: function(request, response) {
  $.ajax({
    url: 'index.php?route=catalog/designation/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
    dataType: 'json',
    success: function(json) {   
    response($.map(json, function(item) {
      return {
      label: item.d_name,
      value: item.designation_id
      }
    }));
    }
  });
  }, 
  select: function(event, ui) {
  $('input[name=\'designation\']').val(ui.item.label);
  $('input[name=\'designation_id\']').val(ui.item.value);
  return false;
  },
  focus: function(event, ui) {
  return false;
  }
});

$('#designation_id').on('change', function() {
  designation_name = $('#designation_id option:selected').text();
  $('#designation').val(designation_name);
  designation = $('#designation_id').val();
  $.ajax({
  url: 'index.php?route=catalog/employee/getgrade&token=<?php echo $token; ?>&filter_designation_id=' +  encodeURIComponent(designation),
  dataType: 'json',
  success: function(json) {   
    //$('#state_id').find('option').remove();
    if(json['grade_datas']){
    $('#grade_id').val(json['grade_datas'][0]['grade_id']);
    $('#grade').val(json['grade_datas'][0]['grade']);
    }
  }
  });
});

$('input[name=\'grade11\']').autocomplete({
  delay: 500,
  source: function(request, response) {
  $.ajax({
    url: 'index.php?route=catalog/grade/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
    dataType: 'json',
    success: function(json) {   
    response($.map(json, function(item) {
      return {
      label: item.g_name,
      value: item.grade_id
      }
    }));
    }
  });
  }, 
  select: function(event, ui) {
  $('input[name=\'grade\']').val(ui.item.label);
  $('input[name=\'grade_id\']').val(ui.item.value);
  return false;
  },
  focus: function(event, ui) {
  return false;
  }
});

$('#grade_id').on('change', function() {
  grade_name = $('#grade_id option:selected').text();
  $('#grade').val(grade_name);
});

$('input[name=\'region11\']').autocomplete({
  delay: 500,
  source: function(request, response) {
  $.ajax({
    url: 'index.php?route=catalog/region/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
    dataType: 'json',
    success: function(json) {   
    response($.map(json, function(item) {
      return {
      label: item.region,
      value: item.region_id
      }
    }));
    }
  });
  }, 
  select: function(event, ui) {
  $('input[name=\'region\']').val(ui.item.label);
  $('input[name=\'region_id\']').val(ui.item.value);
  return false;
  },
  focus: function(event, ui) {
  return false;
  }
});

$('#region_id').on('change', function() {
  region = $(this).val();
  state = $('#state_id').val();
  region_name = $('#region_id option:selected').text();
  $('#region').val(region_name);
  $.ajax({
  url: 'index.php?route=catalog/employee/getdivision_location&token=<?php echo $token; ?>&filter_region_id=' +  encodeURIComponent(region)+'&filter_state_id=' +  encodeURIComponent(state),
  dataType: 'json',
  success: function(json) {   
    $('#division_id').find('option').remove();
    if(json['division_datas']){
    $.each(json['division_datas'], function (i, item) {
      $('#division_id').append($('<option>', { 
        value: item.division_id,
        text : item.division 
      }));
    });
    }
    $('#unit_id').find('option').remove();
    if(json['unit_datas']){
    $.each(json['unit_datas'], function (i, item) {
      $('#unit_id').append($('<option>', { 
        value: item.unit_id,
        text : item.unit 
      }));
    });
    }
  }
  });
});

$('input[name=\'division11\']').autocomplete({
  delay: 500,
  source: function(request, response) {
  filter_region_id = $('#region_id').val();
  $.ajax({
    url: 'index.php?route=catalog/division/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_region_id=' +  encodeURIComponent(filter_region_id),
    dataType: 'json',
    success: function(json) {   
    response($.map(json, function(item) {
      return {
      label: item.division,
      value: item.division_id
      }
    }));
    }
  });
  }, 
  select: function(event, ui) {
  $('input[name=\'division\']').val(ui.item.label);
  $('input[name=\'division_id\']').val(ui.item.value);
  return false;
  },
  focus: function(event, ui) {
  return false;
  }
});

$('#division_id').on('change', function() {
  division = $(this).val();
  state = $('#state_id').val();
  division_name = $('#division_id option:selected').text();
  $('#division').val(division_name);
  $.ajax({
  url: 'index.php?route=catalog/employee/getlocation_master&token=<?php echo $token; ?>&filter_division_id=' +  encodeURIComponent(division)+'&filter_state_id=' +  encodeURIComponent(state),
  dataType: 'json',
  success: function(json) {   
    $('#unit_id').find('option').remove();
    if(json['unit_datas']){
    $.each(json['unit_datas'], function (i, item) {
      $('#unit_id').append($('<option>', { 
        value: item.unit_id,
        text : item.unit 
      }));
    });
    }
  }
  });
});

$('input[name=\'unit11\']').autocomplete({
  delay: 500,
  source: function(request, response) {
  filter_division_id = $('#division_id').val();
  $.ajax({
    url: 'index.php?route=catalog/unit/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term)+'&filter_division_id=' +  encodeURIComponent(filter_division_id),
    dataType: 'json',
    success: function(json) {   
    response($.map(json, function(item) {
      return {
      label: item.unit,
      value: item.unit_id
      }
    }));
    }
  });
  }, 
  select: function(event, ui) {
  $('input[name=\'unit\']').val(ui.item.label);
  $('input[name=\'unit_id\']').val(ui.item.value);
  return false;
  },
  focus: function(event, ui) {
  return false;
  }
});

$('#unit_id').on('change', function() {
    unit_name = $('#unit_id option:selected').text();
    $('#unit').val(unit_name);
    unit = $('#unit_id').val();
    $.ajax({
    url: 'index.php?route=catalog/employee/getcontractors_by_unit_master&token=<?php echo $token; ?>&filter_unit_id=' +  encodeURIComponent(unit),
    dataType: 'json',
    success: function(json) {   
        $('#contractor_id').find('option').remove();
      if(json['contractor_datas']){
        $.each(json['contractor_datas'], function (i, item) {
            $('#contractor_id').append($('<option>', { 
              value: item.contractor_id,
              text : item.contractor_name
            }));
        });
      }
        //$('#state_id').find('option').remove();
        //if(json['state_datas']){
        //$('#state_id').val(json['state_datas'][0]['state_id']);
        //$('#state').val(json['state_datas'][0]['state']);
        //}
    } 
    });
});

$('input[name=\'state11\']').autocomplete({
  delay: 500,
  source: function(request, response) {
  $.ajax({
    url: 'index.php?route=catalog/state/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
    dataType: 'json',
    success: function(json) {   
    response($.map(json, function(item) {
      return {
      label: item.state,
      value: item.state_id
      }
    }));
    }
  });
  }, 
  select: function(event, ui) {
  $('input[name=\'state\']').val(ui.item.label);
  $('input[name=\'state_id\']').val(ui.item.value);
  return false;
  },
  focus: function(event, ui) {
  return false;
  }
});

$('#state_id').on('change', function() {
  state_name = $('#state_id option:selected').text();
  $('#state').val(state_name);
  /*
  state = $(this).val();
  division = $('#division_id').val();
  $.ajax({
  url: 'index.php?route=catalog/employee/getstate_locaton&token=<?php echo $token; ?>&filter_state_id=' +  encodeURIComponent(state)+'&filter_division_id=' +  encodeURIComponent(division),
  dataType: 'json',
  success: function(json) {   
    $('#unit_id').find('option').remove();
    if(json['unit_datas']){
    $.each(json['unit_datas'], function (i, item) {
      $('#unit_id').append($('<option>', { 
        value: item.unit_id,
        text : item.unit 
      }));
    });
    }
  }
  });
  */
});


$('input[name=\'employement11\']').autocomplete({
  delay: 500,
  source: function(request, response) {
  $.ajax({
    url: 'index.php?route=catalog/employement/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
    dataType: 'json',
    success: function(json) {   
    response($.map(json, function(item) {
      return {
      label: item.employement,
      value: item.employement_id
      }
    }));
    }
  });
  }, 
  select: function(event, ui) {
  $('input[name=\'employement\']').val(ui.item.label);
  $('input[name=\'employement_id\']').val(ui.item.value);
  return false;
  },
  focus: function(event, ui) {
  return false;
  }
});

$('#employement_id').on('change', function() {
  employement_name = $('#employement_id option:selected').text();
  $('#employement').val(employement_name);
});
//--></script>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'dd-mm-yy'});
});
//--></script>


<script type="text/javascript">
  $(document).on('keyup', '#name', function() {
    var e_name = $("#name").val();
    $("#name").val(e_name.trimStart());
  });


$('input[name=\'contractor\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/contautocomplete&token=<?php echo $token; ?>&filter_contractor=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.contractor_cname,
            name: item.name,
            value: item.contractor_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'contractor\']').val(ui.item.name);
    document.getElementById("filter_contractor").setAttribute("value", ui.item.name);
    document.getElementById("filter_contractor_id").setAttribute("value", ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});
</script>
<?php echo $footer; ?>