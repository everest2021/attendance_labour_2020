<?php echo $header; ?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
  $part = explode('/', $this->request->get['route']);

  if (isset($part[0])) {
    $route .= $part[0];
  }

  if (isset($part[1])) {
    $route .= '/' . $part[1];
  }
}
?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <?php if($this->user->hasPermission('modify', $route) || ($this->user->hasPermission('add', $route) && !isset($this->request->get['designation_id'])) ){ ?>
          <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
        <?php } ?>
        <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
      </div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs">
        <a href="#tab-general"><?php echo $tab_general; ?></a>
      </div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <table class="form">
            <tr>
              <td><span class="required">*</span> <?php echo 'Name'; ?></td>
              <td>
                <input tabindex="1" oninput="this.value = this.value.toUpperCase()" type="text" name="d_name" value="<?php echo $d_name; ?>" />
                <?php if ($error_title) { ?>
                  <span class="error"><?php echo $error_title; ?></span>
                <?php } ?>
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><?php echo 'Code'; ?></td>
              <td><input tabindex="1" oninput="this.value = this.value.toUpperCase()" type="text" name="d_code" value="<?php echo $d_code; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo 'Grade'; ?></td>
              <td>
                <select name="grade_id" id="grade_id">
                  <?php foreach($grade_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $grade_id) { ?>
                      <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
                <input type="hidden" name="grade_name" id="grade" value="<?php echo $grade_name; ?>" />
              </td>
            </tr>
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select tabindex="2" name="status">
                  <?php if ($status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#grade_id').on('change', function() {
    grade_name = $('#grade_id option:selected').text();
    $('#grade').val(grade_name);
  });
</script>
<?php echo $footer; ?>