<?php echo $header; ?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
  $part = explode('/', $this->request->get['route']);

  if (isset($part[0])) {
    $route .= $part[0];
  }

  if (isset($part[1])) {
    $route .= '/' . $part[1];
  }
}
?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <?php if($this->user->hasPermission('add', $route)){ ?>
          <a href="<?php echo $insert; ?>" class="button" style=""><?php echo 'Insert'; ?></a>
        <?php } ?>
        <?php if($this->user->hasPermission('delete', $route)){ ?>
        <a style="display: none;" onclick="$('form').submit();" class="button"><?php echo 'Delete'; ?></a>
        <?php } ?>
        <?php if($this->user->hasPermission('modify', $route)){ ?>
        <a href="<?php echo $export; ?>" class="button"><?php echo 'Export'; ?></a>
        <?php } ?>
      </div>
    </div>
    <div class="content">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left"><?php if ($sort == 'name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Name'; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"><?php echo 'Name'; ?></a>
                <?php } ?></td>
              <td class="right"><?php echo 'Action'; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td></td>
              <td><input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name; ?>" style="width:390px;" /></td>
              <td align="right"><a onclick="filter();" class="button"><?php echo 'Filter'; ?></a></td>
            </tr>
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
              <?php if ($shifts) { ?>
              <?php foreach ($shifts as $shift) { ?>
              <tr>
                <td style="text-align: center;"><?php if ($shift['selected']) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $shift['shift_id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $shift['shift_id']; ?>" />
                  <?php } ?></td>
                <td class="left"><?php echo $shift['name']; ?></td>
                <td class="right">
                  <?php //if($this->user->hasPermission('modify', $route)){ ?>
                    <?php foreach ($shift['action'] as $action) { ?>
                    [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                    <?php } ?>
                  <?php //} ?>
                </td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="3"><?php echo 'No Data'; ?></td>
              </tr>
              <?php } ?>
            </form>
          </tbody>
        </table>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/shift&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/shift/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.shift_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<?php echo $footer; ?>