<?php echo $header; ?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
  $part = explode('/', $this->request->get['route']);

  if (isset($part[0])) {
    $route .= $part[0];
  }

  if (isset($part[1])) {
    $route .= '/' . $part[1];
  }
}
?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <?php if($this->user->hasPermission('add', $route)){ ?>
          <a href="<?php echo $insert; ?>" class="button" style=""><?php echo 'Insert'; ?></a>
        <?php } ?>
        <?php if($this->user->hasPermission('delete', $route)){ ?>
        <a onclick="$('form').submit();" class="button"><?php echo 'Delete'; ?></a>
        <?php } ?>
        <?php if($this->user->hasPermission('modify', $route)){ ?>
        <a href="<?php echo $export; ?>" class="button"><?php echo 'Export'; ?></a>
        <?php } ?>
      </div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td class="left"><?php echo 'User Id'; ?></td>
              <td class="left"><?php echo 'User Name'; ?></td>
              <td class="left"><?php echo 'Date'; ?></td>
              <td class="left"><?php echo 'Status'; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($recalculate) { ?>
            <?php foreach ($recalculate as $rvalue) { ?>
            <tr>
              <td class="left"><?php echo $rvalue['user_id']; ?></td>
              <td class="left"><?php echo $rvalue['user_name']; ?></td>
              <td class="left"><?php echo $rvalue['date']; ?></td>
              <td class="left"><?php echo $rvalue['status']; ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <!-- <div class="pagination"><?php echo $pagination; ?></div> -->
    </div>
  </div>
</div>
<?php echo $footer; ?>