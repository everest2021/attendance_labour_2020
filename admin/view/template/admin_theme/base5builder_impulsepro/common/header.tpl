<?php

if(isset($this->request->get['route'])){
	$current_location = explode("/", $this->request->get['route']);
	if($current_location[0] == "common"){
		$is_homepage = TRUE;
	}else{
		$is_homepage = FALSE;
	}
}else{
	$is_homepage = FALSE;
}

$get_url = explode("&", $_SERVER['QUERY_STRING']);

$get_route = substr($get_url[0], 6);

$get_route = explode("/", $get_route);

$page_name = array("shoppica2","journal_banner","journal_bgslider","journal_cp","journal_filter","journal_gallery","journal_menu","journal_product_slider","journal_product_tabs","journal_rev_slider","journal_slider");

// array_push($page_name, "EDIT-ME");

if(array_intersect($page_name, $get_route)){
	$is_custom_page = TRUE;
}else{
	$is_custom_page = FALSE;
}

?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
  $part = explode('/', $this->request->get['route']);

  if (isset($part[0])) {
    $route .= $part[0];
  }

  if (isset($part[1])) {
    $route .= '/' . $part[1];
  }
}
?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" xml:lang="<?php echo $lang; ?>">
<head>
	<meta charset="utf-8" />
	<title><?php echo $title; ?></title>
	<base href="<?php echo $base; ?>" />
	<?php if ($description) { ?>
	<meta name="description" content="<?php echo $description; ?>" />
	<?php } ?>
	<?php if ($keywords) { ?>
	<meta name="keywords" content="<?php echo $keywords; ?>" />
	<?php } ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>

	<!-- Le styles -->
	<?php if(isset($this->request->get['route']) && $this->request->get['route'] == 'bill/bill_history/configuremail') { ?>
		<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
	<?php } ?>
	<?php if(!$is_custom_page){ ?>
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/bootstrap.css" rel="stylesheet" />
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style.css" rel="stylesheet" />
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/bootstrap-responsive.css" rel="stylesheet" />
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-responsive.css" rel="stylesheet" />
	<?php }else{ ?>
	<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" />
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-custom-page.css" rel="stylesheet" />
	<?php
}
?>

<?php /*  ?>
<link type="text/css" href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' />
<?php */  ?>

	  <!--[if IE 7]>
	  <link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-ie7.css" rel="stylesheet">
	  <![endif]-->
	  <!--[if IE 8]>
	  <link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-ie8.css" rel="stylesheet">
	  <![endif]-->
	  <?php if($route == 'report/performance' || $route == 'report/daily' || $route == 'report/dailysummary' || $route == 'report/late' || $route == 'report/lateearly' || $route == 'report/lateearlysumm' || $route == 'report/working' || $route == 'report/averageworking' || $route == 'report/muster' || $route == 'report/leave_register' || $route == 'report/employeehistory' || $route == 'report/leave_report' || $route == 'report/today' || $route == 'report/leave_report_type' || $route == 'report/manualpunch' || $route == 'report/early' || $route == 'report/lessworking' || $route == 'tool/recalculate' || $route == 'report/leave_credit' || $route == 'report/dailylabour' || $route == 'report/daily1') { ?> 
	  	<link type="text/css" href="view/stylesheet/jquery-ui.css" rel="stylesheet" />
	  	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/jquery-2.1.1.min.js"></script>
	  	<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
	  	<link type="text/css" href="view/stylesheet/bootstrap-multiselect.css" rel="stylesheet" />
	  	<script type="text/javascript" src="view/javascript/jquery/bootstrap-multiselect.js"></script>
	  	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.js"></script>
	  <?php } elseif($route == 'user/user') { ?>
		<link type="text/css" href="view/javascript/admin_theme/base5builder_impulsepro/ui/themes/ui-lightness/jquery-ui-1.8.20.custom-min.css" rel="stylesheet" />
	  	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/jquery-2.1.1.min.js"></script>
	  	<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
	  	<link type="text/css" href="view/stylesheet/bootstrap-multiselect.css" rel="stylesheet" />
	  	<script type="text/javascript" src="view/javascript/jquery/bootstrap-multiselect.js"></script>
	  	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/ui/jquery-ui-1.8.20.custom.min.js"></script>
	  <?php } else { ?>
	  	<link type="text/css" href="view/javascript/admin_theme/base5builder_impulsepro/ui/themes/ui-lightness/jquery-ui-1.8.20.custom-min.css" rel="stylesheet" />
	  	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/jquery.js"></script>
	  	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/ui/jquery-ui-1.8.20.custom.min.js"></script>
	  <?php } ?>
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/tabs.js"></script>
	  <script type="text/javascript" src="view/javascript/jquery/ui/external/jquery.cookie.js"></script>
	  <?php foreach ($styles as $style) { ?>
	  <link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
	  <?php } ?>
	  <?php foreach ($scripts as $script) { ?>
	  <script type="text/javascript" src="<?php echo $script; ?>"></script>
	  <?php } ?>
	  <?php if($this->user->getUserName() && $is_homepage){ ?>
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/jquery.flot.min.js"></script>
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/jquery.flot.pie.min.js"></script>
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/curvedLines.min.js"></script>
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/jquery.flot.tooltip.min.js"></script>
	  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/modernizr.js"></script>

		<!--[if lte IE 8]>
		<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/excanvas.min.js"></script>
		<![endif]-->
		<?php } ?>
		<script type="text/javascript">
		$(document).ready(function(){

		// Signin - Button

		$(".form-signin-body-right input").click(function(){
			$(".form-signin").submit();
		});

		// Signin - Enter Key

		$('.form-signin input').keydown(function(e) {
			if (e.keyCode == 13) {
				$('.form-signin').submit();
			}
		});

	    // Confirm Delete
	    $('#form').submit(function(){
	    	if ($(this).attr('action').indexOf('delete',1) != -1) {
	    		if (!confirm('<?php echo $text_confirm; ?>')) {
	    			return false;
	    		}
	    	}
	    });

		// Confirm Uninstall
		$('a').click(function(){
			if ($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall', 1) != -1) {
				if (!confirm('<?php echo $text_confirm; ?>')) {
					return false;
				}
			}
		});
	});
		</script>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
</head>

<body>
	<div class="container-fluid">
		<?php if ($logged) { ?>
		<div class="right-header-content clearfix" style="background-color: #F6F7F8;border: 2px solid #b8c9db;">
			<header id="header" class="navbar navbar-static-top">
				<nav class="navbar navbar-default" style="margin-bottom: 0px !important;" role="navigation">
					<div class="navbar-header" style="min-height: 0px;padding: 0;float: left;padding-top: 2px;padding-left: 5px;margin-right: 5px;">
					    <a href="<?php echo $home; ?>" class="navbar-brand">
					    	<img style="width:90px; height:40px;" src="view/image/logo_jmc.png" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" />
					    	<!-- Your Logo Here -->
					    </a>
				  	</div>
					<div class="collapse navbar-collapse navbar-ex1-collapse">
					    <ul class="nav navbar-nav">
				        	<?php if($user_dept == 0){ ?>
				        	<li class="root active">
				          		<a style="color: #384e73;" href="#" class="dropdown-toggle" data-toggle="dropdown">Master <b class="caret"></b></a>
				          		<ul class="dropdown-menu">
									<li><a href="<?php echo $company; ?>"><?php echo "Company"; ?></a></li>
									<li><a href="<?php echo $contractor; ?>"><?php echo "Contractor"; ?></a></li>
									<li><a href="<?php echo $division; ?>"><?php echo "Division"; ?></a></li>
									<li><a href="<?php echo $category; ?>"><?php echo "Category"; ?></a></li>
									<li><a href="<?php echo $unit; ?>"><?php echo "Site"; ?></a></li>
					            	<li><a href="<?php echo $department; ?>"><?php echo "Department"; ?></a></li>
									<li><a href="<?php echo $designation; ?>"><?php echo "Designation"; ?></a></li>
									<li><a href="<?php echo $grade; ?>"><?php echo "Grade"; ?></a></li>
									<li><a href="<?php echo $employement; ?>"><?php echo "Employment Type"; ?></a></li>
									<li><a href="<?php echo $employee; ?>"><?php echo "Employee"; ?></a></li>
									<li><a href="<?php echo $employee_change; ?>"><?php echo "Employee Transfer"; ?></a></li>
									<li><a href="<?php echo $employee_local; ?>"><?php echo "Employee Exit"; ?></a></li>
									<li><a href="<?php echo $shift; ?>"><?php echo "Shift"; ?></a></li>
									<li><a href="<?php echo $device; ?>"><?php echo "Device"; ?></a></li>
									<li style="display: none;"><a href="<?php echo $region; ?>"><?php echo "Region"; ?></a></li>
									<li style="display: none;"><a href="<?php echo $state; ?>"><?php echo "State"; ?></a></li>
									<li style="display:none;"><a href="<?php echo $week; ?>"><?php echo "Week Off"; ?></a></li>
									<li style="display: none;"><a href="<?php echo $holiday; ?>"><?php echo "Holiday"; ?></a></li>
									<li style="display:none;"><a href="<?php echo $halfday; ?>"><?php echo "Half Day"; ?></a></li>
									<li style="display: none;"><a href="<?php echo $shift_schedule; ?>"><?php echo "Shift Schedule"; ?></a></li>
									<li style="display:none;"><a href="<?php echo $complimentary; ?>"><?php echo "Compensatory Off"; ?></a></li>
				            	</ul>
				        	</li>
				        	<?php } ?>

				        	<?php if($user_dept == 0){ ?>
				        	<li class="root">
				        		<a style="color: #384e73;" href="#" class="dropdown-toggle" data-toggle="dropdown">Transaction <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li style="display:none;"><a href="<?php echo $tr_atten; ?>"><?php echo 'Day Processing'; ?></a></li>
									<li style="display:none;"><a href="<?php echo $requestformunit; ?>"><?php echo 'Request Application'; ?></a></li>
									<li style="display:none;"><a href="<?php echo $day_process; ?>"><?php echo 'Day closing'; ?></a></li>
									<li><a href="<?php echo $manualpunch; ?>"><?php echo 'Manual Punch'; ?></a></li>
								</ul>
							</li>
							<?php } ?>

							<?php if($user_dept == 0){ ?>
							<li class="root">
				        		<a style="color: #384e73;" href="#" class="dropdown-toggle" data-toggle="dropdown">Reports <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo $today_report; ?>"><?php echo "Today's Attendance"; ?></a></li>
									<li><a href="<?php echo $daily; ?>"><?php echo 'Daily In/Out Report'; ?></a></li>
									<li><a href="<?php echo $daily1; ?>"><?php echo 'Daily In/Out 1 Report'; ?></a></li>
									<li><a href="<?php echo $dailylabour; ?>"><?php echo 'Daily Labour Report'; ?></a></li>
									<li><a href="<?php echo $muster; ?>"><?php echo 'Monthly Muster Report'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $attendance_report; ?>"><?php echo 'Daily Attendance'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $dailysummary; ?>"><?php echo 'Daily Summary Report'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $late; ?>"><?php echo 'Late Arrival Report'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $early; ?>"><?php echo 'Early Going Report'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $lessworking; ?>"><?php echo 'Less Working Hours Report'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $lateearly; ?>"><?php echo 'Late / Early / Less Report'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $lateearlysumm; ?>"><?php echo 'Late / Early / Less Summary Report'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $working; ?>"><?php echo 'Working Hours Daily Report'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $averageworking; ?>"><?php echo 'Average Working Hours Report'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $performance_report; ?>"><?php echo 'Monthly Detailed Report'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $deficit_report; ?>"><?php echo 'Deficit Report'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $leave_report_type; ?>"><?php echo 'Out Door Entry Report'; ?></a></li>
									<li><a href="<?php echo $employeehistory; ?>"><?php echo 'Employee Transfer History Report'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $manualpunchreport; ?>"><?php echo 'Manual Punch Report'; ?></a></li>
									<li style="display: none;"><a href="<?php echo $lta; ?>"><?php echo 'LTA Register'; ?></a></li>
								</ul>
							</li>
							<?php } ?>

							<?php if($user_dept == 1 || $is_super1 == '1'){ ?>
								<li class="root">
				        			<a style="color: #384e73;" href="#" class="dropdown-toggle" data-toggle="dropdown">Reports <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li><a href="<?php echo $performance_report; ?>"><?php echo 'Periodic Attendance'; ?></a></li>
									</ul>
								</li>
							<?php } ?>

							<li class="root" style="display: none;">
			        			<a style="color: #384e73;" href="#" class="dropdown-toggle" data-toggle="dropdown">Leave <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<?php if($user_dept == 0){ ?>
										<li><a href="<?php echo $leavemaster; ?>"><?php echo "Leave Master"; ?></a></li>
										<li><a href="<?php echo $leave; ?>"><?php echo "Employee Leave Master"; ?></a></li>
									<?php } ?>

									<?php if($is_dept == 1){ ?>
										<li><a href="<?php echo $leave_transaction; ?>"><?php echo 'Self Leave Entry'; ?></a></li>
									<?php } elseif($is_super == 1){ ?>
										<li><a href="<?php echo $leave_transaction; ?>"><?php echo 'Self Leave Entry'; ?></a></li>
									<?php } elseif($is_super1 == 1){ ?>
									
									<?php } else { ?>
										<li><a href="<?php echo $leave_transaction; ?>"><?php echo 'Leave Entry'; ?></a></li>
									<?php } ?>
									
									<?php if($is_dept == 1){ ?>
										<li><a href="<?php echo $leave_transaction_dept; ?>"><?php echo 'Dept Leave Approval'; ?></a></li>
									<?php } elseif($is_super == 1 || $is_super1 == 1){ ?>
										<li><a href="<?php echo $leave_transaction_super; ?>"><?php echo 'Dept Leave Approval'; ?></a></li>
									<?php } ?>

									<?php if(isset($this->session->data['is_user'])){ ?>
										<li><a href="<?php echo $requestform; ?>"><?php echo 'Request Form'; ?></a></li>
									<?php } elseif($is_dept == 1){ ?>
										<li><a href="<?php echo $requestform; ?>"><?php echo 'Self Request Form'; ?></a></li>
									<?php } elseif($is_super == 1){ ?>
										<li><a href="<?php echo $requestform; ?>"><?php echo 'Self Request Form'; ?></a></li>
									<?php } ?>

									<?php if($is_dept == 1){ ?>
										<li><a href="<?php echo $requestformdept; ?>"><?php echo 'Request Approval Form'; ?></a></li>
									<?php } ?>

									<?php if($user_dept == 0){ ?>
									<li><a href="<?php echo $leave_process; ?>"><?php echo 'Leave Processing'; ?></a></li>
									<?php } ?>

									<li><a href="<?php echo $leave_led; ?>"><?php echo 'Leave Ledger'; ?></a></li>
									<li><a href="<?php echo $leave_report; ?>"><?php echo 'Leave Report'; ?></a></li>
									<li><a href="<?php echo $leave_credit; ?>"><?php echo 'Leave Credit Report'; ?></a></li>
								</ul>
							</li>

							<?php if($this->user->getId() == '1'){ ?>
								<li class="root">
					        		<a style="color: #384e73;" href="#" class="dropdown-toggle" data-toggle="dropdown">Utility <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li><a href="<?php echo $user; ?>"><?php echo "User"; ?></a></li>
										<li><a href="<?php echo $user_group; ?>"><?php echo 'User Roles'; ?></a></li>
										<?php if($this->user->hasPermission('modify', 'tool/backup')) { ?>
											<li><a href="<?php echo $backup; ?>"><?php echo 'Data Import'; ?></a></li>
										<?php } ?>
										<?php if($this->user->hasPermission('modify', 'tool/empimport')) { ?>
											<li><a href="<?php echo $empimport; ?>"><?php echo 'Employee Import'; ?></a></li>
										<?php } ?>
										<li><a target="_blank" href="http://10.200.1.35:4370/labour/service/dataprocess.php?daytime=1"><?php echo 'Update New Attendance'; ?></a></li>
										<li><a href="<?php echo HTTP_CATALOG.'service/reset_process_status.php?http_catalog='.HTTP_CATALOG ?>"><?php echo 'Reset Process Status'; ?></a></li>
										<li><a href="<?php echo $recalculate ?>"><?php echo 'Recalculate Attendance'; ?></a></li>
										<li><a href="<?php echo $new_employee; ?>"><?php echo 'New Employee'; ?></a></li>
									</ul>
								</li>
							<?php } else { ?>
								<li class="root">
					        		<a style="color: #384e73;" href="#" class="dropdown-toggle" data-toggle="dropdown">Utility <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li><a href="<?php echo $user; ?>"><?php echo "User"; ?></a></li>
										<?php if($this->user->hasPermission('modify', 'tool/backup')) { ?>
											<li><a href="<?php echo $backup; ?>"><?php echo 'Data Import'; ?></a></li>
										<?php } ?>
										<?php if($this->user->hasPermission('modify', 'tool/empimport')) { ?>
											<li><a href="<?php echo $empimport; ?>"><?php echo 'Employee Import'; ?></a></li>
										<?php } ?>
										<?php if($this->user->hasPermission('modify', 'tool/externalapi')) { ?>
											<li><a target="_blank" href="http://10.200.1.35:4370/labour/service/dataprocess.php?daytime=1"><?php echo 'Update New Attendance'; ?></a></li>
										<?php } ?>
										<?php if($this->user->hasPermission('modify', 'tool/recalculate')) { ?>
											<li><a href="<?php echo $recalculate; ?>"><?php echo 'Recalculate Attendance'; ?></a></li>
										<?php } ?>
										<?php if($this->user->hasPermission('modify', 'report/new_employee')) { ?>
											<li><a href="<?php echo $new_employee; ?>"><?php echo 'New Employee'; ?></a></li>
										<?php } ?>
									</ul>
								</li>
							<?php } ?>
				      	</ul>
		    		</div><!-- /.navbar-collapse -->
					<div class="secondary-menu">
						<ul>
							<li id="logout"><a class="top" href="<?php echo $logout; ?>"><span><?php echo $text_logout; ?></span></a></li>
						</ul>
					</div>
					<div class="admin-info"><?php echo $logged; ?></div>
				</nav>
			</header>
		</div>
		<?php } ?>
