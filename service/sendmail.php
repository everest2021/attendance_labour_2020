<?php 
error_reporting(E_ALL);
ini_set("display_errors", 1);
# Include the Autoloader (see "Libraries" for install instructions)
require 'vendor/autoload.php';
use Mailgun\Mailgun;

$client = new \Http\Adapter\Guzzle6\Client();
$mailgun = new \Mailgun\Mailgun('key-ce2b6f6cb8877156254b3f617f9fd4c0', $client);
# Instantiate the client.
$domain = "sandbox2340d557f87e437d90d554557995fdc8.mailgun.org";
$html = file_get_contents('test_html.html');


//$result = $mailgun->get("/lists");


// //creating campaign list
// $ch = curl_init();
// curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
// curl_setopt($ch, CURLOPT_USERPWD, 'api:key-ce2b6f6cb8877156254b3f617f9fd4c0');
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
// curl_setopt($ch, CURLOPT_URL, 
//           'https://api.mailgun.net/v3/sandbox2340d557f87e437d90d554557995fdc8.mailgun.org/campaigns');
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//             array('name' => 'Invitation Club Member',
//                   'id' => '7777777',
//           	));
// $result = curl_exec($ch);
// curl_close($ch);

//get campaign list
// $ch = curl_init();
// curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
// curl_setopt($ch, CURLOPT_USERPWD, 'api:key-ce2b6f6cb8877156254b3f617f9fd4c0');
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
// curl_setopt($ch, CURLOPT_URL, 
//           'https://api.mailgun.net/v3/sandbox2340d557f87e437d90d554557995fdc8.mailgun.org/campaigns');
// $result = curl_exec($ch);
// curl_close($ch);

// echo '<pre>';
// print_r($result);
// exit;


//creating mail list
// $ch = curl_init();
// curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
// curl_setopt($ch, CURLOPT_USERPWD, 'api:key-ce2b6f6cb8877156254b3f617f9fd4c0');
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
// curl_setopt($ch, CURLOPT_URL, 
//           'https://api.mailgun.net/v3/lists');
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//             array('address' => 'mail11@sandbox2340d557f87e437d90d554557995fdc8.mailgun.org',
//                   'description' => 'Mailgun1 mailing1 list1',
//                   'name' => 'Mailgun1 mailing1 list1'));
// $result = curl_exec($ch);
// curl_close($ch);

//creating mail list memeber
// $ch = curl_init();
// curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
// curl_setopt($ch, CURLOPT_USERPWD, 'api:key-ce2b6f6cb8877156254b3f617f9fd4c0');
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
// curl_setopt($ch, CURLOPT_URL, 
//           'https://api.mailgun.net/v3/lists/mail1@sandbox2340d557f87e437d90d554557995fdc8.mailgun.org/members');
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//             array('subscribed' => True,
//                   'address' => 'fargose.aaron@gmail.com',
//                   'name' => 'Aaron Fargose',
//                   'description' => 'Developer',
//                   'vars' => '{"age":25}'
//             ));
// $result = curl_exec($ch);
// curl_close($ch);

//echo '<pre>';
//print_r($result);
//exit;


// curl -s --user 'api:key-ce2b6f6cb8877156254b3f617f9fd4c0' \
//     https://api.mailgun.net/v3/lists \
//     -F address='mail1@sandbox2340d557f87e437d90d554557995fdc8.mailgun.org' \
//     -F description='Mailgun mailing list'


// curl -s --user 'api:key-ce2b6f6cb8877156254b3f617f9fd4c0' \
//     https://api.mailgun.net/v3/lists/mail1@sandbox2340d557f87e437d90d554557995fdc8.mailgun.org/members \
//     -F subscribed=True \
//     -F address='eric.frgose@gmail.com' \
//     -F name='Eric Fargose' \
//     -F description='Developer' \
//     -F vars='{"age": 27}'


// $batchMsg = $mailgun->BatchMessage($domain);
// $batchMsg->setFromAddress("postmaster@sandbox2340d557f87e437d90d554557995fdc8.mailgun.org", 
//                           array("first"=>"Aaron", "last" => "Fargose"));
// # Define the subject. 
// $batchMsg->setSubject("Help!");
// # Define the body of the message.
// $batchMsg->setTextBody("The printer is on fire!");
// # Next, let's add a few recipients to the batch job.
// $batchMsg->addToRecipient("fargose.aaron@gmail.com", 
//                           array("first" => "Aaron", "last" => "Fargose"));
// $batchMsg->addToRecipient("eric.fargose@.com", 
//                           array("first" => "Eric", "last" => "Fargose"));
// $batchMsg->finalize();
// echo 'Done';exit;
// echo '<pre>';
// print_r($html);
// exit;

# Make the call to the client.
$result = $mailgun->sendMessage($domain, array(
    'from'    => 'Aaron Fargose <postmaster@sandbox2340d557f87e437d90d554557995fdc8.mailgun.org>',
    'to'      => 'Club Members <mail11@sandbox2340d557f87e437d90d554557995fdc8.mailgun.org>',
    'subject' => 'Hello',
    'text'    => 'Testing some Mailgun awesomness!<a href="http://www.google.com"></a>',
	'html'    => $html,
	'o:campaign' => 'Invitation Club Member'
	));

$id = $result->http_response_body->id;
echo '<pre>';
print_r($result);
echo $id;exit;
?>