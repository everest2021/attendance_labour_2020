<?php
$servername = "localhost";
$username = "root";
$password = "JmC@2018";
$dbname = "db_attendance_jmc";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}
$months_array = array(
	//'5' => '05',
	//'6' => '06',
	//'7' => '07',
	//'8' => '08',
	//'9' => '09',
	//'10' => '10',
	'11' => '11',
);
if(date('j') === '27') {
	foreach($months_array as $mkey => $mvalue){
		//$today_date = date('Y-m-d');
		$today_date = '2018-'.$mvalue.'-07';
		//$compare_dates = date('Y-m-26');
		$compare_dates = '2018-'.$mvalue.'-26';
		$compare_date = date('Y-m-d', strtotime($compare_dates.' -2 month'));
		$employee_datas_sql = "SELECT  `emp_code`, `company`, `company_id`, `region`, `region_id`, `division`, `division_id`, `department`, `department_id`, `unit`, `unit_id`, `doj`, `name`  FROM `oc_employee` WHERE `emp_code` IN ('7512108', '7529896', '8001148', '8001336', '8001521', '8001660', '8001829', '8002268', '8002611', '8002803', '8003100', '8003404', '8003417', '8003657', '8003774', '8003808', '8003939', '8003941', '8003981', '8003983', '8003998', '8003999', '8004025', '8004026', '8004027', '8004032', '8004145', '8004147', '8004149', '8004151', '8004152', '8004153', '8004159', '8004168', '8004171', '8004172', '8004174', '8004175', '8004191', '8004194', '8004195', '8004196', '8004210', '8004211', '8004212', '8004215', '8004291', '8004292', '8004293', '8004294', '8004295', '8004296', '8004298', '8004300', '8004305', '8004306', '8004320', '8004323', '8004324', '8004327', '8004328', '8004329', '8004373', '8004374', '8004375', '8004376', '8004382', '8004383', '8004387', '8004388', '8004392', '8004396', '8004399', '8004401', '8004427', '8004432', '8004433', '8004434', '8004435', '8004436', '8004437', '8004440', '8004441', '8004444', '8004446', '8004449', '8004450', '8004455', '8004475', '8004476', '8004492', '8004494', '8004497', '8004502', '8004503', '8004505', '8004506', '8004507', '8004514', '8004515', '8004585', '8004589', '8004592', '8004593', '8004594', '8004598', '8004601', '8004603', '8004606', '8004608', '8004609', '8004610', '8004617', '8004618', '8004649', '8004652', '8004654', '8004655', '8004657', '8004690', '8004691', '8004692', '8004693', '8004694', '8004721', '8004725', '8004775', '8004782', '8004802', '8004803', '8004805', '8004827', '8004832', '8004847', '8004850', '8004887', '8004964', '8005152', '8005248', '8005255', '8005340', '8005348', '8005374', '8005375', '8005376', '8005377', '8005378', '8005379', '8005380', '8005381', '8005382', '8005383', '8005384', '8005385', '8005386', '8005387', '8005388', '8505388', '8506200', '8506494', '8506495', '8506654', '8506671', '8506694', '8506695', '8506696', '8506697', '8506698', '8506699', '8506700', '8506701', '10190024', '10190071', '10220128', '10250074', '10270011', '10270012', '10270013', '10270014', '10270015', '10586530', '30170259', '30170260', '30170261', '30170262', '30170263', '30170264', '30170266', '30170267', '30170268', '30220653', '30220656', '30220659', '30220671', '30220672', '30220675', '30250242', '30250243', '30250244', '30250245', '30250246', '30250248', '30250249', '30250252', '30250253', '30250254', '30250255', '30250256', '30250257', '30260021', '30260022', '30260023', '30320024', '30320025', '30320026', '30330015', '30330016', '30330017', '30330018', '30330019', '30340044', '30340045', '30340046', '30340047', '30340048', '30340049', '30340050', '30340051', '30340052', '30340053', '30340054', '30340055', '30340056', '30340057', '30340058', '30340059', '30340060', '30350018', '30350019', '30350020', '30350021', '30350022', '30370002', '30370005', '30380001', '30380002', '30380003', '30380004', '30380007', '30380008', '30380009', '30380010', '30380011', '30380012', '30380013', '30380014', '30380015', '30380016', '50040185', '50150127', '50590005', '50650030', '50680042', '50680058', '50680061', '50680062', '50680063', '50680064', '50680065', '50680066', '50690029', '50770005', '50770011', '50800001', '60020076', '60380061', '60460171', '60460189', '60470183', '60470230', '60530004', '60530026', '60530073', '60540019', '60540037', '60540058', '60540072', '60540092', '60540108', '60540129', '60540134', '60540150', '60540179', '60540190', '60540191', '60540196', '60540212', '60540217', '60540236', '60540239', '60540247', '60540250', '60540257', '60540258', '60540260', '60540261', '60540263', '60540266', '60540282', '60540287', '60540288', '60540291', '60540297', '60540305', '60540309', '60540310', '60540312', '60540316', '60540319', '60540323', '60540324', '60540326', '60540327', '60540335', '60540341', '60540342', '60540345', '60540347', '60540349', '60540350', '60540359', '60540360', '60540362', '60540363', '60540364', '60540368', '60540370', '60540371', '60540372', '60540373', '60540376', '60540378', '60540379', '60540380', '60540382', '60540384', '60540385', '60540388', '60540393', '60540395', '60540396', '60540398', '60540400', '60540404', '60540405', '60540406', '60540407', '60540408', '60540409', '60540410', '60540411', '60540412', '60540413', '60540414', '60570029', '60570030', '60570031', '60570032', '60580026', '60580027', '60580028', '60580029', '60580030', '60580042', '60580051', '60600003', '80000044', '80000046', '90100001', '104813821', '104830763', '104836494', '104838525', '104854106', '104859515', '104864688', '104866992', '104869226', '104876990', '104877613', '104879076', '104880083', '104914162', '104915157', '104921744', '104928677', '104943193', '104948951', '104951654', '104952231', '104975040', '104976898', '104978435', '104979371', '104986216', '105030307', '105037906', '105042729', '105048719', '105052694', '105060050', '105069394', '105072882', '105081766', '105084013', '105111371', '105138687', '105176348', '105198313', '105209284', '105244257', '105371252', '105386066', '105388031', '105391127', '105406253', '105407010', '105418080', '105420950', '105438575', '105439148', '105494416', '105521520', '105562201', '105568979', '105573265', '105573894', '105575466', '105578771', '105597069', '105603893', '105610433', '105617143', '105617346', '105636444', '105638113', '105643982', '105644902', '105644905', '105650915', '105655711', '105669982', '105675287', '105682935', '105698850', '105703651', '105708136', '105742104', '105757501', '105757797', '105758722', '105765693', '105767551', '105770357', '105787699', '105792453', '105801740', '105804122', '105804162', '105806187', '105806627', '105807825', '105811304', '105813229', '105813816', '105817471', '105819640', '105821279', '105824493', '105829679', '105836179', '105839516', '105842352', '105855911', '105861876', '105862230', '105862232', '105862903', '105864162', '105867580', '105870855', '105873420', '105877480', '105879302', '105880243', '105880367', '105883610', '105885012', '105885013', '105885203', '105885838', '105886116', '105889036', '105889571', '105891978', '105894033', '105895552', '105895711', '105897122', '300350002', '960460021') AND `company_id` <> '1' AND (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$compare_date."') ";
		$employee_datas = query($employee_datas_sql, $conn)->rows;
		// echo $employee_datas_sql;
		// echo '<br />';
		//exit;
		// echo '<pre>';
		// print_r($employee_datas);
		// exit;
		//$from_dates = date('Y-m-26');
		$from_dates = '2018-'.$mvalue.'-26';
		$from_date = date('Y-m-d', strtotime($from_dates.' -2 month'));
		//$to_dates = date('Y-m-25');
		$to_dates = '2018-'.$mvalue.'-25';
		$to_date = date('Y-m-d', strtotime($to_dates.' -1 month'));

		$current_month = date('n', strtotime($to_date . ' +0 month'));
		if($current_month == 1){
			$current_year = date('Y', strtotime($to_date . ' +0 month'));
		} else {
			$current_year = date('Y', strtotime($to_date . ' +0 month'));
		}
		//echo $employee_datas_sql;exit;
		foreach($employee_datas as $ekey => $evalue){
			$transaction_datas = query("SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$evalue['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' ", $conn)->rows;
			// echo '<pre>';
			// print_r($transaction_datas);
			// exit;
			//echo "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$evalue['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' ";exit;
			$present_count = 0;
			$leave_count = 0;
			$count_t = count($transaction_datas) - 1;
			foreach($transaction_datas as $tkey => $tvalue){
				if($tvalue['leave_status'] == '1'){
					if($tvalue['firsthalf_status'] == 'OD'){
						$present_count = $present_count + 1;
					}
					if($tvalue['firsthalf_status'] == 'PL'){
						$leave_count = $leave_count + 1;
						$present_count = $present_count + 1;
					}
				} elseif($tvalue['leave_status'] == '0.5'){
					if($tvalue['firsthalf_status'] == 'OD'){
						$present_count = $present_count + 0.5;
						if($tvalue['secondhalf_status'] == '1'){
							$present_count = $present_count + 0.5;
						}
					}
					if($tvalue['secondhalf_status'] == 'OD'){
						$present_count = $present_count + 0.5;
						if($tvalue['firsthalf_status'] == '1'){
							$present_count = $present_count + 0.5;
						}
					}
					if($tvalue['firsthalf_status'] == 'PL'){
						$leave_count = $leave_count + 0.5;
						$present_count = $present_count + 0.5;
						if($tvalue['secondhalf_status'] == '1'){
							$present_count = $present_count + 0.5;
						}
					}
					if($tvalue['secondhalf_status'] == 'PL'){
						$leave_count = $leave_count + 0.5;
						$present_count = $present_count + 0.5;
						if($tvalue['firsthalf_status'] == '1'){
							$present_count = $present_count + 0.5;
						}
					}
				} elseif($tvalue['weekly_off'] != '0'){
					if($tkey == 0){
						$last_key = $tkey;
						$last_key_1 = $last_key;
					} else {
						$last_key = $tkey - 1;
						if($last_key == 0){
							$last_key_1 = $last_key;
						} else {
							$last_key_1 = $last_key - 1;
						}
					}
					if($count_t == $tkey){
						$next_key = $tkey;
						$next_key_1 = $next_key;
					} else {
						$next_key = $tkey + 1;	
						$next_key_1 = $next_key + 1;
					}
					
					if(isset($transaction_datas[$last_key]['company_id']) && $transaction_datas[$last_key]['company_id'] <> '1'){
						if($transaction_datas[$last_key]['act_intime'] != '00:00:00' && $transaction_datas[$last_key]['act_outtime'] == '00:00:00'){
							$last_exception = '1';
						} else {
							$last_exception = '0';
						}
					} else {
						$last_exception = '0';
					}

					if(isset($transaction_datas[$next_key]['company_id']) && $transaction_datas[$next_key]['company_id'] <> '1'){
						if($transaction_datas[$next_key]['act_intime'] != '00:00:00' && $transaction_datas[$next_key]['act_outtime'] == '00:00:00'){
							$next_exception = '1';
						} else {
							$next_exception = '0';
						}
					} else {
						$next_exception = '0';
					}

					if(isset($transaction_datas[$last_key_1]['company_id']) && $transaction_datas[$last_key_1]['company_id'] <> '1'){
						if($transaction_datas[$last_key_1]['act_intime'] != '00:00:00' && $transaction_datas[$last_key_1]['act_outtime'] == '00:00:00'){
							$last_exception_1 = '1';
						} else {
							$last_exception_1 = '0';
						}
					} else {
						$last_exception_1 = '0';
					}

					if(isset($transaction_datas[$next_key_1]['company_id']) && $transaction_datas[$next_key_1]['company_id'] <> '1'){
						if($transaction_datas[$next_key_1]['act_intime'] != '00:00:00' && $transaction_datas[$next_key_1]['act_outtime'] == '00:00:00'){
							$next_exception_1 = '1';
						} else {
							$next_exception_1 = '0';
						}
					} else {
						$next_exception_1 = '0';
					}

					if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1' && $last_exception == '0' && $next_exception == '0'){
					} else {
						if($transaction_datas[$next_key]['weekly_off'] != '0'){
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key_1]['absent_status'] == '1' && $last_exception == '0' && $next_exception_1 == '0'){
							} else {
								$present_count = $present_count + 1;		
							}
						} else {
							if($transaction_datas[$last_key]['weekly_off'] != '0'){
								if($transaction_datas[$last_key_1]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1' && $last_exception_1 == '0' && $next_exception == '0'){
								} else {
									$present_count = $present_count + 1;		
								}	
							} else {
								$present_count = $present_count + 1;	
							}
						}
					}
				} elseif($tvalue['holiday_id'] != '0'){
					$present_count = $present_count + 1;
				} elseif($tvalue['present_status'] == '1'){
					$present_count = $present_count + 1;
				} elseif($tvalue['present_status'] == '0.5'){
					$present_count = $present_count + 0.5;
				} elseif($tvalue['act_intime'] != '00:00:00' && $tvalue['act_outtime'] == '00:00:00'){
					$present_count = $present_count + 1;
				}
			}
			
			// echo $evalue['emp_code'];
			// echo '<br />';
			// echo $present_count;
			// echo '<br />';
			// exit;

			if($present_count >= '9.5'){
				if($present_count >= '9.5' && $present_count <= '14.5'){
					$bal_to_add = 0.5;
				} elseif($present_count >= '15' && $present_count <= '24.5'){
					$bal_to_add = 1;	
				} else {
					$bal_to_add = 1.5;
				}

				// echo $bal_to_add;
				// echo '<br />';
				// exit;
				if(date('n') == 4){
					//echo 'aaaa';exit;
					$is_exist_leaves = query("SELECT * FROM `oc_leave` WHERE `emp_id` = '".$evalue['emp_code']."' AND `close_status` = '0' ", $conn);
					if($is_exist_leaves->num_rows > 0){
						$is_exist_leave = $is_exist_leaves->row;
						
						$is_exist_pls = query("SELECT * FROM `oc_leave_employee` WHERE `leave_id` = '".$is_exist_leave['leave_id']."' AND `leavename` = 'PL' ", $conn);
						if($is_exist_pls->num_rows > 0){
							$update_sql = "UPDATE `oc_leave_employee` SET `value` = (`value` + ".$bal_to_add.") WHERE `id` = '".$is_exist_pls->row['id']."' ";
						} else {
							$update_sql = "INSERT INTO `oc_leave_employee` SET `leave_id` = '".$is_exist_leave['leave_id']."', `emp_id` = '".$is_exist_leave['emp_id']."', `leavename` = 'PL', `value` = '".$bal_to_add."' , `value_open` = '".$bal_to_add."' ";
						}
						// echo $update_sql;
						// echo '<br />';
						//exit;
						query($update_sql, $conn);

						$is_exist_pls = query("SELECT * FROM `oc_leave_employee` WHERE `leave_id` = '".$is_exist_leave['leave_id']."' AND `leavename` = 'PL' ", $conn);
						if($is_exist_pls->num_rows > 0){
							$is_exist_pl = $is_exist_pls->row;
							$update_old_leave_sql = "UPDATE `oc_leave` SET `close_status` = '1' WHERE `leave_id` = '".$is_exist_leave['leave_id']."' ";
							// echo $update_old_leave_sql;
							// echo '<br />';
							query($update_old_leave_sql, $conn);

							$db_value = $is_exist_pl['value'];
							$acc_value = $is_exist_pl['value'];
							$year = $is_exist_leave['year'];
							$year_1 = $is_exist_leave['year'] + 1;
							$comp_date = $year.'-03-26';
							$comp_date_end = $year_1.'-03-25';
							
							$sql = "SELECT COUNT(*) as `bal_p`, `encash` FROM `oc_leave_transaction` WHERE `emp_id` = '" . $evalue['emp_code'] . "' AND `leave_amount` = '' AND `leave_type`= 'PL' AND (`p_status` = '1') AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' AND date(`date`) <= '".$comp_date_end."' ";
							// echo $sql;
							// echo '<br />';
							$leave_1_datas = query($sql, $conn);
							$bal_p = 0;
							if($leave_1_datas->num_rows > 0){
								$leave_1_data = $leave_1_datas->row;
								$bal_p = $leave_1_data['bal_p'];
								if($bal_p > 0){
								} else {
									$bal_p = 0;
								}
							}

							$comp_date = $year.'-03-26';
							$comp_date_end = $year_1.'-03-25';
							$sql1 = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction` WHERE `emp_id` = '" . $evalue['emp_code'] . "' AND `days` = '' AND `leave_type`= 'PL' AND (`p_status` = '1') AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' AND date(`date`) <= '".$comp_date_end."' ";
							// echo $sql1;
							// echo '<br />';
							$leave_2_datas = query($sql1, $conn);
							$leave_amount = 0;
							if($leave_2_datas->num_rows > 0){
								$leave_2_data = $leave_2_datas->row;
								$leave_amount = $leave_2_data['leave_amount'];
								if($leave_amount > 0){
								} else {
									$leave_amount = 0;
								}
							}
							$total_bal = $acc_value - ($bal_p + $leave_amount);
							$db_value = $total_bal + $bal_to_add;
							
							if($db_value > 18){
								$db_value = 18;
							}
							$bal_to_add = $db_value;
							
							$insert_leave_sql = "INSERT INTO `oc_leave` SET 
										`emp_id` = '".$evalue['emp_code']."', 
										`emp_name` = '".$evalue['name']."', 
										`emp_doj` = '".$evalue['doj']."',
										`pl_acc` = '".$bal_to_add."',
										`year` = '".date('Y')."',
										`close_status` = '0' "; 
							// echo $insert_leave_sql;
							// echo '<br />';
							// $new_leave_id = 1;
							query($insert_leave_sql, $conn);
							$new_leave_id = getLastId($conn);

							$update_new_sql = "INSERT INTO `oc_leave_employee` SET `leave_id` = '".$new_leave_id."', `emp_id` = '".$is_exist_leave['emp_id']."', `leavename` = 'PL', `value` = '".$bal_to_add."', `value_open` = '".$bal_to_add."' ";
							// echo $update_new_sql;
							// echo '<br />';
							query($update_new_sql, $conn);

						}  else {
							$bal_to_add = $bal_to_add;
						
							$update_old_leave_sql = "UPDATE `oc_leave` SET `close_status` = '0' WHERE `leave_id` = '".$is_exist_leave['leave_id']."' ";
							// echo $update_old_leave_sql;
							// echo '<br />';
							query($insert_leave_sql, $conn);				

							$insert_leave_sql = "INSERT INTO `oc_leave` SET 
										`emp_id` = '".$evalue['emp_code']."', 
										`emp_name` = '".$evalue['name']."', 
										`emp_doj` = '".$evalue['doj']."',
										`pl_acc` = '".$bal_to_add."',
										`year` = '".date('Y')."',
										`close_status` = '0' "; 
							// echo $insert_leave_sql;
							// echo '<br />';
							//$new_leave_id = 1;
							query($insert_leave_sql, $conn);
							$new_leave_id = getLastId($conn);

							$update_new_sql = "INSERT INTO `oc_leave_employee` SET `leave_id` = '".$new_leave_id."', `emp_id` = '".$is_exist_leave['emp_id']."', `leavename` = 'PL', `value` = '".$bal_to_add."', `value_open` = '".$bal_to_add."' ";
							// echo $update_new_sql;
							// echo '<br />';
							query($update_new_sql, $conn);
						}
					}
					$insert_sql = "INSERT INTO `oc_leave_credit_transaction` SET `emp_code` = '".$evalue['emp_code']."', `name` = '".$evalue['name']."', `leave_value` = '".$bal_to_add."', `leave_name` = 'PL', `month` = '".$current_month."', `year` = '".$current_year."', `dot` = '".$today_date."', `company` = '".$evalue['company']."', `company_id` = '".$evalue['company_id']."', `region` = '".$evalue['region']."', `region_id` = '".$evalue['region_id']."', `division` = '".$evalue['division']."', `division_id` = '".$evalue['division_id']."', `department` = '".$evalue['department']."', `department_id` = '".$evalue['department_id']."', `unit` = '".$evalue['unit']."', `unit_id` = '".$evalue['unit_id']."', `present_count` = '".$present_count."', `leave_taken` = '".$leave_count."' ";
					query($insert_sql, $conn);
				} else {
					$is_exist_leaves = query("SELECT * FROM `oc_leave` WHERE `emp_id` = '".$evalue['emp_code']."' AND `close_status` = '0' ", $conn);
					if($is_exist_leaves->num_rows > 0){
						//$leave_credit_counts = query("SELECT * FROM `oc_leave_credit_transaction` WHERE `emp_code` = '".$evalue['emp_code']."' ", $conn);
						//if($leave_credit_counts->num_rows > 1){
							$is_exist_leave = $is_exist_leaves->row;
							$is_exist_pls = query("SELECT * FROM `oc_leave_employee` WHERE `leave_id` = '".$is_exist_leave['leave_id']."' AND `leavename` = 'PL' ", $conn);
							if($is_exist_pls->num_rows > 0){
								$update_sql = "UPDATE `oc_leave_employee` SET `value` = (`value` + ".$bal_to_add.") WHERE `id` = '".$is_exist_pls->row['id']."' ";
							} else {
								$update_sql = "INSERT INTO `oc_leave_employee` SET `leave_id` = '".$is_exist_leave['leave_id']."', `emp_id` = '".$is_exist_leave['emp_id']."', `leavename` = 'PL', `value` = '".$bal_to_add."' , `value_open` = '".$bal_to_add."' ";
							}
							// echo $update_sql;
							// echo '<br />';
							// echo '<br />';
							// exit;
							query($update_sql, $conn);
						//}
						
						$insert_sql = "INSERT INTO `oc_leave_credit_transaction` SET `emp_code` = '".$evalue['emp_code']."', `name` = '".$evalue['name']."', `leave_value` = '".$bal_to_add."', `leave_name` = 'PL', `month` = '".$current_month."', `year` = '".$current_year."', `dot` = '".$today_date."', `company` = '".$evalue['company']."', `company_id` = '".$evalue['company_id']."', `region` = '".$evalue['region']."', `region_id` = '".$evalue['region_id']."', `division` = '".$evalue['division']."', `division_id` = '".$evalue['division_id']."', `department` = '".$evalue['department']."', `department_id` = '".$evalue['department_id']."', `unit` = '".$evalue['unit']."', `unit_id` = '".$evalue['unit_id']."', `present_count` = '".$present_count."', `leave_taken` = '".$leave_count."' ";
						query($insert_sql, $conn);
						// echo $insert_sql;
						// echo '<br />';
						// echo '<br />';
						//exit;
					}
				}
				//echo 'out';exit;
			}
		}
	}
	//echo 'Done';
	//exit;
}
$conn->close();
echo 'Done';exit;
?>