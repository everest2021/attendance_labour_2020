<?php
//echo 'Maintaince Mode';exit;
$servername = "localhost";
$username = "root";
$password = "JmC@2018";
$dbname = "db_attendance_jmc_labour";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function escape($value, $conn) {
	return $conn->real_escape_string($value);
}
function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}


			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}

if (isset($_GET['emp_id'])) {
	$emp_id = $_GET['emp_id'];
} else {
	$emp_id = '';
}

if (isset($_GET['unit_id'])) {
	$unit_id = $_GET['unit_id'];
} else {
	$unit_id = '';
}

if (isset($_GET['start_date'])) {
	$start_date = date('Y-m-d', strtotime($_GET['start_date']));
} else {
	echo 'Please enter valid start date';
	exit;
}

if (isset($_GET['end_date'])) {
	$end_date = date('Y-m-d', strtotime($_GET['end_date']));
} else {
	echo 'Please enter valid end date';
	exit;
}

if ($start_date == '' || $end_date == '' || $start_date > $end_date) {
	echo 'Please enter valid start date and end date';
	exit;
}

if ($emp_id == '' && $unit_id == '') {
	echo 'Please enter employee id or unit id';
	exit;
}

$day = array();
$days = GetDays($start_date, $end_date);
foreach ($days as $dkey => $dvalue) {
	$dates = explode('-', $dvalue);
	$day[$dkey]['day'] = $dates[2];
	$day[$dkey]['date'] = $dvalue;
}
// echo '<pre>';
// print_r($day);
// exit;
$batch_id = '0';
// echo'<pre>';
// print_r($day);
// exit;
foreach($day as $dkeys => $dvalues){
	$filter_date_start = $dvalues['date'];
	$in = 0;
	$results = getemployees($dvalues['date'], $emp_id, $unit_id, $conn);
	if (empty($results)) {
		echo'PLEASE ENTER VALID DETAILS';
	} else {
		$in = 1;

		foreach ($results as $rkey => $rvalue) {
			if(isset($rvalue['name']) && $rvalue['name'] != ''){
				$emp_name = $rvalue['name'];
				$department = $rvalue['department'];
				$unit = $rvalue['unit'];
				$group = '';

				$day_date = date('j', strtotime($filter_date_start));
				$month = date('n', strtotime($filter_date_start));
				$year = date('Y', strtotime($filter_date_start));

				$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
				$shift_schedule = query($update3, $conn)->row;
				$schedule_raw = explode('_', $shift_schedule[$day_date]);
				if(!isset($schedule_raw[2])){
					$schedule_raw[2]= 1;
				}
				if($schedule_raw[0] == 'S'){
					$shift_data = getshiftdata($schedule_raw[1], $conn);
					if(isset($shift_data['shift_id'])){
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = $shift_data['out_time'];
						$shift_code = $shift_data['shift_code'];
						$shift_id = $shift_data['shift_id'];
						
						$day = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));

						$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
						$trans_exist = query($trans_exist_sql, $conn);
						if($trans_exist->num_rows == 0){
							$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
							//echo $sql.';';
							//echo '<br />';
							query($sql, $conn);

						} else {
							$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
							//echo $sql.';';
							//echo '<br />';
							query($sql, $conn);
						}
					} else {
						$shift_data = getshiftdata('1', $conn);
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = $shift_data['out_time'];
						$shift_code = $shift_data['shift_code'];
						$shift_id = $shift_data['shift_id'];
						
						$day = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));

						$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
						$trans_exist = query($trans_exist_sql, $conn);
						if($trans_exist->num_rows == 0){
							$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
							//echo $sql.';';
							//echo '<br />';
							query($sql, $conn);

						} else {
							$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
							//echo $sql.';';
							//echo '<br />';
							query($sql, $conn);

						}
					}
				} elseif ($schedule_raw[0] == 'W') {
					$shift_data = getshiftdata($schedule_raw[2], $conn);
					if(!isset($shift_data['shift_id'])){
						$shift_data = getshiftdata('1', $conn);
					}
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
					$shift_code = $shift_data['shift_code'];
					$shift_id = $shift_data['shift_id'];

					$act_intime = '00:00:00';
					$act_outtime = '00:00:00';
					$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = query($trans_exist_sql, $conn);
					
					if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
						//echo $sql.';';
						//echo '<br />';
						query($sql, $conn);

					} else {
						$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
						//echo $sql.';';
						//echo '<br />';
						query($sql, $conn);

					}
				} elseif ($schedule_raw[0] == 'H') {
					$shift_data = getshiftdata($schedule_raw[2], $conn);
					if(!isset($shift_data['shift_id'])){
						$shift_data = getshiftdata('1', $conn);
					}
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
					$shift_code = $shift_data['shift_code'];
					$shift_id = $shift_data['shift_id'];

					$act_intime = '00:00:00';
					$act_outtime = '00:00:00';
					
					$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = query($trans_exist_sql, $conn);
					if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
						//echo $sql.';';
						//echo '<br />';
						query($sql, $conn);

					} else {
						$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
						//echo $sql.';';
						//echo '<br />';
						query($sql, $conn);

					}
				} elseif ($schedule_raw[0] == 'HD') {
					$shift_data = getshiftdata($schedule_raw[2], $conn);
					if(!isset($shift_data['shift_id'])){
						$shift_data = getshiftdata('1', $conn);
					}
					$shift_intime = $shift_data['in_time'];
					if($shift_data['shift_id'] == '1'){
						if($rvalue['sat_status'] == '1'){
							$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
							$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
						} else {
							$shift_outtime = $shift_data['out_time'];
						}
					} else {
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = $shift_data['out_time'];
					}
					$shift_code = $shift_data['shift_code'];
					$shift_id = $shift_data['shift_id'];
					//$shift_intime = $shift_data['in_time'];
					//$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
					//$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
					//$shift_outtime = $shift_data['out_time'];

					$act_intime = '00:00:00';
					$act_outtime = '00:00:00';
					
					$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = query($trans_exist_sql, $conn);
					if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
						//echo $sql.';';
						//echo '<br />';
						query($sql, $conn);

					} else {
						$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
						//echo $sql.';';
						//echo '<br />';
						query($sql, $conn);
					}
				}
			}
		}
	}
}

if ($in == 1) {
	$sql = "UPDATE `oc_employee` SET `is_new` = '0' ";
	query($sql, $conn);
}

//$sql = "UPDATE `oc_transaction` SET `day_close_status` = '1' WHERE `date` = '".$filter_date_start."'";
//query($sql, $conn);
$conn->close();
echo 'Done';exit;

function GetDays($sStartDate, $sEndDate){
	// Firstly, format the provided dates.  
	// This function works best with YYYY-MM-DD  
	// but other date formats will work thanks  
	// to strtotime().  
	$sStartDate = date("Y-m-d", strtotime($sStartDate));  
	$sEndDate = date("Y-m-d", strtotime($sEndDate));  
	// Start the variable off with the start date  
	$aDays[] = $sStartDate;  
	// Set a 'temp' variable, sCurrentDate, with  
	// the start date - before beginning the loop  
	$sCurrentDate = $sStartDate;  
	// While the current date is less than the end date  
	while($sCurrentDate < $sEndDate){  
	// Add a day to the current date  
	$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
		// Add this new day to the aDays array  
	$aDays[] = $sCurrentDate;  
	}
	// Once the loop has finished, return the  
	// array of days.  
	return $aDays;  
}

function getemployees($punch_date, $emp_id, $unit_id, $conn) {
	$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name`, `company`, `company_id`, `contractor`, `contractor_id`, `contractor_code`, `category`, `category_id`, `designation`, `designation_id` FROM `oc_employee` WHERE (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$punch_date."') ";
	if (isset($emp_id) && $emp_id != '') {
		$sql .= "AND emp_code = '".$emp_id."' ";
	}

	if (isset($unit_id) && $unit_id != '') {
		$sql .= "AND unit_id = '".$unit_id."' ";
	}
	$sql .= " ORDER BY `shift_type` ";
	// echo $sql;	
	// echo '<br>';	
	$query = query($sql, $conn);
	return $query->rows;
}

function getshiftdata($shift_id, $conn) {
	$query = query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ", $conn);
	if($query->num_rows > 0){
		return $query->row;
	} else {
		return array();
	}
}



echo 'Done';exit;
?>