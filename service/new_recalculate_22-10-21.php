<?php
// error_reporting(E_ALL);
// ini_set("display_errors", 1);
//echo 'Maintaince Mode';exit;
$servername = "localhost";
$username = "root";
$password = "JmC@2018";
$dbname = "db_attendance_jmc_labour";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function escape($value, $conn) {
	return $conn->real_escape_string($value);
}

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}

function mssql_escape($data) {
    $data = stripslashes($data);
	$data = str_replace("'", "''", $data);
	return $data;
}



$task_status = "SELECT * FROM oc_task_status WHERE status = '1' ";
$task_datas = query($task_status, $conn);
if ($task_datas->num_rows > 0) {
	// echo "Already Running!";
	// exit;
}


$recalculate_sql = "SELECT * FROM oc_task WHERE status = '1' ORDER BY id ASC LIMIT 1 ";
$recalculate_datas = query($recalculate_sql, $conn);
if ($recalculate_datas->num_rows > 0 && $task_datas->num_rows == 0) {

	query("UPDATE oc_task_status SET status = 1 ", $conn);

	$recalculate_array = json_decode($recalculate_datas->row['data'], true);

	$filter_date_start = '';
	$filter_date_start_constant = '';
	$filter_date_end = '';
	$filter_date_end_constant = '';
	$filter_name = '';
	$filter_name_id = '';
	$filter_unit = '';
	$filter_department = '';
	$filter_division = '';
	$filter_region = '';
	$filter_company = '';
	$filter_contractor = '';
	$filter_email = '';

	if (isset($recalculate_array['filter_date_start'])) {
		$filter_date_start = $recalculate_array['filter_date_start'];
	}
	
	if (isset($recalculate_array['filter_date_start_constant'])) {
		$filter_date_start_constant = $recalculate_array['filter_date_start_constant'];
	}
	
	if (isset($recalculate_array['filter_date_end'])) {
		$filter_date_end = $recalculate_array['filter_date_end'];
	}
	
	if (isset($recalculate_array['filter_date_end_constant'])) {
		$filter_date_end_constant = $recalculate_array['filter_date_end_constant'];
	}
	
	if (isset($recalculate_array['filter_name'])) {
		$filter_name = $recalculate_array['filter_name'];
	}
	
	if (isset($recalculate_array['filter_name_id'])) {
		$filter_name_id = $recalculate_array['filter_name_id'];
	}
	
	if (isset($recalculate_array['filter_unit'])) {
		$filter_unit = $recalculate_array['filter_unit'];
	}
	
	if (isset($recalculate_array['filter_department'])) {
		$filter_department = $recalculate_array['filter_department'];
	}
	
	if (isset($recalculate_array['filter_division'])) {
		$filter_division = $recalculate_array['filter_division'];
	}
	
	if (isset($recalculate_array['filter_region'])) {
		$filter_region = $recalculate_array['filter_region'];
	}
	
	if (isset($recalculate_array['filter_company'])) {
		$filter_company = $recalculate_array['filter_company'];
	}
	
	if (isset($recalculate_array['filter_contractor'])) {
		$filter_contractor = $recalculate_array['filter_contractor'];
	}
	
	if (isset($recalculate_array['filter_email'])) {
		$filter_email = $recalculate_array['filter_email'];
	}

	//url_code(empty_shift and empty_process)
	$start_date = date("Y-m-d", strtotime($filter_date_start));
	$end_date = date("Y-m-d", strtotime($filter_date_end));
	$unit_id = $filter_unit;
	$emp_id = $filter_name_id;
	if ($filter_unit > 0 || $filter_name_id > 0) {
		$day = array();
		$days = GetDays($start_date, $end_date);
		foreach ($days as $dkey => $dvalue) {
			$dates = explode('-', $dvalue);
			$day[$dkey]['day'] = $dates[2];
			$day[$dkey]['date'] = $dvalue;
		}
		// echo '<pre>';
		// print_r($day);
		// exit;

		//shift empty

		$batch_id = '0';
		foreach($day as $dkeys => $dvalues){
			$filter_date_start = $dvalues['date'];
			//echo $filter_date_start;exit;
			$results = getemployees2($dvalues['date'], $emp_id, $unit_id, $conn);
			// echo '<pre>';
			// print_r($results);
			// exit;
			foreach ($results as $rkey => $rvalue) {
				//$emp_data = getempdata($rvalue['emp_code'], $conn);
				if(isset($rvalue['name']) && $rvalue['name'] != ''){
					$emp_name = $rvalue['name'];
					$department = $rvalue['department'];
					$unit = $rvalue['unit'];
					$group = '';

					$day_date = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));
					
					$update33 = query("SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ",$conn);
					if($update33->num_rows == 0){
						$insert1 = query("INSERT INTO `oc_shift_schedule` SET 
							`emp_code` = '".$rvalue['emp_code']."',
							`1` = 'S_1',
							`2` = 'S_1',
							`3` = 'S_1',
							`4` = 'S_1',
							`5` = 'S_1',
							`6` = 'S_1',
							`7` = 'S_1',
							`8` = 'S_1',
							`9` = 'S_1',
							`10` = 'S_1',
							`11` = 'S_1',
							`12` = 'S_1', 
							`13` = 'S_1', 
							`14` = 'S_1', 
							`15` = 'S_1', 
							`16` = 'S_1', 
							`17` = 'S_1', 
							`18` = 'S_1', 
							`19` = 'S_1', 
							`20` = 'S_1', 
							`21` = 'S_1', 
							`22` = 'S_1', 
							`23` = 'S_1', 
							`24` = 'S_1', 
							`25` = 'S_1', 
							`26` = 'S_1', 
							`27` = 'S_1', 
							`28` = 'S_1', 
							`29` = 'S_1', 
							`30` = 'S_1', 
							`31` = 'S_1', 
							`month` = '".$month."',
							`year` = '".$year."',
							`status` = '1' ,
							`unit` = '".$unit."' ",$conn);
						// echo "<pre>";
						// echo $insert1;
						$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
						$shift_schedule = query($update3, $conn)->row;
					}
				}	
			}
		}
		
		//shift empty

		//data process empty

		$batch_id = '0';
		foreach($day as $dkeys => $dvalues){
			$filter_date_start = $dvalues['date'];
			$in = 0;
			$results = getemployees2($dvalues['date'], $emp_id, $unit_id, $conn);
			if (empty($results)) {
				echo'PLEASE ENTER VALID DETAILS';
			} else {
				$in = 1;

				foreach ($results as $rkey => $rvalue) {
					if(isset($rvalue['name']) && $rvalue['name'] != ''){
						$emp_name = $rvalue['name'];
						$department = $rvalue['department'];
						$unit = $rvalue['unit'];
						$group = '';

						$day_date = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));

						$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
						$shift_schedule = query($update3, $conn)->row;
						$schedule_raw = explode('_', $shift_schedule[$day_date]);
						if(!isset($schedule_raw[2])){
							$schedule_raw[2]= 1;
						}
						if($schedule_raw[0] == 'S'){
							$shift_data = getshiftdata2($schedule_raw[1], $conn);
							if(isset($shift_data['shift_id'])){
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];
								$shift_code = $shift_data['shift_code'];
								$shift_id = $shift_data['shift_id'];
								
								$day = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));

								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = query($trans_exist_sql, $conn);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
									//echo $sql.';';
									//echo '<br />';
									query($sql, $conn);

								} else {
									$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
									//echo $sql.';';
									//echo '<br />';
									query($sql, $conn);
								}
							} else {
								$shift_data = getshiftdata2('1', $conn);
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];
								$shift_code = $shift_data['shift_code'];
								$shift_id = $shift_data['shift_id'];
								
								$day = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));

								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = query($trans_exist_sql, $conn);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
									//echo $sql.';';
									//echo '<br />';
									query($sql, $conn);

								} else {
									$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
									//echo $sql.';';
									//echo '<br />';
									query($sql, $conn);

								}
							}
						} elseif ($schedule_raw[0] == 'W') {
							$shift_data = getshiftdata2($schedule_raw[2], $conn);
							if(!isset($shift_data['shift_id'])){
								$shift_data = getshiftdata2('1', $conn);
							}
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
							$shift_code = $shift_data['shift_code'];
							$shift_id = $shift_data['shift_id'];

							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = query($trans_exist_sql, $conn);
							
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
								//echo $sql.';';
								//echo '<br />';
								query($sql, $conn);

							} else {
								$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
								//echo $sql.';';
								//echo '<br />';
								query($sql, $conn);

							}
						} elseif ($schedule_raw[0] == 'H') {
							$shift_data = getshiftdata2($schedule_raw[2], $conn);
							if(!isset($shift_data['shift_id'])){
								$shift_data = getshiftdata2('1', $conn);
							}
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
							$shift_code = $shift_data['shift_code'];
							$shift_id = $shift_data['shift_id'];

							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = query($trans_exist_sql, $conn);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
								//echo $sql.';';
								//echo '<br />';
								query($sql, $conn);

							} else {
								$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
								//echo $sql.';';
								//echo '<br />';
								query($sql, $conn);

							}
						} elseif ($schedule_raw[0] == 'HD') {
							$shift_data = getshiftdata2($schedule_raw[2], $conn);
							if(!isset($shift_data['shift_id'])){
								$shift_data = getshiftdata2('1', $conn);
							}
							$shift_intime = $shift_data['in_time'];
							if($shift_data['shift_id'] == '1'){
								if($rvalue['sat_status'] == '1'){
									$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
									$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
								} else {
									$shift_outtime = $shift_data['out_time'];
								}
							} else {
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];
							}
							$shift_code = $shift_data['shift_code'];
							$shift_id = $shift_data['shift_id'];
							//$shift_intime = $shift_data['in_time'];
							//$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
							//$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
							//$shift_outtime = $shift_data['out_time'];

							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = query($trans_exist_sql, $conn);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' ";
								//echo $sql.';';
								//echo '<br />';
								query($sql, $conn);

							} else {
								$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($emp_name, $conn)."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."' ";
								//echo $sql.';';
								//echo '<br />';
								query($sql, $conn);
							}
						}
					}
				}
			}
		}

		if ($in == 1) {
			$sql = "UPDATE `oc_employee` SET `is_new` = '0' ";
			query($sql, $conn);
		}

		//data process empty
	}

	//url_code(empty_shift and empty_process)

	//new_code

	$serverName = "10.200.1.35";
	$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"avi", "PWD"=>"avi2123");
	$conn1 = sqlsrv_connect($serverName, $connectionInfo);
	if($conn1) {
		//echo "Connection established.<br />";exit;
	} else {
		// echo "Connection could not be established.<br />";
		// echo "<br />";
		$dat['connection'] = 'Connection could not be established';
		// echo '<pre>';
		// print_r(sqlsrv_errors());
		// exit;
		//die( print_r( sqlsrv_errors(), true));
	}

	$sql = "SELECT * FROM etimetracklite1.dbo.Staffdevicelogsnew WHERE CAST(LogDate as DATE) >= '".$filter_date_start."' AND CAST(LogDate as DATE) <= '".$filter_date_end."' ";
	if($filter_name_id){
		$sql .= " AND UserId = '".$filter_name_id."' ";
	}
	$sql .= " ORDER BY DeviceLogId ";
	$params = array();
	$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$stmt = sqlsrv_query($conn1, $sql, $params, $options);
	if( $stmt === false) {
	   //echo'<pre>';
	   //print_r(sqlsrv_errors());
	   //exit;
	}
	$exp_datas = array();
	while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
		$user_id = $row['UserId'];
		$device_id_check = $row['DeviceId'];
		if($device_id_check != '' || $device_id_check != null){
			$device_id = $row['DeviceId'];
		} else {
			$device_id = 0;
		}
		$log_id = $row['DeviceLogId'];
		$logdates = $row['LogDate'];

		if($logdates != null && strlen($user_id) <= 12){	
			$logdate = $logdates->format("Y-m-d");
			$logyear = $logdates->format("Y");
			$logtimes = $row['LogDate'];
			$logtime = $logtimes->format("H:i:s");
			if($logyear >= '2018' && $logtime != '00:00:00'){
				$download_dates = $row['DownloadDate'];
				if($download_dates != '' || $download_dates != null){
					$download_date = $download_dates->format("Y-m-d");
				} else {
					$download_date = '0000-00-00';
				}
				$exp_datas[] = array(
					'user_id' => $user_id,
					'log_id' => $log_id,
					'download_date' => $download_date,
					'date' => $logdate,
					'time' => $logtime,
					'device_id' => $device_id,
				);
			}
		}
	}

	$getEmployees_dat_array = array();

	$getEmployees_dat_sql = "SELECT * FROM `oc_employee` WHERE 1=1 ";
	$getEmployees_dat_datas = query($getEmployees_dat_sql, $conn)->rows;
	foreach ($getEmployees_dat_datas as $gedkey => $gedvalue) {
		$getEmployees_dat_array[$gedvalue['emp_code']] = $gedvalue;
	}

	$employees = array();
	$employees_final = array();
	foreach($exp_datas as $data) {
		$emp_id  = $data['user_id'];
		$in_time = $data['time'];
		if($emp_id != '') {
			if (isset($getEmployees_dat_array[$emp_id])) {
				$employees[] = array(
					'employee_id' => $getEmployees_dat_array[$emp_id]['emp_code'],
					'emp_name' => $getEmployees_dat_array[$emp_id]['name'],
					'card_id' => $getEmployees_dat_array[$emp_id]['emp_code'],
					'department' => $getEmployees_dat_array[$emp_id]['department'],
					'unit' => $getEmployees_dat_array[$emp_id]['unit'],
					'unit_id' => $getEmployees_dat_array[$emp_id]['unit_id'],
					'group' => '',
					'in_time' => $in_time,
					'device_id' => $data['device_id'],
					'punch_date' => $data['date'],
					'download_date' => $data['download_date'],
					'log_id' => $data['log_id'],
					'fdate' => date('Y-m-d H:i:s', strtotime($data['date'].' '.$in_time))
				);
			} else {
				
				$employees[] = array(
					'employee_id' => $emp_id,
					'emp_name' => '',
					'card_id' => $emp_id,
					'department' => '',
					'unit' => '',
					'unit_id' => 0,
					'group' => '',
					'in_time' => $in_time,
					'device_id' => $data['device_id'],
					'punch_date' => $data['date'],
					'download_date' => $data['download_date'],
					'log_id' => $data['log_id'],
					'fdate' => date('Y-m-d H:i:s', strtotime($data['date'].' '.$in_time))
				);
				
			}
		}
	}
	usort($employees, "sortByOrder");
	$o_emp = array();
	foreach ($employees as $ekey => $evalue) {
		$employees_final[] = $evalue;
	}

	if(isset($employees_final) && count($employees_final) > 0){
		foreach ($employees_final as $fkey => $employee) {
			$exist = getorderhistory($employee, $conn);
			if($exist == 0){
				$mystring = $employee['in_time'];
				$findme   = ':';
				$pos = strpos($mystring, $findme);
				if($pos !== false){
					$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 3, 2). ":" . substr($employee['in_time'], 6, 2);
				} else {
					$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 2, 2). ":" . substr($employee['in_time'], 4, 2);
				}
				$employee['in_time'] = $in_times;
				insert_attendance_data($employee, $conn);
			}
		}
	}

	$stats_sql = "SELECT * FROM `oc_status` WHERE 1=1 ";
	if(!empty($filter_unit)){
		$stats_sql .= " AND `unit_id` = '".$filter_unit."' ";
	}
	if(!empty($filter_division)){
		$stats_sql .= " AND `division_id` = '".$filter_division."' ";
	}
	$stats_data = query($stats_sql, $conn);
	if($stats_data->num_rows == 0){
		$sql = "INSERT INTO `oc_status` SET `process_status` = '1' ";
		if(!empty($filter_unit)){
			$sql .= " , `unit_id` = '".$filter_unit."' ";
		} else {
			$sql .= " , `unit_id` = '0' ";
		}
		if(!empty($filter_division)){
			$sql .= " , `division_id` = '".$filter_division."' ";
		} else {
			$sql .= " , `division_id` = '0' ";
		}
		query($sql, $conn);
		$employee_datas_sql = "SELECT * FROM `oc_employee` WHERE 1=1 AND (`dol` = '0000-00-00' OR `dol` > '".$filter_date_start."')";
		if (!empty($filter_name_id)) {
			$employee_datas_sql .= " AND `emp_code` = '" . escape(strtolower($filter_name_id), $conn) . "'";
		}

		if (!empty($filter_company)) {
			$company_string = str_replace("multiselect-all,", "", html_entity_decode($filter_company));
			$company_string = "'" . str_replace(",", "','", $company_string) . "'";
			$employee_datas_sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
		}

		if (!empty($filter_contractor)) {
			$contractor_string = str_replace("multiselect-all,", "", html_entity_decode($filter_contractor));
			$contractor_string = "'" . str_replace(",", "','", $contractor_string) . "'";
			$employee_datas_sql .= " AND `contractor_id` IN (" . strtolower($contractor_string) . ") ";
		}

		if (!empty($filter_region)) {
			//$employee_datas_sql .= " AND `region_id` = '" . escape(strtolower($filter_region), $conn) . "'";
		}

		if (!empty($filter_division)) {
			$employee_datas_sql .= " AND `division_id` = '" . escape(strtolower($filter_division), $conn) . "'";
		}

		if (!empty($filter_department)) {
			$employee_datas_sql .= " AND `department_id` = '" . escape(strtolower($filter_department), $conn) . "'";
		}

		if (!empty($filter_unit)) {
			$employee_datas_sql .= " AND `unit_id` = '" . escape(strtolower($filter_unit), $conn) . "'";
		}

		$employee_datas_sql .= " ORDER BY `emp_code`";
		$employee_datas = query($employee_datas_sql, $conn);
		
		if($employee_datas->num_rows > 0){

			//function getempdata

			$getempdatasql = "SELECT * FROM `oc_employee` WHERE 1=1 ";
			$getempdata_datas = query($getempdatasql, $conn)->rows;
			$getempdata_array = array();
			foreach ($getempdata_datas as $gekey => $gevalue) {
				$getempdata_array[$gevalue['emp_code']] = $gevalue;
			}

			//function getempdata
			$days = array();
			$dayss = GetDays($filter_date_start_constant, $filter_date_end_constant);
			foreach ($dayss as $dkey => $dvalue) {
				if(strtotime($dvalue) < strtotime(date('Y-m-d'))){
					$dates = explode('-', $dvalue);
					$days[$dkey]['day'] = ltrim($dates[2], '0');
					$days[$dkey]['month'] = $dates[1];
					$days[$dkey]['year'] = $dates[0];
					$days[$dkey]['date'] = $dvalue;
					$exceed = 0;
				} else {
					$exceed = 1;
				}
			}
					
			if (!empty($filter_name_id)) {
				// $this->log->write('Recalculate Start For Emp Code ' . $filter_name_id . ' And Date ' . $filter_date_start_constant . ' - ' . $filter_date_end_constant);
			} else {
				// $this->log->write('Recalculate Start For Date ' . $filter_date_start_constant . ' - ' . $filter_date_end_constant);
			}
			$cnt1 = 0;
			$cnt = 0;
			$date_string = '';
			$employee_string = '';
			$punch_date_array = array();
			foreach($employee_datas->rows as $rkey => $rvalue){
				$employee_string .= $rvalue['emp_code'].', ';
			}
			$employee_string = rtrim($employee_string, ', ');
			$employee_string=  "'" . str_replace(",", "','", $employee_string) . "'";
			$date_string = rtrim($date_string, ', ');
			$date_string = "'" . str_replace(",", "','", $date_string) . "'";
			$reset_attendance_sql = "UPDATE `oc_attendance` SET `status` = '0', `transaction_id` = '0' WHERE `emp_id` IN (" . $employee_string . ") AND ((`punch_date` >= '".$filter_date_start_constant."' AND `punch_time` >= '05:00:00') OR (`punch_date` <= '".$filter_date_end_constant."' AND `punch_time` <= '04:00:00'))";
			if($filter_unit){
				$reset_attendance_sql .= " AND `unit_id` = '".$filter_unit."' ";
			}
			// $this->log->write("update_status_zero");
			// $this->log->write($reset_attendance_sql);
			// $this->log->write("update_status_zero");
			query($reset_attendance_sql, $conn);

			$reset_transaction = "UPDATE `oc_transaction` SET `act_intime` = '00:00:00', `act_outtime` = '00:00:00', `date_out` = '0000-00-00', `late_time` = '00:00:00', `early_time` = '00:00:00', `late_mark` = '0', `early_mark` = '0', `working_time` = '00:00:00', `leave_status` = '0', `absent_status` = '1', `present_status` = '0', `abnormal_status` = '0', `batch_id` = '0', `weekly_off` = '0', `holiday_id` = '0', `halfday_status` = '0', `day_close_status` = '0', `firsthalf_status` = '0', `secondhalf_status` = '0' WHERE `date` >= '".$filter_date_start_constant."' AND `date` <= '".$filter_date_end_constant."' AND `emp_id` IN (" . strtolower($employee_string) . ") AND (`manual_status` = '0')";
			if($filter_unit){
				$reset_transaction .= " AND `unit_id` = '".$filter_unit."' ";
			}

			query($reset_transaction, $conn);

			$batch_id = 0;
			$current_processed_data = array();
			$current_processed_data_date = array();
			foreach($employee_datas->rows as $rkey => $rvalue){
				$current_processed_data_date = array();
				$emp_code = $rvalue['emp_code'];
				$unprocessed_dates = query("SELECT  a.`id`, a.`emp_id`, a.`punch_date`, a.`punch_time`, a.`device_id` FROM `oc_attendance` a WHERE a.`status` = '0' AND a.`punch_date` >= '".$filter_date_start_constant."' AND  a.`punch_date` <= '".$filter_date_end_constant."' AND a.`emp_id` = '".$rvalue['emp_code']."' ORDER BY a.`punch_date` ASC, a.`punch_time` ASC ", $conn);

				// $this->log->write("start_here");
				// $this->log->write("SELECT  a.`id`, a.`emp_id`, a.`punch_date`, a.`punch_time`, a.`device_id` FROM `oc_attendance` a AND a.`status` = '0' AND a.`punch_date` >= '".$filter_date_start_constant."' AND  a.`punch_date` <= '".$filter_date_end_constant."' AND a.`emp_id` = '".$rvalue['emp_code']."' ORDER BY a.`punch_date` ASC, a.`punch_time` ASC");
				foreach($unprocessed_dates->rows as $dkey => $dvalue){
					$filter_date_start = $dvalue['punch_date'];
					$current_processed_data[$dvalue['id']] = $dvalue['id'];
					$current_processed_data_date[$dvalue['id']] = $filter_date_start;
					$data['filter_name_id'] = $dvalue['emp_id'];
					$device_id = $dvalue['device_id'];
					$is_processed_status = query("SELECT `status` FROM `oc_attendance` WHERE `id` = '".$dvalue['id']."' ", $conn)->row['status'];
					if (isset($getempdata_array[$dvalue['emp_id']]) && ($getempdata_array[$dvalue['emp_id']]['dol'] == '0000-00-00' || $getempdata_array[$dvalue['emp_id']]['dol'] > $dvalue['punch_date']) && $is_processed_status == '0') {

						$emp_name = $rvalue['name'];
						$department = $rvalue['department'];
						$unit = $rvalue['unit'];
						$group = '';

						$day_date = date('j', strtotime($filter_date_start));
						$day = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));

						$punch_time_exp = explode(':', $dvalue['punch_time']);
						if($punch_time_exp[0] >= '00' && $punch_time_exp[0] <= '04'){
							$filter_date_start = date('Y-m-d', strtotime($filter_date_start .' -1 day'));
							$day_date = date('j', strtotime($filter_date_start));
							$day = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));				
						}

						$shift_schedule[$day_date] = 'S_1';	
						$schedule_raw = explode('_', $shift_schedule[$day_date]);
						if(!isset($schedule_raw[2])){
							$schedule_raw[2] = 1;
						}

						$transaction_id = 0;
						if($schedule_raw[0] == 'S'){
							$shift_data = getshiftdata($schedule_raw[1]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = getshiftdata('1');
							}
							
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
							$shift_code = $shift_data['shift_code'];
							$shift_id = $shift_data['shift_id'];

							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							$act_in_punch_date = $filter_date_start;
							$act_out_punch_date = $filter_date_start;
							
							$trans_exist_sql = "SELECT `transaction_id`, `act_intime`, `date`, `manual_status`, `leave_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = query($trans_exist_sql, $conn);

							if($trans_exist->num_rows == 0){
								$act_intimes = getrawattendance_in_time($rvalue['emp_code'], $filter_date_start, $conn);
								if(isset($act_intimes['punch_time'])) {
									$act_intime = $act_intimes['punch_time'];
									$act_in_punch_date = $act_intimes['punch_date'];
								}
							} else {
								$act_intimes = getrawattendance_in_time($rvalue['emp_code'], $filter_date_start, $conn);
								if(isset($act_intimes['punch_time'])) {
									$act_intime = $act_intimes['punch_time'];
									$act_in_punch_date = $act_intimes['punch_date'];
								}
							}

							$act_outtimes = getrawattendance_out_time($rvalue['emp_code'], $filter_date_start, $act_intime, $act_in_punch_date, $conn);
							if(isset($act_outtimes['punch_time'])) {
								$act_outtime = $act_outtimes['punch_time'];
								$act_out_punch_date = $act_outtimes['punch_date'];
							}

							if($act_intime == $act_outtime){
								$act_outtime = '00:00:00';
							}

							$abnormal_status = 0;
							$first_half = 0;
							$second_half = 0;
							$late_time = '00:00:00';
							$early_time = '00:00:00';
							$working_time = '00:00:00';
							$late_mark = 0;
							$early_mark = 0;
							$shift_early = 0;
							if($act_intime != '00:00:00'){
								$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
								if($since_start->h > 12){
									$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
								}

								if($since_start->invert == 1){
									$shift_early = 1;
								}
								
								if($act_outtime != '00:00:00'){
									if($shift_early == 1){
										if(strtotime($act_outtime) < strtotime($shift_intime)){
											$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
										} else {
											$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
										}
									} else {
										$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
									}
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									$first_half = 1;
									$second_half = 1;
									$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								} else {
									$first_half = 1;
									$second_half = 1;
								}
							} else {
								$first_half = 0;
								$second_half = 0;
							}

							if($first_half == 1 && $second_half == 1){
								$present_status = 1;
								$absent_status = 0;
							} else {
								$present_status = 0;
								$absent_status = 1;
							}

							$day = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));
							if($trans_exist->num_rows > 0){
								$transaction_id = $trans_exist->row['transaction_id'];
								$leave_status = $trans_exist->row['leave_status'];
								$manual_status = $trans_exist->row['manual_status'];
								if($leave_status == 0 && $manual_status == 0){
									// $this->log->write($rvalue['emp_code']);
									// $this->log->write('UPDATES');
									$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$filter_date_start."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
									query($sql, $conn);
								} else {
									// $this->log->write($rvalue['emp_code']);
									// $this->log->write('NOT UPDATES');
									$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
									query($sql, $conn);
								}
							}
						}

						if($act_in_punch_date == $act_out_punch_date) {
							if(($act_intime != '00:00:00') && ($act_outtime != '00:00:00')){
								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
								query($update, $conn);
							} elseif(($act_intime != '00:00:00') && ($act_outtime == '00:00:00')) {
								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` = '".$act_intime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
								query($update, $conn);
							} elseif(($act_intime == '00:00:00') && ($act_outtime != '00:00:00')) {
								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` = '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
								query($update, $conn);
							}
						} else {
							$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
							query($update, $conn);
							
							$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
							query($update, $conn);
						}
					}
				}
				foreach($days as $dkey => $dvalue){
					if(!in_array($dvalue['date'], $current_processed_data_date)){
						$filter_date_start = $dvalue['date'];
						
						$day_date = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));

						$shift_schedule[$day_date] = 'S_1';
						$schedule_raw = explode('_', $shift_schedule[$day_date]);
						if(!isset($schedule_raw[2])){
							$schedule_raw[2]= 1;
						}
						
						if($schedule_raw[0] == 'S'){
							$shift_data = getshiftdata($schedule_raw[1]);
							if(isset($shift_data['shift_id'])){
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];
								$shift_code = $shift_data['shift_code'];
								$shift_id = $shift_data['shift_id'];
								$day = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));
								$trans_exist_sql = "SELECT `transaction_id`, `leave_status`, `manual_status` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = query($trans_exist_sql, $conn);
								if($trans_exist->num_rows > 0){
									$transaction_id = $trans_exist->row['transaction_id'];
									$leave_status = $trans_exist->row['leave_status'];
									$manual_status = $trans_exist->row['manual_status'];
									if($leave_status == 0 && $manual_status == 0){
										$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".escape($rvalue['name'], $conn)."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
										query($sql, $conn);
									} else {
										$sql = "UPDATE `oc_transaction` SET division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."', contractor = '".$rvalue['contractor']."', contractor_code = '".$rvalue['contractor_code']."', contractor_id = '".$rvalue['contractor_id']."', category = '".$rvalue['category']."', category_id = '".$rvalue['category_id']."', designation = '".$rvalue['designation']."', designation_id = '".$rvalue['designation_id']."' WHERE `transaction_id` = '".$transaction_id."' ";
										query($sql, $conn);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	$sql = "DELETE FROM `oc_status` WHERE 1=1 ";
	if(!empty($filter_unit)){
		$sql .= " AND `unit_id` = '".$filter_unit."' ";
	}
	if(!empty($filter_division)){
		$sql .= " AND `division_id` = '".$filter_division."' ";
	}
	query($sql, $conn);
	$sql = "UPDATE oc_task_status SET status = '0' ";
	$sql1 = "UPDATE oc_task SET status = '0' WHERE id = '".$recalculate_datas->row['id']."' ";
	query($sql, $conn);
	query($sql1, $conn);
	$conn->close();



	date_default_timezone_set("Asia/Kolkata");
	$from_email = 'jmcautomail76@gmail.com';
	$message = 'Data Recalculate Process In Labour Done on ' . date('d-m-Y h:i:s A');
	$subject = 'Data Recalculate Process In Labour Done ' . date('d-m-Y h:i:s A');
	// $to_emails_array = array('chandan755@gmail.com');
	$to_emails_array = array($filter_email);
	// $to_emails_array = array('gauravp016@gmail.com');
	// $to_emails_array = array('fargose.aaron@gmail.com', 'eric.fargose@gmail.com', 'chandan755@gmail.com', 'chandanyu6008@gmail.com');
	require_once('PHPMailer/PHPMailerAutoload.php');
	require_once('PHPMailer/class.phpmailer.php');
	require_once('PHPMailer/class.smtp.php');
	$mail = new PHPMailer();
	//$mail->SMTPDebug = 3;
	$mail->IsSMTP();
	$mail->SMTPAuth = true;
	$mail->Helo = "HELO";
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = '587';
	$mail->Username = 'jmcautomail76@gmail.com';
	$mail->Password = 'jmc@2019';
	$mail->SMTPSecure = 'tls';
	$mail->SetFrom($from_email, 'Auto Recalculate Data Process');
	$mail->Subject = $subject;
	//$mail->isHTML(true);
	//$mail->MsgHTML($invoice_mail_text);
	$mail->Body = html_entity_decode($message);
	foreach($to_emails_array as $ekey => $evalue){
		$mail->AddAddress($evalue);
	}
	if($mail->Send()) {
		echo 'Mail Sent';
	 	echo '<br />';
	 	exit;
	  	echo '<pre>';
	  	print_r($mail->ErrorInfo);
	} else {
		echo "Mailer Error: " . $mail->ErrorInfo;
		echo '<pre>';
		print_r($mail->ErrorInfo);
		exit;
	}

	//new_code

}


function sortByOrder($a, $b) {
	$v1 = strtotime($a['fdate']);
	$v2 = strtotime($b['fdate']);
	return $v1 - $v2; // $v2 - $v1 to reverse direction
	// if ($a['punch_date'] == $b['punch_date']) {
		//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
	//}
	//return $a['punch_date'] - $b['punch_date'];
}

function GetDays($sStartDate, $sEndDate){  
	// Firstly, format the provided dates.  
	// This function works best with YYYY-MM-DD  
	// but other date formats will work thanks  
	// to strtotime().  
	$sStartDate = date("Y-m-d", strtotime($sStartDate));  
	$sEndDate = date("Y-m-d", strtotime($sEndDate));  
	// Start the variable off with the start date  
	$aDays[] = $sStartDate;  
	// Set a 'temp' variable, sCurrentDate, with  
	// the start date - before beginning the loop  
	$sCurrentDate = $sStartDate;  
	// While the current date is less than the end date  
	while($sCurrentDate < $sEndDate){  
	// Add a day to the current date  
	$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
		// Add this new day to the aDays array  
	$aDays[] = $sCurrentDate;  
	}
	// Once the loop has finished, return the  
	// array of days.  
	return $aDays;
}

function array_sort($array, $on, $order=SORT_ASC){

	$new_array = array();
	$sortable_array = array();

	if (count($array) > 0) {
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				foreach ($v as $k2 => $v2) {
					if ($k2 == $on) {
						$sortable_array[$k] = $v2;
					}
				}
			} else {
				$sortable_array[$k] = $v;
			}
		}

		switch ($order) {
			case SORT_ASC:
				asort($sortable_array);
				break;
			case SORT_DESC:
				arsort($sortable_array);
				break;
		}

		foreach ($sortable_array as $k => $v) {
			$new_array[$k] = $array[$k];
		}
	}

	return $new_array;
}

function getorderhistory($data, $conn){

	$sql = "SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$data['punch_date']."' AND `punch_time` = '".$data['in_time']."' AND `emp_id` = '".$data['employee_id']."' ";
	$query = query($sql, $conn);
	if($query->num_rows > 0){
		return 1;
	} else {
		return 0;
	}
}
function getEmployees_dat($emp_code, $conn) {
	$sql = "SELECT * FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
	$query = query($sql, $conn);
	return $query->row;
}
function getempdata($emp_code, $conn) {
	$sql = "SELECT * FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
	$query = query($sql, $conn);
	return $query->row;
}
function insert_attendance_data($data, $conn) {
	$sql = query("INSERT INTO `oc_attendance` SET `emp_id` = '".$data['employee_id']."', `card_id` = '".$data['card_id']."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `device_id` = '".$data['device_id']."', `status` = '0', `download_date` = '".$data['download_date']."', `new_log_id` = '".$data['log_id']."', `unit_id` = '".$data['unit_id']."' ", $conn);
}
function getemployees($punch_date, $conn) {
	$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name`, `company`, `company_id`, `contractor`, `contractor_id`, `contractor_code`, `category`, `category_id`, `designation`, `designation_id` FROM `oc_employee` WHERE (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$punch_date."') ";
	$sql .= " ORDER BY `shift_type` ";	
	//echo $sql;	
	$query = query($sql, $conn);
	return $query->rows;
}
function getemployees2($punch_date, $emp_id, $unit_id, $conn) {
	// echo $unit_id;
	// exit;
	$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name`, `company`, `company_id`, `contractor`, `contractor_id`, `contractor_code`, `category`, `category_id`, `designation`, `designation_id` FROM `oc_employee` WHERE (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$punch_date."') ";

	if ($unit_id != '') {
		$sql .= " AND unit_id = '".$unit_id."'";
	}

	if ($emp_id != '') {
		$sql .= " AND emp_id = '".$emp_id."'";
	}

	$sql .= " ORDER BY `shift_type` ";	
	//echo $sql;	
	$query = query($sql, $conn);
	return $query->rows;
}
function getrawattendance_group_date_custom($emp_code, $date, $conn) {
	$query = query("SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$date."' AND `emp_id` = '".$emp_code."' GROUP by `punch_date` ", $conn);
	return $query->row;
}
function getshiftdata2($shift_id, $conn) {
	$query = query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ", $conn);
	if($query->num_rows > 0){
		return $query->row;
	} else {
		return array();
	}
}
function getshiftdata($shift_id) {
	$shift_data = array(
		'shift_id' => 1,
		'name' => '08:00:00 - 20:00:00',
		'in_time' => '08:00:00',
		'out_time' => '20:00:00',
		'shift_code' => 'GS'
	);
	return $shift_data;
	// $query = $this->db->query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ");
	// if($query->num_rows > 0){
	// 	return $query->row;
	// } else {
	// 	return array();
	// }
}

function getrawattendance_in_time($emp_id, $punch_date, $conn) {
	$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
	$query = query("SELECT * FROM oc_attendance WHERE REPLACE(emp_id, ' ','') = '".$emp_id."' AND ((`punch_date` = '".$punch_date."' AND `punch_time` >= '05:00:00') OR (`punch_date` = '".$future_date."' AND `punch_time` <= '04:00:00')) ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ", $conn);
	
	// $this->log->write("SELECT * FROM oc_attendance WHERE REPLACE(emp_id, ' ','') = '".$emp_id."' AND ((`punch_date` = '".$punch_date."' AND `punch_time` >= '05:00:00') OR (`punch_date` = '".$future_date."' AND `punch_time` <= '04:00:00')) ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ");

	$array = $query->row;
	return $array;
}

function getrawattendance_out_time($emp_id, $punch_date, $act_intime, $act_punch_date, $conn) {
	$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
	$query = query("SELECT * FROM oc_attendance WHERE REPLACE(emp_id, ' ','') = '".$emp_id."' AND ((`punch_date` = '".$punch_date."' AND `punch_time` >= '05:00:00') OR (`punch_date` = '".$future_date."' AND `punch_time` <= '04:00:00')) ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC", $conn);
	
	// $this->log->write("SELECT * FROM oc_attendance WHERE REPLACE(emp_id, ' ','') = '".$emp_id."' AND ((`punch_date` = '".$punch_date."' AND `punch_time` >= '05:00:00') OR (`punch_date` = '".$future_date."' AND `punch_time` <= '04:00:00')) ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC");

	$act_outtime = array();
	if($query->num_rows > 0){
		//echo '<pre>';
		//print_r($act_intime);

		$first_punch = $query->rows['0'];
		$array = $query->rows;
		$comp_array = array();
		$hour_array = array();
		
		//echo '<pre>';
		//print_r($array);

		//echo '<pre>';
		//print_r($array);

		foreach ($array as $akey => $avalue) {
			$start_date = new DateTime($act_punch_date.' '.$act_intime);
			$since_start = $start_date->diff(new DateTime($avalue['punch_date'].' '.$avalue['punch_time']));
			
			//echo '<pre>';
			//print_r($since_start);

			if($since_start->d == 0){
				$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
				$hour_array[] = $since_start->h;
				$act_array[] = $avalue;
			}
		}

		// echo '<pre>';
		// print_r($hour_array);

		// echo '<pre>';
		// print_r($comp_array);

		foreach ($hour_array as $ckey => $cvalue) {
			/*if($cvalue > 20){
				unset($hour_array[$ckey]);
			}*/

			if($cvalue > 23){
				unset($hour_array[$ckey]);
			}
		}

		// echo '<pre>';
		// print_r($hour_array);
		
		$act_outtimes = '0';
		if($hour_array){
			$act_outtimes = max($hour_array);
		}

		// echo '<pre>';
		// print_r($act_outtimes);

		foreach ($hour_array as $akey => $avalue) {
			if($avalue == $act_outtimes){
				$act_outtime = $act_array[$akey];
			}
		}
	}
	// echo '<pre>';
	// print_r($act_outtime);
	// exit;		
	return $act_outtime;
}
echo 'Done';exit;
?>