-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2021 at 06:52 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_attendance_jmc_labour`
--

-- --------------------------------------------------------

--
-- Table structure for table `oc_address`
--

CREATE TABLE `oc_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(32) NOT NULL,
  `company_id` varchar(32) NOT NULL,
  `tax_id` varchar(32) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_affiliate`
--

CREATE TABLE `oc_affiliate` (
  `affiliate_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `company` varchar(32) NOT NULL,
  `website` varchar(255) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_affiliate_transaction`
--

CREATE TABLE `oc_affiliate_transaction` (
  `affiliate_transaction_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attendance`
--

CREATE TABLE `oc_attendance` (
  `id` int(11) NOT NULL,
  `emp_id` varchar(255) NOT NULL,
  `card_id` int(11) NOT NULL,
  `punch_date` date NOT NULL,
  `punch_time` time NOT NULL,
  `terminal_addr` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `log_id` int(11) NOT NULL,
  `download_date` date NOT NULL,
  `new_log_id` int(11) NOT NULL,
  `is_new_employee` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_attendance`
--

INSERT INTO `oc_attendance` (`id`, `emp_id`, `card_id`, `punch_date`, `punch_time`, `terminal_addr`, `status`, `transaction_id`, `device_id`, `log_id`, `download_date`, `new_log_id`, `is_new_employee`) VALUES
(1, '235666539189', 1000, '2021-11-12', '09:18:00', 0, 1, 12, 1, 1, '2020-02-01', 0, 0),
(2, '235666539189', 1000, '2021-11-12', '18:00:00', 0, 1, 12, 2, 2, '2020-02-01', 0, 0),
(3, '272424159714', 1001, '2021-11-10', '09:22:00', 0, 1, 23, 1, 3, '2020-01-01', 0, 0),
(12, '1001', 1001, '2020-01-23', '19:34:00', 0, 0, 0, 2, 12, '2020-02-22', 0, 0),
(13, '1002', 1002, '2020-02-22', '09:42:00', 0, 0, 0, 2, 13, '2020-02-22', 0, 0),
(14, '1002', 1002, '2020-02-22', '18:12:00', 0, 0, 0, 2, 14, '2020-02-22', 0, 0),
(15, '1001', 1001, '2020-02-22', '09:12:00', 0, 0, 0, 1, 13, '2020-02-22', 0, 0),
(16, '1001', 1001, '2020-02-22', '19:23:00', 0, 0, 0, 2, 14, '2020-02-22', 0, 0),
(17, '1002', 1002, '2020-02-22', '09:45:00', 0, 0, 0, 2, 15, '2020-02-22', 0, 0),
(18, '1002', 1002, '2020-02-22', '18:56:00', 0, 0, 0, 2, 16, '2020-02-22', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_attendance_2020_1`
--

CREATE TABLE `oc_attendance_2020_1` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `punch_date` date NOT NULL,
  `punch_time` time NOT NULL,
  `terminal_addr` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `log_id` int(11) NOT NULL,
  `new_log_id` int(11) NOT NULL,
  `download_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_attendance_2020_1`
--

INSERT INTO `oc_attendance_2020_1` (`id`, `emp_id`, `card_id`, `punch_date`, `punch_time`, `terminal_addr`, `status`, `transaction_id`, `device_id`, `log_id`, `new_log_id`, `download_date`) VALUES
(1, 1001, 1001, '2020-01-30', '09:22:00', 0, 0, 0, 1, 3, 0, '2020-01-30'),
(2, 1001, 1001, '2020-01-30', '19:40:00', 0, 0, 0, 2, 4, 0, '2020-01-30'),
(3, 1002, 1002, '2020-01-30', '09:30:00', 0, 0, 0, 2, 5, 0, '2020-01-30'),
(4, 1002, 1002, '2020-01-30', '18:33:00', 0, 0, 0, 2, 6, 0, '2020-01-30');

-- --------------------------------------------------------

--
-- Table structure for table `oc_attendance_2020_2`
--

CREATE TABLE `oc_attendance_2020_2` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `punch_date` date NOT NULL,
  `punch_time` time NOT NULL,
  `terminal_addr` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `log_id` int(11) NOT NULL,
  `new_log_id` int(11) NOT NULL,
  `download_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_attendance_2020_2`
--

INSERT INTO `oc_attendance_2020_2` (`id`, `emp_id`, `card_id`, `punch_date`, `punch_time`, `terminal_addr`, `status`, `transaction_id`, `device_id`, `log_id`, `new_log_id`, `download_date`) VALUES
(204, 1000, 1000, '2020-02-01', '09:18:00', 0, 0, 0, 1, 1, 0, '2020-02-01'),
(205, 1000, 1000, '2020-02-01', '18:00:00', 0, 0, 0, 2, 2, 0, '2020-02-01'),
(206, 1001, 1001, '2020-02-05', '10:22:00', 0, 0, 0, 1, 7, 0, '2020-02-05'),
(207, 1001, 1001, '2020-02-05', '18:40:00', 0, 0, 0, 2, 8, 0, '2020-02-05'),
(208, 1002, 1002, '2020-02-05', '09:11:00', 0, 0, 0, 2, 9, 0, '2020-02-05'),
(209, 1002, 1002, '2020-02-05', '19:33:00', 0, 0, 0, 2, 10, 0, '2020-02-05'),
(210, 1001, 1001, '2020-02-22', '09:44:00', 0, 0, 0, 1, 11, 0, '2020-02-22'),
(211, 1001, 1001, '2020-02-22', '19:34:00', 0, 0, 0, 2, 12, 0, '2020-02-22'),
(212, 1002, 1002, '2020-02-22', '09:42:00', 0, 0, 0, 2, 13, 0, '2020-02-22'),
(213, 1002, 1002, '2020-02-22', '18:12:00', 0, 0, 0, 2, 14, 0, '2020-02-22'),
(214, 1001, 1001, '2020-02-22', '09:12:00', 0, 0, 0, 1, 13, 0, '2020-02-22'),
(215, 1001, 1001, '2020-02-22', '19:23:00', 0, 0, 0, 2, 14, 0, '2020-02-22'),
(216, 1002, 1002, '2020-02-22', '09:45:00', 0, 0, 0, 2, 15, 0, '2020-02-22'),
(217, 1002, 1002, '2020-02-22', '18:56:00', 0, 0, 0, 2, 16, 0, '2020-02-22'),
(218, 1000, 1000, '2020-02-28', '07:00:00', 0, 1, 405, 1, 0, 1, '2020-02-28'),
(219, 1000, 1000, '2020-02-28', '19:23:34', 0, 1, 405, 2, 0, 2, '2020-02-28'),
(220, 1001, 1001, '2020-02-29', '07:00:00', 0, 1, 411, 1, 0, 1, '2020-02-29'),
(221, 1001, 1001, '2020-02-29', '19:23:34', 0, 1, 411, 2, 0, 2, '2020-02-29');

-- --------------------------------------------------------

--
-- Table structure for table `oc_attendance_2020_3`
--

CREATE TABLE `oc_attendance_2020_3` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `punch_date` date NOT NULL,
  `punch_time` time NOT NULL,
  `terminal_addr` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `log_id` int(11) NOT NULL,
  `new_log_id` int(11) NOT NULL,
  `download_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_attendance_2020_3`
--

INSERT INTO `oc_attendance_2020_3` (`id`, `emp_id`, `card_id`, `punch_date`, `punch_time`, `terminal_addr`, `status`, `transaction_id`, `device_id`, `log_id`, `new_log_id`, `download_date`) VALUES
(417, 1001, 1001, '2020-03-05', '10:17:00', 0, 1, 635, 3, 18, 0, '2020-03-05'),
(418, 1001, 1001, '2020-03-05', '22:04:00', 0, 1, 635, 4, 22, 0, '2020-03-05'),
(419, 1000, 1000, '2020-03-12', '08:00:00', 0, 1, 669, 43, 26, 0, '2020-03-12'),
(420, 1000, 1000, '2020-03-12', '22:00:00', 0, 1, 669, 3, 27, 0, '2020-03-12'),
(421, 1001, 1001, '2020-03-01', '10:00:00', 0, 1, 615, 1, 0, 3, '2020-03-01'),
(422, 1000, 1000, '2020-03-01', '07:00:00', 0, 1, 614, 1, 0, 3, '2020-03-01'),
(423, 1001, 1001, '2020-03-01', '19:23:34', 0, 1, 615, 2, 0, 4, '2020-03-01'),
(424, 1000, 1000, '2020-03-01', '18:00:00', 0, 1, 614, 2, 0, 4, '2020-03-01'),
(425, 1003, 1003, '2020-03-01', '13:00:00', 0, 1, 617, 4, 28, 0, '2020-03-01'),
(426, 1003, 1003, '2020-03-01', '16:00:00', 0, 1, 617, 4, 29, 0, '2020-03-01');

-- --------------------------------------------------------

--
-- Table structure for table `oc_attendance_2020_4`
--

CREATE TABLE `oc_attendance_2020_4` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `punch_date` date NOT NULL,
  `punch_time` time NOT NULL,
  `terminal_addr` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `log_id` int(11) NOT NULL,
  `new_log_id` int(11) NOT NULL,
  `download_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attendance_2020_5`
--

CREATE TABLE `oc_attendance_2020_5` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `punch_date` date NOT NULL,
  `punch_time` time NOT NULL,
  `terminal_addr` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `log_id` int(11) NOT NULL,
  `new_log_id` int(11) NOT NULL,
  `download_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attendance_new`
--

CREATE TABLE `oc_attendance_new` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(255) NOT NULL,
  `punch_date` date NOT NULL,
  `punch_time` time NOT NULL,
  `shift_intime` time NOT NULL,
  `late_time` time NOT NULL,
  `status` int(11) NOT NULL,
  `dept` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `division` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `device_id` int(11) NOT NULL,
  `download_date` date NOT NULL,
  `log_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `company` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute`
--

CREATE TABLE `oc_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_description`
--

CREATE TABLE `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group`
--

CREATE TABLE `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group_description`
--

CREATE TABLE `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner`
--

CREATE TABLE `oc_banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner_image`
--

CREATE TABLE `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner_image_description`
--

CREATE TABLE `oc_banner_image_description` (
  `banner_image_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_bill`
--

CREATE TABLE `oc_bill` (
  `bill_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `month` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `horse_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `transaction_type` int(11) NOT NULL,
  `trainer_id` int(11) NOT NULL,
  `invoice_date` date NOT NULL,
  `dot` date NOT NULL,
  `cancel_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_bill_owner`
--

CREATE TABLE `oc_bill_owner` (
  `id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `owner_share` int(11) NOT NULL,
  `owner_amt` float(10,2) NOT NULL,
  `owner_amt_rec` float(10,2) NOT NULL,
  `month` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `transaction_type` int(11) NOT NULL,
  `horse_id` int(11) NOT NULL,
  `trainer_id` int(11) NOT NULL,
  `invoice_date` date NOT NULL,
  `cancel_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_categories`
--

CREATE TABLE `oc_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category`
--

CREATE TABLE `oc_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `category_name` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category`
--

INSERT INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`, `category_name`) VALUES
(1, NULL, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'PIECE RATE');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_description`
--

CREATE TABLE `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_filter`
--

CREATE TABLE `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_path`
--

CREATE TABLE `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_layout`
--

CREATE TABLE `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_store`
--

CREATE TABLE `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_company`
--

CREATE TABLE `oc_company` (
  `company_id` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `company_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_company`
--

INSERT INTO `oc_company` (`company_id`, `company`, `company_code`) VALUES
(1, 'JMC PROJECTS (I) LTD', ''),
(2, 'ASCENT LOCAL', ''),
(4, 'ASK ME ENTERPRISE', ''),
(5, 'COSMOS-APOLLO', ''),
(6, 'APOLLO ENTERPRISES', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_complimentary`
--

CREATE TABLE `oc_complimentary` (
  `complimentary_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `department_mumbai` text NOT NULL,
  `loc` text NOT NULL,
  `department_pune` text NOT NULL,
  `department_moving` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_contractor`
--

CREATE TABLE `oc_contractor` (
  `contractor_id` int(11) NOT NULL,
  `contractor_name` varchar(255) NOT NULL,
  `contractor_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_contractor`
--

INSERT INTO `oc_contractor` (`contractor_id`, `contractor_name`, `contractor_code`) VALUES
(1, 'NITESH NIKAM', '3000026485'),
(2, 'ASHOK KUMAR AASHIK', '3000010689'),
(3, 'JAYA DUBEY', '3000010577'),
(4, 'HARISH KUKREJA', '50000000LLLLL'),
(5, 'PATEL YADAV', '3000029029'),
(6, 'AVIRA CONSTRUCTION CO', '3000026593'),
(7, 'SANTOSH SAH', '3000030003'),
(8, 'VISHAL SECURITY FORCE', '3000004642'),
(9, 'MOHAMMAD SAHIDULLA ', '3000027805'),
(10, 'K.G.N.R ENTERPRISES', '3000028831'),
(11, 'JAHIDA KHATOON', '3000029686'),
(12, 'ADITYA BUILDING SOLUTIONS ', '3000030613'),
(13, 'ALIK SK ', '3000030448'),
(14, 'AMAN SINGH ', '3000030112'),
(15, 'ANAND RAJKUMAR PAL ', 'VA0215'),
(17, 'BULU BHUYAN', '3000013383'),
(18, 'G GROUP FABRICATION ', '3000028871'),
(19, 'GANGA SAGAR SHEWALE ', 'VGM0192'),
(20, 'GOURAV SARKAR ', '3000021980'),
(21, 'HASAMUDEEN A ALI ', '3000025735'),
(22, 'HEERALAL KUSHWAHA ', 'VHK0116'),
(23, 'LAKSHMI NARAYAN GURU', 'VLN0033'),
(24, 'M.K.ENTERPRISES ', 'VMK0502'),
(25, 'MAZ ENTERPRISES', '3000027483'),
(26, 'MD VASIM KHAN ', '3000026499'),
(27, 'MOHD RIZVAN ', '3000026987'),
(28, 'NIRAJ', '3000027139'),
(29, 'NIRMAL BASAK ', '3000027681'),
(30, 'NIRMAN ', 'VNM0058'),
(31, 'PUNAVASI BHARATI ', 'VPB0140'),
(33, 'RAMESH H PANDIT ', '3000025643'),
(34, 'REYAZUDDIN HASIB MD ', '3000023948'),
(35, 'SACHI PRADHAN ', '3000030118'),
(36, 'SK AKTAR JAMIL ', '3000026866'),
(37, 'SKY SCRAPER ENGINEERS AND CONTRCTORS', '3000028591'),
(38, 'SONA LAL PRASAD YADAV ', '3000027682'),
(39, 'SUBHASHCHANDRA RAM ', 'VBSR0896'),
(40, 'SUBODH KUMAR PRADHAN ', '3000028616'),
(42, 'VIVEK KUMAR GUPTA ', '3000028093'),
(43, 'ISHAN CONSTRUCTION', '3000027100'),
(44, 'ASHAYBAR SHARMA', '3000007620'),
(45, 'SAIDUL ISLAM', '3000025948'),
(46, 'V K ENTERPRISES', '3000026819'),
(47, 'RANJU DEVI', '3000027190'),
(48, 'SATYENDRA RAY', '3000012229'),
(49, 'SUBODH KUMAR CHAUDHARY', '3000015803'),
(51, 'VED PRAKASH', '3000007646'),
(52, 'KANAK JASMINE ENTERPRISES', 'VKJ0647'),
(53, 'ALANGIR SHAIKH', '3000026775'),
(54, 'SUJIT KUMAR', '3000025100'),
(55, 'SHESHERAO NAMWAD', '3000027798'),
(56, 'DADU LAL', '3000026295'),
(57, 'SANTANU CHOUDHARY', '3000004638'),
(59, 'ASHOK MANDAL', '3000028592'),
(60, 'BHANUPRATAP YADAV', '3000000921'),
(61, 'BHIM ENTERPRISE', '3000015995'),
(63, 'DAROGI SINGH', '3000027246'),
(64, 'MANAVAR ALAM', '3000002148'),
(65, 'MD ARJAT AL', '3000027836'),
(68, 'PINAKI BISWAS', '3000024716'),
(70, 'RAHAMAT KHAN', '3000000039'),
(71, 'RAJ CONTRACTOR', '3000026298'),
(72, 'SECURE U SECURITY SERVICES', '3000020996'),
(73, 'SHARDA P RAM', '3000021288'),
(74, 'SULOCHANA BALIIRAM MOKSHI', '2000003492'),
(75, 'SUMIT KUMAR PATEL', '3000028438'),
(77, 'SURAJIT BARMAN', '3000026498'),
(78, 'SURESH TIRAPPA CHANDRA', '3000013309'),
(79, 'UJJWAL S MANDAL', '3000026595'),
(80, 'ABHIJIT BISWAS', '3000029910'),
(81, 'AJAY JAGGU BAHENWAL', '3000027806'),
(82, 'ALOK ENTERPRISE', '3000020704'),
(83, 'AMRITLAL SINGH', '3000025847'),
(84, 'ANIL S CONSTRUCTION', '3000025974'),
(85, 'ANTOSH YADAV', '3000024851'),
(86, 'AQUA FILTRON EQUIPMENTS', '2000002517'),
(87, 'ASAR INFRASTRUCTURE', '3000029933'),
(88, 'AURA FACILITIES MANAGEMENT PVT LTD', '3000021301'),
(89, 'BASAVA CONSTRUCTION', '3000029504'),
(90, 'BINOD KUMAR SHARMA', '3000029583'),
(91, 'FINE FIX', '3000029073'),
(92, 'H CONSTRUCTION', '3000003853'),
(93, 'IMRAN ALI', '3000028730'),
(94, 'JIYARUL SK', '3000024830'),
(95, 'KRISHNA MANDAL', '3000024545'),
(96, 'MATA BUILDTECH', '3000029476'),
(97, 'MD HABIZUR RAHMAN', '3000028435'),
(98, 'MD INNATULLAH', '3000024736'),
(99, 'MD MASRUR RAHAMAN', '3000017733'),
(100, 'MOHAMMAD JUBER KHAN', '3000028594'),
(101, 'MOHAMMED  AYATULLA KHOMENI', '3000028789'),
(102, 'MONIRUL SK', '3000028320'),
(103, 'NAND LAL', '3000029088'),
(104, 'NOZRUL ISLAM', '3000027699'),
(105, 'RAMESH BERA', '3000024731'),
(106, 'RAVI SHANKAR', '3000025148'),
(107, 'RK ENTERPRISES', '3000029270'),
(108, 'SABITA CONSTRUCTION', 'VSC1536'),
(109, 'SADGURU KRUPA ENTERPRISES', '3000025547'),
(110, 'SAHIL INTERIOR', '3000007416'),
(111, 'SEKH AZIMUL', '3000030625'),
(112, 'SHAHZAD ENTERPRISES', '3000028892'),
(113, 'SHER MOHAMMAD SEKH', '3000002081'),
(114, 'SUNDAR SHARMA', 'VSS0237'),
(115, 'SURAJ KUSHWAHA', '3000004732'),
(116, 'ASHOK KUMAR RAJAK', '3000000959'),
(117, 'D. G. PRADHAN', '3000013277'),
(118, 'GHIRAO NISHAD', '3000021360'),
(119, 'MANISH ENTERPRISES', '3000022749'),
(120, 'MD ASRAFUL SK', '3000011121'),
(121, 'MD MOBARAK HOSSAIN', '3000003595'),
(122, 'PUSPRAJ SINGH CHANDEL', '3000024548'),
(123, 'RAGHUNATH BHUYAN', '3000007940'),
(125, 'DHARMENDRA RAY', '3000025730'),
(126, 'SHARMA CONSTRUCTION', 'VSC1552'),
(127, 'RAVINDRA KUMAR', 'VRK0809'),
(128, 'JUNAID ENTERPRISES', '3000029072'),
(129, 'NABA KUMAR BAIDYA', '3000006125'),
(130, 'OMKAR OUTSOURCING PRIVATE LIMITED', '3000010593'),
(131, 'VIVEK S INDAPURE', 'VVS0397'),
(132, 'ZEBA CONSTRUCTION', '3000006909'),
(133, 'MOHEDUL KHAN', '3000026976'),
(134, 'MENAKA CONSTRUCTION', '3000025435'),
(135, 'AMOL KHAN', '3000028790'),
(136, 'ABHISHEK ENTERPRISE', '2000001132'),
(137, 'SHIVV SHAKTI CONSTRUCTION', '3000030310'),
(138, 'RAMNATH SINGH', '3000024780'),
(139, 'IQBAL AHMAD', '3000030069'),
(140, 'RAHUL SK', '3000026458'),
(141, 'ASHIS KUMAR SATYANARAYAN DUBEY', '3000030136'),
(142, 'UMESH JANARDAN GAWADE', '3000030106'),
(143, 'SEKH KAISHUL', '3000026379'),
(144, 'PRASANTA KUMAR RAUT', '3000000376'),
(145, 'STAR INTERIORS', 'VSI0964'),
(146, 'S.A. ENTERPRISES', '3000025156'),
(147, 'DIGAMBAR PIRAJI DEVKAR', '3000030977'),
(148, 'M S M WATERPROOFING', '3000029265'),
(149, 'SEKH ALAMAS', '3000023971'),
(150, 'BINDESHWARI SAHANI  ', '3000021398'),
(151, 'DILDAR SHAIKH & CO.', '3000030137'),
(152, 'PRAMOD YADAV', '3000008625'),
(153, 'SABIR SHEKH', '3000027465'),
(154, 'KUNJ BIHARI URF LAGANDHARI', '3000010573'),
(155, 'SAI CONSTRUCTION COMPANY', '3000027864'),
(156, 'MERAJ ALAM', '3000029183'),
(157, 'MD . KASHFUL HAQUE', '3000027862'),
(158, 'ANSUR RAHMAN', '3000026816'),
(159, 'MUNNA KUMAR', '3000031016'),
(160, 'AISHA ENTERPRISES', 'VAE0844'),
(161, 'MAHESH KUMAR PATEL', '3000023958'),
(162, 'PIONEER INTERNATIONAL', '3000031052'),
(163, 'BAHARUL ISLAM', '3000031053'),
(164, 'FIROJ', '3000013249'),
(165, 'ALISHA SHAHNAWZ ALI KHAN', '3000021763'),
(166, 'RAJESH KUSHWAHA', '3000022802'),
(167, 'EELVIN ENGINEERING INDIA PVT LTD', '3000029356'),
(168, 'SHREE KRISHNA FAB-CONS ', '3000027982'),
(169, 'KANWAR ENTERPRISES', '3000007169'),
(170, 'NURASHED ALAM', '3000027619'),
(171, 'MAA KALI CONSTRUCTION', '4000000361'),
(172, 'NAEEM AHAMD CONSTUCATION', '3000007047'),
(173, 'SANJAY SINGH', '3000010461'),
(174, 'ISLAM SK', '3000016253'),
(175, 'MD AKHTARUL ALAM BISWAS', '3000028959'),
(176, 'ABDUR RAHIM', '3000029074'),
(177, 'UDAY PAL', '3000018670'),
(178, 'JITENDRA KUMAR', '3000031108'),
(179, 'BINOD ENTERPRISES', '3000030550'),
(180, 'VINDESHWARI ENTERPRISES', '3000028575'),
(181, 'MD HEDAYAT SK', '3000018709'),
(182, 'ARADHYA ENTERPRISES', '3000025786'),
(183, 'SACHICHDANAND KUMAR', '3000004597'),
(184, 'MURARI RAY', '3000025134'),
(185, 'MD ALAM ', '3000023444'),
(186, 'TIRUPATI BALAJI CONSTRUCTION CO', '3000028757'),
(187, 'ARUN KUMAR', '3000022351'),
(188, 'SIPRA MANDAL', '3000031176'),
(189, 'GANGOTI BAI', '3000029186'),
(190, 'CHANDRADEV YADAV', '3000015948'),
(191, 'MOHD BAJIR', '3000031181'),
(192, 'AMBIKA PRASAD', '3000024981'),
(193, 'S S ELECTRICAL', '3000030583'),
(194, 'SHIV KUMAR YADAV', '3000029697'),
(195, 'ZAKIR HUSSAIN', '3000027179'),
(196, 'M.K.CONSTRUCTION', '3000031082'),
(197, 'SANJAY MEHTA', '3000025633'),
(198, 'MOHD YUSUF', '3000025298'),
(199, 'MAHADHUTYA ELECTRICALS', '3000030482'),
(200, 'SOBOL SINH', '3000024083'),
(201, 'JRS INFRATECH PRIVATE LIMITED', '3000026888'),
(202, 'DRAVIN ENTERPRISES', '3000018027'),
(203, 'AC ENTERPRISES', '3000028840'),
(204, 'AJAY SHANKAR PATEL', '3000029936'),
(205, 'ANJAR ALAM', '3000015511'),
(206, 'ARADHANA DEVI', '3000025325'),
(207, 'ARVIND PUNJAYAR', '3000029259'),
(208, 'ASAGAR ALI MANSURI', '3000024523'),
(209, 'BANTI KUMAR', '3000029979'),
(210, 'KAMLESH KUMAR', '3000024985'),
(211, 'LAL MOHAMMAD', 'VLM0067'),
(212, 'MAHAJURA BIBI', '3000016061'),
(213, 'MANJUR ANSARI', '3000017869'),
(214, 'MD RAJIKUL SK', '3000029502'),
(215, 'MODERN CONSTRUCTION', '3000028724'),
(216, 'MOHAMMAD NASEEM', '3000028681'),
(217, 'PRASANT NAYAK', '3000025565'),
(218, 'RAJ CONSTRUCTIONS', '3000026903'),
(219, 'RAM SINGH', '3000024526'),
(220, 'RAMBABU PRASAD', '3000026142'),
(221, 'REZA NOOR', '3000030081'),
(222, 'SADDAM ANSARI', '3000027461'),
(223, 'SANTOSH KUMAR', '3000028423'),
(224, 'SARIUL KHAN', '3000025566'),
(225, 'SRI MANTA DAS', '3000029811'),
(226, 'SUNIL MEHATA', 'VSM1007'),
(227, 'RAJENDRA KUMAR', '3000029937'),
(228, 'SHAKTI SINGH', '3000029813'),
(229, 'SANJAY SINGH', '3000010641'),
(230, 'ANURADHA SINGH', '3000026310'),
(231, 'TANAVIR ALAM', '3000002006'),
(232, 'ARMOUR SURFACE COATINGS', '3000002918'),
(233, 'JARIN ENTERPRISE', '3000027842'),
(234, 'BADAN SINGH RAGHUWANSHI', '3000024829'),
(235, 'PHOOL SINGH', '3000012613'),
(236, 'SAUDAGAR NAYAK', '3000002470'),
(237, 'BABOR ALI', '3000031254'),
(238, 'VARUN KAUSHIK', '3000031256'),
(239, 'VINOD YADAV', '3000029529'),
(240, 'SANJAY KUMAR SHARMA', '3000025314'),
(241, 'BISWAJIT SARKAR', '3000029691'),
(242, 'RAJENDRA RAY', '3000031190'),
(243, 'OMKAR', '3000017878'),
(244, 'PAVAN KUMAR', '3000027988'),
(245, 'MD. ENATULLAH', 'VMD0186'),
(246, 'CHANDRA ENTERPRISES', 'VCE0219'),
(247, 'JITENDRA KUMAR', '3000028515'),
(248, 'RAJESH RAY', '3000027863'),
(249, 'BALRAM RAM', '3000012524'),
(250, 'MUJAMMIL HUSAIN', '3000031140'),
(251, 'HARISON CONSTRUCTION', '3000031225'),
(252, 'PUNAVASI BHARATI', '3000014000'),
(253, 'VINOD KUMAR DWIVEDI', '3000021849'),
(254, 'FRONTLINE SECURITY & ALLIED SERVICES', '3000025311'),
(255, 'AASMEEN', '3000029579'),
(256, 'KAMLESH KUMAR JANGID', 'VKK0285'),
(257, 'L M CONSTRUCTION', '3000022336'),
(258, 'MOHD SHAHRUKH', '3000028917'),
(259, 'S M ENTERPRISES', '3000030697'),
(260, 'DEEPSHIKHA ENTERPRISES', '3000029533'),
(261, 'MANI LAL KUMAR', '3000031085'),
(262, 'SHREE GANESH KRUPA TRANSPORT', '30000264678'),
(263, 'SHREE VIGHNAHARTA TRANSPORT', '3000012527'),
(264, 'SHREE DATTA KRUPA TRANSPORT', '3000012119'),
(265, 'SHIVAJI DNYADEV PAWAR', '3000002762'),
(266, 'J.C CONCRETE SOLUCTION PRIVATE LIMITED', '3000029647'),
(267, 'ABRARUL HOQUE', '3000030481'),
(268, 'ALI HUSAIN', '3000020576'),
(269, 'ARMAN ANSARI', '3000031083'),
(270, 'ASHIM MANDAL', '3000024972'),
(271, 'ASHOK YADAV', '3000029751'),
(272, 'AVANTIKA TIWARI', '3000030265'),
(273, 'BABULAL', '3000018660'),
(274, 'BANAVARI', '3000017955'),
(275, 'BORAN MANDAL', '3000024969'),
(276, 'CHATURGUN PANDIT', '3000013100'),
(277, 'CITY ELECTRICAL', '3000029753'),
(278, 'D S INFRA TECHNOLOGY', '3000029542'),
(279, 'DMT ENTERPRISES', '3000029854'),
(280, 'GYAN CHAND SHARMA', '3000030499'),
(281, 'HIMANI ELECTRICAL', '3000030498'),
(282, 'IMTIYAZ', '3000031375'),
(283, 'MAA AMBAY CONSTRUCTION ENGINEERING SERVICES', '3000030480'),
(284, 'MANOJ KUMAR', '3000031284'),
(285, 'MANSURI MAHLI', '3000016469'),
(286, 'MD KASHIF', '3000031451'),
(287, 'MOHD MEHRAJ', '2000009334'),
(288, 'MOIN', '3000031084'),
(289, 'MOJAFFAR', '3000031288'),
(290, 'MUKESH KUMAR YADAV', '3000026469'),
(291, 'NIDAEMA INFRACON PRIVATE LIMITED', '3000030949'),
(292, 'OM PRAKASH SINGH', '3000027058'),
(293, 'PANKAJ', '3000027916'),
(294, 'PAWAN KUMAR', '3000028201'),
(295, 'PRAMODKUMAR HARI MANDAL', '3000031287'),
(296, 'R V D BUILDTECH PRIVATE LIMITED', '3000028288'),
(297, 'RADHEY SHYAM MAURIYA', '3000030839'),
(298, 'RAJU KUMAR', '3000030623'),
(299, 'RUPESH KUMAR', '3000029245'),
(300, 'SAMA ENTERPRISES', '3000024030'),
(301, 'SANIL KUMAR', '3000027019'),
(302, 'SAVITA DEVI', 'VSD0369'),
(303, 'SHISH MOHAMMAD', '3000021203'),
(304, 'SHIV SHAKTI CONSTRUCTION', '3000007121'),
(305, 'SHREERAM', '3000028626'),
(306, 'SIKENDAR SK', '3000027915'),
(307, 'SNS STEEL WOODEN DECORATION GROUP', '3000005290'),
(308, 'SOHARAB ALI', 'VSA1212'),
(309, 'TRISHUL CONSTRUCTIONS', '3000026661'),
(310, 'VIJAY ELECTRICALS', '3000030264'),
(311, 'ZAKI ANWER', '3000026470'),
(312, 'BABLU  SHEKH ', '3000031277'),
(313, 'NAZIA HASSAN', '3000031468'),
(314, 'DEVENDRA PRATAP', '3000010181'),
(315, 'JYOTI ENTERPRISES', '3000031069'),
(316, 'SAMIM ANSARI', '3000015062'),
(317, 'PHOOLKUMARI DEVI', '3000029532'),
(318, 'MOHD FAEEM', '3000013370'),
(319, 'AVINASH NATH TIWARI', '3000021407'),
(320, 'DELIGHT FACILITIES', '3000024714'),
(321, 'PRAMOD GUPTA', '3000017981'),
(322, 'VINOD PAL', '3000021680'),
(323, 'MOHD SAHA ALAM', '3000001312'),
(324, 'MD TAIYAB', '3000019249'),
(325, 'MERIDIAN INFRA SERVICES PVT LTD', '3000029118'),
(326, 'P G CONSTRUCTION', '3000028436'),
(327, 'R & L ENTERPRISES', '3000027623'),
(328, 'ARTI GUPTA', '3000031589'),
(329, 'N.S.ENTERPRISES', '3000030855'),
(330, 'ANUJ SHARMA', '3000022857'),
(331, 'ATUL KUMAR', '3000031629'),
(332, 'SANDIP KUMAR ', '3000009517'),
(333, 'RAMESHWAR', '3000031617'),
(334, 'LALIT KUMAR CHANDRAKAR', '3000031012'),
(335, 'LINGRAJ THAKUR', '3000031296'),
(336, 'KRISHNA DAS MAITY', '3000001758'),
(337, 'JHUMA BIBI', '3000025969'),
(338, 'MANOJ KUMAR YADAV', '3000019822'),
(339, 'SK KAMIRUL HAQUE', '3000028525'),
(340, 'DAMU', '3000010245'),
(341, 'MONALISA ENTERPRISES', '3000024531'),
(342, 'RAMESH KUMAR SAHA', '3000008699'),
(343, 'MUKESH KUMAR YADAV', '3000025132'),
(344, 'AJAY YADAV ', '3000028967'),
(345, 'MOHARRAM', '3000031477'),
(346, 'ARUN KUMAR', '3000001756'),
(347, 'HEMANT KUMAR', '3000027126'),
(348, 'SURYABALI PATEL', '3000024707'),
(349, 'DEEPAK CHAUHAN', '3000027303'),
(350, 'PRADEEP CHAUHAN', '3000031628'),
(351, 'ARSHAD HUSSAIN', '3000026924'),
(353, 'ONS CONSTRUCTIONS', '3000031261'),
(354, 'RAMCHANDRA BHAGAT', '3000025578'),
(355, 'BISHWAJIT GHOSH', '3000027134'),
(356, 'JITENDAR SHAH', '3000014296'),
(357, 'JAIVIND KUMAR', '3000026949'),
(358, 'FEKAN VISHWAKARMA', 'FEKAN VISHWAKARMA'),
(359, 'MUKESH KUMAR SHETYA', 'MUKESH KUMAR SHETYA'),
(360, 'SHRIRAM BHARATI', '3000030060'),
(361, 'AMIT KUMAR ', '3000023801'),
(362, 'KUNDAN YADAV', '3000004041'),
(363, 'RAJKUMAR CHAUDHARY', 'RAJKUMAR CHAUDHARY'),
(364, 'RAKESH KUMAR     ', 'RAKESH KUMAR     '),
(365, 'RABIUL KHAN', '3000031124'),
(366, 'AMAL BISWAS', '3000028969'),
(367, 'ANJALI JANA', '3000028601'),
(368, 'PURNA CHANDRA BURJA', '3000031551'),
(369, 'SHIV KUMAR VISHWAKARMA', '3000015631'),
(370, 'DHARMDEV PRASAD YADAV', '3000030307'),
(371, 'J.B.CONSTRUCTION', '3000006263'),
(372, 'AKHILESH KUMAR', '3000030702'),
(373, 'ARCHANADEVI VINOD YADAV', '3000031774'),
(374, 'PANESH HALDER', '3000031297'),
(375, 'SHYAM YADAV', '3000031748'),
(376, 'AJADUR RAHMAN', '3000031714'),
(377, 'NIRAKAR ACHARY', '3000031715'),
(378, 'GEETA ENTERPRISES', '3000025242'),
(379, 'MILI SARDAR', '2000004272'),
(380, 'RADIANCE CONSTRUCTION', '3000025222'),
(381, 'RAJESH KUMAR SHARMA', '3000020533'),
(382, 'RAMPUKAR PRASAD', '3000001531'),
(383, 'SUGREEV PRASAD VERMA', '3000001157'),
(384, 'ASHOK VISHAWAKARMA', '3000025537'),
(385, 'BAHAR CONTRACTION COMPANY', '3000025102'),
(386, 'DEEPAK MEHTA', '3000029990'),
(387, 'DHARMENDRA KUMAR SAHU', '3000029704'),
(388, 'DYNAMIC', '3000000062'),
(389, 'GANESH PRASAD SAH', '3000013385'),
(390, 'PARMEELA SELAR', '3000026336'),
(391, 'RAJNISH CONSTRUCTION', '3000015981'),
(392, 'SATYA CONTRACTION', '3000007967'),
(393, 'VANSHUDAY MEHTA', '3000030279'),
(394, 'M A ANSARI', '3000013264'),
(395, 'HARIKESH YADAV', '3000029292'),
(396, 'ASHOK MISHRA', '3000027764'),
(397, 'M.K.CONSTRCUTION', '3000028998'),
(398, 'HUMAYUN KABIR', '30000319285'),
(399, 'NAND KISHORE ', '3000024091'),
(400, 'AJAY KUMAR ', '3000017908'),
(401, 'AK INDIA ENGINEERING AND CONST', '3000027258'),
(402, 'ARUN KUMAR MEHTA', '3000025887'),
(403, 'ASHISH KUMAR MISHRA', '3000027533'),
(404, 'ASHOK KUMAR AASHIK', '3000025583'),
(405, 'AVADHESH VISHWAKARMA', '3000029714'),
(406, 'BAC PVT LTD', '2000009764'),
(407, 'DILIP KUMAR ', '3000018132'),
(408, 'DILIP SHARDIYA ', '3000029230'),
(409, 'ECE INDUSTRIESE', '3000021259'),
(410, 'FURKAN', '3000030139'),
(411, 'JK BUILDERS', '3000003155'),
(412, 'JUGRAJ SINGH', '3000016447'),
(413, 'NIRAJ KUMARI', '3000028408'),
(414, 'PAVAN YADAV', '3000022818'),
(415, 'PRADEEP SINGH ', '3000015619'),
(416, 'PRIYA SHARMA', '3000029871'),
(417, 'RAMKARAN BIRLA ', '3000025766'),
(418, 'RD CONTRUCTION', '3000031552'),
(419, 'SANT SINGAJI KAMGAR KARIGAR SAHAKARI ', '3000025728'),
(420, 'SHANTILAL DHANILAL TAWDE', '3000015760'),
(421, 'SHARIF MANSURI ', '3000027471'),
(422, 'SHRAWAN CHAUHAN', '3000010983'),
(423, 'SUBAL CHANDRA GIRI ', '3000030866'),
(424, 'SUNDAR YADAV', '3000020513'),
(425, 'SUNIL ROY', '3000029652'),
(426, 'UPENDRA MOCHI', '3000024529'),
(427, 'VIDYA NAND SINGH', '3000017224'),
(428, 'PRAMOOD PANDAY', '3000026847'),
(429, 'GSR PLACEMENT & SECURITY SERVICES', '3000013987'),
(430, 'MANAV DEKOR', '3000025126'),
(431, 'MEDIA DECOR', '3000025693'),
(432, 'ASHOK BARAL', '3000027585'),
(433, 'BUBUN MANNA', '3000015685'),
(434, 'N C INFRACON PVT LTD', '3000027869'),
(435, 'PNS INFRA TECH', '3000028612'),
(436, 'RAM KRISHAN TRIPATHI', '3000030672'),
(437, 'REWLAL SAW', '3000030040'),
(438, 'SELMEC ENGINEERINGH & CONSTRUCTION', '2000008643'),
(439, 'SONIYA FEBRICATORS', '3000030913'),
(440, 'TAPAS KUMAR DAS', '3000002796'),
(441, 'AYUSH CONSTRUCTION', '3000029988'),
(442, 'GAUTAM CONSTRUCTION', '3000023051'),
(443, 'KRISHNA PRASAD', '3000020547'),
(444, 'ONS CONSTRUCTIONS', '2000003083'),
(445, 'PRAMOD DALAI', '3000030305'),
(446, ' SINGH PROTECTIVE SERVICE PVT LTD', '3000027920'),
(447, 'SUNEEL KUMAR SINGH', '3000031363'),
(448, 'VISHESHWAR CHOUDHARI', '3000027304'),
(449, 'MAHENDRA SINGH SAHU', '3000031746'),
(450, 'RAKESH KUMAR', '3000031747'),
(451, 'GOBINDA BARAI ', '3000028968'),
(452, 'BIKASHCHANDRA RAY', '3000026174'),
(453, 'DHANIRAM KASHYAP', '3000031294'),
(454, 'ARJUN KUMAR', '3000030473'),
(455, 'UTILITY CONSTRUCTION ENGINEERS', '3000027277'),
(456, 'PRADEEP KUMAR NAYAK', '3000030701'),
(457, 'TRINATH ENGINEERING WORKS', '4200019973'),
(458, 'RAMCHANDRA SINGH', '3000031045'),
(459, 'TAJER ALI', '3000029875'),
(460, 'ALAMGIR ALAM', '3000031726'),
(461, 'BISWAS CONSTRUCTION', '3000025323'),
(462, 'NARAYAN CHAKRABORTY', '3000025122'),
(463, 'MIRMAHAMMAD HASIRUL ISLAM ', '3000026619'),
(464, 'GOBARDHAN CONSTRUCTION', '2000006896'),
(465, 'AJAY SHANKAR PAL', '3000005758'),
(466, 'MOHD FARUKH ALI', '3000020523'),
(467, 'UNIQUE SECURITY & MANAGEMENT SERVICES', '3000004172'),
(468, 'AJAY SHARMA', '3000027394'),
(469, 'DURGAWATI DEVI', '3000015946'),
(470, 'PRAVIN ELECTRICALS', '3000021627'),
(471, 'TARTE ENGINEERS PVT LTD', '3000013311'),
(472, 'CHANDRA VILAS ARNY', '3000029808'),
(473, 'KD CONSTRUCTION', '3000031282'),
(474, 'MAHENDRA KUMAR DUBEY', '3000018174'),
(475, 'TROOPS FACILITY', '3000018721'),
(476, 'NISHAN ALI', '3000029273'),
(477, 'GOKUL MANDAL', '3000013179'),
(478, 'SUMIT KUMAR PATHAK ', '3000007862'),
(479, 'KAMDEO PRASAD YADAV', '3000004645'),
(480, 'KAMLAKANT KUSHWAHA', '3000031199'),
(481, 'MANANUM MANEGMENT SERVICE', '3000026550'),
(482, 'GYANTI DEVI', '3000013076'),
(483, 'JABIR HUSSAIN', '3000015939'),
(484, 'MD.FIROJ', '3000013256'),
(485, 'MUSTAI HASAIN', '3000029729'),
(486, 'RAM NARESH', '3000016026'),
(487, 'SHILANATH SHARMA', '3000010560'),
(488, 'SECURITY INTELLIGENCE SERVICE IND LTD', '3000006608'),
(489, 'RIFAKAT ALI', '3000030277'),
(490, 'NIRBHAY SINGH', '3000018689'),
(491, 'ALOK', '3000027520'),
(492, 'RAJENDRA PRASAD YADAV ', '3000029129'),
(493, 'SEKH RAISUL', '3000031604'),
(494, 'SUNIL MOTIRAM BAGUL', '3000025345'),
(495, 'PRASHANT TYAGI', '3000027968'),
(496, 'B.K. INTERPRISES', '3000030892'),
(497, 'GULAB KUMAR MAURYA', '3000009634'),
(498, 'DURGESH SHARMA', '3000026441'),
(499, 'STAR INTERIORS', '3000007945'),
(500, 'D.S CONTINENTAL INFRA', '3000028816'),
(501, 'ELITE DETECTIVE & SECURITY SERVICE ', '3000030211'),
(502, 'GREENTECH STRUCTURRALS', '3000027693'),
(503, 'HEJJAJI ENGINEERING & FABRICATORS', '2000000113'),
(504, 'ISI INDIAN SECURITY', '3000028135'),
(505, 'JIBAN SARKAR', '3000029843'),
(506, 'MANIKHI', '3000008636'),
(507, 'MOHAMMD AZHAR KHAN', '3000030309'),
(508, 'PRAMOD DIGAMBER CHAMPATI ', '3000029242'),
(509, 'R & S ENTERPRISES', '3000030238'),
(510, 'R.K.CONSTRUCTION', '3000030818'),
(511, 'RAJU KHAN', '3000029851'),
(512, 'SHRADHA ENGINEERING PVT.LTD', '2000011684'),
(513, 'SNS INFRATECH', '3000031077'),
(514, 'SRI KRISHNA CONSTRUCTION', '3000031564'),
(515, 'SRS SECURITY & MANAGEMENT SERVICES', '3000028657'),
(516, 'SUNIL KUMAR PANDEY', '3000007996'),
(517, 'TARIQUL ISLAM', '3000031536'),
(518, 'UPENDRA KUMAR', '3000009303'),
(519, 'VANSH CONSTRUCTION ', '3000030914'),
(520, 'HARISHANKAR GUPTA', '3000007068'),
(521, 'SANTOSH KUMAR', '3000014872'),
(522, 'SHAHBOOB HASAN', '3000027632'),
(523, 'SUMESH KUMAR', '3000004320'),
(524, 'ROOF & CELLING (I) PVT LTD', '3000026706'),
(525, 'SURAJ KUMAR', '3000029693'),
(526, 'AJAD ANSARI', '3000030950'),
(527, 'ASHISH TIWARI', '3000017535'),
(528, 'BHUPAT SINGH NIRMALKAR', '3000028155'),
(529, 'KAMLESH MAHTO', '3000011766'),
(530, 'MITHLESH KUMAR', '3000029573'),
(531, 'MOHAMMAD SHAIKH', '3000012723'),
(532, 'SHIVANATH BAITHA', '3000026940'),
(533, 'RAM NARESH RAY', '3000030832'),
(534, 'TARAKNATH MAHAPATRA', '3000030366'),
(535, 'YOUSUF HOSSAIN ', '3000010031'),
(536, 'SRIPRAKASH YADAV ', '3000027278'),
(537, 'SANJEEV KUMAR', '3000010679'),
(538, 'SITA DEVI', '3000031637'),
(539, 'UPENDRA PRASAD', '3000031192'),
(540, 'USHA DEVI', '3000028966'),
(541, 'RASULAN', '3000009943'),
(542, 'NASEEM KHAN', '3000028679'),
(543, 'MD EADIRS', '2000006099'),
(544, 'IRSAD', '3000027463'),
(545, 'INTEXT DECOR', '3000026436'),
(546, 'LAKHDANA WATERPROOFING OPC PRIVATE LIMITED', '3000031087'),
(547, 'ASHOK YADAV', '3000003983'),
(548, 'P3 INFRATECH & INNOVATION', '3000030203'),
(549, 'CHANDAN TRADING COMPANY', '2000002116'),
(550, 'PAWAN KUMAR SHARMA', '3000007618'),
(551, 'DAYARAM CHAUDHARI', '3000010711'),
(552, 'IMAMUL HOQ', '3000003314'),
(553, 'R.K.CONSTRUCTIONS', '3000018455'),
(554, 'BABITA DEVI', '3000031866'),
(555, 'MOHAMMAD SAJID', '3000021887'),
(556, 'ISQUARE INTERIORS', '3000013028'),
(557, 'NASEEB ALI', '3000031840'),
(558, 'MOHAMAD ABDUL ALIM', '3000020984'),
(559, 'MUJAHID', '3000013356'),
(560, 'FOREVER DECORATORS', '3000031627'),
(561, 'NUR SELU', '3000013360'),
(562, 'AUM DECO & INTERIOR', '3000028291'),
(563, 'ANANYA INTERIOR', '3000024975'),
(564, 'ENGINEERS INFRA', '3000010359'),
(565, 'KAMAL MANDAL', '3000009540'),
(566, 'A ONE POP', '3000028954'),
(567, 'PRATAP SINGH', '3000004582'),
(568, 'PM ENTERPRISES', '3000031841'),
(569, 'SUPER GARDENER', '3000031839'),
(570, 'K K ENTERPRISES', '3000031868'),
(571, 'AMIT KUMAR', '3000031869'),
(572, 'BOT ENTERPRISES', '3000021697'),
(573, 'NEW DELHI SECURITY SERVICES PVT. LTD', '3000028150'),
(574, 'PRANAV KUMAR', '3000028167'),
(575, 'SHEEVAM FACILITY SERVICE', '3000020531'),
(576, 'CHANDRABHUSHAN S KEVAT', '3000020146'),
(577, 'RAVINDRA KUMAR', '3000030610'),
(578, 'TUNTUN SINGH', '3000031728'),
(579, 'STRESSTECH ENGINEERS PRIVATE LIMITED', 'VSE1047'),
(580, 'E PACK POLYMERS PVT LTD', '2000010039'),
(581, 'AARYA ENTERPRISES', '3000031459'),
(582, 'MOHD DANISH', '3000031873'),
(583, 'VISHNU VERMA', '3000006133'),
(584, 'SAHEB PRASAD', '3000028239'),
(585, 'SOHAN', '3000020812'),
(586, 'SANDEEP KUMAR YADAV', '3000027177'),
(587, 'DHANU KUMAR RAI', '3000003350'),
(588, 'ORION PEST SOLUTION PVT LTD', '3000028796'),
(589, 'I3 SERVICES MANAGEMENT PVT LTD', '3000013401'),
(590, 'PRINCE SECURITY SERVICE', '3000011997'),
(591, 'BHEECOME CHAND', '3000029653'),
(592, 'BHUPENRA KUMAR DARAKLAL', '3000012429'),
(593, 'B R POWER TECH', '3000003786'),
(594, 'BALAJEE SERVICES', '3000021086'),
(595, 'BHAVISHAN KUMAR', '3000022720'),
(596, 'GULAM MUSTAFA', '3000021989'),
(597, 'GURU DEO YADAV', '3000003536'),
(598, 'MD SAIRUL HOQUE', '3000024753'),
(599, 'NAND KISHOR SINGH ', '3000007557'),
(600, 'NEHA WATER PROFFING', '3000021593'),
(601, 'R.D.ENTERPRISES', '3000025464'),
(602, 'RAJ BALI RAM', '3000019799'),
(603, 'RAJESH KUMAR-1', '3000014485'),
(604, 'RAJESH KUMAR-2', '3000029575'),
(605, 'RAKESH KUMAR', '3000015433'),
(606, 'SAILESH KUMAR TIWARI', '3000030952'),
(607, 'SIKENDAR SINGH', '3000008593'),
(608, 'SREE JEE INFRATECH LLP', '3000015906'),
(609, 'SUBHAS YADAV', '3000031450'),
(610, 'SUNIL KUMAR SINGH', '3000018755'),
(611, 'MUKESH KUMAR', '3000030149'),
(612, 'NEW OBEDIENT SECURITY', '3000015568'),
(613, 'MD. VAKIL', '3000030586'),
(614, 'GOPAL PAWAR', '3000023860'),
(615, 'SANJAY RAMKRUSHNA MAHULE', '2000001245'),
(616, 'SHANKAR BARAL', '3000028283'),
(617, 'SELMEC ENGINEERING CONSTRUCTION', '2000008644'),
(618, 'ABHIMANYU SINGH  ', '3000028410'),
(619, 'BAWA PLUMBING ', '3000027152'),
(620, 'MANGLA SHRI STORES', '2000010239'),
(621, 'MC PAADIT INTERIORS & CONTRACTORS', '3000028504'),
(622, 'SEWAK AAJURAM KAUSHIK', '3000031930'),
(623, 'PANKAJ SHARMA', '3000031897'),
(624, 'K. KUMAR CONSTRUCTION', '3000010440'),
(625, 'ASHIM DEBNATH', '3000011318'),
(626, 'BHAGIRATHI BAHALIA', '3000026173'),
(627, 'BISHNUPADA ROUT', '3000029225'),
(628, 'CHANDESHWAR SHARMA', '3000031228'),
(629, 'H R INFRA', '3000026542'),
(630, 'HEMANTA KUMAR PRUSTY', '3000002875'),
(631, 'KRISHNANDAN YADAV', '3000029226'),
(632, 'NABAKISHOR ROUT', '3000030989'),
(633, 'NAIVEDYA POWER CONSTRUCTION PVT LTD', '3000026543'),
(634, 'NIRANJAN KUNWARA', '3000031260'),
(635, 'PATRA CONSTRUCTION', '2000001230'),
(636, 'PRUTHWI CONSTRUCTION', '3000029228'),
(637, 'R K CONSTRUCTIONS', '3000029229'),
(638, 'RAJAT CONSTRUCTION', '3000027110'),
(639, 'RAJDHANI CONSTRUCTION', '3000026026'),
(640, 'RAMDEV KUMAR GOD', '3000030205'),
(641, 'S S CONSTRUCTION', '3000026094'),
(642, 'SAI KRISHNA ENGG', '3000030943'),
(643, 'SAI KRISHNA ENGG', '3000031482'),
(644, 'SURESH KUMAR MAHARANA', '3000030318'),
(645, 'TAMANNA CONSTRUCTIONS', '3000029953'),
(646, 'TILAK SHARMA', '3000006327'),
(647, 'UPENDRA KUMAR SINGH', '3000030206'),
(648, 'VISHAL CONSTRUCTIONS', '3000031931'),
(649, 'MD RAIYAHAN', '3000027193'),
(650, 'AJAD ANSARI', '3000019897'),
(651, 'MORSALIM SK', '3000017508'),
(652, 'CHOUDHARY CONSTRUCTION', '3000024713'),
(653, 'SUMAN DEVI BIND', '3000031014'),
(654, 'MUSTAFA SK', '3000031775'),
(655, 'SHALLOM PREFAB SYATEMS', '3000031882'),
(656, 'KRISHAN MOHAN KUMAR', '3000031966'),
(657, 'GOPAL SINGH', '2000008858'),
(658, 'JMD TRADING', '3000028107'),
(659, 'CLEANIFY', '3000027464'),
(660, 'MOHAMMAD MUNAJEER SHEIKH', '3000031836'),
(661, 'KHODA BOX MONDAL', '3000020363'),
(662, 'G S STAFFING SOLUTIONS PVT LTD', '3000004480'),
(663, 'RADHASAYAM', '3000031822'),
(664, 'SHEKH MUNNA', '3000031553'),
(665, 'AKTHARUL ISLAM', '3000012545'),
(666, 'ARVIND YADAV', '3000030721'),
(667, 'ASHISH KUMAR', '3000031227'),
(668, 'SEKHA SABIR ALI', '3000011168'),
(669, 'TOPN RAM SAHU', '3000031957'),
(670, 'NILAM K SHARMA', '3000009604'),
(671, 'RS CONSTRACTION', '3000028752'),
(672, 'SADEKUL SK', '3000031963'),
(673, 'OPENDAR NATH', '3000018672'),
(674, 'APR SECURITY INDIA PVT. L', '3000001965'),
(675, 'AMIPRO ASSOCIATES', '3000031646'),
(676, 'BUREAU VERITAS INDIA PVT LIMITED', '3000030723'),
(677, 'DEEPAN RAM', '3000031926'),
(678, 'GAYADHAR DHIR', '3000026931'),
(679, 'KUMAR NARAYAN POTHAL', '3000026898'),
(680, 'MAA MANGALA', '3000031962'),
(681, 'MANDAL ENTERPRISES', '3000031929'),
(682, 'SANTOSH KUMAR', '3000009757'),
(683, 'SOBANI CHARAN SWAIN', '3000029451'),
(684, 'SUDANSHU DHIR', '3000030000'),
(685, 'SUKANTA DAS', '3000030317'),
(686, 'SUMANTA NAIK', '3000030705'),
(687, 'AMIT MANDAL', '3000032068'),
(688, 'D.R.G. CONSTRACTION', '3000032048'),
(689, 'RITESH CONSTRUCTION COMPANY', '3000032046'),
(690, 'JAY BALAJI ENTERPRISES', '3000010671'),
(691, 'SACHIN', '3000025816'),
(692, 'SANDEEP PUTTU GUNAGI', '3000024961'),
(693, 'ABUHURERA CONSTRUCTION', '3000015977'),
(694, 'MINTI SHARMA', '3000022873'),
(695, 'VIKAS ENTERPRISES', ' 3000032102'),
(696, 'SHRI VISHNU SAW MILL & TIMBER MERCHANT', '4000000282'),
(697, 'AMBUJA MOHANTY', '3000030410'),
(698, 'ASHI CONSTRUCTION', '3000024651'),
(699, 'BENGAL WATERPROOFING', '3000024819'),
(700, 'DHANBAI NETAM', '3000030532'),
(701, 'GOUR DAS', '3000009998'),
(702, 'I UNIMAX SECURITY PVT LTD', '3000028965'),
(703, 'ISMAIL SK', '3000018631'),
(704, 'MUKESH SINGH', '3000013606'),
(705, 'NAIMUDDIN ANSARI', '3000020868'),
(706, 'NAND LAL PRASAD', '3000014635'),
(707, 'RADHESHYAM VAISHYA', '3000011863'),
(708, 'RAMAUTAR ', '3000007499'),
(709, 'RITESH SHARMA', '3000020153'),
(710, 'ROUT ENTERPRISES', '2000009789'),
(711, 'SHARMA AND  ASSOCIATES PVT LTD', '3000004398'),
(712, 'SURENDRA PRASAD GUPTA', '3000011716'),
(713, 'TIRUPATI CONSTRUCTION', '2000006796'),
(714, 'YASH CONSTRUCTION', '2000006867'),
(715, 'MASUD', '3000028916'),
(716, 'TOSEET', '3000032107'),
(717, 'BAJORIA HOME MAKERS PRIVATE LIMITED', '3000062043'),
(718, 'CHANDESHWAR PRASAD', '3000025121'),
(719, 'JINNAT ALI', '3000032098'),
(720, 'RAKESH KUMAR SHARMA', '3000029966'),
(721, 'SAHU FABRICATION', '3000028602'),
(722, 'SONU KUMAR', '3000032101'),
(723, 'SHAMBHU KUMAR SAHANI', '3000028489'),
(724, 'CIVIL TECH ENGINEERS & CONTRACTOR', '3000026084'),
(725, 'RISHAV SINGH RAJAWAT', '3000032016'),
(726, 'BEAUTY GHOSH ', '3000026616'),
(727, 'MAHARAJ SINGH', '3000029126'),
(728, 'NAND KUMAR', '3000016318'),
(729, 'RANJEET MECHATRONICS LIMITED', '2000010042'),
(730, 'OMEGA CORE CARE', '3000032185'),
(731, 'CCIL', '2000005942'),
(732, 'DHANANJAY KUMAR', '3000027016'),
(733, 'WESTERN INFRABUILD PRODUCTS LLP', '3000030105'),
(734, 'SJPG INFRASTRUCTURE PVT LTD', '3000031719'),
(735, 'MAA KESHARI ENTERPRISE', '3000027917'),
(736, 'SHARMA ENTERPRISES', '3000032138'),
(737, 'RAM BABU YADAV', '3000006869'),
(738, 'MITHU LAL & SONS TENDER CO', '3000031965'),
(739, 'NASEEB ALI', '3000031846'),
(740, 'ASHOK KUMAR PAL', '3000011342'),
(741, 'GOURANG CHANDRA NAYAK', '3000010581'),
(742, 'MAITRI ENTERPRISES', '3000031881'),
(743, 'MURARI SAH', '3000032186'),
(744, 'SANJIV SAV', '3000032187'),
(745, 'SINGH CONSTRUCTION', '3000028412'),
(746, 'PURUSHOTTAM YADAV', '3000029231'),
(747, 'CHATTERJEE FABRICATOR PRIVATE LIMITED', '3000020920'),
(748, 'EAGLE CONSTRUCTION', '3000022286'),
(749, 'BHANU MAHURIA', '3000032229'),
(750, 'YATINIDHI CONST PVT LTD', '3000031357'),
(751, 'SHEKH AFAQUE ALM', '3000005372'),
(752, 'BALA JI CONSTRUCTION', '3000032008'),
(753, 'PRABAL CONSTRUCTIONS', '2000003495'),
(754, 'KAVI FACADE', '2000006912'),
(755, 'ALOK KUMAR YADAV', '3000018741'),
(756, 'RAMESH CHAUDHARI', '3000032236'),
(757, 'MAHESH CHANDRA CONTRACTOR', '3000032268'),
(758, 'MUNENDRA CHAUDHARY', '3000032266'),
(759, 'SANDILYA CONSTRUCTION', '3000032267'),
(760, 'DINESH BIRLA', '3000024212'),
(761, 'NITIN KUMAR AGRAWAL', '3000032233'),
(762, 'BLUE STAR', '3000024027'),
(763, 'EVEREST INDUSTRIES', '2000001622'),
(764, 'MAA MUNDESHWARI CONTRUCTION  ', '3000029650'),
(765, 'MD FIRDOS', '3000013330'),
(766, 'MD AZAD CHOUDHARY', '3000007023'),
(767, 'NS PILES', '3000030828'),
(768, 'RAKESH MISHRA', '3000022817'),
(769, 'RAY ROOFING', '2000010972'),
(770, 'LAXMI WOODS ', '3000010312'),
(771, 'JIAUL HAQUE', '3000032275'),
(772, 'RAVINDRA PRADHAN', '3000000301'),
(773, 'SAMSUL ALAM', '3000032277'),
(774, 'SHIVA ENTERPRISES', '3000028033'),
(775, 'SATISH', '3000028030'),
(776, 'ASHARUL MIR', '3000017837'),
(777, 'NIRANJAN BISWAS', '3000032276'),
(778, 'SUNIL CHOUDHURY', '3000032280'),
(779, 'ADVANCE EPC', '3000010356'),
(780, 'RAJESH SHARMA', '3000026334'),
(781, 'DINESH CHOUDHARY', '3000032274'),
(782, 'JAWID ALAM', '3000032316'),
(783, 'KAMAR AALAM', '3000032265'),
(784, 'DAYANAND TRADERS', '3000032358'),
(785, 'SACHINDRA KUMAR', '3000032357'),
(786, 'MOTIF MOMENTS', '3000032359'),
(787, 'BINDESHWARI YADAV', '3000032356'),
(788, 'DINESH KUMAR MISHRA', '3000019782'),
(789, 'M A CONTRACTOR', '3000032331'),
(790, 'SIRISH CHANDRA MOHANTA', '3000031318'),
(791, 'JAI BHABANI INTERIOR', '3000031608'),
(792, 'JAYSUNDAR VISHWKARMA', '3000032271'),
(793, 'KRISHNA HARIJAN ', '3000032308'),
(794, 'NANDAN KUMAR', '3000032309'),
(795, 'STAGVOLT POWER ', '3000032317'),
(796, 'RAMPRASD LASARRAM VERMA', '3000032364'),
(797, 'PRABHASH ANANDA BANERJEE', '3000004726'),
(798, 'MAA BHAWANI CONSTRCUTION', '3000032183'),
(799, 'THAKUR KUNAL KARN', '3000032301'),
(800, 'MM ENTERPRISES', '3000032224'),
(801, 'SANTOSH KUMAR', '3000032349'),
(802, 'SHREE GANESH CONSTRUCTION', '3000032362'),
(803, 'YADUVASHI CONTRUCTION', '3000004078'),
(804, 'D D ENTERPRISE', '3000031725'),
(805, 'HASHMI CONTRACTOR', '3000032415'),
(806, 'SHALIM SAH', '3000032412'),
(807, 'ABUHURRA CONSTRUCTION COMPANY AND SUPPLIER', 'VFA0034'),
(808, 'AMITA ENGINEERINGS', '3000026305'),
(809, 'DHANJEE PARSAD RAM', '3000032413'),
(810, 'RAJA SONI', '3000017885'),
(811, 'LAXMAN YADAV', '3000003990'),
(812, 'AJAY KUMAR TIWARY', '3000032440'),
(813, 'MUKESH', '3000029906'),
(814, 'RAVINDRA SHARMA', '3000032334'),
(815, 'TANMAY MANDAL', '3000032361'),
(816, 'UMESH PRAJAPATI', '3000032408'),
(817, 'WAZID ALI', '3000032400'),
(818, 'AARYA ENTERPRISES', '3000031467'),
(819, 'MD HABIZUR RAHMAN', '3000028446'),
(820, 'MOHAMMAD JUBER KHAN', '3000028595'),
(821, 'MD ASMAUL SK', '3000032436'),
(822, 'DIVYA NIWAS CONSTRUCTION', '4000000587'),
(823, 'MD AHASAN KHAN ', '3000032291'),
(824, 'A.S. ENTERPRISES', '3000029290'),
(825, 'JAY BALAJI ENTERPRISES', 'VJB0173'),
(826, 'MERAJ KHAN', '3000032438'),
(827, 'GOYAL CONSTRUCTION CO', '3000032442'),
(828, 'JKR CONSTRUCTIONS', '3000032437'),
(829, 'PRERITA PANDEY', '3000032235'),
(830, 'RAVINDRA RAM', '3000032335'),
(831, 'SUDAMA RAM', '3000032336'),
(832, 'MAINUDDIN JAMAL AHMED PASA', '3000030101'),
(833, 'SHRI TRILOK FABRICATION & ENGG WORKS', '3000032399'),
(834, 'ANIRUDHA ROUT', '3000025881'),
(835, 'DINANATH', '3000020717'),
(836, 'HARINARAYAN MAHATO', '3000002486'),
(837, 'JAIGURU ENTERPRISES', '3000032219'),
(838, 'KISHORI RAY', '3000011328'),
(839, 'SUNIL YADAV', '3000029968'),
(840, 'TECHNO INDUSTRIES PVT LTD', '4000000027'),
(841, 'EMPOWER ENGINEEERING CON', '3000022246'),
(842, 'RAJU RAM', '3000031392'),
(843, 'SUJAN BISWAS', '2000009899'),
(844, 'GREEN GLOBAL', '3000003146'),
(845, 'SHREE GAJANAN ENGINEERS', '3000029893'),
(846, 'RAJU CHAUDHARY', '3000032346'),
(847, 'HUMAYUN KABIR', '3000029285'),
(848, 'G S ENTERPRISES', '3000025310'),
(849, 'PRATIBHA ERECTORS LTD', '3000032189'),
(850, 'NEERAJ KUMAR ', '3000032541'),
(851, 'JITENDAR KARMAKAR', '3000032484'),
(852, 'CHAUDHARY FLEX', '3000032540'),
(853, 'JSM FACILITY SERVICES', '3000032449'),
(854, 'PHD ENTERPRISE', '3000032547'),
(855, 'SHIVNATH  SAHANI', '3000028151'),
(856, 'DASRATHI MAHAKUD', '3000032584'),
(857, 'JANO CONSTRUCTION', '3000032585'),
(858, 'PRAVESH YADAV', '3000032586'),
(859, 'AJIT KUMAR', '3000032545'),
(860, 'GAJENDRA YADAV', '3000032572'),
(861, 'JEETENDRA KUMAR', '3000032543'),
(862, 'MOHAMMAD AKHTAR', '3000032546'),
(863, 'ROCCAFORTE ENGINEERING CONSTRUCTION AND CONSULTANCY', '4000000577'),
(864, 'SK JAMFUL', '3000021326'),
(865, 'DEB KUMAR GHANTI', '3000032511'),
(866, 'EJABUL HOQUE', '3000004968'),
(867, 'MANGATURAM CONTRACTOR', '3000023927'),
(868, 'PAPPU KUMAR', '3000032536'),
(869, 'MD SALAUDDIN', '3000024527'),
(870, 'MD GULAM JILANI', '3000032570'),
(871, 'SOHAM ENTERPRISES', '3000030976'),
(872, 'MD MODASSER BADER', '3000032599'),
(873, 'RADIANCE CONSTRUCTION', '2000007834'),
(874, 'ASHISH KUMAR SINGH', '3000032383'),
(875, 'BALAJI CONSTRUCTION CO', '3000032631'),
(876, 'CANTER ENGINEERS PVT LTD', '3000024706'),
(877, 'RAI INFRASTRUCTURE', '3000029903'),
(878, 'SARANGA BEHERA', '3000009779'),
(879, 'GREAT CHETAK SECURITY SERVICES', '4000000611'),
(880, 'ALLIED INDUSTRIES', '3000014969'),
(881, 'SHAHNAWAZUL HAQUE', '3000032669'),
(882, 'RISHABH DEV SAHU', '3000031175'),
(883, 'SHIV KUMAR YADAV', '3000032667'),
(884, 'BBR INDIA PRIVATE LIMITED', '2000000278'),
(885, 'HALFEN MOMENT INDIA PRIVATE LIMITED', '2000006339'),
(886, 'SHAMKANT MULCHAND PATIL', '3000032034'),
(887, 'MM CONSTRUCTION', '3000031565'),
(888, 'VELAN BUILDERS', '3000028896'),
(889, 'ABHIJAY CONSTRUCTION', '3000032096'),
(890, 'S K INFRA', '3000028661'),
(891, 'ZIAUL HOQUE', '3000031769'),
(892, 'K P ENTERPRISE', '2000001261'),
(893, 'INDRATECH INFRA PROJECTS', '4000000584'),
(894, 'TVC CONCRETES', '2000011584'),
(895, 'POSHS CINOTI PRIVATE LIMITED', '4000000386'),
(896, 'SIDDHNATH ENTERPRISE', '3000010179'),
(897, 'GOBINDA BHAKTA', '3000032568'),
(898, 'FAIYAZ AHMAD', '3000032675'),
(899, 'SHREE SAMARTH ENTERPRISES', '3000032676'),
(900, 'DREAM ENTERPRISES', '4000000619'),
(901, 'MAMPI PANKAJ DAS', '3000032725'),
(902, 'PANLOOB INTERNATIONAL', '4000000618'),
(904, 'RANJIT KUMAR', '3000032746'),
(905, 'BIRENDER KUMAR SINGH', '3000014661'),
(906, 'KHUSHBOO DEVI', '3000032715'),
(907, 'PHULESHWAR MUKHIYA', '3000010640'),
(908, 'MAHIRUL ISLAM', '3000032694'),
(909, 'TILAK DAS', '3000029124'),
(910, 'HEM CONSTRUCTION', '3000032762'),
(911, 'DINESHWAR RAM', '3000032801'),
(912, 'VINOD RAM', '3000005462'),
(913, 'ANANT KUMAR MALLIK', '3000032766'),
(914, 'KRISHNA CONSTRUCTION & CO', '3000032768'),
(915, 'STEP UP CONSTRUCTION', '3000020858'),
(916, 'CHANCHAL BISWAS', '300032800'),
(917, 'SANCTUM INFRA PROJECT', '3000003846'),
(918, 'KADAWAJI', '3000032742'),
(919, 'SANTOSH SHARMA', '3000013238'),
(920, 'MOTHER MARIA ENTERPRISES', '3000032227'),
(921, 'AKHLESH KUMAR', '3000032105'),
(922, 'KRISHNA INTERIORS', '3000026990'),
(923, 'AHEDA BIBI', '3000032559'),
(924, 'HMM INFRA LTD', '2000011460'),
(925, 'KAVITA MISHRA', '3000004145'),
(926, 'ACME MGMT  SOLUTIONS VALUE ENG', '4000000648'),
(927, 'K.D. BUILDWAY CONSTRUCTION PVT LTD', '3000032765'),
(928, 'THREE STARLABOR POOLSOLUTIONS P LTD', '3000032740'),
(929, 'SANJAY PANDIT', '3000032764'),
(930, 'ABDUL QUAIYAM', '3000024441'),
(931, 'UTTAM DAS', '3000024046'),
(932, 'BALRAM SAMAL', '3000032596'),
(933, 'NS CONSTRUCTION', '4000000632'),
(934, 'SHYAMBABU DHANAI GUPTA', '3000010585'),
(935, 'SHARMA & SHARMA COMPANY', '3000032922'),
(936, 'NIRANJAN NAYAK ', '3000029577'),
(937, 'SHRI DEV ENGINEERING', '3000032838'),
(938, 'RINA DEVI', '3000032821'),
(939, 'MUKESH KUMAR', '3000028945'),
(940, 'BARKAT ALI', '3000032917'),
(941, 'MITHLESH', '3000032871'),
(942, 'PRADEEP KUMAR', '3000032793'),
(943, 'PUNIT MUKHIYA', '3000032794'),
(944, 'RAMESH PANDIT', '3000032763'),
(945, 'SABINA KHATUN', '3000032918'),
(946, 'VIJAY KUMAR', '3000032884'),
(947, 'KABITA DEVI', '3000032924'),
(948, 'SHIROMANI VERMA', '3000003928'),
(949, 'SAMBHU CHARAN DAS', '3000011190'),
(950, 'AIROCHEM ENGINEERING COMPANY', '2000010363'),
(951, 'HAKIM', '3000032952'),
(952, 'MKRAP INFRACON PRIVATE LIMITED', '3000024524'),
(953, 'NUR SELU', '3000018650'),
(954, 'RAMPRAVESH RAM', '3000032979'),
(955, 'SUBHASH MANJHI', '3000032978'),
(956, 'MAA SHANTI ENTERPRISES', '3000032977'),
(957, 'S.K. CONSTRUCTION', '3000032980'),
(958, 'MOHAMMAD AKHTAR ALI ', '3000032633'),
(959, ' ASHOK NAMDEO DAKARKE', '3000032177'),
(960, 'PRABODH MANDAL', '3000032945'),
(961, 'MANOJ KUMAR DAS', '3000032944'),
(962, 'PAYEL CONSTRUCTIONS', '3000032991'),
(963, 'MOTILAL KUSHWAHA', '3000028367'),
(964, 'JIARUL RAHAMAN', '3000006632'),
(965, 'ASHOK MANJHI', '3000033027'),
(966, 'PINKI DEVI', '3000032332'),
(967, 'ANUJ TIWARI', '3000032816'),
(968, 'SURESH TIRAPPA CHANDRA', '3000018604'),
(969, 'SUREKHA SARJERAO KASOTE', '3000032875'),
(970, 'SATANA', '3000033058'),
(971, 'BHAGWATI CONSTRUCTIONS', '3000033057'),
(972, 'LOKESH MANDAL', '3000032885'),
(973, 'NUR MOSTOFA', '3000033177'),
(974, 'SINI ENGINEERING', '3000033179'),
(975, 'SANJAY MAHATO', '3000008015'),
(976, 'TRIDEV ENTERPRISES', '4000000695'),
(977, 'ABHISHEK SINGH', '3000033134'),
(978, 'SHARMA CONSTRUCTION', '3000010308'),
(979, 'RAINCATCHER ENGG & COATING (OPC)', '3000029151'),
(980, 'BUILDS MORE', '3000033039'),
(981, 'SUKUMAR MUNIYAN', '3000032983'),
(982, 'GOPINATH NAYAK', '3000008215'),
(983, 'MOHD HASIBUL', '3000033074'),
(984, 'MAKSOODAN SINGH', '3000033060'),
(985, 'NEERAJ KUMAR SHARMA', '3000033122'),
(986, 'VIJAY KUMAR SAH', '3000033123'),
(987, 'SOHARAB', '3000032869'),
(988, 'ARUN KUMAR', '3000032916'),
(989, 'SURESH MUKHUIYA', '3000033124'),
(990, 'SHIVPOOJAN', '3000033219'),
(991, 'RKTECT CONSTRUCTIONS PRIVATE LIMITED', '4000000615'),
(992, 'USHA AUTO ENGINEERS', '2000000679'),
(993, 'TARUN SHARMA', '3000029648'),
(994, 'SWATI ENTERPRISES', '3000025038'),
(995, 'DINESH YADAV', '3000033145'),
(996, 'RAMBABU SHIVKUMAR KUSHWAHA', '3000033064'),
(997, 'SURENDRA YADAV', '3000033063'),
(998, 'RAJMUNI VISHWAKARMA', '3000032904'),
(999, 'SULTAN ALI', '3000033211'),
(1000, 'KMNV INFRATECH PRIVATE LIMITED', '4000000654'),
(1001, 'DEVLAL KUSHWAH', '3000023743'),
(1002, 'JITENDRA CHAUHAN', '3000033237'),
(1003, 'UDAY KUMAR', '3000025388'),
(1004, 'HASMUDLN ABUL HUSAIN SHEKH', '2000009897'),
(1005, 'S.K.ENGINEERING', '3000026809'),
(1006, 'SUKESH BISWAS', '3000021755'),
(1007, 'POWER PRO INFRASTRUCTURE', '3000030428'),
(1008, 'RAJIV RANJAN KUMAR', '3000027422'),
(1009, 'KAILASH SINGH KAUSHIK', '3000031762'),
(1010, 'SHANTARAM SHIMPI', '3000016306'),
(1011, 'SUNDAR SHARMA', '3000010678'),
(1012, 'CHOTELAL RAM NARESH GUPTA', '3000028270'),
(1013, 'J.K. ENGINEERING', '3000033307'),
(1014, 'VIJAY SHANKAR YADAV', '3000033135'),
(1015, 'GYANCHAND', '3000004672'),
(1016, 'MOHD JUBER KHAN', '3000033344'),
(1017, 'HOSSAIN SHAIKH', '3000033343'),
(1018, 'BAPI PRAMANIK', '3000033115'),
(1019, 'ADITYA LAYEK', '3000010770'),
(1020, 'SANTOSH KUMAR 3', '3000033339'),
(1021, 'PAYEL CONSTRUCTIONS 2', '3000033338'),
(1022, 'DIPANKAR BISWAS', '3000033337'),
(1023, 'ANIL MEHATA', '3000033324'),
(1024, 'J.N.C. & COMPANY', '3000033377'),
(1025, 'MOHD FARUKH ALI', 'VFA0014'),
(1026, 'OM PRAKASH SINGH', '3000033325'),
(1027, 'RUHI PRAWEEN', '3000033305'),
(1028, 'JITENDRA  PANNALAL', '3000006310'),
(1029, 'MOTIN MANDAL', '3000033394'),
(1030, 'GUPTA CONSTRUCTION', '3000033372'),
(1031, 'AJAY RAJAK', '3000033341'),
(1032, 'D P ASSOCIATE AND CONSULTANT', '3000033120'),
(1033, 'JAFAR ALI', '3000033340'),
(1034, 'ASHOK CHAUBEY', '3000033435'),
(1035, 'BIBI NISAD ANJUM SHAIKH', '3000033414'),
(1036, 'BATU BHUYAN', '3000033410'),
(1037, 'DARSHAN CONSTRUCTION', '3000033062'),
(1038, 'JAHANGIR MALLICK', '3000033419'),
(1039, 'MD.HOSSAIN ALI MANDAL', '3000020854'),
(1040, 'SEFA ELECTRICAL', '3000025764'),
(1041, 'SHIV KUMAR GUPTA', '3000033417'),
(1042, 'SHIV SHAKTI STEEL FAB', '3000033210'),
(1043, 'SUGAD KANDULNA', '3000033418'),
(1044, 'N.R.CONSTRUCTION', '3000028289'),
(1045, 'ENTITY SERVICES PRIVATE LIMITED', '3000033304'),
(1046, 'RAZA CONSTRUCTION', '3000033452 '),
(1047, 'IKARAR', '3000006006'),
(1048, 'JITAN KUMAR', '3000004766'),
(1049, 'BULTI MANDAL', '3000033405'),
(1050, 'ISHWAR SINGH', '3000033306'),
(1051, 'JOYESH H JELE', '3000033413'),
(1052, 'STRESSTECH ENGINEERS PRIVATE LIMITED', '2000005219'),
(1053, 'HARENDRA SHIVNATH RAM', '3000033412'),
(1054, 'HAQUE MAHABUL AHIYA', '3000033496'),
(1055, 'S.P.ASSOCIATES', '3000031592'),
(1056, 'MAHFUJ KHAN', '3000033373'),
(1057, 'AL SAPPHIRE FAÇADE PVT LTD', '3000026288'),
(1058, 'J K ENTERPRISES', '3000033535'),
(1059, 'KAMAL HOUSSAIN', '3000033527'),
(1060, 'SUNIL THAKUR', '3000016282'),
(1061, 'PRASANTA KUMAR NAYAK', '3000033528'),
(1062, 'BIKASH KUMAR SHA', '3000023139'),
(1063, 'CLEVER SECURITY SERVICES & INTELLIGENCE', '3000033749'),
(1064, 'K D ENTERPRISE', '3000030229'),
(1065, 'MAHABUL ISLAM', '3000032802'),
(1066, 'MD IMRAN ALI', '3000032925'),
(1067, 'SITEN ROY', '3000032820'),
(1068, 'PAPON DAS', '3000032744'),
(1069, 'KRISHNA CONSTRUCTION', '3000031939'),
(1070, 'CHHOTAN DAS', '3000032220'),
(1071, ' KAVLES PRESSURE CONCRETE PVT. LTD', '4000000665'),
(1072, 'SONU LAL', '3000033530'),
(1073, 'MOHAMMAD SHAJIR HUSSAIN', '3000033620'),
(1074, 'RAZA CONSTRUCTION', '3000026664'),
(1075, 'SHYAM SUNDAR RAM', '3000018557'),
(1076, 'MARAN CONSTRUCTION', '3000032506'),
(1077, 'KRISHNA ENGINEERING WORKS', '3000033201'),
(1078, 'JHARNA CONSTRUCTION', '3000033538'),
(1079, 'NARESH KUMAR YADAV', '3000033642'),
(1080, 'NRUSINGHA SWAIN', '3000033591'),
(1081, 'R C ENGINEERING', '3000033595'),
(1082, 'SHREERAM CONSTRUCTION', '3000033622'),
(1083, 'JAKIR HOSSAIN', '3000033462'),
(1084, 'MD ASRAFUL HOQUE', '2000004366'),
(1085, 'VIJAY PRAJAPATI CIVIL CONTRACTOR', '3000033593'),
(1086, 'RUPESH KUMAR GUPTA', '3000033594'),
(1087, 'TRANSINDIA TECHNOLOGIES', '4000000732'),
(1088, 'NURUL HODA', '3000033668'),
(1089, 'ROHAN SURESH BHADKUMBE', '3000033667'),
(1090, 'SHRIKANT PANDIT', '3000033246'),
(1091, 'BIJAY KUMAR YADAV', '3000014231'),
(1092, 'SHEKH NAJIM', '3000033247'),
(1093, 'PAPPU SAH', '3000033061'),
(1094, 'SAJENDRA YADAV', '3000014729'),
(1095, 'SATYANARAYAN CHAUDHARY', '3000033283'),
(1096, 'RAJESH MANJHI', '3000033282'),
(1097, 'MD SAHEB ALI', '3000033681'),
(1098, 'ASMA KHATUN', '3000033564'),
(1099, 'NEELKANTH CONSTRUCTION', '3000033285'),
(1100, 'MOHD ASHIF', '3000009071'),
(1101, 'MD ABDUL HALIM', '3000033669'),
(1102, 'SAI ENTERPRISES', '3000033533'),
(1103, 'AIR KRIPA', '3000022816'),
(1104, 'LAXMI AND KANTI G TECH SOLUTIONS', '3000030226'),
(1105, 'MD MOTALAB SEKH', '3000033827'),
(1106, 'INDIAN OCEAN SERVICES', '3000033828'),
(1107, 'CCD TECHNICAL SOLUTIONS PVT LTD', '3000021247'),
(1108, 'S.R.ENTERPRISES', '3000023984'),
(1109, 'AAKASH CONSTRUCTION & INTERIOR', '3000022486'),
(1110, 'S R ENTERPRISES', '3000033869'),
(1111, 'P R & CO', '3000029784'),
(1112, 'S M JINNAH', '3000031988'),
(1113, 'SHIB SING RAY', '3000033624'),
(1114, 'SWATHI BUILD TECH', '4000000682'),
(1115, 'SFD ENGINEERING', '40000000734'),
(1116, 'KAVYA ENTERPRISES', '3000033902'),
(1117, 'SEKH SAMIM', '3000033867'),
(1118, 'SANIVARAPU RAM REDDY', '3000033866'),
(1119, 'MOHAMMAD BABUDDIN ANSARI', '3000032525'),
(1120, 'AKANKSHA ELECTRICAL', '4000000726'),
(1121, 'GORELAL CHAUDHARY', '3000033784'),
(1122, 'PRABHU ENTERPRISE', '3000033913'),
(1123, 'RBP ENTERPRISES', '3000033912'),
(1124, 'AISHA ENTERPRISES', '3000023006'),
(1125, 'RAJESH RAMNATH PANDEY', '3000033901'),
(1126, 'ATAUR RAHMAN', '3000033934'),
(1127, 'MADHUSUDAN MANDAL', '3000033936'),
(1128, 'MD ZISAN', '3000033937'),
(1129, 'ABSOLUTE ASSOCIATE WATER PROOFING', '4000000783'),
(1130, 'CMC PROJECTS (INDIA) PVT LTD', '3000033893'),
(1131, 'PRADEEP PRASAD SAV', '3000033898'),
(1132, 'MANOJ KUMAR YADAV-2', '3000033896'),
(1133, 'UNIMAPLE MODUTECH PVT LTD', '4000000684'),
(1134, 'JAIMATADI CONSTRUCTION', 'JAIMATADI CONSTRUCTION'),
(1135, 'BABLU KEER', '3000033770'),
(1136, 'SUSHILA KUMARI', '3000033884'),
(1137, 'MD HABIB SK', '3000033680'),
(1138, 'S H ENTERPRISES', '3000028284'),
(1139, 'NCL BUILDTEK LTD', '2000007281'),
(1140, 'GAUTAM BISWAS', '3000034064'),
(1141, 'DILIP KUMAR SWAIN', '3000034004'),
(1142, 'BABLU KUMAR MAITI', '3000033961'),
(1143, 'SANTOSH KUMAR SONI', '3000033851'),
(1144, 'PRADEEP AMRESH CHAUPAL', '3000034105'),
(1145, 'MATHI CONSTRUCTION', '3000032928'),
(1146, 'ELITE DETECTIVE AND SECURITY SERVICES', '30000302011'),
(1147, 'MITHU CONSTRUCTION', '3000033960'),
(1148, 'NISHANT ASSOCIATES', '2000005878'),
(1149, 'PUSHPA CONSTRUCTION COMPANY', '3000028493'),
(1150, 'SHUBHAM VISHWAKARMA', '3000034122'),
(1151, 'ROSHAN ENTERPRISES', '3000034104'),
(1152, 'KOUSIK SIL', '3000034103'),
(1153, 'VIJAY SHANTI CONSTRUCTION', '3000034148'),
(1154, 'ARCSONS CONSTRUCTION', '3000034102 '),
(1155, 'SHREE BALAJI CONSTRUCTION', '3000033801'),
(1156, 'GUNJAN SAFETY SERVICES', '4000000852'),
(1157, 'DEVI DAYAL KUMAR MEHTA', '3000034186'),
(1158, 'RAMKUMAR GUPTA', '3000022051'),
(1159, 'LAXMAN BARUI', '3000032544'),
(1160, 'RAHAMAT KHAN', '300000039'),
(1161, 'MD SHAHABUDDIN', '3000001048'),
(1162, 'RAJANI', '3000034161'),
(1163, 'SHREE AMBEY PACKAGING', '4000000842'),
(1164, 'RADHE SHYAM', '3000034234'),
(1165, 'ABDUL SAMAD SEKH', '3000034162'),
(1166, 'SAK SUPERSTRUCTURE PVT LTD', '4000000855'),
(1167, 'BANMALI ', '3000034194'),
(1168, 'RAM BHAJAN PRAJAPATI', '3000016292'),
(1169, 'RASMI RANJAN NAYAK', '3000034211'),
(1170, 'KALINDRA PRAJAPATI', '3000034108'),
(1171, 'AYUSH GANJEER', '3000034193'),
(1172, 'DEBASHREE SOLUTIONS', '3000034157'),
(1173, 'REENA LATHISH UCHIL', '3000034260'),
(1174, 'ORISON COMMUNICATION', '3000045765'),
(1175, 'SANTOSH PRAJAPATI', '3000034212'),
(1176, 'AMIT GUPTA', '3000014001'),
(1177, 'MUNNA KUMAR SAH', '3000034298'),
(1178, 'PARAS IMPEX', '3000031013'),
(1179, 'SHAMIM AKHTAR', '3000034230'),
(1180, 'UNIQUE ENGINEERS', '4000000716'),
(1181, 'LAXMI ENTERPRISES', '3000034318'),
(1182, 'AJAY SAHA', '3000034163'),
(1183, 'SHEELU SINGH', '3000034226'),
(1184, 'MANASI ENTERPRISES', '3000033966'),
(1185, 'TOM SOY CONSTRUCTION', '3000034006'),
(1186, 'MUNNA PRASAD MEHETA', '3000033967'),
(1187, 'PIYALI SARKAR', '3000034133'),
(1188, 'PABAN GORAI', '3000033887'),
(1189, 'NASEEM', '3000009688'),
(1190, 'SUMAN KUMAR', '3000029818'),
(1191, 'SHATENDRA KUMAR', '3000034289'),
(1192, 'ADITYA PROFILES PVT LTD', '3000024072'),
(1193, 'JC ENTERPRISES', '2000003361'),
(1194, 'PMJD PROJECTS', '3000016082'),
(1195, 'HAFIJUL ANSARI', '3000030041'),
(1196, 'JMC PROJECTS (I) LTD', 'JMC PROJECTS (I) LTD'),
(1197, 'SEEYATI CONSTRUCTION PVT LTD', '3000034353'),
(1198, 'ADITI INTEGRATED SERVICE SOLUTION PVT LTD', '3400034354'),
(1199, 'AKT INFRATECH PVT LTD', '2000010855'),
(1200, 'INTERNATIONAL SECURITY ORGANISATION', '4000000580'),
(1201, 'ABDUL BARI', '3000035615'),
(1202, 'N I ENTERPRISE ', '3000034297'),
(1203, 'SURANDAR RAY', '3000026244'),
(1204, 'SUTAPA BHOWMICK', '3000034214'),
(1205, 'TUHIN CONSTRUCTION', 'TUHIN CONSTRUCTION'),
(1206, 'RIDHI SIDDHI ENGINEERING', '3000034441'),
(1207, 'MANOHAR MASHNAJI MOTIKAR', '3000034434'),
(1208, 'SOMYA ELECTRICALS', '3000029623'),
(1209, 'BHOLAM SINGH', '3000000870'),
(1210, 'PRABIR KUMAR TRIYA', '3000029952'),
(1211, 'ABHINASH BISWAS', '3000034030'),
(1212, 'KULDEEP KUMAR PATEL', '3000022206'),
(1213, 'SINGH ELECTRICAL WORKS', '3000034380'),
(1214, 'VAISHNAVI ENTERPRISES ', '3000034522'),
(1215, 'RAM DAYAL', '3000034535'),
(1216, 'BARUN ADHAKARI', '3000026181'),
(1217, 'SANAKA HALDER', '3000034493'),
(1218, 'RUPAM ENTERPRISE', '3000034564'),
(1219, 'MEENA DEVI      ', '3000034443'),
(1220, 'MANJUDEVI SHAH', '3000034527'),
(1221, 'MULTI TECH SPLICING', '3000026632'),
(1222, 'SANGEETA', '3000034382'),
(1223, 'SUBH HOME SOLUTION', '3000034444'),
(1224, 'RAJKUMAR PANDIT', '3000034562'),
(1225, 'ALIFYA CONSTRUCTION', '3000035765'),
(1226, 'INJAM INFRA STRUCTURE', '3000034732'),
(1227, 'MUKESH SAH', 'MUKESH SAH'),
(1228, 'AM ENTERPRISES ', '3000034625'),
(1229, 'A TO Z INTERIOR DECORATORS', '3000034482'),
(1230, 'AHMAD RZA', '3000034525'),
(1231, 'ANSHU KUMAR MISHRA', '3000034509'),
(1232, 'ASHOK KUMAR DAS', '3000034254'),
(1233, 'BADRI PRASAD SAHU', '3000034255'),
(1234, 'CHHOLE LAL', '3000034350'),
(1235, 'HAFIJUL ALI', '3000034510'),
(1236, 'MOHAMMAD ASLAM', '3000032549'),
(1237, 'NASIRUDDIN ANSARI', '3000034546'),
(1238, 'NEK KHATUN', '3000034490'),
(1239, 'RABINDRA NATH DALUI', '3000034393'),
(1240, 'SHADDAM ANSARI', '3000033514'),
(1241, 'UDAY KUMAR', '3000034258'),
(1242, 'BHAIYA LAL DIWEDI', '3000034428'),
(1243, 'PNADIT ENTERPRISES', '3000033136'),
(1244, 'PANKAJ KUMAR', '3000034361'),
(1245, 'ASIDUL HAQUE', '3000034533'),
(1246, 'NAYYAR ALI', '3000034505'),
(1247, 'SANJEEV KUMAR', '3000034607'),
(1248, 'RAJ KUMAR SINGH', '3000034474'),
(1249, 'CHENTARA BIBI', '3000034068'),
(1250, 'LAXMI WOOD PRODUCTS', '2000010312'),
(1251, 'QUALICOM SOLUTION PVT LTD', '4000000154'),
(1252, 'SAMIR KHAN', '4200019217'),
(1253, 'G.S.ROY CONSTRUCTION', '3000034880'),
(1254, 'MD AHMAD HUSSAIN', '4200019339'),
(1255, 'H R INFRA', '3000025542'),
(1256, 'RANITA CONSTRUCTION', '3000034683'),
(1257, 'KARTICK BISWAS & CO', '4200019328'),
(1258, 'MANOHAR C CHOUDHURY', '3000027280'),
(1259, 'NAZIR HUSSAIN', '3000001281'),
(1260, 'A H ENTERPRISES', '3000034623'),
(1261, 'MUNAR DEVI', '3000034565'),
(1262, 'V.S. BUILDCON', '3000034719'),
(1263, 'RAHISUDDIN', '3000034731'),
(1264, 'ASHOK KUMAR VYAS', '3000034737'),
(1265, 'LALLAN ENTERPRISES', '3000034735'),
(1266, 'COMPLETE INTERIOR DECORATORS GROUP', '3000027624'),
(1267, 'FIROZ KHAN', '3000034783'),
(1268, 'RAMESH KUMAR CHAURASIYA', '3000034773'),
(1269, 'MAINIGAR', '3000034772'),
(1270, 'AASH ENTERPRISES', '3000034774'),
(1271, 'RAJ CONSTRUCTION', '3000034734'),
(1272, 'ANJU JHA', '3000034659'),
(1273, 'JAYMATI DEVI', '3000034352'),
(1274, 'SAMATALI SHEKH', '3000034344'),
(1275, 'M K CONSTRUCSTION', '3000033205'),
(1276, 'VIYO CONSTRUCTION', '3000003661'),
(1277, 'SEKH SAKUR', '3000034848'),
(1278, 'AMIT GUPTA', ' 3000034435');
INSERT INTO `oc_contractor` (`contractor_id`, `contractor_name`, `contractor_code`) VALUES
(1279, 'DINESH CHAUDHARY', '3000034660'),
(1280, 'M I ENGINEERS', '3000015341'),
(1281, 'ABHINASH KUMAR', '3000034685'),
(1282, 'SAHID CONSTRUCTION', '3000034890'),
(1283, 'MASUD RANA', '3000034696'),
(1284, 'MADAN PRADHAN', '3000034832'),
(1285, 'RANJIT MANDAL', '3000034779'),
(1286, 'SANJAY KUMAR SINGH', '3000034891'),
(1287, 'CORE CARTE GLOBAL SARVICE', '4000000846'),
(1288, 'SITARA DEVI', '3000034885'),
(1289, 'ASHOK KUMAR', '3000034884'),
(1290, 'ASHARFI SINGH', '3000034925'),
(1291, 'SUKUMAR BISWAS', '3000035173'),
(1292, 'BIKASH KUMAR SAH', '300023139'),
(1293, 'RENU DEVI', '3000034879'),
(1294, 'AMIT KUMAR', '3000034932'),
(1295, 'SURESH RAI', '3000027766'),
(1296, 'ASHOK KUMAR SUTHAR', '3000034973'),
(1297, 'VINAYAK CONSTRUCTION', '4000001034'),
(1298, 'NAJIM ANSARI', '3000033248'),
(1299, 'MOHD NAZEEM', '3000034856'),
(1300, 'GOKUL KUSHWAHA', '3000034924'),
(1301, 'MD NAKI', '3000013116'),
(1302, 'ARBIND MANDAL', '3000034926'),
(1303, 'RAMESH YADAV', '3000034935'),
(1304, 'RATHINDRANATH DAN', 'RATHINDRANATH DAN'),
(1305, 'ABDUL SUBHAN', '3000034972'),
(1306, 'DILEP KUMAR GUPTA', '3000021262'),
(1307, 'PUNAM DEVI', '3000035095'),
(1308, 'SANTOSH KUMAR', 'SANTOSH KUMAR'),
(1309, 'NOVATEC INDUSTRIAL', '3000034394'),
(1310, 'ADARSH ROCK DRILL', '3000015926'),
(1311, 'SHREERAM CONSTRUCTION 2', '3000034598'),
(1312, 'SUHANA CONSTRUCTION', '3000034892'),
(1313, 'AHAMD HUSSAIN', '3000007943'),
(1314, 'ASHOK KUMAR', '3000035146'),
(1315, 'MAHELKA KHATUN', '3000035132'),
(1316, 'SAFINA KHATOON', '3000035131'),
(1317, 'PARAPILLAR PROJECT MANAGEMENT', '3000035092'),
(1318, 'ABHISEKH KUMAR NAIK', '3000034904'),
(1319, 'MD ANISUL', '3000035177'),
(1320, 'AMBWARISH DEBNATH', 'AMBWARISH DEBNATH'),
(1321, 'DILIP RAI', '3000009755'),
(1322, 'J S ALAM AND CO', '3000035110'),
(1323, 'AZEEM', '3000017623'),
(1324, 'GANESH MANDAL', '3000035176'),
(1325, 'AMCON CONSTRUCTIONS', '3000018509'),
(1326, 'SUNIL KUMAR', '3000035184'),
(1327, 'MAMTA DEVI', '3000035202'),
(1328, 'ABUL KALAM', '3000035203'),
(1329, 'SOFIAS CONSTRUCTION', '3000034722'),
(1330, 'NIRMAL KUMAR BHAGAT', '3000035000'),
(1331, 'VELANKANI MACHINE CRAFT PVT. LTD.', '4000001016'),
(1332, 'SEKH INTEJAR', '3000035278'),
(1333, 'DIPANKAR PRAMANIK', '3000035065'),
(1334, 'DWIPJAY GHOSH', '3000035066'),
(1335, 'BIBI DARAKSHA', '3000035260'),
(1336, 'REKHA', '3000035185'),
(1337, 'GOPINATH NAYAK', '3000024748'),
(1338, 'YAA SECURITY PRIVATE LIMITED', '3000035245'),
(1339, 'NEHAL', '3000009878'),
(1340, 'RAJARAM', '3000033872'),
(1341, 'MOHD SHABUDDIN', '3000035259'),
(1342, 'SHESH PAL', '3000035314'),
(1343, 'RATNESH KUMAR', '3000035342'),
(1344, 'RAJBAHOR SINGH', '3000035354'),
(1345, 'AMIT KUMAR PANDEY', '3000034784'),
(1346, 'ANSAR AHMED KHAN', '3000027845'),
(1347, 'AYUSH ENTERPRISES', '3000034775'),
(1348, 'IMRAN', '3000034810'),
(1349, 'JABIR ANSARI', '3000034776'),
(1350, 'MD HASAN', '3000015143'),
(1351, 'MD. ALAM SK', '3000035249'),
(1352, 'NEHA WATER PROOFING', '3000034301'),
(1353, 'NOVO MODULAR FURNITURE INDIA(P) LTD', '3000033434'),
(1354, 'OM INDIA STAINLESS TUBE LIMITED', '2000011184'),
(1355, 'PAPIA KHATUN', '3000034974'),
(1356, 'SABITA GOYAL', '3000034684'),
(1357, 'SEEMA YADAV', '3000034196'),
(1358, 'SHAMBHU NATH SINGH', '3000034697'),
(1359, 'SOHAN LAL SAHU', '3000035251'),
(1360, 'YUVRAJ GANJIR', '3000035215'),
(1361, 'MORESHWAR JAGOJI WANJARI', '3000035299'),
(1362, 'ONKAR AAJURAM KAUSHIK', '3000033897'),
(1363, 'HARSH PILING', '3000029134'),
(1364, 'AVDOOT ENTERPRISE', '3000031761'),
(1365, 'NAVI HASAN', '3000034903'),
(1366, 'JAIMATADI CONSTRUCTION', '4000000905'),
(1367, 'ULTRA POWER CONSTRUCTION', '3000024960'),
(1368, 'SHYAM LAL', '3000035397'),
(1369, 'NILESH GAVLI', '3000004203'),
(1370, 'SANOJ KUMAR', '3000035475'),
(1371, 'ABDHESH KUMAR', '3000035449'),
(1372, 'USMAN MIYA', '3000016694'),
(1373, 'UMESH DAS', '3000035284'),
(1374, 'BHARMDEV PAL', '3000035376'),
(1375, 'GAGANDEEP SINGH', '3000034992'),
(1376, 'BENGAL WATER PROOFING CO', '3000021158'),
(1377, 'ISWAR KASHYAP', '3000035417'),
(1378, 'K KAUSHLESH DUBEY', '3000035448'),
(1379, 'SUBHAGINI GIRI', '3000035474'),
(1380, 'KAMAR INFRASTRUCTURE PVT LTD', '3000035039'),
(1381, 'RUPESH KUMAR', '3000035521'),
(1382, 'SARDHA DEVI', '3000035477'),
(1383, 'CHANDAN KUMAR', '3000035470'),
(1384, 'SAYAN BANERJEE', '3000035528'),
(1385, 'MD ARSHAD ANSARI', '3000035626'),
(1386, 'DRILLERS & ENGINEERS', '400001017'),
(1387, 'SHIKHA CONSTRUCTION', '3000029673'),
(1388, 'SHAMBHU', '3000007495'),
(1389, 'R G CHANDAK', '3000033625'),
(1390, 'SAGAR SANGRAM DEO', '3000026205'),
(1391, 'DEVI DAYAL', '3000035559'),
(1392, 'LOKNATH BISWAL', '3000035519'),
(1393, 'BINESH THAKUR', '3000035596'),
(1394, 'MULTI DECOR INDIA PVT LTD', '3000018386'),
(1395, 'MITHEW & MITTAL INFRA SOL', '2000012116'),
(1396, 'SUMIT BASISTA', '3000035533'),
(1397, 'HARPREET SINGH MAAN', '3000035656'),
(1398, 'EVERGREEN CONSTRUCTION', '3000035740'),
(1399, 'SANFIELD INDIA LTD', 'SANFIELD INDIA LTD'),
(1400, 'R G AND COMPANY', '3000035627'),
(1401, 'PARVIN KUMAR RAY', '3000035496'),
(1402, 'ANIKUL HOQUE', '3000035497'),
(1403, 'SHRI BHAN', '3000022117'),
(1404, 'K S BISHT', '3000021886'),
(1405, 'MANIRUL ISLAM', '3000035820'),
(1406, 'MOHAMMAD FARUK', 'MOHAMMAD FARUK'),
(1407, 'SK AZIBUL', 'SK AZIBUL'),
(1408, 'PREM LAL YADAV', '3000035652'),
(1409, 'RAJ KUMAR SINGH', '3000035653'),
(1410, 'GOPAL NURSARY', '3000022961'),
(1411, 'KAILASH KAHAR', '3000035476'),
(1412, 'MIRA MONDAL', '3000035520'),
(1413, 'PREETAM SINGH PRAJAPATI', '3000029971'),
(1414, 'SUHAIL AHAMED KHAN', '3000035446'),
(1415, 'PREETAM SINGH PRAJAPATI', '3000029978'),
(1416, 'ASHWANI TYAGI', '3000035693'),
(1417, 'HIMMAT SINGH', '3000002740'),
(1418, 'SAHIL POWER SYSTEMS', '2000011200'),
(1419, 'A S K & CO', '3000035690'),
(1420, 'UPENDER RAY', '3000035557'),
(1421, 'LALIT CHAUHAN', '3000035692'),
(1422, 'MD RAFIKUL SK', '3000035713'),
(1423, 'SANJEET KUMAR SHARMA', '3000035714'),
(1424, 'MOHAMMAD SUFIYAN', '3000035680'),
(1425, 'PANKAJ KUMAR GUPTA', '3000035706'),
(1426, 'LAAP CONSTRUCTION PVT LTD', '2000010125'),
(1427, 'FEERAN', '3000035498'),
(1428, 'MADHU SUDHAN PANIGRAHI', '3000035802'),
(1429, 'SADA KASHYAP', '3000035744'),
(1430, 'JOY MAA KALI ENTERPRISE', 'JOY MAA KALI ENTERPRISE'),
(1431, 'LAMBDA EASTERN TELE. LTD', 'LAMBDA EASTERN TELE. LTD'),
(1432, 'JYOTI SHIVAJI KATTIMANI', '3000036039'),
(1433, 'ELEGANT & ALLIED SECURITY SERVICES', '3000036136'),
(1434, 'SK MOBARAK', 'SK MOBARAK'),
(1435, 'CEC CONSTRUCTION AND SKILL', '3000035745'),
(1436, 'MOHAN', '3000035803'),
(1437, 'SATISH CHANDRA', '3000035830'),
(1438, 'ANIL BHATI', '3000035798'),
(1439, 'SANJAY PASWAN', '3000035799'),
(1440, 'LEKH RAJ', '3000035797'),
(1441, 'AJAMATTULLA', '3000035879'),
(1442, 'SF ENTERPRISES', '3000035910'),
(1443, 'NABI HUSAIN', '3000004128'),
(1444, 'RAJU MANDAL', '3000035918'),
(1445, 'ABHINANDAN KUMAR YADAV', '3000035829'),
(1446, 'DEEVAN SINGH', '3000035896'),
(1447, 'SK MOBARAK', '3000035895'),
(1448, 'IDRIS', '3000036004'),
(1449, 'PROPERTY SOLUTION (I) PVT LTD', '3000021485'),
(1450, 'MOHD MANJOOR ALAM', '3000035801'),
(1451, 'BIRAM BATI', '3000035947'),
(1452, 'MALIK AMIN', '3000035931'),
(1453, 'OM FACILITY SERVICES', '3000035932'),
(1454, 'KHAGESWAR SAHU', '3000034888'),
(1455, 'RAJKUMARI', '3000004086'),
(1456, 'SHUBHAM KUMAR GOND', '3000035969'),
(1457, 'SURESH KUMAR', '3000035972'),
(1458, 'SADAKUNWAR', '3000035971'),
(1459, 'BABLU KUMAR SAH', '3000035812'),
(1460, 'ABBASUDDIN SK', '3000035894'),
(1461, 'SAHIL INFRATECH', '3000030266'),
(1462, 'SALEMA BIBI', '3000035919'),
(1463, 'RAVI DUTT', '3000035691'),
(1464, 'LAMDA ESTERUN TELECOMMUNICATION LTD', '4000000299'),
(1465, 'OMPRAKASH DAS', '3000034630'),
(1466, 'MD ABDUS SALAM', '3000021317'),
(1467, 'BR TRADERS', '3000036081'),
(1468, 'GHANSYAM ENTERPRISES', '2000012100'),
(1469, 'RAJU MAHTO', '3000036042'),
(1470, 'VINAYAGA CONSTRUCTION', '3000036102'),
(1471, 'KANDAN INFRA', '3000035800'),
(1472, 'HEERALAL KUSHWAHA', '3000004741'),
(1473, 'KEPU TRADING LLP', '3000035982'),
(1474, 'SHAILESH TRIGUNA SINGH', '3000036090'),
(1475, 'SWAPNIL ENTERPRISES', '3000035902'),
(1476, 'PARVIN KHATUN', '3000033963'),
(1477, 'HARISHCHANDRA', '3000036180'),
(1478, 'AVI INFOTECH', '1234567891'),
(1479, 'LALARAM', '3000036237'),
(1480, 'JOGESHWAR SETHIYA', '3000035644'),
(1481, 'SONADHAR NETAM', '3000035645'),
(1482, 'SEIKH KABAUL', 'SEIKH KABAUL'),
(1483, 'NILESH YADAV', '3000036024'),
(1484, 'A P INFRATECH', '3000036226'),
(1485, 'ABSTRACT DESIGN & BUILD SOLUTION', '3000036156'),
(1486, 'RAJADEV SHAH', '3000036126'),
(1487, 'BHOLI PURVI', '3000015245'),
(1488, 'CK CONSTRUCTION', '3000036311'),
(1489, 'MD TASIRUDDIN', '3000036312'),
(1490, 'BALDEV NAG', '3000036325'),
(1491, 'HUNNY COLORS', '3000036135'),
(1492, 'ARIHANT ENGINEERING CONTRACTORS', '3000035970'),
(1493, 'SHANTILAL DHANILAL TAWDE', '3000015763'),
(1494, 'PRASENJIT MAITI', '3000036567'),
(1495, 'SNS ENTERPRISES', '3000036385'),
(1496, 'R K ENTERPRISES', '3000036061'),
(1497, 'HENA PARWEEN', '3000036448'),
(1498, 'SADIKUL ALAM', '3000036422'),
(1499, 'DHARMENDAR SAH', '3000036181'),
(1500, 'JOYNADDIN SEKH', '3000036218'),
(1501, 'JITENDRA KUMAR SINGH', '3000035261'),
(1502, 'SKIL HASSAN', '3000036484'),
(1503, 'JAGDISH CHANDRA', '3000007139'),
(1504, 'VIKASH AHUJA', '3000036109'),
(1505, 'MODERNITVE', '4000001408'),
(1506, 'ANKIT KUMAR GABHNE', '3000036435'),
(1507, 'MST SAHINA KHATUN ', 'MST SAHINA KHATUN '),
(1508, 'TEJ ENTERPRISES', '3000036523'),
(1509, 'DASHMESH POWER SOLAR SYSTEM', '4000001407'),
(1510, 'SUMONA ENTEPRISES', '3000036544'),
(1511, 'ABDUL QAYYUM ABDUL SALAM KHAN', '3000036560'),
(1512, 'DINESH KUMAR', '3000036559'),
(1513, 'PULAK JANA', '3000036495'),
(1514, 'AK ENTERPRISES', '3000036553'),
(1515, 'ARUN RAI', '3000036228'),
(1516, 'STAR ASSOCIATE', '3000036227'),
(1517, 'MANJU DEVI', '3000036486'),
(1518, 'MUKIM', '3000036565'),
(1519, 'DHEERAJ RATHI', '3000036487'),
(1520, 'SATYA KUMAR PARKASH', '3000036528'),
(1521, 'DEVANAND', '3000036578'),
(1522, 'BISWA JEET SARKAR', '3000035913'),
(1523, 'MENTOR SECURERS PRIVATE LIMITED', '3000036420'),
(1524, ' SHREEJI KRUPA PROJECT LIMITED', '4000001335'),
(1525, 'SARVAN SINGH', '3000036150'),
(1526, 'SANTOSH', '3000036290'),
(1527, 'MOHAMMAD SAJID KHAN', '3000036314'),
(1528, 'ANNU PRASAD ENTERPRIS', '3000036151'),
(1529, 'SANJAY GIDDH', '3000036382'),
(1530, 'MARUTI ENTERPRISE', '3000024070'),
(1531, 'JAI BHAWANI ENTERPRISES', '3000036648'),
(1532, 'SHYAMAL KAR', '3000036867'),
(1533, 'KAMAL NASKAR', 'KAMAL NASKAR'),
(1534, 'RAJVATI', '3000036748'),
(1535, 'SAVARMAL BIJARAM GURU', '3000036736'),
(1536, 'ARVIND KUMAR SINGH', '3000036733'),
(1537, 'BABITA DEVI', '3000036827'),
(1538, 'RAVI', '3000036672'),
(1539, 'RAMESH CHAND', '3000036728'),
(1540, 'RATAN SAHU', '3000036805'),
(1541, 'KRISHNA CONSTRUCTION', '3000036810'),
(1542, 'MST FATEMA KHATUN', '3000036779'),
(1543, 'MANOJ KUMAR CONSTRATION', '3000036619'),
(1544, 'MOHAN KUMAR RAJAK', '3000036881'),
(1545, 'MD RUHUL SK', 'MD RUHUL SK'),
(1546, 'KARTICK BISWAS & CO  ', '4200019340'),
(1547, 'NAVIN DAS', '3000036829'),
(1548, 'BHIM KUMAR YADAV', '3000002901'),
(1549, 'NAJIR ALAM ', '3000020678'),
(1550, 'RAM SINGH', '3000036876'),
(1551, 'ABM INFRATECH', '3000036879'),
(1552, 'MOHD. ZAHID NAWAZ', '3000004063'),
(1553, 'PARITOSH RAM', '3000017669'),
(1554, 'SMC BUILDERS AND ENGINEERS', '4000001542'),
(1555, 'ACQUIR SPLICING', '2000001689'),
(1556, 'SAHOO CONSTRUCTION & SUPPLIERS', '3000036798'),
(1557, 'ARSH & ARADHYA BUILDERS AND CONSTRUCTION PVT.LTD', '3000036964'),
(1558, 'SAI FABRICATOR', '3000036963'),
(1559, 'GOLAM MOSTAFA', '3000036816'),
(1560, 'MD ISMAIL', '3000036934'),
(1561, 'FLEXION INFRASTRUCTURE PVT LTD', '3000034693'),
(1562, 'NURUL ISLAM KHAN', 'NURUL ISLAM KHAN'),
(1563, 'RAMIUL SK', '3000036727'),
(1564, 'MD NAJIUL HOQUE', '3000037063'),
(1565, 'OM BAHADURI THAKURI ', '3000037037'),
(1566, 'DHARMESH ALIYAR CHOUDHARY              ', '3000037104'),
(1567, 'EJAJUL HOUQE', '3000037095'),
(1568, 'ANJAR ALAM', '3000037038'),
(1569, 'JAHANGIR BOSNI', '3000036379'),
(1570, 'MOSARAF HOSSAIN', '3000037155'),
(1571, 'NORTH INDIA CONSTRUCTION', '3000037082'),
(1572, 'AKHILESHWAR PANDEY', '3000027427'),
(1573, 'I S ENTERPRISE', '2000007490'),
(1574, 'YASMEEN AKRAM KHAN', '3000035116'),
(1575, 'MAYA ENTERPRISES', '4000001556'),
(1576, 'SWEDESH KUMAR', '3000037015'),
(1577, 'BIMAL TECHNICAL SERVICES PRIVATE LIMITED', '2000012778'),
(1578, 'KARTICK BISWAS & CO', '4200019342'),
(1579, 'SEKH ANIKUL', 'SEKH ANIKUL'),
(1580, 'SHIV BARAN SAH', '3000022576'),
(1581, 'MAHTO YOGENDRA RAMKISHUN', '3000014284'),
(1582, 'HARI HAR PRASAD DAHAYAT', '3000037200'),
(1583, 'SAJIRUDDIN HAQUE', '3000005844'),
(1584, 'SANDEEP KUMAR', '3000036999'),
(1585, 'SADDAM HUSSAIN', '3100004642'),
(1586, 'MASHOOQ AHMAD USMANI', '3000037208'),
(1587, 'SANTOSH KUMAR', '3000037226'),
(1588, 'SHAMBHU KUMAR', '3000036871'),
(1589, 'SEC GROUP ', '3000037027'),
(1590, 'JAYCHANDRA KUMAR', '3000037171'),
(1591, 'ANAVAR ALAM ', '3000017933'),
(1592, 'MEMI BIBI', '3000037246'),
(1593, 'DURGESH', '3000037294'),
(1594, 'SUPRIYA KHASKEL', '3000036818'),
(1595, 'P S INFRATECH', '3000020471'),
(1596, 'B.S.CONSTRUCTION', '3000037245'),
(1597, 'PRASHANT JITEN PAUL', '3000037505'),
(1598, 'SONAL ENTERPRISES', '4000001337'),
(1599, 'K.V.ENGINEERING', '2000012320'),
(1600, 'TTPL SURFACE TEXTURES LLP', '4000000881'),
(1601, 'VYAS NARAYAN KAUSHIK', '3000037338'),
(1602, 'CHETAN SINGH', '3000019975'),
(1603, 'MAIMUL SK', '3000037290'),
(1604, 'EQUIPMENT PLUS', '4000001563'),
(1605, 'PARITOSH RAM', '3000017667'),
(1606, 'SADDAM HUSSAIN', '3000036911'),
(1607, 'MOHIN AKTAR', '3000025518'),
(1608, 'SUNIL MEHTA', '3000022217'),
(1609, 'RAVINDER YADAV', '3000037052'),
(1610, 'BIBI HINAJ', '3000037017'),
(1611, 'ADARSH ENTERPRISES', '3000037157'),
(1612, 'RAJESH SINGH', '3000036912'),
(1613, 'SAHEDUL MIA', '3000036910'),
(1614, 'JAI GURUDEV CONSTRUCTION', '3000037646'),
(1615, 'CHHOTE KUMAR', '3000037427'),
(1616, 'CHINTA KUSHWAHA', '3000037396'),
(1617, 'LAMBDA EASTERN TELECOMMUNICATION LIMITED ', '3300002425'),
(1618, 'SHREE ENTERPRISES', '3000037412'),
(1619, 'AQUAPLAST INFRACON PVT LTD', '4000001362'),
(1620, 'K R CONSTRUCTION', '3000037444'),
(1621, 'SANJAY KUMAR SINGH', '3000037404'),
(1622, 'PRADIP BIJOY DAS', '3000037366'),
(1623, 'SHREE KRISHNA HOSPITALITY SERVICES', '3000037355'),
(1624, 'CHUHADMAL KUMAR', '3000037293'),
(1625, 'INDAL SINGH CONTRACTOR', '3000037446'),
(1626, 'KAPIL HARPAL', '3000037137'),
(1627, 'VIMAL TRADING CO', '4000000894'),
(1628, 'SAMAYUN BISWAS', '3000022239'),
(1629, 'CST CONSTRUCTION ', '3000037113'),
(1630, 'M FEBRICO & CONSTRUCTION', '3000035316'),
(1631, 'BITU ENGINEERS', '3000036101'),
(1632, 'MOSTAFA MIAH', '3000035984'),
(1633, 'S KESAVAN', '3000036354'),
(1634, 'DIWAKAR ENTERPRISES', '3000033707'),
(1635, 'MATHAIYAN CIVIL CONT', '3000005357'),
(1636, 'SOLANKI ENTERPRISES', '3000037296'),
(1637, 'SUKTAKA KHATUN', '3000037436'),
(1638, 'PRINCE BUILDERS', '3000037395'),
(1639, 'AL-NAMIRA ENTERPRISES AND SERVICES', '4000001643'),
(1640, 'HUMBE SUPPLIERS', '3000037011'),
(1641, 'JAI BHAGWAN FABRICATION', '4000001642'),
(1642, 'MOMIN MUDASSIR AHMED JALALUDDIN', '3000036817'),
(1643, 'RAVI KUMAR', '3000037486'),
(1644, 'JONY PATEL', '3000037506'),
(1645, 'BABLU NUNIYA', '3000037548'),
(1646, 'BEST CARE FACILITY', '4000001673'),
(1647, 'REENA DEVI', '3000024942'),
(1648, 'VIJAY KUMAR YADAV', '3000037504'),
(1649, 'SURA BODRA', '3000004663'),
(1650, 'VIJENDRA KUMAR', '3000003704'),
(1651, 'TARU BARTARIA', '3000036882'),
(1652, 'BBR (INDIA) PVT LTD', '4000000811'),
(1653, 'SAKHAVAT', '3000009710'),
(1654, 'SONU KUMAR', '3000037571'),
(1655, 'SATANAND', '3000037648'),
(1656, 'SHRI BHAGWAN', '4000001693'),
(1657, 'SARJU YADAV', '3000037623'),
(1658, 'RAJEEV KUMAR SINGH', '3000037683'),
(1659, 'SUNIL YADAV', '3000037622'),
(1660, 'MOHD NASIM', '3000037624'),
(1661, 'JRS INFRATECH', '4200008940'),
(1662, 'DIPAK KUMAR', '3000037678'),
(1663, 'CHNCHAL DEVI', '3000037679'),
(1664, 'SACHIN BHAUSAHEB DHADGE', '3000037566'),
(1665, 'M.D EADIRIS', '2000006090'),
(1666, 'ROCKLIKE INFRASTRUCTURE PVT LTD', '4000001675'),
(1667, 'CHHABBU YADAV', '3000037094'),
(1668, 'IBRAR AHMAD MOMHAMMAD DAR', '3000036745'),
(1669, 'LALTESH KUMAR', '3000037702'),
(1670, 'ZAFAR BROTHERS & CO', '4000001732'),
(1671, 'MD JIMBAR SEKH', '3000037491'),
(1672, 'MANISH KUMAR', '3000037626'),
(1673, 'MOHD KESH', '3000037628'),
(1675, 'SARVJIT KUMAR', '3000037753'),
(1676, 'AJIT KUMAR PAL', '3000024124'),
(1677, 'ROHAN SINGH', '3000037756'),
(1678, 'DEVPRASAD PAL', '3000037745'),
(1679, 'ROSHANLAL GUPTA', '3000037833'),
(1680, 'A.F.CONSTRUCTION', '3000037652'),
(1681, 'KAILASH', '3000037701'),
(1682, 'OMPRAKASH DAS', '2000001274'),
(1683, 'RANJEET KUMAR', '3000026889'),
(1684, 'G TECH', '4000001571'),
(1685, 'SAIMA PARWEEN', ' 3000037883         '),
(1686, 'SANDIP FABRICATORS', '3000037603'),
(1687, 'SHAMAL DILIP RAY', '3000037749'),
(1688, 'KANAI LAL PARIA', '3000026494'),
(1689, 'TARIKUL ISLAM', '3000037909'),
(1690, 'SHABANA KHATUN', '3000037927'),
(1691, 'CDS CONSTUCTION CO', '3000037731'),
(1692, 'TARUN BUILDERS', '3000036844'),
(1693, 'RK BUILDER', '3000037973'),
(1694, 'RAHUL CONSTRUCTIONS              ', '3000038001  '),
(1695, 'RAJESH PUTEL', '3000037516'),
(1696, 'KRISHNA DEVELOPERS', '3000037926'),
(1697, 'MD GULAM NABI', '3000037999'),
(1698, 'DASRATH VISHWAKARMA', '3000038007'),
(1699, 'BHARAT & SONS ENTERPRISES', '3000012852'),
(1700, 'JAGADISH SIKDAR', '3000018780'),
(1701, 'SREEMAA ENTERPRISE', '3000038284'),
(1702, 'RESMINA KHATUN', '3000036082'),
(1703, 'BABU SHASHANK RAJ', '3000037759'),
(1704, 'AJAY YADAV', '3000038050'),
(1705, 'SHOHAIL AKHTAR', '3000038051'),
(1706, 'MD NEHAL', '3000038048'),
(1707, 'DHRITI INFRA', '4000001624'),
(1708, 'JAI PRAKASH KUMAR RAM', '3000038049'),
(1709, 'NAUSAD ALI', '3000037938'),
(1710, 'SELIM HUSSAIN', '3000014014'),
(1711, 'ABDULLA SK', '3000038269'),
(1712, 'BIMAL KRISHNA MANDAL', '3000009671'),
(1713, 'SUMAN SINGH', '3000038089'),
(1714, 'HASMUDDIN', '3000011044'),
(1715, 'DINESH MAHTO', '3000038052'),
(1716, 'ADARSH FABRICATION & ENGINEERING', '3000025522'),
(1717, 'PRAMOD KUMAR', '3000037760'),
(1718, 'SOHAN RAI', '3000038045'),
(1719, 'BIRENDRA KUMAR YADAV', '3000009486'),
(1720, 'KAMALDEV MANJHI', '3000038146'),
(1721, 'AJAY YADAV', '3000038147'),
(1722, 'BINOD PUTEL', '3000038054'),
(1723, 'TARA SINGH', '3000038188'),
(1724, 'MD ISMAIL SK', '3000036410'),
(1725, 'AKRUR KUMAR', '3000038206  '),
(1726, 'ANKIT KUMAR', '3000038158'),
(1727, 'ANIL KUMAR DAS', '3000038128'),
(1728, 'ABHIRUD ENTERPRISES', '4000001756'),
(1729, 'NEW BUSH ELECTRICAL', '4000001661'),
(1730, 'PUSHKAR TOURS & TRAVELS', '4000001755'),
(1731, 'SAI KRIPA ELECTRICALS', '4000001764'),
(1732, 'SHAIKH CONSTRUCTION', '4000001765'),
(1733, 'EJAJ AHMAD ', '3000038207'),
(1734, 'UMMA KULSUM', '3000038288'),
(1735, 'RAHBAR ALAM', '3000037882'),
(1736, 'KANTI DEVI', '3000038157'),
(1737, 'J. A. CONSTRUCTION', '3000038144'),
(1738, 'A Y CONSTRUCTION ', '3000038199'),
(1739, 'SANJAY KUMAR', '3000038034'),
(1740, 'BRD RNTERPRISES', '3000038212'),
(1741, 'KAUSHIK ENGINEERING AND TRADING CO', '3000038033'),
(1742, 'PADMA CHARAN', '3000034624'),
(1743, 'SANJAY SINGH', '3000038289'),
(1744, 'A K CONSTRUCTION', '3000037569'),
(1745, 'ABDUL YUSUF', '3000032947'),
(1746, 'AJAY SAH', '3000017170'),
(1747, 'ARVIND KUMAR', '3000037994'),
(1748, 'ASHIM GHOSH', '3000037496'),
(1749, 'ASHIS BISWAS', '3000037040'),
(1750, 'B S ENTERPRISE', '3000038478'),
(1751, 'BABLU KUMAR JHA', '3000037997'),
(1752, 'BIBHA ENGINEERING AND CONSTRUCTION', '4000001492'),
(1753, 'BIDHAN GHOSH', '3000024683'),
(1754, 'BIKASH KUMAR SINGH', '3000037920'),
(1755, 'CHARITRA ROY', '3000032747'),
(1756, 'DAS ENTERPRISE', '3000037039'),
(1757, 'DHARMENDRA PASWAN', '3000032628'),
(1758, 'DIPAK MISHRA', '3000037977'),
(1759, 'JAINAB CONSTRUCTION', '3000035441'),
(1760, 'JAYRAM PANDIT', '3000032697'),
(1761, 'JITENDRA JHA', '3000038056'),
(1762, 'JUREN DHAR', '3000036593'),
(1763, 'KABATULLA MONDAL', '3000005142'),
(1764, 'MANOJ KUMAR GUPTA', '3000038292'),
(1765, 'MD FIRDOS', '3000013926'),
(1766, 'MD QURBAN ALI', '3000037043'),
(1767, 'MEGHNATH CHAKRABORTY', 'MEGHNATH CHAKRABORTY'),
(1768, 'MOTAHAR HOSSAIN', '3000037921'),
(1769, 'MUKTAR', '3000033498'),
(1770, 'MUKUL HOSSAIN', 'MUKUL HOSSAIN'),
(1771, 'PULAK ACHARJEE', 'PULAK ACHARJEE'),
(1772, 'R D ENTERPRISE', 'R D ENTERPRISE'),
(1773, 'R S CONSTRUCTION', '3000037924'),
(1774, 'RABINDRA NATH BISWAS', '3000037316'),
(1775, 'RAISUDDIN SEKH', '3000037430'),
(1776, 'RIZWAN ALAM', '3000033987'),
(1777, 'ROJINA BEGUM', 'ROJINA BEGUM'),
(1778, 'S A INFRATECH', 'S A INFRATECH'),
(1779, 'S K ENTERPRISE', 'S K ENTERPRISE'),
(1780, 'SABITA DAS', '3000038104'),
(1781, 'SAHABAZ ALAM', 'SAHABAZ ALAM'),
(1782, 'SANJULA DEVI', 'SANJULA DEVI'),
(1783, 'SHAGUFTA PARWEEN', 'SHAGUFTA PARWEEN'),
(1784, 'SHREE DEV ENGINEERING', 'SHREE DEV ENGINEERING'),
(1785, 'SHRI VINAYAK ENTERPRISES', '3000037572'),
(1786, 'SK SAPU', 'SK SAPU'),
(1787, 'SRIRAM SHARMA', '3000038010'),
(1788, 'SUBAL MONDAL', 'SUBAL MONDAL'),
(1789, 'SUBHAS MAJUMDER', '3000032743'),
(1790, 'SUKUMAR MIRDHA', '3000017244'),
(1791, 'SUPRIYA MAITY', '3000026900'),
(1792, 'SURAJ SINGH', 'SURAJ SINGH'),
(1793, 'SWATI BUILDTECH', 'SWATI BUILDTECH'),
(1794, 'UMESH DAS', '3000038165'),
(1795, 'ZIYAUL HAQUE', '3000037253'),
(1796, 'RAMU PANDIT', '3000038012'),
(1797, 'J S S ENGINEERING (P) LTD', '3000021420'),
(1798, 'RANJAN DEYSARKAR', '3000037149'),
(1799, 'SUBAL MANDOL', 'SUBAL MANDOL'),
(1800, 'VARADAA CONSTRUCATION', '4000001686'),
(1801, 'DOLLY ENTERPRISE', 'DOLLY ENTERPRISE'),
(1802, 'SHABULAL MEENA', '3000015274'),
(1803, 'DEVENDRA PRASAD', '3000038318'),
(1804, 'KOUSHIK ADHIKARI', 'KOUSHIK ADHIKARI'),
(1805, 'ROUT FABRICATORS', 'ROUT FABRICATORS'),
(1806, 'MOKAREM HOQUE', 'MOKAREM HOQUE'),
(1807, 'RCT CONSTRUCTION', '4000001925'),
(1808, 'HARSH INDUSTERIES', '4000000844'),
(1809, 'ARVIND KUMAR', '3000038316'),
(1810, 'POONAM DEVI', '3000038317'),
(1811, 'SUMIT KUMAR YADAV', '3000038299'),
(1812, 'ABDUL HAKEEM', '3000038309'),
(1813, 'AKASH MISHRA', '3000038256'),
(1814, 'ANWAR BAITHA', '3000038379'),
(1815, 'MD JAKIR SHAIKH', '3000022623'),
(1816, 'MD NABUAT', '3000038274'),
(1817, 'OMPRAKASH GUPTA', '3000038000'),
(1818, 'NUSRAT KHATUN', '2000005222'),
(1819, 'SP CONSTRUCTION', '3000038347'),
(1820, 'RAJ BUILDERS', '4000001147'),
(1821, 'JITENDRA RAM', '3000038376'),
(1822, 'BULBUL FABRICATION & ENGINEERING WORK', '3000038308'),
(1823, 'UNIQUE POWER GROUP    ', '4000001519'),
(1824, 'UTRACON STRCULSCERR SYSTEM', '4000001267'),
(1825, 'ANJAN KUMAR NAYAK', '4000001903'),
(1826, 'RAHUL ELECTRICALS', 'RAHUL ELECTRICALS'),
(1827, 'NOOR FURNITURE', '3000037167'),
(1828, 'NAJIMIN NISHA', '3000038424'),
(1829, 'SONAMATI DEVI', '3000006872'),
(1830, 'CHANDRAMOHAN CHAUDHARI', '3000038349'),
(1831, 'NAWAB HUSAIN', '3000038407'),
(1832, 'SABITA BISWAS', '3000037588'),
(1833, 'RAJESH KUMAR', '3000038411'),
(1834, 'RAMASHRAY SHARMA', '3000038487'),
(1835, 'SHIV INFRA', '4000001948'),
(1836, 'BDS PEST SERVICES PVT.LIMITED', 'BDS PEST SERVICES PVT.LIMITED'),
(1837, 'AJIT KUMAR', '3000038527'),
(1838, 'ABHISHEK KUMAR', '3000038525'),
(1839, 'D S ENTERPRISES', '3000038102'),
(1840, 'L K LIFE STYLE LLP', '3000003117'),
(1841, 'PIECO', '4000001621'),
(1842, 'SUNITA BHIM PRASAD', '3000035817'),
(1843, 'DEVENDRA RAY', '3000038536'),
(1844, 'BR POWERTECH', 'BR POWERTECH'),
(1845, 'BBDS PEST SERVICES PVT LIMITED', 'BBDS PEST SERVICES PVT LIMITED'),
(1846, 'FERDOUSI BEGAM', '3000038440'),
(1847, 'MUKUL HOSSAIN', '3000038406'),
(1848, 'PULAK ACHARJEE', '3000033964'),
(1849, 'ROUT FABRICATORS', '3000037906'),
(1850, 'SAHABAZ ALAM', '3000038446'),
(1851, 'SANJULA DEVI', '3000038442'),
(1852, 'SKEKH SAPU', '3000038333'),
(1853, 'SAVITA CONSTRUCTION', '3000007843'),
(1854, 'ISHWAR MURMU', '3000038511'),
(1855, 'GREEN CONSTRUCTION', '3000038604'),
(1856, 'WIZARDRY INFRA PVT LTD', '3000038739'),
(1857, 'RACHANA SINGH', '3000038387'),
(1858, 'ADITYA PLUMBING & WATER PROOFING', '4000001937'),
(1859, 'MALL INFRASTRUCTURE', '4000001963'),
(1860, 'UKIRDE BABASAHEB RAJENDRA', '3000038445'),
(1861, 'NEERAJ KUMAR', '3000038559'),
(1862, 'GOODBUILD ENGINEERS LLP', '4000001970'),
(1863, 'JAI SIDH BABA ENGINEERING TECHNOO', '3000038537'),
(1864, 'YESHVEER ENTERPRISES', '3000038611'),
(1865, 'ALAM CONSTRUCTION', '3000038161'),
(1866, 'HAJIKUL ISLAM', '3000038605'),
(1867, 'FIROJA KHATUN', 'FIROJA KHATUN'),
(1868, 'MAA KALI DISTRIBUTOR', '3000038588'),
(1869, 'NIRMAN ENTERPRISE', '3000034938'),
(1870, 'PROTECK ANTI-CORROSIVES', '3000014212'),
(1871, 'RED MAN SERVICE', 'RED MAN SERVICE'),
(1872, 'ROJINA BEGAM', '3000038332'),
(1873, 'SEIKH MONIRUL ISLAM', '3000035870'),
(1874, 'RASHMI TANWAR', '3000038476'),
(1875, 'SOHAN PRAJAPATI', '3000038665'),
(1876, 'G S SECURITY SERVICES', '3000031809'),
(1877, 'PRINCE HIRDAYANAND SHARMA', '3000038576'),
(1878, 'TECH-KNOW EARTHMOVING SERVICE', '2000009254'),
(1879, 'ACROSS NETWORK PRIVATE LIMITED', '3000038602'),
(1880, 'MD SAEED ANWAR', 'MD SAEED ANWAR'),
(1881, 'SAHID ALAM', '3000038626'),
(1882, 'VEERPAL SINGH', '3000038668'),
(1883, 'CHANDRA BHUSHAN SINGH', '3000038773'),
(1884, 'SUDHIR RAVIDAS', '3000038718'),
(1885, 'UNIQUE ENGINEERS PVT LTD', '3000021799'),
(1886, 'MUKESH KUMAR MISHRA', '3000038625'),
(1887, 'SBS CONSTRUCTION', '3000038716       '),
(1888, 'TUFAN SARDAR', 'TUFAN SARDAR'),
(1889, 'KAISH CONSTRUCTION', '3000015519'),
(1890, 'AKHILESH SRIVASTAVA', '3000038687'),
(1891, 'VALLABHI INFRA PVT.LTD', '3000038587'),
(1892, 'MD SAEED ANWAR', '3000038740'),
(1893, 'PANCHARAJ SINGH', '3000038750'),
(1894, 'ISALAMUDADIN', '3000038790'),
(1895, 'CRESCENT TRADERS', '3000038789'),
(1896, 'LALBABU PASAD', '3000012042'),
(1897, 'MANOHAR BAITHA', '3000038791'),
(1898, 'SUKHEN ROY', '3000038879    '),
(1899, 'SUNARPATI DEVI', '3000038887'),
(1900, 'PANPATI DEVI', '3000038890'),
(1901, 'MD AKBAR', '3000038610'),
(1902, 'MD SAKIR RAZA', '3000038815'),
(1903, 'VINOD KUMAR', '3000038785'),
(1904, 'BAIJNATH BAITHA', '3000038786'),
(1905, 'MAHENDRA KUMAR CHOUHAN', '3000038623'),
(1906, 'PVS ENGINEERS', '3000038814'),
(1907, 'SAJID KHAN', '3000035985'),
(1908, 'R.P.ENGINEERING & WELDING', '3000038182'),
(1909, 'MD MOSRAF HOSSAIN', 'MD MOSRAF HOSSAIN'),
(1910, 'RAJIB ALI', 'RAJIB ALI'),
(1911, 'S R CONSTRUCTION', '3000037341'),
(1912, 'M D EADIRIS', '200006090'),
(1913, 'SURYODAY ENTERPRISES', '3000035075'),
(1914, 'TECHNOTORE TOOLS PVT LTD', 'TECHNOTORE TOOLS PVT LTD'),
(1915, 'SAKIBUL ISLAM', 'SAKIBUL ISLAM'),
(1916, 'I CLEAN HOLLOW METAL SYSTEM', '2000002626'),
(1917, 'STANLEY JOSLIN CHRISTIAN         ', '3000039015   '),
(1918, 'JAGDISH MAHTO', '3000039026'),
(1919, 'MD MUKTHAR ALAM', '3000028530'),
(1920, 'NURZABI BEGAM', '3000039027'),
(1921, 'MAHIDUR', 'MAHIDUR'),
(1922, 'FLOOR MART GLOBAL', '4000001996'),
(1923, 'MD SAEED ANWAR', '3000038749'),
(1924, 'MOHAMMAD SHAUKAT', '3000039016'),
(1925, 'PREM PAL', '3000038620'),
(1926, 'ISMAIL HAQUE', '3000036421'),
(1927, 'REIDIUS ENGITECH PVT LTD', 'REIDIUS ENGITECH PVT LTD'),
(1928, 'INDRAJEET SINGH', 'INDRAJEET SINGH'),
(1929, 'MD IJHAR ASRAF', '3000039051'),
(1930, 'MOHD RAKESH ALI', '3000039052'),
(1931, 'ANP INFRA', '3000039024'),
(1932, 'VAN CONSTRUCTIONS AND CONTRACTORS LLP', '3000038952'),
(1933, 'JAGANNATH DAS', '3000039109'),
(1934, 'RAJIB HASAN', '3000039074'),
(1935, 'JAGDEEP BHALLA ASSOCIATE', 'JAGDEEP BHALLA ASSOCIATE'),
(1936, 'ARMATIX (INDIA) PRIVATE LIMITED', 'ARMATIX (INDIA) PRIVATE LIMITED'),
(1937, 'KHALEDUR RAHAMAN', '3000039071'),
(1938, 'MRINAL GHOSH', 'MRINAL GHOSH'),
(1939, 'SHREE SAI ENTERPRISE', '3000039108'),
(1940, 'PREM SHANKAR SINGH', '3000026186'),
(1941, 'ANISUR RAHAMAN MOLLAH', '3000037584   '),
(1942, 'RAJ KUMAR PANDAY', '3000039149'),
(1943, 'MOHAMMED EHSAN', '3000039217'),
(1944, 'SANJAY KUMAR', '3000039152'),
(1945, 'MD HAMIDUL HOQUE', 'MD HAMIDUL HOQUE'),
(1946, 'SNMR FABRICATOR', 'SNMR FABRICATOR'),
(1947, 'B R B ENTERPRISES', '3000039247'),
(1948, 'AKASH BANSODE ', '3000039246'),
(1949, 'RAJU KUMAR MANDAL', '3000039237'),
(1950, 'MOHAMMAD HARUN RASHID', '3000039079'),
(1951, 'PUNAM DEVI', '3000039037'),
(1952, 'MD SULEMAN', '3000039063'),
(1953, 'KIRAN DEVI', '3000039215'),
(1954, 'RENU KHATUN', '3000039197'),
(1955, 'SHREE ENTERPRISES (NEW)', '3000039211 '),
(1956, 'CHANDAN SINGH', '3000038951'),
(1957, 'RAAZ ENTERPRISES          ', '4000001997 '),
(1958, 'K.G.N CONSTRUCTION', 'K.G.N CONSTRUCTION'),
(1959, 'RIPON NARAYAN BISWAS ', '3000039348   '),
(1960, 'NURUL', '3000039325'),
(1961, 'SURESH KUMAR GUPTA', '3000039309'),
(1962, 'ABDUL RAJJAK', '3000039175'),
(1963, 'SERAJUL SK', 'SERAJUL SK'),
(1964, 'ASTHA HEALTH CARE', '3000039025'),
(1965, 'ARABINDA BAUL', '3000039068'),
(1966, 'MASIBUR RAHMAN', '3000039408'),
(1967, 'MRINMOY ROY', '3000039374'),
(1968, 'RAY ROOFING PVT LTD', 'RAY ROOFING PVT LTD'),
(1969, 'SAINA BEGAM', '3000039376'),
(1970, 'SANTOSH PANDIT', 'SANTOSH PANDIT'),
(1971, 'TRO MAX ENGINEERING', 'TRO MAX ENGINEERING'),
(1972, 'SHRI ENTERPRISES', '3000039382'),
(1973, 'PRINCE KUMAR', '3000039222'),
(1974, 'SANJAY KUMAR', '3000039353'),
(1975, 'GOPAL', '3000039423'),
(1976, 'VISHVAKARMA ENTERPRISES', '3000039354'),
(1977, 'PARWEZ ALAM', 'PARWEZ ALAM'),
(1978, 'SHAHJAD ALAM', '3000039378'),
(1979, 'NILAM DEVI', '3000039321'),
(1980, 'ROJINA KHATUN', '3000039422'),
(1981, 'MSBK INFRASTRUCTURE', '3000036236'),
(1982, 'WILY MERCHANT', 'WILY MERCHANT'),
(1983, 'JAYRAM RAMU CHAUHAN', '3000007639'),
(1984, 'BIRENDRA KUMAR', '3000039416'),
(1985, 'SHABBIR KHALKULLA KHAN', '3000039417'),
(1986, 'SHIV SHAKTI ENG. WORKERS', '3000027945'),
(1987, 'STAR ASSOCIATES', '3000026227'),
(1988, 'SHIVNATH SAHANI', '3000028191'),
(1989, 'S K AFIRUL', 'S K AFIRUL'),
(1990, 'ALOM CONSTRUCTION', 'ALOM CONSTRUCTION'),
(1991, 'MOFIJ ALAM', '3000039986'),
(1992, 'HARADHAN MONDAL', '3000039498'),
(1993, 'MAHIDUR RAHMAN', '3000019760'),
(1994, 'MD AHASAN ALI', 'MD AHASAN ALI'),
(1995, 'MD NIFAIUDDIN SEIKH', '3000039630'),
(1996, 'RAJAB ALI', '3000038964'),
(1997, 'UMESH DAS', '3000038167'),
(1998, 'SONATUN KHATUN', '3000039496'),
(1999, 'SANTOSH DEVELOPER ENTERPRAISE', 'SANTOSH DEVELOPER ENTERPRAISE'),
(2000, 'SHRIBAS PAL', '3000039580'),
(2001, 'TRASTAVEN COMMUNICATIONS PVT.LTD.', 'TRASTAVEN COMMUNICATIONS PVT.LTD.'),
(2002, 'RAMESH', '3000039497'),
(2003, 'D.S ENTERPRISES', '3000039509'),
(2004, 'SHAURYA ENGINEERING WORKS', '3000039525'),
(2005, 'ISTKHAR ALAM', '3000039482'),
(2006, 'RAJU SHAIKH', 'RAJU SHAIKH'),
(2007, 'GANDHI AUTOMATIONS PVT LTD.', '3000002889'),
(2008, 'LIMRA INFRAVENTURES', '3000037518'),
(2009, 'SMRUTIRANJAN DASH', '3000038938'),
(2010, 'SUDHIR NAYAK ', '3000039199'),
(2011, 'ASLAM KHAN', '3000039403'),
(2012, 'SANJEEV KUMAR', '3000039483'),
(2013, 'RAHAMAN ENTERPRISE', 'RAHAMAN ENTERPRISE'),
(2014, 'NASIR AHAMAD', '3000039585'),
(2015, 'FORTUNE BUILDCON', '3000039596'),
(2016, 'MD.ALAM SEIKH', 'MD.ALAM SEIKH'),
(2017, 'GAURAV TOMAR', '3000039148'),
(2018, 'RUBI DEVI', '3000039556'),
(2019, 'RAJA SHAIKH', 'RAJA SHAIKH'),
(2020, 'HANNAN ALAM', '3000039615'),
(2021, 'ADITYA ENTERPRISES', '40000002206'),
(2022, 'RAFIQUE ALAM', '3000039651'),
(2023, 'ASRAFUL SK', '3000039664'),
(2024, 'BUILD CARE', 'BUILD CARE'),
(2025, 'JSS ENGINEERING PVT LTD', 'JSS ENGINEERING PVT LTD'),
(2026, 'SNMR FAB INDIA PRIVATE LIMITED', '4000002062'),
(2027, 'SAKTHI MURUGAN', '3000039650'),
(2028, 'JAVED ALI', '3000039652'),
(2029, 'JULEKHA KHATOON', '3000039559'),
(2030, 'GAURAV SAINI', '3000039621'),
(2031, 'NURALA KHAN', '3000039672'),
(2032, 'AMIR SOHEL', '3000039778'),
(2033, 'ANIKUL ISLAM', 'ANIKUL ISLAM'),
(2034, 'FIRDOUS KHANAM', '3000039631'),
(2035, 'MAJIBAR RAHAMAN', '3000039669'),
(2036, 'MD.ABU ZAKARIA', 'MD.ABU ZAKARIA'),
(2037, 'S.K.AFIRUL', '3000039471'),
(2038, 'ALOK RANJAN MAHATO', 'ALOK RANJAN MAHATO'),
(2039, 'JUEL HOQUE', '3000018441'),
(2040, 'ANJU DEVI                          ', '3000039676 '),
(2041, 'JITENDRA PRASAD GUPTA', '3000038575'),
(2042, 'SANTOSH KUMAR SINGH', '3000039609'),
(2043, 'JAGDISH MAHTO', '4200026703'),
(2044, 'MD AHASAN ALI', '3000039512'),
(2045, 'PRODIP BISWAS', '3000039706'),
(2046, 'J K CONSTRUCTIONS', '3000039674'),
(2047, 'MD MAHFUZ KAUSHAR', '3000039673'),
(2048, 'ARUN SINGH KUSHWAH', '3000039214'),
(2049, 'JYOTI ENGINEERING WORKS', '3000039459'),
(2050, 'SUSHEEL KUMAR', '3000039702'),
(2051, 'ASIT ENGINEERING &CO', 'ASIT ENGINEERING &CO'),
(2052, 'J.S.ENTERPRISE', '3000039717'),
(2053, 'MD BALAL HUSSEN', 'MD BALAL HUSSEN'),
(2054, 'STAGVOLT', 'STAGVOLT'),
(2055, 'MAMTA KUMARI', '3000039721'),
(2056, 'TAJMUL SHAIKH', '3000039764'),
(2057, 'SARFARAJ CONSTRUCTION', '3000039809'),
(2058, 'SUNIL KUMAR BHARTI', '3000039724'),
(2059, 'MURARI PRASAD', '3000039649'),
(2060, 'SARTAJ ENTERPRISES', '3000039150 '),
(2061, 'MAA CONSTRUCTION', '3000039811'),
(2062, 'SABITA PRADHAN', '3000028316'),
(2063, 'DHANNU SINGH', '3000039700'),
(2064, 'S P ENTERPRISES', '3000038784'),
(2065, 'RAMRAJ YADAV', '3000039757'),
(2066, 'MD SULEMAN', '3000021373'),
(2067, 'MOSTAFA MIAH', '3000035954'),
(2068, 'BABLU CHAUDHARY        ', '3000039834'),
(2069, 'VASIB', '3000039703'),
(2070, 'SUNIL', '3000039835'),
(2071, 'PALASH BISWAS', '3000039836'),
(2072, 'CABLE CARE PRIVATE LIMITED', '2000013061'),
(2073, 'MODERN INDECOR', '3000002553'),
(2074, 'MD GULSET', '3000039501'),
(2075, 'RAHUL YADAV', '3000039795'),
(2076, 'VIRENDRA YADAV', '3000039801'),
(2077, 'RAM CHANDRA MANDAL', '3000038600'),
(2078, 'SUDHANJALI LENKA', '3000038601'),
(2079, 'GUDDI DEVI', '3000039793'),
(2080, 'ANIL KUMAR MEHTA', '3000033626'),
(2081, 'IMAZUL HAQUE', '3000039792'),
(2082, 'ABDUL GAFFAR ANSARI', '3000039520'),
(2083, 'MADAN YADAV', '3000039794'),
(2084, 'BHOLA NATH', '3000039837'),
(2085, 'MEHTA CONSTRUCTION', '3000023689'),
(2086, 'S D ENTERPRISES', '3000039867'),
(2087, 'MOHAMMAD RAJA RAJA', '3000007392'),
(2088, 'A S ENTERPRISES', '3000039920'),
(2089, 'BHAURANAND FABRICATION ', '4000002215'),
(2090, 'KARTIK MULTI SERVICES', '3000039578'),
(2091, 'PROJECT BUILDERS PVT. LTD', '4000002214'),
(2092, 'RAJENDRA SERVICES', '3000039722'),
(2093, 'SMITH STUCTURES', '4000001917'),
(2094, 'L D CONSTRACTION', '3000039966'),
(2095, 'BAJRANGI CONSTRUCTION', '3000035324'),
(2096, 'CHAKRABORTY CONSTRUCTION COMPANY', '3000007897'),
(2097, 'GOPAL BISWAS                 ', '3000039968'),
(2098, '3000010654', 'PAPAN DEBNATH'),
(2099, 'PAPAN DEBNATH', '3000010654'),
(2100, 'MANOWAR HOSSAIN', '3000039964'),
(2101, 'RAJIV RANJAN SINGH', '3000038273'),
(2102, 'OM INTERNATIONAL', '4000002286'),
(2103, 'K A ONE CONSTRUCTION COMPANY', '3000039611'),
(2104, 'KAMLESH KUMAR', '3000039797'),
(2105, 'MEHANDI HASAN', '3000039796'),
(2106, 'MOHAMMAD SARFARAJ', '3000039581'),
(2107, 'NITU DEVI', '3000039238'),
(2108, 'RAHUL KUMAR', '3000039667'),
(2109, 'RAJU CHAUDHARY', '3000004724'),
(2110, 'RAVIKANT SAINI', '3000039666'),
(2111, 'RAVINDRA RAY', '3000039555'),
(2112, 'SHARVAN KUMAR', '3000039668'),
(2113, 'SHAI PRITTAM ENTERPRISES', '3000039958'),
(2114, 'DILRASA KHATOON SAMIR KHAN', '3000040027'),
(2115, 'KRUSHNA SAMPAT BORHADE', '3000040028'),
(2116, 'SHREE SAMARTH KRUPA ENTERPRISES', '4000002331'),
(2117, 'GITA DEVI', '3000039973'),
(2118, 'SANJAY KUMAR PRAJAPATI', '3000040014'),
(2119, 'ANWARUL HASAN', '3000039974'),
(2120, 'RAHAMAN ALI', '3000039910'),
(2121, 'KUMOD MANJHI              ', '3000039911'),
(2122, 'ARVIND', '3000039924'),
(2123, 'MD MASROOL             ', '3000040045'),
(2124, 'OM PRAKASH VERMA  ', '3000040046'),
(2125, 'C I ENGINEERING', '3000039744'),
(2126, 'STEM INFRASTRUTURE', '4000001825'),
(2127, 'ANKSHI DEVI', '3000039868'),
(2128, 'A F ENTERPRISE', '3000040354'),
(2129, 'TRISTAR INFRATECH ENTERPRISE', '4000002135'),
(2130, 'STONE ERA', 'STONE ERA'),
(2131, 'IMRAN KHAN', '3000040434'),
(2132, 'MD ASIF HOSSAIN           ', '3000040058   '),
(2133, 'SANCHITA BISWAS', '3000036561'),
(2134, 'BADRUL CONSTUCTION', '3000035746'),
(2135, 'MD MUKLESHUR RAHAMAN', '3000040063'),
(2136, 'MOUSUMI MONDAL', '3000038530'),
(2137, 'HUKUMCHAND JAYSAVAL', 'HUKUMCHAND JAYSAVAL'),
(2138, 'NARAYAN MALI', '3000038586'),
(2139, 'GRACE VINTRADE PVT LTD', 'GRACE VINTRADE PVT LTD'),
(2140, 'JAIVIK INFOTECH PRIVATE LIMITED', '3000040059'),
(2141, 'GAUR CONSTRUCTION', '3000040133'),
(2142, 'VAN CONSTRUCTION', '4000002196'),
(2143, 'ADHIKARI ASSOCIATES', '3000037112'),
(2144, 'A S INFRA', '3000040114'),
(2145, 'SANTOSH KUMAR YADAV             ', '3000040073'),
(2146, 'SHAHEE PRAWEEN          ', '3000040074'),
(2147, 'SUSHANTA DEBNATH', '3000040263'),
(2148, 'BALARAM DAS', '3000033460'),
(2149, 'SIULI SANTRA', '3000040164'),
(2150, 'NAJMUL SK', 'NAJMUL SK'),
(2151, 'ANIL KUMAR', '3000040428'),
(2152, 'SURAJ KUMAR SINGH', '3000040101'),
(2153, 'ASHTMOHAN SHARMA', '3000039859'),
(2154, 'SHUTTERING WORK', '254050859202'),
(2155, 'MOHD. KURBA', '3000039960'),
(2156, 'SHRI PAWAR BAPURAO SARJERAO', '4000002412'),
(2157, 'KABITA BHADRA', 'KABITA BHADRA'),
(2158, 'ABDUL MANNAN CHOUDHURY', 'ABDUL MANNAN CHOUDHURY'),
(2159, 'ABDUS SAMAD', '3000040333'),
(2160, 'AAKIB KHAN', '3000040203'),
(2161, 'SUSHIL YADAV   ', '3000040344'),
(2162, 'VIKKY KUMAR', '3000040345'),
(2163, 'PIJUSH BISWAS', '3000040066'),
(2164, 'SAHOO ENTERPRISES', '3000029879'),
(2165, 'ALOK CONSTRUCTION ', '3000040342 '),
(2166, 'SHRIKANT SAH', '3000040201'),
(2167, 'SATYENDRA PRASAD', '3000040202'),
(2168, 'VIJAY YADAV', '3000040204'),
(2169, 'PAMPA MANDAL', '3000040032 '),
(2170, 'SHAGUFATA SAYEED', '3000040033'),
(2171, 'CHANDONA DEVI', '3000040034'),
(2172, 'SUPREME W C S', 'SUPREME W C S'),
(2173, 'MANSOOR ALAM', '3000040353'),
(2174, 'GS CONSTRUCTION', 'GS CONSTRUCTION'),
(2175, 'CRYSTAL BALL', '3000025763'),
(2176, 'AMARCHAND  BAIRWA', '3000038706'),
(2177, 'ARUN RAY', '3000038204'),
(2178, 'CHITRA CHAUHAN', '3000040264'),
(2179, 'BIJAN NAGHAT', '3000040265'),
(2180, 'PRASHOD KUMAR MAHAPATRA', '3000040013'),
(2181, 'ARCHANA KUMARI', '3000040198'),
(2182, 'OM CONSTRUCTION AND MATERIAL       ', '3000040259'),
(2183, 'GOURANGA SAHA', '3000040385'),
(2184, 'NEELAM SANDEEP  YADAV', '3000040388'),
(2185, 'UPENDRA MEHTA', '3000040389'),
(2186, 'M/S SRJ DEVELOPERS', '3000040593'),
(2187, 'WHITEWOOD PROJECTS', '3000040395'),
(2188, 'PRABHAKAR PARASRAM MOHITE', '3000040332'),
(2189, 'EKDANTA FEBCON', '3000040262'),
(2190, 'PIJUS MANNA', '3000040352'),
(2191, 'MARUTI INTERPRISES', '4000002324'),
(2192, 'J & D JHALA CONSTRUCTION', '3000025727'),
(2193, 'IMRAN RAFIQ BHATI       ', '3000040347  '),
(2194, 'JNB ENTERPRISES             ', '4000002440'),
(2195, 'SOUMYA ENTERPRISES  ', '3000040346  '),
(2196, 'SAIDUR RAHMAN', '3000040408'),
(2197, 'SANDEEP KUMAR', '3000040409'),
(2198, 'SAI SHRADDHA TRADERS', '3000039586'),
(2199, 'DISHA MANPOWER & SECURITY MANAGMENT', '4000001936'),
(2200, 'ORIENTAL ASSOCIATES', '3000036625'),
(2201, 'DHARMPAL', '3000025225'),
(2202, 'KAJAL MALLICK', '3000040539'),
(2203, 'VIJAY KUMAR TRIPATHI', '4200026823'),
(2204, 'AMIT SECURITY & ALLIEND SERVICE PVT LTD', '4000003701'),
(2205, 'TECHSTURE TECHNOLOGIES', '3000084669'),
(2206, 'RABINDRANATH JANA', '3000040441'),
(2207, 'ZIAUR RAHAMAN', '3000039816'),
(2208, 'SHRI BALAJI GARMENTS', '3000040291'),
(2209, 'ANITA DEVI', '3000040501'),
(2210, 'SAMSON MALLAIAH ERUKALA', '3000040470'),
(2211, 'KAKADE ASSENMBLY SOLUTIONS & SERVICES', '4000002466'),
(2212, 'MAKE IMPEX', ' 3000040606'),
(2213, 'KASLIWAL INFRATECH', '3000040599'),
(2214, 'SOUMYA ENTERPRISES  ', '3000040346  '),
(2215, ' M/S JAGAT RAJ ', '3000040576'),
(2216, 'NAVNEET KUMAR', '3000039908'),
(2217, 'ABDUL HAMID MONDAL', '3000013754'),
(2218, 'KANKA ALUFORM FABRICATORS', '4000001858'),
(2219, 'P S CONTRACTOR', '3000040327'),
(2220, 'GAUTAM ENTERPRISES', '4000002393'),
(2221, 'MOHD SHAMSHER ALAM', '3000040326'),
(2222, '3000040575', 'RAM BAHADUR'),
(2223, 'RAM BAHADUR', '3000040575'),
(2224, 'SURAJ SINGH', '3000040484'),
(2225, 'RAVI KUMAR SINGH', '3000040234'),
(2226, 'MALKHAN SINGH', '3000038688'),
(2227, 'SHAMIM AKHTAR', '3000040591'),
(2228, 'YOUSUF HOSSAIN', '3000010034'),
(2229, 'SATISH KUMAR', '3000040613'),
(2230, 'POPULAR ENTERPRISE', '150000700'),
(2231, 'M/S CHANDBANU BIBI CONSTRUCTION', 'M/S CHANDBANU BIBI CONSTRUCTION'),
(2232, 'CHITRA PAL SINGH', '2000012497'),
(2233, 'SATYA CONSTRCTION', 'SATYA CONSTRCTION'),
(2234, 'KAJIM SEKH', 'KAJIM SEKH'),
(2235, 'H S ENGINEERS & ASSOCIATES', '3000040512'),
(2236, 'DIL MOHAN GUPTA', '3000040513'),
(2237, 'METAL KRAFT ENGINEERING', 'METAL KRAFT ENGINEERING'),
(2238, 'SHREE  TRADING COMPANY', '3000040508'),
(2239, 'SUKCHAND SHAIKE', '3000040854'),
(2240, 'VEA STONAGE', '3000039519'),
(2241, 'AMIT', '3000040232'),
(2242, 'SHAILENDRA PRASAD', '3000040468'),
(2243, 'ADARSH MACHINERY AND HARDWARE ', '3000040197'),
(2244, 'AJIT KUMAR ', '3000030621'),
(2245, 'AVDESH YADAV', '3000038162'),
(2246, 'BALESHWAR MANDAL', '3000038327'),
(2247, 'BEGUM KHATUN', '3000037340'),
(2248, 'CHANDAN KUMAR', '3000036492'),
(2249, 'DHARMENDRA MAHTO', '3000040432'),
(2250, 'DIPAK KUMAR', '3000040329'),
(2251, 'GANESH SAH', '3000038529'),
(2252, 'GANOUR BHAGAT', '3000009754'),
(2253, 'KALI PADA JAD', '3000040288'),
(2254, 'MAHESH YADAV', '3000031771'),
(2255, 'NRMAL KUMAR', '3000040486'),
(2256, 'PAWAN KUMAR RAY', '3000039742'),
(2257, 'PUNAM KUMARI', '3000039779'),
(2258, 'RAKESH RANJAN', '3000040619'),
(2259, 'RAMSEVAK YADAV', '3000040289'),
(2260, 'RAVTECH ASSOCIATE', '2000013359'),
(2261, 'SHIVSHANKAR KUMAR', '3000035853'),
(2262, 'SHIVSHANKAR YADAV', '3000037503'),
(2263, 'SUDARSHAN PRASAD', '3000006861'),
(2264, 'TRISHA PROJECTS', '3000038088'),
(2265, 'URBAN INFRA', '3000040786         '),
(2267, 'SHANKUTI DEVI', '3000040574'),
(2268, 'MAAHI ENGINEERS AND CONTRACTORS', '3000040720'),
(2269, 'JIBAN SAMADDAR', '3000041023'),
(2270, 'NEXT GENERATION', 'NEXT GENERATION'),
(2271, 'SOMNATH ENTERPRISE', 'SOMNATH ENTERPRISE'),
(2272, 'ARYAN CONSTRUCTION', '3000036524'),
(2273, 'DILCHAND KUMAR', '3000039314'),
(2274, 'JYOTI TIELS ', '4000002202'),
(2275, 'MAHESH YADAV - 2', '3000040438'),
(2276, 'MANISH KUMAR', '3000035516'),
(2277, 'MANISH KUMAR SINGH', '3000039499'),
(2278, 'MANISH YADAV', '3000034121'),
(2279, 'MANTU CONSTRUCTION', '3000040588'),
(2280, 'MOHAMMAD ZAYAUL WAHAB KHAN', '3000039276'),
(2281, 'NESAR ALI SHAH        ', '3000040135'),
(2282, 'NILESH KUMAR', '3000039317'),
(2283, 'NITISH KUMAR', '3000040136'),
(2284, 'PAWAN ENGINEERING', '3000039468'),
(2285, 'RAJNANDINI POWER CONSTRUCTION', '3000037940'),
(2286, 'RANJEET YADAV', '3000031696'),
(2287, 'SANJAY PRASAD', '3000040442'),
(2288, 'SHAILESH KUMAR', '3000035565'),
(2289, 'SHAMBHU DAS', '3000040589'),
(2290, 'UPENDRA KUMAR SAH', '3000039159'),
(2291, 'VISHAL KUMAR PATEL', '3000040461'),
(2292, 'DIKSHA SAHU', '3000040716'),
(2293, 'JAINS POWAR TOOLS & SERVICES', '3000040618'),
(2294, 'RAHUL ENTERPRISES', '3000039971'),
(2295, 'SANCHITA DAS', '3000040862'),
(2296, 'SAI CCONSTRUCTION', '3000040734'),
(2297, 'MUSHRAT SAHIN', '3000037083'),
(2298, 'RADHE KRISHNA TRADERS CO', '3000034475'),
(2299, 'RAVARI DEVI', '3000033537'),
(2300, 'SONALAL SAH', '3000037796'),
(2301, 'NEXT GENERATION', '3000032535'),
(2302, 'SAFED ALI GAZI', '3000040811'),
(2303, 'YEASMIN KHATUN', '3000040812'),
(2304, 'F.K.ENTREPRISES ', '3000040444'),
(2305, 'RAHUL ENGINEERING', '3000040898'),
(2306, 'S.P. ARTS', '3000016115'),
(2307, 'RAHEESUDDEEN ', '3000040873'),
(2308, 'MOHD SAJID ALI', '3000040865'),
(2309, 'EVERSHINE SECURITY AND DETECTIVE SERVICES', '3000040845'),
(2310, 'R.M. TRADERS', '3000040918'),
(2311, 'HARUN RASID', 'HARUN RASID'),
(2312, 'ANUJ YADAV', '3000040526'),
(2313, 'BIRO PASWAN', '3000004331'),
(2314, 'DEELIP', '3000039373'),
(2315, 'DHARMENDRA KUMAR -2', '3000040900'),
(2316, 'MAMTA PANDEY', '3000035393'),
(2317, 'MD SALIM KHAN', '3000040587'),
(2318, 'VIKRAM KUMAR', '3000038888'),
(2319, 'VINOD KUMAR YADAV', '3000039780'),
(2320, 'R.K. DEVELOPERS', '3000040841'),
(2321, 'SOMNATH CONSTRUCTION', 'SOMNATH CONSTRUCTION'),
(2322, 'S.S. MULTI SERVICES', '3000040831'),
(2323, 'KSHIPRA SECURITY AND FACILITY MANAGEMENT SERVICES PRIVATE ', '4000002530'),
(2324, 'KHUSHI SINGH', '3000040973'),
(2325, 'REIDIUS ENGITECH PVT LTD', '4000002050'),
(2326, 'UPENDRA SINGH', 'UPENDRA SINGH'),
(2327, 'YOGENDRA MANDAL ', '3000041003'),
(2328, 'RAM NARESH SINGH & SONS', '2000013505'),
(2329, 'S.G.ENTERPRISES', '3000041011'),
(2330, 'MAHI CONSTRUCTION AND INTERIOR WORKS', '3000041018'),
(2331, 'ISHARAVATI  DEVI', '3000040736'),
(2332, 'VIJAY RAY', '3000040968'),
(2333, 'KHATIBUR RAHMAN', '3000041027'),
(2334, 'INDAR', '3000041088'),
(2335, 'ANOWAR HOSSAIN ', '3000041007'),
(2336, 'SANGITA DEVI', '3000041086'),
(2337, 'B H CONTRACTOR', '3000041112'),
(2338, 'WEATHER SHIELD', '3000040995'),
(2339, 'ANTEQUS CORPORATE', '3000040695'),
(2340, 'M.Z CONSTRUCTION', '3000041084'),
(2341, 'MARK CONSTRUCTION', '3000040698'),
(2342, 'HARUN RASEED', '3000041042'),
(2343, 'MD.ANCHHAR ALI', 'MD.ANCHHAR ALI'),
(2344, 'ANIL KUMAR VAISHY', '3000040945'),
(2345, 'CHANDONA DEVI', '50800004'),
(2346, 'TANU KUMAR PANDIT', '3000040628'),
(2347, 'ANIL YADAV', '3000038262'),
(2348, 'KALPANA DEVI', '3000040426'),
(2349, 'MIRJAHAN ALI', '3000041120'),
(2350, 'RADHEYSHYAM YADAV ', '3000040659'),
(2351, 'MADAN MEHTA ', '3000040661');

-- --------------------------------------------------------

--
-- Table structure for table `oc_contractor_location`
--

CREATE TABLE `oc_contractor_location` (
  `id` int(11) NOT NULL,
  `contractor_id` int(11) NOT NULL DEFAULT '0',
  `unit_id` int(11) NOT NULL DEFAULT '0',
  `unit` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_contractor_location`
--

INSERT INTO `oc_contractor_location` (`id`, `contractor_id`, `unit_id`, `unit`) VALUES
(9, 3, 1, 'AIHB'),
(10, 1, 1, 'AIHB'),
(12, 4, 1, 'AIHB'),
(13, 5, 22, 'THLV'),
(14, 6, 28, 'KLIP'),
(15, 7, 28, 'KLIP'),
(17, 9, 28, 'KLIP'),
(18, 10, 28, 'KLIP'),
(19, 11, 28, 'KLIP'),
(20, 12, 30, 'ATTP'),
(21, 13, 30, 'ATTP'),
(22, 14, 30, 'ATTP'),
(23, 15, 30, 'ATTP'),
(26, 18, 30, 'ATTP'),
(27, 19, 30, 'ATTP'),
(29, 21, 30, 'ATTP'),
(30, 22, 30, 'ATTP'),
(31, 23, 30, 'ATTP'),
(32, 24, 30, 'ATTP'),
(34, 26, 30, 'ATTP'),
(35, 27, 30, 'ATTP'),
(37, 29, 30, 'ATTP'),
(38, 30, 30, 'ATTP'),
(39, 31, 30, 'ATTP'),
(43, 35, 30, 'ATTP'),
(46, 38, 30, 'ATTP'),
(47, 39, 30, 'ATTP'),
(48, 40, 30, 'ATTP'),
(49, 41, 30, 'ATTP'),
(50, 42, 30, 'ATTP'),
(52, 44, 25, 'NIRP'),
(55, 47, 25, 'NIRP'),
(56, 48, 25, 'NIRP'),
(57, 49, 25, 'NIRP'),
(59, 51, 30, 'ATTP'),
(61, 43, 30, 'ATTP'),
(63, 52, 30, 'ATTP'),
(64, 53, 30, 'ATTP'),
(65, 54, 30, 'ATTP'),
(66, 55, 30, 'ATTP'),
(73, 59, 27, 'KLRD'),
(89, 71, 27, 'KLRD'),
(95, 77, 27, 'KLRD'),
(97, 79, 27, 'KLRD'),
(101, 80, 28, 'KLIP'),
(102, 81, 28, 'KLIP'),
(104, 83, 28, 'KLIP'),
(105, 84, 28, 'KLIP'),
(107, 86, 28, 'KLIP'),
(109, 88, 28, 'KLIP'),
(110, 89, 28, 'KLIP'),
(112, 91, 28, 'KLIP'),
(114, 93, 28, 'KLIP'),
(115, 94, 28, 'KLIP'),
(116, 95, 28, 'KLIP'),
(117, 96, 28, 'KLIP'),
(118, 97, 28, 'KLIP'),
(119, 98, 28, 'KLIP'),
(120, 99, 28, 'KLIP'),
(121, 100, 28, 'KLIP'),
(122, 101, 28, 'KLIP'),
(123, 102, 28, 'KLIP'),
(124, 103, 28, 'KLIP'),
(125, 104, 28, 'KLIP'),
(126, 105, 28, 'KLIP'),
(127, 106, 28, 'KLIP'),
(128, 107, 28, 'KLIP'),
(130, 109, 28, 'KLIP'),
(131, 110, 28, 'KLIP'),
(132, 111, 28, 'KLIP'),
(133, 112, 28, 'KLIP'),
(134, 113, 28, 'KLIP'),
(135, 114, 28, 'KLIP'),
(137, 108, 28, 'KLIP'),
(142, 64, 27, 'KLRD'),
(144, 78, 27, 'KLRD'),
(147, 134, 28, 'KLIP'),
(148, 135, 28, 'KLIP'),
(149, 136, 28, 'KLIP'),
(150, 137, 28, 'KLIP'),
(154, 118, 27, 'KLRD'),
(158, 141, 30, 'ATTP'),
(159, 148, 30, 'ATTP'),
(160, 138, 30, 'ATTP'),
(161, 142, 30, 'ATTP'),
(162, 143, 30, 'ATTP'),
(163, 149, 30, 'ATTP'),
(166, 139, 30, 'ATTP'),
(167, 151, 30, 'ATTP'),
(168, 152, 13, 'KVNN'),
(169, 153, 13, 'KVNN'),
(171, 155, 13, 'KVNN'),
(172, 156, 13, 'KVNN'),
(173, 157, 13, 'KVNN'),
(174, 158, 30, 'ATTP'),
(175, 159, 30, 'ATTP'),
(176, 164, 1, 'AIHB'),
(181, 115, 27, 'KLRD'),
(182, 115, 28, 'KLIP'),
(183, 165, 30, 'ATTP'),
(186, 168, 13, 'KVNN'),
(187, 169, 13, 'KVNN'),
(188, 170, 13, 'KVNN'),
(189, 171, 13, 'KVNN'),
(190, 172, 13, 'KVNN'),
(191, 173, 13, 'KVNN'),
(193, 174, 14, 'MMHP'),
(194, 175, 14, 'MMHP'),
(195, 176, 15, 'NLDA'),
(196, 177, 15, 'NLDA'),
(197, 178, 15, 'NLDA'),
(198, 179, 15, 'NLDA'),
(199, 180, 15, 'NLDA'),
(200, 181, 18, 'RECG'),
(201, 182, 18, 'RECG'),
(203, 184, 13, 'KVNN'),
(204, 185, 13, 'KVNN'),
(205, 186, 13, 'KVNN'),
(206, 187, 13, 'KVNN'),
(207, 188, 28, 'KLIP'),
(208, 189, 17, 'PHPG'),
(209, 190, 17, 'PHPG'),
(211, 192, 22, 'THLV'),
(212, 193, 22, 'THLV'),
(215, 196, 22, 'THLV'),
(216, 197, 22, 'THLV'),
(217, 198, 22, 'THLV'),
(218, 199, 22, 'THLV'),
(219, 200, 22, 'THLV'),
(220, 201, 22, 'THLV'),
(221, 202, 22, 'THLV'),
(222, 203, 6, 'EMGG'),
(224, 205, 6, 'EMGG'),
(226, 207, 6, 'EMGG'),
(227, 208, 6, 'EMGG'),
(228, 209, 6, 'EMGG'),
(229, 210, 6, 'EMGG'),
(230, 211, 6, 'EMGG'),
(233, 214, 6, 'EMGG'),
(234, 215, 6, 'EMGG'),
(235, 216, 6, 'EMGG'),
(236, 217, 6, 'EMGG'),
(237, 218, 6, 'EMGG'),
(238, 219, 6, 'EMGG'),
(239, 220, 6, 'EMGG'),
(240, 221, 6, 'EMGG'),
(241, 222, 6, 'EMGG'),
(242, 223, 6, 'EMGG'),
(244, 225, 6, 'EMGG'),
(245, 226, 6, 'EMGG'),
(246, 227, 6, 'EMGG'),
(247, 228, 6, 'EMGG'),
(248, 154, 13, 'KVNN'),
(250, 230, 1, 'AIHB'),
(251, 2, 1, 'AIHB'),
(252, 231, 28, 'KLIP'),
(254, 233, 27, 'KLRD'),
(255, 234, 1, 'AIHB'),
(256, 235, 1, 'AIHB'),
(257, 236, 1, 'AIHB'),
(258, 237, 14, 'MMHP'),
(259, 238, 14, 'MMHP'),
(260, 239, 13, 'KVNN'),
(261, 240, 13, 'KVNN'),
(262, 241, 13, 'KVNN'),
(263, 144, 30, 'ATTP'),
(264, 242, 30, 'ATTP'),
(265, 243, 1, 'AIHB'),
(266, 90, 28, 'KLIP'),
(267, 244, 13, 'KVNN'),
(268, 245, 20, 'SCAO'),
(269, 246, 20, 'SCAO'),
(270, 247, 15, 'NLDA'),
(272, 249, 15, 'NLDA'),
(273, 250, 30, 'ATTP'),
(274, 251, 30, 'ATTP'),
(275, 252, 30, 'ATTP'),
(277, 255, 6, 'EMGG'),
(278, 256, 6, 'EMGG'),
(280, 258, 6, 'EMGG'),
(281, 259, 6, 'EMGG'),
(282, 260, 14, 'MMHP'),
(283, 261, 14, 'MMHP'),
(284, 262, 30, 'ATTP'),
(285, 263, 30, 'ATTP'),
(286, 264, 30, 'ATTP'),
(287, 265, 30, 'ATTP'),
(288, 266, 13, 'KVNN'),
(290, 268, 22, 'THLV'),
(291, 269, 22, 'THLV'),
(292, 270, 22, 'THLV'),
(293, 271, 22, 'THLV'),
(294, 272, 22, 'THLV'),
(296, 274, 22, 'THLV'),
(297, 275, 22, 'THLV'),
(299, 277, 22, 'THLV'),
(300, 278, 22, 'THLV'),
(301, 279, 22, 'THLV'),
(302, 280, 22, 'THLV'),
(303, 281, 22, 'THLV'),
(304, 282, 22, 'THLV'),
(305, 283, 22, 'THLV'),
(306, 284, 22, 'THLV'),
(307, 285, 22, 'THLV'),
(308, 286, 22, 'THLV'),
(309, 287, 22, 'THLV'),
(310, 288, 22, 'THLV'),
(311, 289, 22, 'THLV'),
(313, 291, 22, 'THLV'),
(314, 292, 22, 'THLV'),
(315, 293, 22, 'THLV'),
(317, 295, 22, 'THLV'),
(320, 298, 22, 'THLV'),
(321, 299, 22, 'THLV'),
(322, 300, 22, 'THLV'),
(323, 301, 22, 'THLV'),
(324, 302, 22, 'THLV'),
(325, 303, 22, 'THLV'),
(326, 304, 22, 'THLV'),
(327, 305, 22, 'THLV'),
(330, 308, 22, 'THLV'),
(331, 309, 22, 'THLV'),
(332, 310, 22, 'THLV'),
(333, 311, 22, 'THLV'),
(334, 312, 28, 'KLIP'),
(336, 314, 14, 'MMHP'),
(337, 315, 14, 'MMHP'),
(338, 316, 18, 'RECG'),
(339, 317, 18, 'RECG'),
(340, 318, 6, 'EMGG'),
(341, 319, 6, 'EMGG'),
(344, 320, 20, 'SCAO'),
(345, 321, 20, 'SCAO'),
(346, 322, 20, 'SCAO'),
(347, 323, 20, 'SCAO'),
(348, 324, 20, 'SCAO'),
(349, 325, 22, 'THLV'),
(350, 129, 26, 'MLD2'),
(354, 45, 25, 'NIRP'),
(355, 45, 26, 'MLD2'),
(358, 128, 26, 'MLD2'),
(363, 127, 26, 'MLD2'),
(364, 131, 26, 'MLD2'),
(365, 126, 26, 'MLD2'),
(366, 326, 13, 'KVNN'),
(367, 327, 13, 'KVNN'),
(370, 330, 19, 'RSNO'),
(371, 163, 28, 'KLIP'),
(374, 333, 42, 'KRGN'),
(375, 334, 40, 'NMDC'),
(376, 335, 40, 'NMDC'),
(382, 341, 42, 'KRGN'),
(383, 342, 43, 'GSL1'),
(385, 344, 40, 'NMDC'),
(386, 345, 22, 'THLV'),
(387, 346, 6, 'EMGG'),
(389, 348, 18, 'RECG'),
(390, 349, 18, 'RECG'),
(391, 350, 14, 'MMHP'),
(396, 351, 22, 'THLV'),
(399, 354, 43, 'GSL1'),
(400, 355, 43, 'GSL1'),
(401, 356, 43, 'GSL1'),
(402, 357, 43, 'GSL1'),
(403, 358, 40, 'NMDC'),
(404, 359, 40, 'NMDC'),
(406, 360, 40, 'NMDC'),
(409, 363, 40, 'NMDC'),
(411, 364, 40, 'NMDC'),
(412, 365, 40, 'NMDC'),
(413, 366, 40, 'NMDC'),
(419, 372, 40, 'NMDC'),
(421, 374, 40, 'NMDC'),
(423, 368, 40, 'NMDC'),
(424, 376, 30, 'ATTP'),
(425, 377, 30, 'ATTP'),
(427, 213, 6, 'EMGG'),
(428, 213, 39, 'NTPC'),
(431, 381, 39, 'NTPC'),
(433, 383, 39, 'NTPC'),
(434, 384, 48, 'MGLF'),
(435, 385, 48, 'MGLF'),
(436, 386, 48, 'MGLF'),
(437, 387, 48, 'MGLF'),
(439, 389, 48, 'MGLF'),
(440, 390, 48, 'MGLF'),
(441, 391, 48, 'MGLF'),
(442, 392, 48, 'MGLF'),
(445, 395, 48, 'MGLF'),
(446, 396, 48, 'MGLF'),
(447, 397, 48, 'MGLF'),
(448, 398, 48, 'MGLF'),
(450, 400, 42, 'KRGN'),
(452, 402, 42, 'KRGN'),
(453, 403, 42, 'KRGN'),
(454, 404, 42, 'KRGN'),
(459, 405, 42, 'KRGN'),
(461, 407, 42, 'KRGN'),
(462, 408, 42, 'KRGN'),
(463, 409, 42, 'KRGN'),
(464, 410, 42, 'KRGN'),
(466, 412, 42, 'KRGN'),
(468, 414, 42, 'KRGN'),
(469, 415, 42, 'KRGN'),
(471, 417, 42, 'KRGN'),
(472, 418, 42, 'KRGN'),
(473, 419, 42, 'KRGN'),
(474, 420, 42, 'KRGN'),
(475, 421, 42, 'KRGN'),
(476, 422, 42, 'KRGN'),
(478, 424, 42, 'KRGN'),
(480, 426, 42, 'KRGN'),
(482, 428, 14, 'MMHP'),
(483, 429, 14, 'MMHP'),
(484, 430, 14, 'MMHP'),
(485, 431, 14, 'MMHP'),
(486, 432, 51, 'LTFO'),
(487, 433, 51, 'LTFO'),
(488, 388, 48, 'MGLF'),
(489, 388, 51, 'LTFO'),
(490, 434, 51, 'LTFO'),
(491, 435, 51, 'LTFO'),
(492, 436, 51, 'LTFO'),
(493, 437, 51, 'LTFO'),
(496, 440, 51, 'LTFO'),
(500, 441, 52, 'STFO'),
(501, 442, 52, 'STFO'),
(502, 443, 52, 'STFO'),
(503, 444, 52, 'STFO'),
(506, 439, 51, 'LTFO'),
(507, 439, 52, 'STFO'),
(508, 447, 52, 'STFO'),
(510, 449, 40, 'NMDC'),
(511, 450, 40, 'NMDC'),
(513, 452, 40, 'NMDC'),
(514, 453, 40, 'NMDC'),
(517, 456, 40, 'NMDC'),
(519, 458, 40, 'NMDC'),
(520, 459, 43, 'GSL1'),
(521, 460, 43, 'GSL1'),
(523, 462, 43, 'GSL1'),
(524, 463, 43, 'GSL1'),
(525, 464, 43, 'GSL1'),
(526, 465, 43, 'GSL1'),
(528, 467, 42, 'KRGN'),
(529, 468, 42, 'KRGN'),
(531, 470, 42, 'KRGN'),
(532, 471, 42, 'KRGN'),
(533, 472, 42, 'KRGN'),
(534, 473, 42, 'KRGN'),
(536, 475, 48, 'MGLF'),
(537, 476, 48, 'MGLF'),
(538, 477, 48, 'MGLF'),
(539, 478, 20, 'SCAO'),
(540, 332, 42, 'KRGN'),
(541, 479, 40, 'NMDC'),
(542, 480, 40, 'NMDC'),
(543, 481, 17, 'PHPG'),
(544, 482, 17, 'PHPG'),
(545, 483, 17, 'PHPG'),
(547, 485, 17, 'PHPG'),
(548, 486, 17, 'PHPG'),
(550, 488, 2, 'AHRB'),
(551, 489, 6, 'EMGG'),
(553, 491, 17, 'PHPG'),
(555, 373, 40, 'NMDC'),
(558, 369, 40, 'NMDC'),
(559, 492, 42, 'KRGN'),
(560, 493, 28, 'KLIP'),
(561, 494, 30, 'ATTP'),
(562, 495, 13, 'KVNN'),
(563, 496, 17, 'PHPG'),
(564, 497, 17, 'PHPG'),
(565, 498, 17, 'PHPG'),
(566, 499, 30, 'ATTP'),
(567, 500, 53, 'MCFO'),
(568, 501, 53, 'MCFO'),
(569, 502, 53, 'MCFO'),
(570, 503, 53, 'MCFO'),
(571, 504, 53, 'MCFO'),
(572, 505, 53, 'MCFO'),
(573, 506, 53, 'MCFO'),
(574, 507, 53, 'MCFO'),
(575, 508, 53, 'MCFO'),
(576, 509, 53, 'MCFO'),
(577, 510, 53, 'MCFO'),
(578, 511, 53, 'MCFO'),
(579, 438, 51, 'LTFO'),
(580, 438, 53, 'MCFO'),
(581, 512, 53, 'MCFO'),
(582, 513, 53, 'MCFO'),
(583, 514, 53, 'MCFO'),
(584, 515, 53, 'MCFO'),
(585, 516, 53, 'MCFO'),
(586, 517, 53, 'MCFO'),
(587, 518, 53, 'MCFO'),
(588, 519, 53, 'MCFO'),
(589, 520, 4, 'AIHR'),
(591, 522, 4, 'AIHR'),
(592, 523, 4, 'AIHR'),
(593, 524, 4, 'AIHR'),
(594, 525, 4, 'AIHR'),
(595, 526, 40, 'NMDC'),
(596, 527, 40, 'NMDC'),
(597, 528, 40, 'NMDC'),
(601, 529, 40, 'NMDC'),
(603, 530, 40, 'NMDC'),
(605, 532, 40, 'NMDC'),
(606, 533, 40, 'NMDC'),
(607, 534, 40, 'NMDC'),
(609, 536, 40, 'NMDC'),
(610, 537, 40, 'NMDC'),
(611, 538, 40, 'NMDC'),
(612, 539, 40, 'NMDC'),
(613, 540, 40, 'NMDC'),
(614, 541, 14, 'MMHP'),
(615, 542, 14, 'MMHP'),
(616, 543, 14, 'MMHP'),
(618, 545, 14, 'MMHP'),
(619, 546, 14, 'MMHP'),
(620, 547, 14, 'MMHP'),
(621, 548, 14, 'MMHP'),
(622, 549, 14, 'MMHP'),
(625, 552, 14, 'MMHP'),
(626, 553, 14, 'MMHP'),
(627, 554, 14, 'MMHP'),
(628, 555, 14, 'MMHP'),
(629, 556, 14, 'MMHP'),
(630, 557, 14, 'MMHP'),
(631, 558, 14, 'MMHP'),
(632, 559, 14, 'MMHP'),
(633, 560, 14, 'MMHP'),
(634, 561, 14, 'MMHP'),
(636, 563, 14, 'MMHP'),
(637, 564, 14, 'MMHP'),
(638, 565, 14, 'MMHP'),
(639, 566, 14, 'MMHP'),
(640, 567, 14, 'MMHP'),
(642, 569, 14, 'MMHP'),
(643, 570, 30, 'ATTP'),
(644, 571, 30, 'ATTP'),
(645, 572, 54, 'NRDA'),
(647, 574, 55, 'BWSP'),
(648, 575, 55, 'BWSP'),
(650, 577, 26, 'MLD2'),
(651, 578, 28, 'KLIP'),
(652, 579, 28, 'KLIP'),
(653, 580, 40, 'NMDC'),
(654, 581, 28, 'KLIP'),
(655, 582, 40, 'NMDC'),
(656, 583, 40, 'NMDC'),
(657, 584, 40, 'NMDC'),
(663, 585, 14, 'MMHP'),
(664, 586, 6, 'EMGG'),
(666, 588, 40, 'NMDC'),
(668, 590, 20, 'SCAO'),
(669, 591, 42, 'KRGN'),
(673, 594, 45, 'CUBG'),
(674, 595, 45, 'CUBG'),
(675, 596, 45, 'CUBG'),
(676, 597, 45, 'CUBG'),
(677, 598, 45, 'CUBG'),
(679, 600, 45, 'CUBG'),
(680, 601, 45, 'CUBG'),
(681, 602, 45, 'CUBG'),
(682, 603, 45, 'CUBG'),
(683, 604, 45, 'CUBG'),
(684, 605, 45, 'CUBG'),
(685, 606, 45, 'CUBG'),
(686, 607, 45, 'CUBG'),
(687, 608, 45, 'CUBG'),
(688, 609, 45, 'CUBG'),
(689, 610, 45, 'CUBG'),
(690, 611, 50, 'BHFSCASTINGYARD'),
(691, 474, 48, 'MGLF'),
(692, 474, 50, 'BHFSCASTINGYARD'),
(693, 612, 50, 'BHFSCASTINGYARD'),
(694, 613, 50, 'BHFSCASTINGYARD'),
(695, 393, 48, 'MGLF'),
(696, 393, 50, 'BHFSCASTINGYARD'),
(697, 614, 50, 'BHFSCASTINGYARD'),
(704, 615, 52, 'STFO'),
(705, 616, 52, 'STFO'),
(706, 617, 52, 'STFO'),
(707, 618, 43, 'GSL1'),
(708, 619, 42, 'KRGN'),
(709, 620, 42, 'KRGN'),
(710, 338, 40, 'NMDC'),
(711, 338, 42, 'KRGN'),
(713, 621, 14, 'MMHP'),
(715, 623, 40, 'NMDC'),
(718, 622, 40, 'NMDC'),
(719, 624, 13, 'KVNN'),
(721, 626, 35, 'TSL1'),
(722, 627, 35, 'TSL1'),
(723, 628, 35, 'TSL1'),
(724, 629, 35, 'TSL1'),
(725, 630, 35, 'TSL1'),
(726, 631, 35, 'TSL1'),
(728, 633, 35, 'TSL1'),
(729, 634, 35, 'TSL1'),
(730, 635, 35, 'TSL1'),
(731, 636, 35, 'TSL1'),
(732, 637, 35, 'TSL1'),
(733, 638, 35, 'TSL1'),
(734, 639, 35, 'TSL1'),
(735, 640, 35, 'TSL1'),
(736, 641, 35, 'TSL1'),
(739, 644, 35, 'TSL1'),
(741, 646, 35, 'TSL1'),
(742, 647, 35, 'TSL1'),
(743, 648, 35, 'TSL1'),
(744, 28, 30, 'ATTP'),
(745, 649, 35, 'TSL1'),
(746, 650, 35, 'TSL1'),
(747, 651, 15, 'NLDA'),
(750, 652, 43, 'GSL1'),
(751, 653, 43, 'GSL1'),
(752, 654, 25, 'NIRP'),
(753, 655, 25, 'NIRP'),
(755, 657, 14, 'MMHP'),
(756, 658, 14, 'MMHP'),
(758, 660, 22, 'THLV'),
(759, 661, 44, 'ERSK'),
(761, 663, 42, 'KRGN'),
(762, 664, 43, 'GSL1'),
(763, 587, 6, 'EMGG'),
(764, 587, 22, 'THLV'),
(765, 665, 22, 'THLV'),
(766, 130, 26, 'MLD2'),
(769, 666, 40, 'NMDC'),
(770, 667, 40, 'NMDC'),
(771, 668, 40, 'NMDC'),
(772, 375, 40, 'NMDC'),
(773, 669, 40, 'NMDC'),
(774, 670, 43, 'GSL1'),
(778, 671, 18, 'RECG'),
(779, 672, 14, 'MMHP'),
(780, 673, 20, 'SCAO'),
(781, 413, 42, 'KRGN'),
(782, 674, 19, 'RSNO'),
(783, 675, 40, 'NMDC'),
(784, 676, 40, 'NMDC'),
(785, 370, 40, 'NMDC'),
(786, 677, 35, 'TSL1'),
(787, 678, 35, 'TSL1'),
(788, 679, 35, 'TSL1'),
(789, 680, 35, 'TSL1'),
(790, 681, 35, 'TSL1'),
(791, 682, 35, 'TSL1'),
(792, 683, 35, 'TSL1'),
(793, 684, 35, 'TSL1'),
(794, 685, 35, 'TSL1'),
(795, 686, 35, 'TSL1'),
(796, 340, 42, 'KRGN'),
(801, 687, 22, 'THLV'),
(803, 689, 22, 'THLV'),
(806, 692, 43, 'GSL1'),
(808, 694, 14, 'MMHP'),
(809, 297, 14, 'MMHP'),
(810, 297, 22, 'THLV'),
(811, 695, 30, 'ATTP'),
(813, 697, 35, 'TSL1'),
(814, 698, 39, 'NTPC'),
(817, 701, 39, 'NTPC'),
(818, 702, 39, 'NTPC'),
(819, 703, 35, 'TSL1'),
(820, 411, 39, 'NTPC'),
(821, 411, 42, 'KRGN'),
(822, 411, 43, 'GSL1'),
(823, 704, 39, 'NTPC'),
(825, 706, 39, 'NTPC'),
(827, 708, 39, 'NTPC'),
(828, 709, 39, 'NTPC'),
(829, 710, 39, 'NTPC'),
(830, 711, 39, 'NTPC'),
(831, 712, 39, 'NTPC'),
(832, 713, 39, 'NTPC'),
(833, 714, 39, 'NTPC'),
(837, 715, 6, 'EMGG'),
(838, 716, 22, 'THLV'),
(839, 642, 35, 'TSL1'),
(840, 371, 40, 'NMDC'),
(841, 371, 42, 'KRGN'),
(842, 717, 42, 'KRGN'),
(843, 718, 40, 'NMDC'),
(844, 719, 40, 'NMDC'),
(845, 720, 40, 'NMDC'),
(846, 721, 40, 'NMDC'),
(847, 722, 40, 'NMDC'),
(849, 723, 42, 'KRGN'),
(851, 725, 13, 'KVNN'),
(853, 727, 43, 'GSL1'),
(854, 728, 43, 'GSL1'),
(855, 729, 43, 'GSL1'),
(856, 730, 15, 'NLDA'),
(857, 731, 45, 'CUBG'),
(858, 732, 45, 'CUBG'),
(859, 733, 30, 'ATTP'),
(860, 734, 51, 'LTFO'),
(861, 735, 51, 'LTFO'),
(862, 736, 51, 'LTFO'),
(865, 737, 14, 'MMHP'),
(866, 738, 14, 'MMHP'),
(867, 739, 14, 'MMHP'),
(868, 740, 40, 'NMDC'),
(869, 741, 40, 'NMDC'),
(870, 742, 40, 'NMDC'),
(871, 743, 40, 'NMDC'),
(872, 744, 40, 'NMDC'),
(873, 745, 40, 'NMDC'),
(874, 589, 40, 'NMDC'),
(875, 589, 54, 'NRDA'),
(878, 416, 40, 'NMDC'),
(879, 416, 42, 'KRGN'),
(880, 361, 40, 'NMDC'),
(882, 747, 26, 'MLD2'),
(883, 748, 26, 'MLD2'),
(884, 749, 40, 'NMDC'),
(885, 750, 43, 'GSL1'),
(886, 751, 27, 'KLRD'),
(887, 752, 53, 'MCFO'),
(888, 753, 2, 'AHRB'),
(889, 754, 2, 'AHRB'),
(890, 212, 6, 'EMGG'),
(891, 212, 22, 'THLV'),
(892, 755, 40, 'NMDC'),
(893, 756, 40, 'NMDC'),
(894, 757, 13, 'KVNN'),
(895, 758, 13, 'KVNN'),
(896, 759, 13, 'KVNN'),
(897, 760, 42, 'KRGN'),
(898, 761, 42, 'KRGN'),
(899, 762, 43, 'GSL1'),
(900, 763, 43, 'GSL1'),
(901, 764, 43, 'GSL1'),
(903, 766, 43, 'GSL1'),
(904, 767, 43, 'GSL1'),
(905, 768, 43, 'GSL1'),
(906, 769, 43, 'GSL1'),
(907, 461, 43, 'GSL1'),
(908, 770, 40, 'NMDC'),
(909, 771, 35, 'TSL1'),
(910, 772, 35, 'TSL1'),
(912, 774, 14, 'MMHP'),
(913, 775, 14, 'MMHP'),
(914, 776, 35, 'TSL1'),
(915, 773, 35, 'TSL1'),
(916, 777, 35, 'TSL1'),
(917, 778, 35, 'TSL1'),
(920, 691, 42, 'KRGN'),
(921, 691, 56, 'MAIP'),
(922, 779, 43, 'GSL1'),
(923, 780, 43, 'GSL1'),
(924, 781, 35, 'TSL1'),
(925, 782, 35, 'TSL1'),
(926, 783, 14, 'MMHP'),
(927, 784, 22, 'THLV'),
(928, 785, 22, 'THLV'),
(929, 786, 22, 'THLV'),
(930, 787, 22, 'THLV'),
(931, 788, 13, 'KVNN'),
(932, 789, 13, 'KVNN'),
(934, 790, 35, 'TSL1'),
(935, 791, 28, 'KLIP'),
(936, 82, 28, 'KLIP'),
(937, 132, 28, 'KLIP'),
(938, 401, 40, 'NMDC'),
(939, 401, 42, 'KRGN'),
(940, 792, 40, 'NMDC'),
(941, 793, 40, 'NMDC'),
(942, 794, 40, 'NMDC'),
(943, 795, 40, 'NMDC'),
(944, 796, 30, 'ATTP'),
(945, 797, 26, 'MLD2'),
(946, 798, 15, 'NLDA'),
(947, 799, 15, 'NLDA'),
(948, 800, 15, 'NLDA'),
(949, 801, 15, 'NLDA'),
(950, 802, 42, 'KRGN'),
(951, 803, 42, 'KRGN'),
(952, 804, 43, 'GSL1'),
(953, 805, 13, 'KVNN'),
(954, 806, 13, 'KVNN'),
(956, 808, 22, 'THLV'),
(957, 809, 22, 'THLV'),
(959, 811, 20, 'SCAO'),
(960, 812, 42, 'KRGN'),
(961, 813, 42, 'KRGN'),
(963, 815, 35, 'TSL1'),
(964, 816, 35, 'TSL1'),
(965, 817, 35, 'TSL1'),
(966, 818, 28, 'KLIP'),
(967, 819, 28, 'KLIP'),
(968, 820, 28, 'KLIP'),
(969, 821, 28, 'KLIP'),
(970, 822, 28, 'KLIP'),
(971, 823, 28, 'KLIP'),
(973, 825, 22, 'THLV'),
(974, 826, 40, 'NMDC'),
(975, 827, 40, 'NMDC'),
(976, 828, 40, 'NMDC'),
(977, 829, 40, 'NMDC'),
(978, 830, 40, 'NMDC'),
(979, 831, 40, 'NMDC'),
(980, 832, 43, 'GSL1'),
(981, 833, 43, 'GSL1'),
(982, 834, 39, 'NTPC'),
(985, 835, 39, 'NTPC'),
(986, 836, 39, 'NTPC'),
(987, 837, 39, 'NTPC'),
(989, 839, 39, 'NTPC'),
(990, 840, 39, 'NTPC'),
(991, 841, 51, 'LTFO'),
(992, 842, 50, 'BHFSCASTINGYARD'),
(993, 843, 50, 'BHFSCASTINGYARD'),
(994, 844, 50, 'BHFSCASTINGYARD'),
(995, 845, 57, 'WCPP'),
(997, 847, 48, 'MGLF'),
(998, 848, 58, 'MBWS'),
(999, 849, 35, 'TSL1'),
(1000, 850, 13, 'KVNN'),
(1001, 851, 13, 'KVNN'),
(1002, 852, 13, 'KVNN'),
(1003, 853, 13, 'KVNN'),
(1005, 855, 13, 'KVNN'),
(1006, 856, 35, 'TSL1'),
(1007, 857, 35, 'TSL1'),
(1008, 858, 35, 'TSL1'),
(1009, 859, 22, 'THLV'),
(1010, 860, 22, 'THLV'),
(1011, 861, 22, 'THLV'),
(1012, 862, 22, 'THLV'),
(1013, 863, 15, 'NLDA'),
(1014, 864, 15, 'NLDA'),
(1015, 865, 40, 'NMDC'),
(1016, 866, 40, 'NMDC'),
(1017, 867, 40, 'NMDC'),
(1018, 868, 40, 'NMDC'),
(1019, 869, 22, 'THLV'),
(1020, 870, 28, 'KLIP'),
(1021, 871, 28, 'KLIP'),
(1022, 872, 35, 'TSL1'),
(1025, 380, 39, 'NTPC'),
(1029, 874, 30, 'ATTP'),
(1031, 876, 43, 'GSL1'),
(1032, 877, 43, 'GSL1'),
(1033, 878, 43, 'GSL1'),
(1034, 531, 39, 'NTPC'),
(1035, 879, 35, 'TSL1'),
(1036, 204, 6, 'EMGG'),
(1037, 204, 14, 'MMHP'),
(1038, 204, 15, 'NLDA'),
(1039, 20, 26, 'MLD2'),
(1040, 20, 28, 'KLIP'),
(1041, 20, 30, 'ATTP'),
(1044, 880, 2, 'AHRB'),
(1045, 881, 35, 'TSL1'),
(1046, 882, 40, 'NMDC'),
(1047, 883, 40, 'NMDC'),
(1049, 885, 53, 'MCFO'),
(1050, 886, 53, 'MCFO'),
(1051, 887, 53, 'MCFO'),
(1052, 888, 53, 'MCFO'),
(1053, 889, 53, 'MCFO'),
(1054, 890, 53, 'MCFO'),
(1055, 891, 53, 'MCFO'),
(1056, 892, 53, 'MCFO'),
(1057, 893, 53, 'MCFO'),
(1058, 894, 53, 'MCFO'),
(1059, 895, 53, 'MCFO'),
(1060, 896, 43, 'GSL1'),
(1069, 897, 57, 'WCPP'),
(1070, 898, 28, 'KLIP'),
(1071, 899, 28, 'KLIP'),
(1072, 900, 28, 'KLIP'),
(1073, 901, 28, 'KLIP'),
(1074, 902, 28, 'KLIP'),
(1078, 905, 22, 'THLV'),
(1080, 907, 22, 'THLV'),
(1081, 908, 40, 'NMDC'),
(1086, 909, 43, 'GSL1'),
(1087, 910, 13, 'KVNN'),
(1088, 911, 40, 'NMDC'),
(1089, 912, 40, 'NMDC'),
(1094, 913, 35, 'TSL1'),
(1095, 914, 35, 'TSL1'),
(1096, 915, 39, 'NTPC'),
(1098, 917, 40, 'NMDC'),
(1099, 427, 40, 'NMDC'),
(1100, 427, 42, 'KRGN'),
(1101, 918, 42, 'KRGN'),
(1103, 920, 53, 'MCFO'),
(1104, 921, 53, 'MCFO'),
(1105, 922, 14, 'MMHP'),
(1106, 923, 48, 'MGLF'),
(1107, 924, 48, 'MGLF'),
(1110, 925, 4, 'AIHR'),
(1111, 926, 28, 'KLIP'),
(1113, 928, 28, 'KLIP'),
(1114, 929, 22, 'THLV'),
(1115, 930, 28, 'KLIP'),
(1116, 931, 18, 'RECG'),
(1117, 932, 35, 'TSL1'),
(1118, 933, 42, 'KRGN'),
(1119, 353, 43, 'GSL1'),
(1121, 934, 25, 'NIRP'),
(1122, 934, 26, 'MLD2'),
(1124, 935, 35, 'TSL1'),
(1125, 936, 51, 'LTFO'),
(1126, 936, 52, 'STFO'),
(1133, 707, 39, 'NTPC'),
(1134, 707, 40, 'NMDC'),
(1135, 938, 40, 'NMDC'),
(1137, 939, 59, 'MJIP'),
(1138, 940, 22, 'THLV'),
(1139, 941, 22, 'THLV'),
(1140, 942, 22, 'THLV'),
(1141, 943, 22, 'THLV'),
(1142, 944, 22, 'THLV'),
(1143, 945, 22, 'THLV'),
(1144, 946, 6, 'EMGG'),
(1145, 947, 40, 'NMDC'),
(1146, 948, 40, 'NMDC'),
(1147, 949, 35, 'TSL1'),
(1148, 950, 43, 'GSL1'),
(1150, 952, 13, 'KVNN'),
(1151, 953, 14, 'MMHP'),
(1152, 954, 14, 'MMHP'),
(1154, 956, 22, 'THLV'),
(1155, 957, 22, 'THLV'),
(1156, 958, 50, 'BHFS'),
(1158, 960, 28, 'KLIP'),
(1159, 961, 28, 'KLIP'),
(1160, 962, 35, 'TSL1'),
(1162, 964, 28, 'KLIP'),
(1163, 965, 22, 'THLV'),
(1164, 966, 13, 'KVNN'),
(1165, 967, 13, 'KVNN'),
(1167, 969, 48, 'MGLF'),
(1168, 969, 49, 'MGLFCASTINGYARD'),
(1169, 970, 13, 'KVNN'),
(1170, 971, 13, 'KVNN'),
(1171, 972, 28, 'KLIP'),
(1172, 973, 28, 'KLIP'),
(1173, 974, 28, 'KLIP'),
(1174, 975, 28, 'KLIP'),
(1175, 976, 30, 'ATTP'),
(1176, 977, 30, 'ATTP'),
(1177, 978, 26, 'MLD2'),
(1178, 979, 43, 'GSL1'),
(1180, 981, 35, 'TSL1'),
(1181, 982, 35, 'TSL1'),
(1182, 983, 22, 'THLV'),
(1185, 986, 22, 'THLV'),
(1186, 987, 22, 'THLV'),
(1187, 988, 22, 'THLV'),
(1188, 989, 22, 'THLV'),
(1189, 990, 22, 'THLV'),
(1190, 991, 13, 'KVNN'),
(1191, 992, 13, 'KVNN'),
(1192, 993, 13, 'KVNN'),
(1193, 994, 18, 'RECG'),
(1194, 995, 40, 'NMDC'),
(1195, 996, 40, 'NMDC'),
(1196, 997, 40, 'NMDC'),
(1197, 998, 40, 'NMDC'),
(1198, 999, 40, 'NMDC'),
(1199, 1000, 14, 'MMHP'),
(1200, 1001, 40, 'NMDC'),
(1202, 1003, 49, 'MGLFCASTINGYARD'),
(1203, 1004, 49, 'MGLFCASTINGYARD'),
(1204, 1005, 49, 'MGLFCASTINGYARD'),
(1205, 1006, 49, 'MGLFCASTINGYARD'),
(1206, 1007, 35, 'TSL1'),
(1207, 1008, 35, 'TSL1'),
(1208, 1009, 49, 'MGLFCASTINGYARD'),
(1211, 232, 27, 'KLRD'),
(1212, 232, 30, 'ATTP'),
(1213, 1010, 39, 'NTPC'),
(1214, 116, 27, 'KLRD'),
(1215, 116, 30, 'ATTP'),
(1217, 1012, 30, 'ATTP'),
(1219, 1014, 57, 'WCPP'),
(1220, 34, 28, 'KLIP'),
(1221, 34, 30, 'ATTP'),
(1222, 161, 28, 'KLIP'),
(1229, 906, 21, 'THGG'),
(1230, 906, 22, 'THLV'),
(1232, 1015, 27, 'KLRD'),
(1233, 1016, 28, 'KLIP'),
(1234, 1017, 28, 'KLIP'),
(1235, 61, 27, 'KLRD'),
(1236, 61, 28, 'KLIP'),
(1237, 1018, 48, 'MGLF'),
(1238, 1019, 48, 'MGLF'),
(1239, 1020, 35, 'TSL1'),
(1240, 1021, 35, 'TSL1'),
(1241, 1022, 35, 'TSL1'),
(1242, 1023, 22, 'THLV'),
(1243, 1013, 2, 'AHRB'),
(1244, 1013, 22, 'THLV'),
(1246, 656, 13, 'KVNN'),
(1247, 656, 22, 'THLV'),
(1248, 1025, 22, 'THLV'),
(1249, 1026, 22, 'THLV'),
(1250, 1027, 22, 'THLV'),
(1251, 916, 43, 'GSL1'),
(1252, 1028, 28, 'KLIP'),
(1253, 1030, 28, 'KLIP'),
(1254, 1031, 40, 'NMDC'),
(1255, 1032, 40, 'NMDC'),
(1256, 1033, 40, 'NMDC'),
(1259, 1034, 43, 'GSL1'),
(1260, 117, 27, 'KLRD'),
(1261, 117, 28, 'KLIP'),
(1262, 1035, 30, 'ATTP'),
(1263, 1036, 30, 'ATTP'),
(1264, 1037, 40, 'NMDC'),
(1265, 1038, 40, 'NMDC'),
(1266, 1039, 40, 'NMDC'),
(1267, 1040, 40, 'NMDC'),
(1268, 1041, 40, 'NMDC'),
(1269, 1042, 40, 'NMDC'),
(1270, 1043, 40, 'NMDC'),
(1271, 1044, 14, 'MMHP'),
(1272, 1045, 14, 'MMHP'),
(1274, 1047, 13, 'KVNN'),
(1275, 1048, 13, 'KVNN'),
(1276, 1046, 13, 'KVNN'),
(1278, 1050, 22, 'THLV'),
(1279, 1051, 28, 'KLIP'),
(1280, 1052, 28, 'KLIP'),
(1281, 1053, 28, 'KLIP'),
(1282, 1054, 28, 'KLIP'),
(1283, 1055, 28, 'KLIP'),
(1284, 1056, 30, 'ATTP'),
(1287, 1057, 39, 'NTPC'),
(1288, 1058, 28, 'KLIP'),
(1289, 1059, 28, 'KLIP'),
(1290, 1060, 28, 'KLIP'),
(1291, 1061, 28, 'KLIP'),
(1292, 1062, 60, 'HCCB'),
(1295, 1065, 60, 'HCCB'),
(1296, 1066, 60, 'HCCB'),
(1297, 1067, 60, 'HCCB'),
(1298, 1068, 60, 'HCCB'),
(1299, 487, 6, 'EMGG'),
(1300, 487, 14, 'MMHP'),
(1301, 487, 17, 'PHPG'),
(1302, 487, 22, 'THLV'),
(1303, 307, 14, 'MMHP'),
(1304, 307, 22, 'THLV'),
(1305, 1069, 60, 'HCCB'),
(1306, 662, 44, 'ERSK'),
(1307, 662, 60, 'HCCB'),
(1308, 1070, 60, 'HCCB'),
(1309, 1071, 57, 'WCPP'),
(1311, 1073, 22, 'THLV'),
(1312, 1074, 15, 'NLDA'),
(1313, 1075, 15, 'NLDA'),
(1316, 1076, 53, 'MCFO'),
(1317, 1077, 53, 'MCFO'),
(1318, 1078, 35, 'TSL1'),
(1319, 1079, 35, 'TSL1'),
(1320, 1080, 35, 'TSL1'),
(1321, 1081, 35, 'TSL1'),
(1322, 643, 35, 'TSL1'),
(1323, 1082, 35, 'TSL1'),
(1324, 1083, 40, 'NMDC'),
(1325, 1084, 40, 'NMDC'),
(1326, 1085, 40, 'NMDC'),
(1327, 1086, 40, 'NMDC'),
(1328, 1087, 43, 'GSL1'),
(1329, 1088, 28, 'KLIP'),
(1330, 1089, 28, 'KLIP'),
(1331, 1090, 15, 'NLDA'),
(1334, 1093, 15, 'NLDA'),
(1335, 1094, 15, 'NLDA'),
(1336, 1095, 15, 'NLDA'),
(1337, 1096, 15, 'NLDA'),
(1340, 1099, 43, 'GSL1'),
(1341, 1097, 13, 'KVNN'),
(1342, 1097, 14, 'MMHP'),
(1343, 206, 6, 'EMGG'),
(1344, 206, 14, 'MMHP'),
(1345, 206, 22, 'THLV'),
(1346, 1100, 13, 'KVNN'),
(1347, 1101, 22, 'THLV'),
(1348, 1102, 1, 'AIHB'),
(1351, 1105, 30, 'ATTP'),
(1353, 1107, 43, 'GSL1'),
(1354, 382, 15, 'NLDA'),
(1355, 382, 39, 'NTPC'),
(1356, 1108, 20, 'SCAO'),
(1357, 1109, 20, 'SCAO'),
(1358, 1110, 30, 'ATTP'),
(1359, 1111, 53, 'MCFO'),
(1360, 1112, 53, 'MCFO'),
(1361, 1029, 28, 'KLIP'),
(1362, 1113, 35, 'TSL1'),
(1363, 1114, 43, 'GSL1'),
(1364, 1115, 43, 'GSL1'),
(1365, 1116, 30, 'ATTP'),
(1366, 568, 13, 'KVNN'),
(1367, 568, 14, 'MMHP'),
(1368, 1117, 28, 'KLIP'),
(1369, 1118, 28, 'KLIP'),
(1370, 846, 28, 'KLIP'),
(1371, 846, 57, 'WCPP'),
(1372, 1119, 15, 'NLDA'),
(1373, 1120, 43, 'GSL1'),
(1374, 1121, 15, 'NLDA'),
(1375, 1122, 48, 'MGLF'),
(1376, 1123, 48, 'MGLF'),
(1377, 1124, 28, 'KLIP'),
(1378, 1125, 28, 'KLIP'),
(1379, 1126, 28, 'KLIP'),
(1380, 1127, 28, 'KLIP'),
(1381, 1128, 28, 'KLIP'),
(1382, 1129, 40, 'NMDC'),
(1384, 1131, 40, 'NMDC'),
(1385, 1132, 40, 'NMDC'),
(1386, 1133, 40, 'NMDC'),
(1387, 63, 27, 'KLRD'),
(1388, 63, 28, 'KLIP'),
(1389, 63, 57, 'WCPP'),
(1392, 1134, 39, 'NTPC'),
(1393, 1135, 39, 'NTPC'),
(1394, 1136, 15, 'NLDA'),
(1395, 183, 18, 'RECG'),
(1396, 183, 64, 'EMDH'),
(1397, 1137, 64, 'EMDH'),
(1400, 72, 18, 'RECG'),
(1401, 72, 26, 'MLD2'),
(1402, 72, 27, 'KLRD'),
(1403, 72, 43, 'GSL1'),
(1404, 72, 57, 'WCPP'),
(1405, 72, 64, 'EMDH'),
(1408, 1138, 64, 'EMDH'),
(1409, 1139, 40, 'NMDC'),
(1410, 1140, 40, 'NMDC'),
(1411, 1141, 40, 'NMDC'),
(1412, 1142, 35, 'TSL1'),
(1413, 1143, 35, 'TSL1'),
(1415, 1063, 60, 'HCCB'),
(1416, 1145, 53, 'MCFO'),
(1417, 1146, 53, 'MCFO'),
(1418, 1147, 28, 'KLIP'),
(1420, 1149, 28, 'KLIP'),
(1421, 1150, 28, 'KLIP'),
(1422, 1151, 28, 'KLIP'),
(1423, 1152, 28, 'KLIP'),
(1424, 74, 27, 'KLRD'),
(1425, 74, 28, 'KLIP'),
(1426, 313, 27, 'KLRD'),
(1427, 313, 28, 'KLIP'),
(1428, 1153, 40, 'NMDC'),
(1435, 1154, 30, 'ATTP'),
(1436, 1144, 30, 'ATTP'),
(1437, 1155, 56, 'MAIP'),
(1444, 1156, 28, 'KLIP'),
(1445, 1157, 28, 'KLIP'),
(1449, 490, 1, 'AIHB'),
(1450, 490, 2, 'AHRB'),
(1451, 1158, 15, 'NLDA'),
(1452, 1159, 22, 'THLV'),
(1453, 1160, 28, 'KLIP'),
(1454, 1161, 28, 'KLIP'),
(1455, 1162, 28, 'KLIP'),
(1456, 1163, 19, 'RSNO'),
(1458, 1165, 28, 'KLIP'),
(1459, 1166, 42, 'KRGN'),
(1460, 1167, 40, 'NMDC'),
(1461, 1168, 40, 'NMDC'),
(1463, 1170, 40, 'NMDC'),
(1464, 1171, 40, 'NMDC'),
(1465, 1172, 4, 'AIHR'),
(1466, 1173, 30, 'ATTP'),
(1467, 1174, 43, 'GSL1'),
(1468, 1175, 43, 'GSL1'),
(1470, 1177, 42, 'KRGN'),
(1471, 1178, 42, 'KRGN'),
(1473, 1180, 35, 'TSL1'),
(1474, 119, 28, 'KLIP'),
(1478, 1181, 28, 'KLIP'),
(1479, 1182, 28, 'KLIP'),
(1481, 1184, 35, 'TSL1'),
(1482, 1185, 35, 'TSL1'),
(1483, 1186, 35, 'TSL1'),
(1484, 1187, 35, 'TSL1'),
(1485, 1188, 35, 'TSL1'),
(1486, 1189, 64, 'EMDH'),
(1487, 1190, 64, 'EMDH'),
(1488, 1191, 64, 'EMDH'),
(1489, 696, 39, 'NTPC'),
(1490, 696, 42, 'KRGN'),
(1491, 1192, 39, 'NTPC'),
(1492, 1193, 51, 'LTFO'),
(1494, 1195, 51, 'LTFO'),
(1495, 1196, 62, 'OPRC'),
(1496, 1197, 43, 'GSL1'),
(1508, 1203, 63, 'HDFC'),
(1516, 1206, 28, 'KLIP'),
(1517, 1198, 35, 'TSL1'),
(1518, 1198, 40, 'NMDC'),
(1519, 1207, 28, 'KLIP'),
(1520, 1024, 14, 'MMHP'),
(1521, 1024, 22, 'THLV'),
(1522, 1208, 2, 'AHRB'),
(1526, 1210, 35, 'TSL1'),
(1527, 1211, 35, 'TSL1'),
(1528, 1212, 64, 'EMDH'),
(1529, 1148, 27, 'KLRD'),
(1530, 1148, 28, 'KLIP'),
(1531, 57, 27, 'KLRD'),
(1532, 57, 28, 'KLIP'),
(1533, 57, 30, 'ATTP'),
(1534, 1213, 22, 'THLV'),
(1536, 1215, 14, 'MMHP'),
(1537, 1216, 63, 'HDFC'),
(1538, 1217, 63, 'HDFC'),
(1540, 1219, 43, 'GSL1'),
(1541, 1220, 43, 'GSL1'),
(1542, 1221, 64, 'EMDH'),
(1543, 1222, 64, 'EMDH'),
(1544, 294, 22, 'THLV'),
(1545, 294, 64, 'EMDH'),
(1548, 1223, 42, 'KRGN'),
(1549, 1224, 43, 'GSL1'),
(1552, 1227, 63, 'HDFC'),
(1553, 1228, 30, 'ATTP'),
(1559, 1233, 40, 'NMDC'),
(1560, 1233, 63, 'HDFC'),
(1561, 1232, 40, 'NMDC'),
(1562, 1231, 40, 'NMDC'),
(1563, 1230, 40, 'NMDC'),
(1564, 1229, 40, 'NMDC'),
(1565, 1234, 40, 'NMDC'),
(1566, 1235, 40, 'NMDC'),
(1567, 1236, 40, 'NMDC'),
(1568, 1237, 40, 'NMDC'),
(1569, 1238, 40, 'NMDC'),
(1570, 1239, 40, 'NMDC'),
(1571, 1240, 40, 'NMDC'),
(1572, 1241, 40, 'NMDC'),
(1573, 1242, 48, 'MGLF'),
(1574, 544, 14, 'MMHP'),
(1575, 544, 22, 'THLV'),
(1576, 1243, 48, 'MGLF'),
(1577, 1244, 48, 'MGLF'),
(1578, 1245, 13, 'KVNN'),
(1580, 1247, 48, 'MGLF'),
(1581, 1248, 15, 'NLDA'),
(1583, 1250, 42, 'KRGN'),
(1584, 1251, 42, 'KRGN'),
(1585, 1252, 35, 'TSL1'),
(1587, 1254, 35, 'TSL1'),
(1588, 1255, 35, 'TSL1'),
(1589, 1256, 35, 'TSL1'),
(1591, 1258, 43, 'GSL1'),
(1592, 1259, 15, 'NLDA'),
(1593, 1260, 64, 'EMDH'),
(1594, 1261, 64, 'EMDH'),
(1595, 1262, 22, 'THLV'),
(1596, 1263, 22, 'THLV'),
(1597, 1264, 22, 'THLV'),
(1598, 1265, 22, 'THLV'),
(1599, 1266, 4, 'AIHR'),
(1601, 1176, 27, 'KLRD'),
(1602, 1176, 28, 'KLIP'),
(1603, 1268, 22, 'THLV'),
(1604, 1269, 22, 'THLV'),
(1605, 1270, 22, 'THLV'),
(1606, 1271, 22, 'THLV'),
(1607, 1272, 22, 'THLV'),
(1608, 1273, 63, 'HDFC'),
(1609, 1274, 51, 'LTFO'),
(1610, 1275, 51, 'LTFO'),
(1611, 1276, 26, 'MLD2'),
(1612, 1277, 30, 'ATTP'),
(1617, 70, 27, 'KLRD'),
(1618, 70, 28, 'KLIP'),
(1619, 70, 30, 'ATTP'),
(1620, 70, 57, 'WCPP'),
(1621, 123, 27, 'KLRD'),
(1622, 123, 28, 'KLIP'),
(1623, 123, 57, 'WCPP'),
(1624, 625, 35, 'TSL1'),
(1625, 625, 63, 'HDFC'),
(1626, 1280, 4, 'AIHR'),
(1627, 1281, 35, 'TSL1'),
(1629, 1282, 35, 'TSL1'),
(1630, 1283, 35, 'TSL1'),
(1632, 1285, 35, 'TSL1'),
(1634, 1287, 35, 'TSL1'),
(1635, 1288, 22, 'THLV'),
(1637, 968, 30, 'ATTP'),
(1638, 1290, 22, 'THLV'),
(1640, 1292, 63, 'HDFC'),
(1641, 1293, 15, 'NLDA'),
(1642, 1294, 15, 'NLDA'),
(1643, 1295, 15, 'NLDA'),
(1644, 959, 50, 'BHFS'),
(1646, 1286, 35, 'TSL1'),
(1647, 1286, 43, 'GSL1'),
(1649, 1296, 42, 'KRGN'),
(1655, 1218, 63, 'HDFC'),
(1657, 1299, 64, 'EMDH'),
(1658, 1300, 64, 'EMDH'),
(1660, 1302, 64, 'EMDH'),
(1661, 1303, 64, 'EMDH'),
(1662, 1304, 63, 'HDFC'),
(1663, 1305, 43, 'GSL1'),
(1664, 1306, 1, 'AIHB'),
(1666, 1308, 63, 'HDFC'),
(1667, 1309, 43, 'GSL1'),
(1668, 1278, 27, 'KLRD'),
(1669, 1278, 30, 'ATTP'),
(1670, 1278, 57, 'WCPP'),
(1671, 65, 27, 'KLRD'),
(1672, 65, 28, 'KLIP'),
(1673, 65, 57, 'WCPP'),
(1674, 1106, 28, 'KLIP'),
(1675, 1106, 57, 'WCPP'),
(1676, 1310, 27, 'KLRD'),
(1677, 1310, 28, 'KLIP'),
(1678, 1310, 57, 'WCPP'),
(1681, 551, 14, 'MMHP'),
(1682, 551, 21, 'THGG'),
(1683, 1311, 35, 'TSL1'),
(1684, 1312, 35, 'TSL1'),
(1685, 1313, 35, 'TSL1'),
(1687, 1315, 13, 'KVNN'),
(1688, 1316, 13, 'KVNN'),
(1689, 1064, 60, 'HCCB'),
(1690, 1064, 63, 'HDFC'),
(1691, 1317, 43, 'GSL1'),
(1692, 1318, 51, 'LTFO'),
(1693, 68, 27, 'KLRD'),
(1694, 68, 28, 'KLIP'),
(1695, 68, 57, 'WCPP'),
(1696, 1319, 28, 'KLIP'),
(1700, 1320, 63, 'HDFC'),
(1701, 1321, 22, 'THLV'),
(1704, 1322, 64, 'EMDH'),
(1705, 1323, 64, 'EMDH'),
(1706, 1324, 64, 'EMDH'),
(1707, 1325, 64, 'EMDH'),
(1708, 1202, 63, 'HDFC'),
(1709, 1326, 66, 'SNUD'),
(1710, 1327, 66, 'SNUD'),
(1711, 1328, 66, 'SNUD'),
(1718, 1098, 14, 'MMHP'),
(1719, 1098, 66, 'SNUD'),
(1720, 1329, 48, 'MGLF'),
(1721, 1330, 48, 'MGLF'),
(1722, 1331, 48, 'MGLF'),
(1724, 1333, 35, 'TSL1'),
(1725, 1334, 35, 'TSL1'),
(1726, 1253, 35, 'TSL1'),
(1727, 457, 35, 'TSL1'),
(1728, 457, 40, 'NMDC'),
(1730, 1336, 13, 'KVNN'),
(1731, 1335, 13, 'KVNN'),
(1733, 1338, 64, 'EMDH'),
(1734, 1339, 64, 'EMDH'),
(1735, 1340, 51, 'LTFO'),
(1736, 1279, 21, 'THGG'),
(1737, 1279, 22, 'THLV'),
(1740, 1092, 15, 'NLDA'),
(1741, 1092, 64, 'EMDH'),
(1742, 1092, 66, 'SNUD'),
(1744, 1342, 66, 'SNUD'),
(1745, 1343, 66, 'SNUD'),
(1747, 1345, 40, 'NMDC'),
(1748, 1346, 40, 'NMDC'),
(1749, 1347, 40, 'NMDC'),
(1750, 1348, 40, 'NMDC'),
(1751, 1349, 40, 'NMDC'),
(1752, 1350, 40, 'NMDC'),
(1753, 1351, 40, 'NMDC'),
(1755, 1353, 40, 'NMDC'),
(1756, 1354, 40, 'NMDC'),
(1757, 1355, 40, 'NMDC'),
(1758, 1356, 40, 'NMDC'),
(1760, 1358, 40, 'NMDC'),
(1761, 1359, 40, 'NMDC'),
(1762, 1360, 40, 'NMDC'),
(1765, 1344, 43, 'GSL1'),
(1773, 484, 15, 'NLDA'),
(1774, 484, 17, 'PHPG'),
(1775, 1361, 51, 'LTFO'),
(1776, 1257, 43, 'GSL1'),
(1777, 1257, 63, 'HDFC'),
(1780, 1362, 40, 'NMDC'),
(1783, 1363, 51, 'LTFO'),
(1784, 1364, 52, 'STFO'),
(1785, 1365, 52, 'STFO'),
(1786, 1366, 39, 'NTPC'),
(1787, 1367, 39, 'NTPC'),
(1788, 1368, 66, 'SNUD'),
(1790, 1369, 42, 'KRGN'),
(1792, 1371, 15, 'NLDA'),
(1793, 1372, 2, 'AHRB'),
(1794, 1373, 13, 'KVNN'),
(1795, 1374, 13, 'KVNN'),
(1796, 1375, 53, 'MCFO'),
(1802, 1377, 40, 'NMDC'),
(1803, 1378, 40, 'NMDC'),
(1804, 1379, 40, 'NMDC'),
(1807, 1381, 15, 'NLDA'),
(1808, 1370, 15, 'NLDA'),
(1810, 1383, 22, 'THLV'),
(1811, 1384, 40, 'NMDC'),
(1813, 1199, 63, 'HDFC'),
(1814, 1386, 63, 'HDFC'),
(1815, 1226, 63, 'HDFC'),
(1816, 1387, 63, 'HDFC'),
(1818, 599, 15, 'NLDA'),
(1819, 599, 45, 'CUBG'),
(1821, 1389, 35, 'TSL1'),
(1822, 1390, 35, 'TSL1'),
(1823, 1391, 2, 'AHRB'),
(1824, 1392, 42, 'KRGN'),
(1825, 1393, 13, 'KVNN'),
(1827, 1395, 66, 'SNUD'),
(1828, 1396, 66, 'SNUD'),
(1829, 1385, 63, 'HDFC'),
(1830, 1397, 40, 'NMDC'),
(1831, 854, 13, 'KVNN'),
(1832, 854, 66, 'SNUD'),
(1834, 1399, 63, 'HDFC'),
(1835, 693, 42, 'KRGN'),
(1836, 693, 43, 'GSL1'),
(1837, 1204, 63, 'HDFC'),
(1838, 1400, 64, 'EMDH'),
(1839, 1401, 64, 'EMDH'),
(1840, 1402, 64, 'EMDH'),
(1841, 1403, 18, 'RECG'),
(1842, 1404, 18, 'RECG'),
(1844, 1406, 63, 'HDFC'),
(1845, 1407, 63, 'HDFC'),
(1851, 1408, 15, 'NLDA'),
(1852, 1409, 15, 'NLDA'),
(1853, 1410, 39, 'NTPC'),
(1854, 1411, 39, 'NTPC'),
(1855, 1412, 39, 'NTPC'),
(1856, 1413, 39, 'NTPC'),
(1857, 1414, 39, 'NTPC'),
(1858, 1415, 39, 'NTPC'),
(1859, 406, 39, 'NTPC'),
(1860, 406, 42, 'KRGN'),
(1861, 1416, 22, 'THLV'),
(1862, 1417, 22, 'THLV'),
(1863, 1418, 35, 'TSL1'),
(1864, 1419, 66, 'SNUD'),
(1865, 1420, 66, 'SNUD'),
(1866, 1421, 66, 'SNUD'),
(1867, 592, 39, 'NTPC'),
(1868, 592, 40, 'NMDC'),
(1869, 592, 42, 'KRGN'),
(1870, 592, 43, 'GSL1'),
(1872, 1423, 40, 'NMDC'),
(1873, 1424, 18, 'RECG'),
(1875, 1426, 42, 'KRGN'),
(1876, 1427, 64, 'EMDH'),
(1880, 1428, 40, 'NMDC'),
(1881, 1429, 40, 'NMDC'),
(1882, 1267, 13, 'KVNN'),
(1883, 1267, 66, 'SNUD'),
(1885, 1380, 63, 'HDFC'),
(1886, 1431, 63, 'HDFC'),
(1889, 1434, 63, 'HDFC'),
(1891, 690, 21, 'THGG'),
(1892, 690, 22, 'THLV'),
(1893, 1436, 42, 'KRGN'),
(1894, 1225, 63, 'HDFC'),
(1900, 1438, 66, 'SNUD'),
(1901, 1439, 66, 'SNUD'),
(1902, 1440, 66, 'SNUD'),
(1905, 1442, 22, 'THLV'),
(1906, 1441, 22, 'THLV'),
(1907, 1443, 22, 'THLV'),
(1908, 1297, 28, 'KLIP'),
(1909, 1297, 57, 'WCPP'),
(1910, 1435, 27, 'KLRD'),
(1911, 1435, 57, 'WCPP'),
(1912, 1201, 63, 'HDFC'),
(1913, 1398, 63, 'HDFC'),
(1917, 1445, 64, 'EMDH'),
(1918, 1291, 63, 'HDFC'),
(1919, 1446, 18, 'RECG'),
(1920, 1447, 63, 'HDFC'),
(1922, 466, 22, 'THLV'),
(1923, 466, 64, 'EMDH'),
(1924, 1449, 66, 'SNUD'),
(1925, 1450, 66, 'SNUD'),
(1926, 1451, 66, 'SNUD'),
(1927, 1452, 66, 'SNUD'),
(1930, 985, 6, 'EMGG'),
(1931, 985, 21, 'THGG'),
(1932, 985, 22, 'THLV'),
(1933, 121, 27, 'KLRD'),
(1934, 121, 28, 'KLIP'),
(1935, 121, 57, 'WCPP'),
(1938, 1454, 35, 'TSL1'),
(1940, 1456, 22, 'THLV'),
(1941, 1457, 22, 'THLV'),
(1942, 1458, 22, 'THLV'),
(1945, 1459, 15, 'NLDA'),
(1948, 1462, 63, 'HDFC'),
(1949, 1463, 14, 'MMHP'),
(1950, 807, 21, 'THGG'),
(1951, 807, 22, 'THLV'),
(1952, 1464, 66, 'SNUD'),
(1953, 1465, 66, 'SNUD'),
(1954, 1466, 66, 'SNUD'),
(1955, 1467, 40, 'NMDC'),
(1956, 1468, 40, 'NMDC'),
(1957, 1469, 15, 'NLDA'),
(1958, 1433, 35, 'TSL1'),
(1959, 1470, 53, 'MCFO'),
(1960, 1471, 53, 'MCFO'),
(1961, 1472, 65, 'PATP'),
(1962, 1473, 65, 'PATP'),
(1963, 1474, 65, 'PATP'),
(1964, 1475, 65, 'PATP'),
(1965, 1214, 28, 'KLIP'),
(1966, 1214, 65, 'PATP'),
(1967, 1476, 63, 'HDFC'),
(1968, 1405, 63, 'HDFC'),
(1969, 1249, 15, 'NLDA'),
(1970, 1249, 63, 'HDFC'),
(1971, 1477, 64, 'EMDH'),
(2043, 1479, 40, 'NMDC'),
(2044, 1480, 40, 'NMDC'),
(2045, 1481, 40, 'NMDC'),
(2046, 1482, 63, 'HDFC'),
(2047, 1483, 42, 'KRGN'),
(2052, 1484, 66, 'SNUD'),
(2054, 1486, 66, 'SNUD'),
(2055, 1487, 66, 'SNUD'),
(2059, 1489, 66, 'SNUD'),
(2060, 25, 30, 'ATTP'),
(2061, 25, 57, 'WCPP'),
(2062, 1490, 40, 'NMDC'),
(2063, 1491, 40, 'NMDC'),
(2064, 645, 35, 'TSL1'),
(2065, 645, 63, 'HDFC'),
(2066, 37, 30, 'ATTP'),
(2067, 37, 57, 'WCPP'),
(2068, 1492, 22, 'THLV'),
(2069, 1493, 42, 'KRGN'),
(2070, 814, 35, 'TSL1'),
(2071, 814, 63, 'HDFC'),
(2072, 1448, 63, 'HDFC'),
(2073, 1444, 63, 'HDFC'),
(2075, 1495, 65, 'PATP'),
(2076, 1496, 65, 'PATP'),
(2080, 724, 13, 'KVNN'),
(2081, 724, 66, 'SNUD'),
(2082, 659, 14, 'MMHP'),
(2083, 659, 66, 'SNUD'),
(2084, 1432, 43, 'GSL1'),
(2085, 1430, 63, 'HDFC'),
(2086, 60, 27, 'KLRD'),
(2087, 60, 28, 'KLIP'),
(2088, 60, 30, 'ATTP'),
(2089, 60, 57, 'WCPP'),
(2090, 122, 27, 'KLRD'),
(2091, 122, 28, 'KLIP'),
(2092, 122, 30, 'ATTP'),
(2093, 122, 57, 'WCPP'),
(2094, 394, 26, 'MLD2'),
(2095, 394, 28, 'KLIP'),
(2096, 1497, 43, 'GSL1'),
(2098, 1498, 28, 'KLIP'),
(2099, 1498, 65, 'PATP'),
(2100, 1499, 64, 'EMDH'),
(2101, 1500, 64, 'EMDH'),
(2102, 550, 14, 'MMHP'),
(2103, 550, 64, 'EMDH'),
(2104, 1501, 64, 'EMDH'),
(2105, 1502, 65, 'PATP'),
(2106, 1503, 22, 'THLV'),
(2107, 1504, 17, 'PHPG'),
(2108, 1505, 17, 'PHPG'),
(2109, 1506, 40, 'NMDC'),
(2110, 1507, 63, 'HDFC'),
(2114, 1508, 57, 'WCPP'),
(2115, 1509, 43, 'GSL1'),
(2116, 85, 28, 'KLIP'),
(2117, 85, 57, 'WCPP'),
(2118, 1510, 65, 'PATP'),
(2119, 1511, 57, 'WCPP'),
(2122, 1382, 13, 'KVNN'),
(2123, 1382, 66, 'SNUD'),
(2126, 984, 22, 'THLV'),
(2127, 984, 66, 'SNUD'),
(2128, 1512, 66, 'SNUD'),
(2129, 1513, 43, 'GSL1'),
(2130, 1514, 40, 'NMDC'),
(2131, 1425, 28, 'KLIP'),
(2132, 1425, 30, 'ATTP'),
(2133, 1425, 57, 'WCPP'),
(2135, 1516, 22, 'THLV'),
(2136, 1517, 22, 'THLV'),
(2137, 1518, 22, 'THLV'),
(2138, 1519, 22, 'THLV'),
(2139, 1520, 22, 'THLV'),
(2141, 331, 21, 'THGG'),
(2142, 331, 22, 'THLV'),
(2143, 331, 66, 'SNUD'),
(2144, 331, 67, 'PBGN'),
(2145, 562, 14, 'MMHP'),
(2146, 562, 66, 'SNUD'),
(2147, 562, 67, 'PBGN'),
(2148, 1488, 66, 'SNUD'),
(2149, 1488, 67, 'PBGN'),
(2150, 1485, 66, 'SNUD'),
(2151, 1485, 67, 'PBGN'),
(2152, 1522, 67, 'PBGN'),
(2155, 1524, 51, 'LTFO'),
(2156, 1524, 52, 'STFO'),
(2157, 1525, 48, 'MGLF'),
(2158, 1526, 48, 'MGLF'),
(2159, 1527, 48, 'MGLF'),
(2160, 1528, 48, 'MGLF'),
(2161, 1529, 39, 'NTPC'),
(2162, 1530, 39, 'NTPC'),
(2163, 919, 39, 'NTPC'),
(2164, 919, 42, 'KRGN'),
(2166, 92, 28, 'KLIP'),
(2167, 92, 57, 'WCPP'),
(2168, 824, 22, 'THLV'),
(2169, 824, 57, 'WCPP'),
(2171, 1533, 63, 'HDFC'),
(2173, 1534, 67, 'PBGN'),
(2174, 1535, 65, 'PATP'),
(2175, 56, 30, 'ATTP'),
(2176, 56, 65, 'PATP'),
(2178, 1537, 22, 'THLV'),
(2179, 1538, 66, 'SNUD'),
(2180, 1539, 66, 'SNUD'),
(2181, 1540, 66, 'SNUD'),
(2182, 1541, 66, 'SNUD'),
(2183, 1542, 66, 'SNUD'),
(2184, 1543, 66, 'SNUD'),
(2185, 17, 27, 'KLRD'),
(2186, 17, 30, 'ATTP'),
(2187, 17, 57, 'WCPP'),
(2188, 17, 65, 'PATP'),
(2190, 253, 48, 'MGLF'),
(2191, 253, 49, 'MGLFCASTINGYARD'),
(2192, 1545, 63, 'HDFC'),
(2193, 1546, 63, 'HDFC'),
(2194, 1547, 66, 'SNUD'),
(2196, 1549, 64, 'EMDH'),
(2197, 1550, 64, 'EMDH'),
(2198, 1544, 28, 'KLIP'),
(2199, 1544, 57, 'WCPP'),
(2200, 1551, 66, 'SNUD'),
(2209, 1552, 72, 'DAEG'),
(2210, 1553, 72, 'DAEG'),
(2211, 290, 22, 'THLV'),
(2212, 290, 64, 'EMDH'),
(2213, 290, 66, 'SNUD'),
(2214, 290, 72, 'DAEG'),
(2215, 1164, 28, 'KLIP'),
(2216, 1164, 30, 'ATTP'),
(2217, 1164, 65, 'PATP'),
(2218, 1554, 65, 'PATP'),
(2219, 87, 28, 'KLIP'),
(2220, 87, 65, 'PATP'),
(2221, 1555, 65, 'PATP'),
(2222, 1556, 43, 'GSL1'),
(2223, 1557, 43, 'GSL1'),
(2224, 1558, 43, 'GSL1'),
(2226, 1560, 40, 'NMDC'),
(2227, 1561, 51, 'LTFO'),
(2228, 1561, 52, 'STFO'),
(2231, 1562, 63, 'HDFC'),
(2232, 980, 43, 'GSL1'),
(2233, 1563, 57, 'WCPP'),
(2237, 1531, 28, 'KLIP'),
(2238, 1531, 57, 'WCPP'),
(2239, 1209, 22, 'THLV'),
(2240, 1209, 66, 'SNUD'),
(2241, 1564, 65, 'PATP'),
(2242, 1565, 53, 'MCFO'),
(2250, 1437, 64, 'EMDH'),
(2251, 1437, 66, 'SNUD'),
(2252, 1437, 76, 'EOBN'),
(2253, 1567, 76, 'EOBN'),
(2254, 329, 19, 'RSNO'),
(2255, 329, 22, 'THLV'),
(2256, 329, 66, 'SNUD'),
(2257, 329, 76, 'EOBN'),
(2258, 1568, 76, 'EOBN'),
(2260, 726, 43, 'GSL1'),
(2261, 726, 63, 'HDFC'),
(2262, 1284, 35, 'TSL1'),
(2263, 1284, 63, 'HDFC'),
(2265, 765, 40, 'NMDC'),
(2266, 765, 43, 'GSL1'),
(2267, 1571, 43, 'GSL1'),
(2268, 1572, 28, 'KLIP'),
(2269, 1573, 65, 'PATP'),
(2270, 1574, 28, 'KLIP'),
(2271, 1566, 27, 'KLRD'),
(2272, 1566, 28, 'KLIP'),
(2273, 1575, 66, 'SNUD'),
(2274, 1576, 66, 'SNUD'),
(2277, 1579, 63, 'HDFC'),
(2278, 1580, 65, 'PATP'),
(2279, 1581, 65, 'PATP'),
(2280, 1582, 53, 'MCFO'),
(2281, 1583, 72, 'DAEG'),
(2283, 1585, 72, 'DAEG'),
(2284, 1169, 40, 'NMDC'),
(2285, 1169, 72, 'DAEG'),
(2286, 1586, 28, 'KLIP'),
(2287, 1587, 66, 'SNUD'),
(2289, 1589, 53, 'MCFO'),
(2290, 1590, 67, 'PBGN'),
(2291, 1591, 64, 'EMDH'),
(2292, 1592, 63, 'HDFC'),
(2295, 1536, 22, 'THLV'),
(2296, 1536, 66, 'SNUD'),
(2297, 125, 25, 'NIRP'),
(2298, 125, 26, 'MLD2'),
(2299, 125, 65, 'PATP'),
(2300, 1593, 67, 'PBGN'),
(2301, 1594, 43, 'GSL1'),
(2302, 1595, 76, 'EOBN'),
(2303, 1596, 63, 'HDFC'),
(2304, 1570, 63, 'HDFC'),
(2305, 1494, 63, 'HDFC'),
(2307, 1598, 65, 'PATP'),
(2308, 1599, 40, 'NMDC'),
(2309, 1600, 40, 'NMDC'),
(2310, 1601, 40, 'NMDC'),
(2311, 1602, 13, 'KVNN'),
(2312, 1603, 67, 'PBGN'),
(2313, 1604, 18, 'RECG'),
(2314, 1341, 66, 'SNUD'),
(2315, 1341, 67, 'PBGN'),
(2316, 1605, 72, 'DAEG'),
(2317, 1606, 72, 'DAEG'),
(2318, 1607, 72, 'DAEG'),
(2319, 1608, 72, 'DAEG'),
(2322, 224, 6, 'EMGG'),
(2323, 224, 64, 'EMDH'),
(2324, 224, 72, 'DAEG'),
(2325, 1611, 22, 'THLV'),
(2329, 1532, 63, 'HDFC'),
(2330, 1091, 15, 'NLDA'),
(2331, 1091, 64, 'EMDH'),
(2332, 1615, 76, 'EOBN'),
(2333, 1616, 76, 'EOBN'),
(2334, 1617, 76, 'EOBN'),
(2339, 1618, 28, 'KLIP'),
(2340, 1337, 6, 'EMGG'),
(2341, 1337, 40, 'NMDC'),
(2342, 1337, 72, 'DAEG'),
(2343, 1612, 22, 'THLV'),
(2344, 1612, 72, 'DAEG'),
(2345, 1613, 22, 'THLV'),
(2346, 1613, 72, 'DAEG'),
(2350, 146, 28, 'KLIP'),
(2351, 146, 30, 'ATTP'),
(2352, 1619, 40, 'NMDC'),
(2353, 1620, 72, 'DAEG'),
(2354, 1621, 13, 'KVNN'),
(2355, 1622, 28, 'KLIP'),
(2356, 1623, 67, 'PBGN'),
(2357, 1624, 66, 'SNUD'),
(2358, 1625, 66, 'SNUD'),
(2359, 1626, 40, 'NMDC'),
(2360, 1627, 40, 'NMDC'),
(2364, 1597, 63, 'HDFC'),
(2365, 1578, 63, 'HDFC'),
(2366, 1630, 51, 'LTFO'),
(2367, 1630, 52, 'STFO'),
(2368, 1630, 76, 'EOBN'),
(2369, 1629, 51, 'LTFO'),
(2370, 1629, 52, 'STFO'),
(2371, 1629, 76, 'EOBN'),
(2372, 1631, 53, 'MCFO'),
(2373, 1632, 53, 'MCFO'),
(2374, 1633, 53, 'MCFO'),
(2375, 1634, 53, 'MCFO'),
(2376, 1635, 53, 'MCFO'),
(2377, 1636, 53, 'MCFO'),
(2378, 1637, 53, 'MCFO'),
(2379, 445, 51, 'LTFO'),
(2380, 445, 52, 'STFO'),
(2391, 1638, 48, 'MGLF'),
(2392, 1638, 49, 'MGLFCASTINGYARD'),
(2393, 1639, 77, 'NLMK'),
(2394, 1640, 77, 'NLMK'),
(2395, 1641, 77, 'NLMK'),
(2396, 1642, 77, 'NLMK'),
(2408, 1643, 66, 'SNUD'),
(2409, 1644, 67, 'PBGN'),
(2411, 1646, 28, 'KLIP'),
(2412, 1647, 43, 'GSL1'),
(2413, 1648, 15, 'NLDA'),
(2414, 1649, 15, 'NLDA'),
(2415, 1650, 22, 'THLV'),
(2417, 1610, 22, 'THLV'),
(2418, 1610, 72, 'DAEG'),
(2419, 1453, 24, 'PUNE'),
(2420, 1453, 28, 'KLIP'),
(2421, 1453, 57, 'WCPP'),
(2423, 1628, 63, 'HDFC'),
(2425, 1654, 64, 'EMDH'),
(2426, 1655, 64, 'EMDH'),
(2427, 1656, 72, 'DAEG'),
(2428, 1657, 72, 'DAEG'),
(2429, 1658, 72, 'DAEG'),
(2430, 1659, 72, 'DAEG'),
(2431, 1660, 72, 'DAEG'),
(2432, 1661, 72, 'DAEG'),
(2433, 1662, 72, 'DAEG'),
(2434, 1663, 72, 'DAEG'),
(2435, 1072, 22, 'THLV'),
(2436, 1072, 72, 'DAEG'),
(2437, 1664, 77, 'NLMK'),
(2438, 1665, 76, 'EOBN'),
(2440, 1246, 13, 'KVNN'),
(2441, 1246, 76, 'EOBN'),
(2442, 1667, 76, 'EOBN'),
(2443, 1668, 76, 'EOBN'),
(2444, 1669, 76, 'EOBN'),
(2445, 955, 14, 'MMHP'),
(2446, 955, 76, 'EOBN'),
(2447, 1298, 64, 'EMDH'),
(2448, 1298, 76, 'EOBN'),
(2451, 1672, 13, 'KVNN'),
(2452, 1673, 13, 'KVNN'),
(2453, 1674, 40, 'NMDC'),
(2454, 1675, 72, 'DAEG'),
(2455, 1676, 72, 'DAEG'),
(2456, 1677, 72, 'DAEG'),
(2458, 1588, 66, 'SNUD'),
(2459, 1588, 67, 'PBGN'),
(2460, 1614, 63, 'HDFC'),
(2461, 1679, 57, 'WCPP'),
(2462, 1680, 66, 'SNUD'),
(2463, 1681, 66, 'SNUD'),
(2464, 1682, 66, 'SNUD'),
(2465, 337, 40, 'NMDC'),
(2466, 337, 66, 'SNUD'),
(2467, 1683, 72, 'DAEG'),
(2468, 1684, 72, 'DAEG'),
(2474, 1685, 28, 'KLIP'),
(2475, 1686, 48, 'MGLF'),
(2476, 1686, 49, 'MGLFCASTINGYARD'),
(2477, 1687, 48, 'MGLF'),
(2478, 1687, 49, 'MGLFCASTINGYARD'),
(2479, 1688, 48, 'MGLF'),
(2480, 1688, 49, 'MGLFCASTINGYARD'),
(2481, 150, 28, 'KLIP'),
(2482, 150, 30, 'ATTP'),
(2483, 1689, 72, 'DAEG'),
(2484, 1049, 22, 'THLV'),
(2485, 1049, 72, 'DAEG'),
(2486, 1690, 76, 'EOBN'),
(2487, 1691, 67, 'PBGN'),
(2488, 1692, 67, 'PBGN'),
(2489, 1693, 72, 'DAEG'),
(2492, 1694, 28, 'KLIP'),
(2493, 1651, 18, 'RECG'),
(2494, 1651, 22, 'THLV'),
(2495, 1695, 40, 'NMDC'),
(2496, 1696, 65, 'PATP'),
(2497, 36, 30, 'ATTP'),
(2498, 36, 65, 'PATP'),
(2499, 1697, 65, 'PATP'),
(2500, 1698, 35, 'TSL1'),
(2501, 1699, 40, 'NMDC'),
(2504, 1702, 43, 'GSL1'),
(2505, 1703, 66, 'SNUD'),
(2507, 1705, 72, 'DAEG'),
(2508, 1388, 18, 'RECG'),
(2509, 1388, 72, 'DAEG'),
(2510, 1706, 72, 'DAEG'),
(2511, 1707, 72, 'DAEG'),
(2512, 1704, 72, 'DAEG'),
(2513, 1708, 72, 'DAEG'),
(2514, 1104, 40, 'NMDC'),
(2515, 1104, 66, 'SNUD'),
(2516, 399, 13, 'KVNN'),
(2517, 399, 64, 'EMDH'),
(2518, 1709, 64, 'EMDH'),
(2519, 1710, 15, 'NLDA'),
(2521, 1712, 65, 'PATP'),
(2522, 1713, 43, 'GSL1'),
(2523, 1714, 13, 'KVNN'),
(2524, 1715, 13, 'KVNN'),
(2525, 1716, 13, 'KVNN'),
(2526, 1717, 13, 'KVNN'),
(2527, 1130, 13, 'KVNN'),
(2528, 1130, 40, 'NMDC'),
(2535, 573, 51, 'LTFO'),
(2536, 573, 52, 'STFO'),
(2537, 573, 54, 'NRDA'),
(2538, 446, 51, 'LTFO'),
(2539, 446, 52, 'STFO'),
(2540, 1718, 48, 'MGLF'),
(2541, 1719, 72, 'DAEG'),
(2542, 1720, 72, 'DAEG'),
(2543, 1721, 72, 'DAEG'),
(2544, 1722, 40, 'NMDC'),
(2545, 1609, 22, 'THLV'),
(2546, 1609, 72, 'DAEG'),
(2547, 1723, 22, 'THLV'),
(2550, 1724, 72, 'DAEG'),
(2551, 1725, 28, 'KLIP'),
(2552, 1726, 66, 'SNUD'),
(2553, 448, 18, 'RECG'),
(2554, 448, 66, 'SNUD'),
(2555, 1727, 35, 'TSL1'),
(2556, 1728, 77, 'NLMK'),
(2557, 1394, 66, 'SNUD'),
(2558, 1394, 77, 'NLMK'),
(2559, 1729, 77, 'NLMK'),
(2560, 1730, 77, 'NLMK'),
(2561, 1731, 77, 'NLMK'),
(2562, 1732, 77, 'NLMK'),
(2563, 1733, 57, 'WCPP'),
(2564, 1711, 63, 'HDFC'),
(2565, 1734, 28, 'KLIP'),
(2566, 1521, 22, 'THLV'),
(2567, 1521, 72, 'DAEG'),
(2568, 1521, 76, 'EOBN'),
(2569, 1735, 76, 'EOBN'),
(2570, 1736, 76, 'EOBN'),
(2571, 1737, 76, 'EOBN'),
(2572, 1738, 76, 'EOBN'),
(2573, 1739, 76, 'EOBN'),
(2574, 1740, 76, 'EOBN'),
(2575, 1741, 76, 'EOBN'),
(2576, 1742, 53, 'MCFO'),
(2578, 1744, 60, 'HCCB'),
(2579, 1460, 60, 'HCCB'),
(2580, 1460, 63, 'HDFC'),
(2581, 1745, 60, 'HCCB'),
(2582, 1746, 60, 'HCCB'),
(2583, 1747, 60, 'HCCB'),
(2584, 1748, 60, 'HCCB'),
(2585, 1749, 60, 'HCCB'),
(2587, 1751, 60, 'HCCB'),
(2588, 875, 43, 'GSL1'),
(2589, 875, 60, 'HCCB'),
(2590, 884, 53, 'MCFO'),
(2591, 884, 60, 'HCCB'),
(2592, 884, 63, 'HDFC'),
(2593, 1752, 60, 'HCCB'),
(2594, 1753, 60, 'HCCB'),
(2595, 1754, 60, 'HCCB'),
(2596, 1755, 60, 'HCCB'),
(2597, 1756, 60, 'HCCB'),
(2599, 1758, 60, 'HCCB'),
(2600, 1569, 60, 'HCCB'),
(2601, 1569, 63, 'HDFC'),
(2602, 1759, 60, 'HCCB'),
(2603, 1760, 60, 'HCCB'),
(2604, 1761, 60, 'HCCB'),
(2605, 1762, 60, 'HCCB'),
(2606, 1763, 60, 'HCCB'),
(2607, 339, 42, 'KRGN'),
(2608, 339, 60, 'HCCB'),
(2611, 1764, 60, 'HCCB'),
(2614, 1767, 60, 'HCCB'),
(2615, 133, 28, 'KLIP'),
(2616, 133, 60, 'HCCB'),
(2617, 133, 63, 'HDFC'),
(2618, 1768, 60, 'HCCB'),
(2619, 343, 42, 'KRGN'),
(2620, 343, 60, 'HCCB'),
(2621, 1769, 60, 'HCCB'),
(2622, 1770, 60, 'HCCB'),
(2623, 1771, 60, 'HCCB'),
(2624, 1772, 60, 'HCCB'),
(2625, 1773, 60, 'HCCB'),
(2626, 1774, 60, 'HCCB'),
(2627, 1775, 60, 'HCCB'),
(2628, 1776, 60, 'HCCB'),
(2629, 1777, 60, 'HCCB'),
(2630, 1778, 60, 'HCCB'),
(2631, 1779, 60, 'HCCB'),
(2632, 1780, 60, 'HCCB'),
(2633, 1781, 60, 'HCCB'),
(2634, 1782, 60, 'HCCB'),
(2635, 1783, 60, 'HCCB'),
(2636, 1784, 60, 'HCCB'),
(2637, 1785, 60, 'HCCB'),
(2638, 1786, 60, 'HCCB'),
(2639, 1787, 60, 'HCCB'),
(2640, 1788, 60, 'HCCB'),
(2641, 1789, 60, 'HCCB'),
(2642, 1790, 60, 'HCCB'),
(2643, 425, 42, 'KRGN'),
(2644, 425, 60, 'HCCB'),
(2645, 1791, 60, 'HCCB'),
(2646, 1792, 60, 'HCCB'),
(2647, 1793, 60, 'HCCB'),
(2648, 1794, 60, 'HCCB'),
(2649, 1795, 60, 'HCCB'),
(2650, 1796, 66, 'SNUD'),
(2652, 1798, 60, 'HCCB'),
(2653, 1799, 60, 'HCCB'),
(2654, 454, 40, 'NMDC'),
(2655, 454, 60, 'HCCB'),
(2656, 1800, 28, 'KLIP'),
(2657, 254, 13, 'KVNN'),
(2658, 254, 64, 'EMDH'),
(2659, 254, 66, 'SNUD'),
(2660, 254, 67, 'PBGN'),
(2661, 254, 70, 'MSWS'),
(2662, 254, 72, 'DAEG'),
(2663, 254, 76, 'EOBN'),
(2664, 1801, 60, 'HCCB'),
(2665, 1802, 22, 'THLV'),
(2666, 1803, 22, 'THLV'),
(2667, 1804, 60, 'HCCB'),
(2668, 1805, 60, 'HCCB'),
(2669, 1806, 60, 'HCCB'),
(2670, 1807, 72, 'DAEG'),
(2673, 1808, 76, 'EOBN'),
(2674, 1809, 76, 'EOBN'),
(2675, 1810, 76, 'EOBN'),
(2677, 1812, 76, 'EOBN'),
(2678, 1813, 66, 'SNUD'),
(2679, 1814, 66, 'SNUD'),
(2680, 1700, 63, 'HDFC'),
(2681, 1815, 65, 'PATP'),
(2682, 1816, 65, 'PATP'),
(2683, 1817, 65, 'PATP'),
(2684, 1818, 65, 'PATP'),
(2685, 1819, 65, 'PATP'),
(2686, 1820, 65, 'PATP'),
(2687, 1821, 65, 'PATP'),
(2688, 46, 25, 'NIRP'),
(2689, 46, 26, 'MLD2'),
(2690, 46, 65, 'PATP'),
(2691, 1701, 63, 'HDFC'),
(2692, 1822, 28, 'KLIP'),
(2693, 1823, 57, 'WCPP'),
(2694, 1824, 57, 'WCPP'),
(2695, 1825, 63, 'HDFC'),
(2696, 1826, 63, 'HDFC'),
(2697, 1827, 4, 'AIHR'),
(2698, 195, 22, 'THLV'),
(2699, 195, 72, 'DAEG'),
(2700, 1828, 72, 'DAEG'),
(2701, 1829, 72, 'DAEG'),
(2703, 1830, 13, 'KVNN'),
(2704, 1653, 13, 'KVNN'),
(2705, 1653, 64, 'EMDH'),
(2706, 1831, 65, 'PATP'),
(2707, 1832, 48, 'MGLF'),
(2708, 1833, 66, 'SNUD'),
(2709, 1834, 66, 'SNUD'),
(2710, 1835, 66, 'SNUD'),
(2711, 1836, 63, 'HDFC'),
(2712, 1837, 15, 'NLDA'),
(2713, 1838, 15, 'NLDA'),
(2715, 1839, 40, 'NMDC'),
(2716, 1840, 40, 'NMDC'),
(2717, 1841, 40, 'NMDC'),
(2718, 1842, 40, 'NMDC'),
(2719, 1843, 67, 'PBGN'),
(2720, 1844, 63, 'HDFC'),
(2721, 1845, 63, 'HDFC'),
(2722, 1750, 60, 'HCCB'),
(2723, 1846, 60, 'HCCB'),
(2724, 1847, 60, 'HCCB'),
(2725, 1848, 60, 'HCCB'),
(2726, 1849, 60, 'HCCB'),
(2727, 1850, 60, 'HCCB'),
(2728, 1851, 60, 'HCCB'),
(2729, 1852, 60, 'HCCB'),
(2730, 1853, 28, 'KLIP'),
(2731, 1670, 28, 'KLIP'),
(2732, 1670, 57, 'WCPP'),
(2733, 1854, 28, 'KLIP'),
(2735, 1855, 63, 'HDFC'),
(2737, 1857, 35, 'TSL1'),
(2738, 1858, 77, 'NLMK'),
(2739, 1859, 77, 'NLMK'),
(2740, 1860, 77, 'NLMK'),
(2741, 1861, 66, 'SNUD'),
(2742, 1862, 66, 'SNUD'),
(2743, 1863, 72, 'DAEG'),
(2744, 1103, 40, 'NMDC'),
(2745, 1103, 63, 'HDFC'),
(2746, 1864, 72, 'DAEG'),
(2748, 1866, 63, 'HDFC'),
(2749, 1867, 60, 'HCCB'),
(2750, 1868, 60, 'HCCB'),
(2753, 1871, 60, 'HCCB'),
(2754, 1872, 60, 'HCCB'),
(2755, 1873, 60, 'HCCB'),
(2756, 1874, 76, 'EOBN'),
(2757, 1875, 72, 'DAEG'),
(2758, 33, 30, 'ATTP'),
(2759, 33, 43, 'GSL1'),
(2760, 1876, 58, 'MBWS'),
(2761, 1877, 65, 'PATP'),
(2762, 1878, 65, 'PATP'),
(2763, 1879, 66, 'SNUD'),
(2764, 1577, 63, 'HDFC'),
(2765, 1880, 60, 'HCCB'),
(2767, 1881, 13, 'KVNN'),
(2768, 1882, 13, 'KVNN'),
(2769, 1883, 76, 'EOBN'),
(2770, 1884, 66, 'SNUD'),
(2771, 1811, 67, 'PBGN'),
(2772, 1811, 76, 'EOBN'),
(2773, 1885, 18, 'RECG'),
(2774, 1886, 13, 'KVNN'),
(2775, 1887, 28, 'KLIP'),
(2776, 1888, 43, 'GSL1'),
(2777, 1889, 65, 'PATP'),
(2778, 1890, 40, 'NMDC'),
(2779, 1891, 40, 'NMDC'),
(2780, 1892, 60, 'HCCB'),
(2781, 1893, 66, 'SNUD'),
(2782, 1894, 66, 'SNUD'),
(2783, 1895, 66, 'SNUD'),
(2784, 1896, 66, 'SNUD'),
(2785, 1897, 66, 'SNUD'),
(2787, 1899, 15, 'NLDA'),
(2789, 1901, 22, 'THLV'),
(2790, 1902, 72, 'DAEG'),
(2791, 1903, 72, 'DAEG'),
(2792, 1904, 72, 'DAEG'),
(2793, 1905, 22, 'THLV'),
(2794, 191, 22, 'THLV'),
(2795, 191, 72, 'DAEG'),
(2796, 1906, 72, 'DAEG'),
(2797, 1900, 22, 'THLV'),
(2798, 1900, 72, 'DAEG'),
(2799, 1907, 53, 'MCFO'),
(2800, 1908, 53, 'MCFO'),
(2801, 1523, 51, 'LTFO'),
(2802, 1523, 52, 'STFO'),
(2803, 1523, 53, 'MCFO'),
(2804, 1909, 63, 'HDFC'),
(2805, 1910, 60, 'HCCB'),
(2806, 951, 43, 'GSL1'),
(2807, 951, 60, 'HCCB'),
(2808, 1911, 76, 'EOBN'),
(2809, 1455, 22, 'THLV'),
(2810, 1455, 76, 'EOBN'),
(2811, 1912, 67, 'PBGN'),
(2812, 1913, 65, 'PATP'),
(2813, 1914, 60, 'HCCB'),
(2814, 1915, 60, 'HCCB'),
(2816, 1917, 28, 'KLIP'),
(2817, 1856, 63, 'HDFC'),
(2818, 1918, 72, 'DAEG'),
(2819, 1919, 72, 'DAEG'),
(2820, 1920, 72, 'DAEG'),
(2821, 1921, 60, 'HCCB'),
(2822, 1922, 60, 'HCCB'),
(2823, 1923, 60, 'HCCB'),
(2824, 1924, 65, 'PATP'),
(2825, 166, 28, 'KLIP'),
(2826, 166, 65, 'PATP'),
(2827, 1584, 67, 'PBGN'),
(2828, 1584, 72, 'DAEG'),
(2829, 1584, 76, 'EOBN'),
(2830, 248, 15, 'NLDA'),
(2831, 248, 66, 'SNUD'),
(2832, 1925, 53, 'MCFO'),
(2833, 1926, 53, 'MCFO'),
(2834, 1927, 60, 'HCCB'),
(2835, 1928, 43, 'GSL1'),
(2836, 1929, 72, 'DAEG'),
(2837, 1930, 72, 'DAEG'),
(2838, 1931, 72, 'DAEG'),
(2841, 1934, 72, 'DAEG'),
(2842, 1935, 60, 'HCCB'),
(2843, 1936, 60, 'HCCB'),
(2844, 1937, 60, 'HCCB'),
(2845, 1766, 60, 'HCCB'),
(2846, 1938, 63, 'HDFC'),
(2847, 1645, 28, 'KLIP'),
(2848, 1645, 57, 'WCPP'),
(2849, 1939, 77, 'NLMK'),
(2850, 1932, 72, 'DAEG'),
(2851, 1932, 77, 'NLMK'),
(2852, 1940, 72, 'DAEG'),
(2853, 1933, 63, 'HDFC'),
(2854, 1941, 28, 'KLIP'),
(2855, 1942, 72, 'DAEG'),
(2856, 1943, 67, 'PBGN'),
(2857, 1944, 67, 'PBGN'),
(2858, 1945, 60, 'HCCB'),
(2859, 1946, 60, 'HCCB'),
(2861, 1948, 57, 'WCPP'),
(2862, 1949, 64, 'EMDH'),
(2863, 1950, 22, 'THLV'),
(2864, 1951, 22, 'THLV'),
(2865, 1952, 13, 'KVNN'),
(2866, 1953, 13, 'KVNN'),
(2867, 1954, 77, 'NLMK'),
(2870, 1955, 28, 'KLIP'),
(2871, 1332, 43, 'GSL1'),
(2872, 1332, 66, 'SNUD'),
(2873, 1956, 48, 'MGLF'),
(2874, 73, 27, 'KLRD'),
(2875, 73, 28, 'KLIP'),
(2876, 1957, 28, 'KLIP'),
(2877, 1958, 63, 'HDFC'),
(2878, 1959, 28, 'KLIP'),
(2879, 1960, 72, 'DAEG'),
(2880, 1961, 28, 'KLIP'),
(2881, 1961, 30, 'ATTP'),
(2882, 1961, 31, 'ATP2'),
(2883, 1961, 57, 'WCPP'),
(2884, 1962, 72, 'DAEG'),
(2885, 1963, 63, 'HDFC'),
(2886, 1964, 72, 'DAEG'),
(2890, 1968, 60, 'HCCB'),
(2892, 1970, 60, 'HCCB'),
(2893, 1971, 60, 'HCCB'),
(2894, 1972, 13, 'KVNN'),
(2895, 1973, 67, 'PBGN'),
(2896, 1974, 67, 'PBGN'),
(2897, 1975, 67, 'PBGN'),
(2898, 1976, 67, 'PBGN'),
(2899, 1977, 43, 'GSL1'),
(2900, 1978, 43, 'GSL1'),
(2901, 1979, 43, 'GSL1'),
(2902, 1980, 63, 'HDFC'),
(2903, 1981, 15, 'NLDA'),
(2904, 1982, 63, 'HDFC'),
(2905, 1983, 28, 'KLIP'),
(2906, 1984, 76, 'EOBN'),
(2907, 1985, 57, 'WCPP'),
(2908, 1986, 57, 'WCPP'),
(2909, 1987, 18, 'RECG'),
(2910, 1988, 18, 'RECG'),
(2911, 1765, 60, 'HCCB'),
(2912, 1765, 63, 'HDFC'),
(2913, 1989, 63, 'HDFC'),
(2914, 1990, 63, 'HDFC'),
(2916, 593, 45, 'CUBG'),
(2917, 593, 63, 'HDFC'),
(2920, 1994, 60, 'HCCB'),
(2922, 1967, 60, 'HCCB');
INSERT INTO `oc_contractor_location` (`id`, `contractor_id`, `unit_id`, `unit`) VALUES
(2923, 1996, 60, 'HCCB'),
(2924, 1969, 60, 'HCCB'),
(2925, 1997, 60, 'HCCB'),
(2926, 1992, 63, 'HDFC'),
(2927, 1998, 30, 'ATTP'),
(2928, 1999, 63, 'HDFC'),
(2930, 2001, 63, 'HDFC'),
(2931, 2002, 28, 'KLIP'),
(2932, 1743, 30, 'ATTP'),
(2933, 1743, 57, 'WCPP'),
(2934, 2003, 30, 'ATTP'),
(2935, 2004, 65, 'PATP'),
(2936, 2005, 18, 'RECG'),
(2937, 2006, 63, 'HDFC'),
(2938, 2007, 40, 'NMDC'),
(2939, 2008, 40, 'NMDC'),
(2940, 2009, 40, 'NMDC'),
(2941, 2010, 40, 'NMDC'),
(2942, 873, 40, 'NMDC'),
(2943, 873, 43, 'GSL1'),
(2944, 700, 39, 'NTPC'),
(2945, 700, 40, 'NMDC'),
(2946, 2011, 66, 'SNUD'),
(2947, 2012, 66, 'SNUD'),
(2948, 2013, 63, 'HDFC'),
(2949, 2000, 63, 'HDFC'),
(2950, 2014, 65, 'PATP'),
(2951, 2015, 57, 'WCPP'),
(2952, 2016, 63, 'HDFC'),
(2953, 2017, 72, 'DAEG'),
(2954, 2018, 72, 'DAEG'),
(2955, 120, 15, 'NLDA'),
(2956, 120, 27, 'KLRD'),
(2957, 120, 28, 'KLIP'),
(2958, 120, 57, 'WCPP'),
(2959, 2019, 63, 'HDFC'),
(2960, 2020, 65, 'PATP'),
(2964, 2021, 57, 'WCPP'),
(2965, 2022, 65, 'PATP'),
(2967, 2024, 60, 'HCCB'),
(2968, 2025, 60, 'HCCB'),
(2969, 1995, 60, 'HCCB'),
(2970, 2026, 60, 'HCCB'),
(2971, 1993, 60, 'HCCB'),
(2972, 1966, 60, 'HCCB'),
(2975, 2027, 30, 'ATTP'),
(2976, 2028, 30, 'ATTP'),
(2977, 2029, 76, 'EOBN'),
(2978, 2030, 66, 'SNUD'),
(2979, 1183, 27, 'KLRD'),
(2980, 1183, 30, 'ATTP'),
(2981, 1183, 31, 'ATP2'),
(2982, 1183, 57, 'WCPP'),
(2983, 1183, 65, 'PATP'),
(2984, 328, 27, 'KLRD'),
(2985, 328, 28, 'KLIP'),
(2986, 328, 57, 'WCPP'),
(2987, 328, 65, 'PATP'),
(2988, 2031, 65, 'PATP'),
(2989, 1865, 63, 'HDFC'),
(2991, 2033, 63, 'HDFC'),
(2992, 2034, 63, 'HDFC'),
(2993, 2035, 63, 'HDFC'),
(2994, 2036, 63, 'HDFC'),
(2995, 2037, 63, 'HDFC'),
(2998, 2038, 63, 'HDFC'),
(2999, 2039, 63, 'HDFC'),
(3000, 2040, 28, 'KLIP'),
(3001, 2041, 72, 'DAEG'),
(3002, 2042, 72, 'DAEG'),
(3003, 2043, 72, 'DAEG'),
(3004, 1548, 64, 'EMDH'),
(3005, 1548, 65, 'PATP'),
(3006, 1965, 60, 'HCCB'),
(3007, 2044, 60, 'HCCB'),
(3008, 2045, 65, 'PATP'),
(3009, 2046, 76, 'EOBN'),
(3010, 2047, 76, 'EOBN'),
(3014, 2048, 13, 'KVNN'),
(3015, 2049, 67, 'PBGN'),
(3016, 2050, 67, 'PBGN'),
(3017, 2051, 63, 'HDFC'),
(3018, 2052, 63, 'HDFC'),
(3019, 2053, 63, 'HDFC'),
(3020, 2054, 63, 'HDFC'),
(3021, 2055, 15, 'NLDA'),
(3022, 2056, 65, 'PATP'),
(3023, 2057, 65, 'PATP'),
(3024, 2058, 72, 'DAEG'),
(3025, 2059, 72, 'DAEG'),
(3026, 2060, 72, 'DAEG'),
(3031, 2062, 28, 'KLIP'),
(3032, 2063, 72, 'DAEG'),
(3033, 2064, 72, 'DAEG'),
(3034, 2065, 72, 'DAEG'),
(3035, 2066, 72, 'DAEG'),
(3036, 2067, 53, 'MCFO'),
(3037, 2068, 76, 'EOBN'),
(3038, 2069, 76, 'EOBN'),
(3039, 2070, 76, 'EOBN'),
(3040, 2071, 76, 'EOBN'),
(3041, 362, 40, 'NMDC'),
(3042, 362, 60, 'HCCB'),
(3043, 362, 76, 'EOBN'),
(3044, 2072, 18, 'RECG'),
(3045, 2073, 78, 'DEWD'),
(3046, 2074, 78, 'DEWD'),
(3047, 2075, 78, 'DEWD'),
(3048, 2076, 78, 'DEWD'),
(3049, 2077, 78, 'DEWD'),
(3050, 2078, 78, 'DEWD'),
(3051, 296, 22, 'THLV'),
(3052, 296, 78, 'DEWD'),
(3053, 2079, 72, 'DAEG'),
(3054, 2080, 72, 'DAEG'),
(3055, 2081, 72, 'DAEG'),
(3056, 2082, 72, 'DAEG'),
(3057, 2083, 72, 'DAEG'),
(3058, 2084, 67, 'PBGN'),
(3059, 2085, 30, 'ATTP'),
(3060, 2086, 72, 'DAEG'),
(3061, 2087, 67, 'PBGN'),
(3062, 347, 6, 'EMGG'),
(3063, 347, 17, 'PHPG'),
(3064, 347, 67, 'PBGN'),
(3065, 2088, 57, 'WCPP'),
(3066, 2089, 77, 'NLMK'),
(3067, 2090, 77, 'NLMK'),
(3068, 2091, 77, 'NLMK'),
(3069, 2092, 77, 'NLMK'),
(3070, 2093, 77, 'NLMK'),
(3071, 2094, 65, 'PATP'),
(3072, 2095, 65, 'PATP'),
(3073, 2096, 65, 'PATP'),
(3074, 2097, 28, 'KLIP'),
(3075, 2098, 28, 'KLIP'),
(3076, 2099, 28, 'KLIP'),
(3077, 2100, 28, 'KLIP'),
(3078, 2101, 57, 'WCPP'),
(3079, 2102, 28, 'KLIP'),
(3080, 2103, 66, 'SNUD'),
(3081, 2104, 66, 'SNUD'),
(3082, 2105, 66, 'SNUD'),
(3083, 2106, 66, 'SNUD'),
(3084, 2107, 66, 'SNUD'),
(3085, 2108, 66, 'SNUD'),
(3086, 2109, 66, 'SNUD'),
(3087, 2110, 66, 'SNUD'),
(3088, 2111, 66, 'SNUD'),
(3089, 2112, 66, 'SNUD'),
(3090, 2113, 72, 'DAEG'),
(3091, 2114, 28, 'KLIP'),
(3092, 2115, 28, 'KLIP'),
(3093, 2116, 28, 'KLIP'),
(3094, 1357, 40, 'NMDC'),
(3095, 1357, 76, 'EOBN'),
(3096, 2117, 78, 'DEWD'),
(3097, 2118, 78, 'DEWD'),
(3099, 2119, 67, 'PBGN'),
(3100, 2120, 66, 'SNUD'),
(3101, 2121, 66, 'SNUD'),
(3102, 2122, 66, 'SNUD'),
(3103, 8, 6, 'EMGG'),
(3104, 8, 15, 'NLDA'),
(3105, 8, 18, 'RECG'),
(3106, 8, 22, 'THLV'),
(3107, 8, 25, 'NIRP'),
(3108, 8, 28, 'KLIP'),
(3109, 8, 30, 'ATTP'),
(3110, 8, 48, 'MGLF'),
(3111, 8, 49, 'MGLFCASTINGYARD'),
(3112, 8, 65, 'PATP'),
(3113, 8, 77, 'NLMK'),
(3114, 8, 78, 'DEWD'),
(3115, 2123, 66, 'SNUD'),
(3116, 2124, 66, 'SNUD'),
(3117, 367, 40, 'NMDC'),
(3118, 367, 66, 'SNUD'),
(3119, 810, 22, 'THLV'),
(3120, 810, 66, 'SNUD'),
(3121, 2125, 53, 'MCFO'),
(3122, 2126, 53, 'MCFO'),
(3123, 1301, 22, 'THLV'),
(3124, 1301, 64, 'EMDH'),
(3125, 1301, 72, 'DAEG'),
(3126, 1301, 76, 'EOBN'),
(3127, 2127, 72, 'DAEG'),
(3128, 1194, 51, 'LTFO'),
(3129, 1194, 53, 'MCFO'),
(3132, 1870, 60, 'HCCB'),
(3133, 1869, 60, 'HCCB'),
(3134, 2130, 63, 'HDFC'),
(3138, 2132, 28, 'KLIP'),
(3139, 2133, 53, 'MCFO'),
(3140, 2134, 53, 'MCFO'),
(3141, 2135, 67, 'PBGN'),
(3142, 2136, 43, 'GSL1'),
(3143, 2137, 43, 'GSL1'),
(3144, 2138, 43, 'GSL1'),
(3145, 2139, 63, 'HDFC'),
(3146, 2140, 66, 'SNUD'),
(3147, 2141, 57, 'WCPP'),
(3148, 2142, 57, 'WCPP'),
(3149, 1898, 28, 'KLIP'),
(3150, 1898, 57, 'WCPP'),
(3151, 2143, 53, 'MCFO'),
(3152, 2144, 67, 'PBGN'),
(3153, 2145, 76, 'EOBN'),
(3154, 2146, 76, 'EOBN'),
(3157, 2149, 43, 'GSL1'),
(3160, 2150, 63, 'HDFC'),
(3161, 1757, 60, 'HCCB'),
(3162, 1757, 77, 'NLMK'),
(3163, 2152, 77, 'NLMK'),
(3164, 2153, 77, 'NLMK'),
(3165, 2154, 57, 'WCPP'),
(3166, 2155, 53, 'MCFO'),
(3167, 2156, 65, 'PATP'),
(3168, 2032, 63, 'HDFC'),
(3169, 2147, 63, 'HDFC'),
(3170, 2157, 63, 'HDFC'),
(3171, 2158, 63, 'HDFC'),
(3173, 2160, 72, 'DAEG'),
(3174, 2161, 28, 'KLIP'),
(3175, 2162, 28, 'KLIP'),
(3176, 2163, 28, 'KLIP'),
(3177, 576, 65, 'PATP'),
(3179, 2159, 63, 'HDFC'),
(3180, 1666, 66, 'SNUD'),
(3181, 1666, 76, 'EOBN'),
(3182, 2165, 72, 'DAEG'),
(3183, 2166, 72, 'DAEG'),
(3184, 2167, 72, 'DAEG'),
(3185, 2168, 72, 'DAEG'),
(3186, 2169, 72, 'DAEG'),
(3187, 2170, 72, 'DAEG'),
(3188, 2171, 72, 'DAEG'),
(3189, 273, 66, 'SNUD'),
(3190, 273, 72, 'DAEG'),
(3191, 2172, 60, 'HCCB'),
(3192, 2173, 60, 'HCCB'),
(3193, 2174, 43, 'GSL1'),
(3194, 2175, 43, 'GSL1'),
(3195, 2176, 43, 'GSL1'),
(3196, 2177, 78, 'DEWD'),
(3197, 2178, 78, 'DEWD'),
(3198, 2179, 78, 'DEWD'),
(3199, 2180, 78, 'DEWD'),
(3200, 306, 21, 'THGG'),
(3201, 306, 22, 'THLV'),
(3202, 306, 78, 'DEWD'),
(3203, 2181, 66, 'SNUD'),
(3204, 521, 4, 'AIHR'),
(3205, 521, 66, 'SNUD'),
(3206, 2182, 66, 'SNUD'),
(3207, 2183, 65, 'PATP'),
(3208, 2184, 28, 'KLIP'),
(3209, 2185, 28, 'KLIP'),
(3211, 267, 22, 'THLV'),
(3212, 267, 76, 'EOBN'),
(3213, 1671, 22, 'THLV'),
(3214, 1671, 76, 'EOBN'),
(3215, 927, 22, 'THLV'),
(3216, 927, 76, 'EOBN'),
(3217, 1515, 22, 'THLV'),
(3218, 1515, 76, 'EOBN'),
(3219, 194, 22, 'THLV'),
(3220, 194, 76, 'EOBN'),
(3221, 2187, 28, 'KLIP'),
(3224, 2128, 60, 'HCCB'),
(3225, 2188, 77, 'NLMK'),
(3226, 2189, 77, 'NLMK'),
(3227, 2190, 77, 'NLMK'),
(3228, 2191, 77, 'NLMK'),
(3229, 2192, 77, 'NLMK'),
(3230, 2023, 60, 'HCCB'),
(3231, 2023, 63, 'HDFC'),
(3232, 688, 22, 'THLV'),
(3233, 688, 66, 'SNUD'),
(3234, 688, 76, 'EOBN'),
(3235, 2193, 28, 'KLIP'),
(3236, 2194, 28, 'KLIP'),
(3237, 2195, 28, 'KLIP'),
(3238, 2196, 76, 'EOBN'),
(3239, 2197, 76, 'EOBN'),
(3240, 2198, 81, 'WEPL'),
(3241, 2199, 81, 'WEPL'),
(3242, 2200, 81, 'WEPL'),
(3243, 2201, 43, 'GSL1'),
(3244, 1678, 15, 'NLDA'),
(3245, 1678, 40, 'NMDC'),
(3246, 746, 15, 'NLDA'),
(3247, 746, 40, 'NMDC'),
(3248, 746, 42, 'KRGN'),
(3249, 257, 6, 'EMGG'),
(3250, 257, 60, 'HCCB'),
(3251, 1797, 60, 'HCCB'),
(3253, 2203, 80, 'VEPL'),
(3254, 2204, 80, 'VEPL'),
(3256, 2206, 43, 'GSL1'),
(3257, 423, 40, 'NMDC'),
(3258, 423, 42, 'KRGN'),
(3259, 423, 43, 'GSL1'),
(3260, 1422, 40, 'NMDC'),
(3261, 1422, 63, 'HDFC'),
(3262, 1991, 63, 'HDFC'),
(3263, 2148, 63, 'HDFC'),
(3264, 2207, 63, 'HDFC'),
(3265, 2208, 15, 'NLDA'),
(3266, 2202, 63, 'HDFC'),
(3267, 2209, 72, 'DAEG'),
(3268, 2210, 77, 'NLMK'),
(3269, 2211, 77, 'NLMK'),
(3270, 451, 40, 'NMDC'),
(3271, 451, 77, 'NLMK'),
(3272, 2131, 63, 'HDFC'),
(3273, 1559, 40, 'NMDC'),
(3274, 1559, 63, 'HDFC'),
(3275, 455, 40, 'NMDC'),
(3276, 455, 63, 'HDFC'),
(3277, 2186, 63, 'HDFC'),
(3278, 2212, 65, 'PATP'),
(3279, 2213, 77, 'NLMK'),
(3280, 2214, 28, 'KLIP'),
(3281, 2215, 22, 'THLV'),
(3282, 2216, 60, 'HCCB'),
(3283, 2217, 44, 'ERSK'),
(3284, 2218, 57, 'WCPP'),
(3285, 2219, 13, 'KVNN'),
(3286, 2220, 13, 'KVNN'),
(3287, 2221, 13, 'KVNN'),
(3288, 2222, 13, 'KVNN'),
(3289, 2223, 13, 'KVNN'),
(3290, 2224, 13, 'KVNN'),
(3291, 2225, 13, 'KVNN'),
(3292, 147, 28, 'KLIP'),
(3295, 2226, 43, 'GSL1'),
(3296, 1179, 13, 'KVNN'),
(3297, 2227, 43, 'GSL1'),
(3298, 2228, 43, 'GSL1'),
(3299, 2229, 76, 'EOBN'),
(3301, 2231, 63, 'HDFC'),
(3302, 2232, 79, 'BBEPL'),
(3303, 2233, 63, 'HDFC'),
(3304, 2151, 63, 'HDFC'),
(3305, 2234, 63, 'HDFC'),
(3306, 2235, 66, 'SNUD'),
(3309, 2236, 66, 'SNUD'),
(3310, 2237, 63, 'HDFC'),
(3311, 2238, 60, 'HCCB'),
(3312, 1916, 60, 'HCCB'),
(3314, 2240, 67, 'PBGN'),
(3315, 2241, 67, 'PBGN'),
(3316, 2242, 15, 'NLDA'),
(3317, 2243, 15, 'NLDA'),
(3318, 2244, 15, 'NLDA'),
(3319, 2245, 15, 'NLDA'),
(3320, 2246, 15, 'NLDA'),
(3321, 2247, 15, 'NLDA'),
(3322, 2248, 15, 'NLDA'),
(3323, 2249, 15, 'NLDA'),
(3324, 2250, 15, 'NLDA'),
(3325, 2251, 15, 'NLDA'),
(3326, 2252, 15, 'NLDA'),
(3327, 2253, 15, 'NLDA'),
(3328, 2254, 15, 'NLDA'),
(3329, 2255, 15, 'NLDA'),
(3330, 2256, 15, 'NLDA'),
(3331, 2257, 15, 'NLDA'),
(3332, 2258, 15, 'NLDA'),
(3333, 2259, 15, 'NLDA'),
(3334, 2260, 15, 'NLDA'),
(3335, 2261, 15, 'NLDA'),
(3336, 2262, 15, 'NLDA'),
(3337, 2263, 15, 'NLDA'),
(3338, 2264, 15, 'NLDA'),
(3339, 1002, 15, 'NLDA'),
(3340, 1002, 40, 'NMDC'),
(3341, 2265, 28, 'KLIP'),
(3342, 2266, 72, 'DAEG'),
(3343, 2267, 72, 'DAEG'),
(3344, 1478, 1, 'AIHB'),
(3345, 1478, 2, 'AHRB'),
(3346, 1478, 3, 'AIH1'),
(3347, 1478, 4, 'AIHR'),
(3348, 1478, 5, 'DLOF'),
(3349, 1478, 6, 'EMGG'),
(3350, 1478, 7, 'JPG'),
(3351, 1478, 8, 'JPGH'),
(3352, 1478, 9, 'JPKI'),
(3353, 1478, 10, 'JPNB'),
(3354, 1478, 11, 'JPTH'),
(3355, 1478, 12, 'KMKH'),
(3356, 1478, 13, 'KVNN'),
(3357, 1478, 14, 'MMHP'),
(3358, 1478, 15, 'NLDA'),
(3359, 1478, 16, 'PEMD'),
(3360, 1478, 17, 'PHPG'),
(3361, 1478, 18, 'RECG'),
(3362, 1478, 19, 'RSNO'),
(3363, 1478, 20, 'SCAO'),
(3364, 1478, 21, 'THGG'),
(3365, 1478, 22, 'THLV'),
(3366, 1478, 23, 'TAGP'),
(3367, 1478, 24, 'PUNE'),
(3368, 1478, 25, 'NIRP'),
(3369, 1478, 26, 'MLD2'),
(3370, 1478, 27, 'KLRD'),
(3371, 1478, 28, 'KLIP'),
(3372, 1478, 29, 'IIPH'),
(3373, 1478, 30, 'ATTP'),
(3374, 1478, 31, 'ATP2'),
(3375, 1478, 32, 'VTRF'),
(3376, 1478, 33, 'VMRF'),
(3377, 1478, 34, 'TSL2'),
(3378, 1478, 35, 'TSL1'),
(3379, 1478, 36, 'TPPR'),
(3380, 1478, 37, 'STP1'),
(3381, 1478, 38, 'SKSP'),
(3382, 1478, 39, 'NTPC'),
(3383, 1478, 40, 'NMDC'),
(3384, 1478, 41, 'MECN'),
(3385, 1478, 42, 'KRGN'),
(3386, 1478, 43, 'GSL1'),
(3387, 1478, 44, 'ERSK'),
(3388, 1478, 45, 'CUBG'),
(3389, 1478, 46, 'BSEB'),
(3390, 1478, 47, 'OZAP'),
(3391, 1478, 48, 'MGLF'),
(3392, 1478, 49, 'MGLFCASTINGYARD'),
(3393, 1478, 50, 'BHFS'),
(3394, 1478, 51, 'LTFO'),
(3395, 1478, 52, 'STFO'),
(3396, 1478, 53, 'MCFO'),
(3397, 1478, 54, 'NRDA'),
(3398, 1478, 55, 'BWSP'),
(3399, 1478, 56, 'MAIP'),
(3400, 1478, 57, 'WCPP'),
(3401, 1478, 58, 'MBWS'),
(3402, 1478, 59, 'MJIP'),
(3403, 1478, 60, 'HCCB'),
(3404, 1478, 62, 'OPRC'),
(3405, 1478, 63, 'HDFC'),
(3406, 1478, 64, 'EMDH'),
(3407, 1478, 65, 'PATP'),
(3408, 1478, 66, 'SNUD'),
(3409, 1478, 67, 'PBGN'),
(3410, 1478, 68, 'DGWS'),
(3411, 1478, 70, 'MSWS'),
(3412, 1478, 71, 'SKWS'),
(3413, 1478, 72, 'DAEG'),
(3414, 1478, 73, 'BMWS'),
(3415, 1478, 74, 'MWSP'),
(3416, 1478, 75, 'MWSS'),
(3417, 1478, 76, 'EOBN'),
(3418, 1478, 77, 'NLMK'),
(3419, 1478, 78, 'DEWD'),
(3420, 1478, 79, 'BBEPL'),
(3421, 1478, 80, 'VEPL'),
(3422, 1478, 81, 'WEPL'),
(3425, 2270, 60, 'HCCB'),
(3426, 2271, 60, 'HCCB'),
(3427, 2129, 60, 'HCCB'),
(3428, 838, 15, 'NLDA'),
(3429, 838, 39, 'NTPC'),
(3430, 2272, 15, 'NLDA'),
(3431, 2273, 15, 'NLDA'),
(3432, 2274, 15, 'NLDA'),
(3433, 2275, 15, 'NLDA'),
(3434, 2276, 15, 'NLDA'),
(3436, 2278, 15, 'NLDA'),
(3437, 2279, 15, 'NLDA'),
(3438, 2280, 15, 'NLDA'),
(3439, 2281, 15, 'NLDA'),
(3440, 2282, 15, 'NLDA'),
(3442, 2284, 15, 'NLDA'),
(3443, 2285, 15, 'NLDA'),
(3444, 2286, 15, 'NLDA'),
(3445, 2287, 15, 'NLDA'),
(3446, 2288, 15, 'NLDA'),
(3447, 2289, 15, 'NLDA'),
(3448, 2283, 15, 'NLDA'),
(3449, 2290, 15, 'NLDA'),
(3450, 2291, 15, 'NLDA'),
(3453, 2277, 15, 'NLDA'),
(3454, 2292, 40, 'NMDC'),
(3455, 705, 39, 'NTPC'),
(3456, 705, 77, 'NLMK'),
(3457, 2293, 77, 'NLMK'),
(3458, 2294, 77, 'NLMK'),
(3462, 2239, 63, 'HDFC'),
(3463, 2295, 28, 'KLIP'),
(3469, 1314, 13, 'KVNN'),
(3470, 1314, 22, 'THLV'),
(3471, 469, 15, 'NLDA'),
(3472, 469, 42, 'KRGN'),
(3473, 378, 15, 'NLDA'),
(3474, 378, 35, 'TSL1'),
(3475, 378, 39, 'NTPC'),
(3476, 378, 40, 'NMDC'),
(3477, 378, 42, 'KRGN'),
(3478, 378, 63, 'HDFC'),
(3479, 336, 15, 'NLDA'),
(3480, 336, 40, 'NMDC'),
(3481, 379, 15, 'NLDA'),
(3482, 379, 39, 'NTPC'),
(3483, 2297, 15, 'NLDA'),
(3484, 1352, 15, 'NLDA'),
(3485, 1352, 40, 'NMDC'),
(3486, 2298, 15, 'NLDA'),
(3487, 2299, 15, 'NLDA'),
(3488, 1461, 15, 'NLDA'),
(3489, 1461, 39, 'NTPC'),
(3490, 229, 13, 'KVNN'),
(3491, 229, 15, 'NLDA'),
(3492, 2300, 15, 'NLDA'),
(3493, 535, 15, 'NLDA'),
(3494, 535, 40, 'NMDC'),
(3495, 535, 43, 'GSL1'),
(3496, 2301, 60, 'HCCB'),
(3497, 904, 35, 'TSL1'),
(3498, 904, 63, 'HDFC'),
(3508, 632, 35, 'TSL1'),
(3509, 632, 43, 'GSL1'),
(3510, 2306, 66, 'SNUD'),
(3511, 2307, 66, 'SNUD'),
(3512, 2308, 66, 'SNUD'),
(3513, 2305, 86, 'SBUT'),
(3516, 2302, 86, 'SBUT'),
(3522, 1947, 27, 'KLRD'),
(3523, 1947, 28, 'KLIP'),
(3524, 1947, 30, 'ATTP'),
(3525, 1947, 57, 'WCPP'),
(3526, 1947, 86, 'SBUT'),
(3527, 963, 28, 'KLIP'),
(3528, 963, 86, 'SBUT'),
(3529, 2303, 86, 'SBUT'),
(3530, 2309, 86, 'SBUT'),
(3531, 2310, 66, 'SNUD'),
(3532, 2311, 63, 'HDFC'),
(3533, 2312, 15, 'NLDA'),
(3535, 2314, 15, 'NLDA'),
(3536, 2315, 15, 'NLDA'),
(3537, 2316, 15, 'NLDA'),
(3538, 2317, 15, 'NLDA'),
(3539, 2318, 15, 'NLDA'),
(3540, 2319, 15, 'NLDA'),
(3541, 699, 15, 'NLDA'),
(3542, 699, 39, 'NTPC'),
(3543, 699, 42, 'KRGN'),
(3544, 1376, 15, 'NLDA'),
(3545, 1376, 40, 'NMDC'),
(3546, 2313, 15, 'NLDA'),
(3547, 1307, 57, 'WCPP'),
(3548, 1307, 86, 'SBUT'),
(3549, 2320, 66, 'SNUD'),
(3550, 2321, 63, 'HDFC'),
(3551, 2230, 63, 'HDFC'),
(3552, 1011, 28, 'KLIP'),
(3553, 1011, 43, 'GSL1'),
(3554, 1011, 57, 'WCPP'),
(3555, 1011, 86, 'SBUT'),
(3556, 2304, 86, 'SBUT'),
(3557, 2061, 28, 'KLIP'),
(3558, 2061, 57, 'WCPP'),
(3559, 2061, 86, 'SBUT'),
(3560, 2322, 77, 'NLMK'),
(3561, 2164, 63, 'HDFC'),
(3562, 1652, 63, 'HDFC'),
(3564, 2268, 57, 'WCPP'),
(3566, 2205, 80, 'VEPL'),
(3567, 2323, 66, 'SNUD'),
(3568, 2324, 66, 'SNUD'),
(3569, 2325, 60, 'HCCB'),
(3570, 2326, 43, 'GSL1'),
(3571, 2327, 66, 'SNUD'),
(3572, 2328, 28, 'KLIP'),
(3573, 2329, 67, 'PBGN'),
(3574, 2330, 67, 'PBGN'),
(3575, 2296, 28, 'KLIP'),
(3577, 2332, 15, 'NLDA'),
(3578, 2331, 15, 'NLDA'),
(3579, 2333, 15, 'NLDA'),
(3580, 2334, 65, 'PATP'),
(3581, 2269, 63, 'HDFC'),
(3582, 2335, 66, 'SNUD'),
(3583, 2336, 28, 'KLIP'),
(3584, 2337, 13, 'KVNN'),
(3585, 1289, 22, 'THLV'),
(3586, 1289, 72, 'DAEG'),
(3587, 2338, 86, 'SBUT'),
(3588, 2339, 86, 'SBUT'),
(3589, 2340, 86, 'SBUT'),
(3590, 2341, 86, 'SBUT'),
(3591, 2342, 86, 'SBUT'),
(3592, 276, 22, 'THLV'),
(3593, 276, 66, 'SNUD'),
(3594, 2343, 60, 'HCCB'),
(3595, 937, 39, 'NTPC'),
(3596, 937, 40, 'NMDC'),
(3597, 937, 60, 'HCCB'),
(3599, 2345, 21, 'THGG'),
(3600, 2345, 22, 'THLV'),
(3601, 2346, 21, 'THGG'),
(3602, 2346, 22, 'THLV'),
(3603, 1200, 21, 'THGG'),
(3604, 1200, 22, 'THLV'),
(3605, 1200, 44, 'ERSK'),
(3606, 1200, 63, 'HDFC'),
(3607, 2347, 21, 'THGG'),
(3608, 2347, 22, 'THLV'),
(3609, 2348, 78, 'DEWD'),
(3610, 2349, 76, 'EOBN'),
(3611, 2344, 78, 'DEWD'),
(3612, 2350, 64, 'EMDH'),
(3613, 2351, 64, 'EMDH'),
(3614, 2352, 28, 'KLIP'),
(3615, 2353, 57, 'WCPP'),
(3616, 2354, 57, 'WCPP'),
(3617, 75, 27, 'KLRD'),
(3618, 75, 57, 'WCPP');

-- --------------------------------------------------------

--
-- Table structure for table `oc_country`
--

CREATE TABLE `oc_country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'Korea, Republic of', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(240, 'Jersey', 'JE', 'JEY', '', 1, 1),
(241, 'Guernsey', 'GG', 'GGY', '', 1, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon`
--

CREATE TABLE `oc_coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(10) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_coupon`
--

INSERT INTO `oc_coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date_added`) VALUES
(4, '-10% Discount', '2222', 'P', '10.0000', 0, 0, '0.0000', '2011-01-01', '2012-01-01', 10, '10', 1, '2009-01-27 13:55:03'),
(5, 'Free Shipping', '3333', 'P', '0.0000', 0, 1, '100.0000', '2009-03-01', '2009-08-31', 10, '10', 1, '2009-03-14 21:13:53'),
(6, '-10.00 Discount', '1111', 'F', '10.0000', 0, 0, '10.0000', '1970-11-01', '2020-11-01', 100000, '10000', 1, '2009-03-14 21:15:18');

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_category`
--

CREATE TABLE `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_history`
--

CREATE TABLE `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_product`
--

CREATE TABLE `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_currency`
--

CREATE TABLE `oc_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Pound Sterling', 'GBP', '£', '', '2', 0.01080000, 1, '2015-04-05 21:53:20'),
(2, 'US Dollar', 'USD', '$', '', '2', 0.01610000, 1, '2015-04-05 21:53:20'),
(3, 'Euro', 'EUR', '', '€', '2', 0.01480000, 1, '2015-04-05 21:53:20'),
(4, 'Indian Rupee', 'INR', 'Rs.', '', '2', 1.00000000, 1, '2015-04-05 21:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer`
--

CREATE TABLE `oc_customer` (
  `customer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `school` varchar(255) NOT NULL,
  `school_id` int(11) NOT NULL,
  `dob` date NOT NULL,
  `standard` varchar(255) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `sport_type` varchar(255) NOT NULL,
  `sport_type_id` int(11) NOT NULL,
  `sport` varchar(255) NOT NULL,
  `sport_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `token` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_ban_ip`
--

CREATE TABLE `oc_customer_ban_ip` (
  `customer_ban_ip_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_field`
--

CREATE TABLE `oc_customer_field` (
  `customer_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `custom_field_value_id` int(11) NOT NULL,
  `name` int(128) NOT NULL,
  `value` text NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group`
--

CREATE TABLE `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `company_id_display` int(1) NOT NULL,
  `company_id_required` int(1) NOT NULL,
  `tax_id_display` int(1) NOT NULL,
  `tax_id_required` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group_description`
--

CREATE TABLE `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_history`
--

CREATE TABLE `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_ip`
--

CREATE TABLE `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_online`
--

CREATE TABLE `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_reward`
--

CREATE TABLE `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_transaction`
--

CREATE TABLE `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field`
--

CREATE TABLE `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL,
  `location` varchar(32) NOT NULL,
  `position` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_description`
--

CREATE TABLE `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_to_customer_group`
--

CREATE TABLE `oc_custom_field_to_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value`
--

CREATE TABLE `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value_description`
--

CREATE TABLE `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_department`
--

CREATE TABLE `oc_department` (
  `department_id` int(11) NOT NULL,
  `d_name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_department`
--

INSERT INTO `oc_department` (`department_id`, `d_name`, `code`, `status`) VALUES
(1, 'FINANCE', '', 1),
(2, 'P & M', '', 1),
(3, 'MATERIALS', '', 1),
(4, 'PMG', '', 1),
(5, 'ADMIN', 'ADMIN', 1),
(6, 'STORES', '', 1),
(7, 'PROJECTS', '', 1),
(8, 'SECRET.', '', 1),
(9, 'INFO.TECH.', '', 1),
(10, 'EHS', '', 1),
(11, 'BD&TENDER', '', 1),
(12, 'FORMWORK', '', 1),
(13, 'QMS', '', 1),
(14, 'HRD', '', 1),
(15, 'MEP', '', 1),
(16, 'GEN.MGMT', '', 1),
(17, 'CEDC', '', 1),
(18, 'CONTRACTS', '', 1),
(19, 'GSA', '', 1),
(20, 'ESTIMATION', '', 1),
(21, 'LEGAL', '', 1),
(22, 'ADMIN - FACILITY', 'ADMIN - FACILITY', 1),
(23, 'ADMIN - GENERAL', 'ADMIN - GENERAL', 1),
(24, 'SURVEY', 'SURVEY', 1),
(25, 'ADMIN - SECURITY', 'ADMIN - SECURITY', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_designation`
--

CREATE TABLE `oc_designation` (
  `designation_id` int(11) NOT NULL,
  `d_name` varchar(255) NOT NULL,
  `d_code` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `grade_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_designation`
--

INSERT INTO `oc_designation` (`designation_id`, `d_name`, `d_code`, `status`, `grade_id`, `grade_name`) VALUES
(1, 'EXECUTIVE - ACCOUNTS', '', 1, 1, 'E03'),
(2, 'DEPUTY GEN MANAGER - ACCOUNTS', '', 1, 2, 'E10'),
(3, 'SR. ASST. 3 - ACCOUNTS', '', 1, 3, 'S05'),
(4, 'SR.VICE PRESIDENT-ACCOUNTS', '', 1, 4, 'E18'),
(5, 'ROLLER OPTR. 2 - P & M', '', 1, 5, 'S02');

-- --------------------------------------------------------

--
-- Table structure for table `oc_designationparam`
--

CREATE TABLE `oc_designationparam` (
  `designationparam_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percent` decimal(10,2) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_designationparam`
--

INSERT INTO `oc_designationparam` (`designationparam_id`, `name`, `percent`, `type`, `status`) VALUES
(1, 'Basic', '0.00', 'Earning', 1),
(2, 'Dearness Allowance (D.A)', '0.00', 'Earning', 1),
(3, 'H.R.A', '0.00', 'Earning', 1),
(4, 'Transport/Conveyance Allowance', '0.00', 'Earning', 1),
(5, 'Education Allowance', '0.00', 'Earning', 1),
(6, 'Medical Allowance', '0.00', 'Earning', 1),
(7, 'Other Allowance', '0.00', 'Earning', 1),
(8, 'Provident Fund (P.F)', '0.00', 'Deduction', 1),
(9, 'Employees State Insurance (E.S.I.C)', '0.00', 'Deduction', 1),
(10, 'Professional Tax (P.T)', '0.00', 'Deduction', 1),
(11, 'Income Tax (I.T)', '0.00', 'Deduction', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_devices`
--

CREATE TABLE `oc_devices` (
  `device_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_division`
--

CREATE TABLE `oc_division` (
  `division_id` int(11) NOT NULL,
  `division` varchar(255) NOT NULL,
  `division_code` varchar(255) NOT NULL,
  `region_id` int(11) NOT NULL,
  `region_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_division`
--

INSERT INTO `oc_division` (`division_id`, `division`, `division_code`, `region_id`, `region_name`) VALUES
(1, 'SIO', '', 1, 'SIO'),
(2, 'HO', '', 2, 'HO'),
(3, 'INFRA', '', 3, 'INFRA'),
(4, 'WIO', '', 4, 'WIO'),
(5, 'I & P', '', 5, 'I & P'),
(6, 'INTRNTL', '', 6, 'INTRNTL'),
(7, 'NIO', '', 7, 'NIO'),
(8, 'JRRP', '', 8, 'JRRP');

-- --------------------------------------------------------

--
-- Table structure for table `oc_doctor`
--

CREATE TABLE `oc_doctor` (
  `doctor_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download`
--

CREATE TABLE `oc_download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(128) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `remaining` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download_description`
--

CREATE TABLE `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_early_late`
--

CREATE TABLE `oc_early_late` (
  `id` int(11) NOT NULL,
  `emp_code` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `early_mark` int(11) NOT NULL,
  `late_mark` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_early_late`
--

INSERT INTO `oc_early_late` (`id`, `emp_code`, `month`, `year`, `early_mark`, `late_mark`) VALUES
(1, 1001, 4, 2017, 0, 0),
(2, 1001, 3, 2017, 0, 0),
(3, 1000, 3, 2017, 0, 0),
(4, 1000, 4, 2017, 0, 0),
(5, 1002, 3, 2017, 0, 0),
(6, 1002, 4, 2017, 0, 0),
(7, 1003, 3, 2017, 0, 2),
(8, 1003, 4, 2017, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_employee`
--

CREATE TABLE `oc_employee` (
  `employee_id` int(11) NOT NULL,
  `emp_code` varchar(512) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `grade_id` int(11) NOT NULL DEFAULT '0',
  `grade` varchar(255) NOT NULL DEFAULT '',
  `department_id` int(11) NOT NULL DEFAULT '0',
  `department` varchar(255) NOT NULL DEFAULT '',
  `division` varchar(255) NOT NULL DEFAULT '',
  `division_id` int(11) NOT NULL DEFAULT '0',
  `region` varchar(255) NOT NULL DEFAULT '',
  `region_id` int(11) NOT NULL DEFAULT '0',
  `unit` varchar(255) NOT NULL DEFAULT '',
  `unit_id` int(11) NOT NULL DEFAULT '0',
  `designation_id` int(11) NOT NULL DEFAULT '0',
  `designation` varchar(255) NOT NULL DEFAULT '',
  `dob` date NOT NULL DEFAULT '0000-00-00',
  `doj` date NOT NULL DEFAULT '0000-00-00',
  `doc` date NOT NULL DEFAULT '0000-00-00',
  `dol` date NOT NULL DEFAULT '0000-00-00',
  `shift` int(11) NOT NULL DEFAULT '0',
  `card_number` int(11) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `gender` varchar(255) NOT NULL DEFAULT '',
  `shift_type` varchar(255) NOT NULL DEFAULT '',
  `user_group_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `password` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `salt` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `ip` varchar(255) NOT NULL DEFAULT '',
  `is_dept` int(11) NOT NULL DEFAULT '0',
  `is_super` int(11) NOT NULL DEFAULT '0',
  `is_set` int(11) NOT NULL DEFAULT '0',
  `device_id` int(11) NOT NULL DEFAULT '0',
  `employement_id` int(11) NOT NULL DEFAULT '0',
  `employement` varchar(255) NOT NULL DEFAULT '',
  `state_id` int(11) NOT NULL DEFAULT '0',
  `state` varchar(255) NOT NULL DEFAULT '',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `company` varchar(255) NOT NULL DEFAULT '',
  `group` varchar(255) NOT NULL DEFAULT '',
  `shift_id` int(11) NOT NULL DEFAULT '0',
  `sat_status` int(11) NOT NULL DEFAULT '0',
  `sun_status` int(11) NOT NULL DEFAULT '0',
  `site` varchar(255) NOT NULL DEFAULT '',
  `device` varchar(255) NOT NULL DEFAULT '',
  `is_new` int(11) NOT NULL DEFAULT '0',
  `contractor` varchar(255) NOT NULL DEFAULT '',
  `contractor_id` int(11) NOT NULL DEFAULT '0',
  `contractor_code` varchar(255) NOT NULL DEFAULT '',
  `category` varchar(255) NOT NULL DEFAULT '',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `device_code` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_employee`
--

INSERT INTO `oc_employee` (`employee_id`, `emp_code`, `title`, `name`, `grade_id`, `grade`, `department_id`, `department`, `division`, `division_id`, `region`, `region_id`, `unit`, `unit_id`, `designation_id`, `designation`, `dob`, `doj`, `doc`, `dol`, `shift`, `card_number`, `status`, `gender`, `shift_type`, `user_group_id`, `username`, `password`, `salt`, `ip`, `is_dept`, `is_super`, `is_set`, `device_id`, `employement_id`, `employement`, `state_id`, `state`, `company_id`, `company`, `group`, `shift_id`, `sat_status`, `sun_status`, `site`, `device`, `is_new`, `contractor`, `contractor_id`, `contractor_code`, `category`, `category_id`, `device_code`) VALUES
(1, '482805083058', '', 'NITESH NIKAM', 1, 'SKILLED', 1, 'PROJECTS', 'NIO', 1, 'NIO', 1, 'AIHB', 1, 1, 'CARPENTER', '0000-00-00', '2019-02-08', '2019-02-08', '0000-00-00', 0, 0, 1, 'M', 'F', 11, '503967621896', 'd6861c8dad5bc638da0568f7e9f322bd2f0b2d73', '2bca8c88b', '', 0, 0, 0, 0, 1, 'TEMPORARY', 1, 'Maharashtra', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '503967621896'),
(2, '644576398894', '', 'RAJEEV BHRTI', 1, 'SKILLED', 1, 'PROJECTS', 'NIO', 1, 'NIO', 1, 'AIHB', 1, 1, 'CARPENTER', '0000-00-00', '2019-02-08', '2019-02-08', '0000-00-00', 0, 0, 0, 'M', 'F', 11, '644576398894', '77fe91c6158effde437a644b6315091056c423c0', '5db42b7c2', '', 0, 0, 0, 0, 1, 'TEMPORARY', 1, 'Maharashtra', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '644576398894'),
(3, '841413256488', '', 'BHAGVAT ASHOK BAVSKAR', 1, 'SKILLED', 1, 'PROJECTS', 'NIO', 1, 'NIO', 1, 'AIHB', 1, 2, 'MASON', '0000-00-00', '2019-02-08', '2019-02-08', '0000-00-00', 0, 0, 0, 'M', 'F', 11, '841413256488', 'ba70e820cdebe2cbf51f798cba0992e4609f4660', 'b71836c11', '', 0, 0, 0, 0, 1, 'TEMPORARY', 1, 'Maharashtra', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '841413256488'),
(4, '235666539189', '', 'N KUMAR MEHRA', 1, 'SKILLED', 1, 'PROJECTS', 'NIO', 1, 'NIO', 1, 'AIHB', 1, 2, 'MASON', '0000-00-00', '2019-02-08', '2019-02-08', '0000-00-00', 0, 0, 1, 'M', 'F', 11, '235666539189', '0639aff4826d67bd6a772615bf15c4b0f7c8c7cb', '66824f1f6', '', 0, 0, 0, 0, 1, 'TEMPORARY', 1, 'Maharashtra', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '235666539189'),
(5, '272424159714', '', 'VINOD TATI', 2, 'UNSKILLED', 1, 'PROJECTS', 'NIO', 1, 'NIO', 1, 'AIHB', 1, 7, 'HELPER', '0000-00-00', '2019-02-08', '2019-02-08', '0000-00-00', 0, 0, 1, 'M', 'F', 11, '272424159714', '149918df564b82185c9c20a00ccb8ec903a44122', '00701c435', '', 0, 0, 0, 0, 1, 'TEMPORARY', 1, 'Maharashtra', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '272424159714'),
(6, '301397529689', '', 'GHASHIRAM', 1, 'SKILLED', 1, 'PROJECTS', 'NIO', 1, 'NIO', 1, 'AIHB', 1, 4, 'FITTER', '0000-00-00', '2019-02-08', '2019-02-08', '2019-12-31', 0, 0, 0, 'M', 'F', 11, '301397529689', '93215f7dd6da8a2090d7d88f95508eafb19172e4', '3427efd57', '', 0, 0, 0, 0, 1, 'TEMPORARY', 1, 'Maharashtra', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'JAYA DUBEY', 3, '3000010577', 'SUPPLY', 1, '301397529689'),
(7, '329972605760', '', 'TIPU KHAN', 1, 'SKILLED', 1, 'PROJECTS', 'NIO', 1, 'NIO', 1, 'AIHB', 1, 4, 'FITTER', '0000-00-00', '2019-02-08', '2019-02-08', '2019-12-31', 0, 0, 0, 'M', 'F', 11, '329972605760', 'c87a8bc05611085ff6cfc957aea5577c995423d4', '1164d997b', '', 0, 0, 0, 0, 1, 'TEMPORARY', 1, 'Maharashtra', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'JAYA DUBEY', 3, '3000010577', 'SUPPLY', 1, '329972605760'),
(8, '976927695200', '', 'RAVI KUMAR', 1, 'SKILLED', 1, 'PROJECTS', 'NIO', 1, 'NIO', 1, 'AIHB', 1, 2, 'MASON', '0000-00-00', '2019-02-08', '2019-02-08', '2019-12-31', 0, 0, 0, 'M', 'F', 11, '976927695200', '7e37c27145543620ad8b53ff74532ecc8f1fa93b', '037cd0ba1', '', 0, 0, 0, 0, 1, 'TEMPORARY', 1, 'Maharashtra', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '976927695200'),
(9, '785705412913', '', 'RAJ KUMAR NARVARIYA', 1, 'SKILLED', 1, 'PROJECTS', 'NIO', 1, 'NIO', 1, 'AIHB', 1, 5, 'PLUMBER', '0000-00-00', '2020-07-26', '2020-07-26', '0000-00-00', 0, 0, 1, 'M', 'F', 11, '785705412913', 'b1c22c7a8d8d10954632732220f16a02bf3ac4b9', '042825385', '', 0, 0, 0, 0, 1, 'TEMPORARY', 1, 'Maharashtra', 1, 'JMC PROJECTS (I) LTD', '', 1, 1, 0, '', '', 0, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '785705412913'),
(11, '547534585860', '', 'CHANDAN', 3, 'SEMISKILLED', 1, 'PROJECTS', 'NIO', 1, '', 0, 'AIHB', 1, 1, 'CARPENTER', '1985-01-27', '2019-02-18', '0000-00-00', '2019-12-31', 0, 0, 0, 'M', 'F', 0, '547534585860', '979c1a4bb40babb8a6d459761d837bf7cf66bdf3', 'acc54381a', '', 0, 0, 0, 0, 1, 'TEMPORARY', 0, '', 1, 'JMC PROJECTS (I) LTD', '', 1, 1, 0, '', '', 0, 'HARISH KUKREJA-50000000LLLLL', 4, '50000000LLLLL', 'COMPOSITE', 4, ''),
(12, '589384000000', '', 'ASHOK KUMAR', 1, 'SKILLED', 1, 'PROJECTS', 'NIO', 1, '', 0, 'THLV', 22, 6, 'FOREMAN', '0000-00-00', '2021-01-01', '0000-00-00', '2019-12-31', 0, 0, 0, 'M', 'F', 11, '589384000000', '57b190a318531ff101d012448c138599a16e662e', 'c874a1522', '', 0, 0, 0, 0, 1, 'TEMPORARY', 0, '', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'PATEL YADAV', 5, '3000029029', 'PIECE RATE', 3, ''),
(13, '266468000000', '', 'BRAHMDEV YADAV', 2, 'UNSKILLED', 1, 'PROJECTS', 'NIO', 1, '', 0, 'THLV', 22, 7, 'HELPER', '0000-00-00', '2021-01-01', '0000-00-00', '2019-12-31', 0, 0, 0, 'M', 'F', 11, '266468000000', '5af7bcbb47de785daf6ee00ce2660d1eee9ba9a1', 'a4e3b2285', '', 0, 0, 0, 0, 1, 'TEMPORARY', 0, '', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'PATEL YADAV', 5, '3000029029', 'PIECE RATE', 3, ''),
(14, '730506000000', '', 'PINTU YADAV', 1, 'SKILLED', 1, 'PROJECTS', 'NIO', 1, '', 0, 'THLV', 22, 8, 'SCAFFOLDER', '0000-00-00', '2021-01-01', '0000-00-00', '2019-12-31', 0, 0, 0, 'M', 'F', 11, '730506000000', '8d007f03a374a6a67ee4b3a40dadf29698fe36cc', 'cdcec3b7c', '', 0, 0, 0, 0, 1, 'TEMPORARY', 0, '', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'PATEL YADAV', 5, '3000029029', 'PIECE RATE', 3, ''),
(15, '287880000000', '', 'ASHOK YADAV', 1, 'SKILLED', 1, 'PROJECTS', 'NIO', 1, '', 0, 'THLV', 22, 8, 'SCAFFOLDER', '0000-00-00', '2021-01-01', '0000-00-00', '2019-12-31', 0, 0, 0, 'M', 'F', 11, '287880000000', '43ee92b7a8dac77ab822619162654a1a1354b5d7', 'acead92fa', '', 0, 0, 0, 0, 1, 'TEMPORARY', 0, '', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'PATEL YADAV', 5, '3000029029', 'PIECE RATE', 3, ''),
(16, '211307000000', '', 'MUNNILAL HAMBER', 2, 'UNSKILLED', 1, 'PROJECTS', 'NIO', 1, '', 0, 'THLV', 22, 7, 'HELPER', '0000-00-00', '2021-01-01', '0000-00-00', '2019-12-31', 0, 0, 0, 'M', 'F', 11, '211307000000', '10ffb7493b994c75f65ed37ec9ae5e46f9b8af11', '49669a7b7', '', 0, 0, 0, 0, 1, 'TEMPORARY', 0, '', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'PATEL YADAV', 5, '3000029029', 'PIECE RATE', 3, ''),
(17, '416794000000', '', 'SONA YADAV', 2, 'UNSKILLED', 1, 'PROJECTS', 'NIO', 1, '', 0, 'THLV', 22, 7, 'HELPER', '0000-00-00', '2021-01-01', '0000-00-00', '2019-12-31', 0, 0, 0, 'M', 'F', 11, '416794000000', 'a67f5fc7049ff4cc13a03d9ccc2c70925835688b', 'cbf40a0f9', '', 0, 0, 0, 0, 1, 'TEMPORARY', 0, '', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'PATEL YADAV', 5, '3000029029', 'PIECE RATE', 3, ''),
(18, '923989000000', '', 'JOGENDER KUMAR', 1, 'SKILLED', 1, 'PROJECTS', 'NIO', 1, '', 0, 'THLV', 22, 8, 'SCAFFOLDER', '0000-00-00', '2021-01-01', '0000-00-00', '2019-12-31', 0, 0, 0, 'M', 'F', 11, '923989000000', '7db1c322b958f783f7b3afbae7952a10f1ab7dd8', 'e8cb6d0d4', '', 0, 0, 0, 0, 1, 'TEMPORARY', 0, '', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'PATEL YADAV', 5, '3000029029', 'PIECE RATE', 3, ''),
(19, '238630000000', '', 'JATU YADAV', 1, 'SKILLED', 1, 'PROJECTS', 'NIO', 1, '', 0, 'THLV', 22, 8, 'SCAFFOLDER', '0000-00-00', '2021-01-01', '0000-00-00', '2019-12-31', 0, 0, 0, 'M', 'F', 11, '238630000000', 'a736e8c657b04216695c55be232bc503751bbc68', '2a08d119b', '', 0, 0, 0, 0, 1, 'TEMPORARY', 0, '', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'PATEL YADAV', 5, '3000029029', 'PIECE RATE', 3, ''),
(20, '993789800768', '', 'ARAK HUSSION', 1, 'SKILLED', 1, 'PROJECTS', 'WIO', 6, '', 0, 'KLIP', 28, 4, 'FITTER', '0000-00-00', '2019-02-19', '2019-02-19', '2019-12-31', 0, 0, 0, 'M', 'F', 11, '993789800768', '683dce1adf4cbc6deeec5ab6d5c17114deb9e9c9', '11e02966f', '', 0, 0, 0, 0, 1, 'TEMPORARY', 0, '', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'AVIRA CONSTRUCTION CO', 6, '3000026593', 'PIECE RATE', 3, ''),
(21, '977737069173', '', 'MD SHAHANSHAN', 1, 'SKILLED', 1, 'PROJECTS', 'WIO', 6, '', 0, 'KLIP', 28, 4, 'FITTER', '0000-00-00', '2019-02-19', '2019-02-19', '2019-12-31', 0, 0, 0, 'M', 'F', 11, '977737069173', '619408f564d181565510a0932989898f72ee13ec', '1e5b90537', '', 0, 0, 0, 0, 1, 'TEMPORARY', 0, '', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'AVIRA CONSTRUCTION CO', 6, '3000026593', 'PIECE RATE', 3, ''),
(22, '42344456', '', 'MOHAMMAD HADISH', 36, 'Z25', 7, 'PROJECTS', 'INFRA', 3, '', 0, 'MCFO', 53, 1, 'EXECUTIVE - ACCOUNTS', '0000-00-00', '1970-01-01', '1970-01-01', '0000-00-00', 0, 0, 1, 'M', 'F', 11, '42344456', '71e41c4a2cd7f352d23e22abdf8d3396dbd81bde', 'e246124dc', '', 0, 0, 0, 0, 5, 'TEMPORARY', 0, '', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, ''),
(23, '42344458', '', 'MD SARFARAS ALI', 36, 'Z25', 7, 'PROJECTS', 'INFRA', 3, '', 0, 'MCFO', 53, 1, 'EXECUTIVE - ACCOUNTS', '0000-00-00', '1970-01-01', '1970-01-01', '0000-00-00', 0, 0, 1, 'M', 'F', 11, '42344458', '4985dc5a064e4ace23082b06ffde24e7c5297f94', '960931da1', '', 0, 0, 0, 0, 5, 'TEMPORARY', 0, '', 1, 'JMC PROJECTS (I) LTD', '', 1, 0, 0, '', '', 0, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, ''),
(24, '423444577857', '', 'ADIR', 3, 'S05', 25, 'ADMIN - SECURITY', 'JRRP', 8, '', 0, 'OPRC', 0, 3, 'SR. ASST. 3 - ACCOUNTS', '1999-01-01', '2019-11-10', '0000-00-00', '0000-00-00', 0, 0, 1, 'M', 'F', 0, '423444577857', '2e6383945a5c7908878e98655e6c249b764e7c6a', 'b5937fa68', '', 0, 0, 0, 0, 1, 'PERMANENT', 0, '', 5, 'COSMOS-APOLLO', '', 1, 1, 0, '', '', 0, 'NITESH NIKAM', 1, '3000026485', 'PIECE RATE', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_employee_change_history`
--

CREATE TABLE `oc_employee_change_history` (
  `id` int(11) NOT NULL,
  `emp_code` int(11) NOT NULL,
  `emp_name` varchar(255) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `value_id` int(11) NOT NULL,
  `current_value_id` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `division` varchar(255) NOT NULL,
  `division_id` int(11) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `grade` varchar(255) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `employement` varchar(255) NOT NULL,
  `employement_id` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `region` varchar(255) NOT NULL,
  `region_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_employement`
--

CREATE TABLE `oc_employement` (
  `employement_id` int(11) NOT NULL,
  `employement` varchar(255) NOT NULL,
  `employement_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_employement`
--

INSERT INTO `oc_employement` (`employement_id`, `employement`, `employement_code`) VALUES
(1, 'PERMANENT', ''),
(2, 'CONTRACT', ''),
(3, 'TRAINEE', ''),
(4, 'LOCAL', 'LC'),
(5, 'TEMPORARY', 'TEMPORARY');

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension`
--

CREATE TABLE `oc_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(23, 'payment', 'cod'),
(22, 'total', 'shipping'),
(57, 'total', 'sub_total'),
(58, 'total', 'tax'),
(59, 'total', 'total'),
(410, 'module', 'banner'),
(426, 'module', 'carousel'),
(390, 'total', 'credit'),
(387, 'shipping', 'flat'),
(349, 'total', 'handling'),
(350, 'total', 'low_order_fee'),
(389, 'total', 'coupon'),
(413, 'module', 'category'),
(411, 'module', 'affiliate'),
(408, 'module', 'account'),
(393, 'total', 'reward'),
(398, 'total', 'voucher'),
(407, 'payment', 'free_checkout'),
(427, 'module', 'featured'),
(419, 'module', 'slideshow');

-- --------------------------------------------------------

--
-- Table structure for table `oc_facereading_data`
--

CREATE TABLE `oc_facereading_data` (
  `date` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `emp_id` varchar(255) NOT NULL,
  `mach_no` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter`
--

CREATE TABLE `oc_filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_description`
--

CREATE TABLE `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group`
--

CREATE TABLE `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group_description`
--

CREATE TABLE `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_geo_zone`
--

CREATE TABLE `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`geo_zone_id`, `name`, `description`, `date_modified`, `date_added`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2010-02-26 22:33:24', '2009-01-06 23:26:25'),
(4, 'UK Shipping', 'UK Shipping Zones', '2010-12-15 15:18:13', '2009-06-23 01:14:53');

-- --------------------------------------------------------

--
-- Table structure for table `oc_grade`
--

CREATE TABLE `oc_grade` (
  `grade_id` int(11) NOT NULL,
  `g_name` varchar(255) NOT NULL,
  `g_code` varchar(255) NOT NULL,
  `earning_total` decimal(10,2) NOT NULL,
  `deduction_total` decimal(10,2) NOT NULL,
  `net_total` decimal(10,2) NOT NULL,
  `no_days` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_grade`
--

INSERT INTO `oc_grade` (`grade_id`, `g_name`, `g_code`, `earning_total`, `deduction_total`, `net_total`, `no_days`, `status`) VALUES
(1, 'E03', 'E03', '0.00', '0.00', '0.00', 0, 0),
(2, 'E10', 'E10', '0.00', '0.00', '0.00', 0, 0),
(3, 'S05', 'S05', '0.00', '0.00', '0.00', 0, 0),
(4, 'E18', 'E18', '0.00', '0.00', '0.00', 0, 0),
(5, 'S02', 'S02', '0.00', '0.00', '0.00', 0, 0),
(6, 'E06', 'E06', '0.00', '0.00', '0.00', 0, 0),
(7, 'E08', 'E08', '0.00', '0.00', '0.00', 0, 0),
(8, 'S03', 'S03', '0.00', '0.00', '0.00', 0, 0),
(9, 'E01', 'E01', '0.00', '0.00', '0.00', 0, 0),
(10, 'S06', 'S06', '0.00', '0.00', '0.00', 0, 0),
(11, 'E02', 'E02', '0.00', '0.00', '0.00', 0, 0),
(12, 'E09', 'E09', '0.00', '0.00', '0.00', 0, 0),
(13, 'E07', 'E07', '0.00', '0.00', '0.00', 0, 0),
(14, 'E04', 'E04', '0.00', '0.00', '0.00', 0, 0),
(15, 'E15', 'E15', '0.00', '0.00', '0.00', 0, 0),
(16, 'E05', 'E05', '0.00', '0.00', '0.00', 0, 0),
(17, 'E13', 'E13', '0.00', '0.00', '0.00', 0, 0),
(18, 'S01', 'S01', '0.00', '0.00', '0.00', 0, 0),
(19, 'S04', 'S04', '0.00', '0.00', '0.00', 0, 0),
(20, 'E17', 'E17', '0.00', '0.00', '0.00', 0, 0),
(21, 'C01', 'C01', '0.00', '0.00', '0.00', 0, 0),
(22, 'E14', 'E14', '0.00', '0.00', '0.00', 0, 0),
(23, 'E19', 'E19', '0.00', '0.00', '0.00', 0, 0),
(24, 'C02', 'C02', '0.00', '0.00', '0.00', 0, 0),
(25, 'C04', 'C04', '0.00', '0.00', '0.00', 0, 0),
(26, 'C03', 'C03', '0.00', '0.00', '0.00', 0, 0),
(27, 'E11', 'E11', '0.00', '0.00', '0.00', 0, 0),
(28, 'C05', 'C05', '0.00', '0.00', '0.00', 0, 0),
(29, 'E99', 'E99', '0.00', '0.00', '0.00', 0, 0),
(30, 'C06', 'C06', '0.00', '0.00', '0.00', 0, 0),
(31, 'E20', 'E20', '0.00', '0.00', '0.00', 0, 0),
(32, 'C00', 'C00', '0.00', '0.00', '0.00', 0, 0),
(33, 'A00', 'A00', '0.00', '0.00', '0.00', 0, 0),
(34, 'E00', 'E00', '0.00', '0.00', '0.00', 0, 0),
(35, 'B00', 'B00', '0.00', '0.00', '0.00', 0, 0),
(36, 'Z25', 'Z25', '0.00', '0.00', '0.00', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_gradeparam`
--

CREATE TABLE `oc_gradeparam` (
  `id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `param_name` varchar(255) NOT NULL,
  `param_value` decimal(10,2) NOT NULL,
  `param_percent` decimal(10,2) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_gradeparam`
--

INSERT INTO `oc_gradeparam` (`id`, `grade_id`, `param_name`, `param_value`, `param_percent`, `type`) VALUES
(12, 1, 'Basic', '20000.00', '0.00', 'Earning'),
(13, 1, 'Provident Fund (P.F)', '700.00', '0.00', 'Deduction'),
(14, 1, 'Dearness Allowance (D.A)', '100.00', '0.00', 'Earning'),
(15, 1, 'Employees State Insurance (E.S.I.C)', '800.00', '0.00', 'Deduction'),
(16, 1, 'H.R.A', '200.00', '0.00', 'Earning'),
(17, 1, 'Professional Tax (P.T)', '900.00', '0.00', 'Deduction'),
(18, 1, 'Transport/Conveyance Allowance', '300.00', '0.00', 'Earning'),
(19, 1, 'Income Tax (I.T)', '1000.00', '0.00', 'Deduction'),
(20, 1, 'Education Allowance', '400.00', '0.00', 'Earning'),
(21, 1, 'Medical Allowance', '500.00', '0.00', 'Earning'),
(22, 1, 'Other Allowance', '600.00', '0.00', 'Earning');

-- --------------------------------------------------------

--
-- Table structure for table `oc_halfday`
--

CREATE TABLE `oc_halfday` (
  `halfday_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `location` text NOT NULL,
  `department_mumbai` text NOT NULL,
  `department_pune` text NOT NULL,
  `department_moving` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_holiday`
--

CREATE TABLE `oc_holiday` (
  `holiday_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `division_datas` text NOT NULL,
  `department_mumbai` text NOT NULL,
  `loc` text NOT NULL,
  `department_pune` text NOT NULL,
  `department_moving` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_holiday`
--

INSERT INTO `oc_holiday` (`holiday_id`, `name`, `date`, `division_datas`, `department_mumbai`, `loc`, `department_pune`, `department_moving`) VALUES
(1, 'Dussera', '2018-10-18', 'a:1:{i:2;s:2:\"HO\";}', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_holiday_loc`
--

CREATE TABLE `oc_holiday_loc` (
  `id` int(11) NOT NULL,
  `holiday_id` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `location_id` int(11) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_holiday_loc`
--

INSERT INTO `oc_holiday_loc` (`id`, `holiday_id`, `location`, `location_id`, `value`) VALUES
(1, 1, 'JMCH - AHD', 2, 'a:25:{i:5;s:5:\"ADMIN\";i:22;s:16:\"ADMIN - FACILITY\";i:23;s:15:\"ADMIN - GENERAL\";i:25;s:16:\"ADMIN - SECURITY\";i:11;s:13:\"BD&amp;TENDER\";i:17;s:4:\"CEDC\";i:18;s:9:\"CONTRACTS\";i:10;s:3:\"EHS\";i:20;s:10:\"ESTIMATION\";i:1;s:7:\"FINANCE\";i:12;s:8:\"FORMWORK\";i:16;s:8:\"GEN.MGMT\";i:19;s:3:\"GSA\";i:14;s:3:\"HRD\";i:9;s:10:\"INFO.TECH.\";i:21;s:5:\"LEGAL\";i:3;s:9:\"MATERIALS\";i:15;s:3:\"MEP\";i:2;s:9:\"P &amp; M\";i:4;s:3:\"PMG\";i:7;s:8:\"PROJECTS\";i:13;s:3:\"QMS\";i:8;s:7:\"SECRET.\";i:6;s:6:\"STORES\";i:24;s:6:\"SURVEY\";}'),
(2, 1, 'JMCH - MUM', 6, 'a:25:{i:5;s:5:\"ADMIN\";i:22;s:16:\"ADMIN - FACILITY\";i:23;s:15:\"ADMIN - GENERAL\";i:25;s:16:\"ADMIN - SECURITY\";i:11;s:13:\"BD&amp;TENDER\";i:17;s:4:\"CEDC\";i:18;s:9:\"CONTRACTS\";i:10;s:3:\"EHS\";i:20;s:10:\"ESTIMATION\";i:1;s:7:\"FINANCE\";i:12;s:8:\"FORMWORK\";i:16;s:8:\"GEN.MGMT\";i:19;s:3:\"GSA\";i:14;s:3:\"HRD\";i:9;s:10:\"INFO.TECH.\";i:21;s:5:\"LEGAL\";i:3;s:9:\"MATERIALS\";i:15;s:3:\"MEP\";i:2;s:9:\"P &amp; M\";i:4;s:3:\"PMG\";i:7;s:8:\"PROJECTS\";i:13;s:3:\"QMS\";i:8;s:7:\"SECRET.\";i:6;s:6:\"STORES\";i:24;s:6:\"SURVEY\";}'),
(3, 1, 'CHAR', 28, 'a:25:{i:5;s:5:\"ADMIN\";i:22;s:16:\"ADMIN - FACILITY\";i:23;s:15:\"ADMIN - GENERAL\";i:25;s:16:\"ADMIN - SECURITY\";i:11;s:13:\"BD&amp;TENDER\";i:17;s:4:\"CEDC\";i:18;s:9:\"CONTRACTS\";i:10;s:3:\"EHS\";i:20;s:10:\"ESTIMATION\";i:1;s:7:\"FINANCE\";i:12;s:8:\"FORMWORK\";i:16;s:8:\"GEN.MGMT\";i:19;s:3:\"GSA\";i:14;s:3:\"HRD\";i:9;s:10:\"INFO.TECH.\";i:21;s:5:\"LEGAL\";i:3;s:9:\"MATERIALS\";i:15;s:3:\"MEP\";i:2;s:9:\"P &amp; M\";i:4;s:3:\"PMG\";i:7;s:8:\"PROJECTS\";i:13;s:3:\"QMS\";i:8;s:7:\"SECRET.\";i:6;s:6:\"STORES\";i:24;s:6:\"SURVEY\";}'),
(4, 1, 'SAPP', 7, 'a:25:{i:5;s:5:\"ADMIN\";i:22;s:16:\"ADMIN - FACILITY\";i:23;s:15:\"ADMIN - GENERAL\";i:25;s:16:\"ADMIN - SECURITY\";i:11;s:13:\"BD&amp;TENDER\";i:17;s:4:\"CEDC\";i:18;s:9:\"CONTRACTS\";i:10;s:3:\"EHS\";i:20;s:10:\"ESTIMATION\";i:1;s:7:\"FINANCE\";i:12;s:8:\"FORMWORK\";i:16;s:8:\"GEN.MGMT\";i:19;s:3:\"GSA\";i:14;s:3:\"HRD\";i:9;s:10:\"INFO.TECH.\";i:21;s:5:\"LEGAL\";i:3;s:9:\"MATERIALS\";i:15;s:3:\"MEP\";i:2;s:9:\"P &amp; M\";i:4;s:3:\"PMG\";i:7;s:8:\"PROJECTS\";i:13;s:3:\"QMS\";i:8;s:7:\"SECRET.\";i:6;s:6:\"STORES\";i:24;s:6:\"SURVEY\";}');

-- --------------------------------------------------------

--
-- Table structure for table `oc_horse_owner`
--

CREATE TABLE `oc_horse_owner` (
  `horse_id` int(11) NOT NULL,
  `owner` int(11) NOT NULL,
  `share` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_information`
--

CREATE TABLE `oc_information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information`
--

INSERT INTO `oc_information` (`information_id`, `bottom`, `sort_order`, `status`) VALUES
(3, 1, 3, 1),
(4, 1, 1, 1),
(5, 1, 4, 1),
(6, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_description`
--

CREATE TABLE `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_description`
--

INSERT INTO `oc_information_description` (`information_id`, `language_id`, `title`, `description`) VALUES
(4, 1, 'About Us', '&lt;p&gt;\r\n	About Us&lt;/p&gt;\r\n'),
(5, 1, 'Terms &amp; Conditions', '&lt;p&gt;\r\n	Terms &amp;amp; Conditions&lt;/p&gt;\r\n'),
(3, 1, 'Privacy Policy', '&lt;p&gt;\r\n	Privacy Policy&lt;/p&gt;\r\n'),
(6, 1, 'Delivery Information', '&lt;p&gt;\r\n	Delivery Information&lt;/p&gt;\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_layout`
--

CREATE TABLE `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_store`
--

CREATE TABLE `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_to_store`
--

INSERT INTO `oc_information_to_store` (`information_id`, `store_id`) VALUES
(3, 0),
(4, 0),
(5, 0),
(6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_language`
--

CREATE TABLE `oc_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `filename` varchar(64) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `filename`, `sort_order`, `status`) VALUES
(1, 'English', 'en', 'en_US.UTF-8,en_US,en-gb,english', 'gb.png', 'english', 'english', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout`
--

CREATE TABLE `oc_layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_route`
--

CREATE TABLE `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_leave`
--

CREATE TABLE `oc_leave` (
  `leave_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(255) NOT NULL,
  `emp_doj` date NOT NULL,
  `emp_grade` varchar(255) NOT NULL,
  `pl_acc` varchar(255) NOT NULL,
  `cl_acc` varchar(255) NOT NULL,
  `sl_acc` varchar(255) NOT NULL,
  `cof_acc` varchar(255) NOT NULL,
  `pl_bal` varchar(255) NOT NULL,
  `cl_bal` varchar(255) NOT NULL,
  `sl_bal` varchar(255) NOT NULL,
  `cof_bal` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `close_status` int(1) NOT NULL,
  `emp_designation` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_leave`
--

INSERT INTO `oc_leave` (`leave_id`, `emp_id`, `emp_name`, `emp_doj`, `emp_grade`, `pl_acc`, `cl_acc`, `sl_acc`, `cof_acc`, `pl_bal`, `cl_bal`, `sl_bal`, `cof_bal`, `year`, `close_status`, `emp_designation`) VALUES
(1, 8437, 'Aaron Fargose', '2018-09-01', '', '', '', '', '', '', '', '', '', '2018', 0, ''),
(2, 8438, 'Eric Frgose', '2019-02-01', '', '', '', '', '', '', '', '', '', '2018', 0, ''),
(3, 8439, 'Mukesh Yadav', '2020-03-18', '', '', '', '', '', '', '', '', '', '2018', 0, ''),
(4, 1000, 'Eric', '2020-02-01', '', '', '', '', '', '', '', '', '', '2018', 0, ''),
(5, 1001, 'Aaron', '2020-02-01', '', '', '', '', '', '', '', '', '', '2018', 0, ''),
(6, 1002, 'Mukesh Yadav', '2020-02-01', '', '', '', '', '', '', '', '', '', '2018', 0, ''),
(7, 1003, 'Manish Kanojiya', '2020-02-01', '', '', '', '', '', '', '', '', '', '2018', 0, ''),
(8, 2147483647, 'ADIR', '2019-11-10', '', '', '', '', '', '', '', '', '', '2018', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_leavemaster`
--

CREATE TABLE `oc_leavemaster` (
  `leavemaster_id` int(11) NOT NULL,
  `leavemaster` varchar(255) NOT NULL,
  `leavemaster_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_leavemaster`
--

INSERT INTO `oc_leavemaster` (`leavemaster_id`, `leavemaster`, `leavemaster_code`) VALUES
(1, 'PL', 'PL'),
(2, 'CL', 'CL'),
(3, 'OD', 'OD');

-- --------------------------------------------------------

--
-- Table structure for table `oc_leave_credit_transaction`
--

CREATE TABLE `oc_leave_credit_transaction` (
  `id` int(11) NOT NULL,
  `emp_code` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `leave_value` float NOT NULL,
  `leave_name` varchar(255) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `dot` date NOT NULL,
  `department` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `region` varchar(255) NOT NULL,
  `region_id` int(11) NOT NULL,
  `division` varchar(255) NOT NULL,
  `division_id` int(11) NOT NULL,
  `present_count` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_leave_employee`
--

CREATE TABLE `oc_leave_employee` (
  `id` int(11) NOT NULL,
  `leave_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `leavename` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_leave_transaction`
--

CREATE TABLE `oc_leave_transaction` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `leave_type` varchar(11) NOT NULL,
  `date` date NOT NULL,
  `leave_amount` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `days` varchar(255) NOT NULL,
  `encash` varchar(255) NOT NULL,
  `p_status` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `a_status` int(11) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `year` varchar(11) NOT NULL,
  `dot` date NOT NULL,
  `date_cof` date NOT NULL,
  `linked_batch_id` int(11) NOT NULL,
  `leave_reason` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `region_id` int(11) NOT NULL,
  `division` varchar(255) NOT NULL,
  `division_id` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `applied_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_leave_transaction_temp`
--

CREATE TABLE `oc_leave_transaction_temp` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `leave_type` varchar(11) NOT NULL,
  `date` date NOT NULL,
  `leave_amount` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `days` varchar(255) NOT NULL,
  `encash` varchar(255) NOT NULL,
  `p_status` int(11) NOT NULL,
  `approval_1` int(11) NOT NULL,
  `approved_1_by` int(11) NOT NULL,
  `approval_2` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `a_status` int(11) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `year` varchar(11) NOT NULL,
  `dot` date NOT NULL,
  `date_cof` date NOT NULL,
  `leave_reason` varchar(255) NOT NULL,
  `dept_name` varchar(255) NOT NULL,
  `approval_date_1` datetime NOT NULL,
  `approval_date_2` datetime NOT NULL,
  `reject_date` date NOT NULL,
  `reject_reason` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class`
--

CREATE TABLE `oc_length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class_description`
--

CREATE TABLE `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Centimeter', 'cm'),
(2, 1, 'Millimeter', 'mm'),
(3, 1, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Table structure for table `oc_location`
--

CREATE TABLE `oc_location` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_location`
--

INSERT INTO `oc_location` (`id`, `code`, `name`) VALUES
(1, 'test_location', 'test_location');

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer`
--

CREATE TABLE `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer_to_store`
--

CREATE TABLE `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_manufacturer_to_store`
--

INSERT INTO `oc_manufacturer_to_store` (`manufacturer_id`, `store_id`) VALUES
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_medicine`
--

CREATE TABLE `oc_medicine` (
  `medicine_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `rate` float(10,2) NOT NULL,
  `service` float(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `doctor` int(11) NOT NULL,
  `travel_sheet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_option`
--

CREATE TABLE `oc_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_description`
--

CREATE TABLE `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value`
--

CREATE TABLE `oc_option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value_description`
--

CREATE TABLE `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order`
--

CREATE TABLE `oc_order` (
  `order_id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(32) NOT NULL,
  `payment_company_id` varchar(32) NOT NULL,
  `payment_tax_id` varchar(32) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(32) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_download`
--

CREATE TABLE `oc_order_download` (
  `order_download_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `filename` varchar(128) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `remaining` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_field`
--

CREATE TABLE `oc_order_field` (
  `order_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `custom_field_value_id` int(11) NOT NULL,
  `name` int(128) NOT NULL,
  `value` text NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_fraud`
--

CREATE TABLE `oc_order_fraud` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `country_match` varchar(3) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `high_risk_country` varchar(3) NOT NULL,
  `distance` int(11) NOT NULL,
  `ip_region` varchar(255) NOT NULL,
  `ip_city` varchar(255) NOT NULL,
  `ip_latitude` decimal(10,6) NOT NULL,
  `ip_longitude` decimal(10,6) NOT NULL,
  `ip_isp` varchar(255) NOT NULL,
  `ip_org` varchar(255) NOT NULL,
  `ip_asnum` int(11) NOT NULL,
  `ip_user_type` varchar(255) NOT NULL,
  `ip_country_confidence` varchar(3) NOT NULL,
  `ip_region_confidence` varchar(3) NOT NULL,
  `ip_city_confidence` varchar(3) NOT NULL,
  `ip_postal_confidence` varchar(3) NOT NULL,
  `ip_postal_code` varchar(10) NOT NULL,
  `ip_accuracy_radius` int(11) NOT NULL,
  `ip_net_speed_cell` varchar(255) NOT NULL,
  `ip_metro_code` int(3) NOT NULL,
  `ip_area_code` int(3) NOT NULL,
  `ip_time_zone` varchar(255) NOT NULL,
  `ip_region_name` varchar(255) NOT NULL,
  `ip_domain` varchar(255) NOT NULL,
  `ip_country_name` varchar(255) NOT NULL,
  `ip_continent_code` varchar(2) NOT NULL,
  `ip_corporate_proxy` varchar(3) NOT NULL,
  `anonymous_proxy` varchar(3) NOT NULL,
  `proxy_score` int(3) NOT NULL,
  `is_trans_proxy` varchar(3) NOT NULL,
  `free_mail` varchar(3) NOT NULL,
  `carder_email` varchar(3) NOT NULL,
  `high_risk_username` varchar(3) NOT NULL,
  `high_risk_password` varchar(3) NOT NULL,
  `bin_match` varchar(10) NOT NULL,
  `bin_country` varchar(2) NOT NULL,
  `bin_name_match` varchar(3) NOT NULL,
  `bin_name` varchar(255) NOT NULL,
  `bin_phone_match` varchar(3) NOT NULL,
  `bin_phone` varchar(32) NOT NULL,
  `customer_phone_in_billing_location` varchar(8) NOT NULL,
  `ship_forward` varchar(3) NOT NULL,
  `city_postal_match` varchar(3) NOT NULL,
  `ship_city_postal_match` varchar(3) NOT NULL,
  `score` decimal(10,5) NOT NULL,
  `explanation` text NOT NULL,
  `risk_score` decimal(10,5) NOT NULL,
  `queries_remaining` int(11) NOT NULL,
  `maxmind_id` varchar(8) NOT NULL,
  `error` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_history`
--

CREATE TABLE `oc_order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(5) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_option`
--

CREATE TABLE `oc_order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_product`
--

CREATE TABLE `oc_order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring`
--

CREATE TABLE `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `profile_name` varchar(255) NOT NULL,
  `profile_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `profile_reference` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring_transaction`
--

CREATE TABLE `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_status`
--

CREATE TABLE `oc_order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(2, 1, 'Processing'),
(3, 1, 'Shipped'),
(7, 1, 'Canceled'),
(5, 1, 'Complete'),
(8, 1, 'Denied'),
(9, 1, 'Canceled Reversal'),
(10, 1, 'Failed'),
(11, 1, 'Refunded'),
(12, 1, 'Reversed'),
(13, 1, 'Chargeback'),
(1, 1, 'Pending'),
(16, 1, 'Voided'),
(15, 1, 'Processed'),
(14, 1, 'Expired');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_total`
--

CREATE TABLE `oc_order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_voucher`
--

CREATE TABLE `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_owner`
--

CREATE TABLE `oc_owner` (
  `owner_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `transaction_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_payroll_transaction`
--

CREATE TABLE `oc_payroll_transaction` (
  `t_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `designation_name` varchar(255) NOT NULL,
  `basic` decimal(10,2) NOT NULL,
  `da` decimal(10,2) NOT NULL,
  `hra` decimal(10,2) NOT NULL,
  `ta` decimal(10,2) NOT NULL,
  `ea` decimal(10,2) NOT NULL,
  `earning_total` decimal(10,2) NOT NULL,
  `pf` decimal(10,2) NOT NULL,
  `esi` decimal(10,2) NOT NULL,
  `pt` decimal(10,2) NOT NULL,
  `income_tax` decimal(10,2) NOT NULL,
  `deduction_total` decimal(10,2) NOT NULL,
  `net_total` decimal(10,2) NOT NULL,
  `no_days` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product`
--

CREATE TABLE `oc_product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(13) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL,
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int(5) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_attribute`
--

CREATE TABLE `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_description`
--

CREATE TABLE `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `tag` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_discount`
--

CREATE TABLE `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_filter`
--

CREATE TABLE `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_image`
--

CREATE TABLE `oc_product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option`
--

CREATE TABLE `oc_product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option_value`
--

CREATE TABLE `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_profile`
--

CREATE TABLE `oc_product_profile` (
  `product_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_recurring`
--

CREATE TABLE `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_related`
--

CREATE TABLE `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_reward`
--

CREATE TABLE `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_special`
--

CREATE TABLE `oc_product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_category`
--

CREATE TABLE `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_download`
--

CREATE TABLE `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_layout`
--

CREATE TABLE `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_store`
--

CREATE TABLE `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_profile`
--

CREATE TABLE `oc_profile` (
  `profile_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_profile_description`
--

CREATE TABLE `oc_profile_description` (
  `profile_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_region`
--

CREATE TABLE `oc_region` (
  `region_id` int(11) NOT NULL,
  `region` varchar(255) NOT NULL,
  `region_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_region`
--

INSERT INTO `oc_region` (`region_id`, `region`, `region_code`) VALUES
(1, 'SIO', ''),
(2, 'HO', ''),
(3, 'INFRA', ''),
(4, 'WIO', ''),
(5, 'I & P', ''),
(6, 'INTRNTL', ''),
(7, 'NIO', ''),
(8, 'JRRP', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_requestform`
--

CREATE TABLE `oc_requestform` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `dot` date NOT NULL,
  `doi` date NOT NULL,
  `batch_id` int(11) NOT NULL,
  `request_type` int(11) NOT NULL,
  `description` text NOT NULL,
  `approval_1` int(11) NOT NULL,
  `dept_name` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return`
--

CREATE TABLE `oc_return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_action`
--

CREATE TABLE `oc_return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_history`
--

CREATE TABLE `oc_return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_reason`
--

CREATE TABLE `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_status`
--

CREATE TABLE `oc_return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_review`
--

CREATE TABLE `oc_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_setting`
--

CREATE TABLE `oc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `group` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `group`, `key`, `value`, `serialized`) VALUES
(1, 0, 'shipping', 'shipping_sort_order', '3', 0),
(2, 0, 'sub_total', 'sub_total_sort_order', '1', 0),
(3, 0, 'sub_total', 'sub_total_status', '1', 0),
(4, 0, 'tax', 'tax_status', '1', 0),
(5, 0, 'total', 'total_sort_order', '9', 0),
(6, 0, 'total', 'total_status', '1', 0),
(7, 0, 'tax', 'tax_sort_order', '5', 0),
(8, 0, 'free_checkout', 'free_checkout_sort_order', '1', 0),
(9, 0, 'cod', 'cod_sort_order', '5', 0),
(10, 0, 'cod', 'cod_total', '0.01', 0),
(11, 0, 'cod', 'cod_order_status_id', '1', 0),
(12, 0, 'cod', 'cod_geo_zone_id', '0', 0),
(13, 0, 'cod', 'cod_status', '1', 0),
(14, 0, 'shipping', 'shipping_status', '1', 0),
(15, 0, 'shipping', 'shipping_estimator', '1', 0),
(27, 0, 'coupon', 'coupon_sort_order', '4', 0),
(28, 0, 'coupon', 'coupon_status', '1', 0),
(34, 0, 'flat', 'flat_sort_order', '1', 0),
(35, 0, 'flat', 'flat_status', '1', 0),
(36, 0, 'flat', 'flat_geo_zone_id', '0', 0),
(37, 0, 'flat', 'flat_tax_class_id', '9', 0),
(38, 0, 'carousel', 'carousel_module', 'a:1:{i:0;a:10:{s:9:\"banner_id\";s:1:\"8\";s:5:\"limit\";s:1:\"5\";s:6:\"scroll\";s:1:\"3\";s:5:\"width\";s:2:\"80\";s:6:\"height\";s:2:\"80\";s:11:\"resize_type\";s:7:\"default\";s:9:\"layout_id\";s:1:\"1\";s:8:\"position\";s:14:\"content_bottom\";s:6:\"status\";s:1:\"1\";s:10:\"sort_order\";s:2:\"-1\";}}', 1),
(39, 0, 'featured', 'featured_product', '43,40,42,49,46,47,28', 0),
(40, 0, 'featured', 'featured_module', 'a:1:{i:0;a:8:{s:5:\"limit\";s:1:\"6\";s:11:\"image_width\";s:2:\"80\";s:12:\"image_height\";s:2:\"80\";s:11:\"resize_type\";s:7:\"default\";s:9:\"layout_id\";s:1:\"1\";s:8:\"position\";s:11:\"content_top\";s:6:\"status\";s:1:\"1\";s:10:\"sort_order\";s:1:\"2\";}}', 1),
(41, 0, 'flat', 'flat_cost', '5.00', 0),
(42, 0, 'credit', 'credit_sort_order', '7', 0),
(43, 0, 'credit', 'credit_status', '1', 0),
(53, 0, 'reward', 'reward_sort_order', '2', 0),
(54, 0, 'reward', 'reward_status', '1', 0),
(56, 0, 'affiliate', 'affiliate_module', 'a:1:{i:0;a:4:{s:9:\"layout_id\";s:2:\"10\";s:8:\"position\";s:12:\"column_right\";s:6:\"status\";s:1:\"1\";s:10:\"sort_order\";s:1:\"1\";}}', 1),
(57, 0, 'category', 'category_module', 'a:2:{i:0;a:5:{s:9:\"layout_id\";s:1:\"3\";s:8:\"position\";s:11:\"column_left\";s:5:\"count\";s:1:\"0\";s:6:\"status\";s:1:\"1\";s:10:\"sort_order\";s:1:\"1\";}i:1;a:5:{s:9:\"layout_id\";s:1:\"2\";s:8:\"position\";s:11:\"column_left\";s:5:\"count\";s:1:\"0\";s:6:\"status\";s:1:\"1\";s:10:\"sort_order\";s:1:\"1\";}}', 1),
(60, 0, 'account', 'account_module', 'a:1:{i:0;a:4:{s:9:\"layout_id\";s:1:\"6\";s:8:\"position\";s:12:\"column_right\";s:6:\"status\";s:1:\"1\";s:10:\"sort_order\";s:1:\"1\";}}', 1),
(94, 0, 'voucher', 'voucher_sort_order', '8', 0),
(95, 0, 'voucher', 'voucher_status', '1', 0),
(103, 0, 'free_checkout', 'free_checkout_status', '1', 0),
(104, 0, 'free_checkout', 'free_checkout_order_status_id', '1', 0),
(108, 0, 'slideshow', 'slideshow_module', 'a:1:{i:0;a:8:{s:9:\"banner_id\";s:1:\"7\";s:5:\"width\";s:3:\"980\";s:6:\"height\";s:3:\"280\";s:11:\"resize_type\";s:7:\"default\";s:9:\"layout_id\";s:1:\"1\";s:8:\"position\";s:11:\"content_top\";s:6:\"status\";s:1:\"1\";s:10:\"sort_order\";s:1:\"1\";}}', 1),
(109, 0, 'banner', 'banner_module', 'a:1:{i:0;a:8:{s:9:\"banner_id\";s:1:\"6\";s:5:\"width\";s:3:\"182\";s:6:\"height\";s:3:\"182\";s:11:\"resize_type\";s:7:\"default\";s:9:\"layout_id\";s:1:\"3\";s:8:\"position\";s:11:\"column_left\";s:6:\"status\";s:1:\"1\";s:10:\"sort_order\";s:1:\"3\";}}', 1),
(399, 0, 'config', 'config_image_additional_width', '74', 0),
(400, 0, 'config', 'config_image_additional_height', '74', 0),
(401, 0, 'config', 'config_image_related_width', '80', 0),
(402, 0, 'config', 'config_image_related_height', '80', 0),
(403, 0, 'config', 'config_image_compare_width', '90', 0),
(404, 0, 'config', 'config_image_compare_height', '90', 0),
(405, 0, 'config', 'config_image_wishlist_width', '47', 0),
(406, 0, 'config', 'config_image_wishlist_height', '47', 0),
(407, 0, 'config', 'config_image_cart_width', '47', 0),
(408, 0, 'config', 'config_image_cart_height', '47', 0),
(409, 0, 'config', 'config_ftp_host', '64.79.95.89', 0),
(410, 0, 'config', 'config_ftp_port', '21', 0),
(411, 0, 'config', 'config_ftp_username', '', 0),
(412, 0, 'config', 'config_ftp_password', '', 0),
(413, 0, 'config', 'config_ftp_root', '', 0),
(414, 0, 'config', 'config_ftp_status', '0', 0),
(415, 0, 'config', 'config_mail_protocol', 'mail', 0),
(416, 0, 'config', 'config_mail_parameter', '', 0),
(417, 0, 'config', 'config_smtp_host', '', 0),
(418, 0, 'config', 'config_smtp_username', '', 0),
(419, 0, 'config', 'config_smtp_password', '', 0),
(420, 0, 'config', 'config_smtp_port', '25', 0),
(421, 0, 'config', 'config_smtp_timeout', '5', 0),
(422, 0, 'config', 'config_alert_mail', '0', 0),
(423, 0, 'config', 'config_account_mail', '0', 0),
(424, 0, 'config', 'config_alert_emails', '', 0),
(425, 0, 'config', 'config_fraud_detection', '0', 0),
(426, 0, 'config', 'config_fraud_key', '', 0),
(427, 0, 'config', 'config_fraud_score', '', 0),
(428, 0, 'config', 'config_fraud_status_id', '7', 0),
(429, 0, 'config', 'config_secure', '0', 0),
(430, 0, 'config', 'config_shared', '0', 0),
(431, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(398, 0, 'config', 'config_image_product_height', '80', 0),
(397, 0, 'config', 'config_image_product_width', '80', 0),
(396, 0, 'config', 'config_image_popup_height', '500', 0),
(395, 0, 'config', 'config_image_popup_width', '500', 0),
(394, 0, 'config', 'config_image_thumb_height', '228', 0),
(393, 0, 'config', 'config_image_thumb_width', '228', 0),
(392, 0, 'config', 'config_image_category_height', '80', 0),
(391, 0, 'config', 'config_image_category_width', '80', 0),
(390, 0, 'config', 'config_icon', 'data/cart.png', 0),
(389, 0, 'config', 'config_logo', 'data/logo.png', 0),
(388, 0, 'config', 'config_return_status_id', '2', 0),
(387, 0, 'config', 'config_return_id', '0', 0),
(386, 0, 'config', 'config_commission', '5', 0),
(385, 0, 'config', 'config_affiliate_id', '4', 0),
(384, 0, 'config', 'config_stock_status_id', '5', 0),
(383, 0, 'config', 'config_stock_checkout', '0', 0),
(382, 0, 'config', 'config_stock_warning', '0', 0),
(381, 0, 'config', 'config_stock_display', '0', 0),
(380, 0, 'config', 'config_complete_status_id', '5', 0),
(379, 0, 'config', 'config_order_status_id', '1', 0),
(378, 0, 'config', 'config_invoice_prefix', 'INV-2013-00', 0),
(377, 0, 'config', 'config_order_edit', '100', 0),
(376, 0, 'config', 'config_checkout_id', '5', 0),
(375, 0, 'config', 'config_guest_checkout', '1', 0),
(374, 0, 'config', 'config_cart_weight', '1', 0),
(373, 0, 'config', 'config_account_id', '3', 0),
(372, 0, 'config', 'config_customer_price', '0', 0),
(371, 0, 'config', 'config_customer_group_display', 'a:1:{i:0;s:1:\"1\";}', 1),
(370, 0, 'config', 'config_customer_group_id', '1', 0),
(369, 0, 'config', 'config_customer_online', '0', 0),
(368, 0, 'config', 'config_tax_customer', 'shipping', 0),
(367, 0, 'config', 'config_tax_default', 'shipping', 0),
(366, 0, 'config', 'config_vat', '0', 0),
(365, 0, 'config', 'config_tax', '1', 0),
(364, 0, 'config', 'config_voucher_max', '1000', 0),
(363, 0, 'config', 'config_voucher_min', '1', 0),
(362, 0, 'config', 'config_download', '1', 0),
(361, 0, 'config', 'config_review_status', '1', 0),
(360, 0, 'config', 'config_product_count', '1', 0),
(359, 0, 'config', 'config_admin_limit', '20', 0),
(358, 0, 'config', 'config_catalog_limit', '15', 0),
(357, 0, 'config', 'config_weight_class_id', '1', 0),
(356, 0, 'config', 'config_length_class_id', '1', 0),
(355, 0, 'config', 'config_currency_auto', '1', 0),
(354, 0, 'config', 'config_currency', 'INR', 0),
(353, 0, 'config', 'config_admin_language', 'en', 0),
(352, 0, 'config', 'config_language', 'en', 0),
(351, 0, 'config', 'config_zone_id', '1493', 0),
(350, 0, 'config', 'config_country_id', '99', 0),
(349, 0, 'config', 'config_layout_id', '4', 0),
(348, 0, 'config', 'config_template', 'default', 0),
(347, 0, 'config', 'config_meta_description', 'My Store', 0),
(346, 0, 'config', 'config_title', 'Your Store', 0),
(345, 0, 'config', 'config_fax', '', 0),
(344, 0, 'config', 'config_telephone', '02030428219', 0),
(343, 0, 'config', 'config_email', 'equivets@gmail.com', 0),
(342, 0, 'config', 'config_address', 'Address 1', 0),
(341, 0, 'config', 'config_owner', 'Your Name', 0),
(340, 0, 'config', 'config_name', 'Your Store', 0),
(432, 0, 'config', 'config_seo_url', '0', 0),
(433, 0, 'config', 'config_file_extension_allowed', 'txt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc\r\nrtf\r\nxls\r\nppt\r\nodt\r\nods', 0),
(434, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/jpeg\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/vnd.microsoft.icon\r\nimage/tiff\r\nimage/tiff\r\nimage/svg+xml\r\nimage/svg+xml\r\napplication/zip\r\napplication/x-rar-compressed\r\napplication/x-msdownload\r\napplication/vnd.ms-cab-compressed\r\naudio/mpeg\r\nvideo/quicktime\r\nvideo/quicktime\r\napplication/pdf\r\nimage/vnd.adobe.photoshop\r\napplication/postscript\r\napplication/postscript\r\napplication/postscript\r\napplication/msword\r\napplication/rtf\r\napplication/vnd.ms-excel\r\napplication/vnd.ms-powerpoint\r\napplication/vnd.oasis.opendocument.text\r\napplication/vnd.oasis.opendocument.spreadsheet', 0),
(435, 0, 'config', 'config_maintenance', '0', 0),
(436, 0, 'config', 'config_password', '1', 0),
(437, 0, 'config', 'config_encryption', '3d321e4217f12c605083e8e9d48007bc', 0),
(438, 0, 'config', 'config_compression', '0', 0),
(439, 0, 'config', 'config_error_display', '1', 0),
(440, 0, 'config', 'config_error_log', '1', 0),
(441, 0, 'config', 'config_error_filename', 'error.txt', 0),
(442, 0, 'config', 'config_google_analytics', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_shift`
--

CREATE TABLE `oc_shift` (
  `shift_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `in_time` time NOT NULL,
  `out_time` time NOT NULL,
  `weekly_off_1` int(11) NOT NULL,
  `weekly_off_2` int(11) NOT NULL,
  `shift_code` varchar(255) NOT NULL,
  `dept` varchar(255) NOT NULL,
  `loc` varchar(255) NOT NULL,
  `lunch` int(11) NOT NULL,
  `department_mumbai` text NOT NULL,
  `department_pune` text NOT NULL,
  `department_moving` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_shift`
--

INSERT INTO `oc_shift` (`shift_id`, `name`, `in_time`, `out_time`, `weekly_off_1`, `weekly_off_2`, `shift_code`, `dept`, `loc`, `lunch`, `department_mumbai`, `department_pune`, `department_moving`) VALUES
(1, '09:30:00 - 18:30:00', '09:30:00', '18:30:00', 1, 1, 'T1', '', '', 0, '', '', ''),
(2, '08:00:00 - 20:00:00', '08:00:00', '20:00:00', 1, 1, 'T2', '', '', 0, '', '', ''),
(3, '08:00-20:00', '08:00:00', '20:00:00', 1, 1, 'T2', '', '', 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_shift_schedule`
--

CREATE TABLE `oc_shift_schedule` (
  `id` int(11) NOT NULL,
  `emp_code` int(11) NOT NULL,
  `1` varchar(255) NOT NULL,
  `2` varchar(255) NOT NULL,
  `3` varchar(255) NOT NULL,
  `4` varchar(255) NOT NULL,
  `5` varchar(255) NOT NULL,
  `6` varchar(255) NOT NULL,
  `7` varchar(255) NOT NULL,
  `8` varchar(255) NOT NULL,
  `9` varchar(255) NOT NULL,
  `10` varchar(255) NOT NULL,
  `11` varchar(255) NOT NULL,
  `12` varchar(255) NOT NULL,
  `13` varchar(255) NOT NULL,
  `14` varchar(255) NOT NULL,
  `15` varchar(255) NOT NULL,
  `16` varchar(255) NOT NULL,
  `17` varchar(255) NOT NULL,
  `18` varchar(255) NOT NULL,
  `19` varchar(255) NOT NULL,
  `20` varchar(255) NOT NULL,
  `21` varchar(255) NOT NULL,
  `22` varchar(255) NOT NULL,
  `23` varchar(255) NOT NULL,
  `24` varchar(255) NOT NULL,
  `25` varchar(255) NOT NULL,
  `26` varchar(255) NOT NULL,
  `27` varchar(255) NOT NULL,
  `28` varchar(255) NOT NULL,
  `29` varchar(255) NOT NULL,
  `30` varchar(255) NOT NULL,
  `31` varchar(255) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `unit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_shift_schedule`
--

INSERT INTO `oc_shift_schedule` (`id`, `emp_code`, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`, `10`, `11`, `12`, `13`, `14`, `15`, `16`, `17`, `18`, `19`, `20`, `21`, `22`, `23`, `24`, `25`, `26`, `27`, `28`, `29`, `30`, `31`, `month`, `year`, `status`, `unit`, `unit_id`) VALUES
(1, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 1, 2018, 1, 'CHAR', 28),
(2, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 2, 2018, 1, 'CHAR', 28),
(3, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 3, 2018, 1, 'CHAR', 28),
(4, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 4, 2018, 1, 'CHAR', 28),
(5, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 5, 2018, 1, 'CHAR', 28),
(6, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 6, 2018, 1, 'CHAR', 28),
(7, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 7, 2018, 1, 'CHAR', 28),
(8, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 8, 2018, 1, 'CHAR', 28),
(9, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 9, 2018, 1, 'CHAR', 28),
(10, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 10, 2018, 1, 'CHAR', 28),
(11, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 11, 2018, 1, 'CHAR', 28),
(12, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 12, 2018, 1, 'CHAR', 28),
(13, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 1, 2020, 1, 'CHAR', 28),
(14, 1000, 'S_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'S_1', 'S_1', 2, 2020, 1, 'CHAR', 28),
(15, 1000, 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 3, 2020, 1, 'CHAR', 28),
(16, 1000, 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 4, 2020, 1, 'CHAR', 28),
(17, 1000, 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 5, 2020, 1, 'CHAR', 28),
(18, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 6, 2020, 1, 'CHAR', 28),
(19, 1000, 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 7, 2020, 1, 'CHAR', 28),
(20, 1000, 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 8, 2020, 1, 'CHAR', 28),
(21, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 9, 2020, 1, 'CHAR', 28),
(22, 1000, 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 10, 2020, 1, 'CHAR', 28),
(23, 1000, 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 11, 2020, 1, 'CHAR', 28),
(24, 1000, 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 12, 2020, 1, 'CHAR', 28),
(25, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 1, 2018, 1, 'DM12', 75),
(26, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 2, 2018, 1, 'DM12', 75),
(27, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 3, 2018, 1, 'DM12', 75),
(28, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 4, 2018, 1, 'DM12', 75),
(29, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 5, 2018, 1, 'DM12', 75),
(30, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 6, 2018, 1, 'DM12', 75),
(31, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 7, 2018, 1, 'DM12', 75),
(32, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 8, 2018, 1, 'DM12', 75),
(33, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 9, 2018, 1, 'DM12', 75),
(34, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 10, 2018, 1, 'DM12', 75),
(35, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 11, 2018, 1, 'DM12', 75),
(36, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 12, 2018, 1, 'DM12', 75),
(37, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 1, 2020, 1, 'JMCH - AHD', 2),
(38, 1001, 'S_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'S_1', 'S_1', 2, 2020, 1, 'JMCH - AHD', 2),
(39, 1001, 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 3, 2020, 1, 'JMCH - AHD', 2),
(40, 1001, 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 4, 2020, 1, 'JMCH - AHD', 2),
(41, 1001, 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 5, 2020, 1, 'JMCH - AHD', 2),
(42, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 6, 2020, 1, 'JMCH - AHD', 2),
(43, 1001, 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 7, 2020, 1, 'JMCH - AHD', 2),
(44, 1001, 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 8, 2020, 1, 'JMCH - AHD', 2),
(45, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 9, 2020, 1, 'JMCH - AHD', 2),
(46, 1001, 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 10, 2020, 1, 'JMCH - AHD', 2),
(47, 1001, 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 11, 2020, 1, 'JMCH - AHD', 2),
(48, 1001, 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 12, 2020, 1, 'JMCH - AHD', 2),
(49, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 1, 2018, 1, 'DLOF', 17),
(50, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 2, 2018, 1, 'DLOF', 17),
(51, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 3, 2018, 1, 'DLOF', 17),
(52, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 4, 2018, 1, 'DLOF', 17),
(53, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 5, 2018, 1, 'DLOF', 17),
(54, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 6, 2018, 1, 'DLOF', 17),
(55, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 7, 2018, 1, 'DLOF', 17),
(56, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 8, 2018, 1, 'DLOF', 17),
(57, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 9, 2018, 1, 'DLOF', 17),
(58, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 10, 2018, 1, 'DLOF', 17),
(59, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 11, 2018, 1, 'DLOF', 17),
(60, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 12, 2018, 1, 'DLOF', 17),
(61, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 1, 2020, 1, 'DLOF', 17),
(62, 1002, 'S_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'S_1', 'S_1', 2, 2020, 1, 'DLOF', 17),
(63, 1002, 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 3, 2020, 1, 'DLOF', 17),
(64, 1002, 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 4, 2020, 1, 'DLOF', 17),
(65, 1002, 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 5, 2020, 1, 'DLOF', 17),
(66, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 6, 2020, 1, 'DLOF', 17),
(67, 1002, 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 7, 2020, 1, 'DLOF', 17),
(68, 1002, 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 8, 2020, 1, 'DLOF', 17),
(69, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 9, 2020, 1, 'DLOF', 17),
(70, 1002, 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 10, 2020, 1, 'DLOF', 17),
(71, 1002, 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 11, 2020, 1, 'DLOF', 17),
(72, 1002, 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'HD_1_1', 'W_1_1', 'S_1', 'S_1', 'S_1', 'S_1', 12, 2020, 1, 'DLOF', 17),
(73, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 1, 2018, 1, 'DLOF', 17),
(74, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 2, 2018, 1, 'DLOF', 17),
(75, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 3, 2018, 1, 'DLOF', 17),
(76, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 4, 2018, 1, 'DLOF', 17),
(77, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 5, 2018, 1, 'DLOF', 17),
(78, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 6, 2018, 1, 'DLOF', 17),
(79, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 7, 2018, 1, 'DLOF', 17),
(80, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 8, 2018, 1, 'DLOF', 17),
(81, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 9, 2018, 1, 'DLOF', 17),
(82, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 10, 2018, 1, 'DLOF', 17),
(83, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 11, 2018, 1, 'DLOF', 17),
(84, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 12, 2018, 1, 'DLOF', 17),
(85, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 1, 2020, 1, 'DLOF', 17),
(86, 1003, 'S_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'S_2', 'S_2', 2, 2020, 1, 'DLOF', 17),
(87, 1003, 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 3, 2020, 1, 'DLOF', 17),
(88, 1003, 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 4, 2020, 1, 'DLOF', 17),
(89, 1003, 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 5, 2020, 1, 'DLOF', 17),
(90, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 6, 2020, 1, 'DLOF', 17),
(91, 1003, 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 7, 2020, 1, 'DLOF', 17),
(92, 1003, 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 8, 2020, 1, 'DLOF', 17),
(93, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 9, 2020, 1, 'DLOF', 17),
(94, 1003, 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 10, 2020, 1, 'DLOF', 17),
(95, 1003, 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 11, 2020, 1, 'DLOF', 17),
(96, 1003, 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 'S_2', 'HD_1_2', 'W_1_2', 'S_2', 'S_2', 'S_2', 'S_2', 12, 2020, 1, 'DLOF', 17),
(97, 42344456, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 1, 2021, 1, 'MCFO', 53),
(98, 42344456, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 2, 2021, 1, 'MCFO', 53),
(99, 42344456, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 3, 2021, 1, 'MCFO', 53),
(100, 42344456, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 4, 2021, 1, 'MCFO', 53),
(101, 42344456, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 5, 2021, 1, 'MCFO', 53),
(102, 42344456, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 6, 2021, 1, 'MCFO', 53),
(103, 42344456, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 7, 2021, 1, 'MCFO', 53),
(104, 42344456, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 8, 2021, 1, 'MCFO', 53),
(105, 42344456, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 9, 2021, 1, 'MCFO', 53),
(106, 42344456, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 10, 2021, 1, 'MCFO', 53),
(107, 42344456, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 11, 2021, 1, 'MCFO', 53),
(108, 42344456, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 12, 2021, 1, 'MCFO', 53),
(109, 42344458, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 1, 2021, 1, 'MCFO', 53),
(110, 42344458, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 2, 2021, 1, 'MCFO', 53),
(111, 42344458, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 3, 2021, 1, 'MCFO', 53),
(112, 42344458, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 4, 2021, 1, 'MCFO', 53),
(113, 42344458, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 5, 2021, 1, 'MCFO', 53),
(114, 42344458, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 6, 2021, 1, 'MCFO', 53),
(115, 42344458, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 7, 2021, 1, 'MCFO', 53),
(116, 42344458, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 8, 2021, 1, 'MCFO', 53),
(117, 42344458, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 9, 2021, 1, 'MCFO', 53),
(118, 42344458, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 10, 2021, 1, 'MCFO', 53),
(119, 42344458, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 11, 2021, 1, 'MCFO', 53),
(120, 42344458, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 12, 2021, 1, 'MCFO', 53),
(121, 2147483647, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 1, 2021, 1, 'OPRC', 0),
(122, 2147483647, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 2, 2021, 1, 'OPRC', 0),
(123, 2147483647, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 3, 2021, 1, 'OPRC', 0),
(124, 2147483647, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 4, 2021, 1, 'OPRC', 0),
(125, 2147483647, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 5, 2021, 1, 'OPRC', 0),
(126, 2147483647, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 6, 2021, 1, 'OPRC', 0),
(127, 2147483647, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 7, 2021, 1, 'OPRC', 0),
(128, 2147483647, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 8, 2021, 1, 'OPRC', 0),
(129, 2147483647, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 9, 2021, 1, 'OPRC', 0),
(130, 2147483647, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 10, 2021, 1, 'OPRC', 0),
(131, 2147483647, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 11, 2021, 1, 'OPRC', 0),
(132, 2147483647, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 12, 2021, 1, 'OPRC', 0),
(133, 2147483647, 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 'S_1', 12, 2019, 1, 'OPRC', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_shift_unit`
--

CREATE TABLE `oc_shift_unit` (
  `id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_id` int(11) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_state`
--

CREATE TABLE `oc_state` (
  `state_id` int(11) NOT NULL,
  `state` varchar(255) NOT NULL,
  `state_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_state`
--

INSERT INTO `oc_state` (`state_id`, `state`, `state_code`) VALUES
(1, 'TAMILNADU', ''),
(2, 'GUJARAT', ''),
(3, 'KARNATAKA', ''),
(4, 'MAHARASHTRA', ''),
(5, 'BIHAR', ''),
(6, 'ETHIOPIA', ''),
(7, 'DELHI', ''),
(8, 'CHHATISGARH', ''),
(9, 'ANDHRA PRADESH', ''),
(10, 'MADHYA PRADESH', ''),
(11, 'HARYANA', ''),
(12, 'UTTAR PRADESH', ''),
(13, 'TELANGANA', ''),
(14, 'WEST BENGAL', ''),
(15, 'UTTRANCHAL', ''),
(16, 'SRILANKA', ''),
(17, '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_status`
--

CREATE TABLE `oc_status` (
  `id` int(11) NOT NULL,
  `process_status` int(11) NOT NULL,
  `update_date_time_reprocess` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unit_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_stock_status`
--

CREATE TABLE `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'In Stock'),
(8, 1, 'Pre-Order'),
(5, 1, 'Out Of Stock'),
(6, 1, '2 - 3 Days');

-- --------------------------------------------------------

--
-- Table structure for table `oc_store`
--

CREATE TABLE `oc_store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_task`
--

CREATE TABLE `oc_task` (
  `id` int(11) NOT NULL,
  `data` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `oc_task`
--

INSERT INTO `oc_task` (`id`, `data`, `datetime`, `status`) VALUES
(333, '{\"filter_date_start\":\"2021-11-01\",\"filter_date_start_constant\":\"2021-11-01\",\"filter_date_end\":\"2021-11-13\",\"filter_date_end_constant\":\"2021-11-13\",\"filter_name\":\"\",\"filter_name_id\":\"\",\"filter_unit\":\"1\",\"filter_department\":0,\"filter_division\":0,\"filter_region\":0,\"filter_company\":0,\"filter_contractor\":0,\"filter_email\":\"fargose.aaron@gmail.com\",\"filter_user_id\":\"1\"}', '2021-11-13 12:45:55', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_task_status`
--

CREATE TABLE `oc_task_status` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `oc_task_status`
--

INSERT INTO `oc_task_status` (`id`, `status`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_class`
--

CREATE TABLE `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed Stuff', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate`
--

CREATE TABLE `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (17.5%)', '17.5000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate_to_customer_group`
--

CREATE TABLE `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rule`
--

CREATE TABLE `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_transaction`
--

CREATE TABLE `oc_transaction` (
  `transaction_id` int(11) NOT NULL,
  `emp_id` varchar(512) NOT NULL DEFAULT '0',
  `emp_name` varchar(255) NOT NULL DEFAULT '',
  `shift_id` int(11) NOT NULL DEFAULT '0',
  `shift_code` varchar(255) NOT NULL DEFAULT '',
  `shift_intime` time NOT NULL DEFAULT '00:00:00',
  `shift_outtime` time NOT NULL DEFAULT '00:00:00',
  `act_intime` time NOT NULL DEFAULT '00:00:00',
  `act_outtime` time NOT NULL DEFAULT '00:00:00',
  `weekly_off` int(11) NOT NULL DEFAULT '0',
  `holiday_id` int(11) NOT NULL DEFAULT '0',
  `late_time` time NOT NULL DEFAULT '00:00:00',
  `early_time` time NOT NULL DEFAULT '00:00:00',
  `working_time` time NOT NULL DEFAULT '00:00:00',
  `day` int(11) NOT NULL DEFAULT '0',
  `month` int(11) NOT NULL DEFAULT '0',
  `year` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `status` int(11) NOT NULL DEFAULT '0',
  `department` varchar(255) NOT NULL DEFAULT '',
  `unit` varchar(255) NOT NULL DEFAULT '',
  `group` varchar(255) NOT NULL DEFAULT '',
  `firsthalf_status` varchar(255) NOT NULL DEFAULT '',
  `secondhalf_status` varchar(255) NOT NULL DEFAULT '',
  `manual_status` int(11) NOT NULL DEFAULT '0',
  `day_close_status` int(11) NOT NULL DEFAULT '0',
  `halfday_status` int(11) NOT NULL DEFAULT '0',
  `leave_status` float NOT NULL DEFAULT '0',
  `present_status` float NOT NULL DEFAULT '0',
  `absent_status` float NOT NULL DEFAULT '0',
  `abnormal_status` int(11) NOT NULL DEFAULT '0',
  `date_out` date NOT NULL DEFAULT '0000-00-00',
  `on_duty` int(1) NOT NULL DEFAULT '0',
  `compli_status` int(11) NOT NULL DEFAULT '0',
  `month_close_status` int(11) NOT NULL DEFAULT '0',
  `device_id` int(11) NOT NULL DEFAULT '0',
  `batch_id` int(11) NOT NULL DEFAULT '0',
  `late_mark` int(11) NOT NULL DEFAULT '0',
  `early_mark` int(11) NOT NULL DEFAULT '0',
  `division` varchar(255) NOT NULL DEFAULT '',
  `region` varchar(255) NOT NULL DEFAULT '',
  `department_id` int(11) NOT NULL DEFAULT '0',
  `division_id` int(11) NOT NULL DEFAULT '0',
  `unit_id` int(11) NOT NULL DEFAULT '0',
  `region_id` int(11) NOT NULL DEFAULT '0',
  `shift_change` int(11) NOT NULL DEFAULT '0',
  `shift_free_change` int(11) NOT NULL DEFAULT '0',
  `act_intime_change` int(11) NOT NULL DEFAULT '0',
  `act_outtime_change` int(11) NOT NULL DEFAULT '0',
  `company` varchar(255) NOT NULL DEFAULT '',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `contractor` varchar(255) NOT NULL DEFAULT '',
  `contractor_id` int(11) NOT NULL DEFAULT '0',
  `contractor_code` varchar(255) NOT NULL DEFAULT '',
  `category` varchar(255) NOT NULL DEFAULT '',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `over_time` time NOT NULL DEFAULT '00:00:00',
  `designation` varchar(255) NOT NULL DEFAULT '',
  `designation_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_transaction`
--

INSERT INTO `oc_transaction` (`transaction_id`, `emp_id`, `emp_name`, `shift_id`, `shift_code`, `shift_intime`, `shift_outtime`, `act_intime`, `act_outtime`, `weekly_off`, `holiday_id`, `late_time`, `early_time`, `working_time`, `day`, `month`, `year`, `date`, `status`, `department`, `unit`, `group`, `firsthalf_status`, `secondhalf_status`, `manual_status`, `day_close_status`, `halfday_status`, `leave_status`, `present_status`, `absent_status`, `abnormal_status`, `date_out`, `on_duty`, `compli_status`, `month_close_status`, `device_id`, `batch_id`, `late_mark`, `early_mark`, `division`, `region`, `department_id`, `division_id`, `unit_id`, `region_id`, `shift_change`, `shift_free_change`, `act_intime_change`, `act_outtime_change`, `company`, `company_id`, `contractor`, `contractor_id`, `contractor_code`, `category`, `category_id`, `over_time`, `designation`, `designation_id`) VALUES
(1, '235666539189', 'N KUMAR MEHRA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 11, 2021, '2021-11-01', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-01', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(4, '235666539189', 'N KUMAR MEHRA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 11, 2021, '2021-11-04', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-04', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(5, '235666539189', 'N KUMAR MEHRA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 11, 2021, '2021-11-05', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-05', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(7, '235666539189', 'N KUMAR MEHRA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 11, 2021, '2021-04-01', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-07', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(8, '235666539189', 'N KUMAR MEHRA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 11, 2021, '2021-11-08', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-08', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(9, '235666539189', 'N KUMAR MEHRA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 11, 2021, '2021-11-09', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-09', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(10, '235666539189', 'N KUMAR MEHRA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 11, 2021, '2021-11-10', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-10', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(11, '235666539189', 'N KUMAR MEHRA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 11, 2021, '2021-11-11', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-11', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(12, '235666539189', 'N KUMAR MEHRA', 1, 'GS', '08:00:00', '20:00:00', '09:18:00', '18:00:00', 0, 0, '00:00:00', '00:00:00', '08:42:00', 12, 11, 2021, '2021-11-12', 0, 'PROJECTS', 'AIHB', '', '1', '1', 0, 0, 0, 0, 1, 0, 0, '2021-11-12', 0, 0, 0, 2, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(13, '235666539189', 'N KUMAR MEHRA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 11, 2021, '2021-11-13', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-13', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(14, '272424159714', 'VINOD TATI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 11, 2021, '2021-11-01', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-01', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'HELPER', 7),
(16, '272424159714', 'VINOD TATI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 11, 2021, '2021-11-03', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-03', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'HELPER', 7),
(17, '272424159714', 'VINOD TATI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 11, 2021, '2021-11-04', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-04', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'HELPER', 7),
(18, '272424159714', 'VINOD TATI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 11, 2021, '2021-11-05', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-05', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'HELPER', 7),
(19, '272424159714', 'VINOD TATI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 11, 2021, '2021-11-06', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-06', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'HELPER', 7),
(20, '272424159714', 'VINOD TATI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 11, 2021, '2021-11-07', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-07', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'HELPER', 7),
(21, '272424159714', 'VINOD TATI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 11, 2021, '2021-11-08', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-08', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'HELPER', 7),
(22, '272424159714', 'VINOD TATI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 11, 2021, '2021-11-09', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-09', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'HELPER', 7),
(23, '272424159714', 'VINOD TATI', 1, 'GS', '08:00:00', '20:00:00', '09:22:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 11, 2021, '2021-11-10', 0, 'PROJECTS', 'AIHB', '', '1', '1', 0, 0, 0, 0, 1, 0, 0, '2021-11-10', 0, 0, 0, 1, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'HELPER', 7),
(24, '272424159714', 'VINOD TATI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 11, 2021, '2021-11-11', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-11', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'HELPER', 7),
(25, '272424159714', 'VINOD TATI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 11, 2021, '2021-11-12', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-12', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'HELPER', 7),
(26, '272424159714', 'VINOD TATI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 11, 2021, '2021-11-13', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-13', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'HELPER', 7),
(27, '482805083058', 'NITESH NIKAM', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 11, 2021, '2021-11-01', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-01', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(28, '482805083058', 'NITESH NIKAM', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 11, 2021, '2021-11-02', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-02', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(30, '482805083058', 'NITESH NIKAM', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 11, 2021, '2021-11-04', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-04', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(31, '482805083058', 'NITESH NIKAM', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 11, 2021, '2021-11-05', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-05', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(32, '482805083058', 'NITESH NIKAM', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 11, 2021, '2021-11-06', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-06', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(33, '482805083058', 'NITESH NIKAM', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 11, 2021, '2021-11-07', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-07', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(34, '482805083058', 'NITESH NIKAM', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 11, 2021, '2021-11-08', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-08', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(35, '482805083058', 'NITESH NIKAM', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 11, 2021, '2021-11-09', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-09', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(36, '482805083058', 'NITESH NIKAM', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 11, 2021, '2021-11-10', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-10', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(37, '482805083058', 'NITESH NIKAM', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 11, 2021, '2021-11-11', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-11', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(38, '482805083058', 'NITESH NIKAM', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 11, 2021, '2021-11-12', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-12', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(39, '482805083058', 'NITESH NIKAM', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 11, 2021, '2021-11-13', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-13', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(40, '644576398894', 'RAJEEV BHRTI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 11, 2021, '2021-11-01', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-01', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(41, '644576398894', 'RAJEEV BHRTI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 11, 2021, '2021-11-02', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-02', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(42, '644576398894', 'RAJEEV BHRTI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 11, 2021, '2021-11-03', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-03', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(43, '644576398894', 'RAJEEV BHRTI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 11, 2021, '2021-11-04', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-04', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(44, '644576398894', 'RAJEEV BHRTI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 11, 2021, '2021-11-05', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-05', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(45, '644576398894', 'RAJEEV BHRTI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 11, 2021, '2021-11-06', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-06', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(46, '644576398894', 'RAJEEV BHRTI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 11, 2021, '2021-11-07', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-07', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(47, '644576398894', 'RAJEEV BHRTI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 11, 2021, '2021-11-08', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-08', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(48, '644576398894', 'RAJEEV BHRTI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 11, 2021, '2021-11-09', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-09', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(49, '644576398894', 'RAJEEV BHRTI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 11, 2021, '2021-11-10', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-10', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(50, '644576398894', 'RAJEEV BHRTI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 11, 2021, '2021-11-11', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-11', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(51, '644576398894', 'RAJEEV BHRTI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 11, 2021, '2021-11-12', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-12', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(52, '644576398894', 'RAJEEV BHRTI', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 11, 2021, '2021-11-13', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-13', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'NITESH NIKAM', 1, 'NITESH NIKAM', 'SUPPLY', 1, '00:00:00', 'CARPENTER', 1),
(53, '785705412913', 'RAJ KUMAR NARVARIYA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 11, 2021, '2021-11-01', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-01', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '00:00:00', 'PLUMBER', 5),
(54, '785705412913', 'RAJ KUMAR NARVARIYA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 11, 2021, '2021-11-02', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-02', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '00:00:00', 'PLUMBER', 5),
(55, '785705412913', 'RAJ KUMAR NARVARIYA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 11, 2021, '2021-11-03', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-03', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '00:00:00', 'PLUMBER', 5),
(56, '785705412913', 'RAJ KUMAR NARVARIYA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 11, 2021, '2021-11-04', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-04', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '00:00:00', 'PLUMBER', 5),
(57, '785705412913', 'RAJ KUMAR NARVARIYA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 11, 2021, '2021-11-05', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-05', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '00:00:00', 'PLUMBER', 5),
(58, '785705412913', 'RAJ KUMAR NARVARIYA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 11, 2021, '2021-11-06', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-06', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '00:00:00', 'PLUMBER', 5),
(59, '785705412913', 'RAJ KUMAR NARVARIYA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 11, 2021, '2021-11-07', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-07', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '00:00:00', 'PLUMBER', 5),
(60, '785705412913', 'RAJ KUMAR NARVARIYA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 11, 2021, '2021-11-08', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-08', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '00:00:00', 'PLUMBER', 5),
(61, '785705412913', 'RAJ KUMAR NARVARIYA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 11, 2021, '2021-11-09', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-09', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '00:00:00', 'PLUMBER', 5),
(62, '785705412913', 'RAJ KUMAR NARVARIYA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 11, 2021, '2021-11-10', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-10', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '00:00:00', 'PLUMBER', 5),
(63, '785705412913', 'RAJ KUMAR NARVARIYA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 11, 2021, '2021-11-11', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-11', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '00:00:00', 'PLUMBER', 5),
(64, '785705412913', 'RAJ KUMAR NARVARIYA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 11, 2021, '2021-11-12', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-12', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '00:00:00', 'PLUMBER', 5),
(65, '785705412913', 'RAJ KUMAR NARVARIYA', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 11, 2021, '2021-11-13', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-13', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ANURADHA SINGH', 230, '3000026310', 'SUPPLY', 1, '00:00:00', 'PLUMBER', 5),
(66, '841413256488', 'BHAGVAT ASHOK BAVSKAR', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 11, 2021, '2021-11-01', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-01', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(67, '841413256488', 'BHAGVAT ASHOK BAVSKAR', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 11, 2021, '2021-11-02', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-02', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(68, '841413256488', 'BHAGVAT ASHOK BAVSKAR', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 11, 2021, '2021-11-03', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-03', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(69, '841413256488', 'BHAGVAT ASHOK BAVSKAR', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 11, 2021, '2021-11-04', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-04', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(70, '841413256488', 'BHAGVAT ASHOK BAVSKAR', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 11, 2021, '2021-11-05', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-05', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(71, '841413256488', 'BHAGVAT ASHOK BAVSKAR', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 11, 2021, '2021-11-06', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-06', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(72, '841413256488', 'BHAGVAT ASHOK BAVSKAR', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 11, 2021, '2021-11-07', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-07', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(73, '841413256488', 'BHAGVAT ASHOK BAVSKAR', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 11, 2021, '2021-11-08', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-08', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(74, '841413256488', 'BHAGVAT ASHOK BAVSKAR', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 11, 2021, '2021-11-09', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-09', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(75, '841413256488', 'BHAGVAT ASHOK BAVSKAR', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 11, 2021, '2021-11-10', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-10', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(76, '841413256488', 'BHAGVAT ASHOK BAVSKAR', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 11, 2021, '2021-11-11', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-11', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(77, '841413256488', 'BHAGVAT ASHOK BAVSKAR', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 11, 2021, '2021-11-12', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-12', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(78, '841413256488', 'BHAGVAT ASHOK BAVSKAR', 1, 'GS', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 11, 2021, '2021-11-13', 0, 'PROJECTS', 'AIHB', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-13', 0, 0, 0, 0, 0, 0, 0, 'NIO', 'NIO', 1, 1, 1, 1, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'ASHOK KUMAR AASHIK', 2, '3000010689', 'SUPPLY', 1, '00:00:00', 'MASON', 2),
(79, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 9, 2021, '2021-09-19', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-19', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(80, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 9, 2021, '2021-09-20', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-20', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(81, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 9, 2021, '2021-09-21', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-21', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(82, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 9, 2021, '2021-09-22', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-22', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(83, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 23, 9, 2021, '2021-09-23', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-23', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(84, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 9, 2021, '2021-09-24', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-24', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(85, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 9, 2021, '2021-09-25', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-25', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(86, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 9, 2021, '2021-09-26', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-26', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(87, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 9, 2021, '2021-09-27', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-27', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(88, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 28, 9, 2021, '2021-09-28', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-28', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(89, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 29, 9, 2021, '2021-09-29', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-29', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(90, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 30, 9, 2021, '2021-09-30', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-30', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(91, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 10, 2021, '2021-10-01', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-01', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(92, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 10, 2021, '2021-10-02', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-02', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(93, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 10, 2021, '2021-10-03', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-03', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(94, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 10, 2021, '2021-10-04', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-04', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(95, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 10, 2021, '2021-10-05', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-05', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(96, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 10, 2021, '2021-10-06', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-06', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(97, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 10, 2021, '2021-10-07', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-07', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(98, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 10, 2021, '2021-10-08', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-08', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(99, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 10, 2021, '2021-10-09', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-09', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(100, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 10, 2021, '2021-10-10', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-10', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(101, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 10, 2021, '2021-10-11', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-11', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(102, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 10, 2021, '2021-10-12', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-12', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(103, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 10, 2021, '2021-10-13', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-13', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(104, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 10, 2021, '2021-10-14', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-14', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(105, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 15, 10, 2021, '2021-10-15', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-15', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(106, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 16, 10, 2021, '2021-10-16', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-16', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(107, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 10, 2021, '2021-10-17', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-17', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(108, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 10, 2021, '2021-10-18', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-18', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(109, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 10, 2021, '2021-10-19', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-19', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(110, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 10, 2021, '2021-10-20', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-20', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(111, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 10, 2021, '2021-10-21', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-21', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(112, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 10, 2021, '2021-10-22', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-22', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(113, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 23, 10, 2021, '2021-10-23', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-23', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(114, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 10, 2021, '2021-10-24', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-24', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(115, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 10, 2021, '2021-10-25', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-25', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(116, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 10, 2021, '2021-10-26', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-26', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(117, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 10, 2021, '2021-10-27', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-27', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(118, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 28, 10, 2021, '2021-10-28', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-28', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(119, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 29, 10, 2021, '2021-10-29', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-29', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(120, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 30, 10, 2021, '2021-10-30', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-30', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(121, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 31, 10, 2021, '2021-10-31', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-31', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(122, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 11, 2021, '2021-11-01', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-01', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(123, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 11, 2021, '2021-11-02', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-02', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(124, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 11, 2021, '2021-11-03', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-03', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(125, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 11, 2021, '2021-11-04', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-04', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1);
INSERT INTO `oc_transaction` (`transaction_id`, `emp_id`, `emp_name`, `shift_id`, `shift_code`, `shift_intime`, `shift_outtime`, `act_intime`, `act_outtime`, `weekly_off`, `holiday_id`, `late_time`, `early_time`, `working_time`, `day`, `month`, `year`, `date`, `status`, `department`, `unit`, `group`, `firsthalf_status`, `secondhalf_status`, `manual_status`, `day_close_status`, `halfday_status`, `leave_status`, `present_status`, `absent_status`, `abnormal_status`, `date_out`, `on_duty`, `compli_status`, `month_close_status`, `device_id`, `batch_id`, `late_mark`, `early_mark`, `division`, `region`, `department_id`, `division_id`, `unit_id`, `region_id`, `shift_change`, `shift_free_change`, `act_intime_change`, `act_outtime_change`, `company`, `company_id`, `contractor`, `contractor_id`, `contractor_code`, `category`, `category_id`, `over_time`, `designation`, `designation_id`) VALUES
(126, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 11, 2021, '2021-11-05', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-05', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(127, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 11, 2021, '2021-11-06', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-06', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(128, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 11, 2021, '2021-11-07', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-07', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(129, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 11, 2021, '2021-11-08', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-08', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(130, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 11, 2021, '2021-11-09', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-09', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(131, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 11, 2021, '2021-11-10', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-10', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(132, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 11, 2021, '2021-11-11', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-11', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(133, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 11, 2021, '2021-11-12', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-12', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(134, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 11, 2021, '2021-11-13', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-13', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(135, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 11, 2021, '2021-11-14', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-14', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(136, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 15, 11, 2021, '2021-11-15', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-15', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(137, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 16, 11, 2021, '2021-11-16', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-16', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(138, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 11, 2021, '2021-11-17', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-17', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(139, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 11, 2021, '2021-11-18', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-18', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(140, '42344456', 'MOHAMMAD HADISH', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 11, 2021, '2021-11-19', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-19', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(141, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 9, 2021, '2021-09-19', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-19', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(142, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 9, 2021, '2021-09-20', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-20', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(143, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 9, 2021, '2021-09-21', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-21', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(144, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 9, 2021, '2021-09-22', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-22', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(145, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 23, 9, 2021, '2021-09-23', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-23', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(146, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 9, 2021, '2021-09-24', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-24', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(147, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 9, 2021, '2021-09-25', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-25', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(148, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 9, 2021, '2021-09-26', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-26', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(149, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 9, 2021, '2021-09-27', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-27', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(150, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 28, 9, 2021, '2021-09-28', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-28', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(151, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 29, 9, 2021, '2021-09-29', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-29', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(152, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 30, 9, 2021, '2021-09-30', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-09-30', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(153, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 10, 2021, '2021-10-01', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-01', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(154, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 10, 2021, '2021-10-02', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-02', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(155, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 10, 2021, '2021-10-03', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-03', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(156, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 10, 2021, '2021-10-04', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-04', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(157, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 10, 2021, '2021-10-05', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-05', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(158, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 10, 2021, '2021-10-06', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-06', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(159, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 10, 2021, '2021-10-07', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-07', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(160, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 10, 2021, '2021-10-08', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-08', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(161, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 10, 2021, '2021-10-09', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-09', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(162, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 10, 2021, '2021-10-10', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-10', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(163, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 10, 2021, '2021-10-11', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-11', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(164, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 10, 2021, '2021-10-12', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-12', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(165, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 10, 2021, '2021-10-13', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-13', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(166, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 10, 2021, '2021-10-14', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-14', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(167, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 15, 10, 2021, '2021-10-15', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-15', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(168, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 16, 10, 2021, '2021-10-16', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-16', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(169, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 10, 2021, '2021-10-17', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-17', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(170, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 10, 2021, '2021-10-18', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-18', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(171, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 10, 2021, '2021-10-19', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-19', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(172, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 10, 2021, '2021-10-20', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-20', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(173, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 10, 2021, '2021-10-21', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-21', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(174, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 10, 2021, '2021-10-22', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-22', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(175, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 23, 10, 2021, '2021-10-23', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-23', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(176, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 10, 2021, '2021-10-24', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-24', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(177, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 10, 2021, '2021-10-25', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-25', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(178, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 10, 2021, '2021-10-26', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-26', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(179, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 10, 2021, '2021-10-27', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-27', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(180, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 28, 10, 2021, '2021-10-28', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-28', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(181, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 29, 10, 2021, '2021-10-29', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-29', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(182, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 30, 10, 2021, '2021-10-30', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-30', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(183, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 31, 10, 2021, '2021-10-31', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-10-31', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(184, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 11, 2021, '2021-11-01', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-01', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(185, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 11, 2021, '2021-11-02', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-02', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(186, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 11, 2021, '2021-11-03', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-03', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(187, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 11, 2021, '2021-11-04', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-04', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(188, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 11, 2021, '2021-11-05', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-05', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(189, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 11, 2021, '2021-11-06', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-06', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(190, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 11, 2021, '2021-11-07', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-07', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(191, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 11, 2021, '2021-11-08', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-08', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(192, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 11, 2021, '2021-11-09', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-09', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(193, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 11, 2021, '2021-11-10', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-10', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(194, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 11, 2021, '2021-11-11', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-11', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(195, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 11, 2021, '2021-11-12', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-12', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(196, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 11, 2021, '2021-11-13', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-13', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(197, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 11, 2021, '2021-11-14', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-14', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(198, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 15, 11, 2021, '2021-11-15', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-15', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(199, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 16, 11, 2021, '2021-11-16', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-16', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(200, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 11, 2021, '2021-11-17', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-17', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(201, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 11, 2021, '2021-11-18', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-18', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1),
(202, '42344458', 'MD SARFARAS ALI', 1, 'T1', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 11, 2021, '2021-11-19', 0, 'PROJECTS', 'MCFO', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2021-11-19', 0, 0, 0, 0, 0, 0, 0, 'INFRA', '', 7, 3, 53, 0, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1, 'DIWAKAR ENTERPRISES', 1634, '3000033707', 'PIECE RATE', 1, '00:00:00', 'EXECUTIVE - ACCOUNTS', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_transaction_2020_1`
--

CREATE TABLE `oc_transaction_2020_1` (
  `transaction_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(255) NOT NULL,
  `shift_intime` time NOT NULL,
  `shift_outtime` time NOT NULL,
  `act_intime` time NOT NULL,
  `act_outtime` time NOT NULL,
  `weekly_off` int(11) NOT NULL,
  `holiday_id` int(11) NOT NULL,
  `late_time` time NOT NULL,
  `early_time` time NOT NULL,
  `working_time` time NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `firsthalf_status` varchar(255) NOT NULL,
  `secondhalf_status` varchar(255) NOT NULL,
  `manual_status` int(11) NOT NULL,
  `day_close_status` int(11) NOT NULL,
  `halfday_status` int(11) NOT NULL,
  `leave_status` float NOT NULL,
  `present_status` float NOT NULL,
  `absent_status` float NOT NULL,
  `abnormal_status` int(11) NOT NULL,
  `date_out` date NOT NULL,
  `on_duty` int(1) NOT NULL,
  `compli_status` int(11) NOT NULL,
  `month_close_status` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `late_mark` int(11) NOT NULL,
  `early_mark` int(11) NOT NULL,
  `division` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `shift_change` int(11) NOT NULL,
  `shift_free_change` int(11) NOT NULL,
  `act_intime_change` int(11) NOT NULL,
  `act_outtime_change` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_transaction_2020_1`
--

INSERT INTO `oc_transaction_2020_1` (`transaction_id`, `emp_id`, `emp_name`, `shift_intime`, `shift_outtime`, `act_intime`, `act_outtime`, `weekly_off`, `holiday_id`, `late_time`, `early_time`, `working_time`, `day`, `month`, `year`, `date`, `status`, `department`, `unit`, `group`, `firsthalf_status`, `secondhalf_status`, `manual_status`, `day_close_status`, `halfday_status`, `leave_status`, `present_status`, `absent_status`, `abnormal_status`, `date_out`, `on_duty`, `compli_status`, `month_close_status`, `device_id`, `batch_id`, `late_mark`, `early_mark`, `division`, `region`, `department_id`, `division_id`, `unit_id`, `region_id`, `shift_change`, `shift_free_change`, `act_intime_change`, `act_outtime_change`, `company`, `company_id`) VALUES
(1, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 1, 2020, '2020-01-18', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-18', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(2, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 1, 2020, '2020-01-18', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-18', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(3, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 1, 2020, '2020-01-18', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-18', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(4, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 1, 2020, '2020-01-18', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-18', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(5, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 1, 2020, '2020-01-19', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-19', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(6, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 1, 2020, '2020-01-19', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-19', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(7, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 1, 2020, '2020-01-19', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-19', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(8, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 1, 2020, '2020-01-19', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-19', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(9, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 1, 2020, '2020-01-19', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-19', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(10, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 1, 2020, '2020-01-20', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-20', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(11, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 1, 2020, '2020-01-20', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-20', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(12, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 1, 2020, '2020-01-20', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-20', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(13, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 1, 2020, '2020-01-20', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-20', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(14, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 1, 2020, '2020-01-20', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-20', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(15, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 1, 2020, '2020-01-21', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-21', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(16, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 1, 2020, '2020-01-21', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-21', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(17, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 1, 2020, '2020-01-21', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-21', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(18, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 1, 2020, '2020-01-21', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-21', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(19, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 1, 2020, '2020-01-21', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-21', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(20, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 1, 2020, '2020-01-22', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-22', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(21, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 1, 2020, '2020-01-22', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-22', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(22, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 1, 2020, '2020-01-22', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-22', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(23, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 1, 2020, '2020-01-22', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-22', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(24, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 1, 2020, '2020-01-22', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-22', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(25, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 23, 1, 2020, '2020-01-23', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-23', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(26, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 23, 1, 2020, '2020-01-23', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-23', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(27, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 23, 1, 2020, '2020-01-23', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-23', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(28, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 23, 1, 2020, '2020-01-23', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-23', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(29, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 23, 1, 2020, '2020-01-23', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-23', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(30, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 1, 2020, '2020-01-24', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-24', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(31, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 1, 2020, '2020-01-24', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-24', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(32, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 1, 2020, '2020-01-24', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-24', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(33, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 1, 2020, '2020-01-24', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-24', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(34, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 1, 2020, '2020-01-24', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-24', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(35, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 1, 2020, '2020-01-25', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-25', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(36, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 1, 2020, '2020-01-25', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-25', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(37, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 1, 2020, '2020-01-25', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-25', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(38, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 1, 2020, '2020-01-25', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-25', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(39, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 1, 2020, '2020-01-25', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-25', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(40, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 1, 2020, '2020-01-26', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-26', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(41, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 1, 2020, '2020-01-26', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-26', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(42, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 1, 2020, '2020-01-26', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-26', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(43, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 1, 2020, '2020-01-26', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-26', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(44, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 1, 2020, '2020-01-26', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-26', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(45, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 1, 2020, '2020-01-27', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-27', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(46, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 1, 2020, '2020-01-27', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-27', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(47, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 1, 2020, '2020-01-27', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-27', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(48, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 1, 2020, '2020-01-27', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-27', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(49, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 1, 2020, '2020-01-27', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-27', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(50, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 28, 1, 2020, '2020-01-28', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-28', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(51, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 28, 1, 2020, '2020-01-28', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-28', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(52, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 28, 1, 2020, '2020-01-28', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-28', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(53, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 28, 1, 2020, '2020-01-28', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-28', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(54, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 28, 1, 2020, '2020-01-28', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-28', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(55, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 29, 1, 2020, '2020-01-29', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-29', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(56, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 29, 1, 2020, '2020-01-29', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-29', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(57, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 29, 1, 2020, '2020-01-29', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-29', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(58, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 29, 1, 2020, '2020-01-29', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-29', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(59, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 29, 1, 2020, '2020-01-29', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-29', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(60, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 30, 1, 2020, '2020-01-30', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-30', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(61, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 30, 1, 2020, '2020-01-30', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-30', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(62, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 30, 1, 2020, '2020-01-30', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-30', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(63, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 30, 1, 2020, '2020-01-30', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-30', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(64, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 30, 1, 2020, '2020-01-30', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-30', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(65, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 31, 1, 2020, '2020-01-31', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-31', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(66, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 31, 1, 2020, '2020-01-31', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-31', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(67, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 31, 1, 2020, '2020-01-31', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-31', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(68, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 31, 1, 2020, '2020-01-31', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-31', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(69, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 31, 1, 2020, '2020-01-31', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-01-31', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_transaction_2020_2`
--

CREATE TABLE `oc_transaction_2020_2` (
  `transaction_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(255) NOT NULL,
  `shift_intime` time NOT NULL,
  `shift_outtime` time NOT NULL,
  `act_intime` time NOT NULL,
  `act_outtime` time NOT NULL,
  `weekly_off` int(11) NOT NULL,
  `holiday_id` int(11) NOT NULL,
  `late_time` time NOT NULL,
  `early_time` time NOT NULL,
  `working_time` time NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `firsthalf_status` varchar(255) NOT NULL,
  `secondhalf_status` varchar(255) NOT NULL,
  `manual_status` int(11) NOT NULL,
  `day_close_status` int(11) NOT NULL,
  `halfday_status` int(11) NOT NULL,
  `leave_status` float NOT NULL,
  `present_status` float NOT NULL,
  `absent_status` float NOT NULL,
  `abnormal_status` int(11) NOT NULL,
  `date_out` date NOT NULL,
  `on_duty` int(1) NOT NULL,
  `compli_status` int(11) NOT NULL,
  `month_close_status` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `late_mark` int(11) NOT NULL,
  `early_mark` int(11) NOT NULL,
  `division` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `shift_change` int(11) NOT NULL,
  `shift_free_change` int(11) NOT NULL,
  `act_intime_change` int(11) NOT NULL,
  `act_outtime_change` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_transaction_2020_2`
--

INSERT INTO `oc_transaction_2020_2` (`transaction_id`, `emp_id`, `emp_name`, `shift_intime`, `shift_outtime`, `act_intime`, `act_outtime`, `weekly_off`, `holiday_id`, `late_time`, `early_time`, `working_time`, `day`, `month`, `year`, `date`, `status`, `department`, `unit`, `group`, `firsthalf_status`, `secondhalf_status`, `manual_status`, `day_close_status`, `halfday_status`, `leave_status`, `present_status`, `absent_status`, `abnormal_status`, `date_out`, `on_duty`, `compli_status`, `month_close_status`, `device_id`, `batch_id`, `late_mark`, `early_mark`, `division`, `region`, `department_id`, `division_id`, `unit_id`, `region_id`, `shift_change`, `shift_free_change`, `act_intime_change`, `act_outtime_change`, `company`, `company_id`) VALUES
(269, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 2, 2020, '2020-02-01', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-01', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(270, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 2, 2020, '2020-02-01', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-01', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(271, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 2, 2020, '2020-02-01', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-01', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(272, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 2, 2020, '2020-02-01', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-01', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(273, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 2, 2020, '2020-02-01', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-01', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(274, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 2, 2020, '2020-02-02', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-02', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(275, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 2, 2, 2020, '2020-02-02', 0, 'ADMIN - GENERAL', 'BWSP', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-02', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(276, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 2, 2, 2020, '2020-02-02', 0, 'ADMIN - FACILITY', 'CUBG', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-02', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(277, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 2, 2, 2020, '2020-02-02', 0, 'ADMIN - FACILITY', 'CHAR', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-02', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(278, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 2, 2, 2020, '2020-02-02', 0, 'ADMIN - FACILITY', 'DLOF', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-02', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(279, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 2, 2020, '2020-02-03', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-03', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(280, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 2, 2020, '2020-02-03', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-03', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(281, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 2, 2020, '2020-02-03', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-03', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(282, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 2, 2020, '2020-02-03', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-03', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(283, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 2, 2020, '2020-02-03', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-03', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(284, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 2, 2020, '2020-02-04', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-04', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(285, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 2, 2020, '2020-02-04', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-04', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(286, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 2, 2020, '2020-02-04', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-04', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(287, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 2, 2020, '2020-02-04', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-04', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(288, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 2, 2020, '2020-02-04', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-04', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(289, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 2, 2020, '2020-02-05', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-05', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(290, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 2, 2020, '2020-02-05', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-05', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(291, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 2, 2020, '2020-02-05', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-05', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(292, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 2, 2020, '2020-02-05', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-05', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(293, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 2, 2020, '2020-02-05', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-05', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(294, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 2, 2020, '2020-02-06', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-06', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(295, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 2, 2020, '2020-02-06', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-06', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(296, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 2, 2020, '2020-02-06', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-06', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(297, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 2, 2020, '2020-02-06', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-06', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(298, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 2, 2020, '2020-02-06', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-06', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(299, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 2, 2020, '2020-02-07', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-07', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(300, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 2, 2020, '2020-02-07', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-07', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(301, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 2, 2020, '2020-02-07', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-07', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(302, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 2, 2020, '2020-02-07', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-07', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(303, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 2, 2020, '2020-02-07', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-07', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(304, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 2, 2020, '2020-02-08', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-08', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(305, 1000, 'Eric', '09:30:00', '14:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 2, 2020, '2020-02-08', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-08', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(306, 1001, 'Aaron', '09:30:00', '14:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 2, 2020, '2020-02-08', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-08', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(307, 1002, 'Mukesh Yadav', '09:30:00', '14:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 2, 2020, '2020-02-08', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-08', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(308, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 2, 2020, '2020-02-08', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-08', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(309, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 2, 2020, '2020-02-09', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-09', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(310, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 9, 2, 2020, '2020-02-09', 0, 'ADMIN - GENERAL', 'BWSP', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-09', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(311, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 9, 2, 2020, '2020-02-09', 0, 'ADMIN - FACILITY', 'CUBG', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-09', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(312, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 9, 2, 2020, '2020-02-09', 0, 'ADMIN - FACILITY', 'CHAR', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-09', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(313, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 9, 2, 2020, '2020-02-09', 0, 'ADMIN - FACILITY', 'DLOF', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-09', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(314, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 2, 2020, '2020-02-10', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-10', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(315, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 2, 2020, '2020-02-10', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-10', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(316, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 2, 2020, '2020-02-10', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-10', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(317, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 2, 2020, '2020-02-10', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-10', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(318, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 2, 2020, '2020-02-10', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-10', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(319, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 2, 2020, '2020-02-11', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-11', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(320, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 2, 2020, '2020-02-11', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-11', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(321, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 2, 2020, '2020-02-11', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-11', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(322, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 2, 2020, '2020-02-11', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-11', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(323, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 2, 2020, '2020-02-11', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-11', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(324, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 2, 2020, '2020-02-12', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-12', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(325, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 2, 2020, '2020-02-12', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-12', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(326, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 2, 2020, '2020-02-12', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-12', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(327, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 2, 2020, '2020-02-12', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-12', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(328, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 2, 2020, '2020-02-12', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-12', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(329, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 2, 2020, '2020-02-13', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-13', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(330, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 2, 2020, '2020-02-13', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-13', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(331, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 2, 2020, '2020-02-13', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-13', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(332, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 2, 2020, '2020-02-13', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-13', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(333, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 2, 2020, '2020-02-13', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-13', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(334, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 2, 2020, '2020-02-14', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-14', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(335, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 2, 2020, '2020-02-14', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-14', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(336, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 2, 2020, '2020-02-14', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-14', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(337, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 2, 2020, '2020-02-14', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-14', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(338, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 2, 2020, '2020-02-14', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-14', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(339, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 15, 2, 2020, '2020-02-15', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-15', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(340, 1000, 'Eric', '09:30:00', '14:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 15, 2, 2020, '2020-02-15', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-15', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(341, 1001, 'Aaron', '09:30:00', '14:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 15, 2, 2020, '2020-02-15', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-15', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(342, 1002, 'Mukesh Yadav', '09:30:00', '14:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 15, 2, 2020, '2020-02-15', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-15', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(343, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 15, 2, 2020, '2020-02-15', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-15', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(344, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 16, 2, 2020, '2020-02-16', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-16', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(345, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 16, 2, 2020, '2020-02-16', 0, 'ADMIN - GENERAL', 'BWSP', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-16', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(346, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 16, 2, 2020, '2020-02-16', 0, 'ADMIN - FACILITY', 'CUBG', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-16', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(347, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 16, 2, 2020, '2020-02-16', 0, 'ADMIN - FACILITY', 'CHAR', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-16', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(348, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 16, 2, 2020, '2020-02-16', 0, 'ADMIN - FACILITY', 'DLOF', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-16', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(349, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 2, 2020, '2020-02-17', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-17', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(350, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 2, 2020, '2020-02-17', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-17', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(351, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 2, 2020, '2020-02-17', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-17', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(352, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 2, 2020, '2020-02-17', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-17', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(353, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 2, 2020, '2020-02-17', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-17', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(354, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 2, 2020, '2020-02-18', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-18', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(355, 1000, 'Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 2, 2020, '2020-02-18', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-18', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(356, 1001, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 2, 2020, '2020-02-18', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-18', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(357, 1002, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 2, 2020, '2020-02-18', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-18', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(358, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 2, 2020, '2020-02-18', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-18', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(359, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 2, 2020, '2020-02-19', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-19', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(360, 1000, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 2, 2020, '2020-02-19', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-19', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(361, 1001, ' Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 2, 2020, '2020-02-19', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-19', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(362, 1002, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 2, 2020, '2020-02-19', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-19', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(363, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 2, 2020, '2020-02-19', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-19', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(364, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 2, 2020, '2020-02-20', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-20', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(365, 1000, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 2, 2020, '2020-02-20', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-20', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(366, 1001, ' Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 2, 2020, '2020-02-20', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-20', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(367, 1002, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 2, 2020, '2020-02-20', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-20', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(368, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 2, 2020, '2020-02-20', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-20', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(369, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 2, 2020, '2020-02-21', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-21', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(370, 1000, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 2, 2020, '2020-02-21', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-21', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(371, 1001, ' Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 2, 2020, '2020-02-21', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-21', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(372, 1002, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 2, 2020, '2020-02-21', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-21', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(373, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 2, 2020, '2020-02-21', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-21', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(374, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 2, 2020, '2020-02-22', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-22', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(375, 1000, 'Mukesh Yadav', '09:30:00', '14:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 2, 2020, '2020-02-22', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-22', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(376, 1001, ' Eric', '09:30:00', '14:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 2, 2020, '2020-02-22', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-22', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(377, 1002, 'Aaron', '09:30:00', '14:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 2, 2020, '2020-02-22', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-22', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(378, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 22, 2, 2020, '2020-02-22', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-22', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(379, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 23, 2, 2020, '2020-02-23', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-23', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(380, 1000, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 23, 2, 2020, '2020-02-23', 0, 'ADMIN - GENERAL', 'BWSP', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-23', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(381, 1001, ' Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 23, 2, 2020, '2020-02-23', 0, 'ADMIN - FACILITY', 'CUBG', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-23', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(382, 1002, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 23, 2, 2020, '2020-02-23', 0, 'ADMIN - FACILITY', 'CHAR', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-23', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(383, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 23, 2, 2020, '2020-02-23', 0, 'ADMIN - FACILITY', 'DLOF', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-02-23', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(384, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 2, 2020, '2020-02-24', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-24', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(385, 1000, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 2, 2020, '2020-02-24', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-24', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(386, 1001, ' Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 2, 2020, '2020-02-24', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-24', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(387, 1002, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 2, 2020, '2020-02-24', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-24', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(388, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 24, 2, 2020, '2020-02-24', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-24', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(389, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 2, 2020, '2020-02-25', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-25', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(390, 1000, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 2, 2020, '2020-02-25', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-25', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(391, 1001, ' Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 2, 2020, '2020-02-25', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-25', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(392, 1002, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 2, 2020, '2020-02-25', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-25', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(393, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 25, 2, 2020, '2020-02-25', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-25', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(394, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 2, 2020, '2020-02-26', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-26', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(395, 1000, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 2, 2020, '2020-02-26', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-26', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(396, 1001, ' Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 2, 2020, '2020-02-26', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-26', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(397, 1002, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 2, 2020, '2020-02-26', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-26', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(398, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 26, 2, 2020, '2020-02-26', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-26', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(399, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 2, 2020, '2020-02-27', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-27', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(400, 1000, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 2, 2020, '2020-02-27', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-27', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(401, 1001, ' Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 2, 2020, '2020-02-27', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-27', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(402, 1002, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 2, 2020, '2020-02-27', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-27', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(403, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 27, 2, 2020, '2020-02-27', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-27', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(404, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 28, 2, 2020, '2020-02-28', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-28', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(405, 1000, 'Mukesh Yadav', '09:30:00', '18:30:00', '07:00:00', '19:23:34', 0, 0, '00:00:00', '00:00:00', '10:23:34', 28, 2, 2020, '2020-02-28', 0, 'ADMIN - GENERAL', 'BWSP', '', '1', '1', 0, 0, 0, 0, 1, 0, 0, '2020-02-28', 0, 0, 0, 1, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(406, 1001, ' Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 28, 2, 2020, '2020-02-28', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-28', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(407, 1002, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 28, 2, 2020, '2020-02-28', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-28', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(408, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 28, 2, 2020, '2020-02-28', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-28', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(409, 8439, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 29, 2, 2020, '2020-02-29', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-02-29', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(410, 1000, 'Mukesh Yadav', '09:30:00', '14:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 29, 2, 2020, '2020-02-29', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(411, 1001, ' Eric', '09:30:00', '14:00:00', '07:00:00', '19:23:34', 0, 0, '00:00:00', '00:00:00', '10:23:34', 29, 2, 2020, '2020-02-29', 0, 'ADMIN - FACILITY', 'CUBG', '', '1', '1', 0, 0, 0, 0, 1, 0, 0, '2020-02-29', 0, 0, 0, 1, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(412, 1002, 'Aaron', '09:30:00', '14:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 29, 2, 2020, '2020-02-29', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(413, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 29, 2, 2020, '2020-02-29', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_transaction_2020_3`
--

CREATE TABLE `oc_transaction_2020_3` (
  `transaction_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(255) NOT NULL,
  `shift_intime` time NOT NULL,
  `shift_outtime` time NOT NULL,
  `act_intime` time NOT NULL,
  `act_outtime` time NOT NULL,
  `weekly_off` int(11) NOT NULL,
  `holiday_id` int(11) NOT NULL,
  `late_time` time NOT NULL,
  `early_time` time NOT NULL,
  `working_time` time NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `firsthalf_status` varchar(255) NOT NULL,
  `secondhalf_status` varchar(255) NOT NULL,
  `manual_status` int(11) NOT NULL,
  `day_close_status` int(11) NOT NULL,
  `halfday_status` int(11) NOT NULL,
  `leave_status` float NOT NULL,
  `present_status` float NOT NULL,
  `absent_status` float NOT NULL,
  `abnormal_status` int(11) NOT NULL,
  `date_out` date NOT NULL,
  `on_duty` int(1) NOT NULL,
  `compli_status` int(11) NOT NULL,
  `month_close_status` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `late_mark` int(11) NOT NULL,
  `early_mark` int(11) NOT NULL,
  `division` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `shift_change` int(11) NOT NULL,
  `shift_free_change` int(11) NOT NULL,
  `act_intime_change` int(11) NOT NULL,
  `act_outtime_change` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_transaction_2020_3`
--

INSERT INTO `oc_transaction_2020_3` (`transaction_id`, `emp_id`, `emp_name`, `shift_intime`, `shift_outtime`, `act_intime`, `act_outtime`, `weekly_off`, `holiday_id`, `late_time`, `early_time`, `working_time`, `day`, `month`, `year`, `date`, `status`, `department`, `unit`, `group`, `firsthalf_status`, `secondhalf_status`, `manual_status`, `day_close_status`, `halfday_status`, `leave_status`, `present_status`, `absent_status`, `abnormal_status`, `date_out`, `on_duty`, `compli_status`, `month_close_status`, `device_id`, `batch_id`, `late_mark`, `early_mark`, `division`, `region`, `department_id`, `division_id`, `unit_id`, `region_id`, `shift_change`, `shift_free_change`, `act_intime_change`, `act_outtime_change`, `company`, `company_id`) VALUES
(613, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 1, 3, 2020, '2020-03-01', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-01', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(614, 1000, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '18:00:00', 1, 0, '00:00:00', '00:00:00', '09:00:00', 1, 3, 2020, '2020-03-01', 0, 'ADMIN - GENERAL', 'BWSP', '', 'WO', 'WO', 0, 0, 0, 0, 1, 0, 0, '2020-03-01', 0, 0, 0, 1, 1, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(615, 1001, ' Eric', '09:30:00', '18:30:00', '10:00:00', '19:23:34', 1, 0, '00:00:00', '00:00:00', '09:23:34', 1, 3, 2020, '2020-03-01', 0, 'ADMIN - FACILITY', 'CUBG', '', 'WO', 'WO', 0, 0, 0, 0, 1, 0, 0, '2020-03-01', 0, 0, 0, 1, 1, 1, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(616, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 1, 3, 2020, '2020-03-01', 0, 'ADMIN - FACILITY', 'CHAR', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(617, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '13:00:00', '16:00:00', 1, 0, '05:00:00', '04:00:00', '03:00:00', 1, 3, 2020, '2020-03-01', 0, 'ADMIN - FACILITY', 'DLOF', '', 'WO', 'WO', 0, 0, 0, 0, 0, 1, 0, '2020-03-01', 0, 0, 0, 4, 1, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(618, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 3, 2020, '2020-03-02', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-02', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(619, 1000, 'Mukesh Yadav', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 3, 2020, '2020-03-02', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-02', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(620, 1001, ' Eric', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 3, 2020, '2020-03-02', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-02', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(621, 1002, 'Aaron', '09:30:00', '18:30:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 3, 2020, '2020-03-02', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-02', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(622, 1003, 'Manish Kanojiya', '08:00:00', '20:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 2, 3, 2020, '2020-03-02', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-02', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(623, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 3, 2020, '2020-03-03', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-03', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(624, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 3, 2020, '2020-03-03', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(625, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 3, 2020, '2020-03-03', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(626, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 3, 2020, '2020-03-03', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(627, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 3, 3, 2020, '2020-03-03', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(628, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 3, 2020, '2020-03-04', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-04', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(629, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 3, 2020, '2020-03-04', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-04', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(630, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 3, 2020, '2020-03-04', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-04', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(631, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 3, 2020, '2020-03-04', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-04', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(632, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 4, 3, 2020, '2020-03-04', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-04', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(633, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 3, 2020, '2020-03-05', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-05', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(634, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 3, 2020, '2020-03-05', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-05', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(635, 1001, ' Eric', '09:30:00', '18:30:00', '10:17:00', '22:04:00', 0, 0, '00:17:00', '00:00:00', '11:47:00', 5, 3, 2020, '2020-03-05', 0, 'ADMIN - FACILITY', 'CUBG', '', '1', '1', 0, 0, 0, 0, 1, 0, 0, '2020-03-05', 0, 0, 0, 3, 1, 1, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(636, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 3, 2020, '2020-03-05', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-05', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(637, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 5, 3, 2020, '2020-03-05', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-05', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(638, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 3, 2020, '2020-03-06', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-06', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(639, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 3, 2020, '2020-03-06', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-06', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(640, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 3, 2020, '2020-03-06', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-06', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(641, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 3, 2020, '2020-03-06', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-06', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(642, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 6, 3, 2020, '2020-03-06', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-06', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(643, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 3, 2020, '2020-03-07', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-07', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(644, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 3, 2020, '2020-03-07', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-07', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(645, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 3, 2020, '2020-03-07', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-07', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(646, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 3, 2020, '2020-03-07', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-07', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(647, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 7, 3, 2020, '2020-03-07', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-07', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(648, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 8, 3, 2020, '2020-03-08', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-08', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(649, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 8, 3, 2020, '2020-03-08', 0, 'ADMIN - GENERAL', 'BWSP', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-03-08', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(650, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 8, 3, 2020, '2020-03-08', 0, 'ADMIN - FACILITY', 'CUBG', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-03-08', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(651, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 8, 3, 2020, '2020-03-08', 0, 'ADMIN - FACILITY', 'CHAR', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-03-08', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(652, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 8, 3, 2020, '2020-03-08', 0, 'ADMIN - FACILITY', 'DLOF', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-03-08', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(653, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 3, 2020, '2020-03-09', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-09', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(654, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 3, 2020, '2020-03-09', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-09', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(655, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 3, 2020, '2020-03-09', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-09', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(656, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 3, 2020, '2020-03-09', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-09', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(657, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 9, 3, 2020, '2020-03-09', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-09', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(658, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 3, 2020, '2020-03-10', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-10', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(659, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 3, 2020, '2020-03-10', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-10', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(660, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 3, 2020, '2020-03-10', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-10', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(661, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 3, 2020, '2020-03-10', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-10', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(662, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 10, 3, 2020, '2020-03-10', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-10', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(663, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 3, 2020, '2020-03-11', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-11', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(664, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 3, 2020, '2020-03-11', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-11', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(665, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 3, 2020, '2020-03-11', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-11', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(666, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 3, 2020, '2020-03-11', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-11', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(667, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 11, 3, 2020, '2020-03-11', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-11', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(668, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 3, 2020, '2020-03-12', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-12', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(669, 1000, 'Mukesh Yadav', '09:30:00', '18:30:00', '08:00:00', '22:00:00', 0, 0, '00:00:00', '00:00:00', '13:00:00', 12, 3, 2020, '2020-03-12', 0, 'ADMIN - GENERAL', 'BWSP', '', '1', '1', 0, 0, 0, 0, 1, 0, 0, '2020-03-12', 0, 0, 0, 43, 1, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(670, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 3, 2020, '2020-03-12', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-12', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(671, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 3, 2020, '2020-03-12', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-12', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(672, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 12, 3, 2020, '2020-03-12', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-12', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(673, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 3, 2020, '2020-03-13', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-13', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(674, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 3, 2020, '2020-03-13', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-13', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(675, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 3, 2020, '2020-03-13', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-13', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(676, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 3, 2020, '2020-03-13', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-13', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(677, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 13, 3, 2020, '2020-03-13', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-13', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(678, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 3, 2020, '2020-03-14', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-14', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(679, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 3, 2020, '2020-03-14', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-14', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(680, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 3, 2020, '2020-03-14', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-14', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(681, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 3, 2020, '2020-03-14', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-14', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(682, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 14, 3, 2020, '2020-03-14', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-14', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(683, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 15, 3, 2020, '2020-03-15', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-15', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(684, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 15, 3, 2020, '2020-03-15', 0, 'ADMIN - GENERAL', 'BWSP', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-03-15', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(685, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 15, 3, 2020, '2020-03-15', 0, 'ADMIN - FACILITY', 'CUBG', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-03-15', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(686, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 15, 3, 2020, '2020-03-15', 0, 'ADMIN - FACILITY', 'CHAR', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-03-15', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(687, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 1, 0, '00:00:00', '00:00:00', '00:00:00', 15, 3, 2020, '2020-03-15', 0, 'ADMIN - FACILITY', 'DLOF', '', 'WO', 'WO', 0, 0, 0, 0, 0, 0, 0, '2020-03-15', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(688, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 16, 3, 2020, '2020-03-16', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-16', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(689, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 16, 3, 2020, '2020-03-16', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-16', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(690, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 16, 3, 2020, '2020-03-16', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-16', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(691, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 16, 3, 2020, '2020-03-16', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-16', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(692, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 16, 3, 2020, '2020-03-16', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-16', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(693, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 3, 2020, '2020-03-17', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-17', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(694, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 3, 2020, '2020-03-17', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-17', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(695, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 3, 2020, '2020-03-17', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-17', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(696, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 3, 2020, '2020-03-17', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-17', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(697, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 17, 3, 2020, '2020-03-17', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-17', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(698, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 3, 2020, '2020-03-18', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-18', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(699, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 3, 2020, '2020-03-18', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-18', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(700, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 3, 2020, '2020-03-18', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-18', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(701, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 3, 2020, '2020-03-18', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-18', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(702, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 18, 3, 2020, '2020-03-18', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-18', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(703, 8439, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 3, 2020, '2020-03-19', 0, 'ADMIN - FACILITY', 'BBFL', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-19', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 22, 2, 50, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(704, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 3, 2020, '2020-03-19', 0, 'ADMIN - GENERAL', 'BWSP', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-19', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 23, 3, 66, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(705, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 3, 2020, '2020-03-19', 0, 'ADMIN - FACILITY', 'CUBG', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-19', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 10, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(706, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 3, 2020, '2020-03-19', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-19', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 22, 3, 28, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(707, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 19, 3, 2020, '2020-03-19', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-19', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(708, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 3, 2020, '2020-03-20', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-20', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2),
(709, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 3, 2020, '2020-03-20', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 28, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(710, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 3, 2020, '2020-03-20', 0, 'ADMIN - GENERAL', 'JMCH - AHD', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 23, 2, 2, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(711, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 20, 3, 2020, '2020-03-20', 0, 'ADMIN - SECURITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 25, 3, 17, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(712, 1000, 'Mukesh Yadav', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 3, 2020, '2020-03-21', 0, 'ADMIN - FACILITY', 'CHAR', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-21', 0, 0, 0, 0, 0, 0, 0, 'I & P', 'I & P', 22, 5, 28, 5, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(713, 1001, ' Eric', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 3, 2020, '2020-03-21', 0, 'ADMIN - GENERAL', 'JMCH - AHD', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-21', 0, 0, 0, 0, 0, 0, 0, 'HO', 'HO', 23, 2, 2, 2, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(714, 1002, 'Aaron', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 3, 2020, '2020-03-21', 0, 'ADMIN - SECURITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-21', 0, 0, 0, 0, 0, 0, 0, 'INFRA', 'INFRA', 25, 3, 17, 3, 0, 0, 0, 0, 'JMC PROJECTS (I) LTD', 1),
(715, 1003, 'Manish Kanojiya', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 0, 0, '00:00:00', '00:00:00', '00:00:00', 21, 3, 2020, '2020-03-21', 0, 'ADMIN - FACILITY', 'DLOF', '', '0', '0', 0, 0, 0, 0, 0, 1, 0, '2020-03-21', 0, 0, 0, 0, 0, 0, 0, 'INTRNTL', 'INTRNTL', 22, 6, 17, 6, 0, 0, 0, 0, 'ASCENT LOCAL', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_transaction_2020_4`
--

CREATE TABLE `oc_transaction_2020_4` (
  `transaction_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(255) NOT NULL,
  `shift_intime` time NOT NULL,
  `shift_outtime` time NOT NULL,
  `act_intime` time NOT NULL,
  `act_outtime` time NOT NULL,
  `weekly_off` int(11) NOT NULL,
  `holiday_id` int(11) NOT NULL,
  `late_time` time NOT NULL,
  `early_time` time NOT NULL,
  `working_time` time NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `firsthalf_status` varchar(255) NOT NULL,
  `secondhalf_status` varchar(255) NOT NULL,
  `manual_status` int(11) NOT NULL,
  `day_close_status` int(11) NOT NULL,
  `halfday_status` int(11) NOT NULL,
  `leave_status` float NOT NULL,
  `present_status` float NOT NULL,
  `absent_status` float NOT NULL,
  `abnormal_status` int(11) NOT NULL,
  `date_out` date NOT NULL,
  `on_duty` int(1) NOT NULL,
  `compli_status` int(11) NOT NULL,
  `month_close_status` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `late_mark` int(11) NOT NULL,
  `early_mark` int(11) NOT NULL,
  `division` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `shift_change` int(11) NOT NULL,
  `shift_free_change` int(11) NOT NULL,
  `act_intime_change` int(11) NOT NULL,
  `act_outtime_change` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_transaction_2020_5`
--

CREATE TABLE `oc_transaction_2020_5` (
  `transaction_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(255) NOT NULL,
  `shift_intime` time NOT NULL,
  `shift_outtime` time NOT NULL,
  `act_intime` time NOT NULL,
  `act_outtime` time NOT NULL,
  `weekly_off` int(11) NOT NULL,
  `holiday_id` int(11) NOT NULL,
  `late_time` time NOT NULL,
  `early_time` time NOT NULL,
  `working_time` time NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `firsthalf_status` varchar(255) NOT NULL,
  `secondhalf_status` varchar(255) NOT NULL,
  `manual_status` int(11) NOT NULL,
  `day_close_status` int(11) NOT NULL,
  `halfday_status` int(11) NOT NULL,
  `leave_status` float NOT NULL,
  `present_status` float NOT NULL,
  `absent_status` float NOT NULL,
  `abnormal_status` int(11) NOT NULL,
  `date_out` date NOT NULL,
  `on_duty` int(1) NOT NULL,
  `compli_status` int(11) NOT NULL,
  `month_close_status` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `late_mark` int(11) NOT NULL,
  `early_mark` int(11) NOT NULL,
  `division` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `shift_change` int(11) NOT NULL,
  `shift_free_change` int(11) NOT NULL,
  `act_intime_change` int(11) NOT NULL,
  `act_outtime_change` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_travel_sheet`
--

CREATE TABLE `oc_travel_sheet` (
  `medicine_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_unit`
--

CREATE TABLE `oc_unit` (
  `unit_id` int(11) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `unit_code` varchar(255) NOT NULL,
  `division_id` int(11) NOT NULL,
  `division_name` varchar(255) NOT NULL,
  `state_id` int(11) NOT NULL,
  `state_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oc_unit`
--

INSERT INTO `oc_unit` (`unit_id`, `unit`, `unit_code`, `division_id`, `division_name`, `state_id`, `state_name`) VALUES
(1, 'AIHB', 'AIHB', 1, 'NIO', 1, 'MAHARASHTRA'),
(2, 'AHRB', 'AHRB', 1, 'NIO', 1, 'MAHARASHTRA'),
(3, 'AIH1', 'AIH1', 1, 'NIO', 1, 'MAHARASHTRA'),
(4, 'AIHR', 'AIHR', 1, 'NIO', 1, 'MAHARASHTRA'),
(5, 'DLOF', 'DLOF', 1, 'NIO', 1, 'MAHARASHTRA'),
(6, 'EMGG', 'EMGG', 1, 'NIO', 1, 'MAHARASHTRA'),
(7, 'JPG', 'JPG', 1, 'NIO', 1, 'MAHARASHTRA'),
(8, 'JPGH', 'JPGH', 1, 'NIO', 1, 'MAHARASHTRA'),
(9, 'JPKI', 'JPKI', 1, 'NIO', 1, 'MAHARASHTRA'),
(10, 'JPNB', 'JPNB', 1, 'NIO', 1, 'MAHARASHTRA'),
(11, 'JPTH', 'JPTH', 1, 'NIO', 1, 'MAHARASHTRA'),
(12, 'KMKH', 'KMKH', 1, 'NIO', 1, 'MAHARASHTRA'),
(13, 'KVNN', 'KVNN', 1, 'NIO', 1, 'MAHARASHTRA'),
(14, 'MMHP', 'MMHP', 1, 'NIO', 1, 'MAHARASHTRA'),
(15, 'NLDA', 'NLDA', 5, 'I & P', 1, 'MAHARASHTRA'),
(16, 'PEMD', 'PEMD', 1, 'NIO', 1, 'MAHARASHTRA'),
(17, 'PHPG', 'PHPG', 1, 'NIO', 1, 'MAHARASHTRA'),
(18, 'RECG', 'RECG', 1, 'NIO', 1, 'MAHARASHTRA'),
(19, 'RSNO', 'RSNO', 1, 'NIO', 1, 'MAHARASHTRA'),
(20, 'SCAO', 'SCAO', 1, 'NIO', 1, 'MAHARASHTRA'),
(21, 'THGG', 'THGG', 1, 'NIO', 1, 'MAHARASHTRA'),
(22, 'THLV', 'THLV', 1, 'NIO', 1, 'MAHARASHTRA'),
(23, 'TAGP', 'TAGP', 6, 'WIO', 1, 'MAHARASHTRA'),
(24, 'PUNE', 'PUNE', 6, 'WIO', 1, 'MAHARASHTRA'),
(25, 'NIRP', 'NIRP', 6, 'WIO', 1, 'MAHARASHTRA'),
(26, 'MLD2', 'MLD2', 6, 'WIO', 1, 'MAHARASHTRA'),
(27, 'KLRD', 'KLRD', 6, 'WIO', 1, 'MAHARASHTRA'),
(28, 'KLIP', 'KLIP', 6, 'WIO', 1, 'MAHARASHTRA'),
(29, 'IIPH', 'IIPH', 6, 'WIO', 1, 'MAHARASHTRA'),
(30, 'ATTP', 'ATTP', 6, 'WIO', 1, 'MAHARASHTRA'),
(31, 'ATP2', 'ATP2', 6, 'WIO', 1, 'MAHARASHTRA'),
(32, 'VTRF', 'VTRF', 5, 'I & P', 1, 'MAHARASHTRA'),
(33, 'VMRF', 'VMRF', 5, 'I & P', 1, 'MAHARASHTRA'),
(34, 'TSL2', 'TSL2', 5, 'I & P', 1, 'MAHARASHTRA'),
(35, 'TSL1', 'TSL1', 5, 'I & P', 1, 'MAHARASHTRA'),
(36, 'TPPR', 'TPPR', 5, 'I & P', 1, 'MAHARASHTRA'),
(37, 'STP1', 'STP1', 5, 'I & P', 1, 'MAHARASHTRA'),
(38, 'SKSP', 'SKSP', 5, 'I & P', 1, 'MAHARASHTRA'),
(39, 'NTPC', 'NTPC', 5, 'I & P', 1, 'MAHARASHTRA'),
(40, 'NMDC', 'NMDC', 5, 'I & P', 1, 'MAHARASHTRA'),
(41, 'MECN', 'MECN', 5, 'I & P', 1, 'MAHARASHTRA'),
(42, 'KRGN', 'KRGN', 5, 'I & P', 1, 'MAHARASHTRA'),
(43, 'GSL1', 'GSL1', 5, 'I & P', 1, 'MAHARASHTRA'),
(44, 'ERSK', 'ERSK', 5, 'I & P', 1, 'MAHARASHTRA'),
(45, 'CUBG', 'CUBG', 5, 'I & P', 1, 'MAHARASHTRA'),
(46, 'BSEB', 'BSEB', 5, 'I & P', 1, 'MAHARASHTRA'),
(47, 'OZAP', 'OZAP', 6, 'WIO', 1, 'MAHARASHTRA'),
(48, 'MGLF', 'MGLF', 4, 'INFRA', 1, 'MAHARASHTRA'),
(49, 'MGLFCASTINGYARD', 'MGLFCASTINGYARD', 4, 'INFRA', 1, 'MAHARASHTRA'),
(50, 'BHFS', 'BHFS', 4, 'INFRA', 1, 'MAHARASHTRA'),
(51, 'LTFO', 'LTFO', 4, 'INFRA', 1, 'MAHARASHTRA'),
(52, 'STFO', 'STFO', 4, 'INFRA', 1, 'MAHARASHTRA'),
(53, 'MCFO', 'MCFO', 4, 'INFRA', 1, 'MAHARASHTRA'),
(54, 'NRDA', 'NRDA', 4, 'INFRA', 1, 'MAHARASHTRA'),
(55, 'BWSP', 'BWSP', 4, 'INFRA', 1, 'MAHARASHTRA'),
(56, 'MAIP', 'MAIP', 4, 'INFRA', 1, 'MAHARASHTRA'),
(57, 'WCPP', 'WCPP', 6, 'WIO', 1, 'MAHARASHTRA'),
(58, 'MBWS', 'MBWS', 4, 'INFRA', 1, 'MAHARASHTRA'),
(59, 'MJIP', 'MJIP', 4, 'INFRA', 1, 'MAHARASHTRA'),
(60, 'HCCB', 'HCCB', 5, 'I & P', 1, 'MAHARASHTRA'),
(61, 'NIRP', 'NIRP', 6, 'WIO', 1, 'MAHARASHTRA'),
(62, 'OPRC', 'OPRC', 8, 'INTRNL', 1, 'MAHARASHTRA'),
(63, 'HDFC', 'HDFC', 5, 'I & P', 1, 'MAHARASHTRA'),
(64, 'EMDH', 'EMDH', 1, 'NIO', 1, 'MAHARASHTRA'),
(65, 'PATP', 'PATP', 6, 'WIO', 1, 'MAHARASHTRA'),
(66, 'SNUD', 'SNUD', 1, 'NIO', 1, 'MAHARASHTRA'),
(67, 'PBGN', 'PBGN', 1, 'NIO', 1, 'MAHARASHTRA'),
(68, 'DGWS', 'DGWS', 4, 'INFRA', 1, 'MAHARASHTRA'),
(70, 'MSWS', 'MSWS', 4, 'INFRA', 1, 'MAHARASHTRA'),
(71, 'SKWS', 'SKWS', 4, 'INFRA', 1, 'MAHARASHTRA'),
(72, 'DAEG', 'DAEG', 1, 'NIO', 1, 'MAHARASHTRA'),
(73, 'BMWS', 'BMWS', 4, 'INFRA', 1, 'MAHARASHTRA'),
(74, 'MWSP', 'MWSP', 4, 'INFRA', 1, 'MAHARASHTRA'),
(75, 'MWSS', 'MWSS', 4, 'INFRA', 1, 'MAHARASHTRA'),
(76, 'EOBN', 'EOBN', 1, 'NIO', 1, 'MAHARASHTRA'),
(77, 'NLMK', 'NLMK', 5, 'I & P', 1, 'MAHARASHTRA'),
(78, 'DEWD', 'DEWD', 1, 'NIO', 1, 'MAHARASHTRA'),
(79, 'BBEPL', 'BBEPL', 9, 'SPV', 1, 'MAHARASHTRA'),
(80, 'VEPL', 'VEPL', 9, 'SPV', 1, 'MAHARASHTRA'),
(81, 'WEPL', 'WEPL', 9, 'SPV', 1, 'MAHARASHTRA'),
(86, 'SBUT', 'SBUT', 6, 'WIO', 1, 'MAHARASHTRA');

-- --------------------------------------------------------

--
-- Table structure for table `oc_url_alias`
--

CREATE TABLE `oc_url_alias` (
  `url_alias_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_user`
--

CREATE TABLE `oc_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_super` int(11) NOT NULL,
  `division` text NOT NULL,
  `site` text NOT NULL,
  `region` text NOT NULL,
  `device` text NOT NULL,
  `add` int(11) NOT NULL,
  `edit` int(11) NOT NULL,
  `view` int(11) NOT NULL,
  `company_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_user`
--

INSERT INTO `oc_user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `code`, `ip`, `status`, `date_added`, `is_super`, `division`, `site`, `region`, `device`, `add`, `edit`, `view`, `company_id`) VALUES
(1, 1, 'admin', 'b84def1a645e48ce2e8b57151ac7c59989d032af', 'dd2bbfb9f', 'admin', 'admin', 'fargose.aaron@gmail.com', '', '::1', 1, '2015-02-22 08:57:55', 0, '', '1', '', '', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_user_group`
--

CREATE TABLE `oc_user_group` (
  `user_group_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `uid`, `name`, `permission`, `company_id`) VALUES
(1, 9999, 'Top Administrator', 'a:4:{s:6:\"access\";a:37:{i:0;s:15:\"catalog/company\";i:1;s:14:\"catalog/region\";i:2;s:16:\"catalog/division\";i:3;s:13:\"catalog/state\";i:4;s:12:\"catalog/unit\";i:5;s:18:\"catalog/department\";i:6;s:19:\"catalog/designation\";i:7;s:13:\"catalog/grade\";i:8;s:19:\"catalog/employement\";i:9;s:16:\"catalog/employee\";i:10;s:23:\"catalog/employee_change\";i:11;s:21:\"catalog/employeelocal\";i:12;s:13:\"catalog/shift\";i:13;s:14:\"catalog/device\";i:14;s:18:\"catalog/in_process\";i:15;s:23:\"transaction/manualpunch\";i:16;s:12:\"report/today\";i:17;s:12:\"report/daily\";i:18;s:13:\"report/muster\";i:19;s:22:\"report/employeehistory\";i:20;s:14:\"tool/empimport\";i:21;s:16:\"tool/externalapi\";i:22;s:16:\"tool/recalculate\";i:23;s:11:\"tool/backup\";i:24;s:19:\"report/new_employee\";i:25;s:16:\"snippets/snippet\";i:26;s:18:\"snippets/snippet_1\";i:27;s:16:\"snippets/weekoff\";i:28;s:20:\"user/user_permission\";i:29;s:16:\"tool/recalculate\";i:30;s:9:\"user/user\";i:31;s:14:\"tool/error_log\";i:32;s:18:\"catalog/in_process\";i:33;s:11:\"common/home\";i:34;s:23:\"transaction/transaction\";i:35;s:38:\"transaction/transaction/generate_today\";i:36;s:20:\"snippets/yearprocess\";}s:3:\"add\";a:34:{i:0;s:15:\"catalog/company\";i:1;s:14:\"catalog/region\";i:2;s:16:\"catalog/division\";i:3;s:13:\"catalog/state\";i:4;s:12:\"catalog/unit\";i:5;s:18:\"catalog/department\";i:6;s:19:\"catalog/designation\";i:7;s:13:\"catalog/grade\";i:8;s:19:\"catalog/employement\";i:9;s:16:\"catalog/employee\";i:10;s:23:\"catalog/employee_change\";i:11;s:21:\"catalog/employeelocal\";i:12;s:13:\"catalog/shift\";i:13;s:14:\"catalog/device\";i:14;s:23:\"transaction/manualpunch\";i:15;s:12:\"report/today\";i:16;s:12:\"report/daily\";i:17;s:13:\"report/muster\";i:18;s:22:\"report/employeehistory\";i:19;s:14:\"tool/empimport\";i:20;s:16:\"tool/externalapi\";i:21;s:16:\"tool/recalculate\";i:22;s:11:\"tool/backup\";i:23;s:16:\"snippets/snippet\";i:24;s:18:\"snippets/snippet_1\";i:25;s:16:\"snippets/weekoff\";i:26;s:20:\"user/user_permission\";i:27;s:16:\"tool/recalculate\";i:28;s:9:\"user/user\";i:29;s:14:\"tool/error_log\";i:30;s:11:\"common/home\";i:31;s:23:\"transaction/transaction\";i:32;s:38:\"transaction/transaction/generate_today\";i:33;s:20:\"snippets/yearprocess\";}s:6:\"modify\";a:34:{i:0;s:15:\"catalog/company\";i:1;s:14:\"catalog/region\";i:2;s:16:\"catalog/division\";i:3;s:13:\"catalog/state\";i:4;s:12:\"catalog/unit\";i:5;s:18:\"catalog/department\";i:6;s:19:\"catalog/designation\";i:7;s:13:\"catalog/grade\";i:8;s:19:\"catalog/employement\";i:9;s:16:\"catalog/employee\";i:10;s:23:\"catalog/employee_change\";i:11;s:21:\"catalog/employeelocal\";i:12;s:13:\"catalog/shift\";i:13;s:14:\"catalog/device\";i:14;s:23:\"transaction/manualpunch\";i:15;s:12:\"report/today\";i:16;s:12:\"report/daily\";i:17;s:13:\"report/muster\";i:18;s:22:\"report/employeehistory\";i:19;s:14:\"tool/empimport\";i:20;s:16:\"tool/externalapi\";i:21;s:16:\"tool/recalculate\";i:22;s:11:\"tool/backup\";i:23;s:16:\"snippets/snippet\";i:24;s:18:\"snippets/snippet_1\";i:25;s:16:\"snippets/weekoff\";i:26;s:20:\"user/user_permission\";i:27;s:16:\"tool/recalculate\";i:28;s:9:\"user/user\";i:29;s:14:\"tool/error_log\";i:30;s:11:\"common/home\";i:31;s:23:\"transaction/transaction\";i:32;s:38:\"transaction/transaction/generate_today\";i:33;s:20:\"snippets/yearprocess\";}s:6:\"delete\";a:34:{i:0;s:15:\"catalog/company\";i:1;s:14:\"catalog/region\";i:2;s:16:\"catalog/division\";i:3;s:13:\"catalog/state\";i:4;s:12:\"catalog/unit\";i:5;s:18:\"catalog/department\";i:6;s:19:\"catalog/designation\";i:7;s:13:\"catalog/grade\";i:8;s:19:\"catalog/employement\";i:9;s:16:\"catalog/employee\";i:10;s:23:\"catalog/employee_change\";i:11;s:21:\"catalog/employeelocal\";i:12;s:13:\"catalog/shift\";i:13;s:14:\"catalog/device\";i:14;s:23:\"transaction/manualpunch\";i:15;s:12:\"report/today\";i:16;s:12:\"report/daily\";i:17;s:13:\"report/muster\";i:18;s:22:\"report/employeehistory\";i:19;s:14:\"tool/empimport\";i:20;s:16:\"tool/externalapi\";i:21;s:16:\"tool/recalculate\";i:22;s:11:\"tool/backup\";i:23;s:16:\"snippets/snippet\";i:24;s:18:\"snippets/snippet_1\";i:25;s:16:\"snippets/weekoff\";i:26;s:20:\"user/user_permission\";i:27;s:16:\"tool/recalculate\";i:28;s:9:\"user/user\";i:29;s:14:\"tool/error_log\";i:30;s:11:\"common/home\";i:31;s:23:\"transaction/transaction\";i:32;s:38:\"transaction/transaction/generate_today\";i:33;s:20:\"snippets/yearprocess\";}}', 1),
(10, 2, 'Division Admin', 'a:4:{s:6:\"access\";a:16:{i:0;s:16:\"catalog/employee\";i:1;s:23:\"catalog/employee_change\";i:2;s:14:\"catalog/device\";i:3;s:18:\"catalog/in_process\";i:4;s:12:\"report/today\";i:5;s:12:\"report/daily\";i:6;s:13:\"report/muster\";i:7;s:11:\"tool/backup\";i:8;s:19:\"report/new_employee\";i:9;s:9:\"user/user\";i:10;s:14:\"tool/error_log\";i:11;s:18:\"catalog/in_process\";i:12;s:11:\"common/home\";i:13;s:23:\"transaction/transaction\";i:14;s:38:\"transaction/transaction/generate_today\";i:15;s:20:\"snippets/yearprocess\";}s:3:\"add\";a:12:{i:0;s:23:\"catalog/employee_change\";i:1;s:12:\"report/today\";i:2;s:12:\"report/daily\";i:3;s:13:\"report/muster\";i:4;s:11:\"tool/backup\";i:5;s:19:\"report/new_employee\";i:6;s:9:\"user/user\";i:7;s:14:\"tool/error_log\";i:8;s:11:\"common/home\";i:9;s:23:\"transaction/transaction\";i:10;s:38:\"transaction/transaction/generate_today\";i:11;s:20:\"snippets/yearprocess\";}s:6:\"modify\";a:11:{i:0;s:23:\"catalog/employee_change\";i:1;s:12:\"report/today\";i:2;s:12:\"report/daily\";i:3;s:13:\"report/muster\";i:4;s:11:\"tool/backup\";i:5;s:9:\"user/user\";i:6;s:14:\"tool/error_log\";i:7;s:11:\"common/home\";i:8;s:23:\"transaction/transaction\";i:9;s:38:\"transaction/transaction/generate_today\";i:10;s:20:\"snippets/yearprocess\";}s:6:\"delete\";a:7:{i:0;s:11:\"tool/backup\";i:1;s:9:\"user/user\";i:2;s:14:\"tool/error_log\";i:3;s:11:\"common/home\";i:4;s:23:\"transaction/transaction\";i:5;s:38:\"transaction/transaction/generate_today\";i:6;s:20:\"snippets/yearprocess\";}}', 1),
(20, 3, 'Site Admin', 'a:4:{s:6:\"access\";a:22:{i:0;s:16:\"catalog/employee\";i:1;s:12:\"report/today\";i:2;s:12:\"report/daily\";i:3;s:11:\"report/late\";i:4;s:16:\"report/lateearly\";i:5;s:20:\"report/lateearlysumm\";i:6;s:14:\"report/working\";i:7;s:21:\"report/averageworking\";i:8;s:18:\"report/performance\";i:9;s:13:\"report/muster\";i:10;s:17:\"report/attendance\";i:11;s:22:\"report/employeehistory\";i:12;s:13:\"catalog/leave\";i:13;s:17:\"transaction/leave\";i:14;s:21:\"report/leave_register\";i:15;s:19:\"report/leave_report\";i:16;s:11:\"tool/backup\";i:17;s:9:\"user/user\";i:18;s:14:\"tool/error_log\";i:19;s:11:\"common/home\";i:20;s:23:\"transaction/transaction\";i:21;s:38:\"transaction/transaction/generate_today\";}s:3:\"add\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}s:6:\"modify\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}s:6:\"delete\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}}', 3),
(17, 2, 'Division Admin', 'a:4:{s:6:\"access\";a:27:{i:0;s:16:\"catalog/employee\";i:1;s:23:\"catalog/employee_change\";i:2;s:22:\"catalog/shift_schedule\";i:3;s:12:\"catalog/week\";i:4;s:12:\"report/today\";i:5;s:12:\"report/daily\";i:6;s:11:\"report/late\";i:7;s:16:\"report/lateearly\";i:8;s:20:\"report/lateearlysumm\";i:9;s:14:\"report/working\";i:10;s:21:\"report/averageworking\";i:11;s:18:\"report/performance\";i:12;s:13:\"report/muster\";i:13;s:17:\"report/attendance\";i:14;s:22:\"report/employeehistory\";i:15;s:19:\"catalog/leavemaster\";i:16;s:13:\"catalog/leave\";i:17;s:17:\"transaction/leave\";i:18;s:24:\"transaction/leaveprocess\";i:19;s:21:\"report/leave_register\";i:20;s:19:\"report/leave_report\";i:21;s:11:\"tool/backup\";i:22;s:9:\"user/user\";i:23;s:14:\"tool/error_log\";i:24;s:11:\"common/home\";i:25;s:23:\"transaction/transaction\";i:26;s:38:\"transaction/transaction/generate_today\";}s:3:\"add\";a:27:{i:0;s:16:\"catalog/employee\";i:1;s:23:\"catalog/employee_change\";i:2;s:22:\"catalog/shift_schedule\";i:3;s:12:\"catalog/week\";i:4;s:12:\"report/today\";i:5;s:12:\"report/daily\";i:6;s:11:\"report/late\";i:7;s:16:\"report/lateearly\";i:8;s:20:\"report/lateearlysumm\";i:9;s:14:\"report/working\";i:10;s:21:\"report/averageworking\";i:11;s:18:\"report/performance\";i:12;s:13:\"report/muster\";i:13;s:17:\"report/attendance\";i:14;s:22:\"report/employeehistory\";i:15;s:19:\"catalog/leavemaster\";i:16;s:13:\"catalog/leave\";i:17;s:17:\"transaction/leave\";i:18;s:24:\"transaction/leaveprocess\";i:19;s:21:\"report/leave_register\";i:20;s:19:\"report/leave_report\";i:21;s:11:\"tool/backup\";i:22;s:9:\"user/user\";i:23;s:14:\"tool/error_log\";i:24;s:11:\"common/home\";i:25;s:23:\"transaction/transaction\";i:26;s:38:\"transaction/transaction/generate_today\";}s:6:\"modify\";a:27:{i:0;s:16:\"catalog/employee\";i:1;s:23:\"catalog/employee_change\";i:2;s:22:\"catalog/shift_schedule\";i:3;s:12:\"catalog/week\";i:4;s:12:\"report/today\";i:5;s:12:\"report/daily\";i:6;s:11:\"report/late\";i:7;s:16:\"report/lateearly\";i:8;s:20:\"report/lateearlysumm\";i:9;s:14:\"report/working\";i:10;s:21:\"report/averageworking\";i:11;s:18:\"report/performance\";i:12;s:13:\"report/muster\";i:13;s:17:\"report/attendance\";i:14;s:22:\"report/employeehistory\";i:15;s:19:\"catalog/leavemaster\";i:16;s:13:\"catalog/leave\";i:17;s:17:\"transaction/leave\";i:18;s:24:\"transaction/leaveprocess\";i:19;s:21:\"report/leave_register\";i:20;s:19:\"report/leave_report\";i:21;s:11:\"tool/backup\";i:22;s:9:\"user/user\";i:23;s:14:\"tool/error_log\";i:24;s:11:\"common/home\";i:25;s:23:\"transaction/transaction\";i:26;s:38:\"transaction/transaction/generate_today\";}s:6:\"delete\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}}', 3),
(12, 3, 'Site Admin', 'a:4:{s:6:\"access\";a:19:{i:0;s:16:\"catalog/employee\";i:1;s:14:\"catalog/device\";i:2;s:12:\"report/today\";i:3;s:12:\"report/daily\";i:4;s:11:\"report/late\";i:5;s:16:\"report/lateearly\";i:6;s:20:\"report/lateearlysumm\";i:7;s:14:\"report/working\";i:8;s:21:\"report/averageworking\";i:9;s:18:\"report/performance\";i:10;s:13:\"report/muster\";i:11;s:17:\"report/attendance\";i:12;s:22:\"report/employeehistory\";i:13;s:11:\"tool/backup\";i:14;s:9:\"user/user\";i:15;s:14:\"tool/error_log\";i:16;s:11:\"common/home\";i:17;s:23:\"transaction/transaction\";i:18;s:38:\"transaction/transaction/generate_today\";}s:3:\"add\";a:16:{i:0;s:12:\"report/today\";i:1;s:12:\"report/daily\";i:2;s:11:\"report/late\";i:3;s:16:\"report/lateearly\";i:4;s:20:\"report/lateearlysumm\";i:5;s:14:\"report/working\";i:6;s:21:\"report/averageworking\";i:7;s:18:\"report/performance\";i:8;s:13:\"report/muster\";i:9;s:17:\"report/attendance\";i:10;s:11:\"tool/backup\";i:11;s:9:\"user/user\";i:12;s:14:\"tool/error_log\";i:13;s:11:\"common/home\";i:14;s:23:\"transaction/transaction\";i:15;s:38:\"transaction/transaction/generate_today\";}s:6:\"modify\";a:16:{i:0;s:12:\"report/today\";i:1;s:12:\"report/daily\";i:2;s:11:\"report/late\";i:3;s:16:\"report/lateearly\";i:4;s:20:\"report/lateearlysumm\";i:5;s:14:\"report/working\";i:6;s:21:\"report/averageworking\";i:7;s:18:\"report/performance\";i:8;s:13:\"report/muster\";i:9;s:17:\"report/attendance\";i:10;s:11:\"tool/backup\";i:11;s:9:\"user/user\";i:12;s:14:\"tool/error_log\";i:13;s:11:\"common/home\";i:14;s:23:\"transaction/transaction\";i:15;s:38:\"transaction/transaction/generate_today\";}s:6:\"delete\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}}', 1),
(11, 1, 'Region Admin', 'a:4:{s:6:\"access\";a:17:{i:0;s:16:\"catalog/employee\";i:1;s:12:\"report/today\";i:2;s:12:\"report/daily\";i:3;s:19:\"report/dailysummary\";i:4;s:11:\"report/late\";i:5;s:14:\"report/working\";i:6;s:21:\"report/averageworking\";i:7;s:18:\"report/performance\";i:8;s:13:\"report/muster\";i:9;s:17:\"report/attendance\";i:10;s:22:\"report/employeehistory\";i:11;s:11:\"tool/backup\";i:12;s:9:\"user/user\";i:13;s:14:\"tool/error_log\";i:14;s:11:\"common/home\";i:15;s:23:\"transaction/transaction\";i:16;s:38:\"transaction/transaction/generate_today\";}s:3:\"add\";a:16:{i:0;s:12:\"report/today\";i:1;s:12:\"report/daily\";i:2;s:19:\"report/dailysummary\";i:3;s:11:\"report/late\";i:4;s:14:\"report/working\";i:5;s:21:\"report/averageworking\";i:6;s:18:\"report/performance\";i:7;s:13:\"report/muster\";i:8;s:17:\"report/attendance\";i:9;s:22:\"report/employeehistory\";i:10;s:11:\"tool/backup\";i:11;s:9:\"user/user\";i:12;s:14:\"tool/error_log\";i:13;s:11:\"common/home\";i:14;s:23:\"transaction/transaction\";i:15;s:38:\"transaction/transaction/generate_today\";}s:6:\"modify\";a:6:{i:0;s:11:\"tool/backup\";i:1;s:9:\"user/user\";i:2;s:14:\"tool/error_log\";i:3;s:11:\"common/home\";i:4;s:23:\"transaction/transaction\";i:5;s:38:\"transaction/transaction/generate_today\";}s:6:\"delete\";a:6:{i:0;s:11:\"tool/backup\";i:1;s:9:\"user/user\";i:2;s:14:\"tool/error_log\";i:3;s:11:\"common/home\";i:4;s:23:\"transaction/transaction\";i:5;s:38:\"transaction/transaction/generate_today\";}}', 1),
(13, 9999, 'Top Administrator', 'a:4:{s:6:\"access\";a:34:{i:0;s:15:\"catalog/company\";i:1;s:14:\"catalog/region\";i:2;s:16:\"catalog/division\";i:3;s:13:\"catalog/state\";i:4;s:12:\"catalog/unit\";i:5;s:18:\"catalog/department\";i:6;s:19:\"catalog/designation\";i:7;s:13:\"catalog/grade\";i:8;s:19:\"catalog/employement\";i:9;s:16:\"catalog/employee\";i:10;s:23:\"catalog/employee_change\";i:11;s:13:\"catalog/shift\";i:12;s:14:\"catalog/device\";i:13;s:18:\"catalog/in_process\";i:14;s:23:\"transaction/manualpunch\";i:15;s:12:\"report/today\";i:16;s:12:\"report/daily\";i:17;s:13:\"report/muster\";i:18;s:22:\"report/employeehistory\";i:19;s:16:\"tool/recalculate\";i:20;s:11:\"tool/backup\";i:21;s:19:\"report/new_employee\";i:22;s:16:\"snippets/snippet\";i:23;s:18:\"snippets/snippet_1\";i:24;s:16:\"snippets/weekoff\";i:25;s:20:\"user/user_permission\";i:26;s:16:\"tool/recalculate\";i:27;s:9:\"user/user\";i:28;s:14:\"tool/error_log\";i:29;s:18:\"catalog/in_process\";i:30;s:11:\"common/home\";i:31;s:23:\"transaction/transaction\";i:32;s:38:\"transaction/transaction/generate_today\";i:33;s:20:\"snippets/yearprocess\";}s:3:\"add\";a:31:{i:0;s:15:\"catalog/company\";i:1;s:14:\"catalog/region\";i:2;s:16:\"catalog/division\";i:3;s:13:\"catalog/state\";i:4;s:12:\"catalog/unit\";i:5;s:18:\"catalog/department\";i:6;s:19:\"catalog/designation\";i:7;s:13:\"catalog/grade\";i:8;s:19:\"catalog/employement\";i:9;s:16:\"catalog/employee\";i:10;s:23:\"catalog/employee_change\";i:11;s:13:\"catalog/shift\";i:12;s:14:\"catalog/device\";i:13;s:23:\"transaction/manualpunch\";i:14;s:12:\"report/today\";i:15;s:12:\"report/daily\";i:16;s:13:\"report/muster\";i:17;s:22:\"report/employeehistory\";i:18;s:16:\"tool/recalculate\";i:19;s:11:\"tool/backup\";i:20;s:16:\"snippets/snippet\";i:21;s:18:\"snippets/snippet_1\";i:22;s:16:\"snippets/weekoff\";i:23;s:20:\"user/user_permission\";i:24;s:16:\"tool/recalculate\";i:25;s:9:\"user/user\";i:26;s:14:\"tool/error_log\";i:27;s:11:\"common/home\";i:28;s:23:\"transaction/transaction\";i:29;s:38:\"transaction/transaction/generate_today\";i:30;s:20:\"snippets/yearprocess\";}s:6:\"modify\";a:31:{i:0;s:15:\"catalog/company\";i:1;s:14:\"catalog/region\";i:2;s:16:\"catalog/division\";i:3;s:13:\"catalog/state\";i:4;s:12:\"catalog/unit\";i:5;s:18:\"catalog/department\";i:6;s:19:\"catalog/designation\";i:7;s:13:\"catalog/grade\";i:8;s:19:\"catalog/employement\";i:9;s:16:\"catalog/employee\";i:10;s:23:\"catalog/employee_change\";i:11;s:13:\"catalog/shift\";i:12;s:14:\"catalog/device\";i:13;s:23:\"transaction/manualpunch\";i:14;s:12:\"report/today\";i:15;s:12:\"report/daily\";i:16;s:13:\"report/muster\";i:17;s:22:\"report/employeehistory\";i:18;s:16:\"tool/recalculate\";i:19;s:11:\"tool/backup\";i:20;s:16:\"snippets/snippet\";i:21;s:18:\"snippets/snippet_1\";i:22;s:16:\"snippets/weekoff\";i:23;s:20:\"user/user_permission\";i:24;s:16:\"tool/recalculate\";i:25;s:9:\"user/user\";i:26;s:14:\"tool/error_log\";i:27;s:11:\"common/home\";i:28;s:23:\"transaction/transaction\";i:29;s:38:\"transaction/transaction/generate_today\";i:30;s:20:\"snippets/yearprocess\";}s:6:\"delete\";a:31:{i:0;s:15:\"catalog/company\";i:1;s:14:\"catalog/region\";i:2;s:16:\"catalog/division\";i:3;s:13:\"catalog/state\";i:4;s:12:\"catalog/unit\";i:5;s:18:\"catalog/department\";i:6;s:19:\"catalog/designation\";i:7;s:13:\"catalog/grade\";i:8;s:19:\"catalog/employement\";i:9;s:16:\"catalog/employee\";i:10;s:23:\"catalog/employee_change\";i:11;s:13:\"catalog/shift\";i:12;s:14:\"catalog/device\";i:13;s:23:\"transaction/manualpunch\";i:14;s:12:\"report/today\";i:15;s:12:\"report/daily\";i:16;s:13:\"report/muster\";i:17;s:22:\"report/employeehistory\";i:18;s:16:\"tool/recalculate\";i:19;s:11:\"tool/backup\";i:20;s:16:\"snippets/snippet\";i:21;s:18:\"snippets/snippet_1\";i:22;s:16:\"snippets/weekoff\";i:23;s:20:\"user/user_permission\";i:24;s:16:\"tool/recalculate\";i:25;s:9:\"user/user\";i:26;s:14:\"tool/error_log\";i:27;s:11:\"common/home\";i:28;s:23:\"transaction/transaction\";i:29;s:38:\"transaction/transaction/generate_today\";i:30;s:20:\"snippets/yearprocess\";}}', 2),
(14, 2, 'Division Admin', 'a:4:{s:6:\"access\";a:9:{i:0;s:18:\"catalog/in_process\";i:1;s:19:\"report/new_employee\";i:2;s:9:\"user/user\";i:3;s:14:\"tool/error_log\";i:4;s:18:\"catalog/in_process\";i:5;s:11:\"common/home\";i:6;s:23:\"transaction/transaction\";i:7;s:38:\"transaction/transaction/generate_today\";i:8;s:20:\"snippets/yearprocess\";}s:3:\"add\";a:7:{i:0;s:19:\"report/new_employee\";i:1;s:9:\"user/user\";i:2;s:14:\"tool/error_log\";i:3;s:11:\"common/home\";i:4;s:23:\"transaction/transaction\";i:5;s:38:\"transaction/transaction/generate_today\";i:6;s:20:\"snippets/yearprocess\";}s:6:\"modify\";a:6:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";i:5;s:20:\"snippets/yearprocess\";}s:6:\"delete\";a:6:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";i:5;s:20:\"snippets/yearprocess\";}}', 2),
(15, 1, 'Region Admin', 'a:4:{s:6:\"access\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}s:3:\"add\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}s:6:\"modify\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}s:6:\"delete\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}}', 2),
(16, 3, 'Site Admin', 'a:4:{s:6:\"access\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}s:3:\"add\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}s:6:\"modify\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}s:6:\"delete\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}}', 2),
(18, 2, 'Division Admin', 'a:4:{s:6:\"access\";a:16:{i:0;s:16:\"catalog/employee\";i:1;s:23:\"catalog/employee_change\";i:2;s:18:\"catalog/in_process\";i:3;s:12:\"report/today\";i:4;s:12:\"report/daily\";i:5;s:13:\"report/muster\";i:6;s:22:\"report/employeehistory\";i:7;s:11:\"tool/backup\";i:8;s:19:\"report/new_employee\";i:9;s:9:\"user/user\";i:10;s:14:\"tool/error_log\";i:11;s:18:\"catalog/in_process\";i:12;s:11:\"common/home\";i:13;s:23:\"transaction/transaction\";i:14;s:38:\"transaction/transaction/generate_today\";i:15;s:20:\"snippets/yearprocess\";}s:3:\"add\";a:14:{i:0;s:16:\"catalog/employee\";i:1;s:23:\"catalog/employee_change\";i:2;s:12:\"report/today\";i:3;s:12:\"report/daily\";i:4;s:13:\"report/muster\";i:5;s:22:\"report/employeehistory\";i:6;s:11:\"tool/backup\";i:7;s:19:\"report/new_employee\";i:8;s:9:\"user/user\";i:9;s:14:\"tool/error_log\";i:10;s:11:\"common/home\";i:11;s:23:\"transaction/transaction\";i:12;s:38:\"transaction/transaction/generate_today\";i:13;s:20:\"snippets/yearprocess\";}s:6:\"modify\";a:13:{i:0;s:16:\"catalog/employee\";i:1;s:23:\"catalog/employee_change\";i:2;s:12:\"report/today\";i:3;s:12:\"report/daily\";i:4;s:13:\"report/muster\";i:5;s:22:\"report/employeehistory\";i:6;s:11:\"tool/backup\";i:7;s:9:\"user/user\";i:8;s:14:\"tool/error_log\";i:9;s:11:\"common/home\";i:10;s:23:\"transaction/transaction\";i:11;s:38:\"transaction/transaction/generate_today\";i:12;s:20:\"snippets/yearprocess\";}s:6:\"delete\";a:6:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";i:5;s:20:\"snippets/yearprocess\";}}', 4),
(19, 2, 'Division Admin', 'a:4:{s:6:\"access\";a:16:{i:0;s:16:\"catalog/employee\";i:1;s:23:\"catalog/employee_change\";i:2;s:18:\"catalog/in_process\";i:3;s:12:\"report/today\";i:4;s:12:\"report/daily\";i:5;s:13:\"report/muster\";i:6;s:22:\"report/employeehistory\";i:7;s:11:\"tool/backup\";i:8;s:19:\"report/new_employee\";i:9;s:9:\"user/user\";i:10;s:14:\"tool/error_log\";i:11;s:18:\"catalog/in_process\";i:12;s:11:\"common/home\";i:13;s:23:\"transaction/transaction\";i:14;s:38:\"transaction/transaction/generate_today\";i:15;s:20:\"snippets/yearprocess\";}s:3:\"add\";a:14:{i:0;s:16:\"catalog/employee\";i:1;s:23:\"catalog/employee_change\";i:2;s:12:\"report/today\";i:3;s:12:\"report/daily\";i:4;s:13:\"report/muster\";i:5;s:22:\"report/employeehistory\";i:6;s:11:\"tool/backup\";i:7;s:19:\"report/new_employee\";i:8;s:9:\"user/user\";i:9;s:14:\"tool/error_log\";i:10;s:11:\"common/home\";i:11;s:23:\"transaction/transaction\";i:12;s:38:\"transaction/transaction/generate_today\";i:13;s:20:\"snippets/yearprocess\";}s:6:\"modify\";a:13:{i:0;s:16:\"catalog/employee\";i:1;s:23:\"catalog/employee_change\";i:2;s:12:\"report/today\";i:3;s:12:\"report/daily\";i:4;s:13:\"report/muster\";i:5;s:22:\"report/employeehistory\";i:6;s:11:\"tool/backup\";i:7;s:9:\"user/user\";i:8;s:14:\"tool/error_log\";i:9;s:11:\"common/home\";i:10;s:23:\"transaction/transaction\";i:11;s:38:\"transaction/transaction/generate_today\";i:12;s:20:\"snippets/yearprocess\";}s:6:\"delete\";a:6:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";i:5;s:20:\"snippets/yearprocess\";}}', 5),
(27, 2, 'Division Admin', 'a:4:{s:6:\"access\";a:9:{i:0;s:18:\"catalog/in_process\";i:1;s:19:\"report/new_employee\";i:2;s:9:\"user/user\";i:3;s:14:\"tool/error_log\";i:4;s:18:\"catalog/in_process\";i:5;s:11:\"common/home\";i:6;s:23:\"transaction/transaction\";i:7;s:38:\"transaction/transaction/generate_today\";i:8;s:20:\"snippets/yearprocess\";}s:3:\"add\";a:7:{i:0;s:19:\"report/new_employee\";i:1;s:9:\"user/user\";i:2;s:14:\"tool/error_log\";i:3;s:11:\"common/home\";i:4;s:23:\"transaction/transaction\";i:5;s:38:\"transaction/transaction/generate_today\";i:6;s:20:\"snippets/yearprocess\";}s:6:\"modify\";a:6:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";i:5;s:20:\"snippets/yearprocess\";}s:6:\"delete\";a:6:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";i:5;s:20:\"snippets/yearprocess\";}}', 6),
(21, 3, 'Site Admin', 'a:4:{s:6:\"access\";a:22:{i:0;s:16:\"catalog/employee\";i:1;s:12:\"report/today\";i:2;s:12:\"report/daily\";i:3;s:11:\"report/late\";i:4;s:16:\"report/lateearly\";i:5;s:20:\"report/lateearlysumm\";i:6;s:14:\"report/working\";i:7;s:21:\"report/averageworking\";i:8;s:18:\"report/performance\";i:9;s:13:\"report/muster\";i:10;s:17:\"report/attendance\";i:11;s:22:\"report/employeehistory\";i:12;s:13:\"catalog/leave\";i:13;s:17:\"transaction/leave\";i:14;s:21:\"report/leave_register\";i:15;s:19:\"report/leave_report\";i:16;s:11:\"tool/backup\";i:17;s:9:\"user/user\";i:18;s:14:\"tool/error_log\";i:19;s:11:\"common/home\";i:20;s:23:\"transaction/transaction\";i:21;s:38:\"transaction/transaction/generate_today\";}s:3:\"add\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}s:6:\"modify\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}s:6:\"delete\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}}', 4),
(22, 3, 'Site Admin', 'a:4:{s:6:\"access\";a:22:{i:0;s:16:\"catalog/employee\";i:1;s:12:\"report/today\";i:2;s:12:\"report/daily\";i:3;s:11:\"report/late\";i:4;s:16:\"report/lateearly\";i:5;s:20:\"report/lateearlysumm\";i:6;s:14:\"report/working\";i:7;s:21:\"report/averageworking\";i:8;s:18:\"report/performance\";i:9;s:13:\"report/muster\";i:10;s:17:\"report/attendance\";i:11;s:22:\"report/employeehistory\";i:12;s:13:\"catalog/leave\";i:13;s:17:\"transaction/leave\";i:14;s:21:\"report/leave_register\";i:15;s:19:\"report/leave_report\";i:16;s:11:\"tool/backup\";i:17;s:9:\"user/user\";i:18;s:14:\"tool/error_log\";i:19;s:11:\"common/home\";i:20;s:23:\"transaction/transaction\";i:21;s:38:\"transaction/transaction/generate_today\";}s:3:\"add\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}s:6:\"modify\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}s:6:\"delete\";a:5:{i:0;s:9:\"user/user\";i:1;s:14:\"tool/error_log\";i:2;s:11:\"common/home\";i:3;s:23:\"transaction/transaction\";i:4;s:38:\"transaction/transaction/generate_today\";}}', 5),
(23, 9999, 'Top Administrator', 'a:4:{s:6:\"access\";a:55:{i:0;s:15:\"catalog/company\";i:1;s:13:\"catalog/shift\";i:2;s:13:\"catalog/state\";i:3;s:14:\"catalog/region\";i:4;s:16:\"catalog/division\";i:5;s:12:\"catalog/unit\";i:6;s:18:\"catalog/department\";i:7;s:19:\"catalog/designation\";i:8;s:13:\"catalog/grade\";i:9;s:19:\"catalog/employement\";i:10;s:16:\"catalog/employee\";i:11;s:21:\"catalog/employeelocal\";i:12;s:14:\"tool/empimport\";i:13;s:23:\"catalog/employee_change\";i:14;s:22:\"catalog/shift_schedule\";i:15;s:12:\"catalog/week\";i:16;s:15:\"catalog/holiday\";i:17;s:14:\"catalog/device\";i:18;s:23:\"transaction/manualpunch\";i:19;s:12:\"report/today\";i:20;s:12:\"report/daily\";i:21;s:11:\"report/late\";i:22;s:12:\"report/early\";i:23;s:18:\"report/lessworking\";i:24;s:16:\"report/lateearly\";i:25;s:20:\"report/lateearlysumm\";i:26;s:14:\"report/working\";i:27;s:21:\"report/averageworking\";i:28;s:18:\"report/performance\";i:29;s:13:\"report/muster\";i:30;s:17:\"report/attendance\";i:31;s:22:\"report/employeehistory\";i:32;s:18:\"report/manualpunch\";i:33;s:19:\"catalog/leavemaster\";i:34;s:13:\"catalog/leave\";i:35;s:17:\"transaction/leave\";i:36;s:24:\"transaction/leaveprocess\";i:37;s:21:\"report/leave_register\";i:38;s:19:\"report/leave_report\";i:39;s:24:\"report/leave_report_type\";i:40;s:19:\"report/leave_credit\";i:41;s:16:\"tool/externalapi\";i:42;s:16:\"tool/recalculate\";i:43;s:11:\"tool/backup\";i:44;s:16:\"snippets/snippet\";i:45;s:18:\"snippets/snippet_1\";i:46;s:16:\"snippets/weekoff\";i:47;s:20:\"user/user_permission\";i:48;s:16:\"tool/recalculate\";i:49;s:9:\"user/user\";i:50;s:14:\"tool/error_log\";i:51;s:11:\"common/home\";i:52;s:23:\"transaction/transaction\";i:53;s:38:\"transaction/transaction/generate_today\";i:54;s:20:\"snippets/yearprocess\";}s:3:\"add\";a:55:{i:0;s:15:\"catalog/company\";i:1;s:13:\"catalog/shift\";i:2;s:13:\"catalog/state\";i:3;s:14:\"catalog/region\";i:4;s:16:\"catalog/division\";i:5;s:12:\"catalog/unit\";i:6;s:18:\"catalog/department\";i:7;s:19:\"catalog/designation\";i:8;s:13:\"catalog/grade\";i:9;s:19:\"catalog/employement\";i:10;s:16:\"catalog/employee\";i:11;s:21:\"catalog/employeelocal\";i:12;s:14:\"tool/empimport\";i:13;s:23:\"catalog/employee_change\";i:14;s:22:\"catalog/shift_schedule\";i:15;s:12:\"catalog/week\";i:16;s:15:\"catalog/holiday\";i:17;s:14:\"catalog/device\";i:18;s:23:\"transaction/manualpunch\";i:19;s:12:\"report/today\";i:20;s:12:\"report/daily\";i:21;s:11:\"report/late\";i:22;s:12:\"report/early\";i:23;s:18:\"report/lessworking\";i:24;s:16:\"report/lateearly\";i:25;s:20:\"report/lateearlysumm\";i:26;s:14:\"report/working\";i:27;s:21:\"report/averageworking\";i:28;s:18:\"report/performance\";i:29;s:13:\"report/muster\";i:30;s:17:\"report/attendance\";i:31;s:22:\"report/employeehistory\";i:32;s:18:\"report/manualpunch\";i:33;s:19:\"catalog/leavemaster\";i:34;s:13:\"catalog/leave\";i:35;s:17:\"transaction/leave\";i:36;s:24:\"transaction/leaveprocess\";i:37;s:21:\"report/leave_register\";i:38;s:19:\"report/leave_report\";i:39;s:24:\"report/leave_report_type\";i:40;s:19:\"report/leave_credit\";i:41;s:16:\"tool/externalapi\";i:42;s:16:\"tool/recalculate\";i:43;s:11:\"tool/backup\";i:44;s:16:\"snippets/snippet\";i:45;s:18:\"snippets/snippet_1\";i:46;s:16:\"snippets/weekoff\";i:47;s:20:\"user/user_permission\";i:48;s:16:\"tool/recalculate\";i:49;s:9:\"user/user\";i:50;s:14:\"tool/error_log\";i:51;s:11:\"common/home\";i:52;s:23:\"transaction/transaction\";i:53;s:38:\"transaction/transaction/generate_today\";i:54;s:20:\"snippets/yearprocess\";}s:6:\"modify\";a:12:{i:0;s:16:\"tool/recalculate\";i:1;s:16:\"snippets/snippet\";i:2;s:18:\"snippets/snippet_1\";i:3;s:16:\"snippets/weekoff\";i:4;s:20:\"user/user_permission\";i:5;s:16:\"tool/recalculate\";i:6;s:9:\"user/user\";i:7;s:14:\"tool/error_log\";i:8;s:11:\"common/home\";i:9;s:23:\"transaction/transaction\";i:10;s:38:\"transaction/transaction/generate_today\";i:11;s:20:\"snippets/yearprocess\";}s:6:\"delete\";a:12:{i:0;s:16:\"tool/recalculate\";i:1;s:16:\"snippets/snippet\";i:2;s:18:\"snippets/snippet_1\";i:3;s:16:\"snippets/weekoff\";i:4;s:20:\"user/user_permission\";i:5;s:16:\"tool/recalculate\";i:6;s:9:\"user/user\";i:7;s:14:\"tool/error_log\";i:8;s:11:\"common/home\";i:9;s:23:\"transaction/transaction\";i:10;s:38:\"transaction/transaction/generate_today\";i:11;s:20:\"snippets/yearprocess\";}}', 3),
(24, 9999, 'Top Administrator', 'a:4:{s:6:\"access\";a:37:{i:0;s:15:\"catalog/company\";i:1;s:14:\"catalog/region\";i:2;s:16:\"catalog/division\";i:3;s:13:\"catalog/state\";i:4;s:12:\"catalog/unit\";i:5;s:18:\"catalog/department\";i:6;s:19:\"catalog/designation\";i:7;s:13:\"catalog/grade\";i:8;s:19:\"catalog/employement\";i:9;s:16:\"catalog/employee\";i:10;s:23:\"catalog/employee_change\";i:11;s:21:\"catalog/employeelocal\";i:12;s:13:\"catalog/shift\";i:13;s:14:\"catalog/device\";i:14;s:18:\"catalog/in_process\";i:15;s:23:\"transaction/manualpunch\";i:16;s:12:\"report/today\";i:17;s:12:\"report/daily\";i:18;s:13:\"report/muster\";i:19;s:22:\"report/employeehistory\";i:20;s:14:\"tool/empimport\";i:21;s:16:\"tool/externalapi\";i:22;s:16:\"tool/recalculate\";i:23;s:11:\"tool/backup\";i:24;s:19:\"report/new_employee\";i:25;s:16:\"snippets/snippet\";i:26;s:18:\"snippets/snippet_1\";i:27;s:16:\"snippets/weekoff\";i:28;s:20:\"user/user_permission\";i:29;s:16:\"tool/recalculate\";i:30;s:9:\"user/user\";i:31;s:14:\"tool/error_log\";i:32;s:18:\"catalog/in_process\";i:33;s:11:\"common/home\";i:34;s:23:\"transaction/transaction\";i:35;s:38:\"transaction/transaction/generate_today\";i:36;s:20:\"snippets/yearprocess\";}s:3:\"add\";a:34:{i:0;s:15:\"catalog/company\";i:1;s:14:\"catalog/region\";i:2;s:16:\"catalog/division\";i:3;s:13:\"catalog/state\";i:4;s:12:\"catalog/unit\";i:5;s:18:\"catalog/department\";i:6;s:19:\"catalog/designation\";i:7;s:13:\"catalog/grade\";i:8;s:19:\"catalog/employement\";i:9;s:16:\"catalog/employee\";i:10;s:23:\"catalog/employee_change\";i:11;s:21:\"catalog/employeelocal\";i:12;s:13:\"catalog/shift\";i:13;s:14:\"catalog/device\";i:14;s:23:\"transaction/manualpunch\";i:15;s:12:\"report/today\";i:16;s:12:\"report/daily\";i:17;s:13:\"report/muster\";i:18;s:22:\"report/employeehistory\";i:19;s:14:\"tool/empimport\";i:20;s:16:\"tool/externalapi\";i:21;s:16:\"tool/recalculate\";i:22;s:11:\"tool/backup\";i:23;s:16:\"snippets/snippet\";i:24;s:18:\"snippets/snippet_1\";i:25;s:16:\"snippets/weekoff\";i:26;s:20:\"user/user_permission\";i:27;s:16:\"tool/recalculate\";i:28;s:9:\"user/user\";i:29;s:14:\"tool/error_log\";i:30;s:11:\"common/home\";i:31;s:23:\"transaction/transaction\";i:32;s:38:\"transaction/transaction/generate_today\";i:33;s:20:\"snippets/yearprocess\";}s:6:\"modify\";a:12:{i:0;s:16:\"tool/recalculate\";i:1;s:16:\"snippets/snippet\";i:2;s:18:\"snippets/snippet_1\";i:3;s:16:\"snippets/weekoff\";i:4;s:20:\"user/user_permission\";i:5;s:16:\"tool/recalculate\";i:6;s:9:\"user/user\";i:7;s:14:\"tool/error_log\";i:8;s:11:\"common/home\";i:9;s:23:\"transaction/transaction\";i:10;s:38:\"transaction/transaction/generate_today\";i:11;s:20:\"snippets/yearprocess\";}s:6:\"delete\";a:12:{i:0;s:16:\"tool/recalculate\";i:1;s:16:\"snippets/snippet\";i:2;s:18:\"snippets/snippet_1\";i:3;s:16:\"snippets/weekoff\";i:4;s:20:\"user/user_permission\";i:5;s:16:\"tool/recalculate\";i:6;s:9:\"user/user\";i:7;s:14:\"tool/error_log\";i:8;s:11:\"common/home\";i:9;s:23:\"transaction/transaction\";i:10;s:38:\"transaction/transaction/generate_today\";i:11;s:20:\"snippets/yearprocess\";}}', 4),
(25, 9999, 'Top Administrator', 'a:4:{s:6:\"access\";a:37:{i:0;s:15:\"catalog/company\";i:1;s:14:\"catalog/region\";i:2;s:16:\"catalog/division\";i:3;s:13:\"catalog/state\";i:4;s:12:\"catalog/unit\";i:5;s:18:\"catalog/department\";i:6;s:19:\"catalog/designation\";i:7;s:13:\"catalog/grade\";i:8;s:19:\"catalog/employement\";i:9;s:16:\"catalog/employee\";i:10;s:23:\"catalog/employee_change\";i:11;s:21:\"catalog/employeelocal\";i:12;s:13:\"catalog/shift\";i:13;s:14:\"catalog/device\";i:14;s:18:\"catalog/in_process\";i:15;s:23:\"transaction/manualpunch\";i:16;s:12:\"report/today\";i:17;s:12:\"report/daily\";i:18;s:13:\"report/muster\";i:19;s:22:\"report/employeehistory\";i:20;s:14:\"tool/empimport\";i:21;s:16:\"tool/externalapi\";i:22;s:16:\"tool/recalculate\";i:23;s:11:\"tool/backup\";i:24;s:19:\"report/new_employee\";i:25;s:16:\"snippets/snippet\";i:26;s:18:\"snippets/snippet_1\";i:27;s:16:\"snippets/weekoff\";i:28;s:20:\"user/user_permission\";i:29;s:16:\"tool/recalculate\";i:30;s:9:\"user/user\";i:31;s:14:\"tool/error_log\";i:32;s:18:\"catalog/in_process\";i:33;s:11:\"common/home\";i:34;s:23:\"transaction/transaction\";i:35;s:38:\"transaction/transaction/generate_today\";i:36;s:20:\"snippets/yearprocess\";}s:3:\"add\";a:34:{i:0;s:15:\"catalog/company\";i:1;s:14:\"catalog/region\";i:2;s:16:\"catalog/division\";i:3;s:13:\"catalog/state\";i:4;s:12:\"catalog/unit\";i:5;s:18:\"catalog/department\";i:6;s:19:\"catalog/designation\";i:7;s:13:\"catalog/grade\";i:8;s:19:\"catalog/employement\";i:9;s:16:\"catalog/employee\";i:10;s:23:\"catalog/employee_change\";i:11;s:21:\"catalog/employeelocal\";i:12;s:13:\"catalog/shift\";i:13;s:14:\"catalog/device\";i:14;s:23:\"transaction/manualpunch\";i:15;s:12:\"report/today\";i:16;s:12:\"report/daily\";i:17;s:13:\"report/muster\";i:18;s:22:\"report/employeehistory\";i:19;s:14:\"tool/empimport\";i:20;s:16:\"tool/externalapi\";i:21;s:16:\"tool/recalculate\";i:22;s:11:\"tool/backup\";i:23;s:16:\"snippets/snippet\";i:24;s:18:\"snippets/snippet_1\";i:25;s:16:\"snippets/weekoff\";i:26;s:20:\"user/user_permission\";i:27;s:16:\"tool/recalculate\";i:28;s:9:\"user/user\";i:29;s:14:\"tool/error_log\";i:30;s:11:\"common/home\";i:31;s:23:\"transaction/transaction\";i:32;s:38:\"transaction/transaction/generate_today\";i:33;s:20:\"snippets/yearprocess\";}s:6:\"modify\";a:12:{i:0;s:16:\"tool/recalculate\";i:1;s:16:\"snippets/snippet\";i:2;s:18:\"snippets/snippet_1\";i:3;s:16:\"snippets/weekoff\";i:4;s:20:\"user/user_permission\";i:5;s:16:\"tool/recalculate\";i:6;s:9:\"user/user\";i:7;s:14:\"tool/error_log\";i:8;s:11:\"common/home\";i:9;s:23:\"transaction/transaction\";i:10;s:38:\"transaction/transaction/generate_today\";i:11;s:20:\"snippets/yearprocess\";}s:6:\"delete\";a:12:{i:0;s:16:\"tool/recalculate\";i:1;s:16:\"snippets/snippet\";i:2;s:18:\"snippets/snippet_1\";i:3;s:16:\"snippets/weekoff\";i:4;s:20:\"user/user_permission\";i:5;s:16:\"tool/recalculate\";i:6;s:9:\"user/user\";i:7;s:14:\"tool/error_log\";i:8;s:11:\"common/home\";i:9;s:23:\"transaction/transaction\";i:10;s:38:\"transaction/transaction/generate_today\";i:11;s:20:\"snippets/yearprocess\";}}', 5),
(26, 9999, 'Top Administrator', 'a:4:{s:6:\"access\";a:15:{i:0;s:18:\"catalog/in_process\";i:1;s:16:\"tool/recalculate\";i:2;s:19:\"report/new_employee\";i:3;s:16:\"snippets/snippet\";i:4;s:18:\"snippets/snippet_1\";i:5;s:16:\"snippets/weekoff\";i:6;s:20:\"user/user_permission\";i:7;s:16:\"tool/recalculate\";i:8;s:9:\"user/user\";i:9;s:14:\"tool/error_log\";i:10;s:18:\"catalog/in_process\";i:11;s:11:\"common/home\";i:12;s:23:\"transaction/transaction\";i:13;s:38:\"transaction/transaction/generate_today\";i:14;s:20:\"snippets/yearprocess\";}s:3:\"add\";a:12:{i:0;s:16:\"tool/recalculate\";i:1;s:16:\"snippets/snippet\";i:2;s:18:\"snippets/snippet_1\";i:3;s:16:\"snippets/weekoff\";i:4;s:20:\"user/user_permission\";i:5;s:16:\"tool/recalculate\";i:6;s:9:\"user/user\";i:7;s:14:\"tool/error_log\";i:8;s:11:\"common/home\";i:9;s:23:\"transaction/transaction\";i:10;s:38:\"transaction/transaction/generate_today\";i:11;s:20:\"snippets/yearprocess\";}s:6:\"modify\";a:12:{i:0;s:16:\"tool/recalculate\";i:1;s:16:\"snippets/snippet\";i:2;s:18:\"snippets/snippet_1\";i:3;s:16:\"snippets/weekoff\";i:4;s:20:\"user/user_permission\";i:5;s:16:\"tool/recalculate\";i:6;s:9:\"user/user\";i:7;s:14:\"tool/error_log\";i:8;s:11:\"common/home\";i:9;s:23:\"transaction/transaction\";i:10;s:38:\"transaction/transaction/generate_today\";i:11;s:20:\"snippets/yearprocess\";}s:6:\"delete\";a:12:{i:0;s:16:\"tool/recalculate\";i:1;s:16:\"snippets/snippet\";i:2;s:18:\"snippets/snippet_1\";i:3;s:16:\"snippets/weekoff\";i:4;s:20:\"user/user_permission\";i:5;s:16:\"tool/recalculate\";i:6;s:9:\"user/user\";i:7;s:14:\"tool/error_log\";i:8;s:11:\"common/home\";i:9;s:23:\"transaction/transaction\";i:10;s:38:\"transaction/transaction/generate_today\";i:11;s:20:\"snippets/yearprocess\";}}', 6);

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher`
--

CREATE TABLE `oc_voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_history`
--

CREATE TABLE `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme`
--

CREATE TABLE `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme_description`
--

CREATE TABLE `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_week`
--

CREATE TABLE `oc_week` (
  `week_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `location` text NOT NULL,
  `department_mumbai` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `division_datas` text NOT NULL,
  `department_pune` text NOT NULL,
  `department_moving` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_week_loc`
--

CREATE TABLE `oc_week_loc` (
  `id` int(11) NOT NULL,
  `week_id` int(11) NOT NULL,
  `location` text NOT NULL,
  `location_id` int(11) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class`
--

CREATE TABLE `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class_description`
--

CREATE TABLE `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(5, 1, 'Pound ', 'lb'),
(6, 1, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone`
--

CREATE TABLE `oc_zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1),
(2, 1, 'Badghis', 'BDG', 1),
(3, 1, 'Baghlan', 'BGL', 1),
(4, 1, 'Balkh', 'BAL', 1),
(5, 1, 'Bamian', 'BAM', 1),
(6, 1, 'Farah', 'FRA', 1),
(7, 1, 'Faryab', 'FYB', 1),
(8, 1, 'Ghazni', 'GHA', 1),
(9, 1, 'Ghowr', 'GHO', 1),
(10, 1, 'Helmand', 'HEL', 1),
(11, 1, 'Herat', 'HER', 1),
(12, 1, 'Jowzjan', 'JOW', 1),
(13, 1, 'Kabul', 'KAB', 1),
(14, 1, 'Kandahar', 'KAN', 1),
(15, 1, 'Kapisa', 'KAP', 1),
(16, 1, 'Khost', 'KHO', 1),
(17, 1, 'Konar', 'KNR', 1),
(18, 1, 'Kondoz', 'KDZ', 1),
(19, 1, 'Laghman', 'LAG', 1),
(20, 1, 'Lowgar', 'LOW', 1),
(21, 1, 'Nangrahar', 'NAN', 1),
(22, 1, 'Nimruz', 'NIM', 1),
(23, 1, 'Nurestan', 'NUR', 1),
(24, 1, 'Oruzgan', 'ORU', 1),
(25, 1, 'Paktia', 'PIA', 1),
(26, 1, 'Paktika', 'PKA', 1),
(27, 1, 'Parwan', 'PAR', 1),
(28, 1, 'Samangan', 'SAM', 1),
(29, 1, 'Sar-e Pol', 'SAR', 1),
(30, 1, 'Takhar', 'TAK', 1),
(31, 1, 'Wardak', 'WAR', 1),
(32, 1, 'Zabol', 'ZAB', 1),
(33, 2, 'Berat', 'BR', 1),
(34, 2, 'Bulqize', 'BU', 1),
(35, 2, 'Delvine', 'DL', 1),
(36, 2, 'Devoll', 'DV', 1),
(37, 2, 'Diber', 'DI', 1),
(38, 2, 'Durres', 'DR', 1),
(39, 2, 'Elbasan', 'EL', 1),
(40, 2, 'Kolonje', 'ER', 1),
(41, 2, 'Fier', 'FR', 1),
(42, 2, 'Gjirokaster', 'GJ', 1),
(43, 2, 'Gramsh', 'GR', 1),
(44, 2, 'Has', 'HA', 1),
(45, 2, 'Kavaje', 'KA', 1),
(46, 2, 'Kurbin', 'KB', 1),
(47, 2, 'Kucove', 'KC', 1),
(48, 2, 'Korce', 'KO', 1),
(49, 2, 'Kruje', 'KR', 1),
(50, 2, 'Kukes', 'KU', 1),
(51, 2, 'Librazhd', 'LB', 1),
(52, 2, 'Lezhe', 'LE', 1),
(53, 2, 'Lushnje', 'LU', 1),
(54, 2, 'Malesi e Madhe', 'MM', 1),
(55, 2, 'Mallakaster', 'MK', 1),
(56, 2, 'Mat', 'MT', 1),
(57, 2, 'Mirdite', 'MR', 1),
(58, 2, 'Peqin', 'PQ', 1),
(59, 2, 'Permet', 'PR', 1),
(60, 2, 'Pogradec', 'PG', 1),
(61, 2, 'Puke', 'PU', 1),
(62, 2, 'Shkoder', 'SH', 1),
(63, 2, 'Skrapar', 'SK', 1),
(64, 2, 'Sarande', 'SR', 1),
(65, 2, 'Tepelene', 'TE', 1),
(66, 2, 'Tropoje', 'TP', 1),
(67, 2, 'Tirane', 'TR', 1),
(68, 2, 'Vlore', 'VL', 1),
(69, 3, 'Adrar', 'ADR', 1),
(70, 3, 'Ain Defla', 'ADE', 1),
(71, 3, 'Ain Temouchent', 'ATE', 1),
(72, 3, 'Alger', 'ALG', 1),
(73, 3, 'Annaba', 'ANN', 1),
(74, 3, 'Batna', 'BAT', 1),
(75, 3, 'Bechar', 'BEC', 1),
(76, 3, 'Bejaia', 'BEJ', 1),
(77, 3, 'Biskra', 'BIS', 1),
(78, 3, 'Blida', 'BLI', 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1),
(80, 3, 'Bouira', 'BOA', 1),
(81, 3, 'Boumerdes', 'BMD', 1),
(82, 3, 'Chlef', 'CHL', 1),
(83, 3, 'Constantine', 'CON', 1),
(84, 3, 'Djelfa', 'DJE', 1),
(85, 3, 'El Bayadh', 'EBA', 1),
(86, 3, 'El Oued', 'EOU', 1),
(87, 3, 'El Tarf', 'ETA', 1),
(88, 3, 'Ghardaia', 'GHA', 1),
(89, 3, 'Guelma', 'GUE', 1),
(90, 3, 'Illizi', 'ILL', 1),
(91, 3, 'Jijel', 'JIJ', 1),
(92, 3, 'Khenchela', 'KHE', 1),
(93, 3, 'Laghouat', 'LAG', 1),
(94, 3, 'Muaskar', 'MUA', 1),
(95, 3, 'Medea', 'MED', 1),
(96, 3, 'Mila', 'MIL', 1),
(97, 3, 'Mostaganem', 'MOS', 1),
(98, 3, 'M\'Sila', 'MSI', 1),
(99, 3, 'Naama', 'NAA', 1),
(100, 3, 'Oran', 'ORA', 1),
(101, 3, 'Ouargla', 'OUA', 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1),
(103, 3, 'Relizane', 'REL', 1),
(104, 3, 'Saida', 'SAI', 1),
(105, 3, 'Setif', 'SET', 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1),
(107, 3, 'Skikda', 'SKI', 1),
(108, 3, 'Souk Ahras', 'SAH', 1),
(109, 3, 'Tamanghasset', 'TAM', 1),
(110, 3, 'Tebessa', 'TEB', 1),
(111, 3, 'Tiaret', 'TIA', 1),
(112, 3, 'Tindouf', 'TIN', 1),
(113, 3, 'Tipaza', 'TIP', 1),
(114, 3, 'Tissemsilt', 'TIS', 1),
(115, 3, 'Tizi Ouzou', 'TOU', 1),
(116, 3, 'Tlemcen', 'TLE', 1),
(117, 4, 'Eastern', 'E', 1),
(118, 4, 'Manu\'a', 'M', 1),
(119, 4, 'Rose Island', 'R', 1),
(120, 4, 'Swains Island', 'S', 1),
(121, 4, 'Western', 'W', 1),
(122, 5, 'Andorra la Vella', 'ALV', 1),
(123, 5, 'Canillo', 'CAN', 1),
(124, 5, 'Encamp', 'ENC', 1),
(125, 5, 'Escaldes-Engordany', 'ESE', 1),
(126, 5, 'La Massana', 'LMA', 1),
(127, 5, 'Ordino', 'ORD', 1),
(128, 5, 'Sant Julia de Loria', 'SJL', 1),
(129, 6, 'Bengo', 'BGO', 1),
(130, 6, 'Benguela', 'BGU', 1),
(131, 6, 'Bie', 'BIE', 1),
(132, 6, 'Cabinda', 'CAB', 1),
(133, 6, 'Cuando-Cubango', 'CCU', 1),
(134, 6, 'Cuanza Norte', 'CNO', 1),
(135, 6, 'Cuanza Sul', 'CUS', 1),
(136, 6, 'Cunene', 'CNN', 1),
(137, 6, 'Huambo', 'HUA', 1),
(138, 6, 'Huila', 'HUI', 1),
(139, 6, 'Luanda', 'LUA', 1),
(140, 6, 'Lunda Norte', 'LNO', 1),
(141, 6, 'Lunda Sul', 'LSU', 1),
(142, 6, 'Malange', 'MAL', 1),
(143, 6, 'Moxico', 'MOX', 1),
(144, 6, 'Namibe', 'NAM', 1),
(145, 6, 'Uige', 'UIG', 1),
(146, 6, 'Zaire', 'ZAI', 1),
(147, 9, 'Saint George', 'ASG', 1),
(148, 9, 'Saint John', 'ASJ', 1),
(149, 9, 'Saint Mary', 'ASM', 1),
(150, 9, 'Saint Paul', 'ASL', 1),
(151, 9, 'Saint Peter', 'ASR', 1),
(152, 9, 'Saint Philip', 'ASH', 1),
(153, 9, 'Barbuda', 'BAR', 1),
(154, 9, 'Redonda', 'RED', 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1),
(156, 10, 'Buenos Aires', 'BA', 1),
(157, 10, 'Catamarca', 'CA', 1),
(158, 10, 'Chaco', 'CH', 1),
(159, 10, 'Chubut', 'CU', 1),
(160, 10, 'Cordoba', 'CO', 1),
(161, 10, 'Corrientes', 'CR', 1),
(162, 10, 'Distrito Federal', 'DF', 1),
(163, 10, 'Entre Rios', 'ER', 1),
(164, 10, 'Formosa', 'FO', 1),
(165, 10, 'Jujuy', 'JU', 1),
(166, 10, 'La Pampa', 'LP', 1),
(167, 10, 'La Rioja', 'LR', 1),
(168, 10, 'Mendoza', 'ME', 1),
(169, 10, 'Misiones', 'MI', 1),
(170, 10, 'Neuquen', 'NE', 1),
(171, 10, 'Rio Negro', 'RN', 1),
(172, 10, 'Salta', 'SA', 1),
(173, 10, 'San Juan', 'SJ', 1),
(174, 10, 'San Luis', 'SL', 1),
(175, 10, 'Santa Cruz', 'SC', 1),
(176, 10, 'Santa Fe', 'SF', 1),
(177, 10, 'Santiago del Estero', 'SD', 1),
(178, 10, 'Tierra del Fuego', 'TF', 1),
(179, 10, 'Tucuman', 'TU', 1),
(180, 11, 'Aragatsotn', 'AGT', 1),
(181, 11, 'Ararat', 'ARR', 1),
(182, 11, 'Armavir', 'ARM', 1),
(183, 11, 'Geghark\'unik\'', 'GEG', 1),
(184, 11, 'Kotayk\'', 'KOT', 1),
(185, 11, 'Lorri', 'LOR', 1),
(186, 11, 'Shirak', 'SHI', 1),
(187, 11, 'Syunik\'', 'SYU', 1),
(188, 11, 'Tavush', 'TAV', 1),
(189, 11, 'Vayots\' Dzor', 'VAY', 1),
(190, 11, 'Yerevan', 'YER', 1),
(191, 13, 'Australian Capital Territory', 'ACT', 1),
(192, 13, 'New South Wales', 'NSW', 1),
(193, 13, 'Northern Territory', 'NT', 1),
(194, 13, 'Queensland', 'QLD', 1),
(195, 13, 'South Australia', 'SA', 1),
(196, 13, 'Tasmania', 'TAS', 1),
(197, 13, 'Victoria', 'VIC', 1),
(198, 13, 'Western Australia', 'WA', 1),
(199, 14, 'Burgenland', 'BUR', 1),
(200, 14, 'Kärnten', 'KAR', 1),
(201, 14, 'Nieder&ouml;sterreich', 'NOS', 1),
(202, 14, 'Ober&ouml;sterreich', 'OOS', 1),
(203, 14, 'Salzburg', 'SAL', 1),
(204, 14, 'Steiermark', 'STE', 1),
(205, 14, 'Tirol', 'TIR', 1),
(206, 14, 'Vorarlberg', 'VOR', 1),
(207, 14, 'Wien', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore\'s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Brestskaya (Brest)', 'BR', 1),
(338, 20, 'Homyel\'skaya (Homyel\')', 'HO', 1),
(339, 20, 'Horad Minsk', 'HM', 1),
(340, 20, 'Hrodzyenskaya (Hrodna)', 'HR', 1),
(341, 20, 'Mahilyowskaya (Mahilyow)', 'MA', 1),
(342, 20, 'Minskaya', 'MI', 1),
(343, 20, 'Vitsyebskaya (Vitsyebsk)', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Liège', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George\'s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith\'s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amapá', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Ceará', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Espírito Santo', 'ES', 1),
(448, 30, 'Goiás', 'GO', 1),
(449, 30, 'Maranhão', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Pará', 'PA', 1),
(454, 30, 'Paraíba', 'PB', 1),
(455, 30, 'Paraná', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piauí', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rondônia', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'São Paulo', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M\'Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O\'Hi', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chi', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovar-Bilogora', 'BB', 1),
(848, 53, 'City of Zagreb', 'CZ', 1),
(849, 53, 'Dubrovnik-Neretva', 'DN', 1),
(850, 53, 'Istra', 'IS', 1),
(851, 53, 'Karlovac', 'KA', 1),
(852, 53, 'Koprivnica-Krizevci', 'KK', 1),
(853, 53, 'Krapina-Zagorje', 'KZ', 1),
(854, 53, 'Lika-Senj', 'LS', 1),
(855, 53, 'Medimurje', 'ME', 1),
(856, 53, 'Osijek-Baranja', 'OB', 1),
(857, 53, 'Pozega-Slavonia', 'PS', 1),
(858, 53, 'Primorje-Gorski Kotar', 'PG', 1),
(859, 53, 'Sibenik', 'SI', 1),
(860, 53, 'Sisak-Moslavina', 'SM', 1),
(861, 53, 'Slavonski Brod-Posavina', 'SB', 1),
(862, 53, 'Split-Dalmatia', 'SD', 1),
(863, 53, 'Varazdin', 'VA', 1),
(864, 53, 'Virovitica-Podravina', 'VP', 1),
(865, 53, 'Vukovar-Srijem', 'VS', 1),
(866, 53, 'Zadar-Knin', 'ZK', 1),
(867, 53, 'Zagreb', 'ZA', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, 'Ústecký', 'U', 1),
(890, 56, 'Jihočeský', 'C', 1),
(891, 56, 'Jihomoravský', 'B', 1),
(892, 56, 'Karlovarský', 'K', 1),
(893, 56, 'Královehradecký', 'H', 1),
(894, 56, 'Liberecký', 'L', 1),
(895, 56, 'Moravskoslezský', 'T', 1),
(896, 56, 'Olomoucký', 'M', 1),
(897, 56, 'Pardubický', 'E', 1),
(898, 56, 'Plzeňský', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'Středočeský', 'S', 1),
(901, 56, 'Vysočina', 'J', 1),
(902, 56, 'Zlínský', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '\'Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma\'iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa\'id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina\'', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina\'', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan Laani', 'AL', 1),
(1086, 72, 'Etela-Suomen Laani', 'ES', 1),
(1087, 72, 'Ita-Suomen Laani', 'IS', 1),
(1088, 72, 'Lansi-Suomen Laani', 'LS', 1),
(1089, 72, 'Lapin Lanani', 'LA', 1),
(1090, 72, 'Oulun Laani', 'OU', 1),
(1114, 74, 'Ain', '01', 1),
(1115, 74, 'Aisne', '02', 1),
(1116, 74, 'Allier', '03', 1),
(1117, 74, 'Alpes de Haute Provence', '04', 1),
(1118, 74, 'Hautes-Alpes', '05', 1),
(1119, 74, 'Alpes Maritimes', '06', 1),
(1120, 74, 'Ard&egrave;che', '07', 1),
(1121, 74, 'Ardennes', '08', 1),
(1122, 74, 'Ari&egrave;ge', '09', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d\'Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-W&uuml;rttemberg', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Th&uuml;ringen', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand\'Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1428, 97, 'Bacs-Kiskun', 'BK', 1),
(1429, 97, 'Baranya', 'BA', 1),
(1430, 97, 'Bekes', 'BE', 1),
(1431, 97, 'Bekescsaba', 'BS', 1),
(1432, 97, 'Borsod-Abauj-Zemplen', 'BZ', 1),
(1433, 97, 'Budapest', 'BU', 1),
(1434, 97, 'Csongrad', 'CS', 1),
(1435, 97, 'Debrecen', 'DE', 1),
(1436, 97, 'Dunaujvaros', 'DU', 1),
(1437, 97, 'Eger', 'EG', 1),
(1438, 97, 'Fejer', 'FE', 1),
(1439, 97, 'Gyor', 'GY', 1),
(1440, 97, 'Gyor-Moson-Sopron', 'GM', 1),
(1441, 97, 'Hajdu-Bihar', 'HB', 1),
(1442, 97, 'Heves', 'HE', 1),
(1443, 97, 'Hodmezovasarhely', 'HO', 1),
(1444, 97, 'Jasz-Nagykun-Szolnok', 'JN', 1),
(1445, 97, 'Kaposvar', 'KA', 1),
(1446, 97, 'Kecskemet', 'KE', 1),
(1447, 97, 'Komarom-Esztergom', 'KO', 1),
(1448, 97, 'Miskolc', 'MI', 1),
(1449, 97, 'Nagykanizsa', 'NA', 1),
(1450, 97, 'Nograd', 'NO', 1),
(1451, 97, 'Nyiregyhaza', 'NY', 1),
(1452, 97, 'Pecs', 'PE', 1),
(1453, 97, 'Pest', 'PS', 1),
(1454, 97, 'Somogy', 'SO', 1),
(1455, 97, 'Sopron', 'SP', 1),
(1456, 97, 'Szabolcs-Szatmar-Bereg', 'SS', 1),
(1457, 97, 'Szeged', 'SZ', 1),
(1458, 97, 'Szekesfehervar', 'SE', 1),
(1459, 97, 'Szolnok', 'SL', 1),
(1460, 97, 'Szombathely', 'SM', 1),
(1461, 97, 'Tatabanya', 'TA', 1),
(1462, 97, 'Tolna', 'TO', 1),
(1463, 97, 'Vas', 'VA', 1),
(1464, 97, 'Veszprem', 'VE', 1),
(1465, 97, 'Zala', 'ZA', 1),
(1466, 97, 'Zalaegerszeg', 'ZZ', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Pondicherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'BoDeTaBek', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta Raya', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1),
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1),
(1559, 101, 'Kerman', 'KRB', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta\'mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1),
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1),
(1597, 103, 'Leitrim', 'LE', 1),
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1),
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1),
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be\'er Sheva', 'BS', 1),
(1613, 104, 'Bika\'at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '\'Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al \'Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa\'', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa\'', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma\'an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Almaty', 'AL', 1),
(1717, 109, 'Almaty City', 'AC', 1),
(1718, 109, 'Aqmola', 'AM', 1),
(1719, 109, 'Aqtobe', 'AQ', 1),
(1720, 109, 'Astana City', 'AS', 1),
(1721, 109, 'Atyrau', 'AT', 1),
(1722, 109, 'Batys Qazaqstan', 'BA', 1),
(1723, 109, 'Bayqongyr City', 'BY', 1),
(1724, 109, 'Mangghystau', 'MA', 1),
(1725, 109, 'Ongtustik Qazaqstan', 'ON', 1),
(1726, 109, 'Pavlodar', 'PA', 1),
(1727, 109, 'Qaraghandy', 'QA', 1),
(1728, 109, 'Qostanay', 'QO', 1),
(1729, 109, 'Qyzylorda', 'QY', 1),
(1730, 109, 'Shyghys Qazaqstan', 'SH', 1),
(1731, 109, 'Soltustik Qazaqstan', 'SO', 1),
(1732, 109, 'Zhambyl', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P\'yongan-bukto', 'PYB', 1),
(1769, 112, 'P\'yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P\'yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch\'ungch\'ong-bukto', 'CO', 1),
(1774, 113, 'Ch\'ungch\'ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch\'on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t\'ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al \'Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra\'', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1819, 117, 'Aizkraukles Rajons', 'AIZ', 1),
(1820, 117, 'Aluksnes Rajons', 'ALU', 1),
(1821, 117, 'Balvu Rajons', 'BAL', 1),
(1822, 117, 'Bauskas Rajons', 'BAU', 1),
(1823, 117, 'Cesu Rajons', 'CES', 1),
(1824, 117, 'Daugavpils Rajons', 'DGR', 1),
(1825, 117, 'Dobeles Rajons', 'DOB', 1),
(1826, 117, 'Gulbenes Rajons', 'GUL', 1),
(1827, 117, 'Jekabpils Rajons', 'JEK', 1),
(1828, 117, 'Jelgavas Rajons', 'JGR', 1),
(1829, 117, 'Kraslavas Rajons', 'KRA', 1),
(1830, 117, 'Kuldigas Rajons', 'KUL', 1),
(1831, 117, 'Liepajas Rajons', 'LPR', 1),
(1832, 117, 'Limbazu Rajons', 'LIM', 1),
(1833, 117, 'Ludzas Rajons', 'LUD', 1),
(1834, 117, 'Madonas Rajons', 'MAD', 1),
(1835, 117, 'Ogres Rajons', 'OGR', 1),
(1836, 117, 'Preilu Rajons', 'PRE', 1),
(1837, 117, 'Rezeknes Rajons', 'RZR', 1),
(1838, 117, 'Rigas Rajons', 'RGR', 1),
(1839, 117, 'Saldus Rajons', 'SAL', 1),
(1840, 117, 'Talsu Rajons', 'TAL', 1),
(1841, 117, 'Tukuma Rajons', 'TUK', 1),
(1842, 117, 'Valkas Rajons', 'VLK', 1),
(1843, 117, 'Valmieras Rajons', 'VLM', 1),
(1844, 117, 'Ventspils Rajons', 'VSR', 1),
(1845, 117, 'Daugavpils', 'DGV', 1),
(1846, 117, 'Jelgava', 'JGV', 1),
(1847, 117, 'Jurmala', 'JUR', 1),
(1848, 117, 'Liepaja', 'LPK', 1),
(1849, 117, 'Rezekne', 'RZK', 1),
(1850, 117, 'Riga', 'RGA', 1),
(1851, 117, 'Ventspils', 'VSL', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale\'s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha\'s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al \'Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati\'', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'JO', 1),
(1972, 129, 'Kedah', 'KE', 1),
(1973, 129, 'Kelantan', 'KL', 1),
(1974, 129, 'Labuan', 'LA', 1),
(1975, 129, 'Melaka', 'ME', 1),
(1976, 129, 'Negeri Sembilan', 'NS', 1),
(1977, 129, 'Pahang', 'PA', 1),
(1978, 129, 'Perak', 'PE', 1),
(1979, 129, 'Perlis', 'PR', 1),
(1980, 129, 'Pulau Pinang', 'PP', 1),
(1981, 129, 'Sabah', 'SA', 1),
(1982, 129, 'Sarawak', 'SR', 1),
(1983, 129, 'Selangor', 'SE', 1),
(1984, 129, 'Terengganu', 'TE', 1),
(1985, 129, 'Wilayah Persekutuan', 'WP', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St‚nga Nistrului', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '035', 1),
(2198, 142, 'Darhan uul', '037', 1),
(2199, 142, 'Hentiy', '039', 1),
(2200, 142, 'Hovsgol', '041', 1),
(2201, 142, 'Hovd', '043', 1),
(2202, 142, 'Uvs', '046', 1),
(2203, 142, 'Tov', '047', 1),
(2204, 142, 'Selenge', '049', 1),
(2205, 142, 'Suhbaatar', '051', 1),
(2206, 142, 'Omnogovi', '053', 1),
(2207, 142, 'Ovorhangay', '055', 1),
(2208, 142, 'Dzavhan', '057', 1),
(2209, 142, 'DundgovL', '059', 1),
(2210, 142, 'Dornod', '061', 1),
(2211, 142, 'Dornogov', '063', 1),
(2212, 142, 'Govi-Sumber', '064', 1),
(2213, 142, 'Govi-Altay', '065', 1),
(2214, 142, 'Bulgan', '067', 1),
(2215, 142, 'Bayanhongor', '069', 1),
(2216, 142, 'Bayan-Olgiy', '071', 1),
(2217, 142, 'Arhangay', '073', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord Brabant', 'NB', 1),
(2336, 150, 'Noord Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke\'s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa\'id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Arges', 'AG', 1),
(2682, 175, 'Bacau', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1),
(2685, 175, 'Botosani', 'BT', 1),
(2686, 175, 'Brasov', 'BV', 1),
(2687, 175, 'Braila', 'BR', 1),
(2688, 175, 'Bucuresti', 'B', 1),
(2689, 175, 'Buzau', 'BZ', 1),
(2690, 175, 'Caras-Severin', 'CS', 1),
(2691, 175, 'Calarasi', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanta', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dimbovita', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galati', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomita', 'IL', 1),
(2703, 175, 'Iasi', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramures', 'MM', 1),
(2706, 175, 'Mehedinti', 'MH', 1),
(2707, 175, 'Mures', 'MS', 1),
(2708, 175, 'Neamt', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Salaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timis', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Valcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Abakan', 'AB', 1),
(2722, 176, 'Aginskoye', 'AG', 1),
(2723, 176, 'Anadyr', 'AN', 1),
(2724, 176, 'Arkahangelsk', 'AR', 1),
(2725, 176, 'Astrakhan', 'AS', 1),
(2726, 176, 'Barnaul', 'BA', 1),
(2727, 176, 'Belgorod', 'BE', 1),
(2728, 176, 'Birobidzhan', 'BI', 1),
(2729, 176, 'Blagoveshchensk', 'BL', 1),
(2730, 176, 'Bryansk', 'BR', 1),
(2731, 176, 'Cheboksary', 'CH', 1),
(2732, 176, 'Chelyabinsk', 'CL', 1),
(2733, 176, 'Cherkessk', 'CR', 1),
(2734, 176, 'Chita', 'CI', 1),
(2735, 176, 'Dudinka', 'DU', 1),
(2736, 176, 'Elista', 'EL', 1),
(2737, 176, 'Gomo-Altaysk', 'GO', 1),
(2738, 176, 'Gorno-Altaysk', 'GA', 1),
(2739, 176, 'Groznyy', 'GR', 1),
(2740, 176, 'Irkutsk', 'IR', 1),
(2741, 176, 'Ivanovo', 'IV', 1),
(2742, 176, 'Izhevsk', 'IZ', 1),
(2743, 176, 'Kalinigrad', 'KA', 1),
(2744, 176, 'Kaluga', 'KL', 1),
(2745, 176, 'Kasnodar', 'KS', 1),
(2746, 176, 'Kazan', 'KZ', 1),
(2747, 176, 'Kemerovo', 'KE', 1),
(2748, 176, 'Khabarovsk', 'KH', 1),
(2749, 176, 'Khanty-Mansiysk', 'KM', 1),
(2750, 176, 'Kostroma', 'KO', 1),
(2751, 176, 'Krasnodar', 'KR', 1),
(2752, 176, 'Krasnoyarsk', 'KN', 1),
(2753, 176, 'Kudymkar', 'KU', 1),
(2754, 176, 'Kurgan', 'KG', 1),
(2755, 176, 'Kursk', 'KK', 1),
(2756, 176, 'Kyzyl', 'KY', 1),
(2757, 176, 'Lipetsk', 'LI', 1),
(2758, 176, 'Magadan', 'MA', 1),
(2759, 176, 'Makhachkala', 'MK', 1),
(2760, 176, 'Maykop', 'MY', 1),
(2761, 176, 'Moscow', 'MO', 1),
(2762, 176, 'Murmansk', 'MU', 1),
(2763, 176, 'Nalchik', 'NA', 1),
(2764, 176, 'Naryan Mar', 'NR', 1),
(2765, 176, 'Nazran', 'NZ', 1),
(2766, 176, 'Nizhniy Novgorod', 'NI', 1),
(2767, 176, 'Novgorod', 'NO', 1),
(2768, 176, 'Novosibirsk', 'NV', 1),
(2769, 176, 'Omsk', 'OM', 1),
(2770, 176, 'Orel', 'OR', 1),
(2771, 176, 'Orenburg', 'OE', 1),
(2772, 176, 'Palana', 'PA', 1),
(2773, 176, 'Penza', 'PE', 1),
(2774, 176, 'Perm', 'PR', 1),
(2775, 176, 'Petropavlovsk-Kamchatskiy', 'PK', 1),
(2776, 176, 'Petrozavodsk', 'PT', 1),
(2777, 176, 'Pskov', 'PS', 1),
(2778, 176, 'Rostov-na-Donu', 'RO', 1),
(2779, 176, 'Ryazan', 'RY', 1),
(2780, 176, 'Salekhard', 'SL', 1),
(2781, 176, 'Samara', 'SA', 1),
(2782, 176, 'Saransk', 'SR', 1),
(2783, 176, 'Saratov', 'SV', 1),
(2784, 176, 'Smolensk', 'SM', 1),
(2785, 176, 'St. Petersburg', 'SP', 1),
(2786, 176, 'Stavropol', 'ST', 1),
(2787, 176, 'Syktyvkar', 'SY', 1),
(2788, 176, 'Tambov', 'TA', 1),
(2789, 176, 'Tomsk', 'TO', 1),
(2790, 176, 'Tula', 'TU', 1),
(2791, 176, 'Tura', 'TR', 1),
(2792, 176, 'Tver', 'TV', 1),
(2793, 176, 'Tyumen', 'TY', 1),
(2794, 176, 'Ufa', 'UF', 1),
(2795, 176, 'Ul\'yanovsk', 'UL', 1),
(2796, 176, 'Ulan-Ude', 'UU', 1),
(2797, 176, 'Ust\'-Ordynskiy', 'US', 1),
(2798, 176, 'Vladikavkaz', 'VL', 1),
(2799, 176, 'Vladimir', 'VA', 1),
(2800, 176, 'Vladivostok', 'VV', 1),
(2801, 176, 'Volgograd', 'VG', 1),
(2802, 176, 'Vologda', 'VD', 1),
(2803, 176, 'Voronezh', 'VO', 1),
(2804, 176, 'Vyatka', 'VY', 1),
(2805, 176, 'Yakutsk', 'YA', 1),
(2806, 176, 'Yaroslavl', 'YR', 1),
(2807, 176, 'Yekaterinburg', 'YE', 1),
(2808, 176, 'Yoshkar-Ola', 'YO', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A\'ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa\'asaleleaga', 'FA', 1),
(2856, 181, 'Gaga\'emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa\'itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va\'a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '\'Asir', 'AS', 1),
(2882, 184, 'Ha\'il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand\' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand\' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrický', 'BA', 1),
(2926, 189, 'Bratislavský', 'BR', 1),
(2927, 189, 'Košický', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Prešovský', 'PR', 1),
(2930, 189, 'Trenčiansky', 'TC', 1),
(2931, 189, 'Trnavský', 'TV', 1),
(2932, 189, 'Žilinský', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1),
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3031, 197, 'Ascension', 'A', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3033, 197, 'Tristan da Cunha', 'T', 1),
(3034, 199, 'A\'ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa\'iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa\'iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1),
(3059, 199, 'Warab', 'WRB', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'G&auml;vleborg', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'J&auml;mtland', 'Z', 1),
(3080, 203, 'J&ouml;nk&ouml;ping', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, '&Ouml;rebro', 'T', 1),
(3085, 203, '&Ouml;sterg&ouml;tland', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'S&ouml;dermanland', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'V&auml;rmland', 'S', 1),
(3091, 203, 'V&auml;sterbotten', 'AC', 1),
(3092, 203, 'V&auml;sternorrland', 'Y', 1),
(3093, 203, 'V&auml;stmanland', 'U', 1),
(3094, 203, 'V&auml;stra G&ouml;taland', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1),
(3104, 204, 'Graub&uuml;nden', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Z&uuml;rich', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1),
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1),
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1),
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t\'ou', 'NT', 1),
(3145, 206, 'P\'eng-hu', 'PH', 1),
(3146, 206, 'P\'ing-tung', 'PT', 1),
(3147, 206, 'T\'ai-chung', 'TG', 1),
(3148, 206, 'T\'ai-nan', 'TA', 1),
(3149, 206, 'T\'ai-pei county', 'TP', 1),
(3150, 206, 'T\'ai-tung', 'TT', 1),
(3151, 206, 'T\'ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T\'ai-chung', 'TH', 1),
(3157, 206, 'T\'ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T\'ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha\'apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava\'u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Adıyaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'Ağrı', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Aydın', 'AYI', 1),
(3326, 215, 'Balıkesir', 'BAL', 1),
(3327, 215, 'Bartın', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bingöl', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, 'Çanakkale', 'CKL', 1),
(3337, 215, 'Çankırı', 'CKR', 1),
(3338, 215, 'Çorum', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbakir', 'DIY', 1),
(3341, 215, 'Düzce', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elazığ', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eskişehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'Gümüşhane', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'Iğdır', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, 'İstanbul', 'IST', 1),
(3355, 215, 'İzmir', 'IZM', 1),
(3356, 215, 'Kahramanmaraş', 'KAH', 1),
(3357, 215, 'Karabük', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'Kırıkkale', 'KRK', 1),
(3364, 215, 'Kırklareli', 'KLR', 1),
(3365, 215, 'Kırşehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'Kütahya', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Muğla', 'MUG', 1),
(3374, 215, 'Muş', 'MUS', 1),
(3375, 215, 'Nevşehir', 'NEV', 1),
(3376, 215, 'Niğde', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, 'Şanlıurfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, 'Şırnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirdağ', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'Uşak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Cherkasy', 'CK', 1),
(3481, 220, 'Chernihiv', 'CH', 1),
(3482, 220, 'Chernivtsi', 'CV', 1),
(3483, 220, 'Crimea', 'CR', 1),
(3484, 220, 'Dnipropetrovs\'k', 'DN', 1),
(3485, 220, 'Donets\'k', 'DO', 1),
(3486, 220, 'Ivano-Frankivs\'k', 'IV', 1),
(3487, 220, 'Kharkiv Kherson', 'KL', 1),
(3488, 220, 'Khmel\'nyts\'kyy', 'KM', 1),
(3489, 220, 'Kirovohrad', 'KR', 1),
(3490, 220, 'Kiev', 'KV', 1),
(3491, 220, 'Kyyiv', 'KY', 1),
(3492, 220, 'Luhans\'k', 'LU', 1),
(3493, 220, 'L\'viv', 'LV', 1),
(3494, 220, 'Mykolayiv', 'MY', 1),
(3495, 220, 'Odesa', 'OD', 1),
(3496, 220, 'Poltava', 'PO', 1),
(3497, 220, 'Rivne', 'RI', 1),
(3498, 220, 'Sevastopol', 'SE', 1),
(3499, 220, 'Sumy', 'SU', 1),
(3500, 220, 'Ternopil\'', 'TE', 1),
(3501, 220, 'Vinnytsya', 'VI', 1),
(3502, 220, 'Volyn\'', 'VO', 1),
(3503, 220, 'Zakarpattya', 'ZK', 1),
(3504, 220, 'Zaporizhzhya', 'ZA', 1),
(3505, 220, 'Zhytomyr', 'ZH', 1),
(3506, 221, 'Abu Zaby', 'AZ', 1),
(3507, 221, '\'Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubayy', 'DU', 1),
(3511, 221, 'R\'as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg\'ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog\'iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', 'AG', 1),
(3752, 230, 'Bac Giang', 'BG', 1),
(3753, 230, 'Bac Kan', 'BK', 1),
(3754, 230, 'Bac Lieu', 'BL', 1),
(3755, 230, 'Bac Ninh', 'BC', 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1),
(3757, 230, 'Ben Tre', 'BN', 1),
(3758, 230, 'Binh Dinh', 'BH', 1),
(3759, 230, 'Binh Duong', 'BU', 1),
(3760, 230, 'Binh Phuoc', 'BP', 1),
(3761, 230, 'Binh Thuan', 'BT', 1),
(3762, 230, 'Ca Mau', 'CM', 1),
(3763, 230, 'Can Tho', 'CT', 1),
(3764, 230, 'Cao Bang', 'CB', 1),
(3765, 230, 'Dak Lak', 'DL', 1),
(3766, 230, 'Dak Nong', 'DG', 1),
(3767, 230, 'Da Nang', 'DN', 1),
(3768, 230, 'Dien Bien', 'DB', 1),
(3769, 230, 'Dong Nai', 'DI', 1),
(3770, 230, 'Dong Thap', 'DT', 1),
(3771, 230, 'Gia Lai', 'GL', 1),
(3772, 230, 'Ha Giang', 'HG', 1),
(3773, 230, 'Hai Duong', 'HD', 1),
(3774, 230, 'Hai Phong', 'HP', 1),
(3775, 230, 'Ha Nam', 'HM', 1),
(3776, 230, 'Ha Noi', 'HI', 1),
(3777, 230, 'Ha Tay', 'HT', 1),
(3778, 230, 'Ha Tinh', 'HH', 1),
(3779, 230, 'Hoa Binh', 'HB', 1),
(3780, 230, 'Ho Chi Minh City', 'HC', 1),
(3781, 230, 'Hau Giang', 'HU', 1),
(3782, 230, 'Hung Yen', 'HY', 1),
(3783, 232, 'Saint Croix', 'C', 1),
(3784, 232, 'Saint John', 'J', 1),
(3785, 232, 'Saint Thomas', 'T', 1),
(3786, 233, 'Alo', 'A', 1),
(3787, 233, 'Sigave', 'S', 1),
(3788, 233, 'Wallis', 'W', 1),
(3789, 235, 'Abyan', 'AB', 1),
(3790, 235, 'Adan', 'AD', 1),
(3791, 235, 'Amran', 'AM', 1),
(3792, 235, 'Al Bayda', 'BA', 1),
(3793, 235, 'Ad Dali', 'DA', 1),
(3794, 235, 'Dhamar', 'DH', 1),
(3795, 235, 'Hadramawt', 'HD', 1),
(3796, 235, 'Hajjah', 'HJ', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma\'rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa\'dah', 'SD', 1),
(3805, 235, 'San\'a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta\'izz', 'TA', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3862, 105, 'Carbonia-Iglesias', 'CI', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3895, 105, 'Medio Campidano', 'VS', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3902, 105, 'Ogliastra', 'OG', 1),
(3903, 105, 'Olbia-Tempio', 'OT', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koroška', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kraška', '10', 1),
(3966, 190, 'Goriška', '11', 1),
(3967, 190, 'Obalno-kraška', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1),
(3971, 138, 'Aguascalientes', 'AG', 1),
(3972, 222, 'Isle of Man', 'IOM', 1),
(3973, 242, 'Andrijevica', '01', 1),
(3974, 242, 'Bar', '02', 1),
(3975, 242, 'Berane', '03', 1),
(3976, 242, 'Bijelo Polje', '04', 1),
(3977, 242, 'Budva', '05', 1),
(3978, 242, 'Cetinje', '06', 1),
(3979, 242, 'Danilovgrad', '07', 1),
(3980, 242, 'Herceg-Novi', '08', 1),
(3981, 242, 'Kolašin', '09', 1),
(3982, 242, 'Kotor', '10', 1),
(3983, 242, 'Mojkovac', '11', 1),
(3984, 242, 'Nikšić', '12', 1),
(3985, 242, 'Plav', '13', 1),
(3986, 242, 'Pljevlja', '14', 1),
(3987, 242, 'Plužine', '15', 1),
(3988, 242, 'Podgorica', '16', 1),
(3989, 242, 'Rožaje', '17', 1),
(3990, 242, 'Šavnik', '18', 1),
(3991, 242, 'Tivat', '19', 1),
(3992, 242, 'Ulcinj', '20', 1),
(3993, 242, 'Žabljak', '21', 1),
(3994, 243, 'Belgrade', '00', 1),
(3995, 243, 'North Bačka', '01', 1),
(3996, 243, 'Central Banat', '02', 1),
(3997, 243, 'North Banat', '03', 1),
(3998, 243, 'South Banat', '04', 1),
(3999, 243, 'West Bačka', '05', 1),
(4000, 243, 'South Bačka', '06', 1),
(4001, 243, 'Srem', '07', 1),
(4002, 243, 'Mačva', '08', 1),
(4003, 243, 'Kolubara', '09', 1),
(4004, 243, 'Podunavlje', '10', 1),
(4005, 243, 'Braničevo', '11', 1),
(4006, 243, 'Šumadija', '12', 1),
(4007, 243, 'Pomoravlje', '13', 1),
(4008, 243, 'Bor', '14', 1),
(4009, 243, 'Zaječar', '15', 1),
(4010, 243, 'Zlatibor', '16', 1),
(4011, 243, 'Moravica', '17', 1),
(4012, 243, 'Raška', '18', 1),
(4013, 243, 'Rasina', '19', 1),
(4014, 243, 'Nišava', '20', 1),
(4015, 243, 'Toplica', '21', 1),
(4016, 243, 'Pirot', '22', 1),
(4017, 243, 'Jablanica', '23', 1),
(4018, 243, 'Pčinja', '24', 1),
(4019, 243, 'Kosovo', 'KM', 1),
(4020, 245, 'Bonaire', 'BO', 1),
(4021, 245, 'Saba', 'SA', 1),
(4022, 245, 'Sint Eustatius', 'SE', 1),
(4023, 248, 'Central Equatoria', 'EC', 1),
(4024, 248, 'Eastern Equatoria', 'EE', 1),
(4025, 248, 'Jonglei', 'JG', 1),
(4026, 248, 'Lakes', 'LK', 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1),
(4028, 248, 'Unity', 'UY', 1),
(4029, 248, 'Upper Nile', 'NU', 1),
(4030, 248, 'Warrap', 'WR', 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1),
(4032, 248, 'Western Equatoria', 'EW', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone_to_geo_zone`
--

CREATE TABLE `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(57, 222, 0, 3, '2010-02-26 22:33:24', '0000-00-00 00:00:00'),
(65, 222, 0, 4, '2010-12-15 15:18:13', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oc_address`
--
ALTER TABLE `oc_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `oc_affiliate`
--
ALTER TABLE `oc_affiliate`
  ADD PRIMARY KEY (`affiliate_id`);

--
-- Indexes for table `oc_affiliate_transaction`
--
ALTER TABLE `oc_affiliate_transaction`
  ADD PRIMARY KEY (`affiliate_transaction_id`);

--
-- Indexes for table `oc_attendance`
--
ALTER TABLE `oc_attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`),
  ADD KEY `punch_date` (`punch_date`),
  ADD KEY `punch_time` (`punch_time`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `oc_attendance_2020_1`
--
ALTER TABLE `oc_attendance_2020_1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`) USING BTREE,
  ADD KEY `punch_date` (`punch_date`) USING BTREE,
  ADD KEY `punch_time` (`punch_time`) USING BTREE,
  ADD KEY `status` (`status`) USING BTREE;

--
-- Indexes for table `oc_attendance_2020_2`
--
ALTER TABLE `oc_attendance_2020_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`) USING BTREE,
  ADD KEY `punch_date` (`punch_date`) USING BTREE,
  ADD KEY `punch_time` (`punch_time`) USING BTREE,
  ADD KEY `status` (`status`) USING BTREE;

--
-- Indexes for table `oc_attendance_2020_3`
--
ALTER TABLE `oc_attendance_2020_3`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`) USING BTREE,
  ADD KEY `punch_date` (`punch_date`) USING BTREE,
  ADD KEY `punch_time` (`punch_time`) USING BTREE,
  ADD KEY `status` (`status`) USING BTREE;

--
-- Indexes for table `oc_attendance_2020_4`
--
ALTER TABLE `oc_attendance_2020_4`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`) USING BTREE,
  ADD KEY `punch_date` (`punch_date`) USING BTREE,
  ADD KEY `punch_time` (`punch_time`) USING BTREE,
  ADD KEY `status` (`status`) USING BTREE;

--
-- Indexes for table `oc_attendance_2020_5`
--
ALTER TABLE `oc_attendance_2020_5`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`) USING BTREE,
  ADD KEY `punch_date` (`punch_date`) USING BTREE,
  ADD KEY `punch_time` (`punch_time`) USING BTREE,
  ADD KEY `status` (`status`) USING BTREE;

--
-- Indexes for table `oc_attendance_new`
--
ALTER TABLE `oc_attendance_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `oc_attribute_description`
--
ALTER TABLE `oc_attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Indexes for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Indexes for table `oc_attribute_group_description`
--
ALTER TABLE `oc_attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Indexes for table `oc_banner`
--
ALTER TABLE `oc_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Indexes for table `oc_banner_image_description`
--
ALTER TABLE `oc_banner_image_description`
  ADD PRIMARY KEY (`banner_image_id`,`language_id`);

--
-- Indexes for table `oc_bill_owner`
--
ALTER TABLE `oc_bill_owner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_categories`
--
ALTER TABLE `oc_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_category`
--
ALTER TABLE `oc_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `oc_category_description`
--
ALTER TABLE `oc_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_category_filter`
--
ALTER TABLE `oc_category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Indexes for table `oc_category_path`
--
ALTER TABLE `oc_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Indexes for table `oc_category_to_layout`
--
ALTER TABLE `oc_category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `oc_category_to_store`
--
ALTER TABLE `oc_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `oc_company`
--
ALTER TABLE `oc_company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `oc_complimentary`
--
ALTER TABLE `oc_complimentary`
  ADD PRIMARY KEY (`complimentary_id`);

--
-- Indexes for table `oc_contractor`
--
ALTER TABLE `oc_contractor`
  ADD PRIMARY KEY (`contractor_id`);

--
-- Indexes for table `oc_contractor_location`
--
ALTER TABLE `oc_contractor_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_country`
--
ALTER TABLE `oc_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `oc_coupon_category`
--
ALTER TABLE `oc_coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Indexes for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Indexes for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Indexes for table `oc_currency`
--
ALTER TABLE `oc_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `oc_customer`
--
ALTER TABLE `oc_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `oc_customer_ban_ip`
--
ALTER TABLE `oc_customer_ban_ip`
  ADD PRIMARY KEY (`customer_ban_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_field`
--
ALTER TABLE `oc_customer_field`
  ADD PRIMARY KEY (`customer_id`,`custom_field_id`,`custom_field_value_id`);

--
-- Indexes for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Indexes for table `oc_customer_group_description`
--
ALTER TABLE `oc_customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Indexes for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Indexes for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_online`
--
ALTER TABLE `oc_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Indexes for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Indexes for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Indexes for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Indexes for table `oc_custom_field_description`
--
ALTER TABLE `oc_custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Indexes for table `oc_custom_field_to_customer_group`
--
ALTER TABLE `oc_custom_field_to_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Indexes for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Indexes for table `oc_custom_field_value_description`
--
ALTER TABLE `oc_custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Indexes for table `oc_department`
--
ALTER TABLE `oc_department`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `oc_designation`
--
ALTER TABLE `oc_designation`
  ADD PRIMARY KEY (`designation_id`);

--
-- Indexes for table `oc_designationparam`
--
ALTER TABLE `oc_designationparam`
  ADD PRIMARY KEY (`designationparam_id`);

--
-- Indexes for table `oc_devices`
--
ALTER TABLE `oc_devices`
  ADD PRIMARY KEY (`device_id`);

--
-- Indexes for table `oc_division`
--
ALTER TABLE `oc_division`
  ADD PRIMARY KEY (`division_id`);

--
-- Indexes for table `oc_doctor`
--
ALTER TABLE `oc_doctor`
  ADD PRIMARY KEY (`doctor_id`);

--
-- Indexes for table `oc_download`
--
ALTER TABLE `oc_download`
  ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `oc_download_description`
--
ALTER TABLE `oc_download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Indexes for table `oc_early_late`
--
ALTER TABLE `oc_early_late`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_employee`
--
ALTER TABLE `oc_employee`
  ADD PRIMARY KEY (`employee_id`),
  ADD KEY `emp_code` (`emp_code`),
  ADD KEY `unit_id` (`unit_id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `dol` (`dol`),
  ADD KEY `division_id` (`division_id`);

--
-- Indexes for table `oc_employee_change_history`
--
ALTER TABLE `oc_employee_change_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_employement`
--
ALTER TABLE `oc_employement`
  ADD PRIMARY KEY (`employement_id`);

--
-- Indexes for table `oc_extension`
--
ALTER TABLE `oc_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Indexes for table `oc_filter`
--
ALTER TABLE `oc_filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `oc_filter_description`
--
ALTER TABLE `oc_filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`);

--
-- Indexes for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Indexes for table `oc_filter_group_description`
--
ALTER TABLE `oc_filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`);

--
-- Indexes for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Indexes for table `oc_grade`
--
ALTER TABLE `oc_grade`
  ADD PRIMARY KEY (`grade_id`);

--
-- Indexes for table `oc_gradeparam`
--
ALTER TABLE `oc_gradeparam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_halfday`
--
ALTER TABLE `oc_halfday`
  ADD PRIMARY KEY (`halfday_id`);

--
-- Indexes for table `oc_holiday`
--
ALTER TABLE `oc_holiday`
  ADD PRIMARY KEY (`holiday_id`);

--
-- Indexes for table `oc_holiday_loc`
--
ALTER TABLE `oc_holiday_loc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_information`
--
ALTER TABLE `oc_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Indexes for table `oc_information_description`
--
ALTER TABLE `oc_information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Indexes for table `oc_information_to_layout`
--
ALTER TABLE `oc_information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `oc_information_to_store`
--
ALTER TABLE `oc_information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `oc_language`
--
ALTER TABLE `oc_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_layout`
--
ALTER TABLE `oc_layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Indexes for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Indexes for table `oc_leave`
--
ALTER TABLE `oc_leave`
  ADD PRIMARY KEY (`leave_id`);

--
-- Indexes for table `oc_leavemaster`
--
ALTER TABLE `oc_leavemaster`
  ADD PRIMARY KEY (`leavemaster_id`);

--
-- Indexes for table `oc_leave_credit_transaction`
--
ALTER TABLE `oc_leave_credit_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_leave_employee`
--
ALTER TABLE `oc_leave_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_leave_transaction`
--
ALTER TABLE `oc_leave_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_leave_transaction_temp`
--
ALTER TABLE `oc_leave_transaction_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Indexes for table `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Indexes for table `oc_location`
--
ALTER TABLE `oc_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Indexes for table `oc_manufacturer_to_store`
--
ALTER TABLE `oc_manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Indexes for table `oc_medicine`
--
ALTER TABLE `oc_medicine`
  ADD PRIMARY KEY (`medicine_id`);

--
-- Indexes for table `oc_option`
--
ALTER TABLE `oc_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `oc_option_description`
--
ALTER TABLE `oc_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Indexes for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  ADD PRIMARY KEY (`option_value_id`);

--
-- Indexes for table `oc_option_value_description`
--
ALTER TABLE `oc_option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`);

--
-- Indexes for table `oc_order`
--
ALTER TABLE `oc_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `oc_order_download`
--
ALTER TABLE `oc_order_download`
  ADD PRIMARY KEY (`order_download_id`);

--
-- Indexes for table `oc_order_field`
--
ALTER TABLE `oc_order_field`
  ADD PRIMARY KEY (`order_id`,`custom_field_id`,`custom_field_value_id`);

--
-- Indexes for table `oc_order_fraud`
--
ALTER TABLE `oc_order_fraud`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Indexes for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Indexes for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  ADD PRIMARY KEY (`order_product_id`);

--
-- Indexes for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Indexes for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Indexes for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Indexes for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `idx_orders_total_orders_id` (`order_id`);

--
-- Indexes for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Indexes for table `oc_owner`
--
ALTER TABLE `oc_owner`
  ADD PRIMARY KEY (`owner_id`);

--
-- Indexes for table `oc_payroll_transaction`
--
ALTER TABLE `oc_payroll_transaction`
  ADD PRIMARY KEY (`t_id`);

--
-- Indexes for table `oc_product`
--
ALTER TABLE `oc_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `oc_product_attribute`
--
ALTER TABLE `oc_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`);

--
-- Indexes for table `oc_product_description`
--
ALTER TABLE `oc_product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_filter`
--
ALTER TABLE `oc_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Indexes for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  ADD PRIMARY KEY (`product_image_id`);

--
-- Indexes for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  ADD PRIMARY KEY (`product_option_id`);

--
-- Indexes for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`);

--
-- Indexes for table `oc_product_profile`
--
ALTER TABLE `oc_product_profile`
  ADD PRIMARY KEY (`product_id`,`profile_id`,`customer_group_id`);

--
-- Indexes for table `oc_product_recurring`
--
ALTER TABLE `oc_product_recurring`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_product_related`
--
ALTER TABLE `oc_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Indexes for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Indexes for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`);

--
-- Indexes for table `oc_product_to_download`
--
ALTER TABLE `oc_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Indexes for table `oc_product_to_layout`
--
ALTER TABLE `oc_product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_product_to_store`
--
ALTER TABLE `oc_product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_profile`
--
ALTER TABLE `oc_profile`
  ADD PRIMARY KEY (`profile_id`);

--
-- Indexes for table `oc_profile_description`
--
ALTER TABLE `oc_profile_description`
  ADD PRIMARY KEY (`profile_id`,`language_id`);

--
-- Indexes for table `oc_region`
--
ALTER TABLE `oc_region`
  ADD PRIMARY KEY (`region_id`);

--
-- Indexes for table `oc_requestform`
--
ALTER TABLE `oc_requestform`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_return`
--
ALTER TABLE `oc_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `oc_return_action`
--
ALTER TABLE `oc_return_action`
  ADD PRIMARY KEY (`return_action_id`,`language_id`);

--
-- Indexes for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Indexes for table `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  ADD PRIMARY KEY (`return_reason_id`,`language_id`);

--
-- Indexes for table `oc_return_status`
--
ALTER TABLE `oc_return_status`
  ADD PRIMARY KEY (`return_status_id`,`language_id`);

--
-- Indexes for table `oc_review`
--
ALTER TABLE `oc_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_setting`
--
ALTER TABLE `oc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `oc_shift`
--
ALTER TABLE `oc_shift`
  ADD PRIMARY KEY (`shift_id`);

--
-- Indexes for table `oc_shift_schedule`
--
ALTER TABLE `oc_shift_schedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_code` (`emp_code`),
  ADD KEY `month` (`month`),
  ADD KEY `unit` (`unit`),
  ADD KEY `year` (`year`),
  ADD KEY `unit_id` (`unit_id`);

--
-- Indexes for table `oc_shift_unit`
--
ALTER TABLE `oc_shift_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_state`
--
ALTER TABLE `oc_state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `oc_status`
--
ALTER TABLE `oc_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  ADD PRIMARY KEY (`stock_status_id`,`language_id`);

--
-- Indexes for table `oc_store`
--
ALTER TABLE `oc_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `oc_task`
--
ALTER TABLE `oc_task`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_task_status`
--
ALTER TABLE `oc_task_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Indexes for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Indexes for table `oc_tax_rate_to_customer_group`
--
ALTER TABLE `oc_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Indexes for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Indexes for table `oc_transaction`
--
ALTER TABLE `oc_transaction`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `emp_id` (`emp_id`),
  ADD KEY `date` (`date`),
  ADD KEY `manual_status` (`manual_status`),
  ADD KEY `department_id` (`department`),
  ADD KEY `unit_id` (`unit_id`) USING BTREE,
  ADD KEY `division_id` (`division_id`),
  ADD KEY `region_id` (`region_id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `contractor_id` (`contractor_id`);

--
-- Indexes for table `oc_transaction_2020_1`
--
ALTER TABLE `oc_transaction_2020_1`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `emp_id` (`emp_id`) USING BTREE,
  ADD KEY `month` (`month`) USING BTREE,
  ADD KEY `year` (`year`) USING BTREE,
  ADD KEY `unit` (`unit`) USING BTREE,
  ADD KEY `department` (`department`) USING BTREE,
  ADD KEY `group` (`group`) USING BTREE,
  ADD KEY `firsthalf_status` (`firsthalf_status`) USING BTREE,
  ADD KEY `secondhalf_status` (`secondhalf_status`) USING BTREE;

--
-- Indexes for table `oc_transaction_2020_2`
--
ALTER TABLE `oc_transaction_2020_2`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `emp_id` (`emp_id`) USING BTREE,
  ADD KEY `month` (`month`) USING BTREE,
  ADD KEY `year` (`year`) USING BTREE,
  ADD KEY `unit` (`unit`) USING BTREE,
  ADD KEY `department` (`department`) USING BTREE,
  ADD KEY `group` (`group`) USING BTREE,
  ADD KEY `firsthalf_status` (`firsthalf_status`) USING BTREE,
  ADD KEY `secondhalf_status` (`secondhalf_status`) USING BTREE;

--
-- Indexes for table `oc_transaction_2020_3`
--
ALTER TABLE `oc_transaction_2020_3`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `emp_id` (`emp_id`) USING BTREE,
  ADD KEY `month` (`month`) USING BTREE,
  ADD KEY `year` (`year`) USING BTREE,
  ADD KEY `unit` (`unit`) USING BTREE,
  ADD KEY `department` (`department`) USING BTREE,
  ADD KEY `group` (`group`) USING BTREE,
  ADD KEY `firsthalf_status` (`firsthalf_status`) USING BTREE,
  ADD KEY `secondhalf_status` (`secondhalf_status`) USING BTREE;

--
-- Indexes for table `oc_transaction_2020_4`
--
ALTER TABLE `oc_transaction_2020_4`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `emp_id` (`emp_id`) USING BTREE,
  ADD KEY `month` (`month`) USING BTREE,
  ADD KEY `year` (`year`) USING BTREE,
  ADD KEY `unit` (`unit`) USING BTREE,
  ADD KEY `department` (`department`) USING BTREE,
  ADD KEY `group` (`group`) USING BTREE,
  ADD KEY `firsthalf_status` (`firsthalf_status`) USING BTREE,
  ADD KEY `secondhalf_status` (`secondhalf_status`) USING BTREE;

--
-- Indexes for table `oc_transaction_2020_5`
--
ALTER TABLE `oc_transaction_2020_5`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `emp_id` (`emp_id`) USING BTREE,
  ADD KEY `month` (`month`) USING BTREE,
  ADD KEY `year` (`year`) USING BTREE,
  ADD KEY `unit` (`unit`) USING BTREE,
  ADD KEY `department` (`department`) USING BTREE,
  ADD KEY `group` (`group`) USING BTREE,
  ADD KEY `firsthalf_status` (`firsthalf_status`) USING BTREE,
  ADD KEY `secondhalf_status` (`secondhalf_status`) USING BTREE;

--
-- Indexes for table `oc_unit`
--
ALTER TABLE `oc_unit`
  ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `oc_url_alias`
--
ALTER TABLE `oc_url_alias`
  ADD PRIMARY KEY (`url_alias_id`);

--
-- Indexes for table `oc_user`
--
ALTER TABLE `oc_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Indexes for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Indexes for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Indexes for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Indexes for table `oc_voucher_theme_description`
--
ALTER TABLE `oc_voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Indexes for table `oc_week`
--
ALTER TABLE `oc_week`
  ADD PRIMARY KEY (`week_id`);

--
-- Indexes for table `oc_week_loc`
--
ALTER TABLE `oc_week_loc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Indexes for table `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Indexes for table `oc_zone`
--
ALTER TABLE `oc_zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oc_address`
--
ALTER TABLE `oc_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_affiliate`
--
ALTER TABLE `oc_affiliate`
  MODIFY `affiliate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_affiliate_transaction`
--
ALTER TABLE `oc_affiliate_transaction`
  MODIFY `affiliate_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_attendance`
--
ALTER TABLE `oc_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `oc_attendance_2020_1`
--
ALTER TABLE `oc_attendance_2020_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oc_attendance_2020_2`
--
ALTER TABLE `oc_attendance_2020_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;

--
-- AUTO_INCREMENT for table `oc_attendance_2020_3`
--
ALTER TABLE `oc_attendance_2020_3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=427;

--
-- AUTO_INCREMENT for table `oc_attendance_2020_4`
--
ALTER TABLE `oc_attendance_2020_4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_attendance_2020_5`
--
ALTER TABLE `oc_attendance_2020_5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_attendance_new`
--
ALTER TABLE `oc_attendance_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_banner`
--
ALTER TABLE `oc_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_bill_owner`
--
ALTER TABLE `oc_bill_owner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_categories`
--
ALTER TABLE `oc_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_company`
--
ALTER TABLE `oc_company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_complimentary`
--
ALTER TABLE `oc_complimentary`
  MODIFY `complimentary_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_contractor`
--
ALTER TABLE `oc_contractor`
  MODIFY `contractor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2352;

--
-- AUTO_INCREMENT for table `oc_contractor_location`
--
ALTER TABLE `oc_contractor_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3619;

--
-- AUTO_INCREMENT for table `oc_country`
--
ALTER TABLE `oc_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- AUTO_INCREMENT for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_currency`
--
ALTER TABLE `oc_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_ban_ip`
--
ALTER TABLE `oc_customer_ban_ip`
  MODIFY `customer_ban_ip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_department`
--
ALTER TABLE `oc_department`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `oc_designation`
--
ALTER TABLE `oc_designation`
  MODIFY `designation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `oc_designationparam`
--
ALTER TABLE `oc_designationparam`
  MODIFY `designationparam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `oc_devices`
--
ALTER TABLE `oc_devices`
  MODIFY `device_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_division`
--
ALTER TABLE `oc_division`
  MODIFY `division_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oc_doctor`
--
ALTER TABLE `oc_doctor`
  MODIFY `doctor_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_download`
--
ALTER TABLE `oc_download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_early_late`
--
ALTER TABLE `oc_early_late`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oc_employee`
--
ALTER TABLE `oc_employee`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `oc_employee_change_history`
--
ALTER TABLE `oc_employee_change_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_employement`
--
ALTER TABLE `oc_employement`
  MODIFY `employement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `oc_extension`
--
ALTER TABLE `oc_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=428;

--
-- AUTO_INCREMENT for table `oc_filter`
--
ALTER TABLE `oc_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oc_grade`
--
ALTER TABLE `oc_grade`
  MODIFY `grade_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `oc_gradeparam`
--
ALTER TABLE `oc_gradeparam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `oc_halfday`
--
ALTER TABLE `oc_halfday`
  MODIFY `halfday_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_holiday`
--
ALTER TABLE `oc_holiday`
  MODIFY `holiday_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_holiday_loc`
--
ALTER TABLE `oc_holiday_loc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oc_information`
--
ALTER TABLE `oc_information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_language`
--
ALTER TABLE `oc_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_layout`
--
ALTER TABLE `oc_layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_leave`
--
ALTER TABLE `oc_leave`
  MODIFY `leave_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oc_leavemaster`
--
ALTER TABLE `oc_leavemaster`
  MODIFY `leavemaster_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oc_leave_credit_transaction`
--
ALTER TABLE `oc_leave_credit_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_leave_employee`
--
ALTER TABLE `oc_leave_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_leave_transaction`
--
ALTER TABLE `oc_leave_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_leave_transaction_temp`
--
ALTER TABLE `oc_leave_transaction_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oc_location`
--
ALTER TABLE `oc_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oc_medicine`
--
ALTER TABLE `oc_medicine`
  MODIFY `medicine_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_option`
--
ALTER TABLE `oc_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order`
--
ALTER TABLE `oc_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_download`
--
ALTER TABLE `oc_order_download`
  MODIFY `order_download_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_owner`
--
ALTER TABLE `oc_owner`
  MODIFY `owner_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_payroll_transaction`
--
ALTER TABLE `oc_payroll_transaction`
  MODIFY `t_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product`
--
ALTER TABLE `oc_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_profile`
--
ALTER TABLE `oc_profile`
  MODIFY `profile_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_region`
--
ALTER TABLE `oc_region`
  MODIFY `region_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oc_requestform`
--
ALTER TABLE `oc_requestform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_return`
--
ALTER TABLE `oc_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_return_action`
--
ALTER TABLE `oc_return_action`
  MODIFY `return_action_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  MODIFY `return_reason_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_return_status`
--
ALTER TABLE `oc_return_status`
  MODIFY `return_status_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_review`
--
ALTER TABLE `oc_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_setting`
--
ALTER TABLE `oc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=443;

--
-- AUTO_INCREMENT for table `oc_shift`
--
ALTER TABLE `oc_shift`
  MODIFY `shift_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oc_shift_schedule`
--
ALTER TABLE `oc_shift_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT for table `oc_shift_unit`
--
ALTER TABLE `oc_shift_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_state`
--
ALTER TABLE `oc_state`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `oc_status`
--
ALTER TABLE `oc_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  MODIFY `stock_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oc_store`
--
ALTER TABLE `oc_store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_task`
--
ALTER TABLE `oc_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=334;

--
-- AUTO_INCREMENT for table `oc_task_status`
--
ALTER TABLE `oc_task_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `oc_transaction`
--
ALTER TABLE `oc_transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;

--
-- AUTO_INCREMENT for table `oc_transaction_2020_1`
--
ALTER TABLE `oc_transaction_2020_1`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `oc_transaction_2020_2`
--
ALTER TABLE `oc_transaction_2020_2`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=414;

--
-- AUTO_INCREMENT for table `oc_transaction_2020_3`
--
ALTER TABLE `oc_transaction_2020_3`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=716;

--
-- AUTO_INCREMENT for table `oc_transaction_2020_4`
--
ALTER TABLE `oc_transaction_2020_4`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_transaction_2020_5`
--
ALTER TABLE `oc_transaction_2020_5`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_unit`
--
ALTER TABLE `oc_unit`
  MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `oc_url_alias`
--
ALTER TABLE `oc_url_alias`
  MODIFY `url_alias_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_user`
--
ALTER TABLE `oc_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_week`
--
ALTER TABLE `oc_week`
  MODIFY `week_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_week_loc`
--
ALTER TABLE `oc_week_loc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_zone`
--
ALTER TABLE `oc_zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4033;

--
-- AUTO_INCREMENT for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
