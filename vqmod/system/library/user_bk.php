<?php
class User {
	private $user_id;
	private $username;
	private $add;
	private $edit;
	private $view;
	private $division;
	private $site;
	private $permission = array();

	public function __construct($registry) {
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');

		if (isset($this->session->data['user_id'])) {
			$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$this->session->data['user_id'] . "' AND status = '1'");

			if ($user_query->num_rows) {
				$this->user_id = $user_query->row['user_id'];
				$this->username = $user_query->row['username'];
				$this->add = $user_query->row['add'];
				$this->edit = $user_query->row['edit'];
				$this->view = $user_query->row['view'];
				$this->division = $user_query->row['division'];
				$this->site = $user_query->row['site'];

				$this->db->query("UPDATE " . DB_PREFIX . "user SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE user_id = '" . (int)$this->session->data['user_id'] . "'");

				$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

				$permissions = unserialize($user_group_query->row['permission']);

				if (is_array($permissions)) {
					foreach ($permissions as $key => $value) {
						$this->permission[$key] = $value;
					}
				}
			} else {
				$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "employee WHERE emp_code = '" . (int)$this->session->data['user_id'] . "' AND status = '1'");
				if ($user_query->num_rows) {
					$this->user_id = $user_query->row['emp_code'];
					$this->username = $user_query->row['username'];
					$this->add = $user_query->row['add'];
					$this->edit = $user_query->row['edit'];
					$this->view = $user_query->row['view'];
					$this->division = $user_query->row['division'];
					$this->site = $user_query->row['site'];

					$this->db->query("UPDATE " . DB_PREFIX . "employee SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE emp_code = '" . (int)$this->session->data['user_id'] . "'");

					$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

					$permissions = unserialize($user_group_query->row['permission']);

					if (is_array($permissions)) {
						foreach ($permissions as $key => $value) {
							$this->permission[$key] = $value;
						}
					}
				} else {
					$this->logout();
				}
			}
		}
	}

	public function login($username, $password) {
		unset($this->session->data['user_id']);
		unset($this->session->data['emp_code']);
		unset($this->session->data['is_dept']);
		unset($this->session->data['dept_name']);
		unset($this->session->data['d_emp_id']);
		unset($this->session->data['is_user']);
		unset($this->session->data['is_super']);
		unset($this->session->data['is_super1']);
		$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE username = '" . $this->db->escape($username) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
		if ($user_query->num_rows) {
			$this->session->data['user_id'] = $user_query->row['user_id'];
			if($user_query->row['is_super'] == '1'){
				$this->session->data['is_super1'] = $user_query->row['is_super'];
				//$this->session->data['d_emp_id'] = 0;//$user_query->row['emp_code'];
			}
			$this->user_id = $user_query->row['user_id'];
			$this->username = $user_query->row['username'];			
			$this->add = $user_query->row['add'];
			$this->edit = $user_query->row['edit'];
			$this->view = $user_query->row['view'];
			$this->division = $user_query->row['division'];
			$this->site = $user_query->row['site'];
			$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

			$permissions = unserialize($user_group_query->row['permission']);

			if (is_array($permissions)) {
				foreach ($permissions as $key => $value) {
					$this->permission[$key] = $value;
				}
			}

			return true;
		} else {
			$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "employee WHERE username = '" . $this->db->escape($username) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
			if ($user_query->num_rows) {
				$this->session->data['user_id'] = $user_query->row['emp_code'];
				$this->add = $user_query->row['add'];
				$this->edit = $user_query->row['edit'];
				$this->view = $user_query->row['view'];
				$this->division = $user_query->row['division'];
				$this->site = $user_query->row['site'];
				if($user_query->row['is_super'] == '1'){
					$this->session->data['is_super'] = $user_query->row['is_super'];
					$this->session->data['d_emp_id'] = $user_query->row['emp_code'];
				} elseif($user_query->row['is_dept'] == '1'){
					$this->session->data['is_dept'] = $user_query->row['is_dept'];
					$this->session->data['dept_name'] = $user_query->row['department'];
					$this->session->data['d_emp_id'] = $user_query->row['emp_code'];
				} else {
					$this->session->data['is_user'] = '1';
					$this->session->data['emp_code'] = $user_query->row['emp_code'];
				}

				$this->user_id = $user_query->row['emp_code'];
				$this->username = $user_query->row['username'];

				$this->db->query("UPDATE " . DB_PREFIX . "employee SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE emp_code = '" . (int)$user_query->row['emp_code'] . "'");

				$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

				$permissions = unserialize($user_group_query->row['permission']);

				if (is_array($permissions)) {
					foreach ($permissions as $key => $value) {
						$this->permission[$key] = $value;
					}
				}
				return true;
			} else {
				return false;
			}
		}
	}

	public function logout() {
		unset($this->session->data['user_id']);
		unset($this->session->data['emp_code']);
		unset($this->session->data['is_dept']);
		unset($this->session->data['dept_name']);
		unset($this->session->data['d_emp_id']);
		unset($this->session->data['is_user']);
		unset($this->session->data['is_super']);
		unset($this->session->data['is_super1']);

		$this->user_id = '';
		$this->username = '';

		session_destroy();
	}

	public function hasPermission($key, $value) {
		if (isset($this->permission[$key])) {
			return in_array($value, $this->permission[$key]);
		} else {
			return false;
		}
	}

	public function isLogged() {
		return $this->user_id;
	}

	public function getId() {
		return $this->user_id;
	}

	public function getadd() {
		return $this->add;
	}

	public function getedit() {
		return $this->edit;
	}

	public function getview() {
		return $this->view;
	}

	public function getdivision() {
		return $this->division;
	}

	public function getsite() {
		return $this->site;
	}

	public function getUserName() {
		if(isset($this->session->data['is_dept']) && $this->session->data['is_dept'] == '1'){
			$user_name = $this->db->query("SELECT `name` FROM " . DB_PREFIX . "employee WHERE `emp_code` = '" . $this->db->escape($this->session->data['d_emp_id']) . "' ")->row['name'];
		} elseif(isset($this->session->data['is_user'])){
			$user_name = $this->db->query("SELECT `name` FROM " . DB_PREFIX . "employee WHERE `emp_code` = '" . $this->db->escape($this->session->data['user_id']) . "' ")->row['name'];
		} elseif(isset($this->session->data['is_super'])){
			$user_name = $this->db->query("SELECT `name` FROM " . DB_PREFIX . "employee WHERE `emp_code` = '" . $this->db->escape($this->session->data['d_emp_id']) . "' ")->row['name'];
		}else {
			$user_name = $this->username;
		}
		return $user_name;
	}
}
?>