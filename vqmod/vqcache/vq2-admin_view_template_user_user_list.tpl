<?php echo $header; ?>
<style type="text/css">
  input:-webkit-autofill,
input:-webkit-autofill:hover, 
input:-webkit-autofill:focus, 
input:-webkit-autofill:active{
    -webkit-box-shadow: 0 0 0 30px white inset !important;
}

.fil {
  position: absolute;
    left: 350px;
    top: 73px;
}
</style>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    
			<div class="heading"><h1><img src="view/image/admin_theme/base5builder_impulsepro/icon-users-large.png" alt="" /> <?php echo $heading_title; ?></h1>
			

      <div class="buttons">
        <?php if($show == 1){ ?>
          <a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a>
          <a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a>
        <?php } ?>
      </div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left"><?php if ($sort == 'username') { ?>
                <a href="<?php echo $sort_username; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_username; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_username; ?>"><?php echo $column_username; ?></a>
                <?php } ?>
                <input type="text" class="" id="user_name" name="user_name" value="<?php echo $user; ?>"></td>
                <input type="hidden" class="" id="user_ids" name="user_ids" value="<?php echo $user_id; ?>">
                <a class="fil btn btn-primary" onclick="filter()">Filter</a>
              </td>
              <td class="left"><?php if ($sort == 'status') { ?>
                <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'date_added') { ?>
                <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                <?php } ?></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($users) { ?>
            <?php foreach ($users as $user) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($user['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $user['user_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $user['user_id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $user['username']; ?></td>
              <td class="left"><?php echo $user['status']; ?></td>
              <td class="left"><?php echo $user['date_added']; ?></td>
              <td class="right"><?php foreach ($user['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>



<script type="text/javascript">
  

  function filter() {
      url = 'index.php?route=user/user&token=<?php echo $token; ?>';

      var user_id = $('input[name=\'user_ids\']').attr('value');
      var user_name = $('input[name=\'user_name\']').attr('value');
      if (user_id && user_name) {
        url += '&user_id=' + encodeURIComponent(user_id);
        url += '&user=' + encodeURIComponent(user_name);
      }

      location = url;
      return false;
  }
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<script src ="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script type="text/javascript">

  (function($) {
    if (!$.curCSS) {
       $.curCSS = $.css;
    }
})(jQuery);


$(document).on('keyup', '#user_name', function() {
  var value = $(this).val();
  if (value.length <= 0) {
    $('input[name=\'user_ids\']').val('');
  }
});

  $('input[name=\'user_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=user/user/autocomplete&token=<?php echo $token; ?>&user_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.username,
            values: item.username,
            user_ids: item.user_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('#user_name').val(ui.item.values);
    document.getElementById("user_name").setAttribute("value", ui.item.values);
    $('input[name=\'user_ids\']').val(ui.item.user_ids);
    return false;
  }
});
</script>
<?php echo $footer; ?> 