<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    
			<div class="heading"><h1><img src="view/image/admin_theme/base5builder_impulsepro/icon-users-large.png" alt="" /> <?php echo $heading_title; ?></h1>
			

      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><span class="required">*</span> <?php echo $entry_name; ?></td>
            <td><input type="text" name="name" value="<?php echo $name; ?>" />
            <td><input type="hidden" name="uid" value="<?php echo $uid; ?>" />
              <?php if ($error_name) { ?>
              <span class="error"><?php echo $error_name; ?></span>
              <?php  } ?></td>
          </tr>
          <tr>
            <td><?php echo 'View'; ?></td>
            <?php foreach($company_datas as $ckey => $cvalue){ ?>
              <td>
                <h4><?php echo $cvalue['company']; ?></h4>
                <div class="scrollbox">
                  <?php $class = 'odd'; ?>
                  <?php foreach ($permissions as $permission => $pvalue) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($permission, $access[$cvalue['company_id']])) { ?>
                    <input type="checkbox" name="permission[access][<?php echo $cvalue['company_id'] ?>][]" value="<?php echo $permission; ?>" checked="checked" />
                    <?php echo $pvalue; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="permission[access][<?php echo $cvalue['company_id'] ?>][]" value="<?php echo $permission; ?>" />
                    <?php echo $pvalue; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
              </td>
            <?php } ?>
          </tr>
          <tr>
            <td><?php echo 'Add'; ?></td>
            <?php foreach($company_datas as $ckey => $cvalue){ ?>
              <td>
                <h4><?php echo $cvalue['company']; ?></h4>
                <div class="scrollbox">
                  <?php $class = 'odd'; ?>
                  <?php foreach ($permissions as $permission => $pvalue) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($permission, $add[$cvalue['company_id']])) { ?>
                    <input type="checkbox" name="permission[add][<?php echo $cvalue['company_id'] ?>][]" value="<?php echo $permission; ?>" checked="checked" />
                    <?php echo $pvalue; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="permission[add][<?php echo $cvalue['company_id'] ?>][]" value="<?php echo $permission; ?>" />
                    <?php echo $pvalue; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
              </td>
            <?php } ?>
          </tr>
          <tr>
            <td><?php echo 'Edit'; ?></td>
            <?php foreach($company_datas as $ckey => $cvalue){ ?>
              <td>
                <h4><?php echo $cvalue['company']; ?></h4>
                <div class="scrollbox">
                  <?php $class = 'odd'; ?>
                  <?php foreach ($permissions as $permission => $pvalue) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($permission, $modify[$cvalue['company_id']])) { ?>
                    <input type="checkbox" name="permission[modify][<?php echo $cvalue['company_id'] ?>][]" value="<?php echo $permission; ?>" checked="checked" />
                    <?php echo $pvalue; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="permission[modify][<?php echo $cvalue['company_id'] ?>][]" value="<?php echo $permission; ?>" />
                    <?php echo $pvalue; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
              </td>
            <?php } ?>
          </tr>
          <tr>
            <td><?php echo 'Delete'; ?></td>
            <?php foreach($company_datas as $ckey => $cvalue){ ?>
              <td>
                <h4><?php echo $cvalue['company']; ?></h4>
                <div class="scrollbox">
                  <?php $class = 'odd'; ?>
                  <?php foreach ($permissions as $permission => $pvalue) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($permission, $delete[$cvalue['company_id']])) { ?>
                    <input type="checkbox" name="permission[delete][<?php echo $cvalue['company_id'] ?>][]" value="<?php echo $permission; ?>" checked="checked" />
                    <?php echo $pvalue; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="permission[delete][<?php echo $cvalue['company_id'] ?>][]" value="<?php echo $permission; ?>" />
                    <?php echo $pvalue; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
              </td>
            <?php } ?>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?> 