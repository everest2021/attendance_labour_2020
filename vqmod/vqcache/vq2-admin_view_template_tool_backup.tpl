<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      
			<h1><img src="view/image/admin_theme/base5builder_impulsepro/icon-backup-large.png" alt="" /> <?php echo $heading_title; ?></h1>
			
      <div class="buttons">
        <form action="<?php echo $restore; ?>" method="post" enctype="multipart/form-data" id="restore">
          <input type="file" name="import" />
          <a onclick="$('#restore').submit();" class="button"><?php echo 'Upload and Process Data'; ?></a>
        </form>
      </div>
    </div>
    <div class="content">
    </div>
  </div>
</div>
<?php echo $footer; ?>